-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 13, 2014 at 11:53 AM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ts_thinksumo_base`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `acc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acc_username` varchar(30) NOT NULL,
  `acc_password` varchar(32) NOT NULL,
  `acc_last_name` varchar(30) NOT NULL,
  `acc_first_name` varchar(60) NOT NULL,
  `acc_type` enum('admin','dev','user') NOT NULL DEFAULT 'user',
  `acc_failed_login` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `acc_status` enum('active','locked','deleted') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`acc_id`),
  UNIQUE KEY `username` (`acc_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`acc_id`, `acc_username`, `acc_password`, `acc_last_name`, `acc_first_name`, `acc_type`, `acc_failed_login`, `acc_status`) VALUES
(1, 'developer', '5e8edd851d2fdfbd7415232c67367cc3', 'Developer', 'ThinkSumo', 'dev', 0, 'active'),
(2, 'admin', 'b913b0f82adfa847d425eac4f404604a', 'Administrator', 'System', 'admin', 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `art_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_title` varchar(100) NOT NULL,
  `art_blurb` varchar(200) NOT NULL,
  `art_slug` varchar(100) NOT NULL,
  `art_content` text NOT NULL,
  `art_thumb` text NOT NULL,
  `art_image` text NOT NULL,
  `art_published` enum('published','draft') NOT NULL,
  `art_featured` enum('yes','no') NOT NULL,
  `art_date` date NOT NULL,
  `art_author` varchar(100) NOT NULL,
  `art_date_created` datetime NOT NULL,
  `art_created_by` varchar(30) NOT NULL,
  `art_date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `art_modified_by` varchar(30) NOT NULL,
  PRIMARY KEY (`art_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `ban_id` int(11) NOT NULL AUTO_INCREMENT,
  `ban_title` varchar(100) NOT NULL,
  `ban_description` varchar(255) NOT NULL,
  `ban_link` varchar(512) NOT NULL,
  `ban_image` text NOT NULL,
  `ban_thumb` text NOT NULL,
  `ban_order` int(11) NOT NULL,
  `ban_published` enum('published','draft') NOT NULL,
  `ban_created_by` varchar(30) NOT NULL,
  `ban_date_created` datetime NOT NULL,
  `ban_date_modified` datetime NOT NULL,
  `ban_modified_by` varchar(100) NOT NULL,
  PRIMARY KEY (`ban_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `pag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pag_title` varchar(140) NOT NULL,
  `pct_id` int(10) unsigned DEFAULT '0',
  `pag_slug` varchar(80) DEFAULT NULL,
  `pag_content` text NOT NULL,
  `pag_date_created` datetime NOT NULL,
  `pag_date_published` datetime DEFAULT NULL,
  `pag_type` enum('editable','static') NOT NULL DEFAULT 'editable',
  `pag_status` enum('published','draft') NOT NULL DEFAULT 'published',
  PRIMARY KEY (`pag_id`),
  UNIQUE KEY `slug` (`pag_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page_category`
--

CREATE TABLE IF NOT EXISTS `page_category` (
  `pct_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pct_name` varchar(50) NOT NULL,
  PRIMARY KEY (`pct_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photo_album`
--

CREATE TABLE IF NOT EXISTS `photo_album` (
  `alb_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alb_name` varchar(50) NOT NULL,
  `alb_description` text NOT NULL,
  `alb_slug` varchar(80) NOT NULL,
  PRIMARY KEY (`alb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('2ee50742686959423ddb693c39ace759', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1393068549, 'a:6:{s:9:"user_data";s:0:"";s:12:"acc_username";s:9:"developer";s:8:"acc_type";s:3:"dev";s:14:"acc_first_name";s:9:"ThinkSumo";s:13:"acc_last_name";s:9:"Developer";s:8:"acc_name";s:19:"ThinkSumo Developer";}'),
('e0dc9f4419e2d032ca9b9327d3ec8376', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1394176048, '');

-- --------------------------------------------------------

--
-- Table structure for table `site_options`
--

CREATE TABLE IF NOT EXISTS `site_options` (
  `opt_id` int(11) NOT NULL AUTO_INCREMENT,
  `opt_name` varchar(256) NOT NULL,
  `opt_slug` varchar(100) NOT NULL,
  `opt_type` enum('text','image') NOT NULL DEFAULT 'text',
  `opt_value` text NOT NULL,
  PRIMARY KEY (`opt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `site_options`
--

INSERT INTO `site_options` (`opt_id`, `opt_name`, `opt_slug`, `opt_type`, `opt_value`) VALUES
(1, 'Site Name', 'site-name', 'text', 'Think Sumo');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `usr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usr_username` varchar(30) NOT NULL,
  `usr_password` varchar(32) NOT NULL,
  `usr_lname` varchar(50) NOT NULL,
  `usr_fname` varchar(100) NOT NULL,
  `usr_birthdate` date DEFAULT NULL,
  `usr_gender` enum('male','female') DEFAULT NULL,
  `usr_email` varchar(100) DEFAULT NULL,
  `usr_image` text,
  `usr_company` varchar(100) DEFAULT NULL,
  `usr_website` varchar(100) DEFAULT NULL,
  `usr_occupation` varchar(100) DEFAULT NULL,
  `usr_mobile` varchar(50) DEFAULT NULL,
  `usr_landline` varchar(50) DEFAULT NULL,
  `usr_address1` varchar(150) DEFAULT NULL,
  `usr_address2` varchar(150) DEFAULT NULL,
  `usr_city` varchar(100) DEFAULT NULL,
  `usr_country` varchar(50) DEFAULT NULL,
  `usr_zip` varchar(10) DEFAULT NULL,
  `usr_failed_login` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `usr_last_login` datetime DEFAULT NULL,
  `usr_status` enum('active','locked','deleted') NOT NULL DEFAULT 'active',
  `usr_date_created` datetime NOT NULL,
  `usr_verification` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`usr_id`),
  UNIQUE KEY `username` (`usr_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`usr_id`, `usr_username`, `usr_password`, `usr_lname`, `usr_fname`, `usr_birthdate`, `usr_gender`, `usr_email`, `usr_image`, `usr_company`, `usr_website`, `usr_occupation`, `usr_mobile`, `usr_landline`, `usr_address1`, `usr_address2`, `usr_city`, `usr_country`, `usr_zip`, `usr_failed_login`, `usr_last_login`, `usr_status`, `usr_date_created`, `usr_verification`) VALUES
(1, 'kbanzuela', 'password', 'Banzuela', 'Karlo', '0000-00-00 00:00:00', 'male', '', '', '', '', '', '', '', '', '', '', '', '', 1, '0000-00-00 00:00:00', 'active', '2014-01-01 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_api_ids`
--

CREATE TABLE IF NOT EXISTS `user_api_ids` (
  `uai_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `uai_type` enum('facebook','twitter','instagram') NOT NULL,
  `uai_api_id` text NOT NULL,
  `uai_api_token` text NOT NULL,
  `uai_api_secret` text NOT NULL,
  PRIMARY KEY (`uai_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_api_ids`
--

INSERT INTO `user_api_ids` (`uai_id`, `usr_id`, `uai_type`, `uai_api_id`, `uai_api_token`, `uai_api_secret`) VALUES
(1, 1, 'facebook', '<p>tes</p>', '<p>test</p>', '<p>test</p>');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
