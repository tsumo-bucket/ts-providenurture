-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2014 at 08:04 AM
-- Server version: 5.6.11
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ts_thinksumo_base`
--
CREATE DATABASE IF NOT EXISTS `ts_thinksumo_base` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ts_thinksumo_base`;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `acc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acc_username` varchar(30) NOT NULL,
  `acc_password` varchar(32) NOT NULL,
  `acc_last_name` varchar(30) NOT NULL,
  `acc_first_name` varchar(60) NOT NULL,
  `acc_type` enum('admin','dev','user') NOT NULL DEFAULT 'user',
  `acc_failed_login` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `acc_status` enum('active','locked','deleted') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`acc_id`),
  UNIQUE KEY `username` (`acc_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`acc_id`, `acc_username`, `acc_password`, `acc_last_name`, `acc_first_name`, `acc_type`, `acc_failed_login`, `acc_status`) VALUES
(1, 'developer', '5e8edd851d2fdfbd7415232c67367cc3', 'Developer', 'ThinkSumo', 'dev', 0, 'active'),
(2, 'admin', 'b913b0f82adfa847d425eac4f404604a', 'Administrator', 'System', 'admin', 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `art_id` int(11) NOT NULL AUTO_INCREMENT,
  `art_title` varchar(100) NOT NULL,
  `art_blurb` varchar(200) NOT NULL,
  `art_slug` varchar(100) NOT NULL,
  `art_content` text NOT NULL,
  `art_thumb` text NOT NULL,
  `art_image` text NOT NULL,
  `art_published` enum('published','draft') NOT NULL,
  `art_featured` enum('yes','no') NOT NULL,
  `art_date` date NOT NULL,
  `art_author` varchar(100) NOT NULL,
  `art_date_created` datetime NOT NULL,
  `art_created_by` varchar(30) NOT NULL,
  `art_date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `art_modified_by` varchar(30) NOT NULL,
  PRIMARY KEY (`art_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `ban_id` int(11) NOT NULL AUTO_INCREMENT,
  `ban_title` varchar(100) NOT NULL,
  `ban_description` varchar(255) NOT NULL,
  `ban_link` varchar(512) NOT NULL,
  `ban_image` text NOT NULL,
  `ban_thumb` text NOT NULL,
  `ban_order` int(11) NOT NULL,
  `ban_published` enum('published','draft') NOT NULL,
  `ban_created_by` varchar(30) NOT NULL,
  `ban_date_created` datetime NOT NULL,
  `ban_date_modified` datetime NOT NULL,
  `ban_modified_by` varchar(100) NOT NULL,
  PRIMARY KEY (`ban_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `pag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pag_title` varchar(140) NOT NULL,
  `pct_id` int(10) unsigned DEFAULT '0',
  `pag_slug` varchar(80) DEFAULT NULL,
  `pag_content` text NOT NULL,
  `pag_date_created` datetime NOT NULL,
  `pag_date_published` datetime DEFAULT NULL,
  `pag_type` enum('editable','static') NOT NULL DEFAULT 'editable',
  `pag_status` enum('published','draft') NOT NULL DEFAULT 'published',
  PRIMARY KEY (`pag_id`),
  UNIQUE KEY `slug` (`pag_slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page_category`
--

CREATE TABLE `page_category` (
  `pct_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pct_name` varchar(50) NOT NULL,
  PRIMARY KEY (`pct_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE `photo` (
  `pho_id` int(11) NOT NULL AUTO_INCREMENT,
  `phg_id` int(11) DEFAULT NULL,
  `pho_src` varchar(5000) DEFAULT NULL,
  `pho_caption` varchar(100) DEFAULT NULL,
  `pho_date_created` datetime DEFAULT NULL,
  `pho_created_by` varchar(50) DEFAULT NULL,
  `pho_order` int(11) NOT NULL,
  PRIMARY KEY (`pho_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=104 ;

--
-- Dumping data for table `photo`
--
-- --------------------------------------------------------

--
-- Table structure for table `photo_album`
--

CREATE TABLE `photo_album` (
  `alb_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alb_name` varchar(50) NOT NULL,
  `alb_description` text NOT NULL,
  `alb_slug` varchar(80) NOT NULL,
  PRIMARY KEY (`alb_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `photo_album`
--

INSERT INTO `photo_album` (`alb_id`, `alb_name`, `alb_description`, `alb_slug`) VALUES
(1, 'Test', '<p>lorem ipsum</p>', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `photo_gallery`
--

CREATE TABLE `photo_gallery` (
  `phg_id` int(11) NOT NULL AUTO_INCREMENT,
  `phg_name` varchar(100) DEFAULT NULL,
  `phg_description` text,
  `phg_status` enum('draft','published') DEFAULT 'draft',
  `phg_date_created` datetime DEFAULT NULL,
  `phg_created_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`phg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `photo_gallery`
--


-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('acc551eac053a0704941f364f35e7cea', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1395298947, 'a:6:{s:9:"user_data";s:0:"";s:12:"acc_username";s:9:"developer";s:8:"acc_type";s:3:"dev";s:14:"acc_first_name";s:9:"ThinkSumo";s:13:"acc_last_name";s:9:"Developer";s:8:"acc_name";s:19:"ThinkSumo Developer";}'),
('be6ca071be63517f4f121bd5ea7c34fd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/53', 1395296908, 'a:1:{s:9:"user_data";s:0:"";}');

-- --------------------------------------------------------

--
-- Table structure for table `site_options`
--

CREATE TABLE `site_options` (
  `opt_id` int(11) NOT NULL AUTO_INCREMENT,
  `opt_name` varchar(256) NOT NULL,
  `opt_slug` varchar(100) NOT NULL,
  `opt_type` enum('text','image') NOT NULL DEFAULT 'text',
  `opt_value` text NOT NULL,
  PRIMARY KEY (`opt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `site_options`
--

INSERT INTO `site_options` (`opt_id`, `opt_name`, `opt_slug`, `opt_type`, `opt_value`) VALUES
(1, 'Site Name', 'site-name', 'text', 'Toyota Anticounterfeit');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
