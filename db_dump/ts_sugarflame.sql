-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2017 at 07:49 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ts_sugarflame`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `acc_id` int(10) UNSIGNED NOT NULL,
  `acc_username` varchar(30) NOT NULL,
  `acc_password` varchar(32) NOT NULL,
  `acc_last_name` varchar(30) NOT NULL,
  `acc_first_name` varchar(60) NOT NULL,
  `acc_type` enum('admin','developer','user') NOT NULL DEFAULT 'user',
  `acc_failed_login` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `acc_status` enum('active','locked','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`acc_id`, `acc_username`, `acc_password`, `acc_last_name`, `acc_first_name`, `acc_type`, `acc_failed_login`, `acc_status`) VALUES
(1, 'developer', '5e8edd851d2fdfbd7415232c67367cc3', 'Developer', 'ThinkSumo', 'developer', 0, 'active'),
(2, 'admin', 'bfb0e820dc42dbec4677348b07dbdf31', 'Administrator', 'System', 'admin', 0, 'active'),
(3, 'jordan.aquino@sumofy.me', '5f4dcc3b5aa765d61d8327deb882cf99', 'Jordan', 'Aquino', 'admin', 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `block`
--

CREATE TABLE `block` (
  `blo_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `blo_user` int(11) NOT NULL,
  `blo_status` enum('active','inactive') NOT NULL,
  `blo_date_created` datetime NOT NULL,
  `blo_date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `cty_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `cty_name` varchar(100) NOT NULL,
  `cty_status` enum('active','archived') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `conversation`
--

CREATE TABLE `conversation` (
  `cnv_id` int(11) NOT NULL,
  `cnv_slug` varchar(100) NOT NULL,
  `cnv_user_1` int(11) NOT NULL COMMENT 'sender',
  `cnv_user_2` int(11) NOT NULL COMMENT 'receiver',
  `cnv_user_1_deleted` enum('yes','no') NOT NULL,
  `cnv_user_1_deleted_date` datetime NOT NULL,
  `cnv_user_2_deleted` enum('yes','no') NOT NULL,
  `cnv_user_2_deleted_date` datetime NOT NULL,
  `cnv_status` enum('active','archived') NOT NULL,
  `cnv_date_created` datetime NOT NULL,
  `cnv_date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `cnt_id` int(11) NOT NULL,
  `cnt_name` varchar(100) NOT NULL,
  `cnt_short_name` varchar(100) NOT NULL,
  `cnt_status` enum('active','archived') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `eml_id` int(11) NOT NULL,
  `eml_mail_to` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_cc` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_bcc` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_subject` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_message` text CHARACTER SET utf8 NOT NULL,
  `eml_from` varchar(200) NOT NULL,
  `eml_from_name` varchar(200) NOT NULL,
  `eml_date_sent` datetime NOT NULL,
  `eml_status` enum('sent','failed','resent') NOT NULL,
  `eml_debug` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `flag`
--

CREATE TABLE `flag` (
  `flg_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `pos_id` int(11) NOT NULL,
  `flg_status` enum('active','inactive') NOT NULL,
  `flg_date_created` datetime NOT NULL,
  `flg_date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE `follow` (
  `fol_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `fol_user` int(11) NOT NULL,
  `fol_status` enum('active','inactive') NOT NULL,
  `fol_date_created` datetime NOT NULL,
  `fol_date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `img_id` int(11) NOT NULL,
  `usr_id` int(11) DEFAULT NULL,
  `img_image` text,
  `img_thumb` text,
  `img_icon` text NOT NULL,
  `img_offset_x` int(11) NOT NULL,
  `img_offset_y` int(11) NOT NULL,
  `img_order` int(11) NOT NULL,
  `img_status` enum('active','rejected','private','inactive') NOT NULL,
  `img_source` enum('upload','facebook','webcam') NOT NULL,
  `img_date_created` datetime NOT NULL,
  `img_date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `inf_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `inf_height` int(11) DEFAULT NULL COMMENT 'in centimeters',
  `inf_height_unit` enum('cm','in','ft') NOT NULL,
  `inf_body_type` enum('slim','athletic','average','curvy','a few extra pounds','overweight','other') DEFAULT NULL,
  `inf_ethnicity` enum('asian','black / african descent','latin / hispanic','east indian','middle eastern','mixed','native american','pacific islander','white / caucasian','other') DEFAULT NULL,
  `inf_smoke` enum('non smoker','light smoker','heavy smoker') DEFAULT NULL,
  `inf_drink` enum('non drinker','social drinker','heavy drinker') DEFAULT NULL,
  `inf_relationship` enum('single','divorced','separated','married but looking','open relationship','widowed') DEFAULT NULL,
  `inf_children` enum('prefer not to say','yes','no') DEFAULT NULL,
  `inf_language` enum('english','espanol','francais','deutsch','chinese','italiano','nederlandese','portuges','russian') DEFAULT NULL,
  `inf_education` enum('high school','some college','associates degree','bachelors degree','graduate degree','post doctorate') DEFAULT NULL,
  `inf_occupation` varchar(100) DEFAULT NULL,
  `inf_spending_habits` int(11) DEFAULT NULL,
  `inf_quality_time` enum('yes','no') DEFAULT NULL COMMENT 'other sugar perks',
  `inf_gifts` enum('yes','no') DEFAULT NULL COMMENT 'other sugar perks',
  `inf_travel` enum('yes','no') DEFAULT NULL COMMENT 'other sugar perks',
  `inf_staycations` enum('yes','no') DEFAULT NULL COMMENT 'other sugar perks',
  `inf_high_life` enum('yes','no') DEFAULT NULL COMMENT 'other sugar perks',
  `inf_simple` enum('yes','no') DEFAULT NULL COMMENT 'other sugar perks',
  `inf_other` varchar(250) DEFAULT NULL,
  `inf_net_worth` int(11) DEFAULT NULL COMMENT 'for sugar sponsors',
  `inf_yearly_income` int(11) DEFAULT NULL COMMENT 'for sugar sponsors',
  `inf_preferred_range_from` int(11) DEFAULT NULL COMMENT 'for sugar babies',
  `inf_preferred_range_to` int(11) DEFAULT NULL COMMENT 'for sugar babies',
  `inf_orientation` enum('gay / lesbian','straight','bisexual','willing to explore','rather not say') DEFAULT NULL,
  `inf_relationship_length` enum('short term','long term') DEFAULT NULL,
  `inf_relationship_loyalty` enum('no strings attached','exclusive') DEFAULT NULL,
  `inf_relationship_preference` enum('rather not say','open to discussion','strictly sexual','serious relationship','marriage','just friends') DEFAULT NULL,
  `inf_sexual_limit` enum('thinking about it','open to new things','anything goes','rather not say','lets keep that to imagination','no experimentation') DEFAULT NULL,
  `inf_privacy_expectations` enum('open to any arrangement','very discreet','ok being in public','depends on attraction level') DEFAULT NULL,
  `inf_date_created` datetime NOT NULL,
  `inf_date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invite`
--

CREATE TABLE `invite` (
  `inv_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `inv_email` varchar(100) NOT NULL,
  `inv_date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `like`
--

CREATE TABLE `like` (
  `lke_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `lke_user` int(11) NOT NULL,
  `lke_date_created` int(11) NOT NULL,
  `lke_status` enum('active','inactive') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `msg_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `msg_content` text NOT NULL,
  `msg_user` int(11) NOT NULL,
  `msg_status` enum('unread','read') NOT NULL,
  `msg_date_created` datetime NOT NULL,
  `cnv_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `not_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `not_message` enum('yes','no') NOT NULL DEFAULT 'yes',
  `not_like` enum('yes','no') NOT NULL DEFAULT 'yes',
  `not_profile` enum('yes','no') NOT NULL DEFAULT 'yes',
  `not_content` enum('yes','no') NOT NULL DEFAULT 'yes',
  `not_email` enum('yes','no') NOT NULL DEFAULT 'yes',
  `not_date_created` datetime NOT NULL,
  `not_date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `pag_id` int(10) UNSIGNED NOT NULL,
  `pag_title` varchar(140) NOT NULL,
  `pct_id` int(10) UNSIGNED DEFAULT '0',
  `pag_slug` varchar(80) DEFAULT NULL,
  `pag_content` text NOT NULL,
  `pag_date_created` datetime NOT NULL,
  `pag_date_published` datetime DEFAULT NULL,
  `pag_type` enum('editable','static') NOT NULL DEFAULT 'editable',
  `pag_status` enum('published','draft') NOT NULL DEFAULT 'published'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`pag_id`, `pag_title`, `pct_id`, `pag_slug`, `pag_content`, `pag_date_created`, `pag_date_published`, `pag_type`, `pag_status`) VALUES
(1, 'About', 0, 'about', '<p><strong></strong><strong>ABOUT US</strong></p><p>SugarFlame is a 100% free sugar dating site where sugar daddies and sugar babies can meet and interact. The service is discreet and private offering an interactive and social environment that allows for safe and discreet communication between sugar daddies and sugar babies.  </p><p><strong>OUR COMMUNITY</strong></p><p>SugarFlame is where sugar daddies enjoy the companionship of beautiful women; and where sugar babies enjoy a life of luxury, comfort and relaxation through the generous support of their sugar babies. We simply provide a venue where all this can happen in a sustainable manner. </p><p><strong>OUR MISSION</strong></p><ul><li>To simplify the dating game by defining the role you take in a relationship - Do you provide Sugar or Flame. </li><li>To provide an interactive community where Sugar Daddies and Sugar Babies can discreetly engage in relationships that meet their needs and expectations. </li></ul><p><strong>OUR PROCESS</strong></p><p>SugarFlame provides a simple and straightforward way for you to find your sugar flame match. </p><p>1. Choose your role. Providers give financial support. Nurturers give love and affection. You simply choose which role best fits you - Sugar or Flame </p><p>2. State your expectation/s. Define the amount of support (Sugar) you can provide your Flame partner and vice versa.  </p><p>With these two simple steps, we match you with thousands who fit your expectations. It’s that simple. </p><p><br><br></p><p><strong>FIND YOUR SUGARFLAME MATCH!</strong></p>', '2017-03-28 10:25:39', '2017-03-28 00:00:00', 'editable', 'published'),
(2, 'Support', 0, 'support', '<h4>Please find your question below, then click to expand the answer.</h4><h4>How long does it take for my profile to be approved?</h4><p>Please be patient, photos and profiles enter the approval process in the order they were received. This usually takes 24 hours, however if the site is experiencing a high volume, this process may take up to 48 hours. If your profile or photos are taking longer, please visit your account <a href="https://www.sugarflame.com/login">Profile</a> and make sure every section is 100% complete.</p><p>NOTE: If a section is complete, there will be a green check mark next to it. If you are not a Premium Member, and have not completed all required sections, your profile and/or photo(s) will not be able to enter the approval process.</p><h4>Why was my account suspended?</h4><p>If your account was suspended, a hold was placed due to suspicious account activity. Your profile and photos will be saved, but you will not be able to use the site, and other members will not be able to view your profile. Messages sent to your account will still be received, but cannot be accessed unless the suspension is fully reviewed and granted by <a href="https://www.sugarflame.com/support">Customer Support</a>. Reasons for account suspension can include:</p><ul><li>Reports by other members about your profile or conduct</li><li>Abusive, vulgar or sexually explicit language</li><li>Asking for money up front or in advance of your date</li><li>Using our site as an escort, or using the Service to solicit clients for an escort service</li><li>Using our site to promote, solicit, or engage in prostitution</li><li>Promoting or advertising a business</li><li>Selling pictures, videos or cam sessions</li><li>Soliciting passwords, bank information or other personal identifying information for commercial or unlawful purposes from other users</li><li>Posting any false, misleading, or inaccurate content about yourself and/or your profile</li><li>Having multiple active accounts</li><li>Creating a profile if you are under the age of 18</li><li>Posting or sending material that exploits people under the age of 18 in a sexual or violent manner, or solicits personal information from anyone under 18</li><li>You have disputed a payment that appears as W8Tech.com, W8 Tech, 2BuySafe.com/W8TECH or ALW*W8TECH.COM on your credit card statement</li></ul><p>NOTE: We reserve the right to suspend accounts for any reason whatsoever, as outlined in our <a href="https://www.seekingarrangement.com/terms">terms and conditions</a>.</p><h4>How do I delete my account?</h4><p>You may deactivate your account by following these instructions:</p><ul><li>Log into your account</li><li>Click on your username/thumbnail in the top right corner</li><li>Select Settings from the drop-down menu</li><li>On the bottom of the page, you will see the “Deactivate Account” link.</li><li>Click the link and follow the prompts.</li></ul><p><br><br></p><p>Once you deactivate your account, your profile will be removed from search and will no longer be viewable by other members.</p><h4>General Questions</h4><h4>Can I restore my deleted messages?</h4><p>Unfortunately, the option to restore messages is not available. We only allow the temporary “Restore” option that shows immediately after deleting a message.</p><h4>I did not receive an activation email, can you re-send it?</h4><p>Activation emails may take a few minutes, please be patient. You can resend the activation email <a href="https://www.sugarflame.com/login">HERE</a>. Please check your spam/junk mail folder for the activation email. Occasionally email providers will automatically mark our messages as spam. If you do not see an activation email in your spam/junk folder, please contact <a href="https://www.sugarflame.com/support">Customer Support</a> with an alternate email address so we can change it to the email associated with your account.</p><br>Gmail users: If the activation email is not in your inbox, please check your Social, Promotion, Spam<p><img src="https://lh3.googleusercontent.com/zav2HhWIW3onSHY8YUyhS18S7MX9zN3KoP_GfpPJOGboOI_5XmFIerfCCGd5ac3a8fnU3FANYGOsIiVlv7nWnvYMd9qDb8zLEfKIddJX4oAsJasp5YzVi5G1WkAoUAzT0ZWdgv0j"></p><p><br></p><h4>How do I use a free Sugar Baby account on SugarFlame?</h4><p>As a Standard (free) Sugar Baby, you are able to use Seeking Arrangement 100% free of charge, however you must always complete the below three steps in order to send messages and favorite members.</p><p>1. Your profile must be 100% complete. Please visit your profile and make sure there’s a green check mark next to each profile section.</p><p>2. Your profile must be approved. Please be patient, this may take 24-48 hours during peak times.</p><p>3. You will also need an approved public photo. Only Premium members can communicate without a public photo.</p><h4>I forgot my password!</h4><p>You can reset your password <a href="https://www.sugarflame.com/password">HERE</a>. Be sure to input the email address you used to create your account.</p><p>Tip: Check your spam/junk mail folder if you do not see a prompt password reset email. Occasionally, email providers will automatically mark our messages as spam.</p><h4>I''m getting an error message while on the website or on the mobile app, what should I do?</h4><p>Please send a detailed description of what you’re seeing, what device you are using to access the site (make and model of your device), which operating system you are using (Windows, Mac OS, iOS (iPhone), Android, etc.), and which internet browser you are using (Google Chrome, Internet Explorer, Firefox, Safari, Dolphin, etc.). Where possible, please include a screenshot of the issue or error message. Send this information to <a href="https://www.sugarflame.com">Customer Support</a> and we will be glad to assist.</p><h4>Can I change myself from a Sugar Baby to Sugar Daddy or Sugar Mommy? (Change your account type)</h4><p>If you signed up with the wrong account type for your own profile, please contact <a href="https://www.sugarflame.com">Customer Support</a> and let us know which account type you would like.</p><p>NOTE: If you need to change the gender to Male or Female for what you are looking for, click on your username in the top right-hand corner, then click “Your Profile.” From there you can select “Basic Info” and change your setting of what you are looking for.</p><p><br><br></p><h4>How do I restore my deactivated account?</h4><p>Simply log into your deactivated account to reactivate. You will then be sent a reactivation email. Click the activation link in it and your account will automatically become restored.</p><h4>How do I change the gender to Male or Female for what I am looking for?</h4><p>To edit your preferences, click on your username in the top right-hand corner, then click “Your Profile.” From there you can select “Basic Info” and change your setting of what you are looking for.</p><p>NOTE: If you want to change yourself from a Sugar Baby (Flame) to a Sugar Daddy (Sugar) or vice-versa, please contact <a href="https://www.sugarflame.com">Customer Support</a> and let us know which account type you would like.</p><h4>How do I block someone that is rude or harassing me?</h4><p>To block a member: Click the gear icon on the user’s profile, next to the Favorite button, and click on ‘Block (username)’. Blocking will prevent the member from communicating with you and from seeing your profile.</p><p>IMPORTANT: If a user has threatened you or committed an act of violence or theft, please contact your local law enforcement agency. Also, report the member from their profile or your conversation with them.</p><h4>How do I report a member violation?</h4><p>Click the gear icon on the user’s profile, next to the Favorite button, and click on ‘Report Member’. Pick the appropriate reason from the list and provide a clear description of the violation. Additionally, you may also block the member which will prevent them from communicating with you and from seeing your profile.</p><p>IMPORTANT: If a user has threatened you or committed an act of violence or theft, please contact your local law enforcement agency. Also, report the member from their profile or your conversation with them as described above.</p><h4>I''m a Sugar Baby. Do you have any tips or advice for finding a Sugar Daddy?</h4><p>Finding the right Sugar Daddy for you is a process that probably won’t happen overnight, but if you’re willing to put in some time and have patience, we’re confident that you’ll eventually find the right person for you.</p><p>Please keep in mind that the more you log in, the more detailed your profile is, and the more photos you have, the more views, favorites, and responses you will receive. Update your profile with more information about yourself and the relationship you’re looking for, uploading more photos and messaging more members. Be more pro-active in your search by breaking the ice with as many members as you can.</p><p><br><br></p><h4>How can I submit a customer support ticket?</h4><p>If you have an issue that is not explained in our FAQ sections, please contact <a href="https://www.sugarflame.com">Customer Support</a> and someone from our dedicated support team will reply back within 24-48 hours.</p><p>You can also send an email to support@SugarFlame.com from the email associated with your SugarFlame account.</p><p>Note: We do not offer customer support via telephone at this time.</p><h4>Privacy Questions</h4><h4>I received a suspicious email. How do I report a phishing scam?</h4><p>Please report all phishing emails by clicking the report phishing feature within your email client and/or email website (most email websites and clients have this feature). If you know which member may have sent this to you, please contact <a href="https://www.sugarflame.com">Customer Support</a> with any details you have so we can take action.</p><h4>How do I hide my profile, join date or activity?</h4><p>You can hide your profile by going to the Settings section of your account and clicking the “Hidden” button next to the “Search and Dashboard” option. Turning this setting to “Hidden” will only hide your profile from search and other member’s dashboards. Members you message and favorite can still view your profile.</p><p>Premium Members have more options of what they can hide, such as:</p><ul><li>Join Date</li><li>Recent Login Location</li><li>Online Status / Last Active Date</li><li>When You View Someone</li><li>When You Favorite Someone</li></ul><p>These features are coming out in our June build. </p><h4>Will a member I blocked be able to view my profile?</h4><p>No. Once you have blocked another member, they will no longer be able to message you, view your profile, or see your profile in his/her search results.</p><h4>Profile Maintenance</h4><h4>What information is not allowed on my profile?</h4><ul><li>Duplicate Information</li><li>Contact Information (last name, phone number, social media usernames, email address, etc.)</li><li>Links to any other websites</li><li>Requests for sex or sexually explicit text</li><li>Specific monetary amounts</li><li>Commercial activity of any kind</li></ul><h4>Photo Information</h4><h4>How long does it take for my photo to be approved?</h4><p>Please be patient, photos and profiles enter the approval process in the order they were received. This usually takes 24 hours, however if the site is experiencing a high volume, this process may take up to 48 hours. If you haven’t completed your profile, and are not a Premium Member, you will need to complete your profile first before your photo(s) enter the approval process.</p><h4>How do I upload photos from my browser?</h4><p>1. To access your photos, click your thumbnail photo on the top right to display the drop down menu. Then click “Your Profile”</p><p>2. Use the left hand menu to choose the “Photos” option.</p><p>3. Click the “Add Public Photo” button if you would like to add a public photo. </p><p><br><br></p><h4>Public photo guidelines</h4><p>When uploading a public photo, please follow the rules listed below. Photos that violate the guideline will be denied and your account may be suspended.</p><h5>Photos can include:</h5><ul><li>Photos must include yourself.</li><li>Bottom nose to chin photos.</li><li>Partially blurred or masked photos.</li><li>Clothed photo of body without your face.</li><li>Couple photos, only if you’re present in the photo.</li><li>Houses, Cars, Yachts are allowed if you’re present in the photo.</li><li>Fully covered lingerie, underwear and bikini photos.</li></ul><h5>Things that are not allowed:</h5><p>It’s important to note that violating the rules below may result in a disabled account or discontinued use, without warning.</p><ul><li>Any photos that doesn’t feature yourself.</li><li>Photos featuring only lips and hands.</li><li>Photos including children.</li><li>Nude or sexually explicit photos.</li><li>Photos containing or depicting illegal content.</li><li>Duplicate photos.</li><li>Photos from other members on SugarFlame.</li><li>Copyrighted photos from any website.</li></ul><h4>Private photo guidelines</h4><p>When uploading a private photo, please follow the rules listed below. Photos that violate the guideline will be denied and your account may be suspended.</p><h5>Private photos can include:</h5><ul><li>Photos must include yourself.</li><li>Bottom nose to chin photos.</li><li>Partially blurred or masked photos.</li><li>Clothed photo of body without your face.</li><li>Couple photos, only if you’re present in the photo.</li><li>Houses, Cars, Yachts are allowed if you’re present in the photo.</li><li>Fully covered lingerie, underwear and bikini photos.</li></ul><h5>Things that are not allowed:</h5><p>It’s important to note that violating the rules below may result in a disabled account or discontinued use, without warning.</p><ul><li>Any photos that doesn’t feature yourself.</li><li>Photos featuring only lips and hands.</li><li>Photos including children.</li><li>Nude or sexually explicit photos.</li><li>Photos containing or depicting illegal content.</li><li>Duplicate photos.</li><li>Photos from other members on SugarFlame.</li><li>Copyrighted photos from any website.</li></ul><h4>How do I delete a photo?</h4><p>1. To access your photos, click your thumbnail photo on the top right to display the drop down menu. Then click “Your Profile”</p><p>2. Use the left hand menu to choose the “Photos” option.</p><p>3. Click the Settings (Gear) icon, and click the “Delete Photo” option located in the lower right hand corner.</p><p><br><br></p><h4>How do I set my main profile photo?</h4><p>Main profile photos must be public. Please note private photos cannot be set as a main profile photo.</p><p>1. To access your photos, click your thumbnail photo on the top right to display the drop down menu. Then click “Your Profile”</p><p>2. Use the left hand menu to choose the “Photos” option.</p><p>3. Click the Settings (Gear) icon, and click the “Set as Primary” option located in the lower right hand corner.</p><h4>How do I prove my identity after my photos were denied?</h4><p>Please send us a photo or scanned copy of your government issued photo ID and we will be happy to approve your photo(s). You may blur out all personal information, as we only need to see your photo. Email the photo from the email address associated with your account to: support@SugarFlame.com.</p><h4>How do I report someone who is using my photo on the website?</h4><p>If you have found someone using your photo on the website, you can request that the photo be removed by writing to customer support. Please <a href="https://www.sugarflame.com">submit a support ticket here</a>. Be sure to provide your email address so we may contact you if we have questions. For example, we may ask you to provide us with a copy of a government issued identification or other evidence that proves the photo is yours.</p><p>Need further assistance? <a href="https://www.sugarflame.com">Report an issue</a>* Responses may take up to 24 hours</p><p>Disclaimer: An arrangement is not an escort service. SugarFlame in no way, shape or form supports escorts or prostitutes using our website for personal gain. Profiles suspect of this usage will be addressed by the SugarFlame Misconduct Team and banned from our website.</p>', '2017-03-28 10:28:24', '2017-03-28 00:00:00', 'editable', 'published'),
(3, 'Privacy Policy', 0, 'privacy-policy', '<h2>Privacy Policy</h2><p>sugarflame.com respects your privacy. All personal information you submitted to sugarflame.com will be kept in the strictest confidence. Personal information you provide to us will only be used by sugarflame.com and will never be sold or given to third parties for any purpose without your consent. We are strongly committed to protecting your privacy online and have taken steps to protect information you share with us. For example, we use SSL encryption to safeguard your credit card data when used in E-commerce transactions. To help us protect your privacy, please maintain secrecy of username and password used in connection with your participation in any sugarflame.com service.</p><h3>Our Commitment</h3><p>We are committed to protecting the privacy of our users. We want to provide a safe and secure user experience. We will use our best efforts to ensure that the information submitted to us remain private, and is used only for the purposes set forth herein.</p><h3>Privacy Scope</h3><p>This privacy policy covers sugarflame.com’s treatment of personally identifiable information that is collected when you are on the sugarflame.com website, and when you use sugarflame.com services. This policy also covers our treatment of any personally identifiable information that our business partners share with us.</p><h3>Information Collected</h3><p>When you register as a member of sugarflame.com, you are asked for a number of personal information, including your name, email address, zip code, phone numbers, occupation, hobbies and interests. Our website also automatically receives and records information on our server logs from your browser, including your IP address, browser type and sugarflame.com cookie information. sugarflame.com uses this information to personalize the content you see, to fulfill your service requests, to inform you of updates to our services, and to alert you to products and services we believe may be of interest to you.</p><h3>Visitor Information</h3><p>We gather information about all users collectively, such as what areas users visits most frequently and what services users access the most. We only use such data anonymously and in the aggregate. This information helps us determine what is the most beneficial for our users and how we can continually create a better overall user experience. We may share this information with our partners, but only in the aggregate, so that they may also understand how our visitors use our website.</p><h3>Specific Information</h3><p>In certain cases, when you signup to use a services, respond to an offer, submit your profile, or purchase a product or service, for example, we may collect more specific information about you, such as your name, address, phone number, email, credit card number, etc. During your visit to our website, we may also ask you for other information, such as information on your interests, likes and dislikes regarding the website, etc., in an effort to deliver the best user experience. Personal information collected are used solely by sugarflame.com and are never sold or given to third parties for any purpose without your explicit consent. We also provide you with the opportunity to opt-opt out of several types of communications at the time of registration. If you choose to receive these types of communications at the time of registration, but later decide you no longer want to receive them, simply change your preferences on your account setting or communicate your preferences to us via email.</p><h3>General Disclosure Information</h3><p>Except as described in this privacy policy, we do not disclose information about your individual visits to sugarflame.com, or personal information you provide, such as your name, address, email, phone number, credit card number, etc., to any third parties, except when we believe it is required by law or to protect sugarflame.com, our users or others. However, we may record and share aggregate information with our partners. We employ other companies and individuals to perform functions on our behalf. Examples may include hosting our web servers, analyzing data, providing marketing services, processing credit card payments, and other services. These companies may have access to your personal information as necessary to perform their functions, but they may not share that information with any third party.</p><h3>Disclosure for Accounts Payable or Collections</h3><p>It is your duty to make sure that your keep your account(s) in good standing with us.  If you have an outstanding balance, or if you dispute your payment to us with your credit card or financial institution for any reason, please note that all information collected from your activity on our website may be disclosed to the bank and/or collections agency we work with for the purposes of establishing proof of user’s identify, proof of use, proof that service was provided, and / or collecting any outstanding debt owed to us.</p><h3>Updating Account Information</h3><p>sugarflame.com allows you to change your personal information at any time. To do so, simply logon to sugarflame.com with your username and password, and you will be able to update the information you have submitted.</p><h3>Online Transactions</h3><p>When you place an order online, your personal details and credit card information are encrypted using SSL encryption technology before being sent over the Internet. Although we cannot guarantee encryption or the privacy of your personal details, SSL makes it very difficult for your information to be stolen or intercepted while being transferred. Our credit card transactions are processed by <a href="http://www.stripe.com"><u>STRIPE</u></a>. All information sent to MultiCards is in the secure SSL environment, which will protect it against disclosure to third parties. When we receive your order, it is on an SSL server. For MultiCards’ privacy statement, please click<a href="https://www.stripe.com/privacy.html"><u>HERE</u></a>. At the point when an end-user wishes to make a payment, his registration details may be passed to Paymentwall that provides payment processing services, in order for Paymentwall to better protect service and end-users from unauthorized payments.</p><h3>Cookies</h3><p>To enhance your experience at sugarflame.com and to access member services, we use a feature on your Internet browser called a ‘cookie’. Cookies are small files that your web browser places on your computer’s hard drive. We use cookies for remembering user preferences, user sessions, tracking visits to understand visitor behavior, and planning for increased loads. Because of our use of cookies, we can deliver a faster services and a more personalized website experience. You have the option of setting your browser to reject cookies. However, doing so will hinder your ability to logon or use member services offered on our website.</p><h3>Privacy Policy Changes</h3>If we decide to change our privacy policy, we will post those changes here so that you will always know what information we gather, how we might use that information, and whether we will disclose it to anyone. If at any time you have questions or concerns about our privacy policy, please feel free to email us at privacy@sugarflame.com sugarflame.com uses reasonable precautions to keep personal information disclosed to us secure. However, we are not responsible for any breach of security. This privacy policy is not intended to, and does not, create any contractual or other legal rights in or on behalf of any party.', '2017-03-28 10:29:48', '2017-03-28 00:00:00', 'editable', 'published'),
(4, 'Terms of Use', 0, 'terms-of-use', '<h1>General Safety</h1><h3>GENERAL SAFETY TIPS</h3><ul><li>When meeting someone for the first time, meet them for lunch or coffee during the day.</li><li>Always let your family or friends know before you meet someone the first time. Let them know where, when, and who you will be meeting.</li><li>Don’t accept personal checks or MoneyGram as a form of allowance. Members may cancel these payments without notice.</li><li>Don’t disclose your personal banking information, including account numbers or passwords.</li></ul><h3>TAKE ACTION</h3><ul><li>Report members who request sexual offers in exchange for money.</li><li>Report members who identify themselves as escorts or prostitutes since this is strictly against our <a href="https://www.sugarflame.com/terms">terms of use</a>.</li><li>Report any member guilty of online harassment.</li><li>Report anyone who asks for money upfront and before meeting in person. This includes, but is not limited to sob stories asking for money, and individuals needing to pay their cell phone bills.</li><li>Report members who ask for your address so they can send you gifts.</li><li>Report members who ask for your bank account details, including account numbers or passwords.</li></ul><p><br><br></p><h1>Privacy</h1><h3>PROTECTING YOUR PRIVACY</h3><ul><li>Never place your personal contact information on your profile such as your full name, phone number, email address, or address.</li><li>Use a temporary <a href="https://hushed.com/">Hushed</a> disposable phone number or a Google Voice number for added privacy, only give your main number to people you trust.</li><li>Don’t place links to other social networks or websites on your SugarFlame profile, or in your messages to people you just met.</li><li>Don’t use photos you have posted on other social media sites (Facebook, Instagram, etc.) on your SugarFlame profile.</li><li>Do not use the same username on all social networks as it allows unwanted users to find you easily.</li></ul><h3>PREVENTING YOUR ACCOUNT FROM BEING HACKED:</h3><ul><li>Avoid hacking by using complex passwords on your SugarFlame account.</li><li>Check to make sure your primary email account is secure. Anyone with access to your email can retrieve your SugarFlame password.</li><li>Never share your account login and password with anyone.</li></ul><p><br><br></p><h1>Precautions</h1><h3>PROTECTING YOURSELF FROM CRIMINALS:</h3><ul><li>Date background verified members first. Ask for the members facebook and linkedin account. </li><li>SugarFlame does not perform background checks on members, so please proceed with caution.</li><li>Do your own research, and always find out more about someone before you decide to meet them.</li><li>Don’t discuss sex in your SugarFlame profile description or messages you send to members.</li></ul><h3>WARNING ABOUT MINORS:</h3><p>SugarFlame.com is for users who are 18 years of age or older.</p><p>Having an arrangement with a minor is considered illegal. If you suspect a member to be underage, please ask for a photo ID, or report such members to Customer Support, and we will verify their age for you.</p><h3>WARNING ABOUT SEX OPPORTUNISTS:</h3><p>Sex opportunists are Sugar Daddies or Sugar Mommas who promise to give you the world, but their only goal is to have sex with you, and not to form a solid arrangement. Here are some tips to avoid being taken advantage of by a sex opportunist:</p><ul><li>Don’t trust anyone until they have earned your trust, especially when they promise a large allowance.</li><li>Don’t have sex on the first date, get to know the person first.</li><li>Don’t fall for a “test-drive”. You are not a car, and they are not shopping for one. If someone ask for a test drive, they just want sex with you once, and never see you again.</li></ul><h3>HERE ARE SOME COMMON TRICKS USED BY SEX OPPORTUNISTS:</h3><ul><li>Sugar Daddy promises gifts of hefty allowances, spends the night with the SB and disappears after (by giving all sorts of excuses) without ever giving any allowances.  Excuses may include:<ul><li>I have an emergency, I need to go.</li><li>I have a business meeting, I need to leave.</li><li>I don’t have cash, can I send you the allowance later?</li><li>Sudden attack of guilt, says he feels guilty he cheated on his wife, then chases you out the door.</li></ul></li></ul><h3>WARNING ABOUT FINANCIAL OPPORTUNISTS:</h3><p>Financial opportunists are attractive women or men who are only after your money. Their goal is to scam you, and they have no intention of ever having a relationship with you. Here are some tips to avoid being taken advantage of by a financial opportunist:</p><ul><li>Never ever send money to anyone you have not met in person, regardless of the reason, and there will be many.</li><li>If someone says she is currently in Nigeria, Ghana or Philippines and asks for money, proceed with caution. Verify all information with extreme scrutiny.</li></ul><p>Here are some common tricks used by financial opportunists. She/He asks for money for various reasons, and if you send the money you will never hear from her again:</p><ul><li>I need money to prove you are a real Sugar Daddy/Momma and are ready to spoil me.</li><li>I need money for a spa day to look gorgeous when I meet you.</li><li>I need money for travel expenses.</li><li>I need money for an emergency surgery for myself or a family member etc.</li><li>I need money because my phone is broken/stolen, or I don’t have minutes, but I really want to call you.</li></ul><p><br><br></p><p>Disclaimer: An arrangement is not an escort service. SugarFlame in no way, shape or form supports escorts or prostitutes using our website for personal gain. Profiles suspect of this usage will be addressed by the SugarFlame Misconduct Team and banned from our website.</p>', '2017-03-28 10:31:22', '2017-03-28 00:00:00', 'editable', 'published');

-- --------------------------------------------------------

--
-- Table structure for table `page_category`
--

CREATE TABLE `page_category` (
  `pct_id` int(10) UNSIGNED NOT NULL,
  `pct_name` varchar(50) NOT NULL,
  `pct_slug` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `pos_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `pos_content` text NOT NULL,
  `pos_status` enum('active','archived') NOT NULL,
  `pos_date_created` datetime NOT NULL,
  `pos_date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `post_like`
--

CREATE TABLE `post_like` (
  `pol_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `pos_id` int(11) NOT NULL,
  `pol_status` enum('active','inactive') NOT NULL,
  `pol_date_created` datetime NOT NULL,
  `pol_date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `preference`
--

CREATE TABLE `preference` (
  `prf_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `que_id` int(11) DEFAULT NULL,
  `prf_content` text,
  `prf_date_created` datetime NOT NULL,
  `prf_date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `privacy`
--

CREATE TABLE `privacy` (
  `pri_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `pri_comment` enum('yes','no') NOT NULL DEFAULT 'yes',
  `pri_profile` enum('yes','no') NOT NULL DEFAULT 'no',
  `pri_date_created` datetime NOT NULL,
  `pri_date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `que_id` int(11) NOT NULL,
  `que_question` text,
  `que_slug` text NOT NULL,
  `que_characters` int(11) NOT NULL,
  `que_type` enum('impression','about me','looking for','extra') NOT NULL DEFAULT 'extra',
  `que_category` enum('flame','sugar','both') NOT NULL,
  `que_status` enum('published','draft') NOT NULL DEFAULT 'draft',
  `que_date_created` datetime DEFAULT NULL,
  `que_date_modified` datetime DEFAULT NULL,
  `que_created_by` int(11) DEFAULT NULL,
  `que_modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`que_id`, `que_question`, `que_slug`, `que_characters`, `que_type`, `que_category`, `que_status`, `que_date_created`, `que_date_modified`, `que_created_by`, `que_modified_by`) VALUES
(1, 'Say something catchy to make a good first impression!', 'say-something-catchy-to-make-a-good-first-impression-', 20, 'impression', 'both', 'published', '2017-02-16 07:29:05', '2017-02-16 07:35:55', 1, 1),
(2, 'Describe yourself to your SugarBaby - Your likes, dislikes, etc', 'describe-yourself-to-your-sugarbaby---your-likes-dislikes-etc', 50, 'about me', 'sugar', 'published', '2017-02-16 07:36:33', '2017-02-16 15:36:33', 1, NULL),
(3, 'Describe yourself to your Sugar MAN/WOMAN - Your likes, dislikes, etc', 'describe-yourself-to-your-sugar-manwoman---your-likes-dislikes-etc', 50, 'about me', 'flame', 'published', '2017-02-16 07:37:27', NULL, 1, NULL),
(4, 'Define what is your ideal relationship', 'define-what-is-your-ideal-relationship', 50, 'looking for', 'both', 'published', '2017-02-16 07:37:55', NULL, 1, NULL),
(5, 'Do you believe in the supernatural?', 'do-you-believe-in-the-supernatural', 50, 'extra', 'both', 'published', '2017-02-16 07:56:59', '2017-02-16 07:56:59', 1, 1),
(6, 'What would you say is the ideal vacation?', '', 50, 'extra', 'both', 'published', '2017-03-09 00:00:00', NULL, NULL, NULL),
(7, 'What would be harder for you, to tell someone you love them or that you do not love them back?', '', 50, 'extra', 'both', 'published', '2017-03-09 00:00:00', NULL, NULL, NULL),
(8, 'What scares you the most and why?', '', 0, 'extra', '', 'draft', '2017-03-09 00:00:00', NULL, NULL, NULL),
(9, 'What do you do in your free time?', '', 0, 'extra', 'both', 'published', '2017-03-09 00:00:00', NULL, NULL, NULL),
(10, 'Which is your favorite part of the human body and why?', '', 0, 'extra', 'both', 'published', '2017-03-09 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `reg_id` int(11) NOT NULL,
  `cnt_id` int(11) NOT NULL,
  `reg_name` varchar(100) NOT NULL,
  `reg_status` enum('active','archived') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

CREATE TABLE `search` (
  `sch_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `sch_name` varchar(100) NOT NULL,
  `sch_content` text NOT NULL,
  `sch_status` enum('active','archived') NOT NULL,
  `sch_date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('000c8407b21a6b201fbf81331257fdd4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802325, ''),
('001dacb1c01730e046ac88f837a18bf6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803334, ''),
('001febf3bb11a5af8b8d6cdc85208fdf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801693, ''),
('002dee418c739a39e306c15aa5f3c2a9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800907, ''),
('00638b186211312c42ee1df2ec75224c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799444, ''),
('006616fe881bb2970cea016537432929', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799945, ''),
('00730dd823ccb2c2ca6d4e43b4830db7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802493, ''),
('00760c51503ec575bce845f091b7cb43', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803167, ''),
('007f2910ad0d5b31d8a0d28deb72ee2a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799214, ''),
('00861a00aacc2beb12f4beaee1117dfb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800120, ''),
('00a72e690e91fa8d61756892ad693529', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803169, ''),
('00d4c67c78a5c4537e675213ef5471ce', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799531, ''),
('00e9c85f0488b357defb4c7d2e046c8c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802055, ''),
('012385d3b857ae5449163bbe3c2c621a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800913, ''),
('0167a3c14e54373c7aeecdfcaca3e6db', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801899, ''),
('01e206e81266270f85b54ed30993b73b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800130, ''),
('01e52b4167db4a44900eca544912068a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801989, ''),
('01ec69e078304448ceefd1b2b00794d0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799620, ''),
('021400a72d738466322c4b28cfcbde81', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802436, ''),
('023124b6fc79821475da525c73597df7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802721, ''),
('023d0836404f48f40bd3d3d22cac0e65', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799553, ''),
('025163c7049deb88a4c109606d5bcdf1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802595, ''),
('0260afa073c66b187b64b74fde69e43a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802430, ''),
('02890d9fe67b4e448082a5dec1445bff', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800785, ''),
('033a50b336702c7d3029075260863330', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800827, ''),
('0350799ef079fbe2486313c38e8fa3d9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801261, ''),
('038d66489ba16390bbf68ff96b874cae', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800863, ''),
('03adaca1622cdb251dd9a7b6c7da6075', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801367, ''),
('03b6ea0573e8c070602ae099c5567bda', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802822, ''),
('03c0de5cab3a65bfcbb78a69cacd25ee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802311, ''),
('03c275546788f9571d937da8a8e0173d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803012, ''),
('03caa1215c65c44ffe7ff4c19dc3c002', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801111, ''),
('03dcada27eb0718b5a98b920b9aaaa74', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800084, ''),
('03e058e29cc4c54d2a0e47b6e8054027', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802795, ''),
('041c103d8ae1fca501f8a4cb1077656c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799765, ''),
('043830c1f6798f1bfe96b18ce5918390', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801121, ''),
('0484d7cc3e47fff14bb4fddcd7038d3e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799959, ''),
('048e3e7c5cd52516b6a30aea75dbbd91', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799919, ''),
('04b8a45b8b3942cf82271eb8162d470f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801237, ''),
('051f99f0e255cf2a48f7608a7f2802a2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802007, ''),
('05504efc4f75945bdb8c22f2ec1a3c2d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802142, ''),
('0566f4841c4b8bee871a2fa9bb9aef5e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799641, ''),
('056da6fb82541ec47cd8779cbf36d39d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799211, ''),
('058d290dd958de063f0e01b373b94544', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801639, ''),
('05cf6b75406a515b469e6c0be32fe9ee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800127, ''),
('05d0df28654ebe640f61b49d6e17ddf9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801153, ''),
('05ef8e347fb8d1973255d5c77a85b1f2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800961, ''),
('0616cb1f68cdbbc2679bc50b7593314c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800147, ''),
('064ac9d43ce28f75497b779ae8c85736', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802889, ''),
('065a0cc1784f9ab0208ca66f939a5a26', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800539, ''),
('065bd1328186742ca75523b148bd7a05', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802918, ''),
('0675811ae5086c6d330b8dc5bdfd2fab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800877, ''),
('0677e8925ea083c6d3f07a345c1712cc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800173, ''),
('067884838aa3ab3e8c6653dd541d32a4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799535, ''),
('0693fdc8e5f085f84bd4dc56e648b923', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802545, ''),
('06c35a14ede360bb727ab8e20a450935', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799169, ''),
('06e626bb32fae4036883e1757fe0c5f4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800917, ''),
('0709489046ba514df93d837a34e01fa8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799507, ''),
('07216ed485946c10fcd40b0823da03df', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800235, ''),
('074ad1fcaca9dcdaf39d2708aa03b03e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800471, ''),
('074e55a0a74aa718c0f9c58b2ca34308', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802464, ''),
('07598044d098b613829180ad3c13319f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799664, ''),
('0766c18a6a898af5e1b21f5ee79b4bf6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800181, ''),
('07792796fa0bae69d11589724a3243d6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801903, ''),
('0781ddf83a8310cf18382cbda60b2969', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802957, ''),
('07d5db1705129c3803820052cd90fab1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800078, ''),
('07f46ccec1edb267937c10313d905510', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799436, ''),
('07fad30e7442e4326221c78d3f77ad72', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803037, ''),
('081885472f4cbb2081cdbfcd2d001642', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802349, ''),
('081ac96f245bc17124881870a6852f08', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802564, ''),
('0835aeaf0c0d9b3c5279950414984412', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800323, ''),
('08652974c394321bf9e69a66f4e419bf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802429, ''),
('0868b9497bf5540f653bb98e1ee64c80', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800991, ''),
('086ed5823c300b2ec2cbae1cd648df40', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802373, ''),
('090f905b97468ad2c273a4d686e3e231', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800737, ''),
('092834a0fa476b7105f85c2422265dbf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800003, ''),
('0953953b6086cee1e070ca6be5d46fe0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803288, ''),
('09822ad2254351f2d468a908d12519b6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800641, ''),
('09987e60a23e462df0229cf1e948b955', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799958, ''),
('09a3866a0464db820e0f1199291415f7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802961, ''),
('09f6db69cf70f89288e82eaf8eb99586', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800601, ''),
('0a05ada64d82055d4bd314f418998229', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803205, ''),
('0a3565fbd77a5fd5bbd92a6e121ea9dd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800993, ''),
('0a4dc47f2570e0b3fe28e4d8bbb10c15', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803230, ''),
('0a71cc6b16864e269a740abdbcfb6f3e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803173, ''),
('0a74d9d0bf02999e69003d224b4d0810', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801091, ''),
('0a849c38a8a13cd0bbfedc700ba97895', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801503, ''),
('0aa511244b994477cf4fffd542a91be6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799616, ''),
('0ae5300de05d93a5bc7be54605dd0e5f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799696, ''),
('0ae6aeb6a2b5f55b7614cb851d075f2c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799777, ''),
('0b4f70e9ff6e4c1320237aa52f856c6b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800193, ''),
('0b68ae19d09cb92c834d92bc0e97824d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801081, ''),
('0b98462f1a1a72a132fa7ff01b20b8f5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799964, ''),
('0b98c20bb3deb6ec9e6b7aff93672fcd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799616, ''),
('0ba75fefb861e94fa8747be7694df7c9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800521, ''),
('0bd829ed08dcb1c3d3f0a62004f2d7b0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801225, ''),
('0c0ebbc207ccf7f442481bebeaa2b8d2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800967, ''),
('0c24d34681fd4a805b41d3a66d822a79', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799781, ''),
('0c265bd1c14e682ee841f8097549b389', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801227, ''),
('0c725050aa4d17e2d32b565ddbdfdc68', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801839, ''),
('0ca39afc2bbc4031b18a5eda2b8b1fdf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800151, ''),
('0d386babc04d23d0a4436e39ec9e44e4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802815, ''),
('0d4461ad6ae50665ade218cfd2c81cd7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799957, ''),
('0d69696942d396731d8eaa7a1c0fc336', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799759, ''),
('0d8fc47f7212631aee8c42bc271efe0d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801939, ''),
('0d94f7790abf7fe8766308fe7eea3f42', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802679, ''),
('0d9c90822df0b1524da22d67a463814d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803296, ''),
('0ddcc78b985662d3a05015ad363731d1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800397, ''),
('0dead8691701b01d18fbf17932129601', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801053, ''),
('0e08bf375f0a0e43e9f813d7405b4971', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799898, ''),
('0e0bdd9509ef5f4f2c8036204ebfb674', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800011, ''),
('0e222028fd57002f541d0698c69ae845', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800225, ''),
('0e2ea4e6a70bfbb99d10f2a691d57b46', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802391, ''),
('0e5615ee35cf673d446cab5ff72419e8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801937, ''),
('0e6d693029017aa8ff671ec7e5fdc9ef', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800487, ''),
('0e87f6f26a83aa5a266ef3b90410110c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801997, ''),
('0e90ffaf5a86a1c330e965b22559969a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800903, ''),
('0ea0b8ae0bfb78b967b2ff12be487837', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800679, ''),
('0ebfc444e64832a053b2d18bebe97bc8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802505, ''),
('0ee04b2f09ea248c4b3ec4fdb69ab492', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802347, ''),
('0ef73607d642ff5b35c3b7092d1c71b5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799785, ''),
('0ef8844637cfffb123d34d0ec49ad476', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802819, ''),
('0efd27d8970d030cde0d51b861d4fc20', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802029, ''),
('0efdd547c6d34f0541ef7d0fb86dd2c3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801523, ''),
('0f0834ba1077ee3e26eb1044f17d4699', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491798965, ''),
('0f1076bc4aaae02efb0e75afba9dace0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803011, ''),
('0f33dcca830cbb1dd2cce7f69ba0709b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801441, ''),
('0f3f7c13daa3b07f8fe5cc1c1b8e78ce', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800651, ''),
('0f9ee2b67d91984cae5e4184559dea28', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802359, ''),
('0fb58314bae22f2c61df5be72fd91cb0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799837, ''),
('10087d2bfd0aeb38ed2a9cebc41ebd96', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801451, ''),
('100a7687ab82397278a50cb900e62093', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801313, ''),
('1024d0d8c5ced5e0d5499601927c98d6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802303, ''),
('1041082f2b57d907132a8ec89a823a16', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801511, ''),
('104cb8d62b02ee6eca9750a6c65c074c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801917, ''),
('104ffc59c9572de2bc1250f069e5fce7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802692, ''),
('1054b86f46922a777598993a00024bd3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802828, ''),
('105f63d78ebd013da42aa17d50aa23ce', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799744, ''),
('1066028d417324733b087e50b4b7a870', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803159, ''),
('106a71bcbb7fe2e7a34627d1446b2bc0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800015, ''),
('10a417490d9f03e97fd4d00ef1580e11', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799688, ''),
('10b4b2237994d1ab025507068af286de', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799729, ''),
('10e8e16118780be22532110abacd4954', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802781, ''),
('10eca0989a2f014a72e9f620649aa8cc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801275, ''),
('110746787e8db363eeedfaf864b5a94d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802147, ''),
('113220e5e8d1d46d5a286e590ca226bd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802001, ''),
('113381a98d954fababb41f6ad045c1a0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800813, ''),
('1167afb4900f1536d02322a7a1e567e5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802455, ''),
('117d24171e73a2726d6f978edf44d9f8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802562, ''),
('118071aba63475bc13604e87d54de122', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800755, ''),
('11c6bab812ddf10ee3dc92a321fa5d1c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800955, ''),
('11cf20075c2a3334a73ca777f75579d7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799177, ''),
('11d97aa6b11cacfe3ed8220550ea2e9e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799983, ''),
('11ef665f3f33c1cf69b52b8a2c968cfe', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800265, ''),
('11fc0c51032e10e1746abceedf3d7dbf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799976, ''),
('122c6f921e5780e3813cb18643b86831', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801471, ''),
('1255d3d6233a31bbfb3dfbf34f8f137a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800173, ''),
('12703b367bc62ed5259a619d45d65421', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799860, ''),
('12c2ced3915575fc39ea54fc1c175292', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802949, ''),
('12d4176ca19269175c240f6a707b8760', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799131, ''),
('12d84919442a7df8a1c10639c39d28aa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799687, ''),
('1307b0ce3e472f72d6e2f3ebb0340a10', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802579, ''),
('139d780888c6a3cf7490d80acd2c4cb6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801943, ''),
('13cbfd22237efa34d9539a296bb2a87b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801867, ''),
('140838e2173b83e610e4f9c0cf5f817d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799958, ''),
('141bce44dbf8265f26d8c52bc0cf1d27', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802857, ''),
('14399b86dc7ac910d900df28481dd2a0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799856, ''),
('14445f9d4673ca7cd7db94c5559acdf9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800551, ''),
('146acc06b0cbd711578ee50e3e32a71b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800443, ''),
('146f72d08121ff4ef1a026885d5b1819', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800046, ''),
('14736129ef0522130cb19bf3712bfe4e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800523, ''),
('148c7f8b5e4e3b790369f98ce7c99cd3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802365, ''),
('149b8da470b101c1fa325e798aca5837', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800355, ''),
('14b953886176ada70b90c261ee3b40e5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799510, ''),
('14bded148707a21b2bd46d578ef7f5c5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803085, ''),
('14e3290b42fbb5fcb1f3bce02c17b99f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803143, ''),
('14e86e03e4ba956cfdd202ae94971227', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802444, ''),
('15010d721431f28e15991bf3a6146200', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802525, ''),
('15311ba76c257f7bf3507e675992ecab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803127, ''),
('153667afdc4993debcf2c2bc9b1ec568', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802105, ''),
('15485d269a9a7f895e49d334e50ea28a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802414, ''),
('1551cf8a037f4c567e9befa1fff914f4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802745, ''),
('15ba5149c83c68480030a990a17e58c5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800155, ''),
('15bc7ebb72cddfcd7b920680e02ad56d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799958, ''),
('15d9d13d6f1c0bed14e55b678ff037a4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801423, ''),
('15dc78c458a07dfe8c96e5dbb8065e42', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800973, ''),
('15ef1242960887b0c2fbd33fa10c4f19', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799956, ''),
('16063d01f2e976d7bd53ff408c063ade', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801341, ''),
('1612d7de753e29c8eae9b2cbadce569e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801487, ''),
('1634a184741aa9c3239b39b24e7d2fe4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802019, ''),
('163b54d65bbf309e784e91c6109976ae', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800763, ''),
('167ae32c2506dcbd9f46e13854c32193', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803165, ''),
('168c23d4b4c8dd9485cdc8a37fb97652', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803067, ''),
('16d15be622bd62aa730a1a49d54c1b47', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800174, ''),
('16e822b134f93822f75a9142491fdff0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801531, ''),
('16f5c76e667d657ed385074199e2291a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801739, ''),
('177d58abb07f48add4ff3c2c49ccb4d3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802275, ''),
('178fa486dea7ea01b0f80b21de51d7b4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802489, ''),
('17c271af0f3139d385d441e108b026e4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802319, ''),
('17f15064369316300a62ab8552145bc8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801079, ''),
('17f90d629e73be4d36a476a9100a1677', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802709, ''),
('180d1d02175f3c1de8ad0393b157c8b8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802541, ''),
('182b68adfc652f14e065a49f4415b4bf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800579, ''),
('1831c462f596c4505ba91c6f9319bd1a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800787, ''),
('18391102008692d1c1a3fb7f1ea1b5dd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800349, ''),
('1860480dfe58977479de01b79e38933f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801525, ''),
('1883a3333001ebbd1aef56c70e90e5c5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803101, ''),
('189b9563bda947225ce36811d156ec3c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801555, ''),
('18a50389eeea52d951dc8ae4ed2b00ec', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801063, ''),
('18c228754aabdef66cd17d66a0c1796b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802851, ''),
('1908830c49394f1cc61ed617c3d33d42', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801836, ''),
('190ea024c0b9152eb8eb3acd0ccee6a4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802799, ''),
('191638fa389309bf981d59d79e966c42', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802990, ''),
('19214c2fe41179966ad2f8db7eb44e88', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802463, ''),
('192ed0806654cd2043852a0d1acb945c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803284, ''),
('198237fc447d26f45719ee2f6ce8a222', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801305, ''),
('198f8cd9359da3c2d44fde3c097eb720', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800134, ''),
('1995bd1f4612a1e1ba264ebe37cb9419', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800983, ''),
('19962ce2a31be344eb786488e32bd6d3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803199, ''),
('19a650864797698dbb7352b0bbcf9fd1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801033, ''),
('19bea68d49a5e929a80463172288e1a4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802656, ''),
('19f7aa860a14b22354df39035a7572c6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802021, ''),
('1a06b0a20c137d2c379dd8ae3a5db29e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801955, ''),
('1a266b2230ca9242a2a59dafacb18fb9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799660, ''),
('1a76515935ea91c0875f54b73b55ee9f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803075, ''),
('1a8ae18afe0d6636977b843a26eaa490', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803137, ''),
('1ad19577c75fb377841e472f84875b73', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801207, ''),
('1ad2a2b9d813d5ae5103e01dbffebdcc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799715, ''),
('1ade77b62243d5e7cb88c988ce045a5b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800175, ''),
('1ae4a7b106db3721e716328691772ab5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799666, ''),
('1ae7822eb970861c6f9b817d2ed07ab1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803041, ''),
('1b0be117913f4f1e2ef884c844128cf0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800825, ''),
('1b2531dbc8fb03195add1f3b43f5da9f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800781, ''),
('1b258334cfa0109b36a6e01dda09427d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800577, ''),
('1b324dfb23c11ed7f5317b96e81cf5a6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800050, ''),
('1bbb0fb93449a304098d2e21daaa0961', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803201, ''),
('1be38f3f632a42d596943f42ebc7e61c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803099, ''),
('1beba5bd9d9f39b01f4f462f2642d23f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799915, ''),
('1c103b8d78a547d7d7259caea5b6f417', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801015, ''),
('1c15c2502db4c891b56643289287562d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803258, ''),
('1c1f7c02d87a57b2262734a413567f10', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802824, ''),
('1c24a6aa3be8325fb2bef92c0c6ed510', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803053, ''),
('1c54cbbb6ea7c91997aa26349316a377', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801333, ''),
('1cae721b688af14e2c0e26c2f81ca6c9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803005, ''),
('1cb806343decadeca2976ab8e21fbd6e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801157, ''),
('1cc9cffbc0e3f05e636723ca777b4151', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802929, ''),
('1d21077117e7d2ce32ac390ffafc6195', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799512, ''),
('1d21261c7a304883a87a43f8c7006163', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800533, ''),
('1d5ecb1b96686ec594061aea34881f4a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800699, ''),
('1d750e4cf72b56987492c892dd20b4dc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801109, ''),
('1d8e760299e249b2747814430a0d4454', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799621, ''),
('1dd2ea7137889c2123e54f08e1e2e2c3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803322, ''),
('1df687f168e8601d8e2bedddd571fcc4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801661, ''),
('1e3f6e93937b5a5a925d6a5adb68fa54', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801921, ''),
('1e85733b046afb59200c2166b61913b7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802295, ''),
('1ec8f7debac7b40b0ffb1bcbc504989d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801161, ''),
('1ed3b1411d12e75a2e3df72e6f4810f0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801663, ''),
('1eecafd9f625be4988bbdcf5db55862b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801703, ''),
('1efc38195f9317a7fe8dac265187594d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801824, ''),
('1fc1fec0656f300eb3c64c144d1769bd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802979, ''),
('1fc8c2c3dc10a4c80b6e1e57461af1c7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802655, ''),
('1ff7aa62c8d7fbd373423ee3f33b1ee5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800143, ''),
('1ff8f4298795ddaeb3a4432d77a56a09', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800451, ''),
('2030dd016b68bc64598fe5f74096a9fa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802389, ''),
('2050b4c02c71ca11e99bad93f062b9eb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799267, ''),
('2076c5b1149d8ccf95f9fa336bb71805', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802593, ''),
('207ca4ed12d4c3778010c35c9f2f551b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803057, ''),
('2085479594e2d2443027132bae144c0b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799929, ''),
('20885b60ec69ab4a584f460a403ce435', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801557, ''),
('20a20f293a9a87e4ea000d4bd2df9083', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799674, ''),
('20b3015cf9314da54242a78c2523b4a8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799761, ''),
('20c8f0705a78b1738ef686e5b381a157', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799536, ''),
('20d9c4577d6b5625cbdc8a3a5e6497c4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800777, ''),
('20f9fb88be7f78297965aad165a48a82', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800779, ''),
('21085c1d2223fd0b3a3b2fe5fe3d5d9f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799636, ''),
('211f260ad4cc2247c2776176f0c901d3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801499, ''),
('212739a9e7de724ec3030bb71dd72b69', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801037, ''),
('2162c635f37b180b525aa29b3a95bc36', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799747, ''),
('2170aa9a8a3e55aba73f1fb47a2d360d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801017, ''),
('21c5150429d33dd6b5268c1a9242afa6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800187, ''),
('21d6423e1010775408aae331e98d5438', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803151, ''),
('21dd8dfd4a213baf313f2033a843597e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801575, ''),
('21eb1a37eaee44d4207377da8c0182a0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802929, ''),
('220c6dbaf266152007f92bf8ff0ba2f2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802993, ''),
('2214880fae5a6cfbd66ec8f84eaf26aa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801231, ''),
('22699b226ef1272d5ab0bf8659be331f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800985, ''),
('22bae3c9f634b77ab6ad296d3b3933bd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799987, ''),
('22de4c29b99e31f7c63681671303a876', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799523, ''),
('22eaa64460870a3fbc26c726dbc83f99', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799961, ''),
('23081af43c25cc393f2c38975afb5e1d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802775, ''),
('233b84cb3283bff505a5264eeb6c17e4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802287, ''),
('234f2c82d5567e25b0f161ff42c796b7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803286, ''),
('236d468adbe0a01dd98ab0f048249c49', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800963, ''),
('2385390b914f7e1b4cd1671056bc4e1b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803009, ''),
('23872912bad83966ff5cce4cac28c3d9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802369, ''),
('239139f4d7e6bce3a78937f09bf187fd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799557, ''),
('23a180059ddcb78e489a01c1333827a3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800361, ''),
('23cf3fb17579dd3247994fa4053b2920', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801251, ''),
('23d8de638ec254cf55f1fa9dbde73eac', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801413, ''),
('23e3309793f8aee82652e9fd5428e430', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802140, ''),
('2422289005f332fb79bf1aec93b54d2b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800493, ''),
('242735ca69aaf9b518988f4a67c865ee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800387, ''),
('2444ff58a94d0130f12d81a71108bfab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801375, ''),
('2456a678838afb0cd58cdcd32c3b3f2a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799165, ''),
('2462408c3b31ae5c78f4c084bddd6cf8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800163, ''),
('246e389454ba72b2cc3b81fc37c8ba3e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802841, ''),
('248c209756a39f4e7e582a4ee5012873', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803119, ''),
('24ab47016abf2b5d1e96c4384855abdb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801087, ''),
('24be61766ec6da9970f8084bc2eb5291', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800695, ''),
('24d580043b1951d0145d707c465b168d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800743, ''),
('24da098d339202dccda25fadcb0455bf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801515, ''),
('2506533f266d8fc6200ef8af9b1b8a0b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800937, ''),
('2567dedc38cc35b24478c77e2f56ba82', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800254, ''),
('259f8c713cfa6b143ada2537af3b4dd1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802327, ''),
('25c01720d9a7bbd072ce7b3ca8aaa2a1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491798734, ''),
('25e13892a96ec5ddccede93c97609fd3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801631, ''),
('25ede5745ac4f66079db8c3f32baa3a2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802959, ''),
('2607f305dbd8664dc2c5c065d91ebaa5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800399, ''),
('26371bda0c362960795f4f57b0fa8240', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801729, ''),
('266a85468cd2c88c529d25963609cf63', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799528, ''),
('2671dc1239f9b28a83ea18ba70484cf7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800173, ''),
('2677a98e4266de5d2d39c8943c24c0ed', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800198, ''),
('268086792fabf4d48a6dbc44867efc5f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800589, ''),
('269fd616e0e7b20c74dde500dd98795e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801679, ''),
('2707e338373dd7b4ca9ae0e3a060b392', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799989, ''),
('27107636fd02268a0de67cc9e34e5bcd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803195, ''),
('272cfdd26e47a5f20264f817d8e73106', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803215, ''),
('2736274cc92ebef8948dd0a679804f95', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800987, ''),
('2739eebb31a1e6c11121e3c704bc0a03', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799971, ''),
('275d8e62172d29a6b518989e0f7cae69', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800947, ''),
('27c894f68560497404f608b551d6aecf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801871, ''),
('27d2fa9bae84fe8fa51e3110aef2dbd4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799567, ''),
('281b1e1c677c456368d25d457916f306', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802975, ''),
('2880cb63ceb351f196f51fb7baa3cded', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800979, ''),
('288417170a328798aa19c3bc7ce71773', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799220, ''),
('288cedf9836c3884bdc3f25449023675', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801637, ''),
('28b4abfa340055300db6228842443534', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800915, ''),
('28e49a96089dc71596d892d187b3e0ea', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802791, ''),
('28fea0f8006eed0fd150b33a7f578403', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799193, ''),
('2904a6f8fb6323d360058b95eda9d785', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801455, ''),
('294ec0d2d4a9550d698790142264dd33', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801381, ''),
('2954fa566a95d178d84303e2c7da927f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803227, ''),
('295ce659009e505f34af51544feea934', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799704, ''),
('297dbd9d0ddc3325c9ef1dfb36dfe4d8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803141, ''),
('299169182df9ad48850e2c2d4c545eee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800727, ''),
('299bc23433e22bbce908e1c25a4b8a9d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801479, ''),
('29a1a74c56d3c7e57b38247e860669fb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799494, ''),
('29a6324b0e9254aebdd2a6a6c7a39e3f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801337, ''),
('2a06f253eaa30f4164dfb0a0a6931641', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801285, ''),
('2a341a20d492041cc79b400f837654e6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800891, ''),
('2a97a27c92c6361e340d3360f3bc3f0a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803177, ''),
('2acf9eb996ecd288c381bb2a6d74960b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799488, ''),
('2b024c3ef4d93442090b17239b733d33', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800519, ''),
('2b04bb05650db827b712803112694f63', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800793, ''),
('2b0517f041e3e3788300269cd43caec9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802839, ''),
('2b22390dfebbc8146f28be82225913de', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802414, ''),
('2b6248d9995dc8f96971f0de6db5e88a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799506, ''),
('2b6998534a48d3333404938b1be96a7a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801657, ''),
('2bbc7e2340de4c95bb1565ef43231eac', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801713, ''),
('2bc1580863f3f58b8c073fae409c5ea6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801349, ''),
('2c61faa441803920cee3dac45e7719ed', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801719, ''),
('2c9131f1d80e155227405121216e9c64', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803274, ''),
('2cb0c6759dee8cca0fa059f8c4b34d2a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802426, ''),
('2cb593f73f524ceb2f81199d4905ba39', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802509, ''),
('2ce44886d542a70b3aaf84a78a155bc0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799616, ''),
('2cfec32ab7b7163c797f7d329f1fccb2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802606, ''),
('2d2db7e0b6e38ec2f57240d818588427', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801415, ''),
('2d389ef253fe232b1b1f63614b301ea3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800273, ''),
('2d4ab1c5b0bbe4bd45de705d4414350a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801983, ''),
('2d6e9b845a133d40f7c8b980d5824a1a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799540, ''),
('2d7a3a1578d7cf7b6c16cc821c75bd1a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802237, ''),
('2d8a8f2a14f5691188e1c6ff5586dfa1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800805, ''),
('2d8ee723f12d230383e93b3ac9e527ec', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799553, ''),
('2d9847b9a66725906e14e6b9b69e979c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799585, ''),
('2dce7c8b56436f14927deddc8ffad8df', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801585, ''),
('2ddca38a16ca076dccda68c4acc6c9b1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803308, ''),
('2e189918e61fc5350a3de17aee2df2b3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802988, ''),
('2e334c39d02a002319bf1a4682b37264', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801481, ''),
('2e573136c670b9b24c4ee048fb725a88', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801473, ''),
('2e5aa10b0c6a25e0ad52d8fc61478fd2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799715, ''),
('2e5b872076355037d26409eeec2c5317', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802089, ''),
('2e68cefd383b073b1579ab80b3bb21a7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800517, ''),
('2e8f9c618da8948d2ca3c41c4b5aa3c4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802615, ''),
('2e955566ce2e60ef9790af51a322e623', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800053, ''),
('2e9d5d4151d50dfdf2459eaab83c4fb1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803193, ''),
('2ec91a4386bd72d85af1227781047656', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801379, ''),
('2ecbcd1a43ec27aa30bda1a4f451ac6d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800975, ''),
('2ed3e5368bccff8c22bd0d6632ed203e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802414, 'a:7:{s:9:"user_data";s:0:"";s:6:"acc_id";s:1:"1";s:12:"acc_username";s:9:"developer";s:13:"acc_last_name";s:9:"Developer";s:14:"acc_first_name";s:9:"ThinkSumo";s:8:"acc_type";s:9:"developer";s:8:"acc_name";s:19:"ThinkSumo Developer";}'),
('2edc9e1ef3a6135e69c2bd2b893f12cc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802115, ''),
('2f0561a804f8db6cf805b1b207a3b1dc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800197, ''),
('2f0e82d80ff85a456fd1bfbfae027c4f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802552, ''),
('2f1df237a01f7cb5f49d65db9cb3cf7e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802560, ''),
('2f52a11bed2d9d17262878d79f7ef0db', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803262, ''),
('2f5b8896fd2f318aabf3c180c1c27173', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800175, ''),
('2f76cc3f6b09beafb1f01ce8b0aadac6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802067, ''),
('2f98066a3099bfb6a142603682537f07', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801425, ''),
('2fb31351f1b8c73d7fdd980fe2049fd9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800313, ''),
('2fbf1b9090bb3116f021874d39a5cce9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799976, ''),
('2fc3c5dfd6baa2a7a6fe72c7f2a64827', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802795, ''),
('2fe09555b8bb65d5985e0385d70694c1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800483, ''),
('2ffd49b63f50e5cd5906701a7209725c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800489, ''),
('301c4d8568bb59c31d4d76a58533e91a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800275, ''),
('302e86f7000b4bdd982443dd292af3f4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803003, ''),
('3038949f25103ba9b6cd99d098a16607', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801209, ''),
('304cf2f2a862dededfb17e38000c548d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800197, ''),
('305d1a2ca428ada5ab2bf8e33dce7d7c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801137, ''),
('306e855f137f81b127664e7a87ef62e8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802381, ''),
('3097f28db9ec1f9918bad1bff8843903', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800927, ''),
('30a37cc9b5386bb3d15ef086436bb845', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800261, ''),
('30dfac83078abffe557ab103e3ad0132', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802383, ''),
('30e2dc5e06ababcfcaed2313f4845048', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801371, ''),
('310fd9e1fc440dd7ca5eb58e8fc963a2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802699, ''),
('314f74d5fe4da522f9979d86ab6c2b8d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800082, ''),
('31821fc9b8094c988a48b203d5a31ee4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801545, ''),
('319933798cb75559c5fc08811adb7b30', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801343, ''),
('31ae3ece017d0df4b446e041a666b8df', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799783, ''),
('31d6106c8c94fd01f9f2fc325610e486', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800169, ''),
('31d7f75b17d46416e83834a720ecf79e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801599, ''),
('3223b842314336274ddabd111a457cec', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801617, ''),
('3235cecc03bbf6ed523b6aad6876462c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802353, ''),
('323753837af5b0c37647e35d4abaebb3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799709, ''),
('323f11952bdc1dbacd7038d44e5b8444', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801907, ''),
('3251b999902c74aa826780a31d874b68', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802135, ''),
('32752fbef9e51e41fd6364f62ea0d32f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803248, '');
INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('3294d558a7e3b3ca4fcb38fb8e5fc10e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802688, ''),
('329594e2016cdd9a9b34fd2f5a74f7a5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801959, ''),
('32be3e5031d15349722150f839d79663', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800391, ''),
('32ff54e396ded987d1517c13774cce72', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801533, ''),
('3356d06e0b4951683a4b37e8e9b4755c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800040, ''),
('33c4073f53ae5d91f0e3f4c7dd1a7044', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800759, ''),
('33e00d5eb0e83da25697a1b15375e6a4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800359, ''),
('3401adb0a244adde6bd91ac30bbd2daa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799621, ''),
('3414f425b905aa8e3b702fff0afbd6bb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799486, ''),
('342e6ad781e88c8707fe9c62d26dab05', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801653, ''),
('3478e0426161113e2502ab5b45180055', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800373, ''),
('34806d4fe57e8971c077593a6bea6991', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802937, ''),
('34a57e9247f8942e14aa3baa14f4b0a5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803098, ''),
('34b5cd171d4c35cd8dd342fd4f8fd65e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802380, ''),
('34cb4ae5301bf6bfd274a2d68c7820ea', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799566, ''),
('34d6c55b88f4469999572dd356525ae4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803012, ''),
('3504b35ff51eb0cdc2fa53bec8dfe7a0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800503, ''),
('353cab466bb87715e0b4494a8baffcec', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803298, ''),
('355c02257a787bac2109432be91efcde', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800653, ''),
('3567245befc1fd0689561bf0787bdbf8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802309, ''),
('35a08b1283431a6f521e4f3bcfcca530', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800643, ''),
('35b6798f4fe6b291331244fc6ca5737d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803230, ''),
('35b9c4b6042cd78a42e6e8508695ba77', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799711, ''),
('35fc365ecd9662a3906225f2823588da', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802464, ''),
('362047b5b45974b5cc88d382ac4c901e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800437, ''),
('365bdae5f9493633af15cbdfea5aa6dc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799956, ''),
('367c65f81bb9efaa8330cc2db6be9049', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801249, ''),
('368eca8f0afc99336682d8344687cd79', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802011, ''),
('36a9d7aef1d8af6ebc2c9dc05b700a9e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803276, ''),
('36dd82098ee58bea937d7459666c57ab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799750, ''),
('3771f547bbc6b85515a6c079900da5a7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802687, ''),
('37a7bdf7da4896d9c6ba21340f12e322', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800257, ''),
('37c44b5702ded9bdc5012a015c9322df', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802551, ''),
('37d6f650d1d98e5af2a40b62182042a2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802649, ''),
('37ebefac73b7a40b2e91e0ecb7931c53', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799540, ''),
('385d7a4eb9ce96679dbb44a9f5297029', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799858, ''),
('38c1ac985ba86586dc04bd88fd2399c4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800167, ''),
('38d275e5735d5b51496296c738e5bd11', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802398, ''),
('391407ce0fbc7d2ad0d76fc3d92f209b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801517, ''),
('392bbb6f7743573467024106f8d48579', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802811, ''),
('392f72c2e0b6342a0798916c16af46f2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802456, ''),
('393d28248f621426fd9180c15ef4b282', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800981, ''),
('39457437418fac6e9b807868f52f5723', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802436, ''),
('395277cfda2d9a27e1ec9e0cd0520d4e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803217, ''),
('396333c148713af75c7d1a87485f4e4c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799140, ''),
('397ffa4c2e374cf7d641d48acf6babf1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799137, ''),
('399a587eb3b99346521d5c59d9a9c4c1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802649, ''),
('39ec110339595b5348cb2cde231c48aa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799739, ''),
('3a087e420dd3835c371ee66919a592e1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800565, ''),
('3a0bad850918ee874e60751b20e3190a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802267, ''),
('3a37c705e557161b043ce3326adcd15c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800581, ''),
('3a4516599c7d0ea1d66f166e3eaaf9b1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801823, ''),
('3a53820622fac45fd8c8caeccf65701d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802737, ''),
('3a53f1def8952d039e15a4dde2501b03', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801077, ''),
('3a818712563fb345dc3ffa3a1b7d8e6b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800303, ''),
('3a825a11eb5430a7a5a5045e74028c12', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800837, ''),
('3aeed539005e235f3ef481090322f118', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799621, ''),
('3af8b22e01a165e59415064cec56729e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799573, ''),
('3b28b82c8795c98ced737e2451b95c98', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799610, ''),
('3b59c97d88dd1ae6ee8ae10e496880a3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802849, ''),
('3b797900f6d181924aec3f4ae62d236d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800585, ''),
('3bcd94d410a5eb1022e68bdaeb952f96', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491798867, ''),
('3bcf113240b2acf0a2829b467cf962ef', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801129, ''),
('3bdd1a0910f3404ebd7ed5812529d71f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801824, ''),
('3c1f7d6f7affbaa2b4f6877caaa5305b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799821, ''),
('3c2b288752ea33bad903a161a4a95179', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801403, ''),
('3c4dfa5d9d61a34eef92c6c6b2eec843', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799907, ''),
('3c539dce65e7f4353bbc3399e07f3133', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802953, ''),
('3c8278f76bc10535ac880a8a56565e63', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799453, ''),
('3c847773078a75ebf2734deac43eacfb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800325, ''),
('3caf035552c5afaa72678cab7547f0ea', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799861, ''),
('3cbac29ec45aafbefedcec4cd9379193', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801019, ''),
('3cf67f33f58f60f8f93d641e83d9d71c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800333, ''),
('3d01c53041b52efa406721c1e1b3060e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803268, ''),
('3d70694e1b1e8216c110cfbebcd9678d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803010, ''),
('3d9369129617fd1cb29f500f5ee461dd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803256, ''),
('3da0847320ed889aab45a58dc84689db', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801253, ''),
('3dae0aa7cd699bbb0245e06180eed712', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799999, ''),
('3df2c2c22bee386121daebb6f9537b98', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802847, ''),
('3e0941e3ea4d750536e5d28bfb68460e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800465, ''),
('3e1cc561f26aed52cdb0b1477f199d11', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800599, ''),
('3e3058760f86740e1ec15482bbc2bd44', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800126, ''),
('3e661bb6a1f9df5188255e8cc9828ccc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799241, ''),
('3e6b95a0994ff20bdb40c63094f13027', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799971, ''),
('3ebd8cf37d11660a4b6f0f34c1369363', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800475, ''),
('3ed733d8de496980436fc84305f1ae85', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803007, ''),
('3ee351a9ba58e1678cbf484197f92f2c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800385, ''),
('3f31901fc91d0d0d0cf70a3e5b1134e5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799503, ''),
('3f36c70655942f4bcdd38f0d1a3156d6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800431, ''),
('3f510eedbbd0cf47f843fb5e5b41cf08', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799153, ''),
('3f87e3b3b70d321e9d6d4ae68963c179', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802013, ''),
('3fc381a7fe8754107d62f90a4c8a9aec', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803338, ''),
('3fd3bfab87a7e79b7d6545865fe381d7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801701, ''),
('3fdef8e1f6896f2f609f5ceb86804359', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801961, ''),
('4001f5e6985d59a386050e68155041da', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803153, ''),
('401a84d6a520864123eb70a5ced869de', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802805, ''),
('404d3f41c79f28fcfe0d8cfef3c0fb14', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801175, ''),
('4053871d4bb9be4161b831ba49349b61', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801051, ''),
('4053eb07704f3e064cde8fac8820fe8d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799538, ''),
('4055f762b42e60541140bf6057a198af', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802073, ''),
('405e153df35d1bdfa30d9a33b548262b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799223, ''),
('407675c77706f90e81ad5545365f27d6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802647, ''),
('407feed6b68baabfe66fcbee186b1ef4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803061, ''),
('40921614f2739e0f82572eb2b04ff21e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802607, ''),
('40e38540189a14a7856bba59d264d99e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800305, ''),
('40ffb796739a47afbb0a987c19775cf2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802893, ''),
('410284bb6f7bcd34577556fbfbe194c9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800185, ''),
('4107ed99fbc874b83dc8a1b6bacc7105', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800889, ''),
('4111b483976e2af43fa89f52c45beec4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800801, ''),
('41379302799cff8e46e750bbb8f237f8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802136, ''),
('41405e8555befe668cb5c5e690892dba', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801835, ''),
('41454e12ea3dd1d9bc58ce8c9e8cf6a1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801549, ''),
('417cdf948f6be3e7de61c250dd69f105', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802444, ''),
('41980b1bf493e7ee1f2e1debc756b2f8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802049, ''),
('41de2e004f02886f5fb0130860faa3e2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799983, ''),
('41e7104dc9133eb901e39cfcb8975b5f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802395, ''),
('41f30a5f7e5461967a93ebd0bc3fa4f9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802833, ''),
('41fa5655f9dc4cd6baf6eaace5c7f531', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800729, ''),
('420a21a16879c1d041a43ca54153c258', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802561, ''),
('421b2398b158febe8482cecfc96df209', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801001, ''),
('423f966ab55ad1b421f35301812debf4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803117, ''),
('425f0bec2df19c315b270534309257d0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801497, ''),
('426633b0fe6d7ca6cf587bcbdc48278b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800929, ''),
('42a170fb4cf6b26148ed861087180e69', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802835, ''),
('42c5d26254a84d939e41b8d33504d3aa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801105, ''),
('42d4b322bb7deb197bfe239103c2f99e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801377, ''),
('42ddedf7d0e38ee91d94f3edf93fad6b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802795, ''),
('432b85ab43a76f873d3f0a134db21471', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801643, ''),
('4342f37b06ccfb37b23d81a9d57964bd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802994, ''),
('43544ceae67559229ad37cf3109f4515', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802146, ''),
('439345f8dfb44fad13dbb92a50482688', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801539, ''),
('43b3f34ae225f2aa2e2dbd638cd61d0e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801919, ''),
('43baea28a53fc9920938c16d29d4ba81', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801195, ''),
('43ea9f87fd8e8fb90f464ba92bbff98a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802633, ''),
('44148e5f6dbe0441180a51ea97553831', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801383, ''),
('44991c3ba7b0bbfd78b067a5a2c7f995', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801815, ''),
('44a6c67068483c5e1cc45ec60609aa05', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801587, ''),
('44b10eb8c3604e94c25df2fc284c436f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799572, ''),
('44bf70f7e9fe3cba4b1433b53484a690', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803149, ''),
('44d6f472be67de5d0db7ade57cd27590', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801293, ''),
('450235b7f53c19fdcb312ee923e61fc8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800311, ''),
('45034a9ddc164f77d25ed1fee1b5d646', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799230, ''),
('451d282287bdac598d1e7241ce1a64e4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799713, ''),
('4522af9e64e9d4d383d441ae32018abe', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801905, ''),
('452661a549119f18556260ab7f8fd610', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801823, ''),
('4527cf69bf54b6e3f2716474fa3647ad', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800569, ''),
('454f7762c2e6e4bded601cc621dc3f76', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801419, ''),
('4551794bccb5fc88dd93c102ad57ca85', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800199, ''),
('4585a463c44e7dbfe62312d86fe60619', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802521, ''),
('45f7a7f96c7a62ef991d11511a329bf5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802809, ''),
('4623b9e26f93049e29e1b8e038299f49', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799605, ''),
('46441e2b36fe1b23622876c050dfb6d7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800711, ''),
('465d96aba719e640b7beb57b26669636', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801695, ''),
('4669dbd29c722ce8c5b81cb29d7e4c13', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799599, ''),
('4684ff450002198e3e505a24ba6144ae', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802885, ''),
('469eb7e4ce2f7ce404fd88f25dda076e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799224, ''),
('46ab98fefc50e7796295c507e94dd530', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800741, ''),
('46b19ad0129214c4464eb861135352a8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802087, ''),
('46c28600b10d170629a63f1a72baf7bf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800051, ''),
('46c997b7adfdbbf54dd692ae943b798a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802351, ''),
('4701aca14e71ee1a73c48626684b134a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802041, ''),
('471d74e78f69195b1181305acfb8274e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801911, ''),
('4792b5e4f66d91b977eab4401018c9b3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802414, ''),
('47df154fe47e1b2422eda427ab8d9319', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802283, ''),
('48102dd40474b2514ce41accd42675bf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799498, ''),
('481dda5f28b1a2331997a49b98af9c24', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800171, ''),
('482a94d0b2c68489495b10f096483ef3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803300, ''),
('483f61ee51864ed62d441ea519a125fc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802973, ''),
('484fe74955ae79f607fffe19530c3eaa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802584, ''),
('485ccfab791f077b9cf4027a0aacffa5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802458, ''),
('48791312b68f4939e34df3c3bc36d6db', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801339, ''),
('487e3156ab716c555d771bcec4f24ced', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800429, ''),
('489432e3f8834ad303c38991164cd202', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799584, ''),
('48adf64adac96d75665b0e389f598875', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799899, ''),
('48b89f40a3d2c13d0a2d4b824d8a7e84', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803326, ''),
('48dd3537a9427419235eace3fa8b5fc0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802121, ''),
('48ff7a9648e91d4b904f0a23cdf40e36', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803009, ''),
('490bc4819ef88de9673bae2108da28a8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800180, ''),
('490cfb7f822591d8e4f83b9fc63a32f5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799725, ''),
('4918c1c264d23326b8d4e4c08ba3bbd5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799717, ''),
('491c25910f136aa0d9edeb3bc24e16ee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800821, ''),
('492506cd173720cabae36cf9e88ee0dd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801623, ''),
('492f57c510dcaba99fdbf36ca5fb9f1a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799603, ''),
('4960eebf6508c04b392cd9046778d91c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802093, ''),
('496d835476678014d3c8a6c0966eef91', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802229, ''),
('497825618c008a9855c99f57379da5af', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800073, ''),
('49844593d719485fa5c14e43d678da14', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800195, ''),
('498b27e7a6b727e668be06df3e26f098', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799716, ''),
('499b1a485361aa18b88299745519d584', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800477, ''),
('49dfaf2c4e8f5581f4959b31def976ba', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803266, ''),
('49e0fccc6648a9df4d491ef21571ce33', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802831, ''),
('49e4fb22800b9f2ff99fceb7d1a81db4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802741, ''),
('4a00958b9efa17a4f9b5dca40e9a5286', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801103, ''),
('4a14e705b9a1741336b8378bb1655544', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799529, ''),
('4a1a607ee18efab7d8b8050c4a8fbcaa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800635, ''),
('4a3ce33e0debde0857939154c1c6c9ec', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799614, ''),
('4a646e52746d633001b823f0a4a3ab8a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800747, ''),
('4a826fc1e89ba5d855cc73e70962de99', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800189, ''),
('4a83f5a473ab6dc1e4fbd496ed8e617f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801327, ''),
('4a90b1c69908865f10485550d8843123', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802425, ''),
('4abd32716c374591396ddcf2a056480f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802807, ''),
('4ac8cf374041c45a479c2edeb359b099', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801979, ''),
('4aed414a04216130fd4d3facf2287fd5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801519, ''),
('4af82034e7c46de2547f7fcb9601fa2a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799799, ''),
('4b0a02f1e0511d372d23001bba7217f4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800141, ''),
('4b1c8ef0f019559037ab4ef10a587e2b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801509, ''),
('4b2daafe83b7b9d80807339875045368', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800339, ''),
('4b3144163d78a3cb66b1e2a9663a9707', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802057, ''),
('4b318533753ab916cb2784879f55581e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800199, ''),
('4b36e1949e345491a7737335c00b8720', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800201, ''),
('4b80163f1d554850f5be6f6f5eac7e7f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803091, ''),
('4b922198b322f5282ed24603b848039d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799676, ''),
('4ba30119836736c3afcec5f58e6600c0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801691, ''),
('4ba57458a2c94adc44e3d7445f6823ea', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803063, ''),
('4bbed7cb58c3f79098b4de439f6a2f60', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799683, ''),
('4bcf11ce0f71e143bd79c4603c5053f0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802597, ''),
('4bd17bae31e7ee4e290ecbdee98d3b8a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800425, ''),
('4bd5a0e0911775e6ec772a79b42b20de', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800911, ''),
('4bee02bce9e8a45b59520760b6d9a22f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802510, ''),
('4bf2e6eb02dccacae980c878b2f8f5bb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802499, ''),
('4c382b2aa20cec5d3710962bfcbc5a7b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801435, ''),
('4c434b7bfd8c23bbd27b8c4916235ec6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800331, ''),
('4c45d9b7acf4678cb87c1e43271f861d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802717, ''),
('4c546839e5d280c9acb9530918f946f7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801975, ''),
('4c5ee70550c92ddfd8d6ba6d6dbf1268', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800417, ''),
('4c8d7ccc4a353fc3ef03aeff4455978c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801117, ''),
('4c8df7d93614646411e7c7ed14dd4f5b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799204, ''),
('4c9ed18b04a615dfacbf78e2917c6bc7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800379, ''),
('4cc43c05f34d068d8408703b4ab93d39', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800629, ''),
('4cec992c738a136b635c25f4b48fee6c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800943, ''),
('4cf862f44cba9f9a49193fb213ea4e4e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801483, ''),
('4d068f2c2660d0c83bdf063dce7864d7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801013, ''),
('4d224f9fb9ccc9dc8861711790f397d8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799866, ''),
('4d3751814327b488c1989d696a2a1b95', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799655, ''),
('4d563cf37b88d90d84dddac0a2061586', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802483, ''),
('4d7609eb092a9f62806d92211de73b45', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799472, ''),
('4d7cfc5346b360c8fcc0a2e16d1cbcf3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800061, ''),
('4d9dd4123e635c991da6c31c2a4dfa53', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800439, ''),
('4da906eb7f5a990f8a93e7c0888c1839', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800769, ''),
('4dc75875406b8851af4046f21abf3cff', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801177, ''),
('4dd238b17fe27f6bc9296534b9bb56bb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800731, ''),
('4df0b551dabe8cc90e58eda2555d98ff', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800931, ''),
('4e2b93f794b1085cc514900e764c6783', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801477, ''),
('4e41ef578655c73f959c9aad1a683a4f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802667, ''),
('4e45f0ad9b436d3be69631525c8842e2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801373, ''),
('4f730524609c4fdd593fefe1ec24a135', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801449, ''),
('4f734f05f837a36cf41049378ca8a81f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800165, ''),
('4fe94ca5c6fdaf84f1f40b4f5511a463', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800059, ''),
('500de54cf065dec7f8f6fa26e2a40b5d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802609, ''),
('501d2f0e11cfc102f1d0c4c5a39a45fd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802477, ''),
('50281c5ede915d2b6a66f588e3dc7325', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803139, ''),
('50352020811817a14d47119f540a9c2d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801879, ''),
('50370945c8a34f88d75df78ddf3c6716', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801041, ''),
('5076d05d99195f92b4e2484a63444d16', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799484, ''),
('509f005d2076820551c1af4c1509bf14', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802604, ''),
('50a91fb33d9aeeb40e7548b2088731b7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799229, ''),
('50e187091d1def2e805a06dc592eeba6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802582, ''),
('50eb159371f16c4268dd9a70b6998d0e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802777, ''),
('50fcca519fb5ba3f0d2bed2266ffd4b7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799836, ''),
('5102fe1373528438b369cafb367b44d2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801165, ''),
('51085339fb46aa1ed3d41089cea03348', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803302, ''),
('510e12db205e627101f930b2ed6f1ae7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801039, ''),
('512270e31d77d037083bd4cacf6cd9c9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801901, ''),
('5129c0e513642f7ce64cac617a10db82', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799187, ''),
('5141818ccbd507e88e32426b185213e5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801131, ''),
('514f371ee8f8059a80f929dd4eeea2d6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800547, ''),
('51525d479ffb46b62f4fba2a08617ae1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799093, ''),
('515622b2618ff67a777848ca19d70bf6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802871, ''),
('5172d7902595c728dd3d619e09812322', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803236, ''),
('51a4e8dfad2c9e25086452ceb58b1cf0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800557, ''),
('51aed796479a72ebeee36ceee94a89a3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800111, ''),
('51caebe87573f6380bd587a3a65c3600', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802444, ''),
('51f1d3a43c457433da852844974003f8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802797, ''),
('52437aa5b16d318dbc16e80d0f994f10', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801345, ''),
('5244caa79a6eda2ccc8bce367e19d5b2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799265, ''),
('52746c365b72d234b88486acecc44636', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800058, ''),
('527c0beedad0dd01b6bc9292575a2902', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801257, ''),
('52d88d93dda9571f7d6bc8c17c0f0c55', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801241, ''),
('538d841086afa6ca18b2ee3b688af910', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799954, ''),
('53931e8c35a853fdf2a8c4675baffafc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799743, ''),
('539b8a500122126877583e4c7b29b273', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799627, ''),
('53a67b3ed8ccb0585617f76fc7a440af', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799462, ''),
('53a754cf32b2a6655145315624864062', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802685, ''),
('53aec18dc57f23d736c6ee3fcfaa3171', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801411, ''),
('53c3a779754f48e389b6bacb904f1dce', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800421, ''),
('53c881e6a68d202fb42bc82146946f29', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799280, ''),
('5428c821cf54a33dbd2fd5219ae815a8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799539, ''),
('542df23d752f2e8668406e7fa66a0b4c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799523, ''),
('5447e2d7c3f0d94b9f956708de8d924a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803232, ''),
('545d329434796a3b22c4df27b0632717', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800615, ''),
('5460e767fc98982c204210fdd2a645d3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799630, ''),
('5491ec6565ba8f68b7e403ca3785bac2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800633, ''),
('54dd2d3d126c1ddcfc3b2f186c37cc48', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802053, ''),
('54e8b27cc11dbb77be99ed541323a40b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799658, ''),
('54f3a9ec6d7fbc84022136ebd57646b9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802071, ''),
('54f6b078a09870f8c154450b0717a4fd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803015, ''),
('55206be222b0b58363acba384afa9939', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799696, ''),
('5538439f2ace8d077904309259948dd0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802277, ''),
('5538fded614b6e04c2a9726e2d81e188', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802101, ''),
('555edd1fe72baf9bc7497747358c8160', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801569, ''),
('55622ecf0e2e68946d1a4f491cbc29b2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799982, ''),
('55886eeb720f9edfa1bff11dc35c5a85', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802500, ''),
('55c9a8cb6be84bbddcd2323aca27239c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802083, ''),
('5613b4843c715c114824ec2fa22d6816', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800021, ''),
('5642eaae9079087369dab103b7f5ac5b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800525, ''),
('565fc94b0e57e434f0416da5582f35df', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803027, ''),
('5663ff4badaffcb017bbc5b20d256547', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802063, ''),
('567ad562717cb4aca572c89b9a7fcd3c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802607, ''),
('5685ab0572fbcdf8cfdc2c7859ee2bb7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800245, ''),
('5696b5581b63c2ad6a8227ce2cb0f323', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802803, ''),
('56a6670171da3233f01777401bfd06fb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800449, ''),
('56b97c94ad735889a5d4a1e8b3568e7d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802456, ''),
('56c7ee7ad38218cc60a962613f1768c2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801319, ''),
('56d6df9f94fbca7c823dbfc184c7638b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799717, ''),
('56d6fc4233f34f4b9326a16668b76ad0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801125, ''),
('56d787f5e9a6c7400b66094311c18709', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803043, ''),
('56d7abbad2b2964d311717a17c32319d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799692, ''),
('574b227875bf52a8bff84cb51e0a67a3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800631, ''),
('5798b6b17e58856cbd851911de75015c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801191, ''),
('579ad15529a12b7dd65032e68c76769c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802408, ''),
('57a9dc5167ef470fa72aa897342ac91c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800219, ''),
('57b5c1173865bd8fb88638a3ae5b81d0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800843, ''),
('583bf8fb7adf83c7d9e5890b997a4d1e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799651, ''),
('5868c3084d73d7831c92548c6db64241', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799830, ''),
('586ecae71dbf86f7af9af524998fabcd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802645, ''),
('5884d24bac81e4c57090fad90776c13d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800761, ''),
('58b41a41ab86f9f72122d7431310e687', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802631, ''),
('595544758f2ff6331baa162e849c64e7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802999, ''),
('5956a653fb0fd1ca5fd2ac51b5f39422', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800951, ''),
('59921535d7bcba71c113f1c266670e35', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801031, ''),
('59d31f994cbed99a900ec913bc6bb256', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800177, ''),
('59de4390eff271bca22f87eead71456b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802475, ''),
('5a0d1e6a7d57161e2d000341cc653946', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801171, ''),
('5a292d88cf5f457a6007cd72072c725b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799870, ''),
('5a2c57a95432d96a812884f26cae8d00', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799682, ''),
('5a3af57ef5e4ed722da8d2ff60646be5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800357, ''),
('5a6d612d8e45c07c0d28af32881ce179', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802371, ''),
('5a7da0a6adfc608af3b31815a6efb83e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802111, ''),
('5a901303eacf7366476d83e9546cec6a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800013, ''),
('5acea61f193d43e31c06ea40a7bfde39', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801069, ''),
('5ad349059b503c121131d927b7f90af0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801427, ''),
('5ae82e34b979994306b56730199eb629', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802125, ''),
('5aeb57c335700ed2dfdaaf43891794b3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802119, ''),
('5aef9ec26c0628163151abcef9ef833d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801633, ''),
('5b4f9b5017d78339fc59bb6651ca647f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802383, ''),
('5b8ced3f176a0fb971ee2cf1b1b539ab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803147, ''),
('5ba296fb5eaa53a368db31785e858a8b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801311, ''),
('5bc66a039626cc9eacf93ccee1273784', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802239, ''),
('5bc783a3dae6e00d7e2bff97422ed87d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799158, ''),
('5bd13587f46db23d88c5ab691bad10ee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799741, ''),
('5bf6a188436e4c1b5b0d798c84744413', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802291, ''),
('5bf800905de84577ceb8d24976b3ac74', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801836, ''),
('5c2738f08e751390a9aee9f9411a02e2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801563, ''),
('5c47cf8189388ce9c0a819b22844b6dd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799736, ''),
('5c515628a1c75efc674c0779025d474b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802137, ''),
('5c6fdfb6f0d2eee8f66ccd45595da2b6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799530, ''),
('5cad66e84db324d9f34584f1a33c0455', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802404, ''),
('5ce10e820e0935f2d092bba347b47935', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800243, ''),
('5cf1b76452e124181516edaa758ac0c0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801317, ''),
('5d00bb6087faeeb21b461fc03b453703', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802653, ''),
('5d23c31d07aae58fca71d726ef7fcdca', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800971, ''),
('5d52823fc8ad80781c27664119ff33d3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802804, ''),
('5d647061512dc36ff99ae1b913de239b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800115, ''),
('5d8206abdb4e7e14c736df01e124d9c3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799995, ''),
('5d94333e70ee77b21b0790962a69e63f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799635, ''),
('5da78a53ca6036ba11b286ca878f62eb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799122, ''),
('5dab95061ba12b914796aab765770879', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799276, ''),
('5de3be3d9ac88e7664aa69c9835eb3ac', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801641, ''),
('5e11e98b10beafa4e49ecffdc7886a34', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802887, ''),
('5e1d83f39e74e47a1ebde9f7d2893126', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801465, ''),
('5e287503ffad2c1d016013ea40a661ad', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802621, ''),
('5e693479ddd909412328b0ebc286c747', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800287, ''),
('5e941c8c2841b5945cb209c3ee7c89ba', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800118, ''),
('5ec272a3458e88d5920588d68eb68f20', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800697, ''),
('5ee59512dd19cc4d6af25c4205bad490', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799920, ''),
('5f0ec760396ea82626dea6fb120a7a7e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801201, ''),
('5f3beffc3fd2da3a7fe7a9eac0840ad0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801875, ''),
('5f4ec9da1f1bab5b1b7782c198c0d803', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800537, ''),
('5f506157f0bb5887035233e77bb1bf72', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802077, ''),
('5f53969d091835ab5731d7987c5eb9f0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802321, ''),
('5f7e497312e9511916292a797014b846', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801021, ''),
('5f8e2ed27845a88236ec106dc94aabb4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801409, ''),
('5f9f4a37e024dccdc77262869fadb48c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799641, ''),
('5fb1126dc6657d039316f393e456b804', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800595, ''),
('5fe7a32414cf0c0a200ec57960486097', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802335, ''),
('5ff28112bc8b22858fa5f77942ff57ba', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799639, ''),
('604dcca148d3d2241339b828bb247fb1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800733, ''),
('608621871809e13b9cbe6177d581ae37', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799779, ''),
('609971bad53873048209cd3e97ed359d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801743, ''),
('609b0602e0ec0c5bbece9da9164af999', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801457, ''),
('609e625711f6459bef40bdb6b433304e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799715, ''),
('60a57fb2a569b1d4d6890bda95f4eb62', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801951, ''),
('60aa7b45a1141111c6436cdc055f8b63', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800865, ''),
('60ed5724a18c62b8d1c53348714ef6ae', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801355, ''),
('610e54ecbb4e5b0423f54b13750ee4a7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801303, ''),
('611ebdfd87f1dc4f977e06458d50179d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799958, ''),
('61357c81f530318d47feec19f1b6f17f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799123, ''),
('61419c3825d67d0f448c3cd7b387885a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802445, ''),
('6141f6325f563caea460f3149be7e6f9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800291, ''),
('6163f310df69810bb9d411b9d6f044b4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802555, ''),
('619b53dafd2c59d6258ae996969018b0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801941, ''),
('61a3de5f8750b56e0289e98b3d05c894', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801265, ''),
('61d8ac8714adcc229dfe68a289b58c5b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801737, ''),
('620d25fa3556f109588d9b0e56f6f4c8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802743, ''),
('62310e03c77aa763bb7da67b9af443a6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803304, ''),
('6239b26e4ac24600ddad7bee94a0163d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800467, ''),
('62431e6d5758158338f6f3718f0c715e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799490, ''),
('625a9fc1f39e6509d832e8bc1d4e029c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800463, ''),
('6271d22687dd65baccc9f5750f647b3d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803001, ''),
('627889ac3b57ffc7915c89ea9605b0fc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800575, ''),
('6283d3b291cf96486e38835a9bba6b73', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799280, ''),
('62c1b44ddbd5f670aaabb53dc252f9c8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799188, ''),
('62c1d2db77cfe26ba807fbf2422d8815', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800159, ''),
('62cc327e2749ec7077297c6704cc0eab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802992, ''),
('62d442fd07e94a17f447127d50e3e361', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799706, ''),
('631660fbc6426c06b832799aaf906ef6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802715, ''),
('6317e09d6cb70b8c9d613a12110d2d43', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802653, ''),
('632a974493d933392ec2512d2a072ba7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801609, ''),
('6330d9ddf9efc0ff3dbfbd539c629945', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802821, ''),
('636291d426ab963c89ac17a379c01e46', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801655, ''),
('636a064819b2496c00b6e1460258c3bd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800925, ''),
('6375dc9be33a5cfad30520b918c6597c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802129, ''),
('6396d81eb9d0fb54a36998fca7b55b00', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799541, ''),
('63ab5ac648dc0d4c889eb66cc8938a0e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802945, ''),
('63bb0bf2fc77b0053cc1e456ed9dc6ac', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802997, ''),
('63c12f9ef4c4faf01705bcc3d1616be4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801347, '');
INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('63e04359f255e515af1cda9e05145bff', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800179, ''),
('6477c3ca2e76da0e640ecde06d9901e9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801283, ''),
('648b7390ddb8e7e61150ed6cd36b2700', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800403, ''),
('64b0c388758070a8bbb9e2478a254f81', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801475, ''),
('64b3404cbabcd9bd4659bfeb594cc883', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801273, ''),
('64ef89741078c3e6d9e9f8eca4a28692', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800192, ''),
('656697ecc19620a093ca51257984e675', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801211, ''),
('6576814bc26cc37b0abc6c83d7314a1b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802278, ''),
('658d37815e41d57545c1235210f7678c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799257, ''),
('65deba3737492fe0e94277c7168c7cb4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801607, ''),
('66016948e5a7aed924b20a78f83226d7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800543, ''),
('6613cc28954d002f8239001ca358ea23', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802879, ''),
('661e761028518f20387ff4c53522b8bd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802591, ''),
('66234bf2d23d72eb843ba2ea1b0b7fa7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800667, ''),
('665b0338f00eda151da29989e265450e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799880, ''),
('665faab3776d5d1b2223583848416f2c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801113, ''),
('6697a130e3bf19527a99c10b33a8bf38', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799832, ''),
('66d7f3594356d0c6fec4e3506d58816a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802635, ''),
('66dc232a498b7dea19d47a79747674fc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800819, ''),
('66e8b53b40b510d2a516e5c762f3ac5a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800859, ''),
('66ef40c1346de58da2aa0cd3536ba907', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800953, ''),
('670d010f0b1a3a3852815c6da8de23fd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802081, ''),
('6710d79778b88394dc4500d1475d1cee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799591, ''),
('671636af485a607c4e620e593b3376f7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801717, ''),
('673ed3e8d31f67639ddd5e3cbeb747bf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802696, ''),
('6759cd35a23ab4be509ff697d84c11b2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802245, ''),
('6761cd752f16c952ed10fe6d70cf1355', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800841, ''),
('676e9d75ff9a1196f4e2203d946720ed', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801359, ''),
('67737be1a454a5c23341325a36b65015', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802943, ''),
('677556e90a37c2842d02ae463d07bdf1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803095, ''),
('677a88c58a7652a5ba8eef102aa47a8a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802255, ''),
('67aaa5ee48afacc0e2a6755a08f2324d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800811, ''),
('67ae9d3df8560b96ec1dec6bbc96a4e6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800337, ''),
('67b4265396b346136e16a2b165488c71', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799191, ''),
('67b90f0a4cef3b0e65771ce48c849012', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801097, ''),
('67d45b748ae4cad61224811a344b943b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801837, ''),
('67e7956a03e028af0ec9241fecfdcae3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800321, ''),
('67fe5523639162ab46a7df723cb98cd0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801929, ''),
('6812bff08a1f601a4df7bde2e2464e6a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803197, ''),
('689498572a2ef933b25353b21b8ad1ba', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803272, ''),
('68961dd7a2a5ff70c0b4c399db385e5a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803079, ''),
('68d7d5e3cba7beda6fcc2fae363b7b1a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800715, ''),
('68ecd63e72f4ef35e962e5f5f6f12c1a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801075, ''),
('68ef9d9a3f6ff30639c97494c135e077', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801547, ''),
('68f480dbcf879db9e0f820ede0012ad1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802897, ''),
('68fb429a19a887eb8e68566a8f9077a8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799719, ''),
('68fe11c83827e453c3739b7b2e9b1e64', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803145, ''),
('69b819dd94e5944ad4f7bc0c3f216a64', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800583, ''),
('69c2401564430acb8b57e16cb6effc42', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801167, ''),
('69d6f7f96bb9e79c4ede17628ecebdbc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802611, ''),
('69def3dbf750cc7af563b80e375cfee1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801005, ''),
('69e568b4f3faaf2efe529c58248003bb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801353, ''),
('69f2cc189d7c031dbaa00afd726610d4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801141, ''),
('69feaf24576c25e3e0ce3b018f0fa998', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800661, ''),
('6a267aacf24a61e6e2440bce5dd27379', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803219, ''),
('6a766f8c1375ec2c1cdd9c455805c772', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800163, ''),
('6a8a8bcd07279bc2eb40b65a368bb0bf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799519, ''),
('6a90c0aeba2dcecff6369cbb55abb988', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801687, ''),
('6a94131d22ff6f0f2a1280331afb5f91', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801649, ''),
('6aa65205c414ec69e735eed88ef84b9a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800327, ''),
('6ac1ac7e7b0f1510901079bf33c6a7cd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800995, ''),
('6b0159f740dbfdfc248e9f3fabd12ee5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799126, ''),
('6b37bdc6ef75da49bb00e5f9eba43271', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802895, ''),
('6b83acdab2283e42678084674640e7cb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800895, ''),
('6bc2768c58ea8bbcd0e3bc2b24cdff7b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800088, ''),
('6bcc4f93bab64dba82ed4f51556ce63c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800433, ''),
('6c097ed423a0f344584a6235e39e2302', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802542, ''),
('6c10eb8362e25dbbc8bbb832b7e6791b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802935, ''),
('6c1ba29a0882127c984b6ed2cd70c095', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800485, ''),
('6c2aeb6337f81582acc256968302a0ea', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802723, ''),
('6c46214ee500fbf0740e49b7df2e360f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799808, ''),
('6ccb6ff10b10618cc308882945b18a40', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802337, ''),
('6cd2aa119a3e453f5ec9d70010eb6f33', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799248, ''),
('6ce74fda1235349294d0eb354f591e24', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802793, ''),
('6d26b2c1bdffd173e78994d3cce9d459', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799133, ''),
('6d4602b9d63a67769de2395e9300e9e5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802985, ''),
('6d4a212aaf17c196c890f513100e1f4a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800939, ''),
('6d7508f85184cd9167b359219916fc3e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802117, ''),
('6d7750b60d1f027a2251c67532b79495', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799587, ''),
('6d7fa56c5a5df8f8ace11fe4eea82e07', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802539, ''),
('6de161b7503a5433fd4c8dee76c40f01', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802467, ''),
('6de4cf8c090490641b0060e94864f3ed', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802441, ''),
('6dfe6d651bbcdeccacac094733be277b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800161, ''),
('6e1e356bf8cd5996b0a4e6ee0cbcb22c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799578, ''),
('6e440cdb41f13ea887ee71d4f58aa771', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801645, ''),
('6e57310ddb81a1ca7f47ceaed47d07c7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799616, ''),
('6e5d8e47d8648b66cfb7ec5d1748bca9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800673, ''),
('6e63bd88506fa9b4acc84340683a468e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799959, ''),
('6e9125f12a37e9e15d08ac43c5f45d30', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800921, ''),
('6ea9691336989691a8dcc4330b4f91de', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800147, ''),
('6ed7cfe2cb1f56c8b65ef388bd4a187c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803163, ''),
('6ee6772a971e697681cdc5255fd48992', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800659, ''),
('6eeff6949e9e672c14812ab333cb3fc8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799957, ''),
('6f24c52aeee20cb8fa2da2ea67d3c651', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802603, ''),
('6f2b802b1f3dc344af446c68922a878f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802299, ''),
('6f4ea22b6a55064a8e9555909a30e23b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799553, ''),
('6f97a1d8768f4577f136c5531f5a4ee1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801335, ''),
('6fa67a027630920c37db2ee67c391c42', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799254, ''),
('6fdf8b52f6011e7697aec729d7e186f9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800959, ''),
('6fe30dbe8eba7459c77f048ab3f51fd8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801361, ''),
('700471c0551bb0cdeb4abcc11518fea0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800179, ''),
('7024d05ebe23a8fed6cc1c14c5f42a3c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799482, ''),
('704717fb1a530f644b85a466a3acc3d0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801559, ''),
('707aa2553986a27d2c57049b4c7fdd39', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800783, ''),
('709c3e7df08bad81e4e68a33cc7a63de', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801913, ''),
('70ad4d3baa25d9fa88ed6db3a25b9de0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801699, ''),
('70c34a5cd7bfccb1f89fe637f3e4535f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799877, ''),
('70fa0bb01fe1022054b5b921792d6f93', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800215, ''),
('70fe1c39db37e877c72d50547e3ae452', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800071, ''),
('71061f9918130bc0820aab60719b235f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801595, ''),
('710b98e5542bf7d6576813bb002a5fe0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803059, ''),
('7123515911632da64e9a83024cee9a5b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799864, ''),
('7173c103798731e059052028e325053b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803183, ''),
('7182dcb5e74dd6e457208222696e1f90', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801501, ''),
('71879adc29e4dd9089166357cc6ec670', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799125, ''),
('71b3a373af70d76952269241e824688f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799514, ''),
('71c37030f21f36db25b9cbe38c8aad40', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801485, ''),
('71deefba14805b5a6b83d02ccf7efd1e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802507, ''),
('71e48957050d551386d55a28c24fb5c0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800495, ''),
('721a5389af53602c3caedfa239a27433', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802585, ''),
('722ab8a7f7c67fa824fda2ec77fbe808', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799769, ''),
('7244b8ebf1a9ac0f3f9cb4a8255579f4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802785, ''),
('7265af059c56bf7498808e2362885220', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800611, ''),
('72828f2647955eba8a153f4f24d13a5f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802079, ''),
('72a752ed2d6930a0e6b650354e913405', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802253, ''),
('72b7a247289ae863c23f42e02f3a8e73', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800795, ''),
('72c85fc88fcecaa4f449b4c4187da7f7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800765, ''),
('72cf92d3a8c988b944a06e7ab1d1b46b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802933, ''),
('72d86cda0b0a2ae8e1b0877f434fb7bf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801173, ''),
('735223855d0436a6b1cc41aa8e43675b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801043, ''),
('7373998b9349d4b598798d792ade6ae9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803306, ''),
('737bd3b3cf06eb941e6ad160c5a3a580', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800597, ''),
('738337ee23cd9e7f440d146962d2d62e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802443, ''),
('73b06b0c2993ca84d268b9a284d7ec21', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799993, ''),
('73d90498772be9eb463ebeb17d4eac56', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799852, ''),
('73e385748e32c7cfc758746d11275dc1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801151, ''),
('73f9b1ac47db9175ecd480e38be397e2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801963, ''),
('742e617dd0f46439c0c87293fdda9830', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799175, ''),
('742f2bdc7ea927ac7c90a2664c338a47', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799956, ''),
('7433581eda38b425dd8bad295a464e4e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800627, ''),
('749a9997d4d88cbc8db7f4d470d97253', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801635, ''),
('74a09812fe842ec2ab94b818507e48d2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802765, ''),
('74c609d2eb4f73dd80a2838a5200b98f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799263, ''),
('74cf52a90eef01b827de11eb13b42551', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803045, ''),
('74f20d73fed3218a85b323480899cd20', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802757, ''),
('75376338a3678078dfdd7d1e30c3a0a3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800789, ''),
('7558d0e4290a885195be997b2c95a72c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801869, ''),
('756e30d340b0d1707a02486fcabb03ae', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802473, ''),
('758faf079743641242e4af01499de0fa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799637, ''),
('758fb013674f28e7942f7907a3eb29ec', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803260, ''),
('759f20cd6fd143c9634bce1b8957d9f5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801987, ''),
('75b658d011591015508a0f80272bc55b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800453, ''),
('75b86e94b62eafb5bd568ab313b4b9f5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801863, ''),
('75cd3552df2a23ea3d976deed28873e5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802695, ''),
('75fefbfe99f85c155d92d47a8a7d1e7d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800831, ''),
('760185ddb900e06aba405461e3d66ca6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801189, ''),
('764551b378619883ba9c95ddbb057c10', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802385, ''),
('766309bb199827ff112c0bfe61d0cfa0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800969, ''),
('76c34148d08ec01aa1051c360e13a8c0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801883, ''),
('76cc3793845fc6195f0f82bf26c31489', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801309, ''),
('76edb1992d459b3652bac0f23ef1cae4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802145, ''),
('771ac52980e63de0e78591246fbc7916', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799957, ''),
('774aaf64ac9f00168566935fc4afac75', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800893, ''),
('774e11a31f3527970d3eae1feb4a53ab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799894, ''),
('776e7b2256e23fd4c771f1296749ea0a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800899, ''),
('778f53d9be6e5b723a3fcaba16757085', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800389, ''),
('779adfb95e2e630243755ab7089398f5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801139, ''),
('77b515f0ad6ea5e291c893ca3e5d0f69', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800689, ''),
('77c27f78b11f23ccdb8db77bc1e30321', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801287, ''),
('77c7af69834056991c5066bfa6471d64', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802573, ''),
('77f91ef6f2f8f3072db892bb75790b95', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802609, ''),
('78488f5241ea360626c28712640b3e9d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801855, ''),
('788e85e17e13a285785bf0865e9cd0ec', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800086, ''),
('789d769f07abc9ea6286110c98f5a575', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802342, ''),
('78cc5571832cb03a211760ff87952c26', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800407, ''),
('790778eadd3958dd46fc2ad1ef08335c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802560, ''),
('79201ae186b98f6f9aec492a3662b266', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802656, ''),
('7930dc2de8bdb0a6aba630156fe701d9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802332, ''),
('7948f38060d88f59431ab7a24ae07368', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799654, ''),
('796bb267ec61eaf78460cac6d2b65b00', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802513, ''),
('7989af3cc5796fb5ea3db80373ebd7c4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802497, ''),
('798f2081d14c0221dd1977a7dae33113', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799980, ''),
('7995a43af0e01b1214eebb9bfeded339', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801215, ''),
('79a975f266ef12a388b29f87c9eb15f0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802877, ''),
('79ba672d20b0936224e911b8c427ad82', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802786, ''),
('79db7da3339b44790786602d5dab982d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802627, ''),
('7a351a36f9676560b81c2ec3b0197af3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799900, ''),
('7a38d27480ca52bee6cb3ff63d0515cb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802481, ''),
('7a44349c989c093bc58907bd6c3a31c7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799798, ''),
('7a4466355c3cb1ab81c2815b7de33b7e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802333, ''),
('7a6527c3d5c365a46bd3b97f6e934b30', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801659, ''),
('7a903ec9120a883fab302d7ca2ae70c5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799773, ''),
('7ab801ea9c4a0e0a0de181b5f87d7ec6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800221, ''),
('7ac331b9bb09a9ab8f8bbde4aac10ff1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801003, ''),
('7ae0ab229af1b8bb0477a80a97c07ded', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802341, ''),
('7af4ef52674920013201d73da1e8262b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802343, ''),
('7b22cfd1745bea389fd7836883fb0abe', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801223, ''),
('7b54c0b950fcbb8072ac7c86a36c567a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800196, ''),
('7b587bbdb3bcb64aa76030d4ed7b3355', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802669, ''),
('7b62e1ff2f2dec1b4d7555cf7499e073', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800669, ''),
('7b68c575014f244712e25bc63afdceab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799540, ''),
('7b958a64fa52a0082ae8184076614afc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802617, ''),
('7babc5cc40f8884076712147301fd281', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800427, ''),
('7bd25b0fb99ff2880bd507a3369b747b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802469, ''),
('7c01565f27b7a00c1f28a64780bccf2b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802619, ''),
('7c0c293507954a2caa61a55fc39fb91c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802446, ''),
('7c0d6de2fc903fb80d76ec2eb5fa6784', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801447, ''),
('7c68008eb7bbdf7b8f91ffe49f12f785', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800383, ''),
('7c7cbad6b3a3fdd073ab4f3e2f8af65a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799471, ''),
('7c8bec814865ba7ab3c23c05e624d442', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801437, ''),
('7ca02280a0bf9fe83eaaa21c29c066c9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800709, ''),
('7cb37591723da04b783479e503b4839f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803105, ''),
('7cd5a5a970bd03c2b43f801c527d5dd4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802563, ''),
('7cd6cb58a38863a5e80cbe1514721976', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801887, ''),
('7d57149d87432010414a996c4326dcec', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801107, ''),
('7d6805cb78ce0683e5599a405146169c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801625, ''),
('7d8ef8ba9c279f34c7a16faa1bd49cf8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801229, ''),
('7dda5313b93bc6d2e1ebca1915be989e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800381, ''),
('7e311b22909d3b59244689aa7aefa202', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801095, ''),
('7e5eb47c7de1c3d846f1973efbd119d5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802279, ''),
('7e6a788cec39ad21837963371908b379', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802061, ''),
('7e918d4273472cfbf05a94b535b395e8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802045, ''),
('7eaaa347cadf52dd65bfc295793290d4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801495, ''),
('7ef12981b2f374c1713ef6978fbce181', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802731, ''),
('7f0a08a7674250e1faf0a37cb64440e5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800505, ''),
('7f16bf3b5ac31b40cd883e8843b75e19', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799436, ''),
('7f20e25d6f2e53b39ced822222f563b7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801055, ''),
('7f21026a953c34d1daa7cd3982da7d1c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799456, ''),
('7f62528762bc6668dc253c24aba4ec01', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801291, ''),
('7fb20a3c369d6fc10694c2f636f15e2b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802707, ''),
('7fb596dc80308813716a2d58319d7c7f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801745, ''),
('7fbf3ab73f6a8a1757f8b69374e99b97', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801591, ''),
('7fcac4c59ff1b8ea2921041b90150255', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801851, ''),
('7fecd31e6f064dc3aed13ba3304ac5cb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802037, ''),
('800358cb8fc970e31a92ea57ea687340', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800507, ''),
('802fbd12d0d728740d643c3f9b39163b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800775, ''),
('8096b75fc12eb9f987341e362c314669', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800491, ''),
('809950327ff0fd5ddb96918f17bed608', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800957, ''),
('80aba37fc2135729a75a4c52d2e2b018', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801245, ''),
('80b7329059197dccfa9b8a51fe20471d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802241, ''),
('80c43cfc7bfd59142e6352e5b20264c0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801647, ''),
('80fcb92c8574ac4dba3b96d6e515e442', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801845, ''),
('8138e2d85fe0ddbe1ed6fe279687e9ee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802249, ''),
('8143345f38caeb1bc2fbef44504803f8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803019, ''),
('814f99a94e7c5b422b5e6106655a90d3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800949, ''),
('819c5fc68019bcfb275cec25761d1f14', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803131, ''),
('81b474e451a0ee871b044152096e0f10', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800989, ''),
('81b8fedff71d95a6cabeb087649186d5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802644, ''),
('81e8d3b2731171633af7eea7f5e329ab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801187, ''),
('821a828b78b1c33abe1125d40afb50fb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799854, ''),
('82ab201917ab16bbbc62238dff5fbd1a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801681, ''),
('82da3b45234bb89bbc523ee267424323', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803314, ''),
('82eadeb0997a8f32c500545c2c7bf7f9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799199, ''),
('830d012e70674850b5a09d1852bdeffa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801669, ''),
('833ba0225d657f8771ce74cadded37fd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802605, ''),
('833f1c3137c5be3cdd6367c4d7ce6021', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800157, ''),
('8365f3506a831037fb8eda9eafdc9da3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802531, ''),
('8371b6c3a3895142a04e56d87b93e57e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801671, ''),
('8387c82bda6d579ba859464f210c5acb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800289, ''),
('839c29116024970683ee3429397a0a3a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800042, ''),
('83b9255f178266c871210fdd2c83b791', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802402, ''),
('83eecbbf061a7dcb15ace9adfc50d355', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801057, ''),
('840c7665d72f89d13a963ec90df629fe', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801651, ''),
('844c5788d27642dd0d3714934ae3c61d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802535, ''),
('8458f060167ae83b281188a5e724c156', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799203, ''),
('849d0bb8a07d46841f50157e215938e2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801405, ''),
('8507031c939efda555e099abb92a725f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800363, ''),
('85192ad414329d7786b30d1854403205', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801433, ''),
('851a9b3a24809ca6954a4126929018f0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801009, ''),
('851c29c6b64e8cbad5b2a3ff48817de5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801579, ''),
('852ddf40f3fbc566b242395b343c5223', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802113, ''),
('8534c31f6bffe7b44b656fa7ae744510', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802464, ''),
('854508228ffaaa6b40ee2acd305e78c6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802865, ''),
('8558a6948e23ccb94eeb644ac64fed20', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800567, ''),
('856a66d8036d2522672e31faa4e205c8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800833, ''),
('858757521467b60eb4d3d1bc34947754', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800977, ''),
('859e7e3250f57579ecca5c2b91b3636c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802695, ''),
('85b079bd1c8d5f8c4deb3e3e5aea937e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799883, ''),
('85d95beaecc758a9b5083479090836c7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799492, ''),
('85ddd1f4aa327326599de846be74a93e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799179, ''),
('85fe59170242dd4e70a4985c17356ef2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801893, ''),
('8605414722421a2ea3be69455af0074a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801467, ''),
('86157ab6af7be38e74f88b69ff8d0354', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800773, ''),
('8649d85603b6250c7df61f92c085ff07', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802802, ''),
('867d5bd027b04d85ac757db0f89e2da7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800607, ''),
('86843bb3ba01517240f8c3545d6f213f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801093, ''),
('868ba1ff189b23f86ee6e48ca8022dd3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799648, ''),
('86b57ec08cd5c2a18b0348942b5838fe', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803209, ''),
('86ca86573df1c7f405c1bba28febedd8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800149, ''),
('86ce0c96e934071efa2fe654fccff267', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801565, ''),
('8705012687ce42ffcfffb2c8a66b8ca5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800027, ''),
('87051fa85ea4daa7dca8216126c472cb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800965, ''),
('87504563d9d338b32725df129384470b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799641, ''),
('87a0deadc82f6576c04416df95e19060', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799906, ''),
('87a4de5756e68c40892dd97ec8647aea', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800613, ''),
('881fdb7a06cd6ff7a5bf14b5578d9cf0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799811, ''),
('882ad7dad3adb53d4f6072ea7bdd93dd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800723, ''),
('8831dff52e225ed4d4b56b46d98849e6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801133, ''),
('884ec3b685f10767fb8b05124a2bb013', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801369, ''),
('8858dadb348f9e56dff0d6c4a896abe1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799635, ''),
('8865f67ecc3573f0628b4d18b5e5e206', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802441, ''),
('886c20804b32882b3e32c64e9d3cdbd9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800623, ''),
('88943a9e8514fc95ce0ea65bcec94b5d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800037, ''),
('8895d81727412d1749fd22eb0e1554fb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799958, ''),
('8896edab3a3014f80990f9351d2f661e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802131, ''),
('889d3cc4d6a4510dce48ce349aeb4f0f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800459, ''),
('88a78d98a2397fdb70fa43d9448be048', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802817, ''),
('88b30e0ae004099b939923d7440602e6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802099, ''),
('88b485273b01b926bd8f24117596c55d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799805, ''),
('88d6acf25fcc3c7c8fdd6e8ac6eff9fb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803047, ''),
('88de9e64ec010cabc5ee85152d061058', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802861, ''),
('892409fded3ef8e1ff94d3e7b380a80d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801881, ''),
('892f94e3027dbcdfbd374fd1cebcfcee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801393, ''),
('8941aeb66a7c5066c56031ddd9e6dd8b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800469, ''),
('8943dda1e55ae466f0d96d8a5e7a5e0c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802795, ''),
('898f79001e533e31d12cf7e1a1aa338e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799181, ''),
('8998a04b319f71ab178839a3a4dd57fa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802969, ''),
('89fe3a72d84f2f4120935461af27460c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802236, ''),
('8a0732d1b68561b5c8261f812023d171', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802502, ''),
('8a081f5436c79904b8cc63f1e8e777b3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802869, ''),
('8a113c2e51499a32c015a0be0b6cc1c8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799518, ''),
('8a1673cbc46d72785cfa4822b938ac31', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801365, ''),
('8a3267a8a7a9eb32d366dbc22c0da9db', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802971, ''),
('8a3a25850038d7389821c2d249a795e3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800479, ''),
('8a3deaca4aff2f04864f382158039458', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802281, ''),
('8a62207fe4af97537fe77621d127a305', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799518, ''),
('8a6752c1755a862b8c36257d3ae61e6f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802523, ''),
('8a80575280862ffe2e61e89f32813c0b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802313, ''),
('8abdf8d08aa76f231f18c7b8ca403f10', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801059, ''),
('8acdeb4e111927ce36d450e0ef33df23', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799501, ''),
('8adcb4c05eca550d3ba740e5369f15df', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800535, ''),
('8b0f08f690dc4380fd9b0236bffc6e75', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799969, ''),
('8b1cd25d72233ff2e640f620513db4a0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802085, ''),
('8b2f46cf2256a7eddfea05a5d83932ce', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799516, ''),
('8b37e4b80bc28c7e97252f46c9224813', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803017, ''),
('8b43bdc31d6e06a695eb6caa2af746f1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803109, ''),
('8b677a0cf978cc2921d8b6629332a9ea', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799247, ''),
('8b904774c8284ff8fbdaf209cc54e492', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802023, ''),
('8b95ca31c492b63a054773712c5f46d5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802147, ''),
('8b9919b0e7cbaf46574c38a53f59e5f0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800999, ''),
('8ba6b05a5e2a44e69e577411de36ba96', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801127, ''),
('8bc0b7c557fe45d773fb328284b1b481', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803324, ''),
('8bc4637257eaf4c5d469c0c8a00eaed5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801689, ''),
('8bf59b030dbabe43422bd220b0b084ce', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800647, ''),
('8c07b1fbbba7366223bffc55065c0b19', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802518, ''),
('8c2dce6b144494e97839d76a0d42486a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803133, ''),
('8c33aa6005b691d7173c3bbbb1f02a16', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799925, ''),
('8c359cb63407522605b31d1a04074cb1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800033, ''),
('8c7d58f20ac6cb9c0f5c3043d21145fe', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802285, ''),
('8c82bc86e5563645a60171fa0073ebee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799478, ''),
('8c9a60796521cb07c8d2e1ec274f214f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803187, ''),
('8ca0812e7feafa1d8d1f283b5aa258fa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801673, ''),
('8cc81dcfc71e2b74d995d774e1b0eafe', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801849, ''),
('8ce76375488fa86cf26e411f7f6b6097', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802412, ''),
('8cf1b6bd718952ebc0a6ea8cd3ef8887', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800253, ''),
('8cf1fa97ca92551f3c49adeb87be93c0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799278, ''),
('8cf5ee1acf0b3149b1480156c6092f6e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803093, ''),
('8cf9eb8bf72f2a2e39b4db02c183606f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800007, ''),
('8d0ae61a90804511612aab2aa52b26fa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799590, ''),
('8d46a9d855c7ea3f6d965e889c57eb02', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800735, ''),
('8d4c8371711fe134673a84597f8b1b9e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799843, ''),
('8d6e33bf1949d7e209b5f901d34fb3e0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802059, ''),
('8d76dfd247eb87678241e701fd45d77d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799784, ''),
('8da8687f516aa01ff73627e6155bb539', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800409, ''),
('8de458e5cb23473ff8cd2f2cc55fb6c6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801895, ''),
('8e19d4b0c4f60c01c77e098a93b6c1f1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799616, ''),
('8e2e9e5f1494e267a1aea91bd7935665', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800283, ''),
('8ec55ad96a19dd2a4858a2fc51099f33', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803254, ''),
('8edc8e6557a17c504f4cab1abf943459', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800455, ''),
('8ee7812d81972c7bcd8b672a79265151', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799982, ''),
('8efffcd0055d5803fa9750b6e7493dd8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800259, ''),
('8f16226775c04de433ca47eb402c2bab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802103, ''),
('8f517a51ce76fde989f348fa8af6883b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801459, ''),
('8f6388b0fd88da18ee2d9c4568a86e8f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800945, ''),
('8f7ed07494c63a80c0e98790468d8a5d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802035, ''),
('8f920163ee1aaba70051fd200a8ca6e6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803023, ''),
('8fa785e51a723e311597ebb5e677fef8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801271, ''),
('8fe37d5c003890433c5e577bd905f35e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800923, ''),
('9014fd974252cc2e94a8c584f9203cfd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802033, ''),
('90212d2d8729d330be6fce2b8cd37fda', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802400, ''),
('9031d480631de4975cb3f0a2e4f575d4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803232, ''),
('90bded10fff59ca90635b6d5e94c48fc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803031, ''),
('90c7063043f6ef5307309d587da135be', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800751, ''),
('911299849dd1ab5fb9df99feb388f786', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802611, ''),
('9117723f503f005021441f6e70248571', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802495, ''),
('91315d446b319f49d8db4f12f63010ab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803292, ''),
('91353e992b80031026c91fbb66ed82a9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800251, ''),
('913669b8ca7b7dcfb45c4f832dce72b1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801603, ''),
('913c0980b46b74a5c19a67e95acb183e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800609, ''),
('915c1dd83a4da93b2d573057fb64a8c8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802881, ''),
('916a08eeea22a52d7c309d88fe98476b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800293, ''),
('916de4272b4d073c8c5696d0559ee739', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799579, ''),
('919f31d2d84d6708eb0c07a3037dac60', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802095, ''),
('9206a38a067ab54f7d391f7e11cf516e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802271, ''),
('922f1ae22e7c3bb4cbaf57d5133d222c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799934, ''),
('924203761eab847fa47f14b935d83c4c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802289, ''),
('9248f98197fbef39854eedc31e1f13b7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802343, ''),
('926188962080e8a62132ee788cee54e1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799271, ''),
('927f9ad1c77c1107bdfa560970d51258', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802681, ''),
('9289f06f9ec3a7a63722c498ffd27d0f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801541, ''),
('929db36742a332eb996d9318aa3e2fee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802711, ''),
('92cffd7c7b9e77790f820b7a88b0273c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800031, ''),
('92ebef07403f206d870ba7cc4a194fd1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801321, ''),
('92fe51457de9bb5597ee571130823b22', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801619, ''),
('930c663e5a6b830ae12fed0eb3d58015', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802410, ''),
('939faac013890c93c64fa9a1f927d2c9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801169, ''),
('93a2dc167ea17a0f41a06ad58af817cf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800591, ''),
('93a59f36205bd380827354053f1ddfdb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800849, ''),
('93c11c2c15ea3314e6845e5759a5726c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802773, ''),
('93edaab4e3dabc96ed414aebde768e99', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802735, ''),
('94344fc91649d7e9655c5340b7a22b9d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799927, ''),
('94703ab55b0e2d63aa32f1bb10ac4856', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802692, ''),
('949c89e9490297b053ba3deb1a003c7d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802288, ''),
('94c924bb36698f5f8a0b0c4d91f4c2a8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802139, ''),
('94ca2ab60c30c4a822140b4a9cffd91b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799949, ''),
('94deee4132f887f18d3c9b817d989d9e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801061, ''),
('94e905bdcd8becd9d218ae1f090d6c75', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799749, ''),
('94f591e41895bab3459f370d2b68c612', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800857, ''),
('9505e7ac93f6bd21aeab0aac93be4898', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800873, ''),
('952d5d193f5a916aa4f1b9be0a0bf59e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800423, ''),
('95413b0c980636ba9285a0441fe7ce2b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801351, ''),
('9542038d6e5a5b8b4ff8bea1a799db72', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800001, ''),
('955af3d1c9b6f4c33bbda8838aa87517', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801385, ''),
('956da67a88a55c0de19f321ee2d4c815', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802025, ''),
('95acab4ecde193ede81573110487be5e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803025, ''),
('95ad0d155847a7bebca970032e3020c5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799467, '');
INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('95bbdef0e7546956d8e7e68e2b62613b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800209, ''),
('95c15bcb49c39868597edd0fe424adb1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802315, ''),
('95da5ccd74e6b6b9c72dc17d67a68031', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800347, ''),
('95e15da9cdd91a42738a8e3c6d584e24', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802529, ''),
('95e700c5860730b7846e0b167933d2f7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801399, ''),
('9605bbb98c3fcae27eeb11c4cbecb7fc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802434, ''),
('9618832a8f01f5721810a81bf6477ad0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801363, ''),
('967ce0a46756e6d3bc6d793d94e29f95', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802428, ''),
('96f9f915e7fec27cf456933b2e4d8693', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799767, ''),
('970d47a613f678a57cac01737803f5e9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801853, ''),
('970f3715f0c94781c6e58baa4bfbbbbe', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799828, ''),
('9716999c10d1b3f6f00db8e244f01e61', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800909, ''),
('9724c1c207c0079312f69741d07930d5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800717, ''),
('9725f325f560f9310d1aaace26527864', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801553, ''),
('97281c3dc39d4fd0a9d4be84769c351a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800861, ''),
('97583901e6ac2455f468f0d33e486a55', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801711, ''),
('975b9bf4b541ef9fc5f651620bfb0742', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801329, ''),
('9762d566180ced4c95a07cd41e2cceec', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801491, ''),
('97dad4db394c9dd700107df67a335218', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801981, ''),
('97e03337ec9a61cd537a5263931c2593', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799575, ''),
('97e75a6ded16af26e8a847a4890c5a7f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801615, ''),
('97ec5f745882817d0d15b27e97bf4473', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801627, ''),
('98852d1a6932e6a49471d94f2cd01eab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800281, ''),
('98c3d59dd0aa3b05c228a9e45e8765e5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802759, ''),
('98dae93a2e92365fc818a4d53ac007ce', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800098, ''),
('992ad569dfcd8eb6dd813775f9f06654', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799596, ''),
('9944a20da9ffb555b6b8ae6eda49a2dc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802343, ''),
('9950e45b5a1a593c9622e54c61eb8ab4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800136, ''),
('995fe03bf1fba5b0f858fe59972b333f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801725, ''),
('9960451249f4069dc9c906a87e3c43f4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803077, ''),
('997aa0dfa8bf339819d0cfeac6e319e8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800665, ''),
('99845977420d9d594c88def45bb263b2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801213, ''),
('99a7b9199c5cbae21c502f1ea1c015df', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802965, ''),
('9a0a9a3f5cb40827a8b8a1ef4323a32b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803246, ''),
('9a12be345ab1361dd70742979ecabc44', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801421, ''),
('9a1afc9fa91d0e6898456f60b423ae8f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799235, ''),
('9a370adc101a93cd6035801aa8d2b5af', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799784, ''),
('9a3751f04a067b2dc66b6b9954a5fa0f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803310, ''),
('9a466c30ed0b026897ec498d8b29fa46', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799641, ''),
('9a533439bf9e8b3c50613a1ef5c1cbe7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799128, ''),
('9a5923bb84606e3c0ab9b4bcd50f58b0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800901, ''),
('9a783ad17ea27c13de3398d17bcddb70', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801927, ''),
('9a8681ba5d3ca34182e52767795e6ad5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800217, ''),
('9a944e311565b4cfc61814d93274ee7a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802428, ''),
('9ac4ce1008682a80a7915f0a0ec9d44a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799749, ''),
('9ad7a1eafea6818f00c0e6fe9a312471', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802031, ''),
('9af8f684f6df9125a49b8a38d73ab05d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800847, ''),
('9afe54fa6956f523baafdfebb55fb920', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802763, ''),
('9b15dca3cfc73005654524efa5bc7ee7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799439, ''),
('9b3ef62fd3bb40989824d23d14c9638b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800625, ''),
('9b5253d9a34451b8175a70a156eb7900', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801957, ''),
('9b62235d6c4d385f410f39fcd122908c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799817, ''),
('9b8772c857529a01caef5f484fb0deac', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800681, ''),
('9b97526004fb95e7bc0b254eaffc229d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802133, ''),
('9bbf57e7a8a252c8179206818168d364', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803035, ''),
('9bc955c36068e184474b627676202a4c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802661, ''),
('9bca55cf654721a492ec1b643af77083', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800807, ''),
('9be42991a0fc73b1a09afeb1c9eb47a6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801723, ''),
('9bf29c0cec9032bce86cd61c08de2083', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799895, ''),
('9c1df679f2a897438427fcec9ea6b26a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802925, ''),
('9c2f69b44339fa4c9e34b7dd9b010fe5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802719, ''),
('9c39213308d53e1bbe79b2b70ff9e0c8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801391, ''),
('9c4be8361b32e5343432eb1d1e064a2e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802127, ''),
('9c5d99259ef2208ffc1721233ba3cc64', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803312, ''),
('9c896e03b213f27b7c09d6cda2744a89', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803033, ''),
('9cab8ed27c6334009abddff59656b625', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801995, ''),
('9cd8db22109237db12ecb650b725d74b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801933, ''),
('9cdc41b8afa25637cd4b072b5b02bb94', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802813, ''),
('9cf2acd7de97cb22297a5926398381f4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803340, ''),
('9d1a5afa2f23126ee92ba6d83c1c3063', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801463, ''),
('9d1c48afff5372b0774fe2bba9d6fcb9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801561, ''),
('9d3c08fb84b2425d0a55d011fbb4d78d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802463, ''),
('9d7dba4221213c94565ba3e39482db3c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801824, ''),
('9d93976367557789ca17437215c8c5e3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799259, ''),
('9d9fdd46f50509100ff86e22e37da3f1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801707, ''),
('9dacba32b279eba117a09b1ebdf87dbe', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803175, ''),
('9db85202f356450bd18970c05045a9d2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802345, ''),
('9dbdddc931e520244c3090a80c395cba', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802269, ''),
('9e0c53821dca9c2408934d33672d3509', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799500, ''),
('9e0f69489b3215f0fb2510c054b60df1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802931, ''),
('9e1c1825820d2fafa218f19cd32a65c4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802751, ''),
('9e1e190081d911b495f48d679cd0e216', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799980, ''),
('9e35e86f30f84a295a3c88a71e059c0a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802859, ''),
('9e71b2fa76aed36eb40d5c575fbec8e0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799594, ''),
('9e89feee7cd603bf4dcbc3720bb2464e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801155, ''),
('9e8fdb8a4b9376140cc79ccc263efeff', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799957, ''),
('9e98b3e37fdba739c34745acefd15906', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802341, ''),
('9f337112116b673af4928c10362cd52c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491798481, ''),
('9f40620c99c5c8fd942609982debb518', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800739, ''),
('9f63b14f0a6cdc7ff85a12873057061b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801971, ''),
('9f6fd4393b7d8e4b50d4561629e362ba', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801307, ''),
('9fb3d280c9c06e5dc751c22a260f2fea', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799826, ''),
('9fce2f64aed11fa55d29f4f2cf055510', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800369, ''),
('9ff47e5d7ac1e00d5844fa6fe5097dc3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800092, ''),
('a016532fe2f5a2cc65ec6e841c875ee0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801163, ''),
('a018feaf4e7ce11235b03add1c446128', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802891, ''),
('a01d472e5954104cf76301fcd29f0e81', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799731, ''),
('a01f7a4841c3a265dc855059e6924f9b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799802, 'a:1:{s:9:"user_data";s:0:"";}'),
('a0482056f2534f5990024cd4530fe4f3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802273, ''),
('a07869a275105e82ae2e83d3b685ced9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802143, ''),
('a0829eef950d0dc95aaaabbdb6d68db3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800823, ''),
('a0c1d20e18a36c6ce897a48631bd00db', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800213, ''),
('a0c7f1ea41cdd6829266c9fa12c56246', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801221, ''),
('a0fb5c373c5998b4cc7ac8349586c64b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802236, ''),
('a0ffb48d66e0c92937bf5a72ed12fc82', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802601, ''),
('a134da6ffce05f57579dd4818bf8fd67', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802583, ''),
('a1601b8829c3d642ded34d3ad7b7c710', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799185, ''),
('a18d7957c40a9ee59a25a919b67a29c3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799775, ''),
('a1c8efbd68d1e4af1a6fa0eb75776b17', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802536, ''),
('a1f5fffbb889ba0bbf5c305d62d3bdb6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799460, ''),
('a20150ac0a2ee5c09f8f56f8b11f7273', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801873, ''),
('a21296b89a6c1578a22ae89faa28ea8d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801675, ''),
('a22b0388911941195d8c7eb8b64f7769', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799244, ''),
('a22b15f3f146d748fd9c33fbff836ce5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801665, ''),
('a23d3de5ba4f28b32cd34aed12a54269', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802402, ''),
('a23d4e162e9dba4d0699ccb3a90e5ba6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801007, ''),
('a243d8bb03928244315d101ba4aeb37c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800105, ''),
('a2479566e4714684524a9656d795192d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803065, ''),
('a265fa2fb925efc47ac54a16f73de091', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799635, ''),
('a27e5b6b0538c0717aa6b5ab04f3cab5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800145, ''),
('a2b3d93c353e643ac42a71246b884244', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799802, ''),
('a2bcb7cf6003ddc35042100b2a1cb5d8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801629, ''),
('a2dcee74ac0569061b7d2e790f5b8b92', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802382, ''),
('a2dfdad918a1c9c43503ed8905c55056', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801085, ''),
('a2f5b3861d23efa0c619597830d50ae3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800233, ''),
('a302698917edb1e65c94d5218cd0d8ab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801697, ''),
('a3117475dcf4668a52997cc61ad6f311', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799910, ''),
('a332a548ea0a6736c109067830d92df8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803089, ''),
('a337fa09b33eb7e5d1e9f9764c0fa88d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799617, ''),
('a3916254296cc471f1026123768618d5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802914, ''),
('a3ac96db3dc502b947518cfa1e0f6b50', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800757, ''),
('a3b2bd9066cb605f801864bf09466d59', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799701, ''),
('a3d23cc8db709555a2260f8f6b909304', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800725, ''),
('a3ebc4002b66ffc23f11ff26e09ea504', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803161, ''),
('a40da90920ae45202ef83fa3b3edf07d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800461, ''),
('a44e339cd96de3763082613beb2373ae', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802995, ''),
('a44e69e6bfa1335548a01fdd6b31b272', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801731, ''),
('a47fe6869e77544b1eeee5bfd484935e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799753, ''),
('a498fb4964a1a4dbfe05ee664e338dd7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800885, ''),
('a514a5f20ac251e4f1c4492af68754a8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802599, ''),
('a5154337c2eb6089821f11050204c566', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802981, ''),
('a5386fe8ac9c577b3d4531a97bb1514f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801685, ''),
('a54688576eb93c04412811514378396a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802379, ''),
('a59b4852079758669fcb44daf21be9c0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802263, ''),
('a5c080387c1e0fa7dfcb0e4baa38c9fb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801611, ''),
('a5c0eb331f08a3ccf954be1baec18695', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799564, ''),
('a5e34835074285f2e70474c7a47818d9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802826, ''),
('a5e994e730bf1bfb4e87b6e93fd6c3ad', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799451, ''),
('a60d2ae2f1306e9549f012973c21efd8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799601, ''),
('a61adc28792ebcb0aff2e1b897900626', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800855, ''),
('a6385ac85bb1432951b945b96005790f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801589, ''),
('a682cfba08fa846a557f48243a312be0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802837, ''),
('a6aca3a42378b9f46862c0dfaf980f02', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800065, ''),
('a6adc2335a19d441949ccd050b2d9944', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802069, ''),
('a6b4ce319175dd9b4ee900a8cef80c4f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800307, ''),
('a6b845a5d0071a692294ebd682fa3f82', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801149, ''),
('a6bf0a1488564f0152444f844a2b58cc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803021, ''),
('a6c55a0c120bf2c61dcfa7c769ffbfd0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802639, ''),
('a6cc12c74dda9b4ef3e7880dc37a6eac', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799868, ''),
('a6d6d382fa1d514f3e604bb54fdf0497', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799500, ''),
('a6f963165248df3232c849a10657b6bc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799756, ''),
('a70b576375c3d717caa7cfd4a5658321', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803113, ''),
('a78d27fbd3d5cd25e684d296039062ba', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801023, ''),
('a7b1cc3cd706e50e52d78d98de35aedd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801235, ''),
('a7f6119181f47afd2b047cb3a52b8db5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803012, ''),
('a811fe00068fb80916dbc6f540f5c33b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801277, ''),
('a83209855f801e52b9ad220fd25b2e54', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801969, ''),
('a855645751e11167abbbc8e1525179b5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799874, ''),
('a8737352a3b5fcae10c75301bb9bf56e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803191, ''),
('a87d9615b0855de9512b3c56f7172d92', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802435, ''),
('a8b1d1c7e4de64b6b69eba7ef2b79417', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802779, ''),
('a8c491f9f55de5b1390d498b5e23db57', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802990, ''),
('a8e516bad38ab7b68f20cd32cc8f189c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801489, ''),
('a8fde0153847ff7b232d41e54a512470', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800367, ''),
('a983e5e9ddb4d53060ba17b2999c8a5e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801621, ''),
('a992832bfa12814841c1dbda01914ac3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799693, ''),
('a99b0e17d9f8a7b45e281ab5b369a42b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802357, ''),
('a9b019a8d77720884f135d3c88226c21', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802462, ''),
('a9e1345744a806513009487b289eab06', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803009, ''),
('aa000658991b99280114a53c8b1443bc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801991, ''),
('aa2cc78a46e763682efd1ce97217624b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802297, ''),
('aa2f8dcb3d89ce4fa6b89b424e491507', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802450, ''),
('aa30a7881aa449f9a2d4caf1083dab18', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802461, ''),
('aa4531ddd8cf377d4e66b78d6dba6cd8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801197, ''),
('aa4892c009e452e72408adfe1852abca', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802983, ''),
('aa53e6ad21a4234d85575884adf51170', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800881, ''),
('aa563d7586316e9596d7d852429db2dd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799496, ''),
('aa68a6dfd4d5b12e7cd7015374e51dee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800675, ''),
('ab2bdfd1b0c3193a5cb0c771d1c5eb91', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800285, ''),
('ab31837de2b48d5726afa4b8700ab013', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799553, ''),
('ab4b17080b884e112fc2d0b81b4b8b41', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800255, ''),
('ab6fee892f2fb25d3ae8c1c50dbc0787', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802903, ''),
('ab8e9a8a6eca44902003e07bb34d13d9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799643, ''),
('ab92293f989e8f4d2572d03124cc1115', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799983, ''),
('abc96275095b3d7d9d5de86c8cf099b9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802659, ''),
('abc96ffb19f3bfad3798471d1a55dbf3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799648, ''),
('abdd53417ca8b94544dd94d50de2562e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802301, ''),
('abe41c98ecb3cf485b386b93aa97a50e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802947, ''),
('abf7716751cacf0de6c8c042d6a8f601', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800229, ''),
('abf9f946c2d03e30457d3d3a0e1d9088', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800621, ''),
('ac3eb6996f82b73441fed9504a1cbbfb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802427, ''),
('accafcfbe3bfb343255374d732e81629', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803123, ''),
('acf16acc6dc2aa416904170c8b96ebce', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800875, ''),
('acf6732a86323b95601c302c71781f48', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800319, ''),
('acf7fae41598e7ca0a7afce085603db8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802909, ''),
('ad2fe9c281a3c1ec77791965d404d891', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800663, ''),
('ad4ffb226c0bdcafed49f2d86fd5113e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802323, ''),
('ad5f40aedc593d1e2cb1d9545289e2c2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801179, ''),
('ad5f69e2e8ffdce18707bfdd03737267', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800076, ''),
('ad94a572b97598265c0fc72dcbf99b07', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802514, ''),
('adbbd84dbfe8f37e72293c9945102ae6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803318, ''),
('adbc3335ebe50db4bb4a12e01d5488f4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803270, ''),
('ade91a3e970b159a1f5321622075adc2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799951, ''),
('adeeb8f2c9fd2f2baee026c9a967ffaa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800481, ''),
('adf7b9cfdc45fe5bc7015c9c62d424e2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803039, ''),
('ae1c18252555e98b30cdd8e37f954fbc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799446, ''),
('ae2cf19953219bd6c075972d66fd36a2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800301, ''),
('ae4ba4fb8d51c3ac59dcf4d27860d790', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800405, ''),
('ae4c225e32a1b265e1a936181702f99c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799819, ''),
('ae52a81acebb3e17fdd33df307515e3f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800445, ''),
('ae5ab159ed6e4685c773663bceddb311', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799200, ''),
('ae6e945e995b0f97a676f8095c979021', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802901, ''),
('ae7e7b194f0ff9aa37878c35270c8f06', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799502, ''),
('ae91a57ea2f3cc16c6c4ff9b7936b8ac', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802843, ''),
('aeb56eed05605a75021abb86e1e89d35', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802643, ''),
('af0bc35e52166a8180944911c4673812', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800043, ''),
('af522d583350c5047a66c56fabb45be3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799924, ''),
('af52aee7089dc056575920271e1c3e29', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802625, ''),
('afac35e3369e3a72b31538f39190b80a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799555, ''),
('afbff92239d034e9460d29e015872b1c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799465, ''),
('afddcf4e696b82400890d6d369ed4d57', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800247, ''),
('b07be9c768f74672d9e80691b3b16843', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801823, ''),
('b099137e027db08bd8fed48a73aa9d52', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802251, ''),
('b0d1b9ab70c2f674e65fa1b7cb32423f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800139, ''),
('b0e9e44e79b20803b1b1663b855b6fe9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801115, ''),
('b0feec4f5af5f3c8e5fa4b2d1c9de60a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802863, ''),
('b138eea0d69740faf94395af1da16da4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799233, ''),
('b13a57016cab3dc5992e165312b4817e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799944, ''),
('b15170ce5fbc5c9b5d8cf1b8687b9c5d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800113, ''),
('b1b2c1cf5b368506ae14dbc4af22943d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801407, ''),
('b1e378aba8fa7b48932b1ffb22cc5a6e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799217, ''),
('b1f532002a90c278d732ecc4c9104595', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800851, ''),
('b21a83320e11cb3cedbbfc24cb8fb356', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803129, ''),
('b2531f5a0b5fa40c07b7373ed97146a6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800941, ''),
('b25c0df8e1ba894d33ef2154439cf8a7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802051, ''),
('b28ef13b2618bf888c322ec964259546', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799540, ''),
('b2a7148c2d85fb67980d0ca2626b3327', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800351, ''),
('b2ad0336499efb8507443d8db32da184', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803179, ''),
('b303ac82af28406290620fa2e6013f64', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800223, ''),
('b307d0d69f6c7a59c3c60c1392fcf04c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800413, ''),
('b31e96dc59909610feda149835361305', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800545, ''),
('b34ac6bba96cfd5d7f368e9938c22443', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803189, ''),
('b353ee037564375e1ba16b1ef63ddbab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801401, ''),
('b366a007724a9e7f994933d06eb4d98a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801931, ''),
('b39b677a9ca9c77427c7d36f2abba6f8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802490, ''),
('b3aa155beef28459516248abe9641203', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802265, ''),
('b3afdc927454f54a21b8537960da9a2c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802660, ''),
('b401b14ed6148cdc3d115bdb2ad90046', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802925, ''),
('b40f6e9a828f9ec7df1874a7543b65ab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801953, ''),
('b430905fd1bc7a7609a33a768057143f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802611, ''),
('b475da406dd6c91eb999c9db08864edb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799623, ''),
('b4b14f9d571c794445e95600aca323c2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801835, ''),
('b4be1bd4ed65ff7be2b40220a19f21cb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801877, ''),
('b4d5a0d8de21684b8f98c1f16b7c595b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802109, ''),
('b4e7d88484bd8c0b901dd453d991ae7b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803211, ''),
('b55a58dc9511ad5c4312a4cc0e156b0a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800997, ''),
('b571788862418cc7cebd7ebe1164a935', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801205, ''),
('b585acb1949368ee783dcebba39859da', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803115, ''),
('b5c499e47a7c1c7747969babf1083f07', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801667, ''),
('b5cdc9150c543c2a8c146768c4be24f3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799632, ''),
('b6822c77fa59f1eb5090ca273808ce6a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799145, ''),
('b694433fb6fd2e05b69c73175a192eda', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801123, ''),
('b6b7b6a50cb5f3e120258c6fb0bc7c7e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800100, ''),
('b6f58155864373e60b65f2ac81478c0c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801967, ''),
('b705959656275bfbf129276ddc292c17', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800017, ''),
('b71f102f5e337977e1f86e1fafd67130', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800871, ''),
('b738971c3e8ee1e3db691783171ded15', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799976, ''),
('b76aef38df62fb03e296494c875339fd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802725, ''),
('b773f2bb57f390bc875bb6fb7c8837c3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799997, ''),
('b7a68572f3d13325876c8f5c7cdf0d88', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800473, ''),
('b7aa3799554840b987264a6101cfb7bc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801844, ''),
('b7ad96795e159debebe1cf9452ff9c0b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802571, ''),
('b7be685f2dc85511fa662a9b4c3234aa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800721, ''),
('b7e3d48a1094376b681225b7434ed7de', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802123, ''),
('b7e7c97aa06d4b45cf75f1316aadf7fa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799635, ''),
('b803cb7f1963d47b2926d7281393fa25', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801247, ''),
('b812f0f9234b142396baa8aa2c3be39c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799622, ''),
('b81909ac8b93c2b1cf02d27ae111d26f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802623, ''),
('b83c51804656f5b411dbb2bb8bed810e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801323, ''),
('b852758acd03fbe67baf5d325741fb84', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803157, ''),
('b89111e57e1c46c58c32e5d6dd4c1afd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800797, ''),
('b8a9c061b5478962bb49883bac65427e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800315, ''),
('b8c7b098478494730439cd750fe13e2d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800637, ''),
('b8cce677a9d05e643649aec51bbe7bfb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802801, ''),
('b8dcb72652a073b7e4cd679d0b2378ee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801269, ''),
('b8f4b8286ff704743c75bcd16e677ba0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803083, ''),
('b8f50471a5c2948fa0cd7b3f465b8c3b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801045, ''),
('b908fe3860c2f2401bdb6aaaf2962a31', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800563, ''),
('b91861e674ec62062b0a9d3ad531d800', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801025, ''),
('b92650a745ec4af32928c20cd723086f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800121, ''),
('b92af0ccc455da1eb310b6318ff0c98d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803181, ''),
('b93aee3da1a94b244f65a5d8b809e0f1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799607, ''),
('b966bd86d3a866435e26cddbf4473c39', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800107, ''),
('b967f4ff258c6a9c2d8dfef178969473', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802339, ''),
('b97b8a573684b6f60a24e2dae4816ff9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803316, ''),
('b9a4bc77b74899a00ec4c3a945906dd5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800603, ''),
('b9ebf8d06390021fcc11eae502a75700', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801239, ''),
('b9f82b8613b07f222b3c89891fa99887', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802373, ''),
('ba01f8c739faedf2efb54ff568ecd751', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799714, ''),
('ba46a726fba9928370be234bc2311bea', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802261, ''),
('ba4df2d0be46014a821eb4581b953b74', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801605, ''),
('ba5e6a2448b3288e8956202ca5638e16', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802464, ''),
('ba6470eb9015e2b1dc9479f9e819e21f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799958, ''),
('ba75e5241e6eaef7deb166e3210a4980', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801431, ''),
('ba85f97d75754567b9ea636c19dc3a4b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799261, ''),
('bad5c5d6272247d48a9e9d27b0a1403e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799279, ''),
('bade1e85dea9e1f1726205973333bdab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802805, ''),
('baf5494dd0a8e96585928570f2d2766f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799207, ''),
('bb18473646e0aff3880a7af651723482', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801727, ''),
('bb2c2680f15889dbd72c4d6bc54bdc4c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803221, ''),
('bb48faca572d505335013994cf3a67b6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802329, ''),
('bb51e86b6cc5f07903634c48f0768f40', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800299, ''),
('bb5e38e4964a3380bad54fcd94bb8d80', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799553, ''),
('bb7e668222c4b62964ca658b3eb15d9c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803051, ''),
('bb993b555e67c3652250aac9fd9b537d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799789, ''),
('bba7013766776724b480d797aeddbaf1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803332, ''),
('bbac028942c55e16b0b11640c73362cc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802355, ''),
('bbb946b0eb0d1d10aaf2b7d64373bd44', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800309, ''),
('bbd3e6859cd0c04087589ab59fe5ceb0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803264, ''),
('bbf30be842001c15d6be830e26b078ce', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799488, ''),
('bc07b16632059a2b8437be3aa9456e18', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801857, ''),
('bc08dce0f0938a9945e14f04b7bf8703', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801387, ''),
('bc09437816da7cde14f02d106a13830f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800703, ''),
('bc09f601971d6c3110059764354707eb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801395, ''),
('bc10e35a0b33445093a19beb0609db00', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799958, ''),
('bc193853e775cf525ea52031e0f41b5a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800295, ''),
('bc1ad9f855ab2a99c272334648b8f39d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803125, ''),
('bc2d0c57838b0572bd4cdedaafd6ec48', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801733, ''),
('bc522fc335cfd188d0e511ada806d187', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801119, ''),
('bc52ee253ed58e21c0cfa25bda1ec491', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802142, ''),
('bc6e5b6d257c68ba71b043af136fbdc9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800791, ''),
('bc7733aadb7714e90dbdf0876931bc25', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802873, ''),
('bc774e89857b6f58d4fc8bf1a3aaeac2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799735, ''),
('bca76d7b0e391bc6582c2ef5cf0eb19a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800447, ''),
('bcd3d61b213b06523061f1c0aa8d62e6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802641, ''),
('bcd6cfc5d8650bac46fdee838b417866', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803342, ''),
('bcd842a88fcb11c2d16ee6ffb0f86b74', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802414, ''),
('bcf6013e6fcbc8d4f72570929093b8ab', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802589, ''),
('bd0808f9172e8ea20751a2d4c3588eaf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801445, ''),
('bd0a76b72dfa7af326dfc2cc4b0ded22', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801577, ''),
('bd3467e19a3a9b0cd06c80d82d786a2a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799469, ''),
('bd57aa0bf4b8f6cc4a79380969de4fdb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802377, ''),
('bd66171625aad3dc5214a1bf6d86dbbd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801573, ''),
('bd88996f0a25b7dbaaba231fcffc5c60', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802783, ''),
('bd8ad7f9302ada9986cd356f65c1a397', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802413, ''),
('bd9054a879171bdf54d1606a36bfca05', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800713, ''),
('bd9e4f1b1df351655c564768b009a617', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800237, ''),
('bde74b4732eb41787203ec971c9fb804', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803242, ''),
('bdee92b75cda8c210cbb76e8aeb1d71d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801089, ''),
('be1dc63a79cb50dd4a717091365f92a1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803111, ''),
('be551cfc6ea68c46cfc8055e764f3915', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803320, ''),
('be57396e7998224a50523a15f1548dd3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799941, ''),
('be7054423103f7494028fa71717be8ad', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800211, ''),
('bef631fb5e368805dfd3c5b18bb18498', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800023, ''),
('bf1060659fcc707687d19fb0fd26e536', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799931, ''),
('bf1eaa1ebc6f557053b69e9ce040e4d5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801181, ''),
('bf284cd1e64bc1b700c838c19e70ba3a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802577, ''),
('bf2eb77ec87ddb812aa90b01227251ca', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800531, ''),
('bf3b76f99aff5f1b7f33f1602900e2e6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799142, ''),
('bf3fbdfc8546b3c23ff9ae48990fe65a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800671, ''),
('bf702626fb25bd43bb92c9df7b8aee2d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799621, ''),
('bf7baeb072c342d3fdab99ad24f0e643', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801993, ''),
('bfa73b478ac491b4c4ac341290c9df87', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800573, ''),
('bfc5b21e423e18bace78f687753c37ac', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799953, ''),
('c05d759c4a188c2084677d0d38a00207', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800335, ''),
('c0889231a52918cede62f1f26a9c89ba', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800094, ''),
('c09a085b63bb728242ab923fa4e10929', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801847, ''),
('c0a1c16e18b335d727839664aa94acd2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800497, ''),
('c0b572f059d5e267f76f18a502627565', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799892, ''),
('c0bdeb43dd3686e54dfa3949a0949fd4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803171, ''),
('c0d2aa31d26d173e0ac57375f6a94f46', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801439, ''),
('c0d8c09a94bae7a53a0ae4e9fb99dc2d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802015, ''),
('c15a2e7c63d2ae80e35d4a40ca4fd8b1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799172, ''),
('c15c6a5fd69bae36592b4428525cdeff', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800393, ''),
('c1837dc0ba2593fbd6677337e0458fd5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802075, ''),
('c1a92821c728ffaa584de6ffa4b5f6af', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802547, ''),
('c1b560bd2f535d592e99b74dcdb617a6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802526, ''),
('c1b58ef1790ce4df0582459f249cfb75', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799981, ''),
('c1b79e1c4418ed92dac86798b99dfe20', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800183, ''),
('c1c5170eb42b94bc34b856523ff7f547', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800677, ''),
('c1df5db47871c9e5e2a6c3ad0dd79161', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802039, ''),
('c2177a08d5bfae08ea77e24b72bd4538', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803069, ''),
('c218a8012faea894ff9f78ebcc1c1287', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802438, ''),
('c21f38b0733fec5aa0d6f57c0cf97d21', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800553, ''),
('c226bf8ee7cbeef8417d5519daf70422', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803250, ''),
('c2730c5021f8e8ad2f7905b18cf0d821', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800803, ''),
('c28a4b8dfc8ab155df0362e5fe5e3756', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800511, ''),
('c2a216a90de91f16b5821b7fe0159f00', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801597, ''),
('c2a49ee1275acb63ea03a03f7c7fe613', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800249, ''),
('c2b9d2a6d45376e92690fc7650a79a1a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802907, ''),
('c3184df42e9fc3d1c32c63a8d59ede0e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799936, ''),
('c31d3d7fd9552fb4f276a332fcb67fdf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801859, ''),
('c3272b1230807a62c647963e47e762c8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800529, ''),
('c35a725a4dd7d9d2c19c0295acf5b3e8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800203, ''),
('c376f70c0252ad3df852453300cfa6c9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803343, ''),
('c3964f3e88aa8ee0133e1728e541678d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801067, ''),
('c3a54797b531c469952807d668265c13', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800241, ''),
('c3dd84646f038f71fcd2e311a379f744', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800649, ''),
('c3dee709e8309c246f1a3bed9221ad72', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799706, ''),
('c3ee5eb2ed0ca33030faf65f629b06b6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801885, ''),
('c40810714aead686316bf81caa6bddad', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801677, ''),
('c4416ce2a58fcf78a7d9bbccd379f27f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800063, ''),
('c47ccd08969810e91f0063f5fd55b0bf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801925, ''),
('c4881e6cf01d25b850ce969056ca0f96', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802677, ''),
('c4bdeb1c4701734c24f6d45266a4783f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800879, ''),
('c4c91a399185e747c424e201ecdd2dfa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799833, ''),
('c4ded9609398f3d77bb1de0cafa5b30d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800559, ''),
('c50b184f26b074195230dad4f7b6244b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799208, ''),
('c512bb0f55ed268a9f0afef56ac2f7f7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801583, ''),
('c52ced5ec7d3f18640da3216ea02e02b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799648, ''),
('c582acf958a2cfe22bfde88700f4c599', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799157, ''),
('c5863001f8ed2681844843a61f4ad490', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800933, ''),
('c587e81714299ac761cd3bd89644c0ad', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802565, ''),
('c596e78935db7ad98c97b467ef378547', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802243, ''),
('c5c5995eae749767e2c82952dd511253', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799723, ''),
('c5db505089f56e78086b741fa9834a79', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799439, '');
INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('c5f1dadbbd39ae58f158118e2025d9f0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802993, ''),
('c657ff92cfa5f523088b553572d65e46', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801469, ''),
('c68161583f0f6759b705998aaaeb9a8f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803252, ''),
('c6853711a40a2da97525dea66d07cbc5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802613, ''),
('c68904dfe722b6f6d0f4e01692a6025c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799238, ''),
('c69558d5a006696dedd260a918a58647', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800068, ''),
('c6a865efa7e8c008a3448d10e0d79194', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803232, ''),
('c6c952ec06b68cc19b6518ad8b5c77f1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801029, ''),
('c6e378d08f0a832112bf41f375fa152e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801301, ''),
('c6e74910e6587963a21537f9ebc678f2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800561, ''),
('c6f22f0f79023d37bc7f19db143513c5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800096, ''),
('c70f51f4b104d06749c37370424eb869', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801429, ''),
('c710dcddfcf26d37432f44aaf5a00fea', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801259, ''),
('c7346e318ae6a0e984feeb9e127b7a56', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802916, ''),
('c7474bcd851330b8549fe345c8b28214', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799183, ''),
('c7500cee129b03341ba2053a9fa9e965', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802598, ''),
('c77bcabb1ac6f228c89f6ceddf77ad14', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802898, ''),
('c786107f771ac496c8093cc3eef7bade', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800749, ''),
('c7a6ea616940b2e10371b061d4304dee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803213, ''),
('c7af0e64c50268801d0c9d379fcf1885', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802653, ''),
('c7fac94dc69ac365a22cc5234c18d9a1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803185, ''),
('c820ef2eeb642149152c1217956e4795', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801185, ''),
('c8236bdc26d725100fe653bf8a960d2e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801193, ''),
('c82b56cc55e50ecead7e5fe544822798', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801147, ''),
('c82e6a33a25f08b75e357cd45beb3b8d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802566, ''),
('c8342a8b08d4375058bdd7560243d0e2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803073, ''),
('c852935ab7910c49ecb38326d3e3ea36', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799569, ''),
('c863d22ff8801b239e6eb9d032e27dc9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800817, ''),
('c88e756b12fac4f475014a988465d9b6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800753, ''),
('c8a239f3bb98808f249a301265152e73', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801443, ''),
('c8c7686d3d838cb74ffaff616792b337', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801071, ''),
('c8e8d2bf45d42925819b1ec7340e177a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802649, ''),
('c8f6899cea19102eaead82cc50c3eebd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803243, ''),
('c90e6f1156a9d36e4f3d1df2cc7fd9ff', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800501, ''),
('c945045ebf17a4fb5a5dce2b3a706618', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800365, ''),
('c9534b12aa39de61f09f64e9b8676774', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801083, ''),
('c9ca9999101ee13b3aa50f7ef05f9867', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801844, ''),
('c9d47dc274d58f8672411d7f0897711e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800353, ''),
('c9ea99671d4af3c33247fed6ac36c460', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800441, ''),
('ca1357bf27ecb3f71030f8a917b6a2c1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799194, ''),
('ca1d0454bc630723d138d77aedaced5a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800527, ''),
('ca2e881e65e655966cc9652cb55e71e8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802143, ''),
('ca35f3c11ec9cbb36a04e57f8bcc21ee', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800593, ''),
('ca59905b4342cbcb725053b40414905a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799917, ''),
('ca5d1fecde6e3176a150d4bd852cfb28', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801297, ''),
('ca967595fb107086d2c84af36587a9b5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802487, ''),
('ca96c16a0a19f616cb874edc3e9a549f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802920, ''),
('cb0d56273d76f9224e23400ab8d4ca08', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799454, ''),
('cb40dbd1f560d2c38c33e72c28186d34', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800883, ''),
('cb556b05f6516be9c0d50535e9f9ed74', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801099, ''),
('cb5c101a574637c96ab446bc42e0413a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799670, ''),
('cb738a0929e941ca5e58c0c28bcfc125', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802589, ''),
('cb936d09b4690cf8ba972bccac9de6f4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802729, ''),
('cbc01554a8c5ecb8988886d31f79f8ad', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801593, ''),
('cbc0a9df7b58d2a7b8c41287f2d24d12', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802091, ''),
('cbcca2b0df60bd3231cfe48617d33b95', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802302, ''),
('cc047ad27761712169199327b5308b82', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802912, ''),
('cc07509c0d58ab8ceac6cb21d2a5b1b3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800109, ''),
('cc392a94e7a89836025632754124d3c1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800619, ''),
('cc5455e6cde78859dd8a0dd87ba7115e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803114, ''),
('cc5757b372e7ccdfabae1a7992b2643c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802341, ''),
('cc60aac590a794ebb867d4559933b204', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799642, ''),
('cc6b4a3e353cbd7ff38f038607fd36e7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799732, ''),
('cc985697a8d7f496517e605c334ad73d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800080, ''),
('ccb3fd65df3ad666654c7449b7e28a55', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799958, ''),
('ccdb1efb773ca59c6a0310f12d0e9eb3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801977, ''),
('ccf7e58970b79eec049541fe98cc9008', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800101, ''),
('ccfc39de68136e2cc21f71da95fcb0a4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801521, ''),
('ccfe4f89ffdda3b30aba2f46a56d9b74', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799717, ''),
('cd16579a854a64e472f33b1b090906cb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800549, ''),
('cd40311ce8183b945d5d9d36cbe9f067', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802671, ''),
('cd4b7668ee5aca4dfb2d969005f87463', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802380, ''),
('cd4f42110538eae35f18ef8d65093b37', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800395, ''),
('cd83e5571ab077d64143b5a7a77cf0e4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802673, ''),
('cd97fc48dd63ba6b7a6761cb97bee500', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802437, ''),
('cd99e42726d1c67644c5d79bc9140b36', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799552, ''),
('cda3c049ebe9cb70e097bb5b85b77e5d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802761, ''),
('cdb0e983af2a4ca94d4431715b000661', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802027, ''),
('cdb59d63766b2a257fe650e6724156ff', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802047, ''),
('cdbbe5bc1cc7c05aed5fd0b06010789f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801159, ''),
('cde672131cc0342754c68db3530ec9e8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801417, ''),
('cded376b3a03c9c232951443ef850180', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799958, ''),
('ce2d3041ceae7cab1a02708b47172821', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801145, ''),
('ce3b63491168ce2c1f894c01dd522ad6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800905, ''),
('ce616e5b13f86506a7a89305d1bd43cd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801835, ''),
('ce8706aa26f41a9c2ca6e0301bfd2f7f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802396, ''),
('ced618e2f3bc2927aa5ac885983abc2d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800839, ''),
('ced8663b3b603ca3893ec1864839bb77', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799459, ''),
('cf01f002baa8fda586438a88a3563a57', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802749, ''),
('cf4d509f07c947b3808c9741bbffc984', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800571, ''),
('cf7054082993123e5b0cfb9200e290b6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799840, ''),
('cf9b93c1c6cfdb5ff785839c3a86cee6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800103, ''),
('cfadab71333b22e188f01ea3a0ba2b6b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802703, ''),
('cfb95d7a7d1b93baac354c2d8f5277cf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802955, ''),
('cfbc1403376a57d799d3fc9814c47c6c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801331, ''),
('cfd5a5827e1c3da3b46225057d85c97a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803282, ''),
('cff3f08e919e4b6c6f0e911277af5bce', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799966, ''),
('d004136206ce4c24f6924facd9e84769', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799959, ''),
('d02e5876f7b3fd4bf22bba3235e0f019', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803203, ''),
('d02e8cb57e872ade27a576ffd49a7ea7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800263, ''),
('d0554eeb8f2f3c6522c6b03ef2a7b3ed', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799757, ''),
('d0646babd5b18ed86c9c9054d68a6321', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801709, ''),
('d06a0ddbaae7391ae7a59aebfa2ea844', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799755, ''),
('d072c479628f3c975ae8e29eaa7ba7f9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802549, ''),
('d09a46e8897e74a3914d2b385f93b558', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801741, ''),
('d0c156e76351ffc2897530e44db91e2a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802799, ''),
('d0c63151439c7f70c2a94ed5aa983b65', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801073, ''),
('d0d2383fcab67581d3eba57d8c068372', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799875, ''),
('d119a4dc4db4912224eab355a15fce49', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799635, ''),
('d147c523db64dba3cd8272597a84f7e8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801513, ''),
('d1504c8b04b6fe6df1766a247e9183bf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801389, ''),
('d188a8c3c5095f6c137070dec9814ed6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801299, ''),
('d190fec8c66709b7ad6e76cdb4f53d8d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800809, ''),
('d19a4307c44b49a81a9b0ebc9bbdf501', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799904, ''),
('d1c8b350bbc5649025b964b02383cf84', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800029, ''),
('d1d05e680fad96c0302d5939eeb362b4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802567, ''),
('d1fb0e63c12726ad2027ebd48254ede9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803328, ''),
('d2232d5919d265c63f614fb7fd758d02', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802675, ''),
('d2242a453502634cf6e6581926de3f03', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800799, ''),
('d256dd4e2ff5b8075058e16f7848fe69', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799886, ''),
('d26507cc4c4843a2af95424397027e6c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802927, ''),
('d2ae8f3ff30a28c759a5feb3e77827ed', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800719, ''),
('d2afc3d57ecad788e48be68a304e2116', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800019, ''),
('d2b89513620b859c75461c19a8bfe792', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803228, ''),
('d2f01ab173bd5069c8608a80f927a5a7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801973, ''),
('d2f4ad457511199a17e265e139892af2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802436, ''),
('d300611772a7eab16393ab5564d84964', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799784, ''),
('d341be7a9b865a35563fff478e13c5d1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801581, ''),
('d3723733c2ccb8173155fb8316a9e087', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799700, ''),
('d399aa12e8b7443dee7d0e2c1e661fe1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801537, ''),
('d3ba43ab5e28cee89c6150a44d3f0d0d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799956, ''),
('d3bc46aced95a3b1a2b6976570dfd6ea', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803121, ''),
('d3be4e2a4f04af4106ed996fc2bd1f9f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802590, ''),
('d3bfe63ac81c2dc257a0db7f3b745ae7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801601, ''),
('d3d103c84d974bc18b72490cc81fadaa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802767, ''),
('d3f17e3d52ff68a129669463c3d873c2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800411, ''),
('d409346def845ae789af17513e356e54', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801935, ''),
('d426b9088619a186c89ac2da099beab0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799134, ''),
('d48016012d3eeedb2924f90fc58cef22', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799621, ''),
('d48cd5541944f07204c838ddd60b1ca0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799163, ''),
('d48fd783a6556349ce653a3643e9b975', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802107, ''),
('d496298f4425c6c5e9878685e3670fa8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802065, ''),
('d498ac94a222871b1bcc4d28c88b8c46', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802005, ''),
('d4c91de850a97e9d3f29a1f96344d3ca', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802003, ''),
('d4cd82d15aae89865bc4f4a732fe6bc1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799476, ''),
('d4e224288ca4f531803e1bc8e3283f17', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799685, ''),
('d52cf0f38a1dce32187abc11a56867fd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799912, ''),
('d531ba017cc1d45dd4889802398e0c38', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802582, ''),
('d55f602a0b3b1b78c3e800ae79a5a804', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799809, ''),
('d5ab97e34bf5bb3f2b95621ecc37b65a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802043, ''),
('d5bd17893ccef1355e225fb94ac60aaa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803223, ''),
('d5cecaa6761bb27f495276d9974aa4c5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801199, ''),
('d5dff9cfec69e04bf79aa2b76b29884c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800771, ''),
('d5e4c3c13b28884f1e96a32518116ad6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799991, ''),
('d5f8086d702368b1207a34d727114a6a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800005, ''),
('d6138851f6a896318941cf25ec211ee1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801461, ''),
('d615a6cf08ca49abee63613cc3be6f77', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799946, ''),
('d64109022bf751e17a17f3022a068f01', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800191, ''),
('d68aa8c2815416d21df11f33dd7fb5d0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800317, ''),
('d6c1240e488400e26ba5e17bc688d7c0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802259, ''),
('d6c565dc1431f3938c09ba3899ba6a32', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799616, ''),
('d6db6f0ffc712deafec7caaec4f5cf0f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800123, ''),
('d6dd8b6af2aa642e592ebc1bb6d8a288', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799824, ''),
('d6de2f19f53d4806f44fa22443b91ee1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801897, ''),
('d6e0561ab782147b6ba065bfad630c53', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801065, ''),
('d72c4e96e6c1e81220c3e93d85067e0b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800587, ''),
('d72f22da9745674835154af13b0e795e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799960, ''),
('d73868d6f4b8dd236cff9e63826d57dd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802406, ''),
('d73d55bb97f86e435bd77028a713feb5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800853, ''),
('d76380c2511fc31edf4c23be69ad3f4d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799427, ''),
('d77a8294dff3eafd53ed181cbc44543e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801325, ''),
('d783d504f8cdf7ef8013f8f2c47b5da3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799938, ''),
('d79c5481bbb4a6b5c1fa148e22b974b5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801721, ''),
('d7a55535de1cfc25d2dedea577087f36', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801183, ''),
('d7a90d0d06860b46b8bb85a6dfc6fd9d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799213, ''),
('d7ce357d4c0f73b46d41942cc2aeb262', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799671, ''),
('d7eb24467205797a1c2e54f0fc99db3a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802425, ''),
('d82ae8ba77d331549793835d2b4b981a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800555, ''),
('d8616a59723ad96c94b23bf688efed75', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801011, ''),
('d8705c8390e1533a00adf6cc82d671fd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799533, ''),
('d88df92b01b3e2ccd5f9b05627b5dbb3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800377, ''),
('d8972080d121c39f6c443ba2ab6d8dfd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800701, ''),
('d8b3111939980530e9fecd5f49e6942f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799197, ''),
('d8de4cdbbbae7366abd2e8ef788bdd91', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799237, ''),
('d9330694e43df5fdd9f1bd588107b161', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799814, ''),
('d96b0f652c268459f8a84d424dd5a6b6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802533, ''),
('d9a2be81c4cf98e3c69167ebd9998cef', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800227, ''),
('da1858a7d799618db73c629740d09af9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799787, ''),
('da38a285c19d922999586661b1007801', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799948, ''),
('da50dbe77d3f9028f57e07a6f9f4e1e8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802363, ''),
('da560b1ddeae9cab5ae59f04217a7111', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799888, ''),
('da619f34b9959180703e6496e1b6f01f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802317, ''),
('da737c1ea8340f9c9158fa8fe95092b7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803155, ''),
('da99b49ec6ffc67daf5240188913b303', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803097, ''),
('daa46440a761ad70aad80aab63475d4b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799253, ''),
('daaec787524666542c04d29ecc314203', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801915, ''),
('dac4195381107c594f708b05d836db3b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803343, ''),
('dad2410334b7997482268edc8051b746', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801255, ''),
('db0b8dacbd242e706e881a3992c3ee8d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801453, ''),
('db82adda48bc37096fbc323bd04779f1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802661, ''),
('dbc1cf145cd5f1b6b269be92d98799d4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799448, ''),
('dbd3cd4dcf80c85d4096ad5df9741569', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799561, ''),
('dbfc2abc67535883951aeb15bcf2229e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803055, ''),
('dc11e6445cbb9ee8cc4c8738cbf37fae', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799889, ''),
('dc52011ad6f1aabcfbad132986a0bb20', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800499, ''),
('dc55ff04116bb9dea977abfb6dfbeb96', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800605, ''),
('dc7309f62f51cfb5c86f6a5f69f86126', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799795, ''),
('dcde29dc6d0a43516364d30a7fa4e986', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799525, ''),
('dd07685e3ed8849b514d30a58c77499d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802293, ''),
('dd1b87c658b3c511469647bc1dcf5391', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802657, ''),
('dd30951eda3b57c4a8c06d284a137f22', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803233, ''),
('dd855389f1588232ce272e39b8c742d2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799791, ''),
('dd92816bac0363267c597e19b73ba5fb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803278, ''),
('ddb40708e58b5dbce1d1c1d9fdb0d048', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803029, ''),
('ddddbfd919bf00d2cd96c362c9547e25', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803103, ''),
('dde74e859418d8dbf7b8d937ae3448a0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799793, ''),
('de2ef2b19c2216f05f0fae3600b5306a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803107, ''),
('de36c092b0c3573e57801b475decc52b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802629, ''),
('de50ee77a7180932040267557ff36760', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800271, ''),
('de703135c1709adc748bf40eb8acfd95', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799269, ''),
('de706b0a079046a3ac2eb9f8b70d9d76', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800177, ''),
('de8733e4ada749b750e1064e8dcd91e9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799958, ''),
('de94383ed364f1f2da79f29f586288e5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801535, ''),
('deabbf59ce8da71f26af260088ea268c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800277, ''),
('dec76ac22e56bb806598875416c4ca30', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802644, ''),
('ded07fd25155d3ff6000281aa3bc4a72', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803227, ''),
('ded9628860f2f456936724ecc605ea92', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801049, ''),
('deee0ba08bbabe83b1fdbf91385f3ddb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801985, ''),
('df0051258934cffeed2a66cd1858e9b8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799598, ''),
('df1c13b7e8330e4d71ce5f2de975b7e0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802951, ''),
('df1d0e26e02a26620652c178c46821a4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800267, ''),
('df37b0c219bb5091806cbf423e0531c6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801705, ''),
('df3c6274aae01df9807c83f61000ec74', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802609, ''),
('df5a66bd34f48152f2099eeb4a9fa915', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801861, ''),
('df82a636514f3b8dfb993caa798c2111', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801027, ''),
('dfe228db88db7e13d81f9b814a0ae143', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800153, ''),
('e014ad6548ebf1e393d69329d77db18e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803234, ''),
('e02dba599ee149eeb81f014b5745572f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800329, ''),
('e02eee3d9951ce2a0e3028bcdefc3f3b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800090, ''),
('e03345c5452eac48aeb4ebdce874f85d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803071, ''),
('e050cfce6598279231a1503af21a0d2f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802637, ''),
('e060d2ccc1f7a7f1a7e1e162cc39d0d1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799115, ''),
('e07063c434413d1c5db96503c8947f09', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802799, ''),
('e0866ff17120759414a5eb82f3603f1a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799148, ''),
('e0aa38a67e66734aaac0bdbd22d4f9e0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802991, ''),
('e0d60e9fd191b978649d0e0ef217ed6e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802571, ''),
('e0ee9c154640e0bc945378ce138f4985', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802845, ''),
('e1002bed00241f8a5ee8a7eb19dd54cc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799439, ''),
('e151a8445108947f208c26171434aeec', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800705, ''),
('e16f7b5333a507ca8ed08c50966567b6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801281, ''),
('e18a092ab07faa65268cf1908ab8ced5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799848, ''),
('e18e741b0132a192ef93fda320ba0d70', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800707, ''),
('e1a8721dcb401834555ade2809599bec', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799727, ''),
('e1aef4b3ff96829c67e166be83061389', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799763, ''),
('e1fb0c8a6188250c7060d96adf5d7412', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800195, ''),
('e2270338b7dd910c83b34ca4dfc47c86', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799715, ''),
('e268f3c8da53813137d15d9cdb0160d8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802575, ''),
('e278c47a35be30624233db750517b5dd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801279, ''),
('e28f1e5a7fb1e2746990bf9f379c842d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800371, ''),
('e2b44e640030ce548d9cef96a170d201', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802733, ''),
('e2c51d42337a91d8b6377d92a954051f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801949, ''),
('e2c91392454716277320d18d7827e122', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800239, ''),
('e2e10d1d338c06050e251a6c123ed32e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803244, ''),
('e2eef11c1ebf0f99b489162a64bf18bb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799274, ''),
('e32cb96d9da5389d52f5d1078efce685', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801295, ''),
('e330738ed7f9ffbdbba0ec2bdfe66a93', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799126, ''),
('e35ca81c2c58c96afe563606e1d4ba3c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801551, ''),
('e361258443547023891a4758c4446e6e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800343, ''),
('e3783affdb5878b85ca2dda31283dfe5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802300, ''),
('e3d4e54a2ba2256680560ce897a3bb27', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802685, ''),
('e3d64aec088522d2b5025c0161fd85ff', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802425, ''),
('e3fd132147db0cf698c5daf367486766', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799243, ''),
('e4015ef01dd7834d4b8db106dd123ef6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802395, ''),
('e41f39860cfabbc8a74f67d34508c43b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800055, ''),
('e43fec498713716866d2013ac925ca9d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802801, ''),
('e4d70c510995e98791faeb0af03cb988', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799155, ''),
('e4e395bb154d1df75de8b77c69dfdf86', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803207, ''),
('e4efccedc492f654aefd663d3b3c1887', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802257, ''),
('e5288d5a808edeab1c9f960674c3f4dc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801493, ''),
('e54c9dd97d0d40e918550875c6568787', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802462, ''),
('e57762691746c1d88b4e2062c18791a0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801101, ''),
('e583eff10638baf86800541a5e37a670', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800205, ''),
('e5893c7f10f00c7cbedbbb72665f15ce', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802697, ''),
('e5927d3609cf123337474ce78822bf41', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802263, ''),
('e5962b67f4c0b08b86515a8e1e1b4708', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800845, ''),
('e5df2feca8c2e877ec010def3a77ace4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802769, ''),
('e5e14e7563117b64a3baac412b5efa19', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799648, ''),
('e650433b40f6db6ebf6887ba8587ab43', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799626, ''),
('e6edfda0ee6f74ece6a5718638a31230', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800186, ''),
('e7015a5a5c8e712f4b3bc299a677d3a9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801289, ''),
('e70ce045a9b5f645d893a8192b8e2ca7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803343, ''),
('e73b729a6c9fbd83f1f440464ba6fc5d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802009, ''),
('e763425cc8832af3053d3a912eb7544c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800070, ''),
('e7678c6c9b448372c9add495cad4a3f4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800657, ''),
('e772345805a6c378713d3444aee0e3d2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800683, ''),
('e7b594d6e622e35ed69e5549d103400d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801965, ''),
('e7b8edd7d0b260f5527645f251f43644', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800639, ''),
('e7c48b54112e2a8be5d8ff2e51428511', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802991, ''),
('e7c62b11cb835a0456bb8e59e866c244', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802295, ''),
('e7ed5c0617e944215ed14e16d3523d1d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802941, ''),
('e80a0962fff07535be07ba5b57e4f8a9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802588, ''),
('e83e89ffb2d04f052c4307c3bb51608b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801945, ''),
('e856f00523ad92aa31a28994cf4b3bd9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802375, ''),
('e889da3b088b2a2ff08e2ab82b078dd9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800207, ''),
('e8ab84f517c2ce9025eb5e3cad7e026b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800341, ''),
('e8b074c3aa35f03de35d16f198346af4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800269, ''),
('e8ba781b709bd2909823d87656ad4953', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802143, ''),
('e8e8d5c40bcda3acac730f9959d01771', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800009, ''),
('e8eea2264c391f9dbe1942d6f1312c45', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801889, ''),
('e900d15931b672c4b2c717c2d81a8abf', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800025, ''),
('e914a940ceff8119e60f27f29652ee97', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802747, ''),
('e91aec2c9912718319fee697afcee6e9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802939, ''),
('e922c6fc6563d2e9524f2a8a269ae0fc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801715, ''),
('e93476bb7359b2888a9ca00e076000ae', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799771, ''),
('e95b6abc2fb57440cf327a6a842f0a93', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800035, ''),
('e95ecd981be430b68e103816fd5b9cb4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802713, ''),
('e9673c7a443c50890fac84b3f35244a5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802361, ''),
('e9808734101c5c52274a8286c8402553', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801527, ''),
('e98fef3f9d87d79ef2106e60ee02b33b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802141, ''),
('e9bdd2fd1bf1caf375e50a676ad42d4a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802977, ''),
('e9c6b1652de0ed85ae70f9664442e717', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801923, ''),
('e9f34bd132d88af008cc97d1e438a04b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800137, ''),
('ea3ebd1fee5174ebc985093464a6f7bd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799912, ''),
('ea9505bca7e7932232626f6f6ed2d83c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802247, ''),
('ea98366bb614009d03bfba006f762625', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802383, ''),
('eac22bd63ef68ff458d3db3688d4fa92', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802556, ''),
('ead2a46ecbed08a7d1ccd52f337930a7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799721, ''),
('eae7e4a8b15a150dc25356208237b437', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801397, ''),
('eb068f4b15c3fc1e81db98fadd9de29f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802569, ''),
('eb59e3a7f6eca3804ef1a24be711a18b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803336, ''),
('eb8f78ba8f09b555ad9c80043a1e013d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802755, ''),
('ebc39c7838e06c44034222f61734f497', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800132, ''),
('ebe144af9934afba103642a5a611a201', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801315, ''),
('ec05f601b56906ddb3ac771309c444bc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800617, ''),
('ec0f13fb05d478d53838db4c93d04232', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799474, ''),
('ec34cb5bf32905ff50d66c416acf73c6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799633, ''),
('ec657baf6df77a4a83edbd53fbc20a6e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802789, ''),
('ec72295e25823043d44274ebfedcff9c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801613, ''),
('ec89ad8acd0c052cbd354516b34ec1de', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802147, ''),
('ec8e86166fff6b78e4844a38e6b7d2a4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803049, ''),
('ec959aa084935512077dc172e8c2a3f2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801243, ''),
('ecd07566ab19cc44156ec5af366114c6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801263, ''),
('ecd93c93fd7e207f203145dec8eb347f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800375, ''),
('ece573eefc234c18395347193a141864', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802874, ''),
('ed48876ae3c47efe2d932a92ae135e2a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802307, ''),
('ed4c9d163e3517ff1c4b7178fb8c7f4f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802435, ''),
('ed6e3f5283d46ca6744e3cbd67eacff4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800835, ''),
('ed71bafbc9d39251e3138418f894b767', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803280, ''),
('ed7af721abf73f22c33c426b28e8e308', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799842, ''),
('ee031840b045b0c33d69c3df3bf477bd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801505, ''),
('ee37e528df16ffdd4de35ea3dafe7576', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802923, ''),
('ee7bb28b7e8fcf0799d722843725605c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802471, ''),
('ee7ecd950263bc8b6ca54d194bb13d77', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802449, ''),
('eebc5a76de539c6bf87098e13326e55e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802017, ''),
('eecc8f37df57c71f52d99fe315dfc380', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800745, ''),
('eee1fb180be9424f588552d1fabe7ac9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799985, ''),
('ef134e591cf562ce994c181d42f2595d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800655, ''),
('ef224c24323ba48bcdbf31daab900e4b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799662, ''),
('ef32455a7adc7a1e4da8b918f1288396', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802739, ''),
('ef43b139b34b790fd799c9de10c2078a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802517, ''),
('ef4e5084ef596bea6d49196b3b3ec683', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803135, ''),
('ef5908bff2f906f6ee46f45b5b82a3ba', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799167, ''),
('ef7ec325a96c18b98823d131eb3e832e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800509, ''),
('ef85a01abb4ffa7a855047e0506c4c87', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800869, ''),
('ef8bcf3e4ba6e7b2bcce66c79038b945', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800897, ''),
('ef96e30844c63c57f629966a91e2edd4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491798481, ''),
('efe9e6f47369880dc80ed6c798459a6b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801035, ''),
('f026f5e6fec57dc25b35670e0f63e74a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803330, ''),
('f04fdc630dc4e24b7793ca0472cd240c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803087, ''),
('f07ce38564a3a36e488cd5df7cb98a17', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802663, ''),
('f0ab9bade04d5ef695cab1669b6f0702', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802963, ''),
('f0becd5062d3cc9d50f5fe206ae30f94', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801571, ''),
('f0d9e0228c175d219026b787ba85e660', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802701, ''),
('f0e47068fcc990cd41d1d34e1ae2b3e3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799560, ''),
('f0e538de2fa4c0fa5667034787f58a6e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799650, ''),
('f0f9d4f87705e89a2de04469f8606f20', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799850, ''),
('f1017aef79e8b1d80ee473c46a370b09', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802428, ''),
('f113b67a9ad002e47f55d29c82d5ed79', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801947, ''),
('f114138ba2411b00cc40047b48fdfbce', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799678, ''),
('f132e8cb6ea376e3f639ad098379e23a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801135, ''),
('f180bea5d9f95e42371095820e91034f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799641, ''),
('f19578d4a454ad7e046ff79b4287b7ba', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803081, ''),
('f1b676a196194ebd2b4baee27a19daa5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802705, ''),
('f1e0f082dfbc0321f338d34f558888fd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800401, ''),
('f224d675151a3117163ea6002a4c35af', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799505, ''),
('f23d2fe8f091dec55c378651c68dc139', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800345, ''),
('f2550073436d1791d1a1edf64ae622a8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800457, ''),
('f263e5547c4027e0013c36024ec907cc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802927, ''),
('f26a77656cd025339aee0e7dc9f21033', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802805, ''),
('f2bcb12bf66eaa8271d349af50deb60d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799979, ''),
('f2fbf410937002ba3eb4bb90eb76035b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799680, ''),
('f31ff1ae9b78889bb158bf4ea1145e29', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801823, ''),
('f3663e6ecfc8c86dd0f7cc3a315d0294', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800279, ''),
('f3fa84c8701382982e6afa09faccb2f1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799755, ''),
('f3fb024c53e3c1a9f8820c858be1a59c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800435, ''),
('f46399b945b356de7e50d7e680e1a3b6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802305, ''),
('f476a5659e0a0d3a095c0517caf2a8b2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801529, ''),
('f47bb11c67714b4eba86bc83cbecd725', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801143, ''),
('f4cb1e137c60a45a0ad792974483b94c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803105, ''),
('f4dc600c110d992636079ba38c77ea00', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799280, ''),
('f4e3ba7b45f824ebbb9f5ff7c947434f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801999, ''),
('f4e3ff0f94d3754a4050a19d41954827', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801567, ''),
('f51ba6f206f2192b23f4f0f39548a0c1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801683, ''),
('f5759c39a8418f399b1e5317c6f7c6be', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801267, ''),
('f5adf75381de40856d170953eebf0625', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800415, ''),
('f5cba41eef3a6bfd1991e86289aaa568', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802883, ''),
('f5d4ee204301b23b46c0ff155c6d27bc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800147, ''),
('f5ee21e94619f78cef7d068c18c61a7a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802395, ''),
('f5fc490bfc7ad36a42c163494d728883', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800935, ''),
('f63221d0b521bfff965ea9bd907d0fbd', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801909, ''),
('f63dc1e72f46ae6d28fa448423ecc26a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800919, ''),
('f6603c80086ba3e8191df0774d17962e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799250, ''),
('f67296e2c2ce590fcc4d0cea8cf3713b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799959, ''),
('f684e3e46daeddbf4802e20a7764d847', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800887, ''),
('f6945f07a27f992fc4e9a5a7af1d3d9a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800172, ''),
('f6c869f383680e70fceb24e92fc8653b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800867, ''),
('f70347cbfa9de9124e53b1d457711599', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802967, ''),
('f71945f5bbaf47c4d33749751c837528', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802565, ''),
('f723164fd8aed75a41c9ccb493f3b1a4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800645, ''),
('f72578cd78c9fad86732683d77557bb1', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803011, ''),
('f733a331759bf453af88a5ce1594bc77', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802135, ''),
('f7a5aa8fa8d18b5095d0bcfe2bee4996', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803238, ''),
('f7afdfec5df4ed8621bcadb6067bd752', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802928, ''),
('f7b0b274a6ead453d3fb4636e91d097c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800687, ''),
('f7c50d13de29f15b9754d434f50c8b61', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799441, ''),
('f7ce47bc5c4cb2606b26f53c83a67525', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799147, '');
INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('f7d9f5f5dfbfe34eab7e2cc05cf56569', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799139, ''),
('f7db0e096cc4d22fe0c2f8f1b7837916', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800191, ''),
('f7df7c81bd8e8bf884c8585881a5f710', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799273, ''),
('f7e40b84778c4020528ee5501a00281a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799803, ''),
('f86ec8181d61f28488a9e3a2fa3778f8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799151, ''),
('f8864dd23edf9c513144d1813a6bae00', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799611, ''),
('f89147ae2bfd071ffdfd001b0107de1c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799219, ''),
('f8a023e5eab127d7eadfe8362395781f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803290, ''),
('f8a4de304672577361f94e3ffacec21f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802665, ''),
('f8be944480627525eab759db29aade2e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802097, ''),
('f8e7d392ceb61226a382ee3014bea61d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801891, ''),
('f8f10a18bb617f068a44f6527b35b6c0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801357, ''),
('f90f10b04ceda66077ab502c48fa0615', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802727, ''),
('f94af260321e6153cac4ee10161f1bf8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801865, ''),
('f97841af4c016d60ea6c696d7f717add', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801735, ''),
('f9a4c4154085b9d76579ddf13b750bb6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800541, ''),
('f9acd71522607776eb95e3a83aa45e14', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799963, ''),
('f9b1eac0c9a04e1861a78bf880b11e05', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802854, ''),
('f9cfd5128c5c8c1c39f89eaac1c5d710', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799971, ''),
('f9d94902522c7c3d89824b5fcbd7f8ac', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802651, ''),
('f9e2ca827c0fb1ac2d9508ea808a6d98', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800815, ''),
('f9e60c3b513a02ff55481cb8b3a48d6f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799870, ''),
('fa4249d96a39e18e4c9698a183104168', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802341, ''),
('fa714a2d5381fcf234bf117daf9be8c0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800231, ''),
('fab18d42a2d2bbe6a729a49d5f86e4f5', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803294, ''),
('faf2be87b635ced919b806c831fd84be', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802771, ''),
('fb10d951065902404bd0a44ad0ec3c4a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803013, ''),
('fb226f0b2df6b446a22d5bbc59e1ce9d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491803114, ''),
('fb488e6fab5ee8663755484bc8c51f24', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800693, ''),
('fb50bbd8d9fa1c470283399c01323d80', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802587, ''),
('fb52e45ce89b41de5d495cf5dddf76b4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799882, ''),
('fb7a08077ea014b89780bf44ea1f320b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802484, ''),
('fb82e3ad1401e7c9d423009034feacd3', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802367, ''),
('fba9bdc5b4f8cc7e24032e3ea997184b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799161, ''),
('fbb9e6f739ffe25460fdefbebebd1119', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800048, ''),
('fbcbc7abb51620fc2bc3049dd963bf6a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799668, ''),
('fbe28ba7bef9095e0e79a65976a3c667', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801543, ''),
('fc2cd62f4ae753b794268e5e62cce3c0', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802904, ''),
('fc2dfac56fecfb96c9e4983c2d1a3c52', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800685, ''),
('fc4bb46354422027f7abe8ec00763a9f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801233, ''),
('fc757163b404a0c4a155124f46473239', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799442, ''),
('fc7bcbf31bf7da2c57c6c2e2e1013b3e', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491798481, ''),
('fc8383438ac8ac23c128c2374d8cd196', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802387, ''),
('fcd6bde5c0aa33785ceca0918c5945cb', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801203, ''),
('fcdb031b7aac093643591d8ac2f9dc69', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799480, ''),
('fcf7c350b4a7e42507cb3cfa93b6c46f', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802237, ''),
('fd35becc50021d83142042676b0eb4aa', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802855, ''),
('fd9223342b2ed3da65206aee18958a1c', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800297, ''),
('fd98617b0928617b21dee2a0dbbe9cd7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799226, ''),
('fdb55e08b8ed17b617aee3bbd88390e6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799582, ''),
('fdd522db8296b4c3556c45fe97a4872b', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802452, ''),
('fdd930638728e8da083b5a8a0c5cb7f6', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800515, ''),
('fde3be679fcb1050e6ef8993d0e03cd4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799522, ''),
('fe0b3175c94280a73ca4baa7e5829796', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799940, ''),
('fe2d62885b531bf550e942791f009a4a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802478, ''),
('fe4fa62f75772bd0a728fbbbb02329d4', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799171, ''),
('fe5928ab920c393ecfcaea552b52d4f8', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802753, ''),
('fe5d50f4de54663da6702a55701fbbd7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491802432, ''),
('fe99842773ab3a218c6be36ca7cd5089', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801047, ''),
('fe9ae226767dd6b97b241783dc49b34d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801507, ''),
('febf946da69c59e52059fa02b4e3a69a', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800767, ''),
('fec3dfc1b6e6dbdc0339eea7a56eeebc', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800691, ''),
('ff19564ba1980708f9d5b64637c5a1e7', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800419, ''),
('ff290db2fab52a1233febae224555c11', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799815, ''),
('ff4f48ef345cb571cb359ebab8cf2ab9', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801219, ''),
('ff56e5991537d0b8f7f9182ec7b377c2', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800513, ''),
('ff9fe40fc3dd3e581c13512079f53285', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491801217, ''),
('ffa437dfc67167a4db8324b10a420249', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491800829, ''),
('ffbf778b4c9a28644fd25e52a196d56d', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799845, ''),
('ffc4bc6c451af7a29220ef4fc2dc7fef', '0.0.0.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1491799125, '');

-- --------------------------------------------------------

--
-- Table structure for table `site_options`
--

CREATE TABLE `site_options` (
  `opt_id` int(11) NOT NULL,
  `opt_name` varchar(256) NOT NULL,
  `opt_slug` varchar(100) NOT NULL,
  `opt_type` enum('text','image') NOT NULL DEFAULT 'text',
  `opt_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_options`
--

INSERT INTO `site_options` (`opt_id`, `opt_name`, `opt_slug`, `opt_type`, `opt_value`) VALUES
(1, 'Site Name', 'site-name', 'text', ''),
(2, 'Facebook Title', 'facebook-title', 'text', 'SugarFlame'),
(3, 'Facebook Description', 'facebook-description', 'text', 'SugarFlame'),
(4, 'Facebook Image (1200px x 630px)', 'facebook-image', 'image', './uploads/site_options/facebook-image.png');

-- --------------------------------------------------------

--
-- Table structure for table `source`
--

CREATE TABLE `source` (
  `src_id` int(11) NOT NULL,
  `src_name` varchar(100) NOT NULL,
  `src_status` enum('active','archived') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `source`
--

INSERT INTO `source` (`src_id`, `src_name`, `src_status`) VALUES
(1, 'Facebook', 'active'),
(2, 'Google', 'active'),
(3, 'Youtube', 'active'),
(4, 'Instagram', 'active'),
(5, 'Word of Mouth', 'active'),
(6, 'Others', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE `subscription` (
  `sub_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `sub_status` enum('pending','paid') NOT NULL DEFAULT 'pending',
  `sub_start_date` datetime NOT NULL,
  `sub_end_date` datetime NOT NULL,
  `sub_date_created` datetime NOT NULL,
  `sub_date_modified` datetime NOT NULL,
  `sub_amount` float NOT NULL,
  `sub_type` enum('monthly','yearly') NOT NULL,
  `sub_transaction_id` varchar(100) NOT NULL,
  `vch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `usr_id` int(11) NOT NULL,
  `usr_type` enum('flame','sugar') NOT NULL,
  `usr_first_name` varchar(100) NOT NULL,
  `usr_last_name` varchar(100) NOT NULL,
  `usr_screen_name` varchar(100) NOT NULL,
  `usr_email` varchar(100) NOT NULL,
  `usr_password` varchar(100) NOT NULL,
  `usr_birthmonth` int(11) NOT NULL,
  `usr_birthdate` int(11) NOT NULL,
  `usr_birthyear` int(11) NOT NULL,
  `usr_birthday` date NOT NULL,
  `cty_id` int(11) NOT NULL,
  `usr_status` enum('pending','approved','rejected','deactivated','suspended') NOT NULL DEFAULT 'pending',
  `usr_gender` enum('male','female','trans male','trans female') NOT NULL,
  `usr_interested` enum('male','female','both') NOT NULL,
  `usr_interested_country` int(11) NOT NULL,
  `usr_interested_age_from` int(11) NOT NULL,
  `usr_interested_age_to` int(11) NOT NULL,
  `usr_description` text CHARACTER SET utf8mb4,
  `usr_signup_step` enum('initial','photo','info','preference','done') NOT NULL,
  `usr_verified` enum('yes','no') NOT NULL DEFAULT 'no',
  `usr_password_reset` varchar(100) NOT NULL,
  `usr_password_reset_date` datetime NOT NULL,
  `usr_created_date` datetime NOT NULL,
  `usr_approved_date` datetime NOT NULL,
  `usr_modified_date` datetime NOT NULL,
  `usr_last_login` datetime DEFAULT NULL,
  `usr_token` varchar(100) NOT NULL,
  `usr_token_id` varchar(100) NOT NULL COMMENT 'will be used for profile viewing',
  `img_id` int(11) DEFAULT NULL,
  `src_id` int(11) NOT NULL,
  `usr_location` text NOT NULL,
  `ulc_id` int(11) NOT NULL,
  `cnt_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_inactive`
--

CREATE TABLE `user_inactive` (
  `usr_id` int(11) NOT NULL,
  `usi_id` int(11) NOT NULL,
  `usi_reason` text NOT NULL,
  `usi_comment` text,
  `usi_type` enum('deactivate','delete') NOT NULL,
  `usi_status` enum('active','archived') NOT NULL,
  `usi_created_date` datetime NOT NULL,
  `usi_modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_like`
--

CREATE TABLE `user_like` (
  `usl_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `usl_user` int(11) NOT NULL,
  `usl_status` enum('active','inactive') NOT NULL,
  `usl_date_created` datetime NOT NULL,
  `usl_date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_location`
--

CREATE TABLE `user_location` (
  `ulc_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `ulc_city` varchar(200) NOT NULL,
  `ulc_state` varchar(200) NOT NULL,
  `ulc_country` varchar(200) NOT NULL,
  `ulc_lat` float NOT NULL,
  `ulc_lng` float NOT NULL,
  `ulc_date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `view`
--

CREATE TABLE `view` (
  `vie_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `vie_user` int(11) NOT NULL,
  `vie_date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `vch_id` int(11) NOT NULL,
  `vch_code` varchar(100) NOT NULL,
  `vch_status` enum('active','archived') NOT NULL,
  `vch_value` int(11) NOT NULL COMMENT 'in terms of days',
  `vch_valid_until` datetime NOT NULL,
  `vch_date_created` datetime NOT NULL,
  `vch_date_modified` datetime NOT NULL,
  `vch_created_by` int(11) NOT NULL,
  `vch_modified_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `voucher_xref`
--

CREATE TABLE `voucher_xref` (
  `vcx_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `vch_id` int(11) NOT NULL,
  `vcx_date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`acc_id`),
  ADD UNIQUE KEY `username` (`acc_username`);

--
-- Indexes for table `block`
--
ALTER TABLE `block`
  ADD PRIMARY KEY (`blo_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`cty_id`);

--
-- Indexes for table `conversation`
--
ALTER TABLE `conversation`
  ADD PRIMARY KEY (`cnv_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`cnt_id`);

--
-- Indexes for table `flag`
--
ALTER TABLE `flag`
  ADD PRIMARY KEY (`flg_id`);

--
-- Indexes for table `follow`
--
ALTER TABLE `follow`
  ADD PRIMARY KEY (`fol_id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`img_id`),
  ADD KEY `usr_id` (`usr_id`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`inf_id`);

--
-- Indexes for table `invite`
--
ALTER TABLE `invite`
  ADD PRIMARY KEY (`inv_id`);

--
-- Indexes for table `like`
--
ALTER TABLE `like`
  ADD PRIMARY KEY (`lke_id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`not_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`pag_id`),
  ADD UNIQUE KEY `slug` (`pag_slug`);

--
-- Indexes for table `page_category`
--
ALTER TABLE `page_category`
  ADD PRIMARY KEY (`pct_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `post_like`
--
ALTER TABLE `post_like`
  ADD PRIMARY KEY (`pol_id`);

--
-- Indexes for table `preference`
--
ALTER TABLE `preference`
  ADD PRIMARY KEY (`prf_id`);

--
-- Indexes for table `privacy`
--
ALTER TABLE `privacy`
  ADD PRIMARY KEY (`pri_id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`que_id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`reg_id`);

--
-- Indexes for table `search`
--
ALTER TABLE `search`
  ADD PRIMARY KEY (`sch_id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `site_options`
--
ALTER TABLE `site_options`
  ADD PRIMARY KEY (`opt_id`);

--
-- Indexes for table `source`
--
ALTER TABLE `source`
  ADD PRIMARY KEY (`src_id`);

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`usr_id`),
  ADD KEY `usr_username` (`usr_screen_name`),
  ADD KEY `cty_id` (`cty_id`),
  ADD KEY `usr_preference` (`usr_interested`,`usr_gender`),
  ADD KEY `usr_birthdate` (`usr_birthdate`),
  ADD KEY `usr_status` (`usr_status`);

--
-- Indexes for table `user_inactive`
--
ALTER TABLE `user_inactive`
  ADD PRIMARY KEY (`usi_id`);

--
-- Indexes for table `user_like`
--
ALTER TABLE `user_like`
  ADD PRIMARY KEY (`usl_id`);

--
-- Indexes for table `user_location`
--
ALTER TABLE `user_location`
  ADD PRIMARY KEY (`ulc_id`);

--
-- Indexes for table `view`
--
ALTER TABLE `view`
  ADD PRIMARY KEY (`vie_id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`vch_id`);

--
-- Indexes for table `voucher_xref`
--
ALTER TABLE `voucher_xref`
  ADD PRIMARY KEY (`vcx_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `acc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `block`
--
ALTER TABLE `block`
  MODIFY `blo_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `cty_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `conversation`
--
ALTER TABLE `conversation`
  MODIFY `cnv_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `cnt_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `flag`
--
ALTER TABLE `flag`
  MODIFY `flg_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `follow`
--
ALTER TABLE `follow`
  MODIFY `fol_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `inf_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invite`
--
ALTER TABLE `invite`
  MODIFY `inv_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `like`
--
ALTER TABLE `like`
  MODIFY `lke_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `msg_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `not_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `pag_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `page_category`
--
ALTER TABLE `page_category`
  MODIFY `pct_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `pos_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `post_like`
--
ALTER TABLE `post_like`
  MODIFY `pol_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `preference`
--
ALTER TABLE `preference`
  MODIFY `prf_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `privacy`
--
ALTER TABLE `privacy`
  MODIFY `pri_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `que_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `reg_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `search`
--
ALTER TABLE `search`
  MODIFY `sch_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_options`
--
ALTER TABLE `site_options`
  MODIFY `opt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `source`
--
ALTER TABLE `source`
  MODIFY `src_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `sub_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `usr_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_inactive`
--
ALTER TABLE `user_inactive`
  MODIFY `usi_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_like`
--
ALTER TABLE `user_like`
  MODIFY `usl_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_location`
--
ALTER TABLE `user_location`
  MODIFY `ulc_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `view`
--
ALTER TABLE `view`
  MODIFY `vie_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `vch_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `voucher_xref`
--
ALTER TABLE `voucher_xref`
  MODIFY `vcx_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
