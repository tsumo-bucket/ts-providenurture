-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 05, 2017 at 11:20 AM
-- Server version: 5.5.55-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ts_sugarflame`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `acc_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acc_username` varchar(30) NOT NULL,
  `acc_password` varchar(32) NOT NULL,
  `acc_last_name` varchar(30) NOT NULL,
  `acc_first_name` varchar(60) NOT NULL,
  `acc_type` enum('admin','developer','user') NOT NULL DEFAULT 'user',
  `acc_failed_login` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `acc_status` enum('active','locked','deleted') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`acc_id`),
  UNIQUE KEY `username` (`acc_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`acc_id`, `acc_username`, `acc_password`, `acc_last_name`, `acc_first_name`, `acc_type`, `acc_failed_login`, `acc_status`) VALUES
(1, 'developer', '5e8edd851d2fdfbd7415232c67367cc3', 'Developer', 'ThinkSumo', 'developer', 0, 'active'),
(2, 'admin', 'bfb0e820dc42dbec4677348b07dbdf31', 'Administrator', 'System', 'admin', 0, 'active'),
(3, 'jordan.aquino@sumofy.me', '5f4dcc3b5aa765d61d8327deb882cf99', 'Aquino', 'Jordan', 'admin', 0, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `block`
--

CREATE TABLE IF NOT EXISTS `block` (
  `blo_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `blo_user` int(11) NOT NULL,
  `blo_status` enum('active','inactive') NOT NULL,
  `blo_date_created` datetime NOT NULL,
  `blo_date_modified` datetime NOT NULL,
  PRIMARY KEY (`blo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `cty_id` int(11) NOT NULL AUTO_INCREMENT,
  `reg_id` int(11) NOT NULL,
  `cty_name` varchar(100) NOT NULL,
  `cty_status` enum('active','archived') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`cty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversation`
--

CREATE TABLE IF NOT EXISTS `conversation` (
  `cnv_id` int(11) NOT NULL AUTO_INCREMENT,
  `cnv_slug` varchar(100) NOT NULL,
  `cnv_user_1` int(11) NOT NULL COMMENT 'sender',
  `cnv_user_2` int(11) NOT NULL COMMENT 'receiver',
  `cnv_user_1_deleted` enum('yes','no') NOT NULL,
  `cnv_user_1_deleted_date` datetime NOT NULL,
  `cnv_user_2_deleted` enum('yes','no') NOT NULL,
  `cnv_user_2_deleted_date` datetime NOT NULL,
  `cnv_status` enum('active','archived') NOT NULL,
  `cnv_date_created` datetime NOT NULL,
  `cnv_date_modified` datetime NOT NULL,
  PRIMARY KEY (`cnv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `cnt_id` int(11) NOT NULL AUTO_INCREMENT,
  `cnt_name` varchar(100) NOT NULL,
  `cnt_short_name` varchar(100) NOT NULL,
  `cnt_status` enum('active','archived') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`cnt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`cnt_id`, `cnt_name`, `cnt_short_name`, `cnt_status`) VALUES
(1, 'India', 'IN', 'active'),
(2, 'Spain', 'ES', 'active'),
(3, 'United States', 'US', 'active'),
(4, 'Canada', 'CA', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `eml_id` int(11) NOT NULL,
  `eml_mail_to` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_cc` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_bcc` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_subject` varchar(150) CHARACTER SET utf8 NOT NULL,
  `eml_message` text CHARACTER SET utf8 NOT NULL,
  `eml_from` varchar(200) NOT NULL,
  `eml_from_name` varchar(200) NOT NULL,
  `eml_date_sent` datetime NOT NULL,
  `eml_status` enum('sent','failed','resent') NOT NULL,
  `eml_debug` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `flag`
--

CREATE TABLE IF NOT EXISTS `flag` (
  `flg_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `pos_id` int(11) NOT NULL,
  `flg_status` enum('active','inactive') NOT NULL,
  `flg_date_created` datetime NOT NULL,
  `flg_date_modified` datetime NOT NULL,
  PRIMARY KEY (`flg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE IF NOT EXISTS `follow` (
  `fol_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `fol_user` int(11) NOT NULL,
  `fol_status` enum('active','inactive') NOT NULL,
  `fol_date_created` datetime NOT NULL,
  `fol_date_modified` datetime NOT NULL,
  PRIMARY KEY (`fol_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) DEFAULT NULL,
  `img_image` text,
  `img_thumb` text,
  `img_icon` text NOT NULL,
  `img_offset_x` int(11) NOT NULL,
  `img_offset_y` int(11) NOT NULL,
  `img_order` int(11) NOT NULL,
  `img_status` enum('active','rejected','private','inactive') NOT NULL,
  `img_source` enum('upload','facebook','webcam') NOT NULL,
  `img_date_created` datetime NOT NULL,
  `img_date_modified` datetime NOT NULL,
  PRIMARY KEY (`img_id`),
  KEY `usr_id` (`usr_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`img_id`, `usr_id`, `img_image`, `img_thumb`, `img_icon`, `img_offset_x`, `img_offset_y`, `img_order`, `img_status`, `img_source`, `img_date_created`, `img_date_modified`) VALUES
(1, 1, 'uploads/images/qv1joUk0n2CTNpfWMOx1x1se.png', NULL, '', 0, 0, 1, 'inactive', 'upload', '2017-06-02 05:40:25', '2017-06-02 05:57:36'),
(2, 1, 'uploads/images/VSh7stOQyZK2VnVEOXCc2WIV.png', NULL, '', 0, 0, 1, 'inactive', 'upload', '2017-06-02 06:01:01', '2017-06-02 06:01:50'),
(3, 2, 'uploads/images/BeACfkYi6qLIpGpW9g1oB71P.png', NULL, '', 0, 0, 1, 'active', 'upload', '2017-06-02 06:05:02', '2017-06-02 06:05:02');

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE IF NOT EXISTS `info` (
  `inf_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `inf_height` int(11) DEFAULT NULL COMMENT 'in centimeters',
  `inf_height_unit` enum('cm','in','ft') NOT NULL,
  `inf_body_type` enum('slim','athletic','average','curvy','a few extra pounds','overweight','other') DEFAULT NULL,
  `inf_ethnicity` enum('asian','black / african descent','latin / hispanic','east indian','middle eastern','mixed','native american','pacific islander','white / caucasian','other') DEFAULT NULL,
  `inf_smoke` enum('non smoker','light smoker','heavy smoker') DEFAULT NULL,
  `inf_drink` enum('non drinker','social drinker','heavy drinker') DEFAULT NULL,
  `inf_relationship` enum('single','divorced','separated','married but looking','open relationship','widowed') DEFAULT NULL,
  `inf_children` enum('prefer not to say','yes','no') DEFAULT NULL,
  `inf_language` enum('english','espanol','francais','deutsch','chinese','italiano','nederlandese','portuges','russian') DEFAULT NULL,
  `inf_education` enum('high school','some college','associates degree','bachelors degree','graduate degree','post doctorate') DEFAULT NULL,
  `inf_occupation` varchar(100) DEFAULT NULL,
  `inf_spending_habits` int(11) DEFAULT NULL,
  `inf_quality_time` enum('yes','no') DEFAULT NULL COMMENT 'other sugar perks',
  `inf_gifts` enum('yes','no') DEFAULT NULL COMMENT 'other sugar perks',
  `inf_travel` enum('yes','no') DEFAULT NULL COMMENT 'other sugar perks',
  `inf_staycations` enum('yes','no') DEFAULT NULL COMMENT 'other sugar perks',
  `inf_high_life` enum('yes','no') DEFAULT NULL COMMENT 'other sugar perks',
  `inf_simple` enum('yes','no') DEFAULT NULL COMMENT 'other sugar perks',
  `inf_other` varchar(250) DEFAULT NULL,
  `inf_net_worth` int(11) DEFAULT NULL COMMENT 'for sugar sponsors',
  `inf_yearly_income` int(11) DEFAULT NULL COMMENT 'for sugar sponsors',
  `inf_preferred_range_from` int(11) DEFAULT NULL COMMENT 'for sugar babies',
  `inf_preferred_range_to` int(11) DEFAULT NULL COMMENT 'for sugar babies',
  `inf_orientation` enum('gay / lesbian','straight','bisexual','willing to explore','rather not say') DEFAULT NULL,
  `inf_relationship_length` enum('short term','long term') DEFAULT NULL,
  `inf_relationship_loyalty` enum('no strings attached','exclusive') DEFAULT NULL,
  `inf_relationship_preference` enum('rather not say','open to discussion','strictly sexual','serious relationship','marriage','just friends') DEFAULT NULL,
  `inf_sexual_limit` enum('thinking about it','open to new things','anything goes','rather not say','lets keep that to imagination','no experimentation') DEFAULT NULL,
  `inf_privacy_expectations` enum('open to any arrangement','very discreet','ok being in public','depends on attraction level') DEFAULT NULL,
  `inf_date_created` datetime NOT NULL,
  `inf_date_modified` datetime NOT NULL,
  PRIMARY KEY (`inf_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`inf_id`, `usr_id`, `inf_height`, `inf_height_unit`, `inf_body_type`, `inf_ethnicity`, `inf_smoke`, `inf_drink`, `inf_relationship`, `inf_children`, `inf_language`, `inf_education`, `inf_occupation`, `inf_spending_habits`, `inf_quality_time`, `inf_gifts`, `inf_travel`, `inf_staycations`, `inf_high_life`, `inf_simple`, `inf_other`, `inf_net_worth`, `inf_yearly_income`, `inf_preferred_range_from`, `inf_preferred_range_to`, `inf_orientation`, `inf_relationship_length`, `inf_relationship_loyalty`, `inf_relationship_preference`, `inf_sexual_limit`, `inf_privacy_expectations`, `inf_date_created`, `inf_date_modified`) VALUES
(1, 1, 180, 'cm', 'slim', 'black / african descent', 'non smoker', 'social drinker', 'single', 'no', 'chinese', 'high school', 'testter', 1, 'yes', 'yes', NULL, 'no', 'yes', 'yes', '', 1, 0, 100, 10000, NULL, 'long term', 'exclusive', 'rather not say', 'thinking about it', 'very discreet', '2017-06-02 05:38:06', '2017-06-02 05:44:17'),
(2, 2, 160, 'cm', 'athletic', 'black / african descent', 'light smoker', 'social drinker', 'open relationship', 'yes', 'chinese', 'some college', 'tester', 3, 'yes', 'no', NULL, 'no', 'yes', 'no', '', NULL, NULL, 100, 10000, NULL, 'short term', 'no strings attached', 'open to discussion', 'open to new things', 'open to any arrangement', '2017-06-02 06:02:55', '2017-06-02 06:06:59'),
(3, 4, NULL, 'cm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-02 08:02:03', '2017-06-02 08:07:49');

-- --------------------------------------------------------

--
-- Table structure for table `invite`
--

CREATE TABLE IF NOT EXISTS `invite` (
  `inv_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `inv_email` varchar(100) NOT NULL,
  `inv_date_created` datetime NOT NULL,
  PRIMARY KEY (`inv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `like`
--

CREATE TABLE IF NOT EXISTS `like` (
  `lke_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `lke_user` int(11) NOT NULL,
  `lke_date_created` int(11) NOT NULL,
  `lke_status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`lke_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `msg_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `msg_content` text NOT NULL,
  `msg_user` int(11) NOT NULL,
  `msg_status` enum('unread','read') NOT NULL,
  `msg_date_created` datetime NOT NULL,
  `cnv_id` int(11) NOT NULL,
  PRIMARY KEY (`msg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE IF NOT EXISTS `notification` (
  `not_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `not_message` enum('yes','no') NOT NULL DEFAULT 'yes',
  `not_like` enum('yes','no') NOT NULL DEFAULT 'yes',
  `not_profile` enum('yes','no') NOT NULL DEFAULT 'yes',
  `not_content` enum('yes','no') NOT NULL DEFAULT 'yes',
  `not_email` enum('yes','no') NOT NULL DEFAULT 'yes',
  `not_date_created` datetime NOT NULL,
  `not_date_modified` datetime NOT NULL,
  PRIMARY KEY (`not_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`not_id`, `usr_id`, `not_message`, `not_like`, `not_profile`, `not_content`, `not_email`, `not_date_created`, `not_date_modified`) VALUES
(1, 1, 'yes', 'yes', 'yes', 'yes', 'yes', '2017-06-02 05:38:06', '2017-06-02 05:38:06'),
(2, 2, 'yes', 'yes', 'yes', 'yes', 'yes', '2017-06-02 06:02:55', '2017-06-02 06:02:55'),
(3, 4, 'yes', 'yes', 'yes', 'yes', 'yes', '2017-06-02 08:02:03', '2017-06-02 08:02:03');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `pag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pag_title` varchar(140) NOT NULL,
  `pct_id` int(10) unsigned DEFAULT '0',
  `pag_slug` varchar(80) DEFAULT NULL,
  `pag_content` text NOT NULL,
  `pag_date_created` datetime NOT NULL,
  `pag_date_published` datetime DEFAULT NULL,
  `pag_type` enum('editable','static') NOT NULL DEFAULT 'editable',
  `pag_status` enum('published','draft') NOT NULL DEFAULT 'published',
  PRIMARY KEY (`pag_id`),
  UNIQUE KEY `slug` (`pag_slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`pag_id`, `pag_title`, `pct_id`, `pag_slug`, `pag_content`, `pag_date_created`, `pag_date_published`, `pag_type`, `pag_status`) VALUES
(5, 'Overview', 2, 'overview', '<p>The <strong>National Barangay Operations Office – Knowledge Management Center (NBOO KMC) </strong>is a project under the Department of the Interior and Local Government – National Barangay Operations Office in partnership with the European Union through the EU-PHILIPPINES Justice Support Programme II <strong>(EPJUST II).   </strong></p><p><strong></strong>It is an e-learning system, which aims to strengthen the capacities of barangay officials in handling various cases that concern the poor and disadvantaged people through a quality and holistic distance learning education. </p><p>The major objective of the NBOO KMC is to enhance the knowledge and skills of service providers at the barangay level in handling various cases in a manner that is effective and appropriate. </p><p>Specifically, the end-users of the NBOO KMC are expected to:</p><ul><li>Understand the key concepts and principles in handling various cases</li><li>Describe the salient features of some laws </li><li>Demonstrate protocols and procedures in handling cases at the barangay level</li><li>Realize and promote the rights of the more vulnerable sectors of society</li></ul><p>The NBOO KMC provides 4 major courses, which contain learning materials, including videos, handouts, and quizzes.  These courses lay the foundation on understanding key principles, concepts, and methodologies with regard to barangay-related cases. Further, these courses are based on manuals that are developed under the EPJUST II program through a series of studies, consultations, workshops, pre-testings, and trainings among representatives from the DILG regional and field offices, local government units, and other stakeholders. </p><p>The courses are:</p><ol><li>Reinforcing the Roles of Barangay Tanod as First Responder and in Crime Scene Preservation</li><li>Skills Enhancement of Lupon on Gender-Responsive, Child-Friendly, and Indigenous People’s Relevant Katarungang Pambarangay</li><li>Strengthening the Capacities of Punong Barangays and the Barangay VAW Desk Persons in Handling Violence Against Women Cases</li><li>Human-Rights Based Approach in Handling Children at Risk (CAR) and Children with Conflict with the Law</li></ol>', '2016-09-22 00:00:00', '2016-09-22 00:00:00', 'editable', 'published'),
(6, 'Vision', 2, 'vision', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p><p>\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', '2016-09-22 00:00:00', '2016-09-22 00:00:00', 'editable', 'draft'),
(7, 'Mission', 2, 'mission', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p><p>\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', '2016-09-22 00:00:00', '2016-09-22 00:00:00', 'editable', 'draft'),
(8, 'Terms & Conditions', 0, 'terms-conditions', '<ol><li><strong></strong><strong>Introduction</strong><br>The Website Standard Terms and Conditions written on this\r\nwebpage shall manage your use of this website. These Terms will be applied\r\nfully and affect your use. By using this Website, you agreed to accept all\r\nterms and conditions written in here. You must not use this Website if you\r\ndisagree with any of these Website Standard Terms and Conditions.\r\n	<o:p></o:p>\r\n	Minors or people below 18 years old are not allowed to use\r\nthis Website.\r\n	<o:p></o:p><o:p></o:p></li><li><strong>Intellectual Property Rights</strong>Other than the content you own under these Terms, the National Barangay Operations Office and/or its licensors own all the intellectual property rights and materials contained in this Website.  You are granted limited license only for purposes of viewing the material contained on this Website.<strong></strong></li><li><strong>Restrictions</strong><br>You are specifically restricted from all of the following\r\n	<ul><li>publishing any Website material in any other media;</li><li>selling, sublicensing and/or otherwise commercializing any Website\r\nmaterial;\r\n		</li><li>publicly performing and/or showing any Website material;</li><li>using this Website in any way that is or may be damaging to this Website;</li><li>using this Website in any way that impacts user access to this Website;</li><li>using this Website contrary to applicable laws and regulations, or in any way may cause harm to the Website, or to any person or business entity;</li><li>engaging in any data mining, data harvesting, data extracting or any other similar activity in relation to this Website;</li><li>using this Website to engage in any advertising or marketing.</li></ul>Certain areas of this Website are restricted from being access\r\nby you. The National Barangay Operations Office may further restrict access to\r\nany areas of this Website, at any time, in absolute discretion. Any user ID and\r\npassword you may have for this Website are confidential and you must maintain\r\nconfidentiality as well.\r\n	<o:p></o:p><o:p></o:p></li><li><strong>Your Content</strong><br>In these Website Standard Terms and Conditions, “Your Content” shall mean any audio, video text, images or other material you choose to display on this Website. By displaying Your Content, you grant National Barangay Operations Office a non-exclusive, worldwide irrevocable, sub licensable license to use, reproduce, adapt, publish, translate and distribute it in any and all media.  Your Content must be your own and must not be invading any third-party’s rights. The National Barangay Operations Office reserves the right to remove any of Your Content at any time without notice.</li><li><strong>No Warranties</strong><br>This Website is provided “as is,” with all faults, and the National Barangay Operations Office expresses no representations or warranties, of any kind related to this Website or the materials contained on this Website. Also, nothing contained on this Website shall be interpreted as advising you.</li><li><strong>Limitation of liability</strong><br>In no event shall National Barangay Operations Office, nor any of its officers, including the director and his employees, shall be held liable for anything arising out of or in any way connected with your use of this <a href="https://freedirectorysubmissionsites.com/" target="_blank">website</a> whether such liability is under contract.  The National Barangay Operations Office, including its officers shall not be held liable for any indirect, consequential or special liability arising out of or in any way related to your use of this Website.</li><li><strong>Indemnification</strong><br>You are hereby indemnify to the fullest extent by the National Barangay Operations Office from and against any and/or all liabilities, costs, demands, causes of action, damages and expenses arising in any way related to your breach of any of the provisions of these Terms.</li><li><strong>Severability</strong><br>If any provision of these Terms is found to be invalid under any applicable law, such provisions shall be deleted without affecting the remaining provisions herein.</li><li><strong>Variation of Terms</strong><br>The National Barangay Operations Office is permitted to revise these Terms at any time as it sees fit, and by using this Website you are expected to review these Terms on a regular basis.</li><li><strong>Assignment</strong><br>The National Barangay Operations Office is allowed to assign, transfer, and subcontract its rights and/or obligations under these Terms without any notification. However, you are not allowed to assign, transfer, or subcontract any of your rights and/or obligations under these Terms.</li><li><strong>Entire Agreement</strong><br>These Terms constitute the entire agreement between the National Barangay Operations Office and you in relation to your use of this Website, and supersede all prior agreements and understandings.</li><li><strong>Governing Law &amp; Jurisdiction</strong><br>These Terms will be governed by and interpreted in accordance with the laws of the Republic of Philippines and will be covered by a non-exclusive jurisdiction of the state for the resolution of any disputes. </li></ol>', '2016-09-22 00:00:00', '2016-09-22 00:00:00', 'editable', 'published'),
(9, 'Privacy Policy', 0, 'privacy-policy', '<p>By using this website, you agree to the terms of this privacy policy. We may be updating this policy from time to time. We encourage users to frequently check back on this page. </p><p>Should you be required to provide more personal information such as your name and email address for, e.g., subscriptions or correspondence, that information will only be used for that purpose. Information collected from users will not be used for marketing or commercial purposes.<strong></strong></p><p><strong>Privacy Notice</strong></p><p>This privacy notice discloses the privacy practices for <u>nbookmc.com</u>. This privacy notice applies solely to information collected by this website. It will notify you of the following:</p><ol><li>What personally identifiable information is collected from you through the website, how it is used and with whom it may be shared.</li><li>What choices are available to you regarding the use of your data.</li><li>The security procedures in place to protect the misuse of your information.</li><li>How you can correct any inaccuracies in the information.</li></ol><p><strong>Information Collection, Use, and Sharing</strong><br> We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone.</p><p>We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request.</p><p>Unless you ask us not to, we may contact you via email in the future to tell you about changes to this privacy policy.</p><p><strong>Your Access to and Control Over Information</strong><br> You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:</p><ul><li>See what data we have about you, if any.</li><li>Change/correct any data we have about you.</li><li>Express any concern you have about our use of your data.</li></ul><p><strong>Security</strong><br> We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.</p><p>While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.</p>', '2016-09-22 00:00:00', '2016-09-22 00:00:00', 'editable', 'published');

-- --------------------------------------------------------

--
-- Table structure for table `page_category`
--

CREATE TABLE IF NOT EXISTS `page_category` (
  `pct_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pct_name` varchar(50) NOT NULL,
  `pct_slug` varchar(100) NOT NULL,
  PRIMARY KEY (`pct_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `page_category`
--

INSERT INTO `page_category` (`pct_id`, `pct_name`, `pct_slug`) VALUES
(1, 'Category 1', ''),
(2, 'About', 'about');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `pos_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `pos_content` text NOT NULL,
  `pos_status` enum('active','archived') NOT NULL,
  `pos_date_created` datetime NOT NULL,
  `pos_date_modified` datetime NOT NULL,
  PRIMARY KEY (`pos_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post_like`
--

CREATE TABLE IF NOT EXISTS `post_like` (
  `pol_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `pos_id` int(11) NOT NULL,
  `pol_status` enum('active','inactive') NOT NULL,
  `pol_date_created` datetime NOT NULL,
  `pol_date_modified` datetime NOT NULL,
  PRIMARY KEY (`pol_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `preference`
--

CREATE TABLE IF NOT EXISTS `preference` (
  `prf_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `que_id` int(11) DEFAULT NULL,
  `prf_content` text,
  `prf_date_created` datetime NOT NULL,
  `prf_date_modified` datetime NOT NULL,
  PRIMARY KEY (`prf_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `preference`
--

INSERT INTO `preference` (`prf_id`, `usr_id`, `que_id`, `prf_content`, `prf_date_created`, `prf_date_modified`) VALUES
(1, 1, 2, 'tests', '2017-06-02 05:38:06', '2017-06-02 05:44:17'),
(2, 1, 4, 'sdfg&nbsp;', '2017-06-02 05:38:06', '2017-06-02 05:44:17'),
(3, 1, 5, 'dsfg dsfg sdfg', '2017-06-02 05:39:18', '2017-06-02 05:39:18'),
(4, 1, 5, 'dsfg dsfg sdfg', '2017-06-02 05:44:17', '2017-06-02 05:44:17'),
(5, 2, 3, '&nbsp;asd asd&nbsp;', '2017-06-02 06:02:55', '2017-06-02 06:06:36'),
(6, 2, 4, 'asdasd asd aads', '2017-06-02 06:02:55', '2017-06-02 06:06:36'),
(7, 4, 3, NULL, '2017-06-02 08:02:03', '2017-06-02 08:07:49'),
(8, 4, 4, NULL, '2017-06-02 08:02:03', '2017-06-02 08:07:49');

-- --------------------------------------------------------

--
-- Table structure for table `privacy`
--

CREATE TABLE IF NOT EXISTS `privacy` (
  `pri_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `pri_comment` enum('yes','no') NOT NULL DEFAULT 'yes',
  `pri_profile` enum('yes','no') NOT NULL DEFAULT 'no',
  `pri_date_created` datetime NOT NULL,
  `pri_date_modified` datetime NOT NULL,
  PRIMARY KEY (`pri_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `privacy`
--

INSERT INTO `privacy` (`pri_id`, `usr_id`, `pri_comment`, `pri_profile`, `pri_date_created`, `pri_date_modified`) VALUES
(1, 1, 'yes', 'no', '2017-06-02 05:38:06', '2017-06-02 05:38:06'),
(2, 2, 'yes', 'no', '2017-06-02 06:02:55', '2017-06-02 06:02:55'),
(3, 4, 'yes', 'no', '2017-06-02 08:02:03', '2017-06-02 08:02:03');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `que_id` int(11) NOT NULL AUTO_INCREMENT,
  `que_question` text,
  `que_slug` text NOT NULL,
  `que_characters` int(11) NOT NULL,
  `que_type` enum('impression','about me','looking for','extra') NOT NULL DEFAULT 'extra',
  `que_category` enum('flame','sugar','both') NOT NULL,
  `que_status` enum('published','draft') NOT NULL DEFAULT 'draft',
  `que_date_created` datetime DEFAULT NULL,
  `que_date_modified` datetime DEFAULT NULL,
  `que_created_by` int(11) DEFAULT NULL,
  `que_modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`que_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`que_id`, `que_question`, `que_slug`, `que_characters`, `que_type`, `que_category`, `que_status`, `que_date_created`, `que_date_modified`, `que_created_by`, `que_modified_by`) VALUES
(1, 'Say something catchy to make a good first impression!', 'say-something-catchy-to-make-a-good-first-impression-', 20, 'impression', 'both', 'published', '2017-02-16 07:29:05', '2017-02-16 07:35:55', 1, 1),
(2, 'Describe yourself to your SugarBaby - Your likes, dislikes, etc', 'describe-yourself-to-your-sugarbaby---your-likes-dislikes-etc', 50, 'about me', 'sugar', 'published', '2017-02-16 07:36:33', '2017-02-16 15:36:33', 1, NULL),
(3, 'Describe yourself to your Sugar MAN/WOMAN - Your likes, dislikes, etc', 'describe-yourself-to-your-sugar-manwoman---your-likes-dislikes-etc', 50, 'about me', 'flame', 'published', '2017-02-16 07:37:27', NULL, 1, NULL),
(4, 'Define what is your ideal relationship', 'define-what-is-your-ideal-relationship', 50, 'looking for', 'both', 'published', '2017-02-16 07:37:55', NULL, 1, NULL),
(5, 'Do you believe in the supernatural?', 'do-you-believe-in-the-supernatural', 50, 'extra', 'both', 'published', '2017-02-16 07:56:59', '2017-02-16 07:56:59', 1, 1),
(6, 'What would you say is the ideal vacation?', '', 50, 'extra', 'both', 'published', '2017-03-09 00:00:00', NULL, NULL, NULL),
(7, 'What would be harder for you, to tell someone you love them or that you do not love them back?', '', 50, 'extra', 'both', 'published', '2017-03-09 00:00:00', NULL, NULL, NULL),
(8, 'What scares you the most and why?', '', 0, 'extra', '', 'draft', '2017-03-09 00:00:00', NULL, NULL, NULL),
(9, 'What do you do in your free time?', '', 0, 'extra', 'both', 'published', '2017-03-09 00:00:00', NULL, NULL, NULL),
(10, 'Which is your favorite part of the human body and why?', '', 0, 'extra', 'both', 'published', '2017-03-09 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE IF NOT EXISTS `region` (
  `reg_id` int(11) NOT NULL AUTO_INCREMENT,
  `cnt_id` int(11) NOT NULL,
  `reg_name` varchar(100) NOT NULL,
  `reg_status` enum('active','archived') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`reg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `search`
--

CREATE TABLE IF NOT EXISTS `search` (
  `sch_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `sch_name` varchar(100) NOT NULL,
  `sch_content` text NOT NULL,
  `sch_status` enum('active','archived') NOT NULL,
  `sch_date_created` datetime NOT NULL,
  PRIMARY KEY (`sch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('0136ad806d318d60aeb28602cb622ef3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390405, ''),
('0228cede782ebdb2428d56249ae62abc', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384724, ''),
('0250e64559a7b33868ffb4ebfd36a9ce', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384125, ''),
('02b089b9949723279c5d851de065a446', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385652, ''),
('03778c984800ebf5b2aaa12ce9f93db9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390837, ''),
('037878719631894ab10ae867e1806136', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384661, ''),
('04351619ff51e8377e65516ec824721c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390882, ''),
('0470ad7e31f5cf24f8162466f288bd15', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384631, ''),
('04a5d9558972c4a99b1001996629fa6d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385567, ''),
('061f933af0cfcc2be9c90de8f23a79ef', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383904, ''),
('06214ce348784165e40f81c6b1b44c65', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385394, ''),
('083e9bae951b71b7d4fd60a406ecff8f', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385091, ''),
('08add6061cce6dfa0e58b904570d9519', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385091, ''),
('08fc9af723def221bfcd3875f8694320', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385598, ''),
('09727023f3e44e38fea12e1452c85026', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383891, ''),
('0b6228a96c0fbbbf0831c2ceb9a91d8e', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385328, ''),
('0b904e14f091480129c269c96887d124', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383972, ''),
('0bdef44afc02a0279d9e24072acb25c4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385320, ''),
('0efda71e94b1801de2e63902683e8148', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383754, ''),
('0fb5f867976ae3f58704b816cf8557c7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385091, ''),
('0fc0bd5878af4f425fb80c1adb9f171d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384258, ''),
('1001441ef1f18c73a58169a618f52f2b', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390923, ''),
('10b8d531cf6d112f7c8be1769f895206', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390915, ''),
('1101c965c3339ffe6b6739d4d1c6b989', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383789, ''),
('11652fa2a4c314d17efa93d5441b212c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384806, ''),
('129e58112afbb86a37e164a0ff0d11ae', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390406, ''),
('12ab78f3e5aceecae5b5de5c1edf828a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383769, ''),
('12fb46590fb6b055ac938787aeb25a51', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383963, ''),
('13217905dc031be77bd431d9dab59f54', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385652, ''),
('1391d3ad0e558aaa00e2a4d16061a57f', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385479, ''),
('13be576a06fbcc813ed560a214c92037', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383769, ''),
('14385b2ac101884d737ce2b437e03c51', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385032, ''),
('146e78c8077bf571d531b9bf4f22fed2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384717, ''),
('15b442e19fb5dd0872d1a6f5354ad50c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390363, ''),
('15bf0fd248206abca0e6fd4ef375a884', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385032, ''),
('15cf84371866b9f9d88e72784313602b', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385320, ''),
('17adfde73805e3aadbd6e42e3851c126', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390405, ''),
('17c02b2cd54b09d2ae6c60e478c4a3ba', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384963, ''),
('187bb2b8cb80cbebcbe5871ee18d0b75', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385320, ''),
('188772050ba3bb123bc959512ad752f9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390364, ''),
('19271f3a56aa5ebc502c903cc1faab0b', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385656, ''),
('193659ede4c76c2a5433fbfd334e3988', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384126, ''),
('196d143413fada7a2b30afee0d20e372', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384631, ''),
('19ab970d7ddc130618b3912512263f40', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383892, ''),
('1adc9f820e957bccc9a218556fe7fd59', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385458, ''),
('1af823413f9b2bc87c976aa398099321', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384725, ''),
('1b165484858d2df46ac09b02c3ca6bb6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385211, ''),
('1c6861a93b97ea066e985bc933e2674b', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384278, ''),
('1c8436d6ed5a20568636ceee0c2c73ca', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390915, ''),
('1cc43da6b5fae3bd79792c002b34b3a1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383776, ''),
('1d999ef73d2f91cc2be80e5dd8ef2cbf', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385652, ''),
('1e02ab0a8c0a858a8eedac4e2702936c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385425, ''),
('1e399bbf4b693dcfe4c1384229dd503d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390916, ''),
('1e830a1c6e45d50b1777db7e1d53a9a0', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385031, ''),
('1ef428a0ab3fe24e1b584dcdd6292d4b', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383789, ''),
('20350a62a08c57bdca80b348e8a02d22', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383874, ''),
('209a6a6a9a11f6d36e10d9e663dd6840', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384659, ''),
('22b6dbbecbe6f23dbadd00b300d192d7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383873, ''),
('2323c33956278a5026bc8b4eef430866', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385395, ''),
('2367239a9855ac7d4ddef1930a7034bd', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384806, ''),
('23b969ad8f3bc48763e2eec17b133d54', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383969, ''),
('23eb8a9e6d4501ee5aa300185b2264ed', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383825, ''),
('24b115829d6ec0b7c4e6b8063def902b', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385395, ''),
('2588fc11f45fe4e8d16e18bd62600883', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390916, ''),
('25d156ae78f23157805ca0fd90329181', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384724, ''),
('2620ed29e989204679a96ba0811300fd', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383889, ''),
('27236d9c3959b58bd0a36e5d54713fb6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383874, ''),
('2812cf8f9281874674f8ba765509ff32', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385320, ''),
('281ec1648d9342b84ad717145180308c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385032, ''),
('29a527a17be12b19dd3df675cbe35a0a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383962, ''),
('2a004cf2fa6620c3103234c2a4fe9909', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383968, ''),
('2a0c3e5b7220b1242b24b039d5b441ae', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384725, ''),
('2ace2a484744164709817296510f6a06', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390363, ''),
('2c3df07e76ecbb7a52d6d70389cc0828', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383822, ''),
('2c40e8f5b82cca55335fa319f7882fb6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384126, ''),
('2d31d8703e7df5524c1c98d8209cbbe2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385574, ''),
('2d5da147a7874457796e2c910bc56814', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385598, ''),
('2f575b99f63eb2962d81014c531a112d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385395, ''),
('2fdfaa8eb6ffb9df9343a23c64348e9f', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384725, ''),
('30177db2868602d159b24c8ef846311a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385528, ''),
('30659a44b5f948ab129242f36bbceb66', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384807, ''),
('3091fd1624a7f813d3db76473ae3d062', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385529, ''),
('30d95291bf74de0048f1d7830a35a600', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384631, ''),
('3127c913d05806e7e23adcfe8cd162e7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385524, ''),
('31d58c585cdb42b959b1dc62b21a3639', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383973, ''),
('3265a635046131c4bd29b9029e0a9d15', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390837, ''),
('333561a5ac7a13cb8910db9596f6fb04', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385371, ''),
('34313af356bffa6962227d60f2fe7c23', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384675, ''),
('3436e538136226bdec3e9e99d2eb1684', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383962, ''),
('34c929702ae799ad5662f0f8d3b2b4bc', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385598, ''),
('37fc83d59ef763adb78761851fe6de95', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390915, ''),
('38b10a4bc2230b1477f33223e053dff4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390364, ''),
('395162fab67f721a1848fb13d702a949', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383968, ''),
('39da19c0c2cbf94d7f48a111075333e2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383754, ''),
('3a445f883a809f97f30e6f6120f0707e', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385528, ''),
('3ae3016014c40e7ce6ac9169fed0ea13', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385366, ''),
('3af00aef549dc09846664aad6c03c68a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383904, ''),
('3b6034f73f35ffbb3d764c1f599ff26f', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390923, ''),
('3ccb8ffc650f48c97a475a8ad4981c85', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385459, ''),
('3deb831d66d20e98f933cf0588086667', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390415, ''),
('3e2478016c09acc8d84a796715778e00', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385371, ''),
('3e66016339f9bc69974943a7574e5e81', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385371, ''),
('3e82d5606f3b767dd0bf0670bedf54e5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384278, ''),
('3f5fa87c84769dcdb50b5d2c8868353c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383904, ''),
('408eebffd0590cfd24c0692ba58135c5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385327, ''),
('40cbec3e1bc047047758f141c071dafa', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385211, ''),
('4219e62131eed85a81509cf0c1d77014', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385176, ''),
('4251d11bb862bc60f5134a43d018bd84', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384126, ''),
('42805f6880d7cc0fb3d215f583ed948a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383889, ''),
('439639a981e123e66fcd9addeceee4dc', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384126, ''),
('452b287bb5bb6c23fbd036f4adc26e51', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384656, ''),
('45d9526f9d90ac16fb8103b75a4d545a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385567, ''),
('4674175b1bf3dacf4d59b81355f2ec42', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385371, ''),
('46985b6a57ec0391a6f07fdfc551df25', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383904, ''),
('47254244a4822f0559f9f501a8ad7493', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385579, ''),
('490a2bb425444301b60d896e1e8b63fc', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385425, ''),
('4af4f8e791891236467d4a00271f09c3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384963, ''),
('4b9098a242274ea0cb49bc75bb559a37', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383825, ''),
('4c1af95dff576c3935b405e854087c0e', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385459, ''),
('4cdbcaac3e4debb14eb7e2318b1cd134', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384278, ''),
('4cf504bb779f4498b8e1a60a37cd88fa', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384126, ''),
('4df84c7b7d578dbd938b837153fce6f5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384126, ''),
('4e9c7a2a1fc6f146b8150be789701ce0', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385598, ''),
('4e9d0dae256e51c27c4d8aa481649d21', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385426, ''),
('4fc4f9c2c5bae974b22d698a4ee22e67', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383825, ''),
('5099c7bfba83ab1d1fbb42fafc25586a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384657, ''),
('50ab3721cb69eb79df61424480bb374c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383889, ''),
('50e636187d03d9f45edcfdebcf975e73', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385210, ''),
('51712d4a5cdd913d80df08bf31bf74dc', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384141, ''),
('518bdb2ef3dac5f9a08dc9450e679919', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384661, ''),
('51bc805423b65c88c40fd0a2f2650783', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385367, ''),
('51f746e04ba2ca9068a0f55daf2e423c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384659, ''),
('52082d22c4dd28ece0694ec70f2a5b67', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384797, ''),
('5256f75576ff6291c1e8098aa89f6d8b', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384258, ''),
('5276d1a446eb7d743ef270ccaed4fbf3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383825, ''),
('5297405fae0c06e56b6dc9ffea5f3789', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385523, ''),
('535dea907dd4fa83dd9ff8409cbdd1e1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383789, ''),
('536aa6f116d2859fe15187f6ef3dc957', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390915, ''),
('5378155a931d08abd71243d6da0cc35b', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383963, ''),
('537c412c00dbaa35b9698392a83bb0c7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384141, ''),
('53d8ff138e3bfea964139b22bee8edab', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383974, ''),
('54d0b11dcba6959a1695c990c810aace', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390869, ''),
('55a5656db0b7f93d1edc4999cdb05767', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384797, ''),
('56564c4bdf44d09b6a99de8227f29b17', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384806, ''),
('5782d8de30d627dd4906191685c89de1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385682, ''),
('57eb125e24f286c1d26854bcb71317a7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385158, ''),
('581bf80ca308862f0fc80ae56d099133', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385158, ''),
('59850aac9d18c189452691e0e1f51c0a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390370, ''),
('5a26a1e990b1b0d6c80c551d62dfc9b7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385176, ''),
('5a6f90026165953a07684bb3c85b1e6b', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390923, ''),
('5c6ba7cfb4cbf7b3ce50117ac4e07289', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385425, ''),
('5cd305dfdfe65cfd85b1373ee93880a1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385032, ''),
('5cecc900955b552cad649a8351f0dcd1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383891, ''),
('5e1b434afd380fa382faa6eca19f9f68', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384757, ''),
('6194e854868b5207c23fd9ba70fdaf75', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390869, ''),
('6255d1df6b93df9e6d0ada59952b039e', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383963, ''),
('62c80d4af37c9a8e1eaa0027bac047b8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384963, ''),
('62ee8ab2f6d2f8aa71820b7364747aca', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385367, ''),
('633729175534cb48d90633847f39a8ab', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385158, ''),
('634773b7e69bbe04b058d425f6a7b3cd', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385577, ''),
('6487ac62289a69eb96583d295102cd10', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383874, ''),
('65b5d0bdb14548b705a3dc293d252817', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384806, ''),
('65f27a9cf415eecbc39fb0ef6d475bb9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384657, ''),
('6607f42748cb48702320ac7ff058196c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383889, ''),
('661a8551bfffcff678110a8f965a0715', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384661, ''),
('672e5b431f94bea30dbe1af73e6d899d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385158, ''),
('67382540f5a869d7f7d788b37eac98e6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385062, ''),
('676827f321088070b690278e7fad8b21', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383963, ''),
('6919661e532684ade154a08dc5439db2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383769, ''),
('69395eee932b92bb790dd8337fbff6b3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390915, ''),
('6a855807c437727ab1eebf4c412c86ff', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383769, ''),
('6ab1901761660012f25d82263a3d7d69', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383822, ''),
('6ae1b43ef8dce532a45a89736ca9f0fe', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383892, ''),
('6b14a9b53d253bc213e35858863eec1d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385176, ''),
('6b3b57897f0c5e32af33e67bcb8bb534', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383904, ''),
('6c1b23fe956034597839947faad4f891', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383968, ''),
('6ec1ea99f0ceb98bd307692cd2d01a13', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383777, ''),
('6f509d1118e3eadadb9ceb8c6392f925', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383969, ''),
('6f953f87912fd3a969df4e02fb5de869', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384140, ''),
('700cb08e27242c2253031f5eae80c191', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383789, ''),
('71fb9f9e25b022e631e0b449e4dba1a9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384726, ''),
('71fd2e7c8a91957241cb60bb2aa08e33', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385598, ''),
('72305ddf053f2f99e07c74065f74196e', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385158, ''),
('728d51427d1e1c3a6578583dfd11daab', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383969, ''),
('72b9c1c50c0959b4cddcce26c11630e6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385320, ''),
('74cc5e50f2fabaf065068fb3178840f5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384807, ''),
('750281da0ef4cb2a19adb125ec092b10', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384801, ''),
('755157cfe7adb670f85906c599177c0c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383968, ''),
('75b0266853876a9d3873eef48c785f3d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383969, ''),
('75e59aa1e11449eb1a427e9550661205', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385372, ''),
('76258640716ff7dc2903b669c1614ec5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385367, ''),
('764c86066d9bb2ca86edc12d55638401', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384141, ''),
('76f4eca5bc911791defc0ab74652c59e', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384258, ''),
('77714e422c1c627493c2795699314b49', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384806, ''),
('78c7bd2fbbc34c84d4c25533d7165610', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384806, ''),
('79a8a36bf66abb72e02e230698c055b0', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383826, ''),
('79cd9c33e4e32edbb90dd32c3fc319fb', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383974, ''),
('79f92c02561a9bf8d108213b680ab8ae', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385656, ''),
('7a08b6aa6c75093a9c4db3e40f5c9483', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384659, ''),
('7a1702594855eff106bca092be79c15e', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385652, ''),
('7a4ceb5d85b3fa1a6b75fd55c100cd76', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384659, ''),
('7a4d2dae1b4e64ab2798e3f3a2b53dba', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385062, ''),
('7a94fcc07b7ed4cee306d0faa5389d85', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384670, ''),
('7b27256e63471c794478256a5c4dff1a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385091, ''),
('7b714737109dabe57acfc154bc089b70', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385328, ''),
('7c56c50d34734e478cbe322a1a539746', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385656, ''),
('7c74c6b5b4cb818cbc5e4e0c7238893d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383889, ''),
('7dc1017da965249ba39a5dcaf808b2d7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385656, ''),
('7e32c100dbea079f883fe931e0df2f86', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385159, ''),
('7e6f829ec94e4d047eefe9e67f0a179c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384257, ''),
('7e97ea3d4ee5e19b2d537fcc734196aa', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384126, ''),
('7ebbf9770494459630cbbc7127fc5f01', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385472, ''),
('7f016c4d2254a765464eec4417d9e72a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384258, ''),
('7f50ce9f78916f7ab1e066941f7c4b78', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384258, ''),
('804ae26d13b911afec9d0a4b7d84ef07', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390838, ''),
('80738cb5b8bffb46974a273b100298ba', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384631, ''),
('80809d297a1b7cadac3a75de7c6ee11f', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385425, ''),
('816c66ff45baf4d11a9505d62e3d0cad', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384661, ''),
('81b2263040c9759e3f99f7121e680c78', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384676, ''),
('82cc7ac7bbf7d59b1546851c348e12ce', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383972, ''),
('82ddeef59dca8f231f123df97d4c250b', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385371, ''),
('833eeac94189af8baac428aa794c7a80', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383969, ''),
('835142b189fd51e07c25c6657335e0c6', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383777, ''),
('84f76ac234fc82d0b871277b752ad467', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384798, ''),
('858277764edad4b34b7581a13dd9b9df', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385367, ''),
('860022c8087b74c7c74908a8183b9f7a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390364, ''),
('8608a4bbdf8886fc103f284a0b57680d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385328, ''),
('868832e764f167075bca428a9ec6f7aa', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383754, ''),
('86ed899971a300c6c05cd48972953107', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383968, ''),
('87bc91d4bbe83fe80eab336e79521962', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385459, ''),
('88b001208fbd847afbe4dad8a1bf80f0', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384278, ''),
('88bda4a58067bd4ea35dffd3325ac777', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383969, ''),
('88f0d92410f4389aa37fd5cecd83b202', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385598, ''),
('894aa45310c7eb3e6637061ee94df182', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384806, ''),
('8bcbc3e6e83303e0ff14176bb7ce2391', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383754, ''),
('8be75a2dbd81bc90df063051032223fa', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383789, ''),
('8bffeb7648a8e2b27ed44f247d3e3cdb', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385424, ''),
('8cd18deaa7a88de2058eddb960b940b7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383777, ''),
('8e81c4df41d2706913ced4cf8d216782', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383968, ''),
('8ed9bc6870459a92f14c59cfccc45468', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390881, ''),
('8f1743fe424949a25f07a886d5a8f355', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383825, ''),
('8f24b9cdddf399fa8dad9be371b8bc26', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383973, ''),
('900e8f1b1ff512efb749a3b8361992f1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385091, ''),
('913f5dae5ee0600a4445389fe1e6ad21', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385328, ''),
('91633b38c03c820e5f28ad98b2027591', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383768, ''),
('93ca28b6c53ec13c8b862e0daadc3ca9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390882, ''),
('93e79adaa55dd13752d01071e7142ac3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383874, ''),
('9586e4a988b664133e64c4e67d57e08a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385682, ''),
('963c1bab527eeeaa44f406e7d95d65e9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384141, ''),
('965e6f9f4e58149e95f5b8d58516b0a2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384753, ''),
('96687f881b6874906c1b309e461d2dcd', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385175, ''),
('96e288d9f7861ed111ffd036f56fa5e9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384661, ''),
('976e0c771b45061a3cac84a4808686f0', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383822, ''),
('97ce50745e5ad9f0d23863bb1f742a2d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385682, ''),
('98273c61141b4781a589deb223105fb7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383963, ''),
('99d6f2827882db2c9fc2385cc00c1527', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385567, ''),
('9aab735805a3d205416a7c379837773c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384676, ''),
('9bb0b13d17acb9220c772f5980cfce36', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390442, ''),
('9be6b5dca4a191ea3b833a9a9fd64a20', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390916, ''),
('9d42ca5e41539de5714a2b3fd01fcce4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385211, ''),
('9d85fec8520e7e60b7495033c5bf578d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385566, ''),
('9df2cb1bfa96cd9283b176c15fae93e9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390364, ''),
('9e0e120653eab18d2884e9fe159db579', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385567, ''),
('9ea977ea90de58e78d4d87a66fd1e847', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385395, ''),
('9ec44a433c2e2b1ab62f6cb258bfb52d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385528, ''),
('9edf9fc9f61ca8725c5fb4a0f7a26fec', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384807, ''),
('a0c2585dc6d587bf2b58c91f78156701', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385395, ''),
('a121e3992634619daf4fae4e568e0d8c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384657, ''),
('a173faf09c5b70e3f755d85a1d276110', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383904, ''),
('a1b4d386f7d694bcf2d56bf08b2898c4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384278, ''),
('a1e271e2ca5f335b56f60f45f65c19fa', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385062, ''),
('a384dffb72678337e63b31d1a9fd5fc0', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384141, ''),
('a4409d6ec3e0101edc4392b2d8854a06', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385091, ''),
('a5a4bc5c37f3a55ef5504ffc8756d447', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385459, ''),
('a5e3c7abd153b4a670c6317852f3b390', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384725, ''),
('a627588d43651ca35ff4d96c7c42b828', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383769, ''),
('a793bf95e2019f0c8ce8fe8adad9eeed', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383889, ''),
('aa1cb1be86f0407946e3dd3f935030a3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384806, ''),
('aa66c25dbe22f498c7b2b30e9e08bc71', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385598, ''),
('abca79f29e50373a60cc42e385417cd4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385176, ''),
('acbc21a2e1640aad82929eb24f8dae37', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384963, ''),
('acc0fe21d97e14a503050bb5275fe5f3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385320, ''),
('acd1d12967b291a7d9f59d0c166eedc9', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385584, ''),
('adc916898b17b468b8c7893950875766', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384962, ''),
('ae4399c829dc3b81afdc245f5c0334a4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385328, ''),
('ae8c0a3097596a0292aa6e2462450291', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385371, ''),
('aefcd69df604fa464171c3427cada5f0', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383963, ''),
('af3d85ed464dc8bffce5473e0a0d0315', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385395, ''),
('b0267c3ef098eeefb32edd97dd7504a0', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390522, ''),
('b0e7df1f71d28bbe2fba6ac075c70af0', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385426, ''),
('b131a8e7d74e6b1fef2742acbea8fd43', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383753, ''),
('b2b415bccbc3b6f4153eedec866a7a84', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385367, ''),
('b2bba83326f24a23933d8e24090ebecb', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383823, ''),
('b2eea7ac5ae4f46356c4493a5204bdba', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385328, ''),
('b388f45ffe8031cea2156eaf5aef4942', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384258, ''),
('b77754d44a78134a98702ef52a9820d8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385211, ''),
('b782c7c70a8c888682e5979f865abd0c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383754, ''),
('b8274dec3888e5b7514bf76f509b4cf7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383968, ''),
('b88682bdb3d1fa5ef13d852748884e80', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384657, ''),
('bac48f8821d174045ae835481f64c9af', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385567, ''),
('baf6cf8d5247ebe79511ac1973083a7a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385524, ''),
('bb81052f903e4aeb5ad8bbfd190905f7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384657, ''),
('bb90c6aad9b8284e2bb80bf1409ad021', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383874, ''),
('bb9f66c3411b7badf2db355855270406', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385090, ''),
('bca150d40ed80a6bc3ce8e2edc8834c1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384631, ''),
('bcb8825eba8a88f9d974d6324620105d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385211, ''),
('bcc7653630d99d1dcc29ec5927ba5527', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384659, ''),
('bd48812707a750e7ecf94811a13e9e77', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384661, ''),
('bd71770354d9d47acd1aaa7f63a78a8a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384632, ''),
('be15b73c7a6b72a7167147727e580ab2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385091, ''),
('bed770288a898b4e13115f861d0b2c92', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385032, ''),
('befe494fb5a07c9b88b4223b90e6dea7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390406, ''),
('bf84f0d101c128b1aa367375c5256551', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383769, ''),
('c0d526fc9b24126ac549e7aeee514f45', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383969, ''),
('c0dc98df0ae15fad88e8a08627efc22c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384278, ''),
('c1f45dc004a020b1b78fd85db3de013c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385367, ''),
('c29d6aa78c36d9e1bb2e475260eb8c54', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384963, ''),
('c2cdfbdc517fba66d1370be66d5635ef', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385061, ''),
('c38c19c0e8a8165bf266f2f8aef6501a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383777, ''),
('c53feaeac8dfd67bb4dfdc372d23e3ea', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383974, ''),
('c579b2057290559929835d169a2ee4ee', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385598, ''),
('c5ad341bfe10a4c55054d1471d02240c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383777, ''),
('c68adf7fb27d9ec2d4e4b41dfce346d1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385598, ''),
('c6cc111e45d7877a572907d245fadafe', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384676, ''),
('c78c5314a632ef9811a236f851927a00', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384661, ''),
('c7e9a7fa87e6147bed97098722e6e4c4', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383888, ''),
('c822a19b845c38ac3abf2efe9a76a686', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385682, ''),
('c9433c50c1527e590875b8a0dfeeb504', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390882, ''),
('c98deb03192af4ea9cd0789381f9b790', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383963, ''),
('caf174e526fed7d0af1c1cd77285003c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384753, ''),
('cb24bafbda2c1bdcd821986510a567bd', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383889, ''),
('cbd729d1199d993a4b75b592208bf177', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384963, ''),
('cc548e87e471bc54123eeeab641a35f3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385063, ''),
('ccc578aa16b804e0ba09f09d78414a5c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385524, ''),
('cd26e9012b0ad06aed53f9e9a8e10ba2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385367, ''),
('cd5d8d8f3618b126c48b142bb7024ba1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385524, ''),
('cdb1602d590ba5d863a3f7234ba902c8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383789, ''),
('cea65dabb1e0400880d2154d7e5039a2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383777, ''),
('cefa3ab745a05084ec2bc2dacc63dbbf', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385371, ''),
('d042229ac8ad4028cf1d584f5939ab3d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383973, ''),
('d076d43d13a47d949cdc91f8f3db0d91', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390881, ''),
('d182c9a457cd8db461386fa6ec04ad4e', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383962, ''),
('d2010c124705321cf04aa95548e1bbdc', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390405, ''),
('d446fd819751db9b21784f4a56e876b7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383973, ''),
('d4663993e0a701a90053bd4d5f9e4a78', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384797, ''),
('d47035c7ab6cac6742ec31a4001a7bdb', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384278, ''),
('d4cb16be97756f8fd9d3e5e33347e9ff', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385176, ''),
('d4d61403d3dfeda57127b700cc31fd21', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385598, ''),
('d51601414e3ef09d67bee4f66240f733', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385524, ''),
('d535139f7460e007f2b332b8d9a0b9ad', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385062, ''),
('d5b1f7bcaa53362f8452a11669c912ed', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385524, ''),
('d61c5392ec1027282e20955d68ce8c47', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385459, ''),
('d6d95a60892ac5b2101ce858f4f98222', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385528, ''),
('d72775643afdd820c9c675f5a981ab20', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385177, ''),
('d732952dc359366b3b658176cacd537e', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385598, ''),
('d7a3b1a4c267122a6a51552d7348fe78', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384141, ''),
('d88c9042adc79660245936b485b3a425', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384677, ''),
('d88d9ad33b2b5636995d91ad53340607', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383788, ''),
('da3650db21ea88cd5543fce8ff1e8bcc', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384807, ''),
('da692d876349ed4cf53d9897c3a4af8c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385459, ''),
('dc5af48a5b18f650556eb612de7a4c21', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390363, ''),
('ddf7e3d4ed308490261b54175332e47c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385426, ''),
('debddd7efcc3e29752adfb99fd8e1cda', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385157, ''),
('ded61445f10cf812797e9c22604a9bf2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385528, ''),
('e049e7e9c8983dadb59a5a3fabd86de7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384661, ''),
('e05bf3a5a92a1205621e4a5532162e24', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383963, ''),
('e102a04a861c1ae3d2db0b847cab75c3', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385395, ''),
('e23277166cdc02027c60aa1fab7a52b0', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384675, ''),
('e336e32a4d7bf9df8b6fed1daefbd38f', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384631, ''),
('e346453080f652aec8b7f3516469d9a5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384753, ''),
('e38df8b48806aa3416f1bb88ea3a45ae', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383754, ''),
('e505f0ea5bfb7a93bed219b1b8b73afa', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384963, ''),
('e57be02e3cafc4cdb009b3153d5baf39', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384258, ''),
('e5f97bd7a2d1fe3b6df09c4fd7ba85ea', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385320, ''),
('e60a9724a309be80c20b4a5d9680cba1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390406, ''),
('e6103eeb2e6ecee38fb95b1bb950252a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383968, ''),
('e61649543d08674a09da883ccbddb78c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384657, ''),
('e6704304f2274fa711757bcab431ad32', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385524, ''),
('e68ce94229cf72f4bc912b62949445c8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385596, ''),
('e6ed28c9e9b8b8467abab20eefccd3a1', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383823, ''),
('e7624d3d57854ef1be6c7869c4481834', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384724, ''),
('e788ca50c67e9ed4c6f9f26a42d5aa8f', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383973, ''),
('e87988ccc40cbf05e19a5181e2562dc5', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384657, ''),
('e8f6d284c4f91a32874bfb2b0f7eddcd', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385032, ''),
('ea500c7738857872f767136137217753', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383969, ''),
('ea5fbf6bd2b2119ce7bd25958d277386', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385211, ''),
('eaa74a90ff267f4d4b319107b0ebc756', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384141, ''),
('eaeac361744c3905d23608e564b20419', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385529, ''),
('eb5fade34b55467a8d2d35b53d9b11c8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384675, ''),
('ebabd0da56fd30068be8657e416941ba', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385158, ''),
('ec21c3cf606f511e6662cc15d7c8fbb2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385567, ''),
('eccaf809b106be7c31473c7acea18780', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383904, ''),
('ed47f1232025797e40decfc454da447e', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383825, ''),
('ed557d4eda804a948ec0b6fcdef30b0a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384631, ''),
('ed646316738ab3e85a7af1f9b8fa539e', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383754, ''),
('ed68363575a32d7ef7e4ebc5bce09a0d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390838, ''),
('ed9924fafd75c4f924081eb3a5d65a36', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383962, ''),
('ee10098f632475cf161ffc26db1ca2e8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385328, ''),
('eebe75182d4285477d4853392d0e8f57', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383904, ''),
('eef46107d1e983cb302a671ca2e54076', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384753, ''),
('ef18a94c5c5b30610308a5e142ae0270', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385032, ''),
('f029d258d109e4bb3768b5b736f8c1cd', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384675, ''),
('f074fa3e7849a75ad78671eb0e0956f2', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383874, ''),
('f094216b4898cf4d9646d6f045844e10', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390922, ''),
('f0f33be1a792625b63f6fc35a656420d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384724, ''),
('f2062b155a285e0e0004e60ad7509748', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385567, ''),
('f327a68ba6b02fa7eb8874d6397c38cb', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383891, ''),
('f33737f854c5e707fcd61d213010087c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383972, ''),
('f3c4078e0ca63584eb7d7aaa27703b86', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383962, ''),
('f490f2b142873b4bf98445a873d9814a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390869, ''),
('f53639077788f6664412af45cd66477a', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385599, ''),
('f5edb3b03c13e2e246153742dc5af7e8', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385062, ''),
('f7f825bca5f704040e41a41cbe78758d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390837, ''),
('f82c6ce60a1af771d81e98644485dda0', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385321, ''),
('f83ca0d5281524c6598c91e9414a9c74', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383777, ''),
('f89c2ccdce20547c520f76dd7d3e7586', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383825, ''),
('f93d20a68cb49bfcd984e7496b5d1dfc', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390869, ''),
('f95d96459c30f950b940bd180b354722', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390923, ''),
('fa539b231bdf18131dc36159e6198d4d', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384278, '');
INSERT INTO `session` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('fb4997ddcd3a995777e4c899fcd7b72e', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385062, ''),
('fb65b8a3f7a795eb22bf2738e1927bfb', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496383789, ''),
('fb944871f9c41c260d05953cf63b284f', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496384676, ''),
('fbc236146dd40fdcf41b839166b14675', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385176, ''),
('fc0faefa541108b1eba5c6150f270498', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390406, ''),
('fc56da6fc6bd6fe56b06681154f61ad7', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385459, ''),
('fd1c4435cae9dae182225e29b10050ba', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496390916, ''),
('fdccc1a87c670f033dfa15daa371e127', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385529, ''),
('ff3216dd932252bd085c2e3c5b7cf21c', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36', 1496385211, '');

-- --------------------------------------------------------

--
-- Table structure for table `site_options`
--

CREATE TABLE IF NOT EXISTS `site_options` (
  `opt_id` int(11) NOT NULL AUTO_INCREMENT,
  `opt_name` varchar(256) NOT NULL,
  `opt_slug` varchar(100) NOT NULL,
  `opt_type` enum('text','image') NOT NULL DEFAULT 'text',
  `opt_value` text NOT NULL,
  PRIMARY KEY (`opt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `site_options`
--

INSERT INTO `site_options` (`opt_id`, `opt_name`, `opt_slug`, `opt_type`, `opt_value`) VALUES
(1, 'Site Name', 'site-name', 'text', 'Think Sumo 2'),
(2, 'Facebook Title', 'facebook-title', 'text', 'Think Sumo'),
(3, 'Facebook Description', 'facebook-description', 'text', 'Think Sumo Site'),
(4, 'Facebook Image (1200px x 630px)', 'facebook-image', 'image', './uploads/site_options/facebook-image.png');

-- --------------------------------------------------------

--
-- Table structure for table `source`
--

CREATE TABLE IF NOT EXISTS `source` (
  `src_id` int(11) NOT NULL AUTO_INCREMENT,
  `src_name` varchar(100) NOT NULL,
  `src_status` enum('active','archived') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`src_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE IF NOT EXISTS `subscription` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `sub_status` enum('pending','paid') NOT NULL DEFAULT 'pending',
  `sub_start_date` datetime NOT NULL,
  `sub_end_date` datetime NOT NULL,
  `sub_date_created` datetime NOT NULL,
  `sub_date_modified` datetime NOT NULL,
  `sub_amount` float NOT NULL,
  `sub_type` enum('monthly','yearly') NOT NULL,
  `sub_transaction_id` varchar(100) NOT NULL,
  `vch_id` int(11) NOT NULL,
  PRIMARY KEY (`sub_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `usr_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_type` enum('flame','sugar') NOT NULL,
  `usr_first_name` varchar(100) NOT NULL,
  `usr_last_name` varchar(100) NOT NULL,
  `usr_screen_name` varchar(100) NOT NULL,
  `usr_email` varchar(100) NOT NULL,
  `usr_password` varchar(100) NOT NULL,
  `usr_birthmonth` int(11) NOT NULL,
  `usr_birthdate` int(11) NOT NULL,
  `usr_birthyear` int(11) NOT NULL,
  `usr_birthday` date NOT NULL,
  `cty_id` int(11) NOT NULL,
  `cnt_id` int(11) NOT NULL,
  `usr_location` text NOT NULL,
  `ulc_id` int(11) NOT NULL,
  `usr_status` enum('pending','approved','rejected','deactivated','suspended') NOT NULL DEFAULT 'pending',
  `usr_gender` enum('male','female','trans male','trans female') NOT NULL,
  `usr_interested` enum('male','female','both') NOT NULL,
  `usr_interested_country` int(11) NOT NULL,
  `usr_interested_age_from` int(11) NOT NULL,
  `usr_interested_age_to` int(11) NOT NULL,
  `usr_description` text CHARACTER SET utf8mb4,
  `usr_signup_source` varchar(100) NOT NULL,
  `usr_signup_step` enum('initial','photo','info','preference','done') NOT NULL,
  `usr_verified` enum('yes','no') NOT NULL DEFAULT 'no',
  `usr_social_verified` enum('no','yes') NOT NULL DEFAULT 'no',
  `usr_password_reset` varchar(100) NOT NULL,
  `usr_password_reset_date` datetime NOT NULL,
  `usr_created_date` date NOT NULL,
  `usr_approved_date` datetime NOT NULL,
  `usr_modified_date` date NOT NULL,
  `usr_last_login` datetime DEFAULT NULL,
  `usr_token` varchar(100) NOT NULL,
  `usr_token_id` varchar(100) NOT NULL COMMENT 'will be used for profile viewing',
  `img_id` int(11) DEFAULT NULL,
  `src_id` int(11) NOT NULL,
  PRIMARY KEY (`usr_id`),
  KEY `usr_username` (`usr_screen_name`),
  KEY `cty_id` (`cty_id`),
  KEY `usr_preference` (`usr_interested`,`usr_gender`),
  KEY `usr_birthdate` (`usr_birthdate`),
  KEY `usr_status` (`usr_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`usr_id`, `usr_type`, `usr_first_name`, `usr_last_name`, `usr_screen_name`, `usr_email`, `usr_password`, `usr_birthmonth`, `usr_birthdate`, `usr_birthyear`, `usr_birthday`, `cty_id`, `cnt_id`, `usr_location`, `ulc_id`, `usr_status`, `usr_gender`, `usr_interested`, `usr_interested_country`, `usr_interested_age_from`, `usr_interested_age_to`, `usr_description`, `usr_signup_source`, `usr_signup_step`, `usr_verified`, `usr_social_verified`, `usr_password_reset`, `usr_password_reset_date`, `usr_created_date`, `usr_approved_date`, `usr_modified_date`, `usr_last_login`, `usr_token`, `usr_token_id`, `img_id`, `src_id`) VALUES
(1, 'sugar', 'test', 'test', 'test', 'dona.roxas@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 3, 2, 1998, '1998-03-02', 0, 1, 'Pune, IN', 1, 'pending', 'male', 'female', 1, 18, 80, 'f sdf sd', 'web', 'done', 'no', 'no', '', '0000-00-00 00:00:00', '2017-06-02', '0000-00-00 00:00:00', '2017-06-02', NULL, 'b19d143bdb68951ede0593c9578747f9', '14963818851', NULL, 0),
(2, 'flame', 'asd', 'asd', 'asd', 'dona.roxas@sumo.me', '0b44f2d88a7e04ad484138c173c88d30', 2, 2, 1998, '1998-02-02', 0, 2, 'Asturias, ES', 2, 'pending', 'male', 'female', 2, 18, 80, 'asd asd', 'web', 'done', 'no', 'no', '', '0000-00-00 00:00:00', '2017-06-02', '0000-00-00 00:00:00', '2017-06-02', NULL, 'f5bd8aa2832ee31dccec6666bd3e6854', '14963833752', 3, 0),
(3, 'flame', 'test', 'test', 'tester', 'dona.roxas@sum.me', '', 2, 3, 1997, '1997-02-03', 0, 3, 'Pasadena, US', 3, 'pending', 'female', 'male', 3, 18, 80, NULL, 'facebook', 'info', 'no', 'yes', '', '0000-00-00 00:00:00', '2017-06-02', '0000-00-00 00:00:00', '2017-06-02', NULL, '6950e25ffa006a0ba73a2315f9a491d4', '', NULL, 0),
(4, 'flame', 'asd', 'asd', 'asdasdasd', 'dona.roxas@sumofy.me', '', 3, 3, 1997, '1997-03-03', 0, 4, 'AB, CA', 4, 'pending', 'female', 'male', 4, 18, 80, NULL, 'facebook', 'info', 'no', 'yes', '', '0000-00-00 00:00:00', '2017-06-02', '0000-00-00 00:00:00', '2017-06-02', NULL, '972d8f0787f5e7b2868df893cfd6a98a', '14963905234', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE IF NOT EXISTS `user_accounts` (
  `usa_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `usa_type` varchar(50) NOT NULL,
  `usa_email` varchar(200) NOT NULL,
  `usa_first_name` varchar(200) NOT NULL,
  `usa_last_name` varchar(200) NOT NULL,
  `usa_image` text NOT NULL,
  `usa_birthday` date NOT NULL,
  `usa_friend_count` varchar(200) NOT NULL,
  `usa_photo_count` varchar(200) NOT NULL,
  `usa_access_id` int(11) NOT NULL,
  `usa_access_token` text NOT NULL,
  `usa_date_created` datetime NOT NULL,
  `usa_date_modified` datetime NOT NULL,
  PRIMARY KEY (`usa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`usa_id`, `usr_id`, `usa_type`, `usa_email`, `usa_first_name`, `usa_last_name`, `usa_image`, `usa_birthday`, `usa_friend_count`, `usa_photo_count`, `usa_access_id`, `usa_access_token`, `usa_date_created`, `usa_date_modified`) VALUES
(1, 4, 'facebook', 'dona.roxas@sumofy.me', 'Dona', 'Roxas', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/13164365_10209123870844677_8468225522285934869_n.jpg?oh=f0d19e5e2d706d9b968bfaac31bab9d6&oe=59B2ACC6', '0000-00-00', '679', '25', 2147483647, 'EAAJ4xJ0N2scBAJNd0Hj2t8ZAWEkcG7NbcEWjL9ZBIbOkZBzLCducEbrhJxXbB5iAneDt7UaEkxWxQugxZCJoXwb9RBSGZCDUf7EYxFa3aZA2ZAGPYMwPsz1x6ymgjzLtmzJvtWOoCZCpFqoeaMZAiWlGyU9rZALXKUKv24knbxy8nQjXZCrT7wQXx7d', '2017-06-02 08:02:03', '2017-06-02 08:02:03');

-- --------------------------------------------------------

--
-- Table structure for table `user_inactive`
--

CREATE TABLE IF NOT EXISTS `user_inactive` (
  `usr_id` int(11) NOT NULL,
  `usi_id` int(11) NOT NULL AUTO_INCREMENT,
  `usi_reason` text NOT NULL,
  `usi_comment` text,
  `usi_type` enum('deactivate','delete') NOT NULL,
  `usi_status` enum('active','archived') NOT NULL,
  `usi_created_date` datetime NOT NULL,
  `usi_modified_date` datetime NOT NULL,
  PRIMARY KEY (`usi_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_like`
--

CREATE TABLE IF NOT EXISTS `user_like` (
  `usl_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `usl_user` int(11) NOT NULL,
  `usl_status` enum('active','inactive') NOT NULL,
  `usl_date_created` datetime NOT NULL,
  `usl_date_modified` datetime NOT NULL,
  PRIMARY KEY (`usl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_location`
--

CREATE TABLE IF NOT EXISTS `user_location` (
  `ulc_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `ulc_city` varchar(200) NOT NULL,
  `ulc_state` varchar(200) NOT NULL,
  `ulc_country` varchar(200) NOT NULL,
  `ulc_lat` float NOT NULL,
  `ulc_lng` float NOT NULL,
  `ulc_date_added` datetime NOT NULL,
  PRIMARY KEY (`ulc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_location`
--

INSERT INTO `user_location` (`ulc_id`, `usr_id`, `ulc_city`, `ulc_state`, `ulc_country`, `ulc_lat`, `ulc_lng`, `ulc_date_added`) VALUES
(1, 1, 'Pune', 'MH', 'India', 18.5083, 73.8466, '2017-06-02 05:38:05'),
(2, 2, '', 'Asturias', 'Spain', 43.3614, -5.85933, '2017-06-02 06:02:54'),
(3, 0, 'Pasadena', 'CA', 'United States', 34.1478, -118.145, '2017-06-02 08:00:42'),
(4, 4, '', 'AB', 'Canada', 53.9333, -116.576, '2017-06-02 08:02:03');

-- --------------------------------------------------------

--
-- Table structure for table `user_report`
--

CREATE TABLE IF NOT EXISTS `user_report` (
  `rep_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `rep_user` int(11) NOT NULL,
  `rep_reason` enum('1','2','3','4') NOT NULL,
  `rep_other_reason` text NOT NULL,
  `rep_evidence` text NOT NULL,
  `rep_date_created` datetime NOT NULL,
  `rep_date_modified` datetime NOT NULL,
  PRIMARY KEY (`rep_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `view`
--

CREATE TABLE IF NOT EXISTS `view` (
  `vie_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `vie_user` int(11) NOT NULL,
  `vie_date_created` datetime NOT NULL,
  PRIMARY KEY (`vie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE IF NOT EXISTS `voucher` (
  `vch_id` int(11) NOT NULL AUTO_INCREMENT,
  `vch_code` varchar(100) NOT NULL,
  `vch_status` enum('active','archived') NOT NULL,
  `vch_value` int(11) NOT NULL COMMENT 'in terms of days',
  `vch_valid_until` datetime NOT NULL,
  `vch_date_created` datetime NOT NULL,
  `vch_date_modified` datetime NOT NULL,
  `vch_created_by` int(11) NOT NULL,
  `vch_modified_by` int(11) NOT NULL,
  PRIMARY KEY (`vch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `voucher_xref`
--

CREATE TABLE IF NOT EXISTS `voucher_xref` (
  `vcx_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `vch_id` int(11) NOT NULL,
  `vcx_date_created` int(11) NOT NULL,
  PRIMARY KEY (`vcx_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
