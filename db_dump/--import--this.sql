-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2017 at 08:05 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ts_nboo_kmc`
--

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `pag_id` int(10) UNSIGNED NOT NULL,
  `pag_title` varchar(140) NOT NULL,
  `pct_id` int(10) UNSIGNED DEFAULT '0',
  `pag_slug` varchar(80) DEFAULT NULL,
  `pag_content` text NOT NULL,
  `pag_date_created` datetime NOT NULL,
  `pag_date_published` datetime DEFAULT NULL,
  `pag_type` enum('editable','static') NOT NULL DEFAULT 'editable',
  `pag_status` enum('published','draft') NOT NULL DEFAULT 'published'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`pag_id`, `pag_title`, `pct_id`, `pag_slug`, `pag_content`, `pag_date_created`, `pag_date_published`, `pag_type`, `pag_status`) VALUES
(5, 'Overview', 2, 'overview', '<p>The <strong>National Barangay Operations Office – Knowledge Management Center (NBOO KMC) </strong>is a project under the Department of the Interior and Local Government – National Barangay Operations Office in partnership with the European Union through the EU-PHILIPPINES Justice Support Programme II <strong>(EPJUST II).   </strong></p><p><strong></strong>It is an e-learning system, which aims to strengthen the capacities of barangay officials in handling various cases that concern the poor and disadvantaged people through a quality and holistic distance learning education. </p><p>The major objective of the NBOO KMC is to enhance the knowledge and skills of service providers at the barangay level in handling various cases in a manner that is effective and appropriate. </p><p>Specifically, the end-users of the NBOO KMC are expected to:</p><ul><li>Understand the key concepts and principles in handling various cases</li><li>Describe the salient features of some laws </li><li>Demonstrate protocols and procedures in handling cases at the barangay level</li><li>Realize and promote the rights of the more vulnerable sectors of society</li></ul><p>The NBOO KMC provides 4 major courses, which contain learning materials, including videos, handouts, and quizzes.  These courses lay the foundation on understanding key principles, concepts, and methodologies with regard to barangay-related cases. Further, these courses are based on manuals that are developed under the EPJUST II program through a series of studies, consultations, workshops, pre-testings, and trainings among representatives from the DILG regional and field offices, local government units, and other stakeholders. </p><p>The courses are:</p><ol><li>Reinforcing the Roles of Barangay Tanod as First Responder and in Crime Scene Preservation</li><li>Skills Enhancement of Lupon on Gender-Responsive, Child-Friendly, and Indigenous People’s Relevant Katarungang Pambarangay</li><li>Strengthening the Capacities of Punong Barangays and the Barangay VAW Desk Persons in Handling Violence Against Women Cases</li><li>Human-Rights Based Approach in Handling Children at Risk (CAR) and Children with Conflict with the Law</li></ol>', '2016-09-22 00:00:00', '2016-09-22 00:00:00', 'editable', 'published'),
(6, 'Vision', 2, 'vision', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p><p>\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', '2016-09-22 00:00:00', '2016-09-22 00:00:00', 'editable', 'draft'),
(7, 'Mission', 2, 'mission', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p><p>\r\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', '2016-09-22 00:00:00', '2016-09-22 00:00:00', 'editable', 'draft'),
(8, 'Terms & Conditions', 0, 'terms-conditions', '<ol><li><strong></strong><strong>Introduction</strong><br>The Website Standard Terms and Conditions written on this\r\nwebpage shall manage your use of this website. These Terms will be applied\r\nfully and affect your use. By using this Website, you agreed to accept all\r\nterms and conditions written in here. You must not use this Website if you\r\ndisagree with any of these Website Standard Terms and Conditions.\r\n	<o:p></o:p>\r\n	Minors or people below 18 years old are not allowed to use\r\nthis Website.\r\n	<o:p></o:p><o:p></o:p></li><li><strong>Intellectual Property Rights</strong>Other than the content you own under these Terms, the National Barangay Operations Office and/or its licensors own all the intellectual property rights and materials contained in this Website.  You are granted limited license only for purposes of viewing the material contained on this Website.<strong></strong></li><li><strong>Restrictions</strong><br>You are specifically restricted from all of the following\r\n	<ul><li>publishing any Website material in any other media;</li><li>selling, sublicensing and/or otherwise commercializing any Website\r\nmaterial;\r\n		</li><li>publicly performing and/or showing any Website material;</li><li>using this Website in any way that is or may be damaging to this Website;</li><li>using this Website in any way that impacts user access to this Website;</li><li>using this Website contrary to applicable laws and regulations, or in any way may cause harm to the Website, or to any person or business entity;</li><li>engaging in any data mining, data harvesting, data extracting or any other similar activity in relation to this Website;</li><li>using this Website to engage in any advertising or marketing.</li></ul>Certain areas of this Website are restricted from being access\r\nby you. The National Barangay Operations Office may further restrict access to\r\nany areas of this Website, at any time, in absolute discretion. Any user ID and\r\npassword you may have for this Website are confidential and you must maintain\r\nconfidentiality as well.\r\n	<o:p></o:p><o:p></o:p></li><li><strong>Your Content</strong><br>In these Website Standard Terms and Conditions, “Your Content” shall mean any audio, video text, images or other material you choose to display on this Website. By displaying Your Content, you grant National Barangay Operations Office a non-exclusive, worldwide irrevocable, sub licensable license to use, reproduce, adapt, publish, translate and distribute it in any and all media.  Your Content must be your own and must not be invading any third-party’s rights. The National Barangay Operations Office reserves the right to remove any of Your Content at any time without notice.</li><li><strong>No Warranties</strong><br>This Website is provided “as is,” with all faults, and the National Barangay Operations Office expresses no representations or warranties, of any kind related to this Website or the materials contained on this Website. Also, nothing contained on this Website shall be interpreted as advising you.</li><li><strong>Limitation of liability</strong><br>In no event shall National Barangay Operations Office, nor any of its officers, including the director and his employees, shall be held liable for anything arising out of or in any way connected with your use of this <a href="https://freedirectorysubmissionsites.com/" target="_blank">website</a> whether such liability is under contract.  The National Barangay Operations Office, including its officers shall not be held liable for any indirect, consequential or special liability arising out of or in any way related to your use of this Website.</li><li><strong>Indemnification</strong><br>You are hereby indemnify to the fullest extent by the National Barangay Operations Office from and against any and/or all liabilities, costs, demands, causes of action, damages and expenses arising in any way related to your breach of any of the provisions of these Terms.</li><li><strong>Severability</strong><br>If any provision of these Terms is found to be invalid under any applicable law, such provisions shall be deleted without affecting the remaining provisions herein.</li><li><strong>Variation of Terms</strong><br>The National Barangay Operations Office is permitted to revise these Terms at any time as it sees fit, and by using this Website you are expected to review these Terms on a regular basis.</li><li><strong>Assignment</strong><br>The National Barangay Operations Office is allowed to assign, transfer, and subcontract its rights and/or obligations under these Terms without any notification. However, you are not allowed to assign, transfer, or subcontract any of your rights and/or obligations under these Terms.</li><li><strong>Entire Agreement</strong><br>These Terms constitute the entire agreement between the National Barangay Operations Office and you in relation to your use of this Website, and supersede all prior agreements and understandings.</li><li><strong>Governing Law &amp; Jurisdiction</strong><br>These Terms will be governed by and interpreted in accordance with the laws of the Republic of Philippines and will be covered by a non-exclusive jurisdiction of the state for the resolution of any disputes. </li></ol>', '2016-09-22 00:00:00', '2016-09-22 00:00:00', 'editable', 'published'),
(9, 'Privacy Policy', 0, 'privacy-policy', '<p>By using this website, you agree to the terms of this privacy policy. We may be updating this policy from time to time. We encourage users to frequently check back on this page. </p><p>Should you be required to provide more personal information such as your name and email address for, e.g., subscriptions or correspondence, that information will only be used for that purpose. Information collected from users will not be used for marketing or commercial purposes.<strong></strong></p><p><strong>Privacy Notice</strong></p><p>This privacy notice discloses the privacy practices for <u>nbookmc.com</u>. This privacy notice applies solely to information collected by this website. It will notify you of the following:</p><ol><li>What personally identifiable information is collected from you through the website, how it is used and with whom it may be shared.</li><li>What choices are available to you regarding the use of your data.</li><li>The security procedures in place to protect the misuse of your information.</li><li>How you can correct any inaccuracies in the information.</li></ol><p><strong>Information Collection, Use, and Sharing</strong><br> We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell or rent this information to anyone.</p><p>We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request.</p><p>Unless you ask us not to, we may contact you via email in the future to tell you about changes to this privacy policy.</p><p><strong>Your Access to and Control Over Information</strong><br> You may opt out of any future contacts from us at any time. You can do the following at any time by contacting us via the email address or phone number given on our website:</p><ul><li>See what data we have about you, if any.</li><li>Change/correct any data we have about you.</li><li>Express any concern you have about our use of your data.</li></ul><p><strong>Security</strong><br> We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.</p><p>While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.</p>', '2016-09-22 00:00:00', '2016-09-22 00:00:00', 'editable', 'published');

-- --------------------------------------------------------

--
-- Table structure for table `page_category`
--

CREATE TABLE `page_category` (
  `pct_id` int(10) UNSIGNED NOT NULL,
  `pct_name` varchar(50) NOT NULL,
  `pct_slug` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_category`
--

INSERT INTO `page_category` (`pct_id`, `pct_name`, `pct_slug`) VALUES
(1, 'Category 1', ''),
(2, 'About', 'about');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`pag_id`),
  ADD UNIQUE KEY `slug` (`pag_slug`);

--
-- Indexes for table `page_category`
--
ALTER TABLE `page_category`
  ADD PRIMARY KEY (`pct_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `pag_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `page_category`
--
ALTER TABLE `page_category`
  MODIFY `pct_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
