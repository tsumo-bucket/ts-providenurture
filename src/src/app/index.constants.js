/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('sugar')
    .constant('modulePrefix', '.pn')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
