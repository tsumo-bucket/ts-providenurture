(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('InitialController', InitialController);

  /** @ngInject */
  function InitialController($timeout, $http, webDevTec, toastr, $scope, $q, logged, MemberService, URL, $location, $window, FilterFactory, $log, $state, userData, $rootScope, SettingsService) {
    $('.navbar').addClass('transluscent');
    if(logged) {
      $rootScope.user = angular.fromJson(userData.isLogged(), true);
      $scope.user = $rootScope.user;
      $scope.inf_id = $rootScope.user.inf_id;
      SettingsService.getInfo($scope.inf_id).then(function(res) {
        if(res.success) {

          $scope.user = res.data[0];
          if(res.data[0].inf_preferred_range_to == 0 || res.data[0].inf_preferred_range_to == null) {
            $scope.user.inf_preferred_range_to = 30000;
          }
          else {
            $scope.user.inf_preferred_range_to = res.data[0].inf_preferred_range_to;
          }

          if(res.data[0].inf_preferred_range_from == 0 || res.data[0].inf_preferred_range_to == null) {
            $scope.user.inf_preferred_range_from = 500;
          }
          else {
            $scope.user.inf_preferred_range_from = res.data[0].inf_preferred_range_from;
          }

          if($scope.user.inf_quality_time == 'yes') {
            $scope.user.inf_quality_time = true;
          } 
          else {
            $scope.user.inf_quality_time = false;
          }
          if($scope.user.inf_gifts == 'yes') {
            $scope.user.inf_gifts = true;
          } 
          else {
            $scope.user.inf_gifts = false;
          }
          if($scope.user.inf_staycations == 'yes') {
            $scope.user.inf_staycations = true;
          } 
          else {
            $scope.user.inf_staycations = false;
          }
          if($scope.user.inf_high_life == 'yes') {
            $scope.user.inf_high_life = true;
          } 
          else {
            $scope.user.inf_high_life = false;
          }
          if($scope.user.inf_simple == 'yes') {
            $scope.user.inf_simple = true;
          } 
          else {
            $scope.user.inf_simple = false;
          }
          if($scope.user.inf_other) {
            $scope.user.inf_other_value = true;
            $scope.user.other = $scope.user.inf_other;
          } 
          else {
            $scope.user.inf_other = false;
          }
          SettingsService.getPreferences().then(function(res) {
            $scope.prf_id1 = res.data[0][0].prf_id;
            $scope.prf_id2 = res.data[0][1].prf_id;
            $scope.user.describe = res.data[0][0].prf_content;
            $scope.user.relationship = res.data[0][1].prf_content;
            if(res.data[0][2]) {
              $scope.prf_id3 = res.data[0][2].prf_id;
              $scope.user.other_question = res.data[0][2].que_id;
              $scope.user.other_answer = res.data[0][2].prf_content;
            }
          });
        }
      });
      if($rootScope.user.usr_signup_step == 'photo') {        
        $state.go('account.addPhotos');
      }
      else if($rootScope.user.usr_signup_step == 'preference') {
        $state.go('account.preferences');
      }
      else if($rootScope.user.usr_signup_step == 'info') {
        $state.go('account.personal');
      }
      else if($rootScope.user.usr_signup_step == 'done' && $rootScope.user.usr_status == 'approved') {
        $state.go('members');
      }
    }

    $scope.isWebcam ="false";
    $scope.uploadMethod = '';

    $scope.user = userData.getTemp();
    if($scope.user) {
      if(!$scope.user.usr_gender) {
        $scope.user.usr_gender = '';
        $scope.user.usr_interested_age_from = 18;
        $scope.user.usr_interested_age_to = 80;
      }
    }

    $scope.changeGender = function() {
      $scope.user.usr_type = '';
      $scope.userForm.usr_type.$setPristine();
    }

    $scope.changeDays = function(month) {
      // console.log(month)
      $scope.filter.days = [];
      if(month == 2) {
        var x = 0;
        for (var d = 1; d <= 28; d++)
        {
          $scope.filter.days[x] = {
              name: d,
              value: d
          };
          x++;
        }
      }
      else if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
        var x = 0;
        for (var d = 1; d <= 31; d++)
        {
          $scope.filter.days[x] = {
              name: d,
              value: d
          };
          x++;
        }
      }
      else if(month == 4 || month == 6 || month == 9 || month == 11) {
        var x = 0;
        for (var d = 1; d <= 30; d++)
        {
          $scope.filter.days[x] = {
              name: d,
              value: d
          };
          x++;
        }
      }
    };
    // INITIAL
    $scope.saveEmail = function(user) {
      if($scope.userForm.$invalid) {
        $scope.submitted = true;
        toastr.warning('Please make sure that you have completed all the fields correctly.');
      }
      else {
        $rootScope.user = user;
        $rootScope.user.usr_signup_source = 'web';
        // console.log($rootScope.user);
        var pwdLength = user.usr_passwordz.length;
        $scope.user.pwd = new Array();

        for(var x = 0; x < pwdLength; x++) {
          $scope.user.pwd.push(user.usr_passwordz.charAt(x));
        }
        userData.setTempUser($rootScope.user);
        $state.go('join2');
      }
    };
    $scope.saveType = function(user) {
      if($scope.userForm.$invalid) {
        $scope.submitted = true;
        toastr.warning('Please make sure that you have completed all the fields correctly.');
      }
      else {
        $rootScope.user = user;
        if(user.usr_interested_age_from >= 18 & user.usr_interested_age_to <= 80) {
          userData.setTempData('usr_gender', user.usr_gender);
          userData.setTempData('usr_type', user.usr_type);
          userData.setTempData('usr_interested', user.usr_interested);
          userData.setTempData('usr_interested_age_to', user.usr_interested_age_to);
          userData.setTempData('usr_interested_age_from', user.usr_interested_age_from);
          $state.go('join3');
        }
        else {
          toastr.warning('Make sure to fill out the fields correctly');
        }
      }
    };
    // END INITIAL
    $scope.createAccount = function(user) {
      if($scope.userForm.$invalid) {
        $scope.submitted = true;
        toastr.warning('Please make sure that you have completed all the fields correctly.');
      }
      else {
        if(grecaptcha.getResponse()) {
          $rootScope.actionLoader = true;
          var maxDate, bday;
          maxDate = moment().subtract(18, "years");
          bday = moment(user.usr_birthyear + '/' + user.usr_birthmonth + '/' + user.usr_birthdate);
          // console.log(maxDate);
          // console.log(bday);
          if(maxDate.isAfter(bday)) {
            if($scope.components) {
              if($scope.components.state) {
                var params = {
                  usr_first_name: user.usr_first_name,
                  usr_last_name: user.usr_last_name,
                  usr_screen_name: user.usr_screen_name,
                  usr_gender: user.usr_gender,
                  usr_type: user.usr_type,
                  usr_interested: user.usr_interested,
                  usr_interested_age_from: user.usr_interested_age_from,
                  usr_interested_age_to: user.usr_interested_age_to,
                  usr_email: user.usr_email,
                  usr_birthmonth: user.usr_birthmonth,
                  usr_birthdate: user.usr_birthdate,
                  usr_birthyear: user.usr_birthyear,
                  usr_signup_source: user.usr_signup_source,
                  src_id: user.src_id,
                  country_long_name: $scope.components.country,
                  country_short_name: $scope.components.countryCode,
                  state: $scope.components.state,
                  city: $scope.components.city,
                  lat: $scope.components.latitude,
                  lng: $scope.components.longitude,
                }

                if(user.usr_signup_source != 'facebook') {
                  params.usr_password = user.usr_passwordz;
                }
                else {
                  params.access_token = user.access_token;
                  params.fb_id = user.fb_id;
                  params.first_name = user.first_name;
                  params.last_name = user.last_name;
                  params.fb_birthday = user.fb_birthday;
                  params.fb_image = user.fb_image;
                  params.friend_count = user.friend_count;
                  params.photo_count = user.photo_count;
                }

                if(!user.usr_interested_age_from) {
                  if(user.usr_type == 'baby') {
                    params.usr_interested_age_to = 30;
                  } 
                  else {
                    params.usr_interested_age_to = 18;
                  }
                }

                if(!user.usr_interested_age_to) {
                  if(user.usr_type == 'baby') {
                    params.usr_interested_age_to = 50;
                  } 
                  else {
                    params.usr_interested_age_to = 30;
                  }
                }

                MemberService.createAccount(params).then(function(res) {
                  $rootScope.actionLoader = false;
                  if(res.success) {
                    $state.go('account.personal');
                    // goog_report_conversion(URL.SITE + 'account/information');
                    // fbq('trackCustom', 'CreateAccount', {
                    //   email: res.data[0].usr_email,
                    //   first_name: res.data[0].usr_first_name,
                    //   last_name: res.data[0].usr_last_name
                    // });
                  }
                  else {
                    toastr.error(res.error.message);
                  }
                }, function (error) {
                  $log.debug(error);
                });
              }
              else {
                toastr.error('Please enter a more specific location.');
                $rootScope.actionLoader = false;
              }
            }
            else {
              toastr.error('Please enter your location correctly.');
              $rootScope.actionLoader = false;
            }
          }
          else {
            toastr.error('You must be at least 18 years of age to continue.');
          }
        
        }
        else{
          toastr.error('Verify that you are not a robot.');
        }
      }
    };

    var checkEmail = function(params, response, access_token, photo_count) {
      MemberService.emailCheck(params).then(function(res) {
        if(res.success) {
          if(res.data[0].usr_signup_source == 'facebook') {
            $rootScope.$broadcast('login', res.data[0]);
            $rootScope.actionLoader = false;
            if(res.data[0].usr_signup_step == 'done' && res.data[0].usr_status == 'approved') {
              $state.go('members');
              $scope.hide = false;
            }
            if(res.data[0].usr_signup_step == 'done' && res.data[0].usr_status == 'pending') {
              $state.go('search');
              $scope.hide = true;
            }
            else if(res.data[0].usr_signup_step == 'initial') {
              $state.go('account.addPhotos');
              $scope.hide = true;
            }
            else if(res.data[0].usr_signup_step == 'photo') {
              $state.go('account.addPhotos');
              $scope.hide = true;
            }
            else if(res.data[0].usr_signup_step == 'info') {
              $state.go('account.personal');
              $scope.hide = true;
            }
            else if(res.data[0].usr_signup_step == 'preference') {
              $state.go('account.preferences');
              $scope.hide = true;
            }
          }
          else {
            toastr.warning('Email address is not valid or unavailable.');
            $rootScope.actionLoader = false;
          }
        }
        else {
          $rootScope.user = {
            usr_email: response.email,
            first_name: response.first_name,
            last_name: response.last_name,
            fb_image: response.picture.data.url,
            fb_birthday: response.birthday,
            friend_count: response.friends.summary.total_count,
            photo_count: photo_count,
            fb_id: response.id,
            access_token: access_token,
            usr_signup_source: 'facebook'
          }
          userData.setTempUser($rootScope.user);
          $state.go('join2');
          $rootScope.actionLoader = false;
        }
      });
    };

    // for counting
    // var countPhotos = function(link) {
    //   $http({
    //     method: 'GET',
    //     url: link
    //   }).success(function(response) {
    //     console.log(response);
    //     $scope.photo_count += response.data.length;
    //     if(response.paging.next) {
    //       countPhotos(response.paging.next);
    //     }
    //     console.log($scope.photo_count);
    //   }).error(function(error) {
    //     console.log(error);
    //   });
    // }

    $scope.connectFacebook = function() {
      $rootScope.actionLoader = true;
      FB.getLoginStatus(function(response) {
        if(response.status == 'connected') {
          var access_token = response.authResponse.accessToken;
          FB.api('/me',
            {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday, picture, permissions"},
            function(response) {

            if(!response.birthday || !response.friends) {
              FB.login(function(response) {
                if (response.authResponse) {
                  var access_token = response.authResponse.accessToken;
              
                  FB.api('/me', 
                    {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday"},
                    function(response) {
                      var params = {
                        email: response.email
                      };
                      FB.api(
                        '/me/photos',
                        'GET',
                        {"fields":"images,picture", "type":"uploaded"},
                        function(response) {
                          if(response.data) {
                            $scope.photo_count = response.data.length;
                          }
                          else {
                            $scope.photo_count = '';
                          }
                          
                          if($scope.photo_count) {
                            checkEmail(params, response, access_token, $scope.photo_count);
                          }
                          else {
                            checkEmail(params, response, access_token, $scope.photo_count);
                          }
                        }
                      );

                  });
                } else {
                  $rootScope.actionLoader = false;
                  toastr.warning('User cancelled login or did not fully authorize.');
                }
              }, {scope: 'email,user_birthday,user_friends,user_photos'});
            }
            else {
              var params = {
                email: response.email
              };
              FB.api('/me', 
                {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday, picture, permissions"},
                function(response) {
                  $scope.initial_response = response;
                  var params = {
                    email: response.email
                  };
                  FB.api(
                    '/me/photos',
                    'GET',
                    {"fields":"images,picture", "type":"uploaded"},
                    function(response) {
                      if(response.data) {
                        $scope.photo_count = response.data.length;
                      }
                      else {
                        $scope.photo_count = '';
                      }
                      // for counting
                      // if(response.paging.next) {
                      //   countPhotos(response.paging.next);
                      // }
                      if($scope.photo_count) {
                        checkEmail(params, $scope.initial_response, access_token, $scope.photo_count);
                      }
                      else {
                        checkEmail(params, $scope.initial_response, access_token, $scope.photo_count);
                      }
                    }
                  );
                  
              });
              // checkEmail(params, response, access_token);
            }
          });
        }
        else {
          FB.login(function(response) {
            if (response.authResponse) {
              var access_token = response.authResponse.accessToken;
          
              FB.api('/me', 
                {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday, picture, permissions"},
                function(response) {
                  var params = {
                    email: response.email
                  };
                  FB.api(
                    '/me/photos',
                    'GET',
                    {"fields":"images,picture", "type":"uploaded"},
                    function(response) {
                      $scope.photo_count = response.data.length;

                      // for counting
                      // if(response.paging.next) {
                      //   countPhotos(response.paging.next);
                      // }
                    }
                  );
                  if($scope.photo_count) {
                    checkEmail(params, response, access_token, $scope.photo_count);
                  }
                  else {
                    checkEmail(params, response, access_token, $scope.photo_count);
                  }
              });
            } else {
              $rootScope.actionLoader = false;
              toastr.warning('User cancelled login or did not fully authorize.');
            }
          }, {scope: 'email,user_birthday,user_friends,user_photos'});
        }
      });
    };

    $scope.connectGoogle = function() {
      // var profile = googleUser.getBasicProfile();
      // console.log(profile.getEmail());
      // console.log(profile.getGivenName());
      // console.log(profile.getFamilyName());
      // console.log(profile.getAuthResponse().id_token);
      var auth = gapi.auth2.getAuthInstance();
      console.log(auth);
    };

    $scope.edit = function() {
      if($scope.editEmail) {
        $scope.editEmail = false;
      }
      else {
        $scope.editEmail = true;
      }
    };

  }
})();
