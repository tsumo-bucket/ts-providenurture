(function() {
  'use strict';

  angular
    .module('sugar')
    .service('MemberService', MemberService);

  function MemberService($http, $q, $log, URL, userData, $rootScope) {

    this.createAccount = function(data) {
      var authResult = $q.defer();
      
      $http({
        method: 'POST',
        url: URL.API + 'users/initial',
        data: data
      }).success(function(response) {
        authResult.resolve(response);
        if(response.success) {
          userData.setAccount(response.data[0]);
        }
      }).error(function(error) {
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.emailCheck = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/email',
        data: data
      }).success(function(response) {
        authResult.resolve(response);
        if(response.success) {
          userData.setAccount(response.data[0]);
        }
      }).error(function(error) {
        authResult.reject(error);
      });

      return authResult.promise;
    }

    this.authenticate = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/login',
        data: data
      }).success(function(response) {
        if(!response.error) {
          userData.setUser(response.data, data.remember);
          $rootScope.user = JSON.parse(userData.isLogged());
        }
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('authenticate error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.verify = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/verify',
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.logout = function() {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/logout',
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.deactivate = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/deactivate',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.delete = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/delete',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.forgotPassword = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/forgot',
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('forgotPassword error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.resetPassword = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/reset',
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('resetPassword error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.resendConfirmation = function(data) {
      var authResult = $q.defer();
      $http({
        method: 'POST',
        url: URL.API + 'users/resend',
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('resendConfirmation error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.changePassword = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/password',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('changePassword error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.getProfile = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'users/profile',
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        if(!response.error) {
          // if(response.data[0].usr_image) {
          //   response.data[0].usr_image = URL.BASE + response.data.usr_image;
          // }
          // userData.setUserData(response.data[0]);
        }
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('editProfile error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.getUser = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'users/simple_profile',
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        if(!response.error) {
          // if(response.data[0].usr_image) {
          //   response.data[0].usr_image = URL.BASE + response.data.usr_image;
          // }
          // userData.setUserData(response.data[0]);
        }
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('editProfile error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.viewProfile = function(data) {
      var authResult = $q.defer();
      $http({
        method: 'GET',
        url: URL.API + 'users/profile',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        params: {usr_token_id: data}
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('editProfile error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.getToken = function(data) {
      var authResult = $q.defer();
      $http({
        method: 'GET',
        url: URL.API + 'users/get_token',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        params: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('editProfile error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.editProfile = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/edit',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        if(!response.error) {
          // userData.setUserData(response.data[0]);
        }
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('editProfile error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.updateStep = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'users/signup_step',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        if(!response.error) {
          userData.setAccount(response.data[0]);
        }
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('editProfile error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.editDescription = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/edit_description',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        if(!response.error) {
          // userData.setUserData(response.data[0]);
        }
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('editProfile error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.updateNotifications = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/updateNotifications?' + Math.round(new Date().getTime()/1000),
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('updateNotifications error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.block = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/block',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('updateNotifications error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.unblock = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/block',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('updateNotifications error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.favorite = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/favorite',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('updateNotifications error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.report = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/report',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.linkFacebook = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/facebook',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
        if(response.success) {
          userData.setUserData('usr_social_verified', 'yes');
        }
      }).error(function(error) {
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.unlinkFacebook = function(id) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'user_accounts/delete',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: {usa_id: id}
      }).success(function(response) {
        if(response.success) {
          userData.setUserData('usr_social_verified', 'no');
        }
        authResult.resolve(response);
      }).error(function(error) {
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.follow = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/follow',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('updateNotifications error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.getImages = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'images/list',
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('updateNotifications error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.imagePrivacy = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'images/privacy',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('updateNotifications error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.uploadImage = function(data, ind) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'images/upload?' + ind,
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('updateNotifications error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.deleteImage = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'images/delete',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('updateNotifications error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };
    
    this.blurImage = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'images/blur',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('updateNotifications error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.cropImage = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'images/crop',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        authResult.resolve(response);
      }).error(function(error) {
        // console.log('updateNotifications error: ' + error);
        authResult.reject(error);
      });

      return authResult.promise;
    };

    this.makeProfile = function(data) {
      var authResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'images/makeProfile',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        console.log(response);
        if(!response.error) {
          userData.setUserData('img_id', response.data[0].img_id);
          userData.setUserData('img_image', response.data[0].img_image);
        }
        else {
          userData.setUserData('img_id', null);
          userData.setUserData('img_image', null);
        }
        authResult.resolve(response);
      }).error(function(error) {
        authResult.reject(error);
      });

      return authResult.promise;
    };

  }

})();
