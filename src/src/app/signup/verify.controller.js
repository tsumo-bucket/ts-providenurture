(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('VerifyController', VerifyController);

  /** @ngInject */
  function VerifyController($timeout, $http, webDevTec, toastr, $stateParams, $scope, $q, MemberService, $location, $window, $anchorScroll, FilterFactory, $mdDialog, $mdToast, $log, $state, userData, $rootScope, SettingsService, Upload) {
    // if(userData.isLogged()) {
      // $rootScope.actionLoader = false;
      $rootScope.actionLoader = true;
      var params = {
        usr_id: $stateParams.usr_token
      }
      MemberService.verify(params).then(function(res) {
        $rootScope.actionLoader = false;
        if(res.success) {
          userData.setUserData('usr_verified', 'yes');
        }
      });
    // }
    // else {
    //   // $window.location.reload();
    //   $timeout(function(){
    //     if(!userData.isLogged()) {
    //       // $state.go('home');
    //       $rootScope.actionLoader = false;
    //     }
    //   }, 100);
    // }
  }
})();
