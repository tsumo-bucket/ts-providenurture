(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('PhotosController', PhotosController);

  /** @ngInject */
  function PhotosController($timeout, webDevTec, toastr, $scope, $window, URL, $q, logged, MemberService, $location, $anchorScroll, FilterFactory, $mdDialog, $mdToast, $log, $state, userData, $rootScope, SettingsService, Upload) {

    if(logged) {
      $rootScope.user = angular.fromJson(userData.isLogged(), true);
      $scope.user = $rootScope.user;
      $scope.prf_id1 = $rootScope.user.prf_id1;
      $scope.prf_id2 = $rootScope.user.prf_id2;
      $scope.inf_id = $rootScope.user.inf_id;
    }
    else {
      $timeout(function(){
        if(!userData.isLogged()) {
          $state.go('home');
          $rootScope.actionLoader = false;
        }
      }, 100);
    }
    $rootScope.$watch('user', function(){
      $scope.user = $rootScope.user;
    });

    $scope.isWebcam ="false";
    $scope.uploadMethod = '';

    $scope.images = [
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''}
    ];

    MemberService.getImages().then(function(res) {
      // $log.debug(res);
      if(res.success) {
        $rootScope.uploadedImages = res.data[0];
        $scope.images.splice(0, res.data[0].length)
        for(var x= 0; x < res.data[0].length;x++ ) {
          $scope.images.unshift(res.data[0][x]);
        }
        $scope.selected = res.data[0][0];
      }
      else {
        $rootScope.uploadedImages = [];
      }
    });

    $scope.select = function(img) {
      if($scope.cropping || $scope.blurring || $scope.rotating) {
        $scope.cropping = false;
        $scope.blurring = false;
        $scope.cancelRotate();
        var blurctx = imageCanvas.getContext('2d');
        var rotatectx = blurCanvas.getContext('2d');
        rotatectx.clearRect(0, 0, imageCanvas.width, imageCanvas.height);
        blurctx.clearRect(0, 0, blurCanvas.width, blurCanvas.height);
      }
      
      if(localstream) {
        localstream.getTracks()[0].stop();
      }
      if(img.src == '') {
      }
      else {
        $scope.selected = img;
        $scope.showCam = false;
      }
    };

    if($state.current.name == "account.success") {
      $scope.end = true;
    }
    else if($state.current.name == "account.preferences") {
      $scope.preferences = true;
    }
    else if($state.current.name == "account.information") {
      $scope.info = true;
    }

    $scope.mime_type = "image/jpeg";
    $scope.quality = 0.8;  
    //STEP 1
    // VIA UPLOAD FROM COMPUTER
    $scope.upload = function (file) {
      $scope.loading = true;
      var reader = new window.FileReader();
      reader.readAsDataURL(file); 
      reader.onloadend = function() {
        var base64data = reader.result;  
        var img = new Image();
        img.onload = function() {
          if (img.width) {
            // for resizing big image
            var oc = document.createElement('canvas'), octx = oc.getContext('2d');
            oc.width = img.width;
            oc.height = img.height;
            octx.drawImage(img, 0, 0);
            while (oc.height * 0.5 > 500) {
              oc.width *= 0.5;
              oc.height *= 0.5;
              octx.drawImage(oc, 0, 0, oc.width, oc.height);
            }
            oc.width = 500;
            oc.height = oc.width * img.height / img.width;
            octx.drawImage(img, 0, 0, oc.width, oc.height);


            // for thumb
            var tc = document.createElement('canvas'), tctx = tc.getContext('2d');
            tc.width = img.width;
            tc.height = img.height;
            tctx.drawImage(img, 0, 0);
            while (tc.height * 0.5 > 300) {
              tc.width *= 0.5;
              tc.height *= 0.5;
              tctx.drawImage(oc, 0, 0, tc.width, tc.height);
            }
            tc.width = 300;
            tc.height = tc.width * img.height / img.width;
            tctx.drawImage(img, 0, 0, tc.width, tc.height);
            
            var params = {
              img_image: oc.toDataURL($scope.mime_type, $scope.quality),
              img_thumb: tc.toDataURL($scope.mime_type, $scope.quality),
              img_source: 'upload',
              img_status: 'active'
            };   
            MemberService.uploadImage(params, 1).then(function(res) {
              // $log.debug(res);
              $scope.loading = false;
              if(res.success) {
                var ind = _.findIndex($scope.images, {src:''});
                $rootScope.uploadedImages.push(res.data[0]);
                $scope.images.splice(ind, 1, res.data[0]);
                $scope.selected = res.data[0];
              }
              else {
                toastr.error(res.error.message);
              }
            }, function(res) {
              // $log.debug(res);
              $scope.loading = false;
              toastr.error('File size is too big. Choose another image or resize.');
            });
          } 
        };
        img.src = base64data;
      }
    };
    // END UPLOAD FROM COMPUTER METHOD

    // VIA WEBCAM
    $scope.showCam = false;
    var localstream;
    $scope.webcam = function() {
      $scope.cropping = false;
      $scope.blurring = false;
      $scope.uploadMethod = 'webcam';
      if(!$scope.showCam) {
        $scope.showCam = true;
        // Grab elements, create settings, etc.
        var video = document.getElementById('video');

        // Get access to the camera!
        if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            // Not adding `{ audio: true }` since we only want video now
            navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                localstream = stream;
                video.src = window.URL.createObjectURL(stream);
                video.play();
            });
        }
      }
      else {
        $scope.showCam = false;
        localstream.getTracks()[0].stop();
      }
    };

    function stopCam() {
      video.pause();
      video.src="";
    }


    var x = 0;

    $rootScope.$on('$stateChangeStart', 
    function(event, toState, toParams, fromState, fromParams){ 
        // do something
        if(x == 0) {
          if(fromState.name == 'profile.photos') {
            if($scope.rotating || $scope.cropping || $scope.blurring) {
              event.preventDefault();
                var confirm = $mdDialog.confirm()
                  .title('Discard Changes?')
                  .textContent('Are you sure you want to discard all of your changes?')
                  .ariaLabel('Confirm Discard')
                  .targetEvent(event)
                  .ok('Discard')
                  .cancel('Cancel');

                $mdDialog.show(confirm).then(function() {
                  x += 1;
                  $state.go(toState.name);
                }, function() {
                  x = 0;
                });

            }
          }
        }
    });

    $scope.capture = function() {
      // Elements for taking the snapshot
      // stopCam();

      $scope.loading = true;
      var canvas = document.getElementById('canvas');
      var context = canvas.getContext('2d');
      var video = document.getElementById('video');

      // Trigger photo take
      // document.getElementById("snap").addEventListener("click", function() {
      context.drawImage(video, 0, 0);
      var img = new Image();
      img.setAttribute('crossOrigin', 'anonymous');
      img.onload = function() {
        if (img.width) {
          // for resizing big image
          var oc = document.createElement('canvas'), octx = oc.getContext('2d');
          oc.width = img.width;
          oc.height = img.height;
          octx.drawImage(img, 0, 0);
          while (oc.width * 0.5 > 500) {
            oc.width *= 0.5;
            oc.height *= 0.5;
            octx.drawImage(oc, 0, 0, oc.width, oc.height);
           }
          oc.width = 500;
          oc.height = oc.width * img.height / img.width;
          octx.drawImage(img, 0, 0, oc.width, oc.height);

          // for thumb
          var tc = document.createElement('canvas'), tctx = tc.getContext('2d');
          tc.width = img.width;
          tc.height = img.height;
          tctx.drawImage(img, 0, 0);
          while (tc.width * 0.5 > 300) {
            tc.width *= 0.5;
            tc.height *= 0.5;
            tctx.drawImage(oc, 0, 0, tc.width, tc.height);
           }

          tc.width = 300;
          tc.height = tc.width * img.height / img.width;
          tctx.drawImage(img, 0, 0, tc.width, tc.height);
          
          var params = {};
          var params = {
            img_image: oc.toDataURL($scope.mime_type, $scope.quality),
            img_thumb: tc.toDataURL($scope.mime_type, $scope.quality),
            img_source: 'webcam',
            img_status: 'active'
          };            
          $scope.showCam = false;
          $scope.uploaded = true;
          MemberService.uploadImage(params, 1).then(function(res) {
            // $log.debug(res);
            $scope.loading = false;
            if(res.success) {
              var ind = _.findIndex($scope.images, {src:''});
              $rootScope.uploadedImages.push(res.data[0]);
              $scope.images.splice(ind, 1, res.data[0]);
              $scope.selected = res.data[0];

              localstream.getTracks()[0].stop();
            }
            else {
              toastr.error(res.error.message);
              localstream.getTracks()[0].stop();
            }
          }, function(res) {
            // $log.debug(res);
          });
        } 
      };
      img.src = canvas.toDataURL();
        
    };
    // END WEBCAM METHOD

    // IMPORT FROM FACEBOOK
    $scope.openDialog = function(ev) {
      $scope.$watch('fbPhotos');
      $mdDialog.show({
        controller: ImportController,
        templateUrl: 'app/signup/import.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        locals: {
          fbPhotos: $scope.fbPhotos,
          paging: $scope.paging
        },
        clickOutsideToClose:false,
        fullscreen: true // Only for -xs, -sm breakpoints.,
      })
      .then(function(answer) {
        // $log.debug(answer);
      }, function() {
      });
    };

    function ImportController($scope, MemberService, $q, $mdDialog, fbPhotos, paging) {
      $scope.fbPhotos = fbPhotos;
      $scope.paging = paging;
      $scope.importPhotos = [];
      $scope.cancel = function() {
        $mdDialog.hide();
      };
      $scope.selectImage = function(img) {
        if($scope.importPhotos.length < 20) {
          if($scope.importPhotos.includes(img.images[0].source)) {
            var ind = $scope.importPhotos.indexOf(img.images[0].source);
            $log.debug(ind);
            $scope.importPhotos.splice(ind, 1);
          }
          else {
            $scope.importPhotos.push(img.images[0].source)
          }
          
        } else {

        }
      }

      $scope.import = function() {

        if(($rootScope.uploadedImages.length + $scope.importPhotos.length) > 20) {
          toastr.warning('You are only allowed to upload a maximum of 20 photos. Delete photos you want to replace.');
        }
        else {
          var promises = [];
          $rootScope.actionLoader = true;
          var i = 1;
          for(var x = 0; x < $scope.importPhotos.length; x++) {
            var img = new Image();
            img.setAttribute('crossOrigin', 'anonymous');
            img.onload = function() {
              if (img.width) {
                // for resizing big image
                var oc = document.createElement('canvas'), octx = oc.getContext('2d');
                oc.width = img.width;
                oc.height = img.height;
                octx.drawImage(img, 0, 0);
                while (oc.width * 0.5 > 500) {
                  oc.width *= 0.5;
                  oc.height *= 0.5;
                  octx.drawImage(oc, 0, 0, oc.width, oc.height);
                }
                oc.width = 500;
                oc.height = oc.width * img.height / img.width;
                octx.drawImage(img, 0, 0, oc.width, oc.height);


                // for thumb
                var tc = document.createElement('canvas'), tctx = tc.getContext('2d');
                tc.width = img.width;
                tc.height = img.height;
                tctx.drawImage(img, 0, 0);
                while (tc.width * 0.5 > 300) {
                  tc.width *= 0.5;
                  tc.height *= 0.5;
                  tctx.drawImage(oc, 0, 0, tc.width, tc.height);
                }
                tc.width = 300;
                tc.height = tc.width * img.height / img.width;
                tctx.drawImage(img, 0, 0, tc.width, tc.height);
                
                var params = {};
                var params = {
                  img_image: oc.toDataURL($scope.mime_type, $scope.quality),
                  img_thumb: tc.toDataURL($scope.mime_type, $scope.quality),
                  img_source: 'facebook',
                  img_status: 'active',
                  index: x
                };  
                promises.push(MemberService.uploadImage(params, x));

                $q.all(promises).then(function(res) {
                  $mdDialog.hide();
                  $window.location.reload();
                }, function(res) {
                });
              } 
            };
            img.src = $scope.importPhotos[x];
          }
        }
      };

      $scope.importFromFb = function() {
        $scope.isImporting = true;
        FB.getLoginStatus(function(response) {
          if(response.status == 'connected') {
            FB.api(
              '/me/photos',
              'GET',
              {"fields":"images,picture", "type":"uploaded"},
              function(response) {
                // Insert your code here
                if(response.paging) {
                  $scope.paging = response.paging;
                }

                $scope.fbPhotos = response.data;
                if($scope.fbPhotos) {
                  $scope.openDialog();
                }
                $scope.isImporting = false;
              }
            );
          }
          else {
            FB.login(function(response) {
              // handle the response
              // $log.debug(response)
              FB.api(
                '/me/photos',
                'GET',
                {"fields":"images,picture", "type":"uploaded"},
                function(response) {
                  // Insert your code here
                  if(response.paging) {
                    $scope.paging = response.paging;
                  }
                  $scope.fbPhotos = response.data;
                  if($scope.fbPhotos) {
                    $scope.openDialog();
                  }
                  $scope.isImporting = false;
                }
              );
            }, {scope: 'public_profile,email,user_photos'});
          }
        });
      };

      $scope.loadMore = function(after) {
        $scope.isLoadingMore = true;
        FB.getLoginStatus(function(response) {
          // $log.debug(response);
          if(response.status == 'connected') {
            FB.api(
              '/me/photos',
              'GET',
              {"fields":"images,picture", "pretty":0, "after":after, "type":"uploaded"},
              function(response) {
                // Insert your code here
                // $log.debug(response);
                if(response.paging.next) {
                  $scope.paging = response.paging;
                }
                for(var x = 0; x < response.data.length; x++) {
                  $scope.fbPhotos.push(response.data[x]);
                }
                $timeout(function() {
                  $location.hash('bottom');
                  $anchorScroll();
                });
                $scope.isLoadingMore = false;
              }
            );
          }
          else {
            FB.login(function(response) {
              // handle the response
              // $log.debug(response)
              FB.api(
                '/me/photos',
                'GET',
                {"fields":"images,picture", "pretty":0, "after":after, "type":"uploaded"},
                function(response) {
                  // Insert your code here
                  // $log.debug(response);
                  if(response.paging.next) {
                    $scope.paging = response.paging;
                  }
                  for(var x = 0; x < response.data.length; x++) {
                    $scope.fbPhotos.push(response.data[x]);
                  }
                  // $log.debug($scope.fbPhotos);
                  $scope.isLoadingMore = false;
                }
              );
            }, {scope: 'public_profile,email,user_photos'});
          }
        });
      };
    };

     $scope.importFromFb = function() {
      $scope.isImporting = true;
      FB.getLoginStatus(function(response) {
        if(response.status == 'connected') {
          FB.api(
            '/me/photos',
            'GET',
            {"fields":"images,picture", "type":"uploaded"},
            function(response) {
              // Insert your code here
              if(response.paging) {
                $scope.paging = response.paging;
              }

              $scope.fbPhotos = response.data;
              if($scope.fbPhotos.length > 0) {
                $scope.openDialog();
              }
              else {
                toastr.warning("No photos to import. You have not uploaded any photo on your facebook account yet.");
              }
              $scope.isImporting = false;
            }
          );
        }
        else {
          FB.login(function(response) {
            // handle the response
            FB.api(
              '/me/photos',
              'GET',
              {"fields":"images,picture", "type":"uploaded"},
              function(response) {
                // Insert your code here
                if(response.paging) {
                  $scope.paging = response.paging;
                }
                $scope.fbPhotos = response.data;
                if($scope.fbPhotos.length > 0) {
                  $scope.openDialog();
                }
                else {
                  toastr.warning("No photos to import. You have not uploaded any photo on your facebook account yet.");
                }
                $scope.isImporting = false;
              }
            );
          }, {scope: 'public_profile,email,user_photos'});
        }
      });
    };

    $scope.loadMore = function(after) {
      // $log.debug(after);
      $scope.isLoadingMore = true;
      FB.getLoginStatus(function(response) {
        // $log.debug(response);
        if(response.status == 'connected') {
          FB.api(
            '/me/photos',
            'GET',
            {"fields":"images,picture", "pretty":0, "after":after, "type":"uploaded"},
            function(response) {
              // Insert your code here
              // $log.debug(response);
              if(response.paging.next) {
                $scope.paging = response.paging;
              }
              for(var x = 0; x < response.data.length; x++) {
                $scope.fbPhotos.push(response.data[x]);
              }
              $timeout(function() {
                $location.hash('bottom');
                $anchorScroll();
              });
              $scope.isLoadingMore = false;
            }
          );
        }
        else {
          FB.login(function(response) {
            // handle the response
            // $log.debug(response)
            FB.api(
              '/me/photos',
              'GET',
              {"fields":"images,picture", "pretty":0, "after":after, "type":"uploaded"},
              function(response) {
                // Insert your code here
                // $log.debug(response);
                if(response.paging.next) {
                  $scope.paging = response.paging;
                }
                for(var x = 0; x < response.data.length; x++) {
                  $scope.fbPhotos.push(response.data[x]);
                }
                // $log.debug($scope.fbPhotos);
                $scope.isLoadingMore = false;
              }
            );
          }, {scope: 'public_profile,email,user_photos'});
        }
      });
    };

    $scope.importPhotos = [];
    $scope.selectImage = function(img) {
      // $log.debug($scope.importPhotos.includes(img.images[0].source));

      if($scope.importPhotos.length < 20) {
        if($scope.importPhotos.includes(img.images[0].source)) {
          var ind = $scope.importPhotos.indexOf(img.images[0].source);
          $scope.importPhotos.splice(ind, 1);
        }
        else {
          $scope.importPhotos.push(img.images[0].source)
        }
        
      } else {

      }
    };

   
    // END STEP 1

    $scope.submit = function(data) {
      // $log.debug(data);
      if($state.current.name == 'account.addPhotos') {
        // old process: requires at least 1 photo
        if($rootScope.uploadedImages.length >= 1) {
          MemberService.updateStep().then(function(res) {
            if(res.success) {
              goog_report_conversion(URL.SITE + 'account/success');
              $timeout(function() {
                $state.go('account.success');
              }, 200);
            }
          });
        }
        else {
          toastr.warning('Please upload at least 1 photo to continue.');
        }
      
      }
      else {
        if($scope.cropping) {
          $scope.crop();
        }

        if($scope.blurring) {
          $scope.blur();
        }
        
        if($scope.rotating) {
          $scope.saveRotate();
        }

        if(!$scope.cropping && !$scope.blurring && !$scope.rotating) {
          $state.go('profile.view');
        }
      }
    };

    $scope.confirmDelete = function(ev) {
      var confirm = $mdDialog.confirm()
        .title('Confirm Delete')
        .textContent('Are you sure you want to delete this image?')
        .ariaLabel('Confirm Delete')
        .targetEvent(ev)
        .ok('Delete')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function() {
        // original process requires at least 1 image
        if($rootScope.uploadedImages.length == 1) {
          toastr.warning('You must have at least 1 photo on your profile.');
        }
        else {
          if($scope.selected.img_id == $scope.user.img_id) {
            toastr.warning('You can\'t delete your profile picture. Choose or upload another image and set it as your profile picture.');
          }
          else {
            var params = {
              img_id: $scope.selected.img_id,
              img_status: 'inactive'
            };

            MemberService.deleteImage(params).then(function(res) {
              $window.location.reload();
              // old process: no image required
              // if($scope.user.img_id == $scope.selected.img_id) {
              //   $scope.user.img_id = null;
              //   var params = {
              //     img_id: null
              //   }
              //   MemberService.makeProfile(params).then(function(res) {
              //     $rootScope.$broadcast('change profile picture', new Array());
              //     $window.location.reload();
              //   });
              // }  
              // else {
              // }            
            });
          }
        }

        //new process does not require image
        // var params = {
        //   img_id: $scope.selected.img_id,
        //   img_status: 'inactive'
        // };

        // MemberService.deleteImage(params).then(function(res) {
        //     var params = {
        //       img_id: null
        //     }
        //     $scope.user.img_id = null;
        //     MemberService.makeProfile(params).then(function(res) {
        //       $rootScope.$broadcast('change profile picture', new Array());
        //       $window.location.reload();
        //     });
        // });

      }, function() {
      });
    };

    $scope.cancel = function() {
      $mdDialog.hide();
    };

    $scope.blurImage = function(imgSrc) {
      $scope.blurring = true;
      $scope.cropping = false;
      $scope.cancelRotate();
      $scope.blurFilter(imgSrc);
    };

    $scope.blurFilter = function(selected){
      var ctx = blurCanvas.getContext('2d'),
      img = new Image,
      value = factor.value;
      img.setAttribute('crossOrigin', 'anonymous');
      img.onload = function(){

        //Set canvas height and width base on image original size
        var canvas = document.getElementById('blurCanvas');
        canvas.height = img.height;
        canvas.width = img.width;

        /// turn off image smoothing (prefixed in some browsers)
        ctx.imageSmoothingEnabled =
        ctx.mozImageSmoothingEnabled =
        ctx.msImageSmoothingEnabled =
        ctx.webkitImageSmoothingEnabled = false;
        
        /// draw the image on canvas
        ctx.drawImage(img, 0, 0, img.width, img.height);
        
        stackBlurCanvasRGBA('blurCanvas', 0, 0, img.width, img.height, value);
        
      }
      img.src = selected;
    };

    $scope.blurCancel = function() {
      $scope.blurring = false;
      var ctx = blurCanvas.getContext('2d');
      ctx.clearRect(0, 0, 420, 420);
    };

    $scope.blur = function() {
      $scope.loading = true;
      var canvas = document.getElementById('blurCanvas');
      var img = new Image(); 
      img.onload = function(){
        var tc = document.createElement('canvas'), tctx = tc.getContext('2d');
        tc.width = img.width;
        tc.height = img.height;
        tctx.drawImage(img, 0, 0);
        while (tc.width * 0.5 > 300) {
          tc.width *= 0.5;
          tc.height *= 0.5;
          tctx.drawImage(oc, 0, 0, tc.width, tc.height);
        }
        tc.width = 300;
        tc.height = tc.width * img.height / img.width;
        tctx.drawImage(img, 0, 0, tc.width, tc.height);
        $scope.blurring = false;
        var params = {
          img_id: $scope.selected.img_id,
          img_image: canvas.toDataURL($scope.mime_type, $scope.quality),
          img_thumb: tc.toDataURL($scope.mime_type, $scope.quality)
        };

        MemberService.blurImage(params).then(function(res) {
          $scope.loading = false;
          $scope.selected = res.data[0];
          var ind = _.findIndex($scope.images, {img_id:res.data[0].img_id});
          $scope.images.splice(ind, 1, res.data[0]);
        });
        var ctx = blurCanvas.getContext('2d');
        ctx.clearRect(0, 0, 420, 420);
      }
      img.src = canvas.toDataURL();
      
    };

    $scope.rotating = false;
    var rotation = 0;
    
    $scope.rotate = function(selected) {
      $scope.rotating = true;
      $scope.cropping = false;
      $scope.blurring = false;
      var canvasP = document.getElementById("imageCanvas").parentElement;
      var canvas = document.getElementById("imageCanvas");
      var ctx = canvas.getContext("2d");
      canvas.style.width ='100%';
      canvas.style.height ='100%';
      // ...then set the internal size to match
      canvas.width  = canvas.offsetWidth;
      canvas.height = canvas.offsetHeight;
      //canvas dimensions
      var W = canvasP.offsetWidth;
      var H = canvasP.offsetHeight;
      var img = new Image();
      img.setAttribute('crossOrigin', 'anonymous');
      img.onload = function() {
      
        var ratio = 0;
        var imgWidth = 0;
        var imgHeight = 0;
        var posX = 0;
        var posY = 0;

        if(img.width > W) {
          ratio = W / img.width;
        }
        else{
          ratio = img.width / W;
        }

        rotation++;
        if(rotation % 2 == 0)
        {
          canvas.style.width = W + "px";
          canvas.style.height = (img.height * ratio) + "px";
          canvas.width  = W;
          canvas.height = img.height * ratio;

          imgWidth = W;
          imgHeight = img.height * ratio;
          posX = -imgWidth / 2;
          posY = -imgHeight / 2;  
        }
        else
        {
          canvas.style.height = W + "px";
          canvas.style.width = (img.height * ratio) + "px";
          canvas.height  = W;
          canvas.width = img.height *  ratio;

          imgWidth = img.height * ratio;
          imgHeight = W;

          posX = -imgHeight / 2;
          posY = -imgWidth / 2;
        }

        if(rotation > 3) {
          rotation = 0;
        }
        
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        // ctx.beginPath();
        // ctx.fillStyle = "rgba(255, 255, 255, 0.8)";
        ctx.translate(canvas.width/2, canvas.height/2);
        ctx.rotate((90 * (rotation * 1))*Math.PI/180);
        
        ctx.drawImage(img, posX, posY, W, img.height * ratio);
        ctx.translate(-canvas.width/2, -canvas.height/2);

        ctx.fill();
      }
      img.src = selected;
      
    };

    $scope.cancelRotate = function() {
      $scope.rotating = false;
      $scope.degrees = false;
      var canvas = document.getElementById('imageCanvas');
      var ctx = canvas.getContext('2d');
      ctx.setTransform(1, 0, 0, 1, 0, 0);
      ctx.clearRect(0, 0, 420, 420);
    };

    $scope.saveRotate = function() {
      $scope.loading = true;
      var canvas = document.getElementById('imageCanvas');
      var img = new Image(); 
      img.onload = function(){
        var tc = document.createElement('canvas'), tctx = tc.getContext('2d');
        tc.width = img.width;
        tc.height = img.height;
        tctx.drawImage(img, 0, 0);
        while (tc.width * 0.5 > 300) {
          tc.width *= 0.5;
          tc.height *= 0.5;
          tctx.drawImage(oc, 0, 0, tc.width, tc.height);
        }
        tc.width = 300;
        tc.height = tc.width * img.height / img.width;
        tctx.drawImage(img, 0, 0, tc.width, tc.height);
        $scope.rotating = false;
        var params = {
          img_id: $scope.selected.img_id,
          img_image: canvas.toDataURL($scope.mime_type, $scope.quality),
          img_thumb: tc.toDataURL($scope.mime_type, $scope.quality)
        };

        MemberService.cropImage(params).then(function(res) {
          // $log.debug(res);
          $scope.loading = false;
          $scope.selected = res.data[0];
          var ind = _.findIndex($scope.images, {img_id:res.data[0].img_id});
          $scope.images.splice(ind, 1, res.data[0]);
        });
        var ctx = imageCanvas.getContext('2d');
        ctx.clearRect(0, 0, 420, 420);
      }
      img.src = canvas.toDataURL();
      
    };

    $scope.toggleCrop = function() {
      $scope.cropping = true;
      $scope.blurring = false;
      $scope.cancelRotate();
      $scope.minSize = 200;
      $rootScope.$broadcast('toggleCrop', $scope.cropping);
    };

    $scope.$on('toggleCrop', function(events, args) {
      $scope.cropping = args;
      $scope.minSize = 420;
    });
    
    $scope.croppedImage = '';
    $scope.crop = function() {
      $scope.loading = true;
      $scope.cropping = false;
      var img = new Image();
      img.onload = function() {
        if (img.width) {
          // for thumb
          var tc = document.createElement('canvas'), tctx = tc.getContext('2d');
          tc.width = img.width;
          tc.height = img.height;
          tctx.drawImage(img, 0, 0);
          while (tc.width * 0.5 > 300) {
            tc.width *= 0.5;
            tc.height *= 0.5;
            tctx.drawImage(oc, 0, 0, tc.width, tc.height);
          }
          tc.width = 300;
          tc.height = tc.width * img.height / img.width;
          tctx.drawImage(img, 0, 0, tc.width, tc.height);
          
          var params = {
            img_id: $scope.selected.img_id,
            img_image: $scope.croppedImage,
            img_thumb: tc.toDataURL($scope.mime_type, $scope.quality),
            img_status: 'active'
          };
          MemberService.cropImage(params).then(function(res) {
            // $log.debug(res);
            $scope.loading = false;
            $scope.selected = res.data[0];
            var ind = _.findIndex($scope.images, {img_id:res.data[0].img_id});
            $scope.images.splice(ind, 1, res.data[0]);
          });
        } 
      };
      img.src = $scope.croppedImage;
    };

    $scope.makeProfile = function(id) {
      var params = {
        img_id: id
      }
      $scope.user.img_id = id;
      MemberService.makeProfile(params).then(function(res) {
        if(res.success) {
          toastr.success('Image set as display picture.');
          $rootScope.$broadcast('change profile picture', res.data[0]);
        } else {
          toastr.error(res.error.message);
        }
      });
    }; 

    $scope.setPrivacy = function(img) {
      if(img.img_status == 'active') {
        var option = 'private';
      }
      else {
        var option = 'public';
      }
      var confirm = $mdDialog.confirm()
        .title('Change Privacy')
        .textContent('Are you sure you want to set this image ' +  option +' ?')
        .ariaLabel('Confirm Privacy')
        .targetEvent()
        .ok('OK')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function() {
        if($scope.user.img_id == img.img_id) {
          toastr.warning('Default picture can\'t be set to private.');
        }
        else {
          var params = {
            img_id: img.img_id,
            img_status: ''
          }
          if(img.img_status == 'active') {
            params.img_status = 'private';
          }
          else {
            params.img_status = 'active';
          }


          MemberService.imagePrivacy(params).then(function(res) {
            if(res.success) {
              var ind = _.findIndex($scope.images, {img_id:res.data[0].img_id});
              $scope.images.splice(ind, 1, res.data[0]);
              if(res.data[0].img_status == 'private') {
                toastr.success('Image set as private.');
              }
              else {
                toastr.success('Image set as public.');
              }
            }
          }, function() {
          });
        }
      });
    };
  }
})();
