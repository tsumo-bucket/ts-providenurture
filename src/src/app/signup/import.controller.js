(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('ImportController', ImportController);

  /** @ngInject */
  function ImportController($timeout, webDevTec, toastr, $scope, $window, $q, MemberService, $location, $anchorScroll, FilterFactory, $mdDialog, $mdToast, $log, $state, userData, $rootScope, SettingsService, Upload) {

    if(userData.isLogged()) {
      $rootScope.user = angular.fromJson(userData.isLogged(), true);
      $scope.user = $rootScope.user;
      $scope.prf_id1 = $rootScope.user.prf_id1;
      $scope.prf_id2 = $rootScope.user.prf_id2;
      $scope.inf_id = $rootScope.user.inf_id;
    }
    else {
      $window.location.reload();
      $timeout(function(){
        if(!userData.isLogged()) {
          $state.go('home');
          $rootScope.actionLoader = false;
        }
      }, 100);
    }

    $scope.isWebcam ="false";
    $scope.uploadMethod = '';

    $scope.images = [
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''}
    ];

    MemberService.getImages().then(function(res) {
      $log.debug(res);
      if(res.success) {
        console.log(res.data[0].length);
        $scope.images.splice(0, res.data[0].length)
        for(var x= 0; x < res.data[0].length;x++ ) {
          $scope.images.unshift(res.data[0][x]);
        }
        $scope.selected = res.data[0][0];
      }
      console.log($scope.images);
    });

    $scope.select = function(img) {
      $log.debug(img);
      $scope.selected = img;
      $scope.showCam = false;
    };

    if($state.current.name == "account.success") {
      $scope.end = true;
    }
    else if($state.current.name == "account.preferences") {
      $scope.preferences = true;
    }
    else if($state.current.name == "account.information") {
      $scope.info = true;
    }

    //STEP 1
    // VIA UPLOAD FROM COMPUTER
    $scope.upload = function (file) {
      $log.debug(file)
        console.log(_.findIndex($scope.images, {src:''}));         
      var reader = new window.FileReader();
      reader.readAsDataURL(file); 
      reader.onloadend = function() {
        var base64data = reader.result;    
        var params = {
          img_image: base64data,
          img_source: 'upload',
          img_status: 'active'
        };   
        MemberService.uploadImage(params).then(function(res) {
          $log.debug(res);
          if(res.success) {
            var ind = _.findIndex($scope.images, {src:''});
            $scope.images.splice(ind, 1, res.data[0]);
            $scope.selected = res.data[0];
          }
          else {
            toastr.error(res.error.message);
          }
        }, function(res) {
          $log.debug(res);
        });
      }
    };
    // END UPLOAD FROM COMPUTER METHOD

    // VIA WEBCAM
    $scope.showCam = false;
    $scope.webcam = function() {
      $scope.uploadMethod = 'webcam';
      if(!$scope.showCam) {
        $scope.showCam = true;
      }
      else {
        $scope.showCam = false;
      }
      // Grab elements, create settings, etc.
      var video = document.getElementById('video');

      // Get access to the camera!
      if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
          // Not adding `{ audio: true }` since we only want video now
          navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
              video.src = window.URL.createObjectURL(stream);
              video.play();
          });
      }
    };

    $scope.capture = function() {
      // Elements for taking the snapshot
      var canvas = document.getElementById('canvas');
      var context = canvas.getContext('2d');
      var video = document.getElementById('video');
      $scope.showCam = false;

      // Trigger photo take
      document.getElementById("snap").addEventListener("click", function() {
        context.drawImage(video, 0, 0, 640, 480);
        // $scope.images[0].src = canvas.toDataURL("image/png");
        var params = {
          img_image: canvas.toDataURL("image/png"),
          img_source: 'webcam',
          img_status: 'active'
        };            
        MemberService.uploadImage(params).then(function(res) {
          $log.debug(res);
          if(res.success) {
            var ind = _.findIndex($scope.images, {src:''});
            $scope.images.splice(ind, 1, res.data[0]);
            $scope.selected = res.data[0];
          }
          else {
            toastr.error(res.error.message);
          }
        }, function(res) {
          $log.debug(res);
        });
      });

    };
    // END WEBCAM METHOD

    // IMPORT FROM FACEBOOK
    $scope.openDialog = function(ev) {
      $scope.$watch('fbPhotos');
      $mdDialog.show({
        controller: 'ImportController',
        scope: $scope,
        templateUrl: 'app/signup/import.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false // Only for -xs, -sm breakpoints.,
      })
      .then(function(answer) {
        $log.debug(answer);
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
    };

    $scope.cancel = function() {
      $window.location.reload();
    };

    $scope.importFromFb = function() {
      $scope.isImporting = true;
      FB.getLoginStatus(function(response) {
        $log.debug(response);
        if(response.status == 'connected') {
          FB.api(
            '/me/photos',
            'GET',
            {"fields":"images,picture", "type":"uploaded"},
            function(response) {
              // Insert your code here
              $log.debug(response);
              if(response.paging.next) {
                $scope.paging = response.paging;
              }

              $scope.fbPhotos = response.data;
              if($scope.fbPhotos) {
                $scope.openDialog();
              }
              $scope.isImporting = false;
            }
          );
        }
        else {
          FB.login(function(response) {
            // handle the response
            $log.debug(response)
            FB.api(
              '/me/photos',
              'GET',
              {"fields":"images,picture", "type":"uploaded"},
              function(response) {
                // Insert your code here
                $log.debug(response);
                if(response.paging.next) {
                  $scope.paging = response.paging;
                }
                $scope.fbPhotos = response.data;
                if($scope.fbPhotos) {
                  $scope.openDialog();
                }
                $scope.isImporting = false;
              }
            );
          }, {scope: 'public_profile,email,user_photos'});
        }
      });
    };

    $scope.loadMore = function(after) {
      $log.debug(after);
      $scope.isLoadingMore = true;
      FB.getLoginStatus(function(response) {
        $log.debug(response);
        if(response.status == 'connected') {
          FB.api(
            '/me/photos',
            'GET',
            {"fields":"images,picture", "pretty":0, "after":after, "type":"uploaded"},
            function(response) {
              // Insert your code here
              $log.debug(response);
              if(response.paging.next) {
                $scope.paging = response.paging;
              }
              for(var x = 0; x < response.data.length; x++) {
                $scope.fbPhotos.push(response.data[x]);
              }
              $timeout(function() {
                $location.hash('bottom');
                $anchorScroll();
              });
              $scope.isLoadingMore = false;
            }
          );
        }
        else {
          FB.login(function(response) {
            // handle the response
            $log.debug(response)
            FB.api(
              '/me/photos',
              'GET',
              {"fields":"images,picture", "pretty":0, "after":after, "type":"uploaded"},
              function(response) {
                // Insert your code here
                $log.debug(response);
                if(response.paging.next) {
                  $scope.paging = response.paging;
                }
                for(var x = 0; x < response.data.length; x++) {
                  $scope.fbPhotos.push(response.data[x]);
                }
                $log.debug($scope.fbPhotos);
                $scope.isLoadingMore = false;
              }
            );
          }, {scope: 'public_profile,email,user_photos'});
        }
      });
    };

    $scope.importPhotos = [];
    $scope.selectImage = function(img) {
      $log.debug($scope.importPhotos.includes(img.images[0].source));
      if($scope.importPhotos.length < 12) {
        if($scope.importPhotos.includes(img.images[0].source)) {
          var ind = $scope.importPhotos.indexOf(img.images[0].source);
          $log.debug(ind);
          $scope.importPhotos.splice(ind, 1);
          $log.debug($scope.importPhotos);
        }
        else {
          $scope.importPhotos.push(img.images[0].source)
        }
        
      } else {

      }
    }

    $scope.import = function() {

      if(($rootScope.uploadedImages.length + $scope.importPhotos.length) > 12) {
        toastr.warning('You are only allowed to upload a maximum of 12 photos.');
      }
      else {
        var promises = [];
        var i = 1;
        for(var x = 0; x < $scope.importPhotos.length; x++) {
          console.log($scope.importPhotos);
          console.log($scope.importPhotos[x]);
          var img = new Image();
          img.setAttribute('crossOrigin', 'anonymous');
          img.onload = function() {
            if (img.width) {
              // for resizing big image
              var oc = document.createElement('canvas'), octx = oc.getContext('2d');
              oc.width = img.width;
              oc.height = img.height;
              octx.drawImage(img, 0, 0);
              while (oc.width * 0.5 > 500) {
                oc.width *= 0.5;
                oc.height *= 0.5;
                octx.drawImage(oc, 0, 0, oc.width, oc.height);
              }
              oc.width = 500;
              oc.height = oc.width * img.height / img.width;
              octx.drawImage(img, 0, 0, oc.width, oc.height);


              // for thumb
              var tc = document.createElement('canvas'), tctx = tc.getContext('2d');
              tc.width = img.width;
              tc.height = img.height;
              tctx.drawImage(img, 0, 0);
              while (tc.width * 0.5 > 300) {
                tc.width *= 0.5;
                tc.height *= 0.5;
                tctx.drawImage(oc, 0, 0, tc.width, tc.height);
              }
              tc.width = 300;
              tc.height = tc.width * img.height / img.width;
              tctx.drawImage(img, 0, 0, tc.width, tc.height);
              
              var params = {};
              var params = {
                img_image: oc.toDataURL(),
                img_thumb: tc.toDataURL(),
                img_source: 'facebook',
                img_status: 'active',
                index: x
              };  
              promises.push(MemberService.uploadImage(params, x));

              $q.all(promises).then(function(res) {
                $window.location.reload();
               
              }, function(res) {
              });
            } 
          };
          img.src = $scope.importPhotos[x];
        }
      }
    };
    // END STEP 1

    $scope.submit = function(data) {
      $log.debug(data);
      if($rootScope.uploadedImages.length >= 3) {
        $state.go('account.personal');
      }
      else {
        toastr.error('Please upload at least 3 images to proceed.');
      }
    }

    $scope.confirmDelete = function(ev) {
      var confirm = $mdDialog.confirm()
        .title('Confirm Delete')
        .textContent('Are you sure you want to delete this image?')
        .ariaLabel('Confirm Delete')
        .targetEvent(ev)
        .ok('Delete')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function() {
        $log.debug($scope.selected);
        var params = {
          img_id: $scope.selected.img_id,
          img_status: 'inactive'
        };

        MemberService.deleteImage(params).then(function(res) {
          $log.debug(res);
          $rootScope.uploadedImages = res.data[1];
          $scope.selected = res.data[1][0];
          toastr.success('Image Deleted');
        });
      }, function() {
      });
    };

    $scope.croppedImage = '';
    $scope.crop = function() {
      var params = {
        img_id: $scope.selected.img_id,
        img_image: $scope.croppedImage,
        img_status: 'active'
      };
      MemberService.cropImage(params).then(function(res) {
        $log.debug(res);
        $scope.images = res.data[1];
        $scope.selected = res.data[1][0];
        $scope.cropping = false;
      });
    };

  }
})();
