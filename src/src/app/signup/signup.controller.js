(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('SignupController', SignupController);

  /** @ngInject */
  function SignupController($timeout, $http, webDevTec, toastr, $scope, $q, MemberService, URL, $location, logged, $window, $anchorScroll, FilterFactory, $mdDialog, $mdToast, $log, $state, userData, $rootScope, SettingsService, Upload) {
    $('.navbar').addClass('transluscent');
    var _isNotMobile = (function() {
    var check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
          return !check;
    })();

    if (!_isNotMobile) {
      $rootScope.mainlayout = "mobile";
    }
    else
    {
      $rootScope.mainlayout = "";
    }

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
      if(toState.name == 'account.personal' && fromState.name != 'join3') {
        $rootScope.showError = true;
      }     
      if(toState.name == 'account.preferences' && fromState.name != 'account.personal') {
        $rootScope.showError = true;
      }     

    });

    $scope.options = {
      types: ['(cities)'],
    };

    $scope.editorConfig = {
      sanitize: false,
      toolbar: [
        { name: 'basicStyling', items: ['bold', 'italic', 'underline', 'strikethrough'] },
        { name: 'paragraph', items: ['orderedList', 'unorderedList'] }
      ]
    };

    $scope.address = null;
    $scope.components = null;

    if(logged) {
      $rootScope.user = angular.fromJson(userData.isLogged(), true);
      $scope.user = $rootScope.user;
      SettingsService.getInfoList().then(function(res) {
        if(res.success) {

          $scope.user = res.data[0][0];
          if(res.data[0].inf_preferred_range_to == 0 || res.data[0].inf_preferred_range_to == null) {
            $scope.user.inf_preferred_range_to = 10000;
          }
          else {
            $scope.user.inf_preferred_range_to = res.data[0].inf_preferred_range_to;
          }

          if(!res.data[0].inf_preferred_range_from) {
            $scope.user.inf_preferred_range_from = 100;
          }
          else {
            $scope.user.inf_preferred_range_from = res.data[0].inf_preferred_range_from;
          }

          if($scope.user.inf_quality_time == 'yes') {
            $scope.user.inf_quality_time = true;
          } 
          else {
            $scope.user.inf_quality_time = false;
          }
          if($scope.user.inf_gifts == 'yes') {
            $scope.user.inf_gifts = true;
          } 
          else {
            $scope.user.inf_gifts = false;
          }
          if($scope.user.inf_staycations == 'yes') {
            $scope.user.inf_staycations = true;
          } 
          else {
            $scope.user.inf_staycations = false;
          }
          if($scope.user.inf_high_life == 'yes') {
            $scope.user.inf_high_life = true;
          } 
          else {
            $scope.user.inf_high_life = false;
          }
          if($scope.user.inf_simple == 'yes') {
            $scope.user.inf_simple = true;
          } 
          else {
            $scope.user.inf_simple = false;
          }
          if($scope.user.inf_other) {
            $scope.user.inf_other_value = true;
            $scope.user.other = $scope.user.inf_other;
          } 
          else {
            $scope.user.inf_other = false;
          }
          SettingsService.getPreferences().then(function(res) {
            $scope.prf_id1 = res.data[0][0].prf_id;
            $scope.prf_id2 = res.data[0][1].prf_id;
            $scope.user.describe = res.data[0][0].prf_content;
            $scope.user.relationship = res.data[0][1].prf_content;
            if(res.data[0][2]) {
              $scope.prf_id3 = res.data[0][2].prf_id;
              $scope.user.other_question = res.data[0][2].que_id;
              $scope.user.other_answer = res.data[0][2].prf_content;
            }
          });
        }
      });

      if($rootScope.user.usr_signup_step == 'done' && $rootScope.user.usr_status == 'approved') {
        $state.go('members');
      }
    }
  

    $scope.filter = FilterFactory;
    $scope.isWebcam ="false";
    $scope.uploadMethod = '';
    $rootScope.user = {
      usr_gender: ''
    };

    $scope.photos = [
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''},
      {src: ''}
    ];

    if($state.current.name == "account.success") {
      $scope.end = true;
    }
    else if($state.current.name == "account.preferences") {
      $scope.preferences = true;
    }
    else if($state.current.name == "account.personal") {
      $scope.info = true;
    }
   
    $scope.minRangeSlider = {
      options: {
          floor: 100,
          ceil: 10000,
          step: 100
      }
    };

    $scope.changeGender = function() {
      $scope.user.usr_type = '';
      $scope.userForm.usr_type.$setPristine();
    }

    $scope.changeAge = function(usr_type) {
      if(usr_type == 'sugar') {
        $scope.user.usr_interested_age_from = '18',
        $scope.user.usr_interested_age_to = '25'
      }
      else {
        $scope.user.usr_interested_age_from = '30',
        $scope.user.usr_interested_age_to = '50'
      }
    };

    $scope.user.usr_interested_age_from = '18';
    $scope.user.usr_interested_age_to = '80';

    $scope.heightConversion = function(height) {
      $scope.conversion = '';
      
      if(height % 1 === 0){
        var realFeet = ((height*0.393700) / 12);
        var feet = Math.floor(realFeet);
        var inches = Math.round((realFeet - feet) * 12);

        if(height != null){
          if(feet != 0){
            $scope.conversion = feet + "ft " + inches + 'in';
          }else{
            $scope.conversion = Math.round((realFeet) * 12) + 'in';
          }
        }
      }
    };

    $scope.changeDays = function(month) {
      $scope.filter.days = [];
      if(month == 2) {
        var x = 0;
        for (var d = 1; d <= 28; d++)
        {
          $scope.filter.days[x] = {
              name: d,
              value: d
          };
          x++;
        }
      }
      else if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
        var x = 0;
        for (var d = 1; d <= 31; d++)
        {
          $scope.filter.days[x] = {
              name: d,
              value: d
          };
          x++;
        }
      }
      else if(month == 4 || month == 6 || month == 9 || month == 11) {
        var x = 0;
        for (var d = 1; d <= 30; d++)
        {
          $scope.filter.days[x] = {
              name: d,
              value: d
          };
          x++;
        }
      }
    };
    

    // STEP 2
    $scope.saveInfo = function(data) {

      var promises = [];

      if($scope.infoForm.$invalid) {
        $scope.submitted = true;
        toastr.warning('Please make sure that you have completed all the fields correctly.');
      } 
      else {

        $rootScope.actionLoader = true;
        var info = {
          inf_id: data.inf_id,
          inf_height: data.inf_height,
          inf_height_unit: 'cm',
          inf_body_type: data.inf_body_type,
          inf_ethnicity: data.inf_ethnicity,
          inf_relationship: data.inf_relationship,
          inf_drink: data.inf_drink,
          inf_children: data.inf_children,
          inf_language: data.inf_language,
          inf_smoke: data.inf_smoke,
          inf_education: data.inf_education,
          inf_occupation: data.inf_occupation
        };

        var user = {
          usr_description: data.usr_description
        };

        var pref1 = {
          prf_id: $scope.prf_id1,
          prf_content: data.describe
        };

        promises.push(SettingsService.updateQuestion(pref1));

        var pref2 = {
          prf_id: $scope.prf_id2,
          prf_content: data.relationship
        };

        promises.push(SettingsService.updateQuestion(pref2));
        
        if(data.other_question && data.other_answer) {
          var pref3 = {
            que_id: data.other_question,
            prf_content: data.other_answer
          };
         
          promises.push(SettingsService.createPreference(pref3));
        }

        MemberService.editDescription(user).then(function(res) {
          if(res.success) {
            userData.setUserData('usr_signup_step', res.data[0].usr_signup_step);
            SettingsService.updateInfo(info).then(function(res) {
              if(res.success) {
                $q.all(promises).then(function(res) {
                  // $log.debug(res);
                  // userData.setUserData('usr_signup_step', 'preference');
                  $rootScope.actionLoader = false;
                  $state.go('account.preferences');
                  // goog_report_conversion(URL.SITE + 'account/preferences');
                }, function(err) {
                  $rootScope.actionLoader = false;
                  toastr.warning('Something went wrong. Please try again.');
                  // $log.debug(err);
                });
              }
            });
          }
        });
      }
    };
    // END STEP 2

    // STEP 3
    $scope.savePreference = function(data) {
      // $log.debug(data);
      if($scope.prefForm.$invalid) {
        $scope.submitted = true;
        toastr.warning('Please make sure that you have completed all the fields correctly.');
      } 
      else {
        $rootScope.actionLoader = true;
        var params = {
          inf_id: $scope.inf_id,
          inf_spending_habits: data.inf_spending_habits,
          inf_preferred_range_from: data.inf_preferred_range_from,
          inf_preferred_range_to: data.inf_preferred_range_to,
          inf_orientation: data.inf_orientation,
          inf_relationship_length: data.inf_relationship_length,
          inf_relationship_loyalty: data.inf_relationship_loyalty,
          inf_privacy_expectations: data.inf_privacy_expectations, 
          inf_relationship_preference: data.inf_relationship_preference, 
          inf_sexual_limit: data.inf_sexual_limit
        };

        if($scope.user.usr_type == 'sugar') {
          params.inf_yearly_income = data.inf_yearly_income;
          params.inf_net_worth = data.inf_net_worth;
        }

        if(data.inf_gifts) {
          params.inf_gifts = 'yes';
        } 
        else {
          params.inf_gifts = 'no';
        }
        if(data.inf_other_value || data.other) {
          params.inf_other = data.other;
        }
        else {
          params.inf_other = '';
        }
        
        if(data.inf_staycations) {
          params.inf_staycations = 'yes';
        }
        else {
          params.inf_staycations = 'no';
        }
        if(data.inf_high_life) {
          params.inf_high_life = 'yes';
        }
        else {
          params.inf_high_life = 'no';
        }
        if(data.inf_quality_time) {
          params.inf_quality_time = 'yes';
        }
        else {
          params.inf_quality_time = 'no';
        }
        if(data.inf_simple) {
          params.inf_simple = 'yes';
        }
        else {
          params.inf_simple = 'no';
        }

        SettingsService.updateInfo(params).then(function(res) {
          $rootScope.actionLoader = false;
          if(res.success) {
            // userData.setUserData('usr_signup_step', 'done');
            userData.setUserData('usr_signup_step', res.data[0].usr_signup_step);
            $state.go('account.addPhotos');
            // goog_report_conversion(URL.SITE + 'account/add-photos');
          }
        });
      }
    };
    // END STEP 3

    // GET CITIES
    $scope.getCities = function(reg_id) {
      SettingsService.getCities(reg_id).then(function(res){
        if(res.success) {
          $scope.cities = res.data[0];
        }
      });
    };

    //GET REGIONS
    $scope.getRegions = function(cnt_id) {
      SettingsService.getRegions(cnt_id).then(function(res){
        if(res.success) {
          $scope.regions = res.data[0];
        }
      });
    };

    $scope.resend = function() {
      MemberService.resendConfirmation({usr_email: $scope.user.usr_email}).then(function(res) {
        if(res.success) {
          toastr.success('Please check your email.');
        }
      });
    };

    $scope.confirmDelete = function(ev) {
      var confirm = $mdDialog.confirm()
        .title('Confirm Delete')
        .textContent('Are you sure you want to delete this image?')
        .ariaLabel('Confirm Delete')
        .targetEvent(ev)
        .ok('Delete')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function() {
        // $log.debug($scope.selected);
        var params = {
          img_id: $scope.selected.img_id,
          img_status: 'inactive'
        };

        MemberService.deleteImage(params).then(function(res) {
          // $log.debug(res);
          $scope.photos = res.data[1];
          $scope.selected = res.data[1][0];
          toastr.success('Image Deleted');
        });
      }, function() {
      });
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };
    // userData.destroyTemp();
    // console.log(userData.getTemp());
   
  }
})();
