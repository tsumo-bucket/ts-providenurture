(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('UserController', UserController);

  /** @ngInject */
  function UserController($timeout, webDevTec, logged, toastr, $scope, $window, $log, userData, MemberService, MessageService, FilterFactory, $mdDialog, $mdToast, $state, $stateParams, $rootScope, SettingsService) {
    $rootScope.user = angular.fromJson(userData.isLogged());
    $rootScope.$broadcast('login', $rootScope.user);
    $scope.logged = $rootScope.user;
    $rootScope.actionLoader = true;
    $('.navbar').addClass('transluscent');

    $scope.filter = FilterFactory;
    if(logged) {
      $scope.user = logged;
      $rootScope.$broadcast('login', $scope.user);
      $state.go('user', {usr_token_id: $stateParams.usr_token_id});
      $rootScope.actionLoader = true;
      if($stateParams.usr_token_id == $scope.user.usr_token_id) {
        $state.go('profile.view');
      }
      else {
        MemberService.viewProfile($stateParams.usr_token_id).then(function(res) {
          $rootScope.actionLoader = false;
          if(res.success) {
            $scope.user = res.data[0];
            $scope.user.info = res.data[1][0];
            $scope.user.preference = res.data[2][0];
            if(res.data[3].message) {
              $scope.user.images = [];
            }
            else {
              $scope.user.images = res.data[3];
            }
            if(res.data[4].message) {
              $scope.user.posts = [];
            }
            else {
              $scope.user.posts = res.data[4];
            }
            $scope.user.faved = res.data[5];
            $scope.user.followed = res.data[6];
            $scope.selected = res.data[3][0]; 

            if(res.data[1][0].inf_quality_time == 'yes') {
              $scope.user.info.inf_quality_time = true;
            }
            else {
              $scope.user.info.inf_quality_time = false;
            }
            if(res.data[1][0].inf_gifts == 'yes') {
              $scope.user.info.inf_gifts = true;
            }
            else {
              $scope.user.info.inf_gifts = false;
            }
            if(res.data[1][0].inf_travel == 'yes') {
              $scope.user.info.inf_travel = true;
            }
            else {
              $scope.user.info.inf_travel = false;
            }
            if(res.data[1][0].inf_staycations == 'yes') {
              $scope.user.info.inf_staycations = true;
            }
            else {
              $scope.user.info.inf_staycations = false;
            }
            if(res.data[1][0].inf_high_life == 'yes') {
              $scope.user.info.inf_high_life = true;
            }
            else {
              $scope.user.info.inf_high_life = false;
            }
            if(res.data[1][0].inf_simple == 'yes') {
              $scope.user.info.inf_simple = true;
            }
            else {
              $scope.user.info.inf_simple = false;
            }

            $scope.about = res.data[2][0];
            $scope.relationship = res.data[2][1];
            if(res.data[2][2]) {
              $scope.extra = res.data[2][2];
            }
          
          }
          else {
            $scope.error = true;
          }
        });
      }
    }
    else {
      $timeout(function(){
        if(!logged) {
          $state.go('login', {url: 'user', token: $stateParams.usr_token_id});
          $rootScope.actionLoader = false;
        }
      });
    }

    $scope.aboutOpened = true;
    $scope.postsOpened = false;

    $scope.openAbout = function() {
      $scope.aboutOpened = true;
      $scope.postsOpened = false;
    };

    $scope.openPosts = function() {
      $scope.aboutOpened = false;
      $scope.postsOpened = true;
    };

    $scope.select = function(img) {
      $scope.selected = img;
    };

    $scope.msg = {
      msg_content: ''
    };
    
    $scope.sendMessage = function(msg) {
      if(msg) {
        $scope.sending = true;
        var params = {
          msg_user: $scope.user.usr_id,
          msg_content: msg
        }
        MessageService.sendMessage(params).then(function(res) {
          $scope.msg.msg_content = '';
          $scope.sending = false;
          if(res.success) {
            toastr.success('Sent!');
          }
        });
      };
    };

    $scope.sendOnEnter = function(msg, evt) {
      if(evt.keyCode == 13) {
        $scope.sendMessage(msg);
      }
    };

    $scope.other_user = $stateParams.usr_token_id;   

  }
})();
