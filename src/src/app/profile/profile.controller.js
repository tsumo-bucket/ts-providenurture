(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('ProfileController', ProfileController);

  /** @ngInject */
  function ProfileController($timeout, webDevTec, toastr, $scope, $log, $q, $window, logged, MemberService, FilterFactory, $mdDialog, $mdToast, $state, $rootScope, SettingsService, userData) {
    var _isNotMobile = (function() {
    var check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
          return !check;
    })();

    if (!_isNotMobile) {
      $rootScope.mainlayout = "mobile";
    }
    else
    {
      $rootScope.mainlayout = "";
    }

    if(logged) {
      // $rootScope.actionLoader = false;
    }
    else {
      // $window.location.reload();
      $timeout(function(){
        if(!userData.isLogged()) {
          $state.go('home');
          $rootScope.actionLoader = false;
        }
      }, 100);
    }

    $scope.editorConfig = {
      sanitize: false,
      toolbar: [
        { name: 'basicStyling', items: ['bold', 'italic', 'underline', 'strikethrough'] },
        { name: 'paragraph', items: ['orderedList', 'unorderedList'] }
      ]
    }

    $scope.filter = FilterFactory;
    $rootScope.actionLoader = true;
    $scope.aboutOpened = true;
    $scope.postsOpened = false;

    $scope.openAbout = function() {
      $scope.aboutOpened = true;
      $scope.postsOpened = false;
    };

    $scope.openPosts = function() {
      $scope.aboutOpened = false;
      $scope.postsOpened = true;
    };

    // GET CITIES
    $scope.getCities = function(reg_id) {
      SettingsService.getCities(reg_id).then(function(res){
        if(res.success) {
          $scope.cities = res.data[0];
        }
      });
    };

    //GET REGIONS
    $scope.getRegions = function(cnt_id) {
      SettingsService.getRegions(cnt_id).then(function(res){
        if(res.success) {
          $scope.regions = res.data[0];
        }
      });
    };

    // $scope.getRegions($rootScope.user.reg_id);
    // $scope.getCities($rootScope.user.cty_id);

    $scope.select = function(img) {
      $scope.selected = img;
    };

    $scope.$on('selected', function(events, args){
      // console.log(args);
      $scope.selected = args; //now we've registered!
    })

    MemberService.getProfile().then(function(res) {
      $rootScope.actionLoader = false;
      $scope.user = res.data[0];
      $scope.user.info = res.data[1][0];
      $scope.user.preference = res.data[2][0];
      $scope.user.images = res.data[3];
      $scope.selected = res.data[3][0];
      if(res.data[4].message) {
        $scope.user.posts = [];
      }
      else {
        $scope.user.posts = res.data[4];
      }

      $rootScope.$broadcast('selected', $scope.selected)

      if(res.data[1][0].inf_quality_time == 'yes') {
        $scope.user.info.inf_quality_time = true;
      }
      else {
        $scope.user.info.inf_quality_time = false;
      }
      if(res.data[1][0].inf_gifts == 'yes') {
        $scope.user.info.inf_gifts = true;
      }
      else {
        $scope.user.info.inf_gifts = false;
      }
      if(res.data[1][0].inf_travel == 'yes') {
        $scope.user.info.inf_travel = true;
      }
      else {
        $scope.user.info.inf_travel = false;
      }
      if(res.data[1][0].inf_staycations == 'yes') {
        $scope.user.info.inf_staycations = true;
      }
      else {
        $scope.user.info.inf_staycations = false;
      }
      if(res.data[1][0].inf_high_life == 'yes') {
        $scope.user.info.inf_high_life = true;
      }
      else {
        $scope.user.info.inf_high_life = false;
      }
      if(res.data[1][0].inf_simple == 'yes') {
        $scope.user.info.inf_simple = true;
      }
      else {
        $scope.user.info.inf_simple = false;
      }
      if(res.data[1][0].inf_other) {
        $scope.user.info.inf_other_value = true;
        $scope.user.info.other = $scope.user.info.inf_other;
      } 
      else {
        $scope.user.info.inf_other_value = false;
      }
      $scope.about = res.data[2][0];
      $scope.relationship = res.data[2][1];

      if(res.data[2][2]) {
        $scope.extra = res.data[2][2];
      }
      $scope.user.birthday = new Date($scope.user.usr_birthyear + '-' + $scope.user.usr_birthmonth + '-' + $scope.user.usr_birthdate);
      $scope.age = moment().diff($scope.user.birthday, 'years');

    });


    $scope.minRangeSlider = {
      options: {
          floor: 100,
          ceil: 10000,
          step: 100
      }
    };

    $scope.heightConversion = function(height) {
      $scope.conversion = '';
      
      if(height % 1 === 0){
        var realFeet = ((height*0.393700) / 12);
        var feet = Math.floor(realFeet);
        var inches = Math.round((realFeet - feet) * 12);

        if(height != null){
          if(feet != 0){
            $scope.conversion = feet + "ft " + inches + 'in';
          }else{
            $scope.conversion = Math.round((realFeet) * 12) + 'in';
          }
        }
      }
    };

   

    $scope.saveProfile = function(user, info, about, relationship, extra) {
      if($scope.profileForm.$invalid) {
        toastr.warning('Please make sure that you have completed all the fields correctly.');
      }
      else {
        $rootScope.actionLoader = true;
        var maxDate, bday;
        maxDate = moment().subtract(18, "years");
        bday = moment(user.usr_birthyear + '/' + user.usr_birthmonth + '/' + user.usr_birthdate);
        if(maxDate.isAfter(bday)) {
          var promises = [];
          var user_params = {
            usr_birthdate: user.usr_birthdate,
            usr_birthmonth: user.usr_birthmonth,
            usr_birthyear: user.usr_birthyear,
            usr_description: user.usr_description,
            usr_screen_name: user.usr_screen_name, 
            usr_gender: user.usr_gender            
          };
          if($scope.components) {
            user_params.ulc_id = user.ulc_id;
            user_params.country_long_name = $scope.components.country;
            user_params.country_short_name = $scope.components.countryCode;
            user_params.state = $scope.components.state;
            user_params.city = $scope.components.city;
            user_params.street = $scope.components.street;
            user_params.street_number = $scope.components.streetNumber;
            user_params.lat = $scope.components.latitude;
            user_params.lng = $scope.components.longitude;
          }
          promises.push(MemberService.editProfile(user_params));

          var inf = {
            inf_id: info.inf_id,
            inf_orientation: info.inf_orientation,
            inf_height: info.inf_height,
            inf_height_unit: info.inf_height_unit,
            inf_body_type: info.inf_body_type,
            inf_ethnicity: info.inf_ethnicity,
            inf_relationship: info.inf_relationship,
            inf_children: info.inf_children,
            inf_smoke: info.inf_smoke,
            inf_drink: info.inf_drink,
            inf_language: info.inf_language,
            inf_education: info.inf_education,
            inf_occupation: info.inf_occupation,
            inf_spending_habits: info.inf_spending_habits,
            inf_preferred_range_from: info.inf_preferred_range_from,
            inf_preferred_range_to: info.inf_preferred_range_to,
            inf_relationship_length: info.inf_relationship_length,
            inf_relationship_loyalty: info.inf_relationship_loyalty,
            inf_privacy_expectations: info.inf_privacy_expectations, 
            inf_relationship_preference: info.inf_relationship_preference, 
            inf_sexual_limit: info.inf_sexual_limit
          };
          if(info.inf_gifts) {
            inf.inf_gifts = 'yes';
          } 
          else {
            inf.inf_gifts = 'no';
          }
          if(info.inf_staycations) {
            inf.inf_staycations = 'yes';
          }
          else {
            inf.inf_staycations = 'no';
          }
          if(info.inf_high_life) {
            inf.inf_high_life = 'yes';
          }
          else {
            inf.inf_high_life = 'no';
          }
          if(info.inf_quality_time) {
            inf.inf_quality_time = 'yes';
          }
          else {
            inf.inf_quality_time = 'no';
          }
          if(info.inf_simple) {
            inf.inf_simple = 'yes';
          }
          else {
            inf.inf_simple = 'no';
          }
          if(info.inf_other_value) {
            inf.inf_other = info.other;
          } 
          else {
            inf.inf_other = '';
          }

          if(user.usr_type=='sugar') {
            inf.inf_net_worth = info.inf_net_worth;
            inf.inf_yearly_income = info.inf_yearly_income;
          }

          promises.push(SettingsService.updateInfo(inf));

          var abt = {
            prf_id: about.prf_id,
            prf_content: about.prf_content
          };

          promises.push(SettingsService.updateQuestion(abt));

          var rel = {
            prf_id: relationship.prf_id,
            prf_content: relationship.prf_content
          };

          promises.push(SettingsService.updateQuestion(rel));

          if(extra) {
            if(extra.prf_id) {
              var ext = {
                prf_id: extra.prf_id,
                que_id: extra.que_id,
                prf_content: extra.prf_content
              };

              promises.push(SettingsService.updateQuestion(ext));
            }
            else {
              var ext = {
                que_id: extra.que_id,
                prf_content: extra.prf_content
              };

              promises.push(SettingsService.createPreference(ext));
            }
          }

          $q.all(promises).then(function(res) {
            $rootScope.actionLoader = false;
            $state.go('profile.view');
          }, function(err) {
            $rootScope.actionLoader = false;
          });
        }
        else {
          toastr.error('You must be at least 18 years of age to continue.');
        }
      }

    };

    $scope.submit = function(data) {
      $log.debug(data);
    }
  }
})();
