(function() {
  'use strict';

  angular
    .module('sugar')
    .directive('postButtons', postButtons);

  /** @ngInject */
  function postButtons() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/profile/post-buttons.html',
      scope: {
          post: '=',
          posts: '='
      },
      controller: postBtnCtrl,
      controllerAs: 'postBtn'
    };

    return directive;

    /** @ngInject */
    function postBtnCtrl(moment, $mdDialog, $log, $document, $scope, $state, $window, $mdMenu, MemberService, FeedService, userData, toastr, $rootScope, $mdSidenav) {
      $scope.follow = function(post) {
        if(post.user.followed) {
          var params = {
            fol_id: post.user.followed,
            fol_user: post.user.usr_id
          };
          if(post.user.followed) {
            $scope.post.user.followed = false;
          }
          else {
            $scope.post.user.followed = true;
          }
        }
        else {
           var params = {
            fol_user: post.user.usr_id,
            fol_status: 'active'
          };
          $scope.post.user.followed = true;
        }

        MemberService.follow(params).then(function(res) {
          if(res.data[0].fol_status == 'active') {
            $scope.post.user.followed = res.data[0].fol_id;
            var duplicates = _.where($scope.posts, {usr_token_id: $scope.post.usr_token_id});
            _.each(duplicates, function(element, index, list) {
                element.user.followed = res.data[0].fol_id;
            });
          }
          else {
            $scope.post.user.followed = false;
            var duplicates = _.where($scope.posts, {usr_token_id: $scope.post.usr_token_id});
            _.each(duplicates, function(element, index, list) {
                element.user.followed = false;
            });
          }
        });
      };

      $scope.goTo = function(token) {
        $state.go('messageView', {usr_token_id: token});
      }

      $scope.favorite = function(post) {
        if(post.user.faved) {
          var params = {
            usl_id: post.user.faved,
            usl_user: post.user.usr_id
          };
          if(post.user.faved) {
            $scope.post.user.faved = false;
          }
          else {
            $scope.post.user.faved = true;
          }
        }
        else {
          var params = {
            usl_user: post.user.usr_id
          };
          $scope.post.user.faved = true;
        }

        MemberService.favorite(params).then(function(res) {
          $log.debug(res);
          if(res.data[0].usl_status=='active') {
            $scope.post.user.faved = res.data[0].usl_id;
            var duplicates = _.where($scope.posts, {usr_token_id: $scope.post.usr_token_id});
            _.each(duplicates, function(element, index, list) {
                element.user.faved = res.data[0].usl_id;
            });
          }
          else {
            $scope.post.user.faved = false;
            var duplicates = _.where($scope.posts, {usr_token_id: $scope.post.usr_token_id});
            _.each(duplicates, function(element, index, list) {
                element.user.faved = false;
            });
          }
        });
      };

      $scope.flag = function(post) {
        if(post.flag.flg_id) {
          var params = {
            flg_id: post.flag.flg_id,
            flg_status: 'active',
            pos_id: post.pos_id
          }
          if(post.flag.flg_status=='active') {
            $scope.post.flag.flg_id = post.flag.flg_id;
            $scope.post.flag.flg_status = 'inactive';
          }
          else {
            $scope.post.flag.flg_id = post.flag.flg_id;
            $scope.post.flag.flg_status = 'active';
          }
        }
        else {
          var params = {
            pos_id: post.pos_id
          }
          $scope.post.flag.flg_status = 'active';
        }
        FeedService.flag(params).then(function(res) {
          if(res.data[0].flg_status=='active') {
            $scope.post.flag.flg_id = res.data[0].flg_id;
            $scope.post.flag.flg_status = 'active';
          }
          else {
            $scope.post.flag.flg_id = false;
          }
        });
      };

      $scope.like = function(post) {
        if(post.like.pol_id) {
          var params = {
            pol_id: post.like.pol_id,
            pos_id: post.pos_id,
            pol_status: 'active'
          };
          if(post.like.pol_status == 'active') {
            $scope.post.like.pol_status = 'inactive';
          }
          else {
            $scope.post.like.pol_status = 'active';
          }
        }
        else {
          var params = {
            pos_id: post.pos_id
          }
          $scope.post.like = {
            pol_status: 'active'
          }
        }

        FeedService.likePost(params).then(function(res) {
          console.log(res);
          if(res.data[0].pol_status=='active') {
            $scope.post.like.pol_id = res.data[0].pol_id;
            $scope.post.like.pol_status = 'active';
          }
          else {
            $scope.post.like.pol_id = res.data[0].pol_id;
            $scope.post.like.pol_status = 'inactive';
          }
        });
      };
      
    }
  }

})();
