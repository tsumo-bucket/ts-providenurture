(function() {
  'use strict';

  angular
    .module('sugar')
    .directive('violator', violator);

  /** @ngInject */
  function violator() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/main/violators.html',
      scope: {
        user: '='
      },
      controller: violatorCtrl,
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function violatorCtrl($log, $scope, $state, $window, MemberService, userData, toastr, $rootScope, URL) {
      MemberService.getUser().then(function(res) {
        if(res.success) {
          $scope.user = res.data[0];
          userData.setUserData('usr_signup_step', res.data[0].usr_signup_step);
        }
      });

      $scope.hideV = false;
      if(userData.getViolator()) {
        $scope.hideV = true;
      }
      else {
        $scope.hideV = false;
      }
      $scope.baseUrl = function(res) {
        return URL.BASE + res;
      };

      $scope.hideViolator = function() {
        userData.hideViolator({'hideV': 'true'});
        $scope.hideV = true;
      };

      $scope.resend = function() {
        $rootScope.actionLoader = true;
        MemberService.resendConfirmation({usr_email: $scope.user.usr_email}).then(function(res) {
          $rootScope.actionLoader = false;
          if(res.success) {
            toastr.success('Please check your email.');
          }
        }, function(err) {
          $rootScope.actionLoader = false;
          toastr.success('Oops! Something went wrong. Try again later.');
        });
      }
    }
  }

})();
