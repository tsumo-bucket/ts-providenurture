
/*================================================================
=>                  Directive = Image Load
==================================================================*/
(function() {
  'use strict';

  angular
    .module('sugar')
    .directive('imageonload', imageonload);

  /** @ngInject */
  function imageonload() {
    var directive = {
      restrict: 'A',
      link: function(scope, element, attrs) {
        var bgImg = new Image();

        scope.$watch('ngStyle', function() {
          element.addClass('banner-image-loading');

          var src = element[0].style.backgroundImage;
          src = src.replace('url("', "").replace('")', "");
          src = src.replace("url('", "").replace("')", "");
          src = src.replace("url(", "").replace(")", "");
          // src = src.substring(4, src.length - 1);
          bgImg.src = src;
        });

        bgImg.onload = function(){
          element.removeClass('banner-image-loading');
        };

        // bgImg.src = src;
      }
    };

    return directive;
  }

})();