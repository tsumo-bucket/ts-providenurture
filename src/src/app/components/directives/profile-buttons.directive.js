(function() {
  'use strict';

  angular
    .module('sugar')
    .directive('profileButtons', profileButtons);

  /** @ngInject */
  function profileButtons() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/profile/profile-buttons.html',
      scope: {
        user: '='
      },
      controller: profileBtnCtrl
    };

    return directive;

    /** @ngInject */
    function profileBtnCtrl($log, $document, $scope, $state, $window, $mdDialog, MemberService, userData, toastr, $rootScope) {
      $scope.follow = function(user) {
        if(user.followed.fol_id) {
          var params = {
            fol_id: user.followed.fol_id,
            fol_user: user.usr_id,
            fol_status: 'active'
          };
        }
        else {
           var params = {
            fol_user: user.usr_id,
            fol_status: 'active'
          };
          $scope.user.followed.fol_status = 'active';
        }

        MemberService.follow(params).then(function(res) {
          if(res.data[0].fol_status == 'active') {
            $scope.user.followed.fol_id = res.data[0].fol_id;
            $scope.user.followed.fol_status = 'active';
          }
          else {
            $scope.user.followed.fol_id = res.data[0].fol_id;
            $scope.user.followed.fol_status = 'inactive';
          }
        });
      };

      $scope.favorite = function(user) {
        if(user.faved.usl_id) {
          var params = {
            usl_id: user.faved.usl_id,
            usl_user: user.usr_id
          };

          if(user.faved.usl_status == 'active') {
            $scope.user.faved.usl_id = user.faved.usl_id;
            $scope.user.faved.usl_status = 'inactive';
          }
          else {
            $scope.user.faved.usl_status = 'active';
          }
        }
        else {
          var params = {
            usl_user: user.usr_id
          };
          $scope.user.faved.usl_status = 'active';
        }

        MemberService.favorite(params).then(function(res) {
          if(res.data[0].usl_status=='active') {
            $scope.user.faved.usl_id = res.data[0].usl_id;
            $scope.user.faved.usl_status = 'active';
          }
          else {
            $scope.user.faved.usl_id = res.data[0].usl_id;
            $scope.user.faved.usl_status = 'inactive';
          }
        });
      };

      $scope.block = function(user) {
        
        var params = {
          blo_user: user.usr_id,
          blo_status: 'active'
        };

        MemberService.block(params).then(function(res) {
          if(res.success) {
            $state.go('settings.privacy');
          }
        });
      };

      $scope.goTo = function(token) {
        $state.go('messageView', {usr_token_id: token});
      };

      $scope.openReport = function(ev) {
        $mdDialog.show({
          controller: profileBtnCtrl,
          templateUrl: 'app/profile/report-modal.html',
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: false // Only for -xs, -sm breakpoints.
        })
        .then(function(answer) {
          $log.debug(answer);
        }, function(err) {
        });
      };
    }
  }

})();
