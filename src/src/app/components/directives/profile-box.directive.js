(function() {
  'use strict';

  angular
    .module('sugar')
    .directive('profileBox', profileBox);

  /** @ngInject */
  function profileBox() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/main/profile-box.html',
      scope: {
        user: '='
      },
      controller: profileBoxCtrl,
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function profileBoxCtrl($log, $scope, $state, $window, MemberService, userData, toastr, $rootScope, URL) {
      $scope.user = angular.fromJson(userData.isLogged());
      $scope.baseUrl = function(res) {
        return URL.BASE + res;
      };
    }
  }

})();
