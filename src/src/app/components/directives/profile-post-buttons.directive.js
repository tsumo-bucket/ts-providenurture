(function() {
  'use strict';

  angular
    .module('sugar')
    .directive('profilePostButtons', profilePostButtons);

  /** @ngInject */
  function profilePostButtons() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/profile/profile-posts-button.html',
      scope: {
          post: '='
      },
      controller: postBtnCtrl,
      controllerAs: 'postBtn'
    };

    return directive;

    /** @ngInject */
    function postBtnCtrl(moment, $mdDialog, $log, $document, $scope, $state, $window, $mdMenu, MemberService, FeedService, userData, toastr, $rootScope, $mdSidenav) {

      $scope.flag = function(post) {
        if(post.flag.flg_id) {
          var params = {
            flg_id: post.flag.flg_id,
            flg_status: 'active',
            pos_id: post.pos_id
          }
          if(post.flag.flg_status=='active') {
            $scope.post.flag.flg_status = 'inactive';
          }
          else {
            $scope.post.flag.flg_status = 'active';
          }
        }
        else {
          var params = {
            pos_id: post.pos_id
          }
          $scope.post.flag.flg_status = 'active';
        }
        FeedService.flag(params).then(function(res) {
          if(res.data[0].flg_status=='active') {
            $scope.post.flag.flg_id = res.data[0].flg_id;
            $scope.post.flag.flg_status = 'active';
          }
          else {
            $scope.post.flag.flg_status = 'inactive';
          }
        });
      };

      $scope.like = function(post) {
        if(post.like.pol_id) {
          var params = {
            pol_id: post.like.pol_id,
            pos_id: post.pos_id,
            pol_status: 'active'
          };
          if(post.like.pol_status == 'active') {
            $scope.post.like.pol_status = 'inactive';
          }
          else {
            $scope.post.like.pol_status = 'active';
          }
        }
        else {
          var params = {
            pos_id: post.pos_id
          }
          $scope.post.like = {
            pol_status: 'active'
          }
        }

        FeedService.likePost(params).then(function(res) {
          console.log(res);
          if(res.data[0].pol_status=='active') {
            $scope.post.like.pol_id = res.data[0].pol_id;
            $scope.post.like.pol_status = 'active';
          }
          else {
            $scope.post.like.pol_id = res.data[0].pol_id;
            $scope.post.like.pol_status = 'inactive';
          }
        });
      };
      
    }
  }

})();
