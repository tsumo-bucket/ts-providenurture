(function() {
  'use strict';

  angular
    .module('sugar')
    .directive('reportButton', reportButton);

  /** @ngInject */
  function reportButton() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/profile/report-button.html',
      scope: {
          user: '='
      },
      controller: reportBtnCtrl,
      controllerAs: 'reportBtn'
    };

    return directive;

    /** @ngInject */
    function reportBtnCtrl(moment, $mdDialog, $log, $document, $scope, $state, $window, $mdMenu, MemberService, userData, toastr, $rootScope, $mdSidenav) {
      $scope.openReport = function(ev) {
        $mdDialog.show({
          controller: ReportController,
          templateUrl: 'app/profile/report-modal.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          locals: {
            user: $scope.user
          },
          fullscreen: false // Only for -xs, -sm breakpoints.
        })
        .then(function(answer) {
          $log.debug(answer);
        }, function() {
        });
      };

      function ReportController($scope, $mdDialog, user) {
        $scope.user = user;
        $scope.closeDialog = function() {
          $mdDialog.hide();
        }
        $scope.upload = function (file) {
          $scope.filename = file.name;
          var reader = new window.FileReader();
          reader.readAsDataURL(file); 
          reader.onloadend = function() {
            $scope.evidence = reader.result;    
          }
        };

        $scope.removeFile = function() {
          $scope.file = '';
        };
        $scope.report = function(report) {
          
          if($scope.reportForm.$invalid) {
            toastr.warning('Please choose a reason for reporting and let us know what\'s going on.');
          }
          else {
            var params = {
              rep_reason: report.reason,
              rep_other_reason: report.other_reason,
              rep_user: user.usr_id
            };
            if($scope.evidence) {
              params.rep_evidence = $scope.evidence;
            }
            else {
              params.rep_evidence = '';
            }
            MemberService.report(params).then(function(res) {
              $scope.closeDialog();
              if(res.success) {
                toastr.success('Reported');
              }
              else {
                toastr.warning(res.error.message);
              }
            }, function(err) {

            });
          }
        }
      };

    }
  }

})();
