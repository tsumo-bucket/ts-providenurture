(function() {
  'use strict';

  angular
    .module('sugar')
    .directive('userButtons', userButtons);

  /** @ngInject */
  function userButtons() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/main/user-buttons.html',
      scope: {
        user: '=',
        source: '='
      },
      controller: userBtnCtrl
    };

    return directive;

    /** @ngInject */
    function userBtnCtrl($log, $document, $scope, $state, $window, MemberService, userData, toastr, $rootScope) {
      $scope.favorite = function(user) {
        
        if($scope.source == 0){
          var params = {
            usl_id: user.faved,
            usl_user: user.usl_user
          };

          var ind = _.findIndex($rootScope.myfaves, {usl_id: user.usl_id}) ;

          MemberService.favorite(params).then(function(res) {
            if(res.success) {
              $rootScope.myfaves.splice(ind, 1);
            }       
          });
        }
        else {
          if(user.faved) {
            var params = {
              usl_id: user.faved,
              usl_user: user.usr_id
            };
            $scope.user.faved = false;
          }
          else {
            var params = {
              usl_user: user.usr_id
            };
            $scope.user.faved = true;
          }

          MemberService.favorite(params).then(function(res) {
            $log.debug(res);
            if(res.data[0].usl_status=='active') {
              $scope.user.faved = res.data[0].usl_id;
              $rootScope.myfaves.push(user);
            }
            else {
              $scope.user.faved = false;
            }
          });
        }
      };

      $scope.goTo = function(token) {
        $state.go('messageView', {usr_token_id: token});
      }
    }
  }

})();
