(function() {
  'use strict';

  angular
    .module('sugar')
    .directive('supportBox', supportBox);

  /** @ngInject */
  function supportBox() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/main/support-box.html',
      scope: {
        user: '='
      },
      controller: supportBoxCtrl,
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function supportBoxCtrl($log, $scope, $state, $window, MemberService, userData, toastr, $rootScope, URL) {
      $scope.user = angular.fromJson(userData.isLogged());
      $scope.baseUrl = function(res) {
        return URL.BASE + res;
      };
    }
  }

})();
