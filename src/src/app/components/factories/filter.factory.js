(function() {
  'use strict';

  angular
    .module('sugar')
    .factory('FilterFactory', FilterFactory);

  /** @ngInject */
  function FilterFactory() {
    var Filter = {};

  	Filter.age = [];

  	var y = 0;

  	for(var x = 18; x <= 80; x++)
  	{
    	Filter.age[y] = {
    		value: x,
    		name: x
			}
    	y++;
  	}

  	Filter.years = [];
  	var currentDate = new Date();
    var year = currentDate.getFullYear() - 18;
  	var maxYear = currentDate.getFullYear() - 99;
  	var z = 0;

  	for (var y = year; y >= maxYear; y--)
  	{
    	Filter.years[z] = {
    		name: y,
    		value: y
    	};
    	z++;
  	}

    Filter.usr_interested_age_from;
    Filter.usr_interested_age_to;

  	Filter.months = [
    	{value: 1, name: "January"},
    	{value: 2, name: "February"},
    	{value: 3, name: "March"},
    	{value: 4, name: "April"},
    	{value: 5, name: "May"},
    	{value: 6, name: "June"},
    	{value: 7, name: "July"},
    	{value: 8, name: "August"},
    	{value: 9, name: "September"},
    	{value: 10, name: "October"},
    	{value: 11, name: "November"},
    	{value: 12, name: "December"}
    ];

  	Filter.days = [];

  	var x = 0;
  	for (var d = 1; d <= 31; d++)
  	{
    	Filter.days[x] = {
      		name: d,
      		value: d
    	};
    	x++;
  	}

  	Filter.preferences = [
      {checked: false, value: 'undecided', name: 'Undecided'},
    	{checked: false, value: 'rather not say', name: 'Rather not say.'},
    	{checked: false, value: 'open to discussion', name: 'Open to discussion.'},
    	{checked: false, value: 'strictly sexual', name: 'Stricty Sexual.'},
    	{checked: false, value: 'serious relationship', name: 'Serious relationship.'},
    	{checked: false, value: 'marriage', name: 'Marriage.'},
    	{checked: false, value: 'just friends', name: 'Just Friends. No physical.'}
  	];

  	Filter.privacy = [
      {checked: false, value: 'undecided', name: 'Undecided'},
  		{checked: false, value: 'open to any arrangement', name: 'I\'m open to any arrangement.'},
    	{checked: false, value: 'very discreet', name: 'Very discreet. Only private venues.'},
    	{checked: false, value: 'ok being in public', name: 'I\'m ok being in public.'},
    	{checked: false, value: 'depends on attraction level', name: 'It totally depends on attraction level.'}
  	];

    Filter.relationshipPrivacy = [
      {checked: false, value: 'discreet', name: 'Discreet'},
      {checked: false, value: 'public', name: 'Public'}
    ];

    Filter.loyalty = [
      {checked: false, value: 'undecided', name: 'Undecided'},
      {checked: false, value: 'no strings attached', name: 'No Strings Attached'},
      {checked: false, value: 'exclusive', name: 'Exclusive'}
    ];

    Filter.length = [
      {checked: false, value: 'undecided', name: 'Undecided'},
      {checked: false, value: 'short term', name: 'Short Term'},
      {checked: false, value: 'long term', name: 'Long Term'}
    ];

  	Filter.relationship = [
    	{checked: false, value: 'single', name: 'Single'}, 
    	{checked: false, value: 'open relationship', name: 'Open Relationship'},
      {checked: false, value: 'married', name: 'Married '}, 
      {checked: false, value: 'divorced', name: 'Divorced'},
      {checked: false, value: 'separated', name: 'Separated'}, 
      {checked: false, value: 'married but looking', name: 'Married but looking'}, 
      {checked: false, value: 'rather not say', name: 'Rather not say'}
  	];

  	Filter.children = [
    	{checked: false, value: 'prefer not to say', name: 'Prefer Not to Say'}, 
    	{checked: false, value: 'yes', name: 'Yes'}, 
    	{checked: false, value: 'no', name: 'No'}
  	];

  	Filter.language = [
    	{checked: false, value: 'english', name: 'English'}, 
    	{checked: false, value: 'espanol', name: 'Espanol'}, 
    	{checked: false, value: 'francais', name: 'Francais'},
    	{checked: false, value: 'deutsch', name: 'Deutsch'},
    	{checked: false, value: 'chinese', name: 'Chinese'},
    	{checked: false, value: 'italian', name: 'Italian'},
    	{checked: false, value: 'nederlanese', name: 'Nederlanese'},
    	{checked: false, value: 'portuguese', name: 'Portuguese'},
    	{checked: false, value: 'russian', name: 'Russian'}
  	];

  	Filter.bodyType = [
    	{checked: false, value: 'slim', name: 'Slim'}, 
    	{checked: false, value: 'athletic', name: 'Athletic'}, 
    	{checked: false, value: 'average', name: 'Average'}, 
    	{checked: false, value: 'curvy', name: 'Curvy'}, 
    	{checked: false, value: 'a few extra pounds', name: 'A few extra pounds'}, 
    	{checked: false, value: 'overweight', name: 'Full / Overweight'},
    	{checked: false, value: 'other', name: 'Other'}
  	];

  	Filter.ethnicity = [
    	{checked: false, value: 'asian', name: 'Asian'}, 
    	{checked: false, value: 'black / african descent', name: 'Black / African Descent'}, 
    	{checked: false, value: 'latin / hispanic', name: 'Latin / Hispanic'}, 
    	{checked: false, value: 'east indian', name: 'East Indian'}, 
    	{checked: false, value: 'middle eastern', name: 'Middle Eastern'}, 
      {checked: false, value: 'mixed', name: 'Mixed'}, 
    	{checked: false, value: 'native american', name: 'Native American'}, 
    	{checked: false, value: 'pacific islander', name: 'Pacific Islander'}, 
    	{checked: false, value: 'white / caucasian', name: 'White / Caucasian'}, 
    	{checked: false, value: 'other', name: 'Other'}
  	];

  	Filter.education = [
	    {checked: false, value: 'high school', name: 'High School'}, 
	    {checked: false, value: 'some college', name: 'Some College'}, 
	    {checked: false, value: 'associates degree', name: 'Associate\'s Degree'}, 
	    {checked: false, value: 'bachelors degree', name: 'Bachelor\'s Degree'}, 
	    {checked: false, value: 'graduate degree', name: 'Graduate Degree'}, 
	    {checked: false, value: 'post doctorate', name: 'PhD / Post Doctorate'}
  	];

    Filter.expectation = [
      {checked: false, value: 'Negotiable'},
      {checked: false, value: 'Minimal'},
      {checked: false, value: 'Base Support'},
      {checked: false, value: 'Moderate Support'},
      {checked: false, value: 'Mid Support'},
      {checked: false, value: 'Good Living'},
      {checked: false, value: 'High Roller'},
      {checked: false, value: 'Over the Top'}
    ];

  	Filter.sponsorHabit = [
	    {checked: false, value: '0', name: 'I\'m open to discussion'}, 
      {checked: false, value: '1', name: 'Up to $150 per month'}, 
      {checked: false, value: '2', name: 'Up to $250 per month'}, 
      {checked: false, value: '3', name: 'Up to $500 per month'}, 
      {checked: false, value: '4', name: 'Up to $1000 per month'}, 
      {checked: false, value: '5', name: 'Up to $2000 per month'}, 
      {checked: false, value: '6', name: 'Up to $4000 per month'}, 
      {checked: false, value: '7', name: 'Up to $8000 per month'}, 
      {checked: false, value: '8', name: 'Above $8000 per month'}
  	];

    Filter.babyHabit = [
      {checked: false, value: '0', name: 'Negotiable: No budget Specified '}, 
      {checked: false, value: '1', name: 'Up to $150 per month'}, 
      {checked: false, value: '2', name: 'Minimal: Up to $250 per month'}, 
      {checked: false, value: '3', name: 'Base Support: Up to $500 per month'}, 
      {checked: false, value: '4', name: 'Moderate Support: Up to $1000 per month'}, 
      {checked: false, value: '5', name: 'Mid Support: Up to $2000 per month'}, 
      {checked: false, value: '6', name: 'Easy Living: Up to $4000 per month'}, 
      {checked: false, value: '7', name: 'Good Living: Up to $8000 per month'}, 
      {checked: false, value: '8', name: 'High Roller: Above $8000 per month'}
    ];

    Filter.habits = [
      {checked: false, value: '0', name: 'Negotiable'}, 
      {checked: false, value: '1', name: 'Up to $150/mo'}, 
      {checked: false, value: '2', name: 'Minimal'}, 
      {checked: false, value: '3', name: 'Base Support'}, 
      {checked: false, value: '4', name: 'Moderate Support'}, 
      {checked: false, value: '5', name: 'Mid Support'}, 
      {checked: false, value: '6', name: 'Easy Living'}, 
      {checked: false, value: '7', name: 'Good Living'}, 
      {checked: false, value: '8', name: 'High Roller'}
    ];

  	Filter.networth = [
      {checked: false, value: '0', name: 'Enough to Make my Flame Partner Feel Secure'}, 
      {checked: false, value: '1', name: '$50,000'}, 
      {checked: false, value: '2', name: '$75,000'}, 
      {checked: false, value: '3', name: '$100,000'}, 
      {checked: false, value: '4', name: '$200,000'}, 
      {checked: false, value: '5', name: '$500,000'}, 
      {checked: false, value: '6', name: '$750,000'}, 
      {checked: false, value: '7', name: '$1 million'}, 
      {checked: false, value: '8', name: '$2 million'}, 
      {checked: false, value: '9', name: '$5 million'}, 
      {checked: false, value: '10', name: '$10 million'}, 
      {checked: false, value: '11', name: '$50 million'}, 
      {checked: false, value: '12', name: '$100 million'}, 
	    {checked: false, value: '13', name: 'Above $100 million'} 
  	];

  	Filter.yearlyIncome = [
	    {checked: false, value: '0', name: 'Enough to Support my Flame Partner'}, 
      {checked: false, value: '1', name: '$30,000'}, 
      {checked: false, value: '2', name: '$50,000'}, 
      {checked: false, value: '3', name: '$75,000'}, 
      {checked: false, value: '4', name: '$100,000'}, 
      {checked: false, value: '5', name: '$150,000'}, 
      {checked: false, value: '6', name: '$200,000'}, 
      {checked: false, value: '7', name: '$250,000'}, 
      {checked: false, value: '8', name: '$300,000'}, 
      {checked: false, value: '9', name: '$400,000'}, 
      {checked: false, value: '10', name: '$500,000'}, 
      {checked: false, value: '11', name: '$1 million'}, 
      {checked: false, value: '12', name: 'Above $1 million'} 
  	];

 		Filter.smoke = [
	    {checked: false, value: 'non smoker', name: 'Non-Smoker'}, 
	    {checked: false, value: 'light smoker', name: 'Light-Smoker'}, 
	    {checked: false, value: 'heavy smoker', name: 'Heavy-Smoker'}
  	];

  	Filter.drink = [
	    {checked: false, value: 'non drinker', name: 'Non-Drinker'}, 
	    {checked: false, value: 'social drinker', name: 'Social-Drinker'}, 
	    {checked: false, value: 'heavy drinker', name: 'Heavy-Drinker'}
  	];

  	Filter.gender = [
	    {checked: false, name: 'Man', value: 'male'},
	    {checked: false, name: 'Woman', value: 'female'}
  	];

  	Filter.interest = [
	    {checked: false, name: 'Men', value: 'male'},
	    {checked: false, name: 'Women', value: 'female'},
	    {checked: false, name: 'Both', value: 'both'}
  	];

  	Filter.orientation = [
	    {checked: false, name: 'Gay / Lesbian', value: 'gay / lesbian'},
	    {checked: false, name: 'Straight', value: 'straight'},
	    {checked: false, name: 'Bi-sexual', value: 'bisexual'},
      {checked: false, name: 'Straight but willing to explore', value: 'willing to explore'},
      {checked: false, name: 'I would rather not say', value: 'rather not say'},
	    {checked: false, name: 'I\'m still deciding', value: 'still deciding'}
  	];

  	Filter.explore = [
      {checked: false, value: 'undecided', name: 'Undecided'},
	    {checked: false, value:'thinking about it', name: 'Thinking about it.'}, 
	    {checked: false, value:'open to new things', name: 'Open to new things.'}, 
	    {checked: false, value:'anything goes', name: 'Anything goes.'}, 
	    {checked: false, value:'rather not say', name: 'Rather not say.'}, 
	    {checked: false, value:'lets keep that to imagination', name: 'Let\'s keep that to the imagination.'}, 
	    {checked: false, value:'no experimentation', name: 'No experimentation. I\'m shy.'}
  	];

    Filter.source = [
      {checked: false, value: 1, name: 'Facebook'},
      {checked: false, value: 2, name: 'Google'},
      {checked: false, value: 3, name: 'Youtube'},
      {checked: false, value: 4, name: 'Instagram'},
      {checked: false, value: 5, name: 'Word of Mouth'},
      {checked: false, value: 6, name: 'Others'}
    ];

    Filter.country = [
      {checked: false, value: 1, name: 'Australia'},
      {checked: false, value: 2, name: 'China'},
      {checked: false, value: 3, name: 'India'},
      {checked: false, value: 4, name: 'Philippines'},
      {checked: false, value: 5, name: 'United Kingdom'},
      {checked: false, value: 6, name: 'United States of America'}
    ];

    Filter.questions = [
      {que_id: 5, que_question: 'Do you believe in supernatural?'},
      {que_id: 6, que_question: 'What would you say is the ideal vacation?'},
      {que_id: 7, que_question: 'What would be harder for you, to tell someone you love them or that you do not love them back?'},
      {que_id: 8, que_question: 'What scares you the most and why?'},
      {que_id: 9, que_question: 'What do you do in your free time?'},
      {que_id: 10, que_question: 'Which is your favorite part of the human body and why?'}
    ];

		return Filter;
  }
})();