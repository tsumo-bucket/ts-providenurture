(function() {
  'use strict';

  angular
    .module('sugar')
    .factory('mySocket', mySocket);

  /** @ngInject */
  function mySocket(URL, socketFactory) {
    return socketFactory({
      prefix: '',
      ioSocket: io.connect(URL.SOCKET)
    });
  }
})();