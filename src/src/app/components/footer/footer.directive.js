(function() {
  'use strict';

  angular
    .module('sugar')
    .directive('footerTmp', footerTmp);

  /** @ngInject */
  function footerTmp(modulePrefix) {
    var getModuleView = function(url) {
        if (modulePrefix) {
          var http = new XMLHttpRequest();
          var modUrl = url.replace('.html', '') + modulePrefix + '.html';
          http.open('HEAD', modUrl, false);
          http.send();
          return (http.status !== 404) ? modUrl : url;
        }
        return url;
    };

    var directive = {
      restrict: 'E',
      templateUrl: getModuleView('app/components/footer/footer.html'),
      scope: {
          creationDate: '='
      },
      controller: FooterController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function FooterController(moment, $scope, $translate) {
      var vm = this;

      // "vm.creationDate" is available by directive option "bindToController: true"
      vm.relativeDate = moment(vm.creationDate).fromNow();
      $scope.year = moment().format('YYYY');

      $scope.translate = function(language) {
        $translate.use(language);
      }
    }
  }

})();
