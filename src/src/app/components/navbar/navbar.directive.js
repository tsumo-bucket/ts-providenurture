(function() {
  'use strict';

  angular
    .module('sugar')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar(modulePrefix) {

    var getModuleView = function(url) {
        if (modulePrefix) {
          var http = new XMLHttpRequest();
          var modUrl = url.replace('.html', '') + modulePrefix + '.html';
          http.open('HEAD', modUrl, false);
          http.send();
          return (http.status !== 404) ? modUrl : url;
        }
        return url;
    };

    var directive = {
      restrict: 'E',
      templateUrl: getModuleView('app/components/navbar/navbar.html'),
      scope: {
          creationDate: '='
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController(moment, $mdDialog, $log, $document, $timeout, $scope, $state, MessageService, $window, $mdMenu, MemberService, userData, toastr, $rootScope, $mdSidenav, URL) {
      var vm = this;
      $scope.openRightMenu = function() {
        $mdSidenav('right').toggle();
      };

      $scope.close = function () {
        // Component lookup should always be available since we are not using `ng-if`
        $mdSidenav('right').close()
          .then(function () {
          });
      };

      $rootScope.$on('home', function(events, args) {
        $('.navbar').addClass('transluscent');
        $scope.isHome = args;
      });
      function getMsgCount() {
        MessageService.getCount().then(function(res) {
          if(res.success) {
            $scope.count = res.data[0].unread_count;
          }
        });
      }
      $rootScope.$on('get counter', function(events,args) {
        getMsgCount();
      });

      $rootScope.$on('change profile picture', function(events,args) {
        if(args.img_image) {
          $scope.user.img_image = args.img_image;
        }
        else {
          $scope.user.img_image = '';
        }
      });
      
      if($state.current.name == 'home') {
        $scope.isHome = true;
      }
      else {
        $scope.isHome = false;
      }

      $scope.isActive = function(url) {
        if(url == $state.current.name) {
          return true;
        }
      }
     
      if(userData.isLogged()) {
        getMsgCount();
        $scope.user = angular.fromJson(userData.isLogged());
        $rootScope.$broadcast('login', $scope.user);
        if($scope.user.usr_signup_step == 'done' && $scope.user.usr_status != 'approved') {
          $scope.hide = true;
        }
        else if($scope.user.usr_signup_step == 'initial') {
          $scope.hide = true;
        }
        else if($scope.user.usr_signup_step == 'photo') {
          $scope.hide = true;
        }
        else if($scope.user.usr_signup_step == 'info') {
          $scope.hide = true;
        }
        else if($scope.user.usr_signup_step == 'preference') {
          $scope.hide = true;
        }
      }

      $scope.status = {
        isopen: false
      };

      $scope.appendToEl = angular.element(document.querySelector('#pft'));

      $scope.toggleDropdown = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.status.isopen = !$scope.status.isopen;
      };

      $scope.openLogin = function(ev) {
        $mdSidenav('right').close();
        $mdDialog.show({
          controller: NavbarController,
          templateUrl: 'app/main/login.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: false // Only for -xs, -sm breakpoints.
        })
        .then(function(answer) {
          $log.debug(answer);
        }, function() {
          $scope.status = 'You cancelled the dialog.';
        });
      };

      $scope.openForgot = function(ev) {
        $mdDialog.show({
          controller: NavbarController,
          templateUrl: 'app/main/forgot-password.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: false // Only for -xs, -sm breakpoints.
        })
        .then(function(answer) {
          $log.debug(answer);
        }, function() {
          $scope.status = 'You cancelled the dialog.';
        });
      };


      $scope.cancelDialog = function() {
        $mdDialog.cancel();
      };
      
      $scope.goTo = function(state) {
        $log.debug(state);
        $scope.close();
        $state.go(state);
      };  
      
      $scope.error = {
        message: ''
      };

      $scope.$on('login', function(events, args){
        $scope.user = args; //now we've registered!
        getMsgCount();
      })
      
      var checkEmail = function(params, response, access_token, photo_count) {
        MemberService.emailCheck(params).then(function(res) {
          if(res.success) {
            if(res.data[0].usr_signup_source == 'facebook') {
              $rootScope.$broadcast('login', res.data[0]);
              $rootScope.actionLoader = false;
              if(res.data[0].usr_signup_step == 'done' && res.data[0].usr_status == 'approved') {
                $state.go('members');
                $scope.hide = false;
              }
              if(res.data[0].usr_signup_step == 'done' && res.data[0].usr_status == 'pending') {
                $state.go('search');
                $scope.hide = true;
              }
              else if(res.data[0].usr_signup_step == 'initial') {
                $state.go('account.addPhotos');
                $scope.hide = true;
              }
              else if(res.data[0].usr_signup_step == 'photo') {
                $state.go('account.addPhotos');
                $scope.hide = true;
              }
              else if(res.data[0].usr_signup_step == 'info') {
                $state.go('account.personal');
                $scope.hide = true;
              }
              else if(res.data[0].usr_signup_step == 'preference') {
                $state.go('account.preferences');
                $scope.hide = true;
              }
            }
            else {
              toastr.warning('Email address is not valid or unavailable.');
              $rootScope.actionLoader = false;
            }
          }
          else {
            $rootScope.user = {
              usr_email: response.email,
              first_name: response.first_name,
              last_name: response.last_name,
              fb_image: response.picture.data.url,
              fb_birthday: response.birthday,
              friend_count: response.friends.summary.total_count,
              photo_count: photo_count,
              fb_id: response.id,
              access_token: access_token,
              usr_signup_source: 'facebook'
            }
            userData.setTempUser($rootScope.user);
            $state.go('join2');
            $rootScope.actionLoader = false;
          }
        });
      };

      $scope.facebookLogin = function() {
        $rootScope.actionLoader = true;
        $scope.cancelDialog();
        FB.getLoginStatus(function(response) {
          // console.log(response);
          if(response.status == 'connected') {
            var access_token = response.authResponse.accessToken;
            FB.api('/me',
              {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday, picture, permissions"},
              function(response) {

              if(!response.birthday || !response.friends) {
                FB.login(function(response) {
                  if (response.authResponse) {
                    var access_token = response.authResponse.accessToken;
                
                    FB.api('/me', 
                      {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday"},
                      function(response) {
                        // console.log(response);
                        var params = {
                          email: response.email
                        };
                        FB.api(
                          '/me/photos',
                          'GET',
                          {"fields":"images,picture", "type":"uploaded"},
                          function(response) {
                            if(response.data) {
                              $scope.photo_count = response.data.length;
                            }
                            else {
                              $scope.photo_count = '';
                            }
                            
                            if($scope.photo_count) {
                              checkEmail(params, response, access_token, $scope.photo_count);
                            }
                            else {
                              checkEmail(params, response, access_token, $scope.photo_count);
                            }
                          }
                        );

                    });
                  } else {
                    $rootScope.actionLoader = false;
                    toastr.warning('User cancelled login or did not fully authorize.');
                  }
                }, {scope: 'email,user_birthday,user_friends,user_photos'});
              }
              else {
                var params = {
                  email: response.email
                };
                FB.api('/me', 
                  {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday, picture, permissions"},
                  function(response) {
                    $scope.initial_response = response;
                    var params = {
                      email: response.email
                    };
                    FB.api(
                      '/me/photos',
                      'GET',
                      {"fields":"images,picture", "type":"uploaded"},
                      function(response) {
                        if(response.data) {
                          $scope.photo_count = response.data.length;
                        }
                        else {
                          $scope.photo_count = '';
                        }
                        // for counting
                        // if(response.paging.next) {
                        //   countPhotos(response.paging.next);
                        // }
                        if($scope.photo_count) {
                          checkEmail(params, $scope.initial_response, access_token, $scope.photo_count);
                        }
                        else {
                          checkEmail(params, $scope.initial_response, access_token, $scope.photo_count);
                        }
                      }
                    );
                    
                });
                // checkEmail(params, response, access_token);
              }
            });
          }
          else {
            FB.login(function(response) {
              if (response.authResponse) {
                var access_token = response.authResponse.accessToken;
            
                FB.api('/me', 
                  {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday, picture, permissions"},
                  function(response) {
                    var params = {
                      email: response.email
                    };
                    FB.api(
                      '/me/photos',
                      'GET',
                      {"fields":"images,picture", "type":"uploaded"},
                      function(response) {
                        $scope.photo_count = response.data.length;

                        // for counting
                        // if(response.paging.next) {
                        //   countPhotos(response.paging.next);
                        // }
                      }
                    );
                    if($scope.photo_count) {
                      checkEmail(params, response, access_token, $scope.photo_count);
                    }
                    else {
                      checkEmail(params, response, access_token, $scope.photo_count);
                    }
                });
              } else {
                $rootScope.actionLoader = false;
                toastr.warning('User cancelled login or did not fully authorize.');
              }
            }, {scope: 'email,user_birthday,user_friends,user_photos'});
          }
        });
      };

      $scope.googleLogin = function() {
        // console.log('google');
      };

      $scope.login = function(user) {
        $rootScope.actionLoader = true;
        if($scope.loginForm.$invalid) {
          $scope.submitted = true;
          $scope.error.message = 'Invalid credentials.';
          $rootScope.actionLoader = false;
        }
        else {
          var params = {
            usr_email: user.usr_email,
            usr_password: user.usr_passwordz,
            remember: user.remember
          }
          MemberService.authenticate(params).then(function(res) {
            $scope.submitted = true;
            $log.debug(res);
            $rootScope.actionLoader = false;
            if(res.success) {
              $scope.cancelDialog();
              $scope.user = res.data;
              $rootScope.$broadcast('login', $scope.user);
              if(res.message) {
                var confirm = $mdDialog.confirm()
                  .title('SugarFlame')
                  .textContent(res.message)
                  .ariaLabel('Confirm Delete')
                  .targetEvent()
                  .ok('OK');

                $mdDialog.show(confirm).then(function() {
                  // $window.location.reload();
                  if(res.redirect == '') {
                    $state.go('members');
                    $scope.hide = false;
                  }
                  else if(res.redirect == 'initial') {
                    $state.go('account.addPhotos');
                    $scope.hide = true;
                  }
                  else if(res.redirect == 'photo') {
                    $state.go('account.addPhotos');
                    $scope.hide = true;
                  }
                  else if(res.redirect == 'info') {
                    $state.go('account.personal');
                    $scope.hide = true;
                  }
                  else if(res.redirect == 'preference') {
                    $state.go('account.preferences');
                    $scope.hide = true;
                  }
                }, function() {
                  // $window.location.reload();
                  if(res.redirect == '') {
                    $state.go('members');
                    $scope.hide = false;
                  }
                  else if(res.redirect == 'initial') {
                    $state.go('account.addPhotos');
                    $scope.hide = true;
                  }
                  else if(res.redirect == 'photo') {
                    $state.go('account.addPhotos');
                    $scope.hide = true;
                  }
                  else if(res.redirect == 'info') {
                    $state.go('account.personal');
                    $scope.hide = true;
                  }
                  else if(res.redirect == 'preference') {
                    $state.go('account.preferences');
                    $scope.hide = true;
                  }
                });
              }
              else {
                if(res.redirect == '') {
                  // $window.location.reload();
                  $scope.hide = false;
                  $state.go('members');
                }
                else if(res.redirect == 'initial') {
                  $state.go('account.addPhotos');
                  // $window.location.reload();
                  $scope.hide = true;
                }
                else if(res.redirect == 'photo') {
                  $state.go('account.addPhotos');
                  // $window.location.reload();
                  $scope.hide = true;
                }
                else if(res.redirect == 'info') {
                  $state.go('account.personal');
                  // $window.location.reload();
                  $scope.hide = true;
                }
                else if(res.redirect == 'preference') {
                  $state.go('account.preferences');
                  // $window.location.reload();
                  $scope.hide = true;
                }else if(res.redirect == 'search') {
                  $state.go('search');
                  // $window.location.reload();
                  $scope.hide = true;
                }
              }
 
            }
            else {
              $scope.error.message = res.error;
            }
          });
        }
      };

      $scope.logout = function(ev) {
        $scope.close();
        var confirm = $mdDialog.confirm()
          .title('Confirm Logout')
          .textContent('Are you sure you want to logout?')
          .ariaLabel('Confirm Delete')
          .targetEvent(ev)
          .ok('OK')
          .cancel('Cancel');

        $mdDialog.show(confirm).then(function() {
          MemberService.logout().then(function(res){
            $rootScope.actionLoader = true;
            $timeout(function() {
              userData.destroy();
              $state.go('home');
              $scope.user = false;
              $rootScope.$broadcast('logout', $scope.user)
              $rootScope.actionLoader = false;
              userData.destroyViolator();
              delete $rootScope.hide;
              delete $rootScope.user;
              // $window.location.reload();
            }, 300)
          });
        }, function() {
        });
      };

      $scope.$on('logout', function(events, args){
        $scope.user = args; //now we've registered!
      })

      $scope.$on('token changed', function(events, args) {
        $scope.user = args;
      });

      $scope.sendReset = function(data) {
        $rootScope.actionLoader = true;
        MemberService.forgotPassword(data).then(function(res){
          $scope.cancelDialog();
          $rootScope.actionLoader = false;
          if(res.success) {
            toastr.success(res.message);
          }
          else {
            toastr.error(res.error.message);
          }
        }, function() {
          $rootScope.actionLoader = false;
        });
      };

      $scope.baseUrl = function(res) {
        return URL.BASE + res;
      };
    }
  }

})();
