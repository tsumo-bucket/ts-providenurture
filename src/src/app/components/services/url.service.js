
(function() {
  'use strict';

  angular
      .module('sugar')
      .service('URL', function(){

        return {
          // API: 'https://sugar.dev/api/',
          // SITE: 'https://sugar.dev/',
          // RES: 'https://sugar.dev/resources/',
          // BASE: 'https://sugar.dev/',
          // CLIENT: 'https://localhost:3000/',
          // SOCKET: 'https://localhost:3000/'
          // API: 'http://beta.providenurture.com/api/',
          // SITE: 'http://beta.providenurture.com/',
          // RES: 'http://beta.providenurture.com/resources/',
          // BASE: 'http://beta.providenurture.com/',
          // CLIENT: 'http://beta.providenurture.com/web/',
          // SOCKET: 'http://beta.providenurture.com:3000/'
          
          //Main
          // API: 'https://www.providenurture.com/backend/api/',
          // SITE: 'https://www.providenurture.com/',
          // RES: 'https://www.providenurture.com/backend/resources/',
          // BASE: 'https://www.providenurture.com/backend/',
          // CLIENT: 'https://www.providenurture.com/',
          // SOCKET: 'https://www.providenurture.com:3000/'
          // API: 'https://www.sugarflame.com/api/',

          //demosite
          // API: 'http://beta.providenurture.com/backend/api/',
          // SITE: 'http://beta.providenurture.com/',
          // RES: 'http://beta.providenurture.com/resources/',
          // BASE: 'http://beta.providenurture.com/backend/',
          // CLIENT: 'http://beta.providenurture.com/',
          // SOCKET: 'https://wwww.sugarflame.com:3000/'

          //live
          // API: 'https://www.providenurture.com/backend/api/',
          // SITE: 'https://www.providenurture.com/',
          // RES: 'https://www.providenurture.com/resources/',
          // BASE: 'https://www.providenurture.com/backend/',
          // CLIENT: 'https://www.providenurture.com/',
          // SOCKET: 'https://wwww.sugarflame.com:3000/'

          //local
          API: 'http://localhost/ts-providenurture/api/',
          SITE: 'http://localhost:3000/',
          RES: 'http://localhost/ts-providenurture/resources/',
          BASE: 'http://localhost/ts-providenurture/',
          CLIENT: 'http://localhost:3000/',
          SOCKET: 'http://localhost:3000/'
        };
      });
})();
