(function() {
  'use strict';

  angular
      .module('sugar')
      .service('userData', userData);

  /** @ngInject */
  function userData($window, $rootScope) {

    //to handle redirects
    var _redirectStateName = 'redStateName';
    var _redirectStateParams = 'redStateParams';

    this.isLogged = function() {
      try {
        return ($window.localStorage.sugar_user)?$window.localStorage.sugar_user:$window.sessionStorage.sugar_user;
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
      }
    };

    this.setUser = function(aUser, remember) {
      try {
        if (remember){
          $window.localStorage.sugar_user = angular.toJson(aUser);
        } else {
          if($window.localStorage)
          {
            $window.localStorage.sugar_user = angular.toJson(aUser);
          }
          else
          {
            $window.sessionStorage.sugar_user = angular.toJson(aUser);
          }
        }
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
      }
    };

    this.setTempUser = function(aUser, remember) {
      try {
        if (remember){
          $window.localStorage.sugar_temp = angular.toJson(aUser);
        } else {
          if($window.localStorage)
          {
            $window.localStorage.sugar_temp = angular.toJson(aUser);
          }
          else
          {
            $window.sessionStorage.sugar_temp = angular.toJson(aUser);
          }
        }
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
      }
    };

    this.setAccount = function(aUser) {
      try {
        if($window.localStorage)
        {
          $window.localStorage.sugar_user = angular.toJson(aUser);
        }
        else
        {
          $window.sessionStorage.sugar_user = angular.toJson(aUser);
        }
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
      }
    };


    this.getAccount = function() {
      try {
        return angular.fromJson(($window.localStorage.sugar_user)?$window.localStorage.sugar_user:$window.sessionStorage.sugar_user);
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
        return false;
      }
    };

    this.getTemp = function() {
      try {
        return angular.fromJson(($window.localStorage.sugar_temp)?$window.localStorage.sugar_temp:$window.sessionStorage.sugar_temp);
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
        return false;
      }
    };

    this.getViolator = function() {
      try {
        return angular.fromJson(($window.localStorage.sugar_violator)?$window.localStorage.sugar_violator:$window.sessionStorage.sugar_violator);
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
        return false;
      }
    };

    this.setUserData = function(key, value) {
      try {
        var user = angular.fromJson(($window.localStorage.sugar_user)?$window.localStorage.sugar_user:$window.sessionStorage.sugar_user);
        user[key] = value;
        if($window.localStorage.sugar_user){
          $window.localStorage.sugar_user = angular.toJson(user);
        } else {
          $window.sessionStorage.sugar_user = angular.toJson(user);
        }
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
      }
    };

    this.setTempData = function(key, value) {
      try {
        var user = angular.fromJson(($window.localStorage.sugar_temp)?$window.localStorage.sugar_temp:$window.sessionStorage.sugar_temp);
        user[key] = value;
        if($window.localStorage.sugar_temp){
          $window.localStorage.sugar_temp = angular.toJson(user);
        } else {
          $window.sessionStorage.sugar_temp = angular.toJson(user);
        }
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
      }
    };

    this.hideViolator = function(aUser) {
      try {
        if($window.localStorage.sugar_violator)
        {
          $window.localStorage.sugar_violator = angular.toJson(aUser);
        }
        else
        {
          $window.sessionStorage.sugar_violator = angular.toJson(aUser);
        }
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
      }
    };

    this.getUserData = function(key) {
      try {
        var user = angular.fromJson(($window.localStorage.sugar_user)?$window.localStorage.sugar_user:$window.sessionStorage.sugar_user);
        return user[key];
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
        return false;
      }
    };

    this.unsetUserData = function(key) {
      try {
        var user = angular.fromJson(($window.localStorage.sugar_user)?$window.localStorage.sugar_user:$window.sessionStorage.sugar_user);
        delete user[key];

        if($window.localStorage.sugar_user){
          $window.localStorage.sugar_user = angular.toJson(user);
        } else {
          $window.sessionStorage.sugar_user = angular.toJson(user);
        }
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
      }
    };

    this.destroy = function() {
      try {
        delete($window.localStorage.sugar_user);
        delete($window.sessionStorage.sugar_user);
        return true;
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
        return false;
      }
    };

    this.destroyViolator = function() {
      try {
        delete($window.localStorage.sugar_violator);
        delete($window.sessionStorage.sugar_violator);
        return true;
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
        return false;
      }
    };

    this.destroyTemp = function() {
      try {
        delete($window.localStorage.sugar_temp);
        delete($window.sessionStorage.sugar_temp);
        return true;
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
        return false;
      }
    };

    //adding redirect functions
    this.setRedirect = function (stateName, stateParams){
      try {
        $window.localStorage[_redirectStateName] = stateName;
        $window.localStorage[_redirectStateParams] = angular.toJson(stateParams);
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
      }
    };

    this.getRedirect = function(){
      try {
        if (!$window.localStorage[_redirectStateName]){
          return false;
        }

        return {
          name : $window.localStorage[_redirectStateName],
          params : angular.fromJson($window.localStorage[_redirectStateParams])
        };
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
        return false;
      }
    };

    this.clearRedirect = function(){
      try {
        delete $window.localStorage[_redirectStateName];
        delete $window.localStorage[_redirectStateParams];
      } catch(e) {
        $rootScope.$broadcast('privateBrowsing', {message: 'You are using private browsing and some features may not work. Normal browsing is recommended.', icon : 'times'});
      }
    };

  }

})();