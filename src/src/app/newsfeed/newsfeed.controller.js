(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('NewsfeedController', NewsfeedController);

  /** @ngInject */
  function NewsfeedController($timeout, webDevTec, toastr, URL, $log, logged, $window, $scope, $rootScope, SettingsService, FeedService, MemberService, userData, FilterFactory, $mdDialog, $mdToast, $state) {
    $scope.filter = FilterFactory;
    $('.navbar').addClass('transluscent');
    $scope.filter.gender = [
      {checked: false, name: 'Men', value: 'male'},
      {checked: false, name: 'Women', value: 'female'}
    ];
    $scope.filter.country = [];
    SettingsService.getCountries().then(function(res){
      if(res.success) {
        $scope.filter.country = res.data[0];
      }
    });
    if(logged) {
      if($rootScope.user.usr_status == 'approved'){
        $rootScope.hide = false;
      }
      else
      {
        if($rootScope.user.usr_signup_step == 'initial') {
          $state.go('account.addPhotos');
          $rootScope.hide = true;
        }
        else if($rootScope.user.usr_signup_step == 'photo') {
          $state.go('account.addPhotos');
          $rootScope.hide = true;
        }
        else if($rootScope.user.usr_signup_step == 'info') {
          $state.go('account.personal');
          $rootScope.hide = true;
        }
        else if($rootScope.user.usr_signup_step == 'preference') {
          $state.go('account.preferences');
          $rootScope.hide = true;
        }
        else if($rootScope.user.usr_signup_step == 'done') {
          $state.go('search');
          $rootScope.hide = true;
        }
      }
    }
    else {
      // $window.location.reload();
      $timeout(function(){
        if(!userData.isLogged()) {
          $state.go('home');
          $rootScope.actionLoader = false;
        }
      });
    }

    function getPosts(tab) {
      $scope.next = false;
      $scope.loading = true;
      var params = {
        link: URL.API + 'posts/list?is_infinite=true&page=1&page_size=20',
        params: {
          source: tab
        }
      };
      FeedService.getPosts(params).then(function(res){
        if(res.success){
          $scope.posts = res.data[0];
          $scope.loading = false;
          if(res.pagination) {
            if(res.pagination.num_rows > $scope.posts.length) {
              $scope.next = res.pagination.links.next
            }
            else {
              $scope.next = false;
            }
          }
        }
        else {
          $scope.message = 'Nothing to display.';
          $scope.loading = false;
          $scope.posts = [];
        }
      }, function(err) {
        $scope.loading = false;
        $scope.message = 'Something went wrong. Try again later.';
      });
    }


    if($state.current.name == 'dashboard') {
      $scope.source = 'consolidated';
      getPosts('consolidated');
    }
    else if($state.current.name == 'dashboard.faved') {
      $scope.source = 'i-faved';
      getPosts('i-faved');
    }
    else if($state.current.name == 'dashboard.followed') {
      $scope.source = 'i-followed';
      getPosts('i-followed');
    }
    else if($state.current.name == 'dashboard.messaged') {
      $scope.source = 'i-messaged';
      getPosts('i-messaged');
    }

    $scope.filter = FilterFactory;
    $scope.user = $rootScope.user;

    SettingsService.getInterest().then(function(res) {
      // console.log(res);
      if(res.success) {
        $scope.user.usr_interested = res.data[0].usr_interested;
        $scope.user.usr_interested_age_from = res.data[0].usr_interested_age_from;
        $scope.user.usr_interested_age_to = res.data[0].usr_interested_age_to;
        $scope.user.usr_interested_country = res.data[0].usr_interested_country;
        $scope.user.distance_from = 1;
        $scope.user.distance_to = 5;
        // get value
        if(res.data[0].usr_interested == 'both')
        {
          $scope.filter.gender = [
            {checked: true, name: 'Man', value: 'male'},
            {checked: true, name: 'Woman', value: 'female'}
          ];
        }
        else {
          var gender = _.where($scope.filter.gender, {value: $scope.user.usr_interested})
                      .map(function(item) { 
                        item.checked = true; return item; }); 
          var reject = _.reject($scope.filter.gender, {value: gender[0].value});
          reject.push(gender[0]);
          $scope.filter.gender = reject;
        }
      }
    });

    $scope.$on('rzSliderForceRender', function(events, args){
      var max = angular.element(document.querySelector('.rzslider'));
      max.click();
    });
    $scope.openCriteria = function(ev) {
      $timeout(function () {
          $rootScope.$broadcast('rzSliderForceRender');
      }, 500);
     
      $scope.$broadcast('rzSliderForceRender', $scope.ageRangeSlider);
      $scope.user.usr_interested_age_from = $scope.user.usr_interested_age_from;
      $scope.user.usr_interested_age_to = $scope.user.usr_interested_age_to;
      $mdDialog.show({
        controller: NewsfeedController,
        templateUrl: 'app/dashboard/criteria.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false // Only for -xs, -sm breakpoints.
      })
      .then(function(answer) {
        // console.log(answer);
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
    };
    

    $scope.ageRangeSlider = {
      options: {
        floor: 18,
        ceil: 80,
        step: 1
      }
    };

    $scope.distanceRangeSlider = {
      options: {
        floor: 1,
        ceil: 10,
        step: 1
      }
    };

    $scope.applyCriteria = function(criteria) {
      $scope.cancel();
      var gender;
      if(_.where($scope.filter.gender, {checked: true}).map(function(item) { return item.value; }).length == 2) {
        gender = 'both';
      }
      else {
        gender = _.findWhere($scope.filter.gender, {checked: true}).value;
      }
      var params = {
        usr_interested: gender,
        usr_interested_age_from: criteria.usr_interested_age_from,
        usr_interested_age_to: criteria.usr_interested_age_to,
        usr_interested_country: criteria.usr_interested_country
      };
      // console.log(params);
      SettingsService.updatePreference(params).then(function(res) {
        if(res.success) {
          $window.location.reload();
        }
        else {
          toastr.error(res.error.message);
        }
      });

    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.answer = function(answer) {
      $mdDialog.hide(answer);
    };

    $scope.onTabSelected = function(tab) {
      console.log(tab);
      $scope.next = false;
      $scope.message = '';
      $scope.source = tab;
      $scope.loading = true;
      if(tab) {
        var params = {
          link: URL.API + 'posts/list?is_infinite=true&page=1&page_size=20',
          params: {
            source: tab
          }
        };
      }
      $scope.posts = false;
      FeedService.getPosts(params).then(function(res){
        if(res.success){
          $scope.posts = res.data[0];
          $scope.loading = false;
          if(res.pagination) {
            if(res.pagination.num_rows > $scope.posts.length) {
              $scope.next = res.pagination.links.next
            }
            else {
              $scope.next = false;
            }
          }
        }
        else {
          $scope.posts = [];
          $scope.message = 'Nothing to display.';
          $scope.loading = false;
        }
      }, function(err) {
        $scope.loading = false;
        $scope.message = 'Something went wrong. Try again later.';
      });
    }

    $scope.post = function(post) {
      $scope.errMessage = "";
      if(post) {
        var params = {
          pos_content: post
        }

        FeedService.createPost(params).then(function(res) {
          if(res.success) {
            $scope.post.pos_content = '';
            $scope.postForm.pos_content.$setPristine();
            toastr.success('Posted!');
            if($scope.source == 'consolidated') {
              $scope.posts.unshift(res.data[0]);
            }
          }
          else {
            toastr.error(res.error.message);
          }
        });
      }
      else {
        $scope.errMessage = "This post appears to be blank. Please write something to post.";
      }
    };

    $scope.postOnEnter = function(post, evt) {
      if(evt.keyCode == 13) {
        $scope.post(post);
      }
    };

    $scope.loadMore = function(next) {
      $scope.loadingMore = true;
      $scope.next = false;
      var params = {
        link: next,
        params: {
        }
      };
      
      FeedService.getPosts(params).then(function(res){
        console.log(res);
        if(res.success){
          for(var x = 0; x < res.data[0].length; x++)
          {
            $scope.posts.push(res.data[0][x]);
          }
          $scope.loading = false;
          if(res.pagination) {
            if(res.pagination.num_rows > $scope.posts.length) {
              $scope.next = res.pagination.links.next
            }
            else {
              $scope.next = false;
            }
          }
          $scope.loadingMore = false;
        } 
        else {
          $scope.loading = false;
          $scope.message = 'Nothing to display.';
          $scope.loadingMore = false;
        }
      });

    }

  }
})();
