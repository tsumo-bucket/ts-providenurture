(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('PrivacyController', PrivacyController);

  /** @ngInject */
  function PrivacyController($timeout, webDevTec, toastr, $scope, $log, logged, FeedService, MemberService, SettingsService, FilterFactory, $mdDialog, $mdToast, $state, $rootScope, userData) {
    if(logged) {
      // $rootScope.actionLoader = false;
    }
    else {
      // $window.location.reload();
      $timeout(function(){
        if(!userData.isLogged()) {
          $state.go('home');
          $rootScope.actionLoader = false;
        }
      }, 100);
    }
    $scope.isActive = function(state) {
      if($state.current.name == state) {
        return true;
      }
      else {
        return false;
      }
    }
    $scope.source = 'settings.privacy';
    $scope.onTabSelected = function(src) {
      $state.go(src);
    };
    SettingsService.getPrivacy().then(function(res) {
      
      var data = res.data[0][0];
      $scope.user.pri_id = data.pri_id;
      if(data.pri_comment == 'yes') {
        $scope.user.pri_comment = true;
      }
      else {
        $scope.user.pri_comment = false;
      }
      if(data.pri_profile == 'yes') {
        $scope.user.pri_profile = true;
      }
      else {
        $scope.user.pri_profile = false;
      }
    });

    $scope.loadingBlocks = true;
    FeedService.getBlocks().then(function(res) {
      $scope.loadingBlocks = false;
      if(res.success) {
        $scope.blocks = res.data[0];
      }
    });

    $scope.unblock = function(blo_id, usr_id) {
      var ind = _.findIndex($scope.blocks, {blo_id: blo_id}) ;
      var params = {
        blo_id: blo_id,
        blo_user: usr_id,
        blo_status: 'inactive'
      };

      MemberService.block(params).then(function(res) {
        
        if(res.success) {
          toastr.success('Unblocked');
          $scope.blocks.splice(ind, 1);
        } 
        else {

        }
      });
    };

    $scope.savePrivacy = function(data) {
      $rootScope.actionLoader = true;
      if(data.pri_comment) {
        data.pri_comment = 'yes';
      }
      else {
        data.pri_comment = 'no';
      }

      if(data.pri_profile) {
        data.pri_profile = 'yes';
      }
      else {
        data.pri_profile = 'no';
      }

      var params = {
        pri_profile: data.pri_profile,
        pri_comment: data.pri_comment,
        pri_id: data.pri_id
      }

      SettingsService.updatePrivacy(params).then(function(res) {
        $rootScope.actionLoader = false;
        
        if(res.success) {
          toastr.success('Changes Saved');
          if(res.data[0].pri_comment == 'yes') {
            $scope.user.pri_comment = true;
          }
          else {
            $scope.user.pri_comment = false;
          }

          if(res.data[0].pri_profile == 'yes') {
            $scope.user.pri_profile = true;
          }
          else {
            $scope.user.pri_profile = false;
          }
        }
        else {
          toastr.error(res.error.message);
        }
      }, function(err) {

      });
    };

    $scope.goHome = function() {
      $scope.cancel();
    }

  }
})();
