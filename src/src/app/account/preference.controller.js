(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('PreferenceController', PreferenceController);

  /** @ngInject */
  function PreferenceController($timeout, webDevTec, toastr, $scope, $log, logged, FeedService, MemberService, SettingsService, FilterFactory, $mdDialog, $mdToast, $state, $rootScope, userData) {
   if(logged) {
      // $rootScope.actionLoader = false;
    }
    else {
      // $window.location.reload();
      $timeout(function(){
        if(!userData.isLogged()) {
          $state.go('home');
          $rootScope.actionLoader = false;
        }
      }, 100);
    }

    $scope.goto = function(link) {
      $state.go(link);
    };

    $scope.isActive = function(state) {
      if($state.current.name == state) {
        return true;
      }
      else {
        return false;
      }
    };

    $scope.filter = FilterFactory;
    $scope.source = 'settings.preferences';
    $scope.onTabSelected = function(src) {
      $state.go(src);
    };
     // GET Countries
    $scope.filter.country = [];
    SettingsService.getCountries().then(function(res){
      if(res.success) {
        $scope.filter.country = res.data[0];
      }
    });

    $scope.savePreference = function(data) {
      $rootScope.actionLoader = true;
      
      if($scope.settingsForm.$invalid) {
        $rootScope.actionLoader = false;
        toastr.warning('Make sure to fill out the fields correctly');
      }
      else {
        if(data.usr_interested_age_from >= 18 & data.usr_interested_age_to <= 80) {
          var params = {
            usr_interested: data.usr_interested,
            usr_interested_age_from: data.usr_interested_age_from,
            usr_interested_age_to: data.usr_interested_age_to,
            usr_interested_country: data.usr_interested_country 
          };
          
          SettingsService.updatePreference(params).then(function(res) {
            $rootScope.actionLoader = false;
            if(res.success) {
              toastr.success(res.message);
            }
            else {
              toastr.error(res.error.message);
            }
          });
        }
        else {
          $rootScope.actionLoader = false;
          toastr.warning('Make sure to fill out the fields correctly');
        }
      }
    };

    $scope.goHome = function() {
      $scope.cancel();
    };

  }
})();
