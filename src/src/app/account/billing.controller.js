(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('BillingController', BillingController);

  /** @ngInject */
  function BillingController($timeout, webDevTec, toastr, $scope, $log, logged, FeedService, MemberService, SettingsService, FilterFactory, $mdDialog, $mdToast, $state, $rootScope, userData) {
    if(logged) {
      // $rootScope.actionLoader = false;
    }
    else {
      // $window.location.reload();
      $timeout(function(){
        if(!userData.isLogged()) {
          $state.go('home');
          $rootScope.actionLoader = false;
        }
      }, 100);
    }
    $scope.isActive = function(state) {
      if($state.current.name == state) {
        return true;
      }
      else {
        return false;
      }
    }

    $scope.source = 'settings.billing';
    $scope.onTabSelected = function(src) {
      $state.go(src);
    };

    $scope.goHome = function() {
      $scope.cancel();
    }

  }
})();
