(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('NotificationController', NotificationController);

  /** @ngInject */
  function NotificationController($timeout, webDevTec, toastr, $scope, $log, logged, FeedService, MemberService, SettingsService, FilterFactory, $mdDialog, $mdToast, $state, $rootScope, userData) {
    if(logged) {
      // $rootScope.actionLoader = false;
    }
    else {
      // $window.location.reload();
      $timeout(function(){
        if(!userData.isLogged()) {
          $state.go('home');
          $rootScope.actionLoader = false;
        }
      }, 100);
    }
    $scope.isActive = function(state) {
      if($state.current.name == state) {
        return true;
      }
      else {
        return false;
      }
    }
    $scope.source = 'settings.notification';
    $scope.onTabSelected = function(src) {
      $state.go(src);
    };

    SettingsService.getNotification().then(function(res) {
      var data = res.data[0][0];
      $scope.user.not_id = data.not_id;
      if(data.not_message == 'yes') {
        $scope.user.not_message = true;
      }
      else {
        $scope.user.not_message = false;
      }
      if(data.not_like == 'yes') {
        $scope.user.not_like = true;
      }
      else {
        $scope.user.not_like = false;
      }
      if(data.not_content == 'yes') {
        $scope.user.not_content = true;
      }
      else {
        $scope.user.not_content = false;
      }
      if(data.not_profile == 'yes') {
        $scope.user.not_profile = true;
      }
      else {
        $scope.user.not_profile = false;
      }
      if(data.not_email == 'yes') {
        $scope.user.not_email = true;
      }
      else {
        $scope.user.not_email = false;
      }
    });

    $scope.saveNotification = function(data) {
      $rootScope.actionLoader = true;
      if(data.not_message) {
        data.not_message = 'yes';
      }
      else {
        data.not_message = 'no';
      }

      if(data.not_like) {
        data.not_like = 'yes';
      }
      else {
        data.not_like = 'no';
      }

      if(data.not_profile) {
        data.not_profile = 'yes';
      }
      else {
        data.not_profile = 'no';
      }

      if(data.not_content) {
        data.not_content = 'yes';
      }
      else {
        data.not_content = 'no';
      }

      if(data.not_email) {
        data.not_email = 'yes';
      }
      else {
        data.not_email = 'no';
      }

      var params = {
        not_like: data.not_like,
        not_message: data.not_message,
        not_profile: data.not_profile,
        not_content: data.not_content,
        not_email: data.not_email,
        not_id: data.not_id
      }

      SettingsService.updateNotification(params).then(function(res) {
        $rootScope.actionLoader = false;
        if(res.success) {
          toastr.success(res.message);
          if(res.data[0].not_message == 'yes') {
            $scope.user.not_message = true;
          }
          else {
            $scope.user.not_message = false;
          }

          if(res.data[0].not_like == 'yes') {
            $scope.user.not_like = true;
          }
          else {
            $scope.user.not_like = false;
          }

          if(res.data[0].not_profile == 'yes') {
            $scope.user.not_profile = true;
          }
          else {
            $scope.user.not_profile = false;
          }

          if(res.data[0].not_content == 'yes') {
            $scope.user.not_content = true;
          }
          else {
            $scope.user.not_content = false;
          }

          if(res.data[0].not_email == 'yes') {
            $scope.user.not_email = true;
          }
          else {
            $scope.user.not_email = false;
          }
        }
        else {
          toastr.error(res.error.message);
        }
      }, function(err) {

      });
    };

    $scope.goHome = function() {
      $scope.cancel();
    }

  }
})();
