(function() {
  'use strict';

  angular
    .module('sugar')
    .service('SettingsService', SettingsService);

  function SettingsService($http, $q, URL, userData, $log) {

    this.getSubscription = function(id) {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'subscriptions/view',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        params: {sub_id: id}
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        // $log.debug('authenticate error: ' + error);
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getPageContent = function(id) {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'pages/view',
        params: {pag_id: id}
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        // $log.debug('authenticate error: ' + error);
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getCountries = function(id) {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'countries/list'
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        // $log.debug('authenticate error: ' + error);
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getRegions = function(id) {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'regions/list',
        params: {cnt_id: id}
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        // $log.debug('authenticate error: ' + error);
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getCities = function(id) {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'cities/list',
        params: {reg_id: id}
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        // $log.debug('authenticate error: ' + error);
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getCity = function(id) {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'cities/view',
        params: {cty_id: id}
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        // $log.debug('authenticate error: ' + error);
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getNotification = function() {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'notifications/list',
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        // $log.debug('authenticate error: ' + error);
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getPrivacy = function() {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'privacies/list',
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        // $log.debug('authenticate error: ' + error);
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getInfo = function() {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'infos/view',
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        // $log.debug('authenticate error: ' + error);
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getInfoList = function() {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'infos/list',
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        // $log.debug('authenticate error: ' + error);
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getPreference = function(id) {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'preferences/view',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        params: {prf_id: id}
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        // $log.debug('authenticate error: ' + error);
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getPreferences = function(id) {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'preferences/list',
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        // $log.debug('authenticate error: ' + error);
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getInterest = function(id) {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: URL.API + 'users/preference',
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        // $log.debug('authenticate error: ' + error);
        deferResult.reject(error);
      });

      return deferResult.promise;
    };


    this.updatePreference = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/update_preference',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
        if(response.success) {
          userData.setUserData('usr_interested_country', response.data[0].usr_interested_country);
          userData.setUserData('usr_interested', response.data[0].usr_interested);
          userData.setUserData('usr_interested_age_from', response.data[0].usr_interested_age_from);
          userData.setUserData('usr_interested_age_to', response.data[0].usr_interested_age_to);
        }
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.updateAccount = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/account',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
        userData.setUserData(response.data[0]);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.updatePassword = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/password',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.updateNotification = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'notifications/update',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.updatePrivacy = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'privacies/update',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.updateInfo = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'infos/edit',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.updateQuestion = function(data) {
      var deferResult = $q.defer();
      $log.debug(data);
      $http({
        method: 'POST',
        url: URL.API + 'preferences/edit',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.createPreference = function(data) {
      var deferResult = $q.defer();
      $log.debug(data);
      $http({
        method: 'POST',
        url: URL.API + 'preferences/create',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.fbConnect = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'users/facebook',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

  }

})();
