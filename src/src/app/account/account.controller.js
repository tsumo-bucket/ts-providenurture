(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('AccountController', AccountController);

  /** @ngInject */
  function AccountController($timeout, webDevTec, toastr, $scope, $log, logged, $window, FeedService, MemberService, SettingsService, FilterFactory, $mdDialog, $mdToast, $state, $rootScope, userData) {
    $scope.isActive = function(state) {
      if($state.current.name == state) {
        return true;
      }
      else {
        return false;
      }
    }
    if(logged) {
      // $rootScope.actionLoader = false;
    }
    else {
      // $window.location.reload();
      $timeout(function(){
        if(!userData.isLogged()) {
          $state.go('home');
          $rootScope.actionLoader = false;
        }
      });
    }

    $scope.source = 'settings.account';
    $scope.onTabSelected = function(src) {
      $state.go(src);
    };

    MemberService.getUser().then(function(res) {
      if(res.success) {
        $rootScope.user = res.data[0];
      }
    });

    $scope.filter = FilterFactory;
    $scope.changeDays = function(month) {
      // console.log(month)
      $scope.filter.days = [];
      if(month == 2) {
        var x = 0;
        for (var d = 1; d <= 28; d++)
        {
          $scope.filter.days[x] = {
              name: d,
              value: d
          };
          x++;
        }
      }
      else if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
        var x = 0;
        for (var d = 1; d <= 31; d++)
        {
          $scope.filter.days[x] = {
              name: d,
              value: d
          };
          x++;
        }
      }
      else if(month == 4 || month == 6 || month == 9 || month == 11) {
        var x = 0;
        for (var d = 1; d <= 30; d++)
        {
          $scope.filter.days[x] = {
              name: d,
              value: d
          };
          x++;
        }
      }
    };


  	$scope.openProof = function(ev) {
      $mdDialog.show({
        controller: AccountController,
        templateUrl: 'app/account/proof.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false // Only for -xs, -sm breakpoints.
      })
      .then(function(answer) {
        $log.debug(answer);
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
    };

    $scope.openDelete = function(ev) {
      $mdDialog.show({
        controller: AccountController,
        bindToController: true,
        scope: $scope,
        templateUrl: 'app/account/delete-modal.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false // Only for -xs, -sm breakpoints.
      })
      .then(function(answer) {
        $log.debug(answer);
      }, function() {
        $state.go('home');
      });
    };

    $scope.openPassword = function(ev) {
      $mdDialog.show({
        controller: AccountController,
        bindToController: true,
        scope: $scope,
        templateUrl: 'app/account/password-modal.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false // Only for -xs, -sm breakpoints.
      })
      .then(function(answer) {
        $log.debug(answer);
        // $window.location.reload();
      }, function() {
        // $window.location.reload();
      });
    };

    $scope.openDeactivate = function(ev) {
      $mdDialog.show({
        controller: AccountController,
        scope: $scope,
        bindToController: true,
        templateUrl: 'app/account/deactivate-modal.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false // Only for -xs, -sm breakpoints.
      })
      .then(function(answer) {
        $log.debug(answer);
      }, function() {
        $state.go('home');
      });
    };

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.saveAccount = function(data) {
      $rootScope.actionLoader = true;
      var params = {
        usr_first_name: data.usr_first_name,
        usr_last_name: data.usr_last_name,
        usr_type: data.usr_type,
        usr_birthmonth: data.usr_birthmonth,
        usr_birthdate: data.usr_birthdate,
        usr_birthyear: data.usr_birthyear,
        src_id: data.src_id
      }

      SettingsService.updateAccount(params).then(function(res){
        $rootScope.actionLoader = false;
        if(res.success) {
          toastr.success(res.message);
        }
        else {
          toastr.error(res.error.message);
        }
      });
    };

    $scope.updatePassword = function(data) {
      $rootScope.actionLoader = true;
      var params = {
        usr_password: data.usr_current_password,
        usr_passwordn: data.usr_new_password,
      }

      SettingsService.updatePassword(params).then(function(res){
        $rootScope.actionLoader = false;
        if(res.success) {
          toastr.success('Password Updated');
          $scope.cancel();
          $timeout(function() {
            $window.location.reload();
          },100);
        }
        else {
          toastr.error(res.error.message);
        }
      });
    };

    $scope.goHome = function() {
      $scope.cancel();
    }

    // DELETE ACCOUNT
    $scope.delete = function(data) {
      $scope.submitted = true;
      
      if(data.usr_passwordf && data.usr_emailf) {
        
        var reason = '';
        if(data.found_ideal) {
          reason += "I found the ideal relationship;";
        }
        if(data.not_say) {
          reason += "I'd rather not say";
        }        
        if(data.not_looking) {
          reason += "I'm no longer looking";
        }      
        if(data.too_busy) {
          reason += "I'm too busy";
        }  
        if(data.take_break) {
          reason += "I'm taking a break";
        }     
        if(data.not_using) {
          reason += "I'm not using this service";
        }        
        if(data.not_want) {
          reason += "I didn't find what I wanted";
        }       
        if(data.not_safe) {
          reason += "I didn't feel safe;";
        }
        if(data.other) {
          reason += data.other_reason;
        }     

        var params = {
          usr_email: data.usr_emailf,
          usr_password: data.usr_passwordf,
          usi_reason: reason       
        }; 

        if(data.usi_comment) {
          params.usi_comment = data.usi_comment;
        } else {
          params.usi_comment = '';
        }

        if(reason != '') {
          MemberService.delete(params).then(function(res) {
            
            if(res.success) {
              userData.destroy();
              $rootScope.user_screen_name = $rootScope.user.usr_screen_name;
              $scope.openDelete();
            }
            else {
              toastr.error(res.error.message);
            }
          });
        }
        else {
          toastr.error('Please select at least one reason for deactivation.');
        }

      }
      else {
        toastr.error('Please input password and email to continue.');
      }
    };
    //DEACTIVATE ACCOUNT
    $scope.deactivate = function(data) {
      $scope.submitted = true;
      if(data.usr_passwordf) {
        
        var reason = '';
        if(data.found_ideal) {
          reason += "I found the ideal relationship;";
        }
        if(data.not_say) {
          reason += "I'd rather not say";
        }        
        if(data.not_looking) {
          reason += "I'm no longer looking";
        }      
        if(data.too_busy) {
          reason += "I'm too busy";
        }  
        if(data.take_break) {
          reason += "I'm taking a break";
        }     
        if(data.not_using) {
          reason += "I'm not using this service";
        }        
        if(data.not_want) {
          reason += "I didn't find what I wanted";
        }       
        if(data.not_safe) {
          reason += "I didn't feel safe;";
        }
        if(data.other) {
          reason += data.other_reason;
        }     

        var params = {
          usr_password: data.usr_passwordf,
          usi_reason: reason       
        }; 

        if(data.usi_comment) {
          params.usi_comment = data.usi_comment;
        }

        if(reason != '') {
          MemberService.deactivate(params).then(function(res) {
            $log.debug(res['error']);
            if(res.success) {
              userData.destroy();
              $rootScope.user_screen_name = $rootScope.user.usr_screen_name;
              $scope.openDeactivate();
            }
            else {
              toastr.error(res.error.message);
            }
          });
        }
        else {
          toastr.error('Please select at least one reason for deactivation.');
        }

      }
      else {
        toastr.error('Please input password to continue.');
      }
    };

    //CONNECT TO FACEBOOK
    var checkEmail = function(params, response, access_token, photo_count) {
      var params = {
        fb_email: response.email,
        first_name: response.first_name,
        last_name: response.last_name,
        fb_image: response.picture.data.url,
        fb_birthday: response.birthday,
        friend_count: response.friends.summary.total_count,
        photo_count: photo_count,
        fb_id: response.id,
        access_token: access_token
      };

      MemberService.linkFacebook(params).then(function(res) {
        $rootScope.actionLoader = false;
        if(res.success) {
          $rootScope.user.usa_id = res.data[0].usa_id;
          $rootScope.user.usa_image = res.data[0].usa_image;
          $rootScope.user.usa_first_name = res.data[0].usa_first_name;
          $rootScope.user.usa_last_name = res.data[0].usa_last_name;
        }
        else {
          toastr.warning(res.error.message);
        }
      }, function(err) {
        console.log(err);
      });
    };
    $scope.fbConnect = function() {
      $rootScope.actionLoader = true;
      FB.getLoginStatus(function(response) {
        if(response.status == 'connected') {
          var access_token = response.authResponse.accessToken;
          FB.api('/me',
            {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday, picture, permissions"},
            function(response) {
            if(!response.birthday || !response.friends) {
              FB.login(function(response) {
                if (response.authResponse) {
                  var access_token = response.authResponse.accessToken;
              
                  FB.api('/me', 
                    {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday"},
                    function(response) {
                      var params = {
                        email: response.email
                      };
                      FB.api(
                        '/me/photos',
                        'GET',
                        {"fields":"images,picture", "type":"uploaded"},
                        function(response) {
                          $scope.photo_count = response.data.length;
                        }
                      );

                      if($scope.photo_count) {
                        checkEmail(params, response, access_token, $scope.photo_count);
                      }
                      else {
                        checkEmail(params, response, access_token, $scope.photo_count);
                      }
                  });
                } else {
                  $rootScope.actionLoader = false;
                  toastr.warning('User cancelled login or did not fully authorize.');
                }
              }, {scope: 'email,user_birthday,user_friends,user_photos'});
            }
            else {
              var params = {
                email: response.email
              };
              FB.api('/me', 
                {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday, picture, permissions"},
                function(response) {
                  $scope.initial_response = response;
                  var params = {
                    email: response.email
                  };
                  FB.api(
                    '/me/photos',
                    'GET',
                    {"fields":"images,picture", "type":"uploaded"},
                    function(response) {
                      if(response.data) {
                        $scope.photo_count = response.data.length;
                      }
                      else {
                        $scope.photo_count = '';
                      }
                      // for counting
                      // if(response.paging.next) {
                      //   countPhotos(response.paging.next);
                      // }
                      if($scope.photo_count) {
                        checkEmail(params, $scope.initial_response, access_token, $scope.photo_count);
                      }
                      else {
                        checkEmail(params, $scope.initial_response, access_token, $scope.photo_count);
                      }
                    }
                  );
                  
              });
              // checkEmail(params, response, access_token);
            }
          });
        }
        else {
          FB.login(function(response) {
            if (response.authResponse) {
              var access_token = response.authResponse.accessToken;
              FB.api('/me', 
                {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday, picture, permissions"},
                function(response) {
                  var params = {
                    email: response.email
                  };
                  FB.api(
                    '/me/photos',
                    'GET',
                    {"fields":"images,picture", "type":"uploaded"},
                    function(response) {
                      $scope.photo_count = response.data.length;

                      // for counting
                      // if(response.paging.next) {
                      //   countPhotos(response.paging.next);
                      // }
                    }
                  );
                  if($scope.photo_count) {
                    checkEmail(params, response, access_token, $scope.photo_count);
                  }
                  else {
                    checkEmail(params, response, access_token, $scope.photo_count);
                  }
              });
            } else {
              $rootScope.actionLoader = false;
              toastr.warning('User cancelled login or did not fully authorize.');
            }
          }, {scope: 'email,user_birthday,user_friends,user_photos'});
        }
      });
    };
    $scope.disconnect = function(id) {
      var id = id;
       var confirm = $mdDialog.confirm()
        .title('Disconnect Facebook')
        .textContent('Are you sure you want to disconnect Facebook from your account?')
        .ariaLabel('Confirm Disconnection')
        .targetEvent()
        .ok('OK')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function() {
        $rootScope.actionLoader = true;
        MemberService.unlinkFacebook(id).then(function(res) {
          $rootScope.actionLoader = false;
          if(res.success) {
            $rootScope.user.usa_id = 0;
            $rootScope.user.usa_image = 0;
            $rootScope.user.usa_first_name = 0;
            $rootScope.user.usa_last_name = 0;
          }
        });
      }, function() {
      });
    };
  }
})();
