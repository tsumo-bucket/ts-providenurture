(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('MessagesController', MessagesController);

  /** @ngInject */
  function MessagesController($timeout, webDevTec, toastr, $scope, $log, URL, $window, FeedService, MessageService, MemberService, SettingsService, FilterFactory, $mdDialog, $mdToast, $state, $rootScope, userData) {
    $('.navbar').addClass('transluscent');

    if(userData.isLogged()) {
      $rootScope.actionLoader = false;
    }
    else {
      $timeout(function(){
        // $window.location.reload();
        if(!userData.isLogged()) {
          $state.go('home');
          $state.go('login', {url: 'messages'});
          $rootScope.actionLoader = false;
        }
      });
    }
    var params = {
      link: URL.API + 'messages/list?is_infinite=true&page=1&page_size=15',
      params: {
        source: 'inbox'
      }
    };
    $scope.next = false;
    function getMessages(params) {
      MessageService.getConversations(params).then(function(res) {
        $scope.errmessage = '';
        if(res.success) {
          $scope.messages = res.data[0];
          $scope.loading = false;
          if(res.pagination) {
            if(res.pagination.links) {
              if(res.pagination.links.next) {
                $scope.next = res.pagination.links.next;
              }
            }
            if($scope.messages.length == res.pagination.num_rows) {
              $scope.next = false;
            }
            else if(res.data.length < 15) {
              $scope.next = false;
            }
          }
        }
        else {
          if($scope.source == 'sent') {
            $scope.errmessage = 'Try sending a message and break the ice.';
          }
          else {
            $scope.errmessage = 'It seems nobody has sent you a message. Try reaching out and sending a message to get a reply.';
          }
          $scope.loading = false;
          $scope.messages = [];
          $scope.next = false;
        }
      }, function(err) {
        console.log(err);
      });
    };
    getMessages(params);

    $scope.loadMore = function(source, next) {
      var params = {
        link: next,
        params: {
          source: source
        }
      };
      $scope.next = false;
      MessageService.getConversations(params).then(function(res) {
        $scope.errmessage = '';
        // console.log(res);
        if(res.success) {
          for(var x = 0; x < res.data[0].length; x++) {
            $scope.messages.push(res.data[0][x]);
          }
          $scope.loading = false;
          if(res.pagination) {
            if(res.pagination.links) {
              if(res.pagination.links.next) {
                $scope.next = res.pagination.links.next;
              }
            }
            if($scope.messages.length == res.pagination.num_rows) {
              $scope.next = false;
            }
            else if(res.data.length < 15) {
              $scope.next = false;
            }
          }
        }
        else {
          $scope.loading = false;
          $scope.next = false;
        }
      }, function(err) {
        console.log(err);
      });
    };

    MessageService.getCount().then(function(res) {
      $rootScope.$broadcast('get counter', res);
      if(res.success) {
        $scope.count = res.data[0].unread_count;
        $scope.loading = false;
      }
    });

    $scope.source = 'inbox';
    $scope.loading = true;
    $scope.onTabSelected = function(tab) {
      $scope.next = false;
      $scope.source = tab;
      $scope.errmessage = '';
      $scope.loading = true;
      var params = {
        link: URL.API + 'messages/list?is_infinite=true&page=1&page_size=15',
        params: {
          source: tab
        }
      }
      getMessages(params);
    };
    
    $scope.deleteMessage = function(token, msg) {
      var confirm = $mdDialog.confirm()
        .title('Confirm Delete')
        .textContent('Are you sure you want to delete this entire thread? Both you and the recipient will no longer see your message thread.')
        .ariaLabel('Confirm Delete')
        .targetEvent()
        .ok('OK')
        .cancel('Cancel');

      var ind = _.findIndex($scope.messages, {cnv_id: msg.cnv_id}) ;
      var params = {
        usr_token_id: token
      };

      $mdDialog.show(confirm).then(function() {
        MessageService.deleteMessage(params).then(function(res) {
          $scope.messages.splice(ind, 1);
          toastr.success('Message Thread Deleted');
        });
      }, function() {
      });
    };

  }
})();
