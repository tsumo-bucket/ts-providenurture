(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('MessageController', MessageController);

  /** @ngInject */
  function MessageController($timeout, webDevTec, logged, URL, toastr, $scope, $window, $mdDialog, $location, $anchorScroll, $log, FeedService, MessageService, mySocket, MemberService, SettingsService, $state, $stateParams, $rootScope, userData) {
    $('.navbar').addClass('transluscent');
    var loading = true;

    if(logged) {
      $rootScope.actionLoader = false;
    }
    else {
      $timeout(function(){
        if(!logged) {
          $state.go('login', {url: 'messageView', token: $stateParams.usr_token_id});
          $rootScope.actionLoader = false;
        }
      });
    }

     //read message
    function readMessage() {
      var params = {
        usr_token_id: $stateParams.usr_token_id
      };
      
      MessageService.readMessage(params).then(function(res) {
      });
    }
    readMessage();

    function scrollDown() {
      // call $anchorScroll()
      var threadDiv = document.getElementById('threadScroll');
      threadDiv.scrollTop = threadDiv.scrollHeight;
    }
    if($stateParams.usr_token_id) {
      $rootScope.actionLoader = true;
      MemberService.viewProfile($stateParams.usr_token_id).then(function(res) {
        var loading = false;
        $rootScope.actionLoader = false;
        if(res.success) {
          $scope.other_user = res.data[0];
          $scope.other_user.info = res.data[1][0];
          $scope.other_user.preference = res.data[2][0];
          $scope.other_user.images = res.data[3];
          if(!res.data[4].message) {
            $scope.other_user.posts = res.data[4];
          }
          $scope.other_user.faved = res.data[5];
          $scope.other_user.followed = res.data[6];
          $scope.selected = _.findWhere(res.data[3], {img_id: res.data[0].img_id});
          $scope.other_user.birthday = new Date($scope.other_user.usr_birthyear + '-' + $scope.other_user.usr_birthmonth + '-' + $scope.other_user.usr_birthdate);
          $scope.age = moment().diff($scope.other_user.birthday, 'years');
          // socket 
          var usr_email = $scope.user.usr_email;
          var usr_id = $scope.user.usr_id;
          var password = $scope.user.socket_key;
          var second_user = $scope.other_user.usr_id;
          mySocket.emit('authenticate', usr_email + ':' + usr_id + ':' + password + ':' + second_user);
        }
        else {
          $scope.error = true;
        }
      });
    }
    mySocket.on('authenticate', function(auth) {
      // console.log(auth);
    });
    mySocket.on('chat message', function(msg) {
      readMessage();
      var message = {
        usr_id: msg.usr_id,
        msg_user: msg.msg_user,
        msg_content: msg.msg_content,
        msg_date_created: msg.msg_date_created
      }
      var last = _.last($scope.thread);
      if(msg.msg_user == $scope.user.usr_id || msg.usr_id == $scope.user.usr_id) {
          $scope.thread.push(message);
          $timeout(function() {
            scrollDown();
          }, 100);
      }
     
    });
   
    var params = {
      link: URL.API + 'messages/view?is_infinite=true&page=1&page_size=20',
      params: {usr_token_id: $stateParams.usr_token_id}
    };
    MessageService.getThread(params).then(function(res) {
      if(res.success) {
        $scope.thread = res.data[0];
        $timeout(function() {
          scrollDown();
        }, 100);
        if(res.pagination) {
          if(res.pagination.num_rows > $scope.thread.length) {
            $scope.next = res.pagination.links.next;
          }
          else {
            $scope.next = false;
          }
        }
      }
      else {
        $scope.thread = [];
      }
    });

    $scope.loadMore = function(next) {
      $scope.loadingMore = true;
      $scope.next = false;
      var params = {
        link: next,
        params: {usr_token_id: $stateParams.usr_token_id}
      };
      MessageService.getThread(params).then(function(res) {
        if(res.success) {
          for(var x = 0; x < res.data[0].length; x++) {
            $scope.thread.unshift(res.data[0][x]);
          }
          if(res.pagination) {
            if(res.pagination.num_rows > $scope.thread.length) {
              $scope.next = res.pagination.links.next;
            }
            else {
              $scope.next = false;
            }
          }
          $scope.loadingMore = false;
        }
        else {
          $scope.loadingMore = false;
        }
      }, function(err) {
        $scope.next = false;
        $scope.loadingMore = false;
      });
    };

    $scope.sendMessage = function(msg) {
      var message = msg;
      $scope.msg_content = '';
      if(message) {
        var params = {
          msg_user: $scope.other_user.usr_id,
          msg_content: message
        }

        var displayMsg = {
          msg_content: message,
          msg_date_created: moment().utc().format(),
          usr_id: $scope.user.usr_id,
          msg_user: $scope.other_user.usr_id
        }
        mySocket.emit('chat message', displayMsg);
        $scope.thread.push(displayMsg);
        $timeout(function() {
          scrollDown();
        }, 100);
        MessageService.sendMessage(params).then(function(res) {
        });
      }
    };

    $scope.sendOnEnter = function(msg, evt) {
      if(evt.keyCode == 13) {
        $scope.sendMessage(msg);
      }
    };

    $scope.deleteMessage = function() {

      var confirm = $mdDialog.confirm()
        .title('Confirm Delete')
        .textContent('Are you sure you want to delete this entire thread? Both you and the recipient will no longer see your message thread.')
        .ariaLabel('Confirm Delete')
        .targetEvent()
        .ok('OK')
        .cancel('Cancel');

      var params = {
        usr_token_id: $stateParams.usr_token_id
      };
      $mdDialog.show(confirm).then(function() {
        MessageService.deleteMessage(params).then(function(res) {
          if(res.success) {
            toastr.success('Message Thread Deleted');
            $timeout(function() {
              $state.go('messages');
            }, 500);
          }
        });
      }, function() {
      });

      
    };
  }
})();
