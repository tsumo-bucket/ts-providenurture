(function() {
  'use strict';

  angular
    .module('sugar')
    .service('MessageService', MessageService);

  function MessageService($http, $q, $log, URL, userData, $rootScope) {

    this.getConversations = function(data) {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: data.link,
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        params: data.params
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getThread = function(data) {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: data.link,
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        params: data.params
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.sendMessage = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'messages/send',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.deleteMessage = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'messages/update',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.readMessage = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'messages/read',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getCount = function() {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: URL.API + 'messages/unread',
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

  }

})();
