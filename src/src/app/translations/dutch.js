/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('sugar')
    .constant('langDe', {
        HOME_TITLE: 'DUTCH: Simple and Free Sugar Dating',
        HOME_SUB_TITLE: 'DUTCH: Where successful and beautiful people meet and mingle.'
    })

})();
