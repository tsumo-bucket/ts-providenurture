(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('FavoritesController', FavoritesController);

  /** @ngInject */
  function FavoritesController($timeout, webDevTec, URL, $rootScope, toastr, $log, logged, userData, $scope, FeedService, MemberService, FilterFactory, $mdDialog, $mdToast, $state) {
    if(logged) {
      // $rootScope.actionLoader = false;
    }
    else {
      // $window.location.reload();
      $timeout(function(){
        if(!userData.isLogged()) {
          $state.go('home');
          $rootScope.actionLoader = false;
        }
      }, 100);
    }
    $scope.sortParam = '-usl_date_modified';
    $scope.source = 'i-faved';
    $scope.loading = true;
    var params = URL.API + 'user_likes/list?is_infinite=true&page=1&page_size=20';
    FeedService.getMyFaves(params).then(function(res){
      if(res.success) {
        $rootScope.myfaves = res.data[0];
        $scope.loading = false;
        if(res.pagination.num_rows > $scope.myfaves.length) {
          $scope.nextFaves = res.pagination.links.next
        }
        else {
          $scope.nextFaves = false;
        }
      }
      else {
          $scope.message = 'Nothing to display.';
        $scope.loading = false;
      }
    });
    $scope.onTabSelected = function(tab) {
      $scope.message = '';
      $scope.source = tab;
      $scope.loading = true;
      $scope.nextFaves = false;
      $scope.nextFavedMe = false;
      $scope.nextViewed = false;
      $scope.nextFollowing = false;
      if(tab == 'i-faved') {
        var params = URL.API + 'user_likes/list?is_infinite=true&page=1&page_size=20';
        FeedService.getMyFaves(params).then(function(res){
          if(res.success) {
            $rootScope.myfaves = res.data[0];
            $scope.loading = false;
            if(res.pagination.num_rows > $rootScope.myfaves.length) {
              $scope.nextFaves = res.pagination.links.next
            }
            else {
              $scope.nextFaves = false;
            }
          }
          else {
            $scope.message = 'Nothing to display.';
            $scope.loading = false;
          }
        });
      }
      else if(tab == 'faved-me') {
        var params = URL.API + 'user_likes/list?is_infinite=true&page=1&page_size=20';
        FeedService.getFavedMe(params).then(function(res){
          if(res.success) {
            $scope.favedme = res.data[0];
            $scope.loading = false;
            if(res.pagination.num_rows > $scope.favedme.length) {
              $scope.nextFavedMe = res.pagination.links.next
            }
            else {
              $scope.nextFavedMe = false;
            }
          }
          else {
            $scope.message = 'Nothing to display.';
            $scope.loading = false;
          }
        });
      }
      else if(tab == 'viewed-me') {
        var params = URL.API + 'views/list?is_infinite=true&page=1&page_size=20';
        FeedService.getViews(params).then(function(res){
          if(res.success) {
            $scope.views = res.data[0];
            $scope.loading = false;
            if(res.pagination.num_rows > $scope.views.length) {
              $scope.nextViewed = res.pagination.links.next
            }
            else {
              $scope.nextViewed = false;
            }
          }
          else {
            $scope.message = 'Nothing to display.';
            $scope.loading = false;
          }
        });
      }
      else if(tab == 'following') {
        var params = URL.API + 'follows/list?is_infinite=true&page=1&page_size=20';
        FeedService.getFollowing(params).then(function(res){
          if(res.success) {
            $scope.following = res.data[0];
            $scope.loading = false;
            if(res.pagination.num_rows > $scope.following.length) {
              $scope.nextFollowing = res.pagination.links.next
            }
            else {
              $scope.nextFollowing = false;
            }
          }
          else {
            $scope.message = 'Nothing to display.';
            $scope.loading = false;
          }
        });

      }
    }

    $scope.loadMyFaves = function(next) {
      $scope.moreFaves = true;
      $scope.nextFaves = false;
      FeedService.getMyFaves(next).then(function(res){
        console.log(res);
        if(res.success){
          for(var x = 0; x < res.data[0].length; x++)
          {
            $scope.myfaves.push(res.data[0][x]);
          }
          $scope.loading = false;
          if(res.pagination.num_rows > $scope.myfaves.length) {
            $scope.nextFaves = res.pagination.links.next
          }
          else {
            $scope.nextFaves = false;
          }
          $scope.moreFaves = false;
        } 
        else {
          $scope.loading = false;
          $scope.message = 'Nothing to display.';
          $scope.moreFaves = false;
        }
      });
    };

    $scope.loadFavedMe = function(next) {
      $scope.moreFavedMe = true;
      $scope.nextFavedMe = false;
      FeedService.getFavedMe(next).then(function(res){
        console.log(res);
        if(res.success){
          for(var x = 0; x < res.data[0].length; x++)
          {
            $scope.favedme.push(res.data[0][x]);
          }
          $scope.loading = false;
          if(res.pagination.num_rows > $scope.favedme.length) {
            $scope.nextFavedMe = res.pagination.links.next
          }
          else {
            $scope.nextFavedMe = false;
          }
          $scope.moreFavedMe = false;
        } 
        else {
          $scope.loading = false;
          $scope.message = 'Nothing to display.';
          $scope.moreFavedMe = false;
        }
      });
    };

    $scope.loadViewedMe = function(next) {
      $scope.moreViews = true;
      $scope.nextViewed = false;
      FeedService.getViews(next).then(function(res){
        if(res.success){
          for(var x = 0; x < res.data[0].length; x++)
          {
            $scope.views.push(res.data[0][x]);
          }
          $scope.loading = false;
          if(res.pagination.num_rows > $scope.views.length) {
            $scope.nextViewed = res.pagination.links.next
          }
          else {
            $scope.nextViewed = false;
          }
          $scope.moreViews = false;
        } 
        else {
          $scope.loading = false;
          $scope.message = 'Nothing to display.';
          $scope.moreViews = false;
        }
      });
    };

    $scope.loadFollowing = function(next) {
      $scope.moreFollowing = true;
      $scope.nextFollowing = false;
      FeedService.geFollowing(next).then(function(res){
        console.log(res);
        if(res.success){
          for(var x = 0; x < res.data[0].length; x++)
          {
            $scope.following.push(res.data[0][x]);
            console.log(res.data[0][x]);
          }
          $scope.loading = false;
          console.log(res.pagination.num_rows);
          console.log($scope.following.length);
          if(res.pagination.num_rows > $scope.following.length) {
            $scope.nextFollowing = res.pagination.links.next
          }
          else {
            $scope.nextFollowing = false;
          }
          $scope.moreFollowing = false;
        } 
        else {
          $scope.loading = false;
          $scope.message = 'Nothing to display.';
          $scope.moreFollowing = false;
        }
      });
    };

    $scope.unfavorite = function(user) {
      var params = {
        usl_id: user.faved,
        usl_user: user.usl_user
      };

      var ind = _.findIndex($scope.myfaves, {usl_id: user.usl_id}) ;

      MemberService.favorite(params).then(function(res) {
        if(res.success) {
          $scope.myfaves.splice(ind, 1);
        }       
      });
    };

    $scope.favorite = function(user) {
      console.log(user);
      if(user.faved) {
        var params = {
          usl_id: user.faved,
          usl_user: user.usr_id
        };
      }
      else {
        var params = {
          usl_user: user.usr_id
        };
      }

      MemberService.favorite(params).then(function(res) {
        $log.debug(res);
        if(res.data[0].usl_status=='active') {
          $scope.user.faved = res.data[0].usl_id;
        }
        else {
          $scope.user.faved = false;
        }
      });
    };

  }
})();
