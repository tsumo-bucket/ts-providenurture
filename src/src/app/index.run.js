(function() {
  'use strict';

  angular
    .module('sugar')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, $rootScope, toastr, $timeout, $sce, userData, $mdDialog, ngMeta, MemberService, $state, $document, $window, URL, FilterFactory) {
  	//transfers sessionStorage from one tab to another

    ngMeta.init();
    var sessionStorage_transfer = function(event) {
      if(!event) { event = $window.event; } // ie suq
      if(!event.newValue) return;          // do nothing if no value to work with
      if (event.key == 'getSessionStorage') {
        // another tab asked for the sessionStorage -> send it
        $window.localStorage.setItem('sessionStorage', JSON.stringify(sessionStorage));
        // the other tab should now have it, so we're done with it.
        $window.localStorage.removeItem('sessionStorage'); // <- could do short timeout as well.
      } else if (event.key == 'sessionStorage' && !$window.sessionStorage.length) {
        // another tab sent data <- get it
        var data = JSON.parse(event.newValue);
        for (var key in data) {
          $window.sessionStorage.setItem(key, data[key]);
        }
      }
    };
    
    if (navigator.geolocation) {
      $rootScope.locationSupport = true;
    }
    else {
      $rootScope.locationSupport = false;
    }
    
    $rootScope.trustHtml = function(text) {
      return $sce.trustAsHtml(text);
    };
    $rootScope.$on('privateBrowsing', function(events,args) {
      // console.log(args);
    });

    // listen for changes to $window.localStorage
    if($window.addEventListener) {
      $window.addEventListener("storage", sessionStorage_transfer, false);
    } else {
      $window.attachEvent("onstorage", sessionStorage_transfer);
      $window.location.reload();
    };


    // Ask other tabs for session storage (this is ONLY to trigger event)
    if (!sessionStorage.length) {
      try {
        $window.localStorage.setItem('getSessionStorage', 'foobar');
        $window.localStorage.removeItem('getSessionStorage', 'foobar');
      }
      catch(err) {
        function showAlert() {
          alert = $mdDialog.alert({
            title: '',
            textContent: 'ProvideNurture needs a non-private browser to work on iOS. Please switch to normal browsing for better user experience. Thank you!',
            ok: ''
          });

          $mdDialog
            .show( alert )
            .finally(function() {
              });
            $timeout(function() {
              $('.md-default-theme').addClass('browser-alert');
            });
        }
        showAlert();
      }
    };
    
    if(userData.isLogged()) {
      $rootScope.user = JSON.parse(userData.isLogged());
      if($rootScope.user.usr_status == 'pending' && $rootScope.user.usr_signup_step == 'done') {
        $rootScope.hide = true;
        $state.go('search');
      }
    }
    $rootScope.filter = FilterFactory;
    
    $rootScope.displayValue = function(x, list) {
      var param = {value: x};
      var value = _.findWhere(list, param);
      if(value) {
        return value.name;
      }
      else {
        return '';
      }
    };

    $rootScope.trustSrc = function(data){
      return $sce.trustAsHtml(data);
    };

    $rootScope.dateFormat = function(date) {
      return moment(date).format('LL');
    };

    $rootScope.shortDate = function(date) {
      return moment(date).format('ll');
    };

    $rootScope.relDateFormat = function(date) {
      if(date) {
        var tz = moment.tz.guess();
        var timezone = moment(date, 'YYYY-MM-DD HH:mm:ss').utc(tz).format();
        var formatted = moment(timezone).fromNow();
        return formatted;
      }
    };

    $rootScope.$on('token changed', function(events, args) {
      toastr.warning('You\'ve been logged out!');
    });
   
    $rootScope.$on('home', function(events, args) {
      $rootScope.isHome = args;
      if(!$rootScope.isHome) {
        $('.navbar').addClass('transluscent');
      }
    });

    $rootScope.baseUrl = function(res) {
      return URL.BASE + res;
    };

    $rootScope.s3Url = function(res) {
      return URL.S3_URL + res;
    };
      
    $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
      if($state.current.name == "account.success") {
        $rootScope.end = true;
      }
      else if($state.current.name == "account.preferences") {
        $rootScope.preferences = true;
      }
      else if($state.current.name == "account.personal") {
        $rootScope.info = true;
      }
      else if($state.current.name == "account.addPhotos") {
        $rootScope.addPhotos = true;
      }
      else {
        $rootScope.addPhotos = false;
        $rootScope.info = false;
        $rootScope.preferences = false;
        $rootScope.end = false;
      }
      
      if(toState.name != "home") {
        // console.log('not home');
        $rootScope.isHome = false;
        $rootScope.$broadcast('home', $rootScope.isHome);
      }
      var isLogged = userData.isLogged();
      if(isLogged) {
        var params = {
          usr_email:  userData.getUserData('usr_email')
        }
        MemberService.getToken(params).then(function(res) {
          if(userData.isLogged()) {
            if(res.data[0].usr_token != userData.getUserData('usr_token')) {
              userData.destroy();
              $state.go('home');
              $rootScope.user = false;
              $rootScope.$broadcast('token changed', $rootScope.user);
              delete $rootScope.user;
              delete $rootScope.hide;
            }
          }
        });
      }

    });

  }

})();
