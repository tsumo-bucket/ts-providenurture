(function() {
  'use strict';

  angular
    .module('sugar')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider, ngMetaProvider, modulePrefix) {

    // added karlob - for modular views
    // check index.constants.js for prefix configurations
    var getModuleView = function(url) {
        if (modulePrefix) {
          var http = new XMLHttpRequest();
          var modUrl = url.replace('.html', '') + modulePrefix + '.html';
          http.open('HEAD', modUrl, false);
          http.send();
          return (http.status !== 404) ? modUrl : url;
        }
        return url;
    };

    $stateProvider
      .state('home', {
        cache: false,
        url: '/',
        data: {
          meta: {
            'title': 'Respectable Sugar Dating for Sugar Babies and Sugar Daddies',
            'titleSuffix': ' | ProvideNurture Arrangements',
            'description': 'Join a respectable free sugar dating site focused on longer term tranditional relationships. Enjoy descreet encounters our sugar babes and sugar daddies.',
            'keyword': 'Sugar Dating for Sugar daddies and Sugar Babies'
          }
        },
        templateUrl: getModuleView('app/main/main.html'),
        controller: 'MainController',
        controllerAs: 'main',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        }
      })

      .state('reset', {
        url: '/reset/:usr_password_reset',
        cache: false,
        params: {usr_password_reset: null},
        templateUrl: getModuleView('app/main/main.html'),
        controller: 'ResetController',
        controllerAs: 'reset'
      })

      .state('login', {
        cache: false,
        url: '/login/:url/:token',
        params: {url: null, token: null},
        templateUrl: getModuleView('app/main/login-page.html'),
        controller: 'LoginController',
        controllerAs: 'login',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('join', {
        cache: false,
        url: '/signup',
        templateUrl: getModuleView('app/signup/signup.html'),
        controller: 'InitialController',
        controllerAs: 'initial',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture Signup',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('join2', {
        cache: false,
        url: '/signup/type',
        templateUrl: getModuleView('app/signup/signup-2.html'),
        controller: 'InitialController',
        controllerAs: 'initial',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture Signup',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('join3', {
        cache: false,
        url: '/signup/info',
        templateUrl: getModuleView('app/signup/signup-3.html'),
        controller: 'InitialController',
        controllerAs: 'initial',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture Signup',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('account', {
        cache: false,
        url: '/account',
        templateUrl: getModuleView('app/signup/userdata-setup.html'),
        controller: 'SignupController',
        controllerAs: 'signup',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('account.addPhotos', {
        cache: false,
        url: '/add-photos',
        templateUrl: getModuleView('app/signup/userdata-1.html'),
        controller: 'PhotosController',
        controllerAs: 'photos',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('account.personal', {
        cache: false,
        url: '/information',
        templateUrl: getModuleView('app/signup/userdata-2.html'),
        controller: 'SignupController',
        controllerAs: 'signup',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('account.preferences', {
        cache: false,
        url: '/preferences',
        templateUrl: getModuleView('app/signup/userdata-3.html'),
        controller: 'SignupController',
        controllerAs: 'signup',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('account.success', {
        cache: false,
        url: '/success',
        templateUrl: getModuleView('app/signup/userdata-4.html'),
        controller: 'SignupController',
        controllerAs: 'signup',
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('verify', {
        cache: false,
        url: '/verify/:usr_token',
        params: { usr_token: null },
        templateUrl: getModuleView('app/signup/verify.html'),
        controller: 'VerifyController',
        controllerAs: 'verify',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('dashboard', {
        cache: false,
        url: '/newsfeed',
        templateUrl: getModuleView('app/dashboard/home.html'),
        controller: 'NewsfeedController',
        controllerAs: 'newsfeed',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('dashboard.faved', {
        cache: false,
        url: '/i-faved',
        templateUrl: getModuleView('app/dashboard/home.html'),
        controller: 'NewsfeedController',
        controllerAs: 'newsfeed',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('dashboard.followed', {
        cache: false,
        url: '/i-followed',
        templateUrl: getModuleView('app/dashboard/home.html'),
        controller: 'NewsfeedController',
        controllerAs: 'newsfeed',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('dashboard.messaged', {
        cache: false,
        url: '/i-messaged',
        templateUrl: getModuleView('app/dashboard/home.html'),
        controller: 'NewsfeedController',
        controllerAs: 'newsfeed',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('members', {
        cache: false,
        url: '/members',
        templateUrl: getModuleView('app/members/members.html'),
        controller: 'DashboardController',
        controllerAs: 'dashboard',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('favorites', {
        cache: false,
        url: '/favorites',
        templateUrl: getModuleView('app/favorites/favorites.html'),
        controller: 'FavoritesController',
        controllerAs: 'favorites',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('search', {
        cache: false,
        url: '/search',
        templateUrl: getModuleView('app/search/search.html'),
        controller: 'SearchController',
        controllerAs: 'search',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('messages', {
        cache: false,
        url: '/messages',
        templateUrl: getModuleView('app/messages/index.html'),
        controller: 'MessagesController',
        controllerAs: 'messages',

        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('messageView', {
        cache: false,
        url: '/message/:usr_token_id',
        params: { usr_token_id: null },
        templateUrl: getModuleView('app/messages/view.html'),
        controller: 'MessageController',
        controllerAs: 'message',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('user', {
        cache: false,
        url: '/user/:usr_token_id',
        params: { usr_token_id: null },
        // templateUrl: getModuleView('app/main/error.html',
        templateUrl: getModuleView('app/profile/view.html'),
        controller: 'UserController',
        controllerAs: 'user',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('profile', {
        cache: false,
        url: '/profile',
        templateUrl: getModuleView('app/profile/profile.html'),
        controller: 'ProfileController',
        controllerAs: 'profile',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('profile.view', {
        cache: false,
        url: '/view',
        templateUrl: getModuleView('app/profile/view.html'),
        controller: 'ProfileController',
        controllerAs: 'profile',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('profile.edit', {
        cache: false,
        url: '/edit',
        templateUrl: getModuleView('app/profile/edit.html'),
        controller: 'ProfileController',
        controllerAs: 'profile',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('profile.photos', {
        cache: false,
        url: '/photos',
        templateUrl: getModuleView('app/signup/userdata-1.html'),
        controller: 'PhotosController',
        controllerAs: 'photos',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('settings', {
        cache: false,
        url: '/settings',
        templateUrl: getModuleView('app/account/settings.html'),
        controller: 'PreferenceController',
        controllerAs: 'preference',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('settings.preferences', {
        cache: false,
        url: '/preferences',
        templateUrl: getModuleView('app/account/settings-preferences.html'),
        controller: 'PreferenceController',
        controllerAs: 'preference',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('settings.account', {
        cache: false,
        url: '/account',
        templateUrl: getModuleView('app/account/settings-account.html'),
        controller: 'AccountController',
        controllerAs: 'account',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('settings.notification', {
        cache: false,
        url: '/notification',
        templateUrl: getModuleView('app/account/settings-notification.html'),
        controller: 'NotificationController',
        controllerAs: 'notification',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('settings.privacy', {
        cache: false,
        url: '/privacy',
        templateUrl: getModuleView('app/account/settings-privacy.html'),
        controller: 'PrivacyController',
        controllerAs: 'privacy',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'Sugar Dating Arrangements | Privacy Policy | ProvideNurture',
            'description': 'We take privacy very seriously. ProvideNurture is where wealthy people date attractive men and women in a discreet manner.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('settings.billing', {
        cache: false,
        url: '/subscription',
        templateUrl: getModuleView('app/account/settings-billing.html'),
        controller: 'BillingController',
        controllerAs: 'billing',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('delete', {
        cache: false,
        url: '/account/delete',
        templateUrl: getModuleView('app/account/delete.html'),
        controller: 'AccountController',
        controllerAs: 'account',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('deactivate', {
        cache: false,
        url: '/account/deactivate',
        templateUrl: getModuleView('app/account/deactivate.html'),
        controller: 'AccountController',
        controllerAs: 'account',
        resolve: {
          logged: function(userData, $state, $rootScope, $timeout) {
            var defer = angular.fromJson(userData.isLogged());
            $rootScope.user = defer;
            $rootScope.$broadcast('login', $rootScope.user);
            return defer;
          }
        },
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | Simple and Free Sugar Dating and Online Dating',
            'description': 'Sugar Dating for Sugar Daddies, Sugar Mamas and Sugar Babies. Find Your Sugar Arrangement.',
            'keyword': 'Sugar Dating, Online Dating'
          }
        }
      })

      .state('privacy', {
        cache: false,
        url: '/private-and-discreet',
        data: {
          meta: {
            'title': 'Want a private dating or discreet dating relationship?',
            'titleSuffix': ' | Privacy Policy | ProvideNurture',
            'description': 'ProvideNurture offers private dating and discreet dating encounters allowing you to define your rules for the relationship you want. ',
            'keyword': 'discreet dating, private dating'
          }
        },
        templateUrl: getModuleView('app/miscellaneous/page.html'),
        controller: 'PageController',
        controllerAs: 'page'
      })

      .state('terms', {
        cache: false,
        url: '/terms-of-use',
        data: {
          meta: {
            'title': 'Terms of Use',
            'titleSuffix': ' | ProvideNurture',
            'description': 'By using the ProvideNurture dating website, you agree to be bound by our Terms of Use, whether or not you register as a member of ProvideNurture.com. ',
            'keyword': ''
          }
        },
        templateUrl: getModuleView('app/miscellaneous/page.html'),
        controller: 'PageController',
        controllerAs: 'page'
      })

      .state('support', {
        cache: false,
        url: '/help-center',
        data: {
          meta: {
            'title': 'Find out about sugar dating, sugar daddies & sugar babies.',
            'titleSuffix': ' | Help Center | ProvideNurture',
            'description': 'Find out all you can about sugar dating and how being a sugar daddy, sugar baby, or sugar mama might be the kind of relationship you are looking for.',
            'keyword': 'Sugar Dating, Sugar Babies, Sugar Daddies'
          }
        },
        templateUrl: getModuleView('app/miscellaneous/page.html'),
        controller: 'PageController',
        controllerAs: 'page',
      })

      .state('precautions', {
        cache: false,
        url: '/staying-safe',
        data: {
          meta: {
            'title': 'Find out how to stay safe while dating online. Safe Online Dating',
            'titleSuffix': ' | Tips on Staying Safe | ProvideNurture',
            'description': 'ProvideNurture is more than just about finding a long term sugar dating. It provides you a safe online dating experience in a discreet environment. ',
            'keyword': 'Safe Online Dating, long term relationships'
          }
        },
        templateUrl: getModuleView('app/miscellaneous/page.html'),
        controller: 'PageController',
        controllerAs: 'page'
      })

      .state('about', {
        cache: false,
        url: '/about-us',
        data: {
          meta: {
            'title': 'Sugar Dating for All | Interracial Dating and Asian Dating',
            'titleSuffix': ' | About Us | ProvideNurture',
            'description': 'Respectable sugar dating across all cultures. Interracial Dating and Asian Dating- We match all sorts of successful and beautiful people.',
            'keyword': 'Asian Dating, Interracial Dating'
          }
        },
        templateUrl: getModuleView('app/miscellaneous/about.html'),
        controller: 'PageController',
        controllerAs: 'page'
      })

      .state('find', {
        cache: false,
        url: '/find-your-match',
        data: {
          meta: {
            'title': 'Find Your PN Match | Your ideal seeking arrangement ad.',
            'titleSuffix': ' | Find your PN Match | ProvideNurture',
            'description': 'ProvideNurture is a community of sugar daddies and sugar babies who are seeking arrangement to find an ideal sugar match.',
            'keyword': 'Seeking Arrangement'
          }
        },
        templateUrl: getModuleView('app/miscellaneous/find.html'),
        controller: 'PageController',
        controllerAs: 'page'
      })

      .state('why', {
        cache: false,
        url: '/why-providenurture',
        data: {
          meta: {
            'title': 'ProvideNurture is more than a date hookup or hookups site.',
            'titleSuffix': ' | Marriage with No Contract | ProvideNurture',
            'description': 'ProvideNurture is more than just a hookup or date hookups site. We foster longer term relationships between men and women interested in long term sugaring.',
            'keyword': 'Hookups, Hookup, date hookup'
          }
        },
        templateUrl: getModuleView('app/miscellaneous/why.html'),
        controller: 'PageController',
        controllerAs: 'page'
      })

      .state('life', {
        cache: false,
        url: '/life',
        templateUrl: getModuleView('app/miscellaneous/life.html'),
        controller: 'PageController',
        controllerAs: 'page'
      })

      .state('life.daddy', {
        cache: false,
        url: '/sugar-daddy',
        data: {
          meta: {
            'title': 'ProvideNurture',
            'titleSuffix': ' | What Is A Sugar Daddy? He\'s your ideal man.',
            'description': 'ProvideNurture provides unique opportunities for a sugar daddy to meet his sugar baby. Sugar Daddy Dating may be the ideal relationship for older men.',
            'keyword': 'Sugar Daddy, Sugar Daddy Dating'
          }
        },
        templateUrl: getModuleView('app/miscellaneous/life-daddy.html'),
        controller: 'PageController',
        controllerAs: 'page'
      })

      .state('life.mama', {
        cache: false,
        url: '/nurturer',
        data: {
          meta: {
            'title': 'What\'s a Nurturer? She\'s a kind and secure Sugar Baby.',
            'titleSuffix': ' | The Life of a Nurturer | ProvideNurture',
            'description': 'ProvideNurture enables sugar babies to find a long term sugar daddy who can take care and support their financial needs - Lesbian dating included.',
            'keyword': 'Sugar Baby, Lesbian dating'
          }
        },
        templateUrl: getModuleView('app/miscellaneous/life-mama.html'),
        controller: 'PageController',
        controllerAs: 'page'
      })

      .state('life.femalebaby', {
        cache: false,
        url: '/female-sugar-baby',
        data: {
          meta: {
            'title': 'What\'s a ProvideNurture Sugar Baby? She\'s your ideal girlfriend.',
            'description': ' ProvideNurture empowers sugar babies to set their rules and find a sugar daddy who can take care and support their financial needs. Lesbian dating included.',
            'keyword': 'Sugar Baby, Lesbian dating'
          }
        },
        templateUrl: getModuleView('app/miscellaneous/life-baby.html'),
        controller: 'PageController',
        controllerAs: 'page',
      })

      .state('life.malebaby', {
        cache: false,
        url: '/provider',
        data: {
          meta: {
            'title': 'What Is Provider? He\'s a long term Sugar Daddy.',
            'titleSuffix': ' | The Life of a Provider | ProvideNurture',
            'description': 'ProvideNurture provides a venue for a sugar daddy to meet his ideal sugar baby in a free and secure sugar daddy dating website.',
            'keyword': 'Sugar Daddy, Sugar Daddy Dating'
          }
        },
        templateUrl: getModuleView('app/miscellaneous/life-baby-male.html'),
        controller: 'PageController',
        controllerAs: 'page',
      })

      .state('contact', {
        cache: false,
        url: '/contact-us',
        templateUrl: getModuleView('app/account/contact.html'),
        controller: 'PageController',
        controllerAs: 'page',

        data: {
          meta: {
            'title': '',
            'description': '',
            'keyword': ''
          }
        }
      })
      ;

    $urlRouterProvider.otherwise('/');
    if(window.history && window.history.pushState){
      $locationProvider.html5Mode(true);
      $locationProvider.hashPrefix('!');
    }
  }

})();
