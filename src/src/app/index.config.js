(function() {
  'use strict';

  angular
    .module('sugar')
    .config(config);

  /** @ngInject */
function config($logProvider, toastrConfig, ngMetaProvider, $httpProvider,
                    $locationProvider,
                    //PUT TRANSLATIONS HERE
                    langEn,
                    langDe,
                    langEnPn,
                    langDePn,
                    //end TRANSLATIONS
                    $translateProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.showMethod = 'slideDown';
    toastrConfig.hideMethod = 'slideUp';toastrConfig.escapeHtml = true;
    // toastrConfig.preventDuplicates = true;


    //meta config
    //Add a suffix to all page titles
    ngMetaProvider.useTitleSuffix(true);

    // On /home, the title would change to
    // 'Home Page | Best Website on the Internet!'
    //ngMetaProvider.setDefaultTitleSuffix('');

    // $locationProvider.html5Mode({
    //   enabled: true,
    //   requireBase: false
    // });

    //do this for every translation constant
    langEn = Object.assign(langEn, langEnPn);
    langDe = Object.assign(langDe, langDePn);
    $translateProvider.translations('en', langEn);
    $translateProvider.translations('de', langDe);
    $translateProvider.preferredLanguage('en');

    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

  } 

})();