(function() {
  'use strict';

  angular
    .module('sugar', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize',
    	'ngMessages', 'ngAria', 'ui.router', 'ui.bootstrap', 'ngMaterial',
    	'toastr', 'slick','rzModule', 'ngFileUpload', 'uiCropper',
    	'btford.socket-io', 'vsGoogleAutocomplete', 'ngMeta',
      'pascalprecht.translate', 'ngWYSIWYG', 'AngularReverseGeocode']);

})();
