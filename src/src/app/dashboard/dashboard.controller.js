(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('DashboardController', DashboardController);

  /** @ngInject */
  function DashboardController(webDevTec, $timeout, userData, URL, $window, toastr, reverseGeocode, $log, $scope, $rootScope, $uibModal, SettingsService, FeedService, MemberService, FilterFactory, $mdDialog, $state) {
    $scope.filter = FilterFactory;
    $scope.filter.gender = [
      {checked: false, name: 'Men', value: 'male'},
      {checked: false, name: 'Women', value: 'female'}
    ];
    $scope.filter.country = [];
    SettingsService.getCountries().then(function(res){
      if(res.success) {
        $scope.filter.country = res.data[0];
      }
    });

    $scope.loading = true;
    $scope.user = $rootScope.user;

    $scope.filter.distance_from = 0;
    $scope.filter.distance_to = 10000;

    $scope.ageRangeSlider = {
      options: {
        floor: 18,
        ceil: 80,
        step: 1,
        minRange: 1
      }
    };

    $scope.distanceRangeSlider = {
      options: {
        floor: 0,
        ceil: 10000,
        step: 1,
        minRange: 1
      }
    };
    $scope.next = false;
    $('.navbar').addClass('transluscent');
    $scope.source = '';
    var getUsers = function(params) {
      FeedService.getUsers(params).then(function(res){
        if(res.success){
          $scope.users = res.data[0];
          $scope.loading = false;
          if(res.pagination) {
            if(res.pagination.num_rows > $scope.users.length) {
              $scope.next = res.pagination.links.next
            }
            else {
              $scope.next = false;
            }
          }
        } 
        else {
          $scope.users = [];
          $scope.loading = false;
          $scope.message = 'Nothing to display.';
        }
      });
    };

    function scrollDown() {
      // call $anchorScroll()
      var feedDiv = document.getElementById('feedScroll');
      feedDiv.scrollTop = feedDiv.scrollHeight;
    };

    $rootScope.user = angular.fromJson(userData.isLogged());
    if(userData.isLogged()) {
      if($rootScope.user.usr_status == 'approved'){
        $scope.user = angular.fromJson(userData.isLogged());
        $rootScope.$broadcast('login', $scope.user);
        $rootScope.hide = false;
        SettingsService.getInterest().then(function(res) {
          if(res.success) {
            $scope.user.usr_interested = res.data[0].usr_interested;
            $scope.user.usr_interested_age_from = parseInt(res.data[0].usr_interested_age_from);
            $scope.user.usr_interested_age_to = parseInt(res.data[0].usr_interested_age_to);
            $scope.user.usr_interested_country = res.data[0].usr_interested_country;
            // get value
            if(res.data[0].usr_interested == 'both')
            {
              $scope.filter.gender = [
                {checked: true, name: 'Man', value: 'male'},
                {checked: true, name: 'Woman', value: 'female'}
              ];
            }
            else {
              var gender = _.where($scope.filter.gender, {value: $scope.user.usr_interested})
                          .map(function(item) { 
                            item.checked = true; return item; }); 
              var reject = _.reject($scope.filter.gender, {value: gender[0].value});
              reject.push(gender[0]);
              $scope.filter.gender = reject;
            }
          }
        });
        MemberService.getUser().then(function(res) {
          if(res.success) {
            $scope.user = res.data[0];
              var params = {
                link: URL.API + 'users/list?is_infinite=true&page=1&page_size=20',
                params: {
                  order_by: 'usr_approved_date',
                  order: 'desc',
                  source: $scope.source,
                  distance_from: $scope.filter.distance_from, 
                  distance_to: $scope.filter.distance_to, 
                  lat: $scope.user.ulc_lat, 
                  long: $scope.user.ulc_lng
                }
              };
              if($scope.source == '') {
                params.params.order_by = 'usr_approved_date';
                params.params.order = 'desc';
              }
              else {
                params.params.order_by = 'usr_last_login';
                params.params.order = 'desc';
              }
              getUsers(params);
          }
        });
      }
      else
      {
        if($rootScope.user.usr_signup_step == 'initial') {
          $state.go('account.addPhotos');
          $rootScope.hide = true;
        }
        else if($rootScope.user.usr_signup_step == 'photo') {
          $state.go('account.addPhotos');
          $rootScope.hide = true;
        }
        else if($rootScope.user.usr_signup_step == 'info') {
          $state.go('account.personal');
          $rootScope.hide = true;
        }
        else if($rootScope.user.usr_signup_step == 'preference') {
          $state.go('account.preferences');
          $rootScope.hide = true;
        }
        else if($rootScope.user.usr_signup_step == 'done') {
          $state.go('search');
          $rootScope.hide = true;
        }
      }
    }
    else {
      // $window.location.reload();
      $timeout(function(){
        if(!userData.isLogged()) {
          $state.go('home');
          $rootScope.actionLoader = false;
        }
        else {
          SettingsService.getInterest().then(function(res) {
            if(res.success) {
              $scope.user.usr_interested = res.data[0].usr_interested;
              $scope.user.usr_interested_age_from = parseInt(res.data[0].usr_interested_age_from);
              $scope.user.usr_interested_age_to = parseInt(res.data[0].usr_interested_age_to);
              $scope.user.usr_interested_country = res.data[0].usr_interested_country;
              
              // get value
              if(res.data[0].usr_interested == 'both')
              {
                $scope.filter.gender = [
                  {checked: true, name: 'Men', value: 'male'},
                  {checked: true, name: 'Women', value: 'female'}
                ];
              }
              else {
                var gender = _.where($scope.filter.gender, {value: $scope.user.usr_interested})
                            .map(function(item) { 
                              item.checked = true; return item; }); 
                var reject = _.reject($scope.filter.gender, {value: gender[0].value});
                reject.push(gender[0]);
                $scope.filter.gender = reject;
              }
            }
          });
        }
      }, 100);
    }

    $rootScope.$on('rzSliderForceRender', function() {
      var max = angular.element(document.querySelector('.rzslider'));
      max.click();
    });

    $scope.openCriteria = function(ev, size, parentSelector) {
      $timeout(function () {
          $rootScope.$broadcast('rzSliderForceRender');
      }, 500);

      $mdDialog.show({
        controller: DashboardController,
        templateUrl: 'app/dashboard/criteria.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        locals: {
          filter: $scope.filter,
          gender: $scope.filter.gender,
          user: $scope.user,
          source: $scope.source,
          ageRangeSlider: $scope.ageRangeSlider,
          distanceRangeSlider: $scope.distanceRangeSlider
        },
        clickOutsideToClose:true,
        fullscreen: true // Only for -xs, -sm breakpoints.
      })
      .then(function(answer) {
        // console.log(answer);
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
      
    };
    $scope.$watch('components', function (newValue, oldValue, scope) {
      if(newValue) {
        $scope.user.ulc_lat = newValue.latitude;
        $scope.user.ulc_lng = newValue.longitude;
      }
    },true);

    $scope.resetDirective = function () {};
    function DashboardController($scope, $mdDialog, filter, user, source, ageRangeSlider, distanceRangeSlider, gender, reverseGeocode) {
      $scope.filter = filter;
      $scope.user = user;
      $scope.source = source;
      $scope.filter.gender = gender;
      $scope.ageRangeSlider = ageRangeSlider;
      $scope.distanceRangeSlider = distanceRangeSlider;

      $scope.distance = true;

      $scope.limitDistance = function(distance) {
        if(!distance) {
          $scope.filter.distance_to = 10001;
        } 
        else {
          $scope.filter.distance_to = 10000;
        }
      };
      $scope.original_loc = $scope.user.usr_location;

      var getLocationSuccess = function(position) {
        $scope.user.ulc_lat = position.coords.latitude;
        $scope.user.ulc_lng = position.coords.longitude;
        reverseGeocode.geocodePosition(position.coords.latitude, position.coords.longitude, function(address) {
          $scope.user.usr_location = address;
          $scope.$digest();
        });
      }

      var getLocationFail = function(position) {
        toastr.warning('User denied geolocation permission.');
        $scope.user.location = false;
      }

      $scope.original_lat = $scope.user.ulc_lat;
      $scope.original_lng = $scope.user.ulc_lng;

      $scope.getCurrentLocation = function(location) {
        $scope.location = location;
        if(location) {
          navigator.geolocation.getCurrentPosition(getLocationSuccess, getLocationFail);
        }
        else {
          $scope.user.ulc_lat = $scope.original_lat;
          $scope.user.ulc_lng = $scope.original_lng;
          $scope.user.usr_location = $scope.original_loc;
          $scope.user.location = false;
        }
      };

      $scope.$watch('components', function (newValue, oldValue, scope) {
        if(newValue) {
          $scope.user.ulc_lat = newValue.latitude;
          $scope.user.ulc_lng = newValue.longitude;
        }
      },true);
      
      $scope.applyCriteria = function(criteria, dist) {
        if($scope.criteriaForm.$invalid) {
          toastr.warning('Make sure to fill out the fields correctly');
        }
        else {

          if(criteria.usr_interested_age_from >= 18 && criteria.usr_interested_age_to <= 80) {
            if(criteria.usr_interested_age_to == 18) {
              toastr.warning('Make sure to fill out the fields correctly');
            }
            else {
              $scope.cancel();
              var gender;
              if(_.where($scope.filter.gender, {checked: true}).map(function(item) { return item.value; }).length == 2) {
                gender = 'both';
              }
              else {
                gender = _.findWhere($scope.filter.gender, {checked: true}).value;
              }
              var params = {
                usr_interested: gender,
                usr_interested_age_from: criteria.usr_interested_age_from,
                usr_interested_age_to: criteria.usr_interested_age_to,
                usr_interested_country: criteria.usr_interested_country
              };
              SettingsService.updatePreference(params).then(function(res) {
                if(res.success) {
                  // $window.location.reload();
                  $scope.users = [];
                  var params = {
                    link: URL.API + 'users/list?is_infinite=true&page=1&page_size=20',
                    params: {
                      source: $scope.source,
                      distance_from: dist.distance_from, 
                      distance_to: dist.distance_to, 
                      lat: $scope.user.ulc_lat, 
                      long: $scope.user.ulc_lng 
                    }
                  };
                  if($scope.source == '') {
                    params.params.order_by = 'usr_approved_date';
                    params.params.order = 'desc';
                  }
                  else if($scope.source == 'recent'){
                    params.params.order_by = 'usr_last_login';
                    params.params.order = 'desc';
                  }
                  else if($scope.source == 'facebook') {
                    params.params.order_by = 'usr_approved_date';
                    params.params.order = 'desc';
                  }
                  getUsers(params);
                }
                else {
                  toastr.error(res.error.message);
                  $scope.users = [];
                }
              });
            }
          }
          else {
            toastr.warning('Make sure to fill out the fields correctly');
          }
        }

      };
      
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
    }


    // $scope.applyCriteria = function(criteria, dist) {
    //   $scope.cancel();
    //   var gender;
    //   if(_.where($scope.filter.gender, {checked: true}).map(function(item) { return item.value; }).length == 2) {
    //     gender = 'both';
    //   }
    //   else {
    //     gender = _.findWhere($scope.filter.gender, {checked: true}).value;
    //   }
    //   var params = {
    //     usr_interested: gender,
    //     usr_interested_age_from: criteria.usr_interested_age_from,
    //     usr_interested_age_to: criteria.usr_interested_age_to,
    //     usr_interested_country: criteria.usr_interested_country
    //   };
    //   SettingsService.updatePreference(params).then(function(res) {
    //     if(res.success) {
    //       // $window.location.reload();
    //       $scope.users = [];
    //       var params = {
    //         link: URL.API + 'users/list?is_infinite=true&page=1&page_size=20',
    //         params: {
    //           source: $scope.source,
    //           distance_from: dist.distance_from, 
    //           distance_to: dist.distance_to, 
    //           lat: $scope.user.ulc_lat, 
    //           long: $scope.user.ulc_lng 
    //         }
    //       };
    //       if($scope.source == '') {
    //         params.params.order_by = 'usr_approved_date';
    //         params.params.order = 'desc';
    //       }
    //       else if($scope.source == 'recent'){
    //         params.params.order_by = 'usr_last_login';
    //         params.params.order = 'desc';
    //       }
    //       else if($scope.source == 'facebook') {
    //         params.params.order_by = 'usr_approved_date';
    //         params.params.order = 'desc';
    //       }
    //       getUsers(params);
    //     }
    //     else {
    //       toastr.error(res.error.message);
    //       $scope.users = [];
    //     }
    //   });

    // };
    
    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.onTabSelected = function(tab) {
      $scope.message = '';
      $scope.source = tab;
      $scope.loading = true;
      $scope.next = false;
      
      $scope.users = [];
      var params = {
        link: URL.API + 'users/list?is_infinite=true&page=1&page_size=20',
        params: {
          source: $scope.source,
          distance_from: $scope.filter.distance_from, 
          distance_to: $scope.filter.distance_to, 
          lat: $scope.user.ulc_lat, 
          long: $scope.user.ulc_lng 
        }
      };
      if($scope.source == '') {
        params.params.order_by = 'usr_approved_date';
        params.params.order = 'desc';
      }
      // else if($scope.source == 'recent'){
      //   params.params.order_by = 'usr_last_login';
      //   params.params.order = 'desc';
      // }
      else if($scope.source == 'facebook') {
        params.params.order_by = 'usr_approved_date';
        params.params.order = 'desc';
      }
      getUsers(params);
    };

    $scope.post = function(post) {
      var params = {
        pos_content: post
      }

      FeedService.createPost(params).then(function(res) {
        if(res.success) {
          toastr.success('Posted');
          $scope.post.pos_content = '';
        }
        else {
          toastr.error(res.error.message);
        }
      });
    };

    $scope.favorite = function(user) {
      if(user.faved) {
        var params = {
          usl_id: user.faved,
          usl_user: user.usr_id
        };
      }
      else {
        var params = {
          usl_user: user.usr_id
        };
      }

      MemberService.favorite(params).then(function(res) {
        $log.debug(res);
        if(res.data[0].usl_status=='active') {
          $scope.user.faved = res.data[0].usl_id;
        }
        else {
          $scope.user.faved = false;
        }
      });
    };

    $scope.loadMore = function(next) {
      $scope.loadingMore = true;
      $scope.next = false;
      var params = {
        link: next,
        params: {
        }
      };
      
      FeedService.getUsers(params).then(function(res){
        if(res.success){
          for(var x = 0; x < res.data[0].length; x++)
          {
            $scope.users.push(res.data[0][x]);
          }
          $scope.loading = false;
          if(res.pagination) {
            if(res.pagination.num_rows > $scope.users.length) {
              $scope.next = res.pagination.links.next
            }
            else {
              $scope.next = false;
            }
          }
          $scope.loadingMore = false;
        } 
        else {
          $scope.loading = false;
          $scope.message = 'Nothing to display.';
          $scope.loadingMore = false;
        }
      });

    }

  }
})();
