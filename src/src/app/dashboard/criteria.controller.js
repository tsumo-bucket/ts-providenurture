(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('CriteriaController', CriteriaController);

  /** @ngInject */
  function CriteriaController(webDevTec, $timeout,  $window, toastr, userData, $log, $scope, $rootScope, SettingsService, FeedService, MemberService, FilterFactory, $mdDialog, $state) {

    $scope.filter = FilterFactory;
    $('.navbar').addClass('transluscent');

    if(userData.isLogged()) {
      if($rootScope.user.usr_status == 'approved'){
        $scope.user = angular.fromJson(userData.isLogged());
        $rootScope.$broadcast('login', $scope.user);
        $state.go('members');
        $rootScope.hide = false;
        SettingsService.getInterest().then(function(res) {
          console.log(res);
          if(res.success) {
            $scope.user.usr_interested = res.data[0].usr_interested;
            $scope.user.usr_interested_age_from = parseInt(res.data[0].usr_interested_age_from);
            $scope.user.usr_interested_age_to = parseInt(res.data[0].usr_interested_age_to);
            $scope.user.usr_interested_country = res.data[0].usr_interested_country;
            $scope.user.distance_from = 1;
            $scope.user.distance_to = 5;
            // get value
            if(res.data[0].usr_interested == 'both')
            {
              $scope.filter.gender = [
                {checked: true, name: 'Man', value: 'male'},
                {checked: true, name: 'Woman', value: 'female'}
              ];
            }
            else {
              var gender = _.where($scope.filter.gender, {value: $scope.user.usr_interested})
                          .map(function(item) { 
                            item.checked = true; return item; }); 
              var reject = _.reject($scope.filter.gender, {value: gender[0].value});
              reject.push(gender[0]);
              $scope.filter.gender = reject;
            }
          }
        });

        FeedService.getUsers().then(function(res){
          console.log(res);
          if(res.success){
            $scope.users = res.data[0];
            $scope.loading = false;
          } 
          else {
            $scope.loading = false;
            $scope.message = 'Nothing to display.';
          }
        });
      }
      else
      {
        if($rootScope.user.usr_signup_step == 'initial') {
          $state.go('account.addPhotos');
          $rootScope.hide = true;
        }
        else if($rootScope.user.usr_signup_step == 'photo') {
          $state.go('account.addPhotos');
          $rootScope.hide = true;
        }
        else if($rootScope.user.usr_signup_step == 'info') {
          $state.go('account.personal');
          $rootScope.hide = true;
        }
        else if($rootScope.user.usr_signup_step == 'preference') {
          $state.go('account.preferences');
          $rootScope.hide = true;
        }
        else if($rootScope.user.usr_signup_step == 'done') {
          $state.go('search');
          $rootScope.hide = true;
        }
      }
    }
    else {
      $state.go('home');
    }
    
    $scope.loading = true;
    $scope.user = $rootScope.user;

    $scope.user.distance_from = 1;
    $scope.user.distance_to = 5;

    $scope.ageRangeSlider = {
      options: {
        floor: 18,
        ceil: 80,
        step: 1
      }
    };

    $scope.distanceRangeSlider = {
      options: {
        floor: 1,
        ceil: 10,
        step: 1
      }
    };
    
    $scope.$on('rzSliderForceRender', function() {
      console.log('force render');
      $('.rz-pointer-min').click();
    });

    $scope.openCriteria = function(ev) {
      $timeout(function () {
          $rootScope.$broadcast('rzSliderForceRender');
      }, 100);
      $mdDialog.show({
        controller: 'CriteriaController',
        templateUrl: 'app/dashboard/criteria.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false // Only for -xs, -sm breakpoints.
      })
      .then(function(answer) {
        console.log(answer);
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
       
    };

    $scope.applyCriteria = function(criteria) {
      $scope.cancel();
      var gender;
      if(_.where($scope.filter.gender, {checked: true}).map(function(item) { return item.value; }).length == 2) {
        gender = 'both';
      }
      else {
        gender = _.findWhere($scope.filter.gender, {checked: true}).value;
      }
      var params = {
        usr_interested: gender,
        usr_interested_age_from: criteria.usr_interested_age_from,
        usr_interested_age_to: criteria.usr_interested_age_to,
        usr_interested_country: criteria.usr_interested_country
      };
      console.log(params);
      SettingsService.updatePreference(params).then(function(res) {
        if(res.success) {
          $window.location.reload();
        }
        else {
          toastr.error(res.error.message);
        }
      });

    };
    
    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.source = '';

    $scope.onTabSelected = function(tab) {
      $scope.message = '';
      $scope.source = tab;
      $scope.loading = true;
      console.log(tab)
      // if(tab) {
      //   var params = {
      //     source: tab
      //   };
      // }
      $scope.users = [];
      FeedService.getUsers().then(function(res){
        console.log(res);
        if(res.success){
          $scope.users = res.data[0];
          $scope.loading = false;
        } 
        else {
          $scope.loading = false;
          $scope.message = 'Nothing to display.';
        }
      });
    };

    $scope.post = function(post) {
      var params = {
        pos_content: post
      }

      FeedService.createPost(params).then(function(res) {
        if(res.success) {
          toastr.success('Posted');
          $scope.post.pos_content = '';
        }
        else {
          toastr.error(res.error.message);
        }
      });
    };

    $scope.favorite = function(user) {
      console.log(user);
      if(user.faved) {
        var params = {
          usl_id: user.faved,
          usl_user: user.usr_id
        };
      }
      else {
        var params = {
          usl_user: user.usr_id
        };
      }

      MemberService.favorite(params).then(function(res) {
        $log.debug(res);
        if(res.data[0].usl_status=='active') {
          $scope.user.faved = res.data[0].usl_id;
        }
        else {
          $scope.user.faved = false;
        }
      });
    };

  }
})();
