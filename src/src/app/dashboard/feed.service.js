(function() {
  'use strict';

  angular
    .module('sugar')
    .service('FeedService', FeedService);

  function FeedService($http, $q, $log, URL, userData, $rootScope) {

    this.getUsers = function(data) {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: data.link,
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        params: data.params
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getSugarMan = function(data) {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: URL.API + 'users/sugar_man'
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getSugarWoman = function(data) {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: URL.API + 'users/sugar_woman'
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getFlameMan = function() {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: URL.API + 'users/flame_man'
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getFlameWoman = function() {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: URL.API + 'users/flame_woman'
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getMyFaves = function(data) {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: data,
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        params: {source: 'i-faved'}
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getFavedMe = function(data) {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: data,
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        params: {source: 'faved-me'}
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getViews = function(data) {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: data,
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getFollowing = function(data) {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: data,
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getBlocks = function(data) {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: URL.API + 'blocks/list',
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getPosts = function(data) {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: data.link,
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        params: data.params
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.createPost = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'posts/post',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.likePost = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'posts/like',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.follow = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'posts/like',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.flag = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'flags/report',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.saveSearch = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'searches/save',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.search = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'GET',
        url: data.link,
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        params: data.params
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.getSavedSearches = function(data) {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: URL.API + 'searches/list',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        params: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.deleteSearch = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'searches/update',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };
  }

})();
