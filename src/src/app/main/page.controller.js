(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('PageController', PageController);

  /** @ngInject */
  function PageController($timeout, webDevTec, toastr, $scope, $rootScope, SettingsService, MemberService, $stateParams, $log, $state, $location) {
    $scope.loading = true;
    
    if($state.current.name == 'privacy') {
      $scope.isPrivacy = true;

      // SettingsService.getPageContent(3).then(function(res) {
      //   $scope.page = res.data[0];
      //   $scope.loading = false;

      // });
    }

    $scope.login = function(user) {
      $rootScope.actionLoader = true;
      if($scope.loginForm.$invalid) {
        $scope.submitted = true;
        $scope.error.message = 'Invalid credentials.';
        $rootScope.actionLoader = false;
      }
      else {
        MemberService.authenticate(user).then(function(res) {
          $scope.submitted = true;
          $log.debug(res);
          $rootScope.actionLoader = false;
          if(res.success) {
            $scope.user = res.data;
            $rootScope.$broadcast('login', $scope.user);
            if($stateParams.url && $stateParams.token) {
              $state.go($stateParams.url, {usr_token_id: $stateParams.token});
            }
          }
          else {
            $scope.error.message = res.error;
          }
        });
      }
    };

    if($state.current.name == 'terms') {
      $scope.isTerm = true;
      // SettingsService.getPageContent(4).then(function(res) {
      //   $scope.page = res.data[0];
      //   $scope.loading = false;
      // });
    }

    $scope.scrollTo = function(hash, scroll) {
      $location.hash(hash);
      $scope.hashCurrent = hash;
      $("html, body").animate({scrollTop: scroll}, 'slow');
    };

    $scope.isSupport = false;
    if($state.current.name == 'support') {
      $scope.loading = false;
      $scope.isSupport = true;
      $scope.login = [
        {title: 'How long does it take for my profile to be approved?', content: '<p>Please be patient, photos and profiles enter the approval process in the order they were received. This usually takes 24 hours, however if the site is experiencing a high volume, this process may take up to 48 hours. If your profile or photos are taking longer, please visit your account Profile and make sure every section is 100% complete.</p>' 
        + '<p>NOTE: If a section is complete, there will be a green check mark next to it. If you are not a Premium Member, and have not completed all required sections, your profile and/or photo(s) will not be able to enter the approval process.</p>'},
        {title: 'Why was my account suspended?', content: '<p>If your account was suspended, a hold was placed due to suspicious account activity. Your profile and photos will be saved, but you will not be able to use the site, and other members will not be able to view your profile. Messages sent to your account will still be received, but cannot be accessed unless the suspension is fully reviewed and granted by Customer Support. Reasons for account suspension can include:</p>' 
                + '<ul>'
                + '<li>Reports by other members about your profile or conduct</li>'
                + '<li>Abusive, vulgar or sexually explicit language</li>'
                + '<li>Asking for money up front or in advance of your date</li>'
                + '<li>Using our site as an escort, or using the Service to solicit clients for an escort service</li>'
                + '<li>Using our site to promote, solicit, or engage in prostitution</li>'
                + '<li>Promoting or advertising a business</li>'
                + '<li>Selling pictures, videos or cam sessions</li>'
                + '<li>Soliciting passwords, bank information or other personal identifying information for commercial or unlawful purposes from other users</li>'
                + '<li>Posting any false, misleading, or inaccurate content about yourself and/or your profile</li>'
                + '<li>Having multiple active accounts</li>'
                + '<li>Creating a profile if you are under the age of 18</li>'
                + '<li>Posting or sending material that exploits people under the age of 18 in a sexual or violent manner, or solicits personal information from anyone under 18</li>'
                + '<li>You have disputed a payment that appears as W8Tech.com, W8 Tech, 2BuySafe.com/W8TECH or ALW*W8TECH.COM on your credit card statement</li>'
                + '</ul><p>NOTE: We reserve the right to suspend accounts for any reason whatsoever, as outlined in our <a target="_blank" href="https://www.sugarflame.com/web/#/terms-of-use"> terms and conditions</a>.</p>' },
        {title: 'How do I delete my account?', content: '<p>You may deactivate your account by following these instructions:</p>' +
                '<ul>' +
                '<li>Log into your account</li>' +
                '<li>Click on your username/thumbnail in the top right corner</li>' +
                '<li>Select Settings from the drop-down menu</li>' +
                '<li>On the bottom of the page, you will see the “Deactivate Account” link.</li>' +
                '<li>Click the link and follow the prompts.</li>' +
                '</ul>'}
      ];
      $scope.general = [
        {title: 'Can I restore my deleted messages?', content: '<p>Unfortunately, the option to restore messages is not available. We only allow the temporary “Restore” option that shows immediately after deleting a message.</p>'},
        {title: 'I did not receive an activation email, can you re-send it?', content: '<p>Activation emails may take a few minutes, please be patient. You can resend the activation email HERE. Please check your spam/junk mail folder for the activation email. Occasionally email providers will automatically mark our messages as spam. If you do not see an activation email in your spam/junk folder, please contact Customer Support with an alternate email address so we can change it to the email associated with your account.</p>' + 
                '<p>Gmail users: If the activation email is not in your inbox, please check your Social, Promotion, Spam</p>' },
        {title: 'How do I use a free Sugar Baby account on SugarFlame?', content: '<p>As a Standard (free) Sugar Baby, you are able to use Seeking Arrangement 100% free of charge, however you must always complete the below three steps in order to send messages and favorite members.</p>' +
                '<ul>' +
                '<li>Your profile must be 100% complete. Please visit your profile and make sure there’s a green check mark next to each profile section.</li>' +
                '<li>Your profile must be approved. Please be patient, this may take 24-48 hours during peak times.</li>' +
                '<li>You will also need an approved public photo. Only Premium members can communicate without a public photo.</li>' +
                '</ul>'},
        {title: 'I forgot my password!', content: '<p>You can reset your password HERE. Be sure to input the email address you used to create your account.</p>' + 
                '<p>Tip: Check your spam/junk mail folder if you do not see a prompt password reset email. Occasionally, email providers will automatically mark our messages as spam.</p>' },
        {title: 'I\'m getting an error message while on the website or on the mobile app, what should I do?', content: '<p>Please send a detailed description of what you’re seeing, what device you are using to access the site (make and model of your device), which operating system you are using (Windows, Mac OS, iOS (iPhone), Android, etc.), and which internet browser you are using (Google Chrome, Internet Explorer, Firefox, Safari, Dolphin, etc.). Where possible, please include a screenshot of the issue or error message. Send this information to Customer Support and we will be glad to assist.</p>'},
        {title: 'Can I change myself from a Sugar Baby to Sugar Daddy or Sugar Mommy? (Change your account type)', content: '<p>If you signed up with the wrong account type for your own profile, please contact Customer Support and let us know which account type you would like.</p>' + 
                '<p>NOTE: If you need to change the gender to Male or Female for what you are looking for, click on your username in the top right-hand corner, then click “Your Profile.” From there you can select “Basic Info” and change your setting of what you are looking for.</p>' },
        {title: 'How do I restore my deactivated account?', content: '<p>Simply log into your deactivated account to reactivate. You will then be sent a reactivation email. Click the activation link in it and your account will automatically become restored.</p>'},
        {title: 'How do I change the gender to Male or Female for what I am looking for?', content: '<p>To edit your preferences, click on your username in the top right-hand corner, then click “Your Profile.” From there you can select “Basic Info” and change your setting of what you are looking for.</p>' + 
                '<p>NOTE: If you want to change yourself from a Sugar Baby (Flame) to a Sugar Daddy (Sugar) or vice-versa, please contact Customer Support and let us know which account type you would like.</p>' },
        {title: 'How do I block someone that is rude or harassing me?', content: '<p>To block a member: Click the gear icon on the user’s profile, next to the Favorite button, and click on ‘Block (username)’. Blocking will prevent the member from communicating with you and from seeing your profile.</p>' + 
                '<p>IMPORTANT: If a user has threatened you or committed an act of violence or theft, please contact your local law enforcement agency. Also, report the member from their profile or your conversation with them.</p>' },
        {title: 'How do I report a member violation?', content: '<p>Click the gear icon on the user’s profile, next to the Favorite button, and click on ‘Report Member’. Pick the appropriate reason from the list and provide a clear description of the violation. Additionally, you may also block the member which will prevent them from communicating with you and from seeing your profile.</p>' + 
                '<p>IMPORTANT: If a user has threatened you or committed an act of violence or theft, please contact your local law enforcement agency. Also, report the member from their profile or your conversation with them as described above.</p>' },
        {title: 'I\'m a Sugar Baby. Do you have any tips or advice for finding a Sugar Daddy?', content: '<p>Finding the right Sugar Daddy for you is a process that probably won’t happen overnight, but if you’re willing to put in some time and have patience, we’re confident that you’ll eventually find the right person for you.</p>' + 
                '<p>Please keep in mind that the more you log in, the more detailed your profile is, and the more photos you have, the more views, favorites, and responses you will receive. Update your profile with more information about yourself and the relationship you’re looking for, uploading more photos and messaging more members. Be more pro-active in your search by breaking the ice with as many members as you can.</p>' },
        {title: 'How can I submit a customer support ticket?', content: '<p>If you have an issue that is not explained in our FAQ sections, please contact Customer Support and someone from our dedicated support team will reply back within 24-48 hours.</p>' + 
                '<p>You can also send an email to support@SugarFlame.com from the email associated with your SugarFlame account.</p>' + 
                '<p>Note: We do not offer customer support via telephone at this time.</p>' }
      ];
      $scope.privacy = [
        {title: 'I received a suspicious email. How do I report a phishing scam?', content: '<p>Please report all phishing emails by clicking the report phishing feature within your email client and/or email website (most email websites and clients have this feature). If you know which member may have sent this to you, please contact Customer Support with any details you have so we can take action.</p>'},
        {title: 'How do I hide my profile, join date or activity?', content: '<p>You can hide your profile by going to the Settings section of your account and clicking the “Hidden” button next to the “Search and Dashboard” option. Turning this setting to “Hidden” will only hide your profile from search and other member’s dashboards. Members you message and favorite can still view your profile.</p>' 
                + '<p>Premium Members have more options of what they can hide, such as:</p>' 
                + '<ul>' 
                + '<li>Join Date</li>' 
                + '<li>Recent Login Location</li>' 
                + '<li>Online Status / Last Active Date</li>' 
                + '<li>When You View Someone</li>' 
                + '<li>When You Favorite Someone</li>' 
                + '</ul><p>These features are coming out in our June build.</p>'},
        {title: 'Will a member I blocked be able to view my profile?', content: '<p>No. Once you have blocked another member, they will no longer be able to message you, view your profile, or see your profile in his/her search results.</p>'}
      ];
      $scope.profile = [
        {title: 'What information is not allowed on my profile?', content: '<ul>' 
                + '<li>Duplicate Information</li>' 
                + '<li>Contact Information (last name, phone number, social media usernames, email address, etc.)</li>' 
                + '<li>Links to any other websites</li>' 
                + '<li>Requests for sex or sexually explicit text</li>' 
                + '<li>Specific monetary amounts</li>' 
                + '<li>Commercial activity of any kind</li>' 
                + '</ul>'}
      ];
      $scope.photo = [
        {title: 'How long does it take for my photo to be approved?', content: '<p>Please be patient, photos and profiles enter the approval process in the order they were received. This usually takes 24 hours, however if the site is experiencing a high volume, this process may take up to 48 hours. If you haven’t completed your profile, and are not a Premium Member, you will need to complete your profile first before your photo(s) enter the approval process.</p>'},
        {title: 'How do I upload photos from my browser?', content: '<ul>' 
                + '<li>To access your photos, click your thumbnail photo on the top right to display the drop down menu. Then click My Profile”</li>' 
                + '<li>Use the left hand menu to choose the “Photos” option.</li>' 
                + '<li>Click the “Add Public Photo” button if you would like to add a public photo.</li>' 
                + '</ul>'},
        {title: 'Public photo guidelines', content: '<p>When uploading a public photo, please follow the rules listed below. Photos that violate the guideline will be denied and your account may be suspended.</p>' 
                + '<p>Photos can include:</p>' 
                + '<ul>' 
                + '<li>Photos must include yourself.</li>' 
                + '<li>Bottom nose to chin photos.</li>' 
                + '<li>Partially blurred or masked photos.</li>' 
                + '<li>Clothed photo of body without your face.</li>' 
                + '<li>Couple photos, only if you’re present in the photo.</li>'
                + '<li>Houses, Cars, Yachts are allowed if you’re present in the photo.</li>' 
                + '<li>Fully covered lingerie, underwear and bikini photos.</li>' 
                + '</ul>'
                + '<p>Things that are not allowed:</p>' 
                + '<p>It’s important to note that violating the rules below may result in a disabled account or discontinued use, without warning.</p>' 
                + '<ul>' 
                + '<li>Any photos that doesn’t feature yourself.</li>' 
                + '<li>Photos featuring only lips and hands.</li>' 
                + '<li>Photos featuring only lips and hands.</li>' 
                + '<li>Nude or sexually explicit photos.</li>' 
                + '<li>Photos containing or depicting illegal content.</li>' 
                + '<li>Duplicate photos.</li>' 
                + '<li>Photos from other members on SugarFlame.</li>' 
                + '<li>Copyrighted photos from any website.</li>' 
                + '</ul>'},
        {title: 'Private photo guidelines', content: '<p>When uploading a private photo, please follow the rules listed below. Photos that violate the guideline will be denied and your account may be suspended.</p>' 
                + '<p>Private photos can include:</p>' 
                + '<ul>' 
                + '<li>Photos must include yourself.</li>' 
                + '<li>Bottom nose to chin photos.</li>' 
                + '<li>Partially blurred or masked photos.</li>' 
                + '<li>Clothed photo of body without your face.</li>' 
                + '<li>Couple photos, only if you’re present in the photo.</li>' 
                + '<li>houses, Cars, Yachts are allowed if you’re present in the photo.</li>' 
                + '<li>Fully covered lingerie, underwear and bikini photos.</li>' 
                + '</ul>'
                + '<p>Things that are not allowed:</p>'
                + '<p>It’s important to note that violating the rules below may result in a disabled account or discontinued use, without warning.</p>'
                + '<ul>' 
                + '<li>Any photos that doesn’t feature yourself.</li>' 
                + '<li>Photos featuring only lips and hands.</li>' 
                + '<li>Photos including children.</li>' 
                + '<li>Nude or sexually explicit photos.</li>' 
                + '<li>Photos containing or depicting illegal content.</li>' 
                + '<li>Duplicate photos.</li>' 
                + '<li>Photos from other members on SugarFlame.</li>' 
                + '<li>Copyrighted photos from any website.</li>' 
                + '</ul>' },
        {title: 'How do I delete a photo?', content: '<ul>' 
                + '<li>To access your photos, click your thumbnail photo on the top right to display the drop down menu. Then click “My Profile”</li>' 
                + '<li>Use the left hand menu to choose the “Photos” option.</li>' 
                + '<li>Click the Settings (Gear) icon, and click the “Delete Photo” option located in the lower right hand corner.</li>' 
                + '</ul>'},
        {title: 'How do I set my main profile photo?', content: '<p>Main profile photos must be public. Please note private photos cannot be set as a main profile photo.</p>' 
                + '<ul>' 
                + '<li>To access your photos, click your thumbnail photo on the top right to display the drop down menu. Then click “My Profile”</li>' 
                + '<li>Use the left hand menu to choose the “Photos” option.</li>' 
                + '<li>Click the Settings (Gear) icon, and click the “Set as Primary” option located in the lower right hand corner.</li>' 
                + '</ul>'},
        {title: 'How do I prove my identity after my photos were denied?', content: '<p>Please send us a photo or scanned copy of your government issued photo ID and we will be happy to approve your photo(s). You may blur out all personal information, as we only need to see your photo. Email the photo from the email address associated with your account to: support@SugarFlame.com.</p>'},
        {title: 'How do I report someone who is using my photo on the website?', content: '<p>If you have found someone using your photo on the website, you can request that the photo be removed by writing to customer support. Please submit a support ticket here. Be sure to provide your email address so we may contact you if we have questions. For example, we may ask you to provide us with a copy of a government issued identification or other evidence that proves the photo is yours.</p>' 
              + '<p>Need further assistance? Report an issue* Responses may take up to 24 hours</p>'
              + '<p>Disclaimer: An arrangement is not an escort service. SugarFlame in no way, shape or form supports escorts or prostitutes using our website for personal gain. Profiles suspect of this usage will be addressed by the SugarFlame Misconduct Team and banned from our website.</p>' }
      ];
    }
    $scope.isSafety = false;

    if($state.current.name == 'precautions') {
      $scope.loading = false;
      $scope.isSafety = true;
      $scope.safety = [
        {title: 'General Safety Tips', content: '<ul>' + 
                  '<li>When meeting someone for the first time, meet them for lunch or coffee during the day.</li>' + 
                  '<li>Always let your family or friends know before you meet someone the first time. Let them know where, when, and who you will be meeting.</li>' + 
                  '<li>Don’t accept personal checks or MoneyGram as a form of allowance. Members may cancel these payments without notice.</li>' + 
                  '<li>Don’t disclose your personal banking information, including account numbers or passwords.</li>' + 
                  '</ul>'},
        {title: 'Take Action', content: '<ul>' + 
                  '<li>Report members who request sexual offers in exchange for money.</li>' + 
                  '<li>Report members who identify themselves as escorts or prostitutes since this is strictly against our <a target="_blank" href="https://www.sugarflame.com/web/#/terms-of-use">terms of use</a>.</li>' + 
                  '<li>Report any member guilty of online harassment.</li>' + 
                  '<li>Report anyone who asks for money upfront and before meeting in person. This includes, but is not limited to sob stories asking for money, and individuals needing to pay their cell phone bills.</li>' + 
                  '<li>Report members who ask for your address so they can send you gifts.</li>' + 
                  '<li>Report members who ask for your bank account details, including account numbers or passwords.</li>' + 
                  '</ul>'}
      ];

      $scope.privacy = [
        {title: 'Protecting your privacy', content: '<ul>' + 
                  '<li>Never place your personal contact information on your profile such as your full name, phone number, email address, or address.</li>' + 
                  '<li>Use a temporary <a href="https://hushed.com/" target="_blank">Hushed</a> disposable phone number or a Google Voice number for added privacy, only give your main number to people you trust.</li>' + 
                  '<li>Don\’t place links to other social networks or websites on your SugarFlame profile, or in your messages to people you just met.</li>' + 
                  '<li>Don\’t use photos you have posted on other social media sites (Facebook, Instagram, etc.) on your SugarFlame profile.</li>' + 
                  '<li>Do not use the same username on all social networks as it allows unwanted users to find you easily.</i>' + 
                  '</ul>'},
        {title: 'Preventing your account from being hacked', content: '<ul>' + 
                  '<li>Avoid hacking by using complex passwords on your SugarFlame account.</li>' + 
                  '<li>Check to make sure your primary email account is secure. Anyone with access to your email can retrieve your SugarFlame password.</li>' + 
                  '<li>Never share your account login and password with anyone.</li>' + 
                  '</ul>'}
      ];

      $scope.precautions = [
        {title: 'Protecting yourself from criminals', content: '<ul>' + 
                  '<li>Date background verified members first. Ask for the members facebook and linkedin account.</li>' + 
                  '<li>SugarFlame does not perform background checks on members, so please proceed with caution.</li>' + 
                  '<li>Do your own research, and always find out more about someone before you decide to meet them.</li>' + 
                  '<li>Don’t discuss sex in your SugarFlame profile description or messages you send to members.</li>' + 
                  '</ul>'},
        {title: 'Warning about minors', content: '<p>SugarFlame.com is for users who are 18 years of age or older.</p>' + 
                  '<p>Having an arrangement with a minor is considered illegal. If you suspect a member to be underage, please ask for a photo ID, or report such members to Customer Support, and we will verify their age for you.</p>'},
        {title: 'Warning about sex opportunists', content: '<p>Sex opportunists are Sugar Daddies or Sugar Mommas who promise to give you the world, but their only goal is to have sex with you, and not to form a solid arrangement. Here are some tips to avoid being taken advantage of by a sex opportunist:</p>' + 
                  '<ul>' +
                  '<li>Don’t trust anyone until they have earned your trust, especially when they promise a large allowance.</li>' + 
                  '<li>Don’t have sex on the first date, get to know the person first.</li>' + 
                  '<li>Don’t fall for a “test-drive”. You are not a car, and they are not shopping for one. If someone ask for a test drive, they just want sex with you once, and never see you again.</li>' + 
                  '</ul>'},
        {title: 'Here are some common tricks used by sex opportunists', content: '<p>Sugar Daddy promises gifts of hefty allowances, spends the night with the SB and disappears after (by giving all sorts of excuses) without ever giving any allowances.  Excuses may include:</p>' + 
                  '<ul>' +
                  '<li>I have an emergency, I need to go.</li>' + 
                  '<li>I have a business meeting, I need to leave.</li>' + 
                  '<li>I don’t have cash, can I send you the allowance later?</li>' + 
                  '<li>Sudden attack of guilt, says he feels guilty he cheated on his wife, then chases you out the door.</li>' + 
                  '</ul>'},
        {title: 'Warning about financial opportunists', content: '<p>Financial opportunists are attractive women or men who are only after your money. Their goal is to scam you, and they have no intention of ever having a relationship with you. Here are some tips to avoid being taken advantage of by a financial opportunist:</p>' + 
                  '<ul>' +
                    '<li>Never ever send money to anyone you have not met in person, regardless of the reason, and there will be many.</li>' +
                    '<li>If someone says she is currently in Nigeria, Ghana or Philippines and asks for money, proceed with caution. Verify all information with extreme scrutiny.</li>' +
                  '</ul>' +
                  '<p>Here are some common tricks used by financial opportunists. She/He asks for money for various reasons, and if you send the money you will never hear from her again:</p>' +
                  '<ul>' +
                    '<li>I need money to prove you are a real Sugar Daddy/Momma and are ready to spoil me.</li>'+
                    '<li>I need money for a spa day to look gorgeous when I meet you.</li>' +
                    '<li>I need money for travel expenses.</li>' +
                    '<li>I need money for an emergency surgery for myself or a family member etc.</li>' +
                    '<li>I need money because my phone is broken/stolen, or I don\’t have minutes, but I really want to call you.</li>' +
                  '</ul>' +
                  '<p>Disclaimer: An arrangement is not an escort service. SugarFlame in no way, shape or form supports escorts or prostitutes using our website for personal gain. Profiles suspect of this usage will be addressed by the SugarFlame Misconduct Team and banned from our website.</p>'}
      ];
    }

    if($state.current.name == 'contact') {
      SettingsService.getPageContent().then(function(res) {
        $scope.page = res.data[0];
        $scope.loading = false;
      });
    }
  }
})();
