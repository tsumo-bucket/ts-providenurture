(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, $interval, logged, webDevTec, toastr, $scope, $sce, $log, $window, $document, FeedService, $state, $stateParams, $rootScope, $mdDialog, userData, MemberService, $translate) {

    //sample translation for API calls
    var resp = 'LOGIN_FAILED';
    $translate(resp).then(function(value){
      // console.log(value);
    });
    $translate.use('en');
    //use this to trigger language change
    $scope.changeLanguage = function (key) {
      $translate.use(key);
    };

    $scope.banners = [
      {img: 'assets/images/banner-1-new.jpg'},
      {img: 'assets/images/banner-2-new.jpg'}
    ]
    $scope.banner_image = $scope.banners[0];

    $interval(function() {
      if($scope.banner_image == $scope.banners[0]) {
        $scope.banner_image = $scope.banners[1];
      }
      else {
        $scope.banner_image = $scope.banners[0];
      }
    }, 5000);

    $scope.changeBanner = function(banner) {
      $scope.banner_image = banner;
    }

    if( typeof Storage != 'undefined' ) { 
      userData.destroyTemp();
      if(logged) {
        $rootScope.user = JSON.parse(userData.isLogged());
        $rootScope.$broadcast('login', $rootScope.user);
        if($rootScope.user.usr_status == 'approved'){
          $rootScope.hide = false;
          $state.go('members');
          $('.navbar').addClass('transluscent');
        }
        else
        {
          if($rootScope.user.usr_signup_step == 'initial') {
            $state.go('account.addPhotos');
            $rootScope.hide = true;
          }
          else if($rootScope.user.usr_signup_step == 'photo') {
            $state.go('account.addPhotos');
            $rootScope.hide = true;
          }
          else if($rootScope.user.usr_signup_step == 'info') {
            $state.go('account.personal');
            $rootScope.hide = true;
          }
          else if($rootScope.user.usr_signup_step == 'preference') {
            $state.go('account.preferences');
            $rootScope.hide = true;
          }
          else if($rootScope.user.usr_signup_step == 'done') {
            $state.go('search');
            $rootScope.hide = true;
          }
        }
      }
      else {
        $timeout(function(){
          if(!userData.isLogged()) {
            $rootScope.actionLoader = false;
          }
          else {
            $rootScope.user = JSON.parse(userData.isLogged());
            $rootScope.$broadcast('login', $rootScope.user);
            if($rootScope.user.usr_status == 'approved'){
              $rootScope.hide = false;
              $state.go('members');
              $('.navbar').addClass('transluscent');
            }
            else
            {
              if($rootScope.user.usr_signup_step == 'initial') {
                $state.go('account.addPhotos');
                $rootScope.hide = true;
              }
              else if($rootScope.user.usr_signup_step == 'photo') {
                $state.go('account.addPhotos');
                $rootScope.hide = true;
              }
              else if($rootScope.user.usr_signup_step == 'info') {
                $state.go('account.personal');
                $rootScope.hide = true;
              }
              else if($rootScope.user.usr_signup_step == 'preference') {
                $state.go('account.preferences');
                $rootScope.hide = true;
              }
              else if($rootScope.user.usr_signup_step == 'done') {
                $state.go('search');
                $rootScope.hide = true;
              }
            }
          }
        }, 1000);
      }
    }
    else {
      function showAlert() {
          alert = $mdDialog.alert({
            title: '',
            textContent: 'SugarFlame needs a non-private browser to work on iOS. Please switch to normal browsing for better user experience. Thank you!',
            ok: '',
            addClass: 'browser-alert'
          });

          $mdDialog
            .show( alert )
            .finally(function() {
              });
            $timeout(function() {
              $('.md-default-theme').addClass('browser-alert');
            });
          }
        showAlert();
    }

    $scope.$on('home', function(events, args) {
      $rootScope.isHome = args;
      if(!$rootScope.isHome) {
        $('.navbar').addClass('transluscent');
      }
    });

    $scope.cancel = function(){
      $mdDialog.cancel();
    }

    $scope.openReset = function(ev) {
      $mdDialog.show({
        controller: MainController,
        templateUrl: 'app/main/reset-password.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false // Only for -xs, -sm breakpoints.
      })
      .then(function(answer) {
      }, function() {
      });
    };

    $scope.successReset = function(ev) {
      $mdDialog.show({
        controller: MainController,
        templateUrl: 'app/main/reset-success.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: false // Only for -xs, -sm breakpoints.
      })
      .then(function(answer) {
      }, function() {
        $state.go('home', {usr_password_reset: ''});
        $window.location.reload();
      });
    };

    $scope.ok = function() {
      $state.go('home', {usr_password_reset: ''});
      $window.location.reload();
    }

    if($state.current.name == 'reset') {
      if($stateParams.usr_password_reset) {
        $scope.openReset();
      }
      else {
        $scope.cancel();
      }
    }
    $scope.sugarGender = 'male';
    $scope.filterSugar = function(gender) {
      $scope.sugarGender = gender;
    }

    $scope.flameGender = 'female';
    $scope.filterFlame = function(gender) {
      $scope.flameGender = gender;
    }

    $scope.sugar = [];
    $scope.flame = [];

    FeedService.getSugarMan().then(function(res) {
      // console.log(res);
      if(res.success) {
        $scope.sugarMan = res.data[0];
      }
    });

    FeedService.getSugarWoman().then(function(res) {
      // console.log(res);
      if(res.success) {
        $scope.sugarWoman = res.data[0];
      }
    });

    FeedService.getFlameMan().then(function(res) {
      // console.log(res);
      if(res.success) {
        $scope.flameMan = res.data[0];
      }
    });

    FeedService.getFlameWoman().then(function(res) {
      // console.log(res);
      if(res.success) {
        $scope.flameWoman = res.data[0];
      }
    });

    $scope.resetPassword = function(data) {
      $rootScope.actionLoader = true;
      var params = {
        usr_password_reset: $stateParams.usr_password_reset,
        usr_password: data.usr_password
      };

      MemberService.resetPassword(params).then(function(res) {
        $rootScope.actionLoader = false;
        $scope.cancel();
        if(res.success) {
          $scope.successReset();
        }
        else {
          toastr.error(res.error.message);
          $timeout(function() {
            $window.location.reload();
          }, 1000);
        }
      });

    };

    // $scope.slickImage = {
    //   method: {},
    //   slidesToShow: 1,
    //   slidesToScroll: 1,
    //   arrows: false,
    //   asNavFor: '.slider-text',
    //   fade:true
    // };

    // $scope.slickText = {
    //   method: {},
    //   dots:true,
    //   slidesToShow: 1,
    //   slidesToScroll: 1,
    //   asNavFor: '.slider-image',
    //   arrow:false,
    // };

    if($state.current.name=='home') {
      $('.navbar').removeClass('transluscent');
      $document.on('scroll', function() {
        $scope.$apply(function() {

          if($window.scrollY >= 68) {
              $scope.fixNavs = true;
              $('.navbar').addClass('transluscent');
          } else {
              $scope.fixNavs = false;
              $('.navbar').removeClass('transluscent');
          }
        });
      });
    }
    else {
      $('.navbar').addClass('transluscent');
    }

  }
})();
