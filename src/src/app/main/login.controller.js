(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController($timeout, webDevTec, userData, toastr, $scope, logged, $state, $stateParams, URL, MemberService, $rootScope, $log) {
    if(logged) {
      if($stateParams.url && $stateParams.token) {
        $state.go($stateParams.url, {usr_token_id: $stateParams.token});
      }
    }
    $scope.logon = function(user) {
      $rootScope.actionLoader = true;
      if($scope.loginForm.$invalid) {
        $scope.submitted = true;
        $scope.error =  {
          message: 'Invalid credentials.'
        };
        $rootScope.actionLoader = false;
      }
      else {
        MemberService.authenticate(user).then(function(res) {
          $scope.submitted = true;
          $rootScope.actionLoader = false;
          if(res.success) {
            $scope.user = res.data;
            $rootScope.$broadcast('login', $scope.user);
            if($stateParams.url && $stateParams.token) {
              $state.go($stateParams.url, {usr_token_id: $stateParams.token});
            }
            else if($stateParams.url && !$stateParams.token){
              $state.go($stateParams.url);
            }
            else {
              $state.go('members');
            }
          }
          else {
            $scope.error.message = res.error;
          }
        });
      }
    };

    var checkEmail = function(params, response, access_token, photo_count) {
      MemberService.emailCheck(params).then(function(res) {
        if(res.success) {
          if(res.data[0].usr_signup_source == 'facebook') {
            $rootScope.$broadcast('login', res.data[0]);
            $rootScope.actionLoader = false;
            if(res.data[0].usr_signup_step == 'done' && res.data[0].usr_status == 'approved') {
              $state.go('members');
              $scope.hide = false;
            }
            if(res.data[0].usr_signup_step == 'done' && res.data[0].usr_status == 'pending') {
              $state.go('search');
              $scope.hide = true;
            }
            else if(res.data[0].usr_signup_step == 'initial') {
              $state.go('account.addPhotos');
              $scope.hide = true;
            }
            else if(res.data[0].usr_signup_step == 'photo') {
              $state.go('account.addPhotos');
              $scope.hide = true;
            }
            else if(res.data[0].usr_signup_step == 'info') {
              $state.go('account.personal');
              $scope.hide = true;
            }
            else if(res.data[0].usr_signup_step == 'preference') {
              $state.go('account.preferences');
              $scope.hide = true;
            }
          }
          else {
            toastr.warning('Email address is not valid or unavailable.');
            $rootScope.actionLoader = false;
          }
        }
        else {
          $rootScope.user = {
            usr_email: response.email,
            first_name: response.first_name,
            last_name: response.last_name,
            fb_image: response.picture.data.url,
            fb_birthday: response.birthday,
            friend_count: response.friends.summary.total_count,
            photo_count: photo_count,
            fb_id: response.id,
            access_token: access_token,
            usr_signup_source: 'facebook'
          }
          userData.setTempUser($rootScope.user);
          $state.go('join2');
          $rootScope.actionLoader = false;
        }
      });
    };

    $scope.facebookLogin = function() {
      $rootScope.actionLoader = true;
      FB.getLoginStatus(function(response) {
        console.log(response);
        if(response.status == 'connected') {
          var access_token = response.authResponse.accessToken;
          FB.api('/me',
            {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday, picture, permissions"},
            function(response) {
            console.log(response);

            if(!response.birthday || !response.friends) {
              FB.login(function(response) {
                if (response.authResponse) {
                  var access_token = response.authResponse.accessToken;
              
                  FB.api('/me', 
                    {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday"},
                    function(response) {
                      console.log(response);
                      var params = {
                        email: response.email
                      };
                      FB.api(
                        '/me/photos',
                        'GET',
                        {"fields":"images,picture", "type":"uploaded"},
                        function(response) {
                          if(response.data) {
                            $scope.photo_count = response.data.length;
                          }
                          else {
                            $scope.photo_count = '';
                          }
                          
                          if($scope.photo_count) {
                            checkEmail(params, response, access_token, $scope.photo_count);
                          }
                          else {
                            checkEmail(params, response, access_token, $scope.photo_count);
                          }
                        }
                      );

                  });
                } else {
                  $rootScope.actionLoader = false;
                  toastr.warning('User cancelled login or did not fully authorize.');
                }
              }, {scope: 'email,user_birthday,user_friends,user_photos'});
            }
            else {
              var params = {
                email: response.email
              };
              FB.api('/me', 
                {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday, picture, permissions"},
                function(response) {
                  console.log(response);
                  $scope.initial_response = response;
                  var params = {
                    email: response.email
                  };
                  FB.api(
                    '/me/photos',
                    'GET',
                    {"fields":"images,picture", "type":"uploaded"},
                    function(response) {
                      if(response.data) {
                        $scope.photo_count = response.data.length;
                      }
                      else {
                        $scope.photo_count = '';
                      }
                      // for counting
                      // if(response.paging.next) {
                      //   countPhotos(response.paging.next);
                      // }
                      if($scope.photo_count) {
                        checkEmail(params, $scope.initial_response, access_token, $scope.photo_count);
                      }
                      else {
                        checkEmail(params, $scope.initial_response, access_token, $scope.photo_count);
                      }
                    }
                  );
                  
              });
              // checkEmail(params, response, access_token);
            }
          });
        }
        else {
          FB.login(function(response) {
            if (response.authResponse) {
              var access_token = response.authResponse.accessToken;
              console.log(response);
          
              FB.api('/me', 
                {"fields":"first_name, last_name, email, gender, is_verified, verified, friends, birthday, picture, permissions"},
                function(response) {
                  console.log(response);
                  var params = {
                    email: response.email
                  };
                  FB.api(
                    '/me/photos',
                    'GET',
                    {"fields":"images,picture", "type":"uploaded"},
                    function(response) {
                      $scope.photo_count = response.data.length;

                      // for counting
                      // if(response.paging.next) {
                      //   countPhotos(response.paging.next);
                      // }
                    }
                  );
                  if($scope.photo_count) {
                    checkEmail(params, response, access_token, $scope.photo_count);
                  }
                  else {
                    checkEmail(params, response, access_token, $scope.photo_count);
                  }
              });
            } else {
              $rootScope.actionLoader = false;
              toastr.warning('User cancelled login or did not fully authorize.');
            }
          }, {scope: 'email,user_birthday,user_friends,user_photos'});
        }
      });
    };
  }
})();
