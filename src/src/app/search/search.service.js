(function() {
  'use strict';

  angular
    .module('sugar')
    .service('SearchService', SearchService);

  function SearchService($http, $q, $log, URL, userData, $rootScope) {

    this.getSearches = function(data) {
      var deferResult = $q.defer();
      
      $http({
        method: 'GET',
        url: URL.API + 'users/list',
        headers : {
          'Token' : userData.getUserData('usr_token')
        }
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

    this.search = function(data) {
      var deferResult = $q.defer();

      $http({
        method: 'POST',
        url: URL.API + 'posts/post',
        headers : {
          'Token' : userData.getUserData('usr_token')
        },
        data: data
      }).success(function(response) {
        deferResult.resolve(response);
      }).error(function(error) {
        deferResult.reject(error);
      });

      return deferResult.promise;
    };

  }

})();
