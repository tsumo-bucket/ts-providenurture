(function() {
  'use strict';

  angular
    .module('sugar')
    .controller('SearchController', SearchController);

  /** @ngInject */
  function SearchController($timeout, webDevTec, toastr, URL, $scope, $log, $window, FeedService, MemberService, SettingsService, FilterFactory, $mdDialog, $mdToast, $state, $rootScope, userData, reverseGeocode) {
    $('.navbar').addClass('transluscent');
    $scope.appendToGeneral = angular.element(document.querySelector('#general'));
    $scope.appendToLooks = angular.element(document.querySelector('#looks'));
    $scope.appendToLifestyle = angular.element(document.querySelector('#lifestyle'));

    $scope.filter = FilterFactory;
    $scope.filter.gender = [
      {checked: false, name: 'Men', value: 'male'},
      {checked: false, name: 'Women', value: 'female'}
    ];

    $scope.source = '';
    $scope.loading = true;

    if(userData.isLogged()) {
      // $rootScope.actionLoader = false;
    }
    else {
      // $window.location.reload();
      $timeout(function(){
        if(!userData.isLogged()) {
          $state.go('home');
          $rootScope.actionLoader = false;
        }
      }, 100);
    }
    
    $scope.next = false;
    var searchUsers = function(params) {
      FeedService.search(params).then(function(res){
        $scope.errMessage = 'Nothing to display.';
        if(res.success){
          $scope.users = res.data[0];
          $scope.loading = false;
          if(res.pagination) {
            if(res.pagination.num_rows > $scope.users.length) {
              $scope.next = res.pagination.links.next;
              $scope.pagination = res.pagination;
            }
            else {
              $scope.next = false;
            }
          }
        } 
        else {
          $scope.loading = false;
          $scope.users = [];
          $scope.errMessage = 'Nothing to display.';
          $scope.next = false;
          $scope.pagination.num_rows = 0;
        }
      });
    };

    // $scope.filter.country = [];
    // SettingsService.getCountries().then(function(res) {
    //   if(res.success) {
    //     $scope.filter.country = res.data[0];
    //   }
    // });

    $scope.ageRangeSlider = {
      minValue: 18,
      maxValue: 80,
      options: {
        floor: 18,
        ceil: 80,
        step: 1,
        minRange: 1
      }
    };

    $scope.original_loc = $scope.user.usr_location;

    var getLocationSuccess = function(position) {
      $scope.user.ulc_lat = position.coords.latitude;
      $scope.user.ulc_lng = position.coords.longitude;
      $scope.newLat = position.coords.latitude; 
      $scope.newLng = position.coords.longitude; 
      reverseGeocode.geocodePosition(position.coords.latitude, position.coords.longitude, function(address) {
        $scope.user.usr_location = address;
        $scope.$digest();
      });
    }

    var getLocationFail = function(position) {
      toastr.warning('User denied geolocation permission.');
      $scope.location = false;
    }

    $scope.original_lat = $scope.user.ulc_lat;
    $scope.original_lng = $scope.user.ulc_lng;

    $scope.getCurrentLocation = function(location) {
      $scope.user.location = location;
      if(location) {
        navigator.geolocation.getCurrentPosition(getLocationSuccess, getLocationFail);
      }
      else {
        $scope.user.ulc_lat = $scope.original_lat;
        $scope.user.ulc_lng = $scope.original_lng;
        $scope.user.usr_location = $scope.original_loc;
        $scope.user.location = false;
      }
    };

    $scope.$watch('components', function (newValue, oldValue, scope) {
        if(newValue) {
          $scope.user.ulc_lat = newValue.latitude;
          $scope.user.ulc_lng = newValue.longitude;
        }
    },true);

    MemberService.getUser().then(function(res) {
      if(res.success) {
        $scope.user = res.data[0];
        $scope.ageRangeSlider.minValue = parseInt(res.data[0].usr_interested_age_from);
        $scope.ageRangeSlider.maxValue = parseInt(res.data[0].usr_interested_age_to);
        $scope.filter.usr_interested_age_from = parseInt($scope.user.usr_interested_age_from);
        $scope.filter.usr_interested_age_to = parseInt($scope.user.usr_interested_age_to);
        var params = {
          link: URL.API + 'searches/search?is_infinite=true&page=1&page_size=20',
          params: {
            lat: $scope.user.ulc_lat,
            long: $scope.user.ulc_lng,
            order_by: 'usr_approved_date',
            order: 'desc',
            distance_from: $scope.filter.distance_from,
            distance_to: $scope.filter.distance_to,
            source: ''
          }
        };
        searchUsers(params); 
        SettingsService.getInterest().then(function(res) {
          if(res.success) {
            $scope.filter.usr_interested = res.data[0].usr_interested;
            $scope.ageRangeSlider.minValue = parseInt(res.data[0].usr_interested_age_from);
            $scope.ageRangeSlider.maxValue = parseInt(res.data[0].usr_interested_age_to);
            $scope.filter.usr_interested_age_from = parseInt(res.data[0].usr_interested_age_from);
            $scope.filter.usr_interested_age_to = parseInt(res.data[0].usr_interested_age_to);
            $scope.filter.usr_interested_country = res.data[0].usr_interested_country;
            $scope.filter.distance_from = 0;
            $scope.filter.distance_to = 10000;
            // get value
            if(res.data[0].usr_interested == 'both')
            {
              $scope.filter.gender = [
                {checked: true, name: 'Man', value: 'male'},
                {checked: true, name: 'Woman', value: 'female'}
              ];
            }
            else {
              var gender = _.where($scope.filter.gender, {value: $scope.user.usr_interested})
                          .map(function(item) { 
                            item.checked = true; return item; }); 
              var reject = _.reject($scope.filter.gender, {value: gender[0].value});
              reject.push(gender[0]);
              $scope.filter.gender = reject;
            }
          }
        });
        SettingsService.getInfo().then(function(res) {
          $scope.info = res.data[0];
          $scope.filter.inf_spending_habits = parseInt($scope.info.inf_spending_habits);
          if($scope.user.usr_type == 'flame') {
            $scope.filter.inf_preferred_range_from = parseInt($scope.info.inf_preferred_range_from);
            $scope.filter.inf_preferred_range_to = parseInt($scope.info.inf_preferred_range_to);
          }
        });

      }
    });
    
    $scope.filter.distance_from = 0;
    $scope.filter.distance_to = 10000;
    $scope.filter.height_from = 0;
    $scope.filter.height_to = 300;


    $scope.filterMaster = angular.copy($scope.filter);


    $scope.shortDate = function(date) {
      return moment(date).format('ll');
    };

    $scope.distanceRangeSlider = {
      options: {
        floor: 0,
        ceil: 1000,
        step: 1,
        minRange: 1
      }
    };

    $scope.heightRangeSlider = {
      options: {
        floor: 0,
        ceil: 300,
        step: 1
      }
    };

    $scope.financeRangeSlider = {
      options: {
        floor: 100,
        ceil: 10000,
        step: 100
      }
    };

    $scope.distance = true;

    $scope.limitDistance = function(distance) {
      if(!distance) {
        $scope.filter.distance_to = 10001;
      } 
      else {
        $scope.filter.distance_to = 10000;
      }
    };

    $scope.loadMore = function(next) {
      $scope.next = false;
      $scope.loadingMore = true;
      var params = {
        link: next,
        params: {
          lat: $scope.user.ulc_lat,
          long: $scope.user.ulc_lng
        }
      };
      FeedService.search(params).then(function(res){
        if(res.success){
          for(var x = 0; x < res.data[0].length; x++)
          {
            $scope.users.push(res.data[0][x]);
          }
          $scope.loading = false;
          if(res.pagination) {
            $scope.pagination = res.pagination;
            if(res.pagination.num_rows > $scope.users.length) {
              $scope.next = res.pagination.links.next;
            }
            else {
              $scope.next = false;
            }
          }
          $scope.loadingMore = false;
        } 
        else {
          $scope.next = false;
          $scope.loading = false;
          $scope.users = [];
          $scope.errMessage = 'Nothing to display.';
          $scope.loadingMore = false;
        }
      });
    };

    FeedService.getSavedSearches().then(function(res){
      if(res.success){
        $scope.searches = res.data[0];
        $scope.loading = false;
      } 
      else {
        $scope.loading = false;
        $scope.message = 'No saved searches.';
      }
    });

    $scope.sortParam = 'usr_approved_date';
    $scope.onTabSelected = function(tab) {
      $scope.source = tab;
      $scope.loading = true;
      $scope.next = false;
      var params = {
        link: URL.API + 'searches/search?is_infinite=true&page=1&page_size=20',
        params: {
          lat: $scope.user.ulc_lat,
          long: $scope.user.ulc_lng,
          distance_from: $scope.filter.distance_from,
          distance_to: $scope.filter.distance_to,
          source: tab
        }
      };
      $scope.next = false;
      if($scope.source == 'recent') {
        params.params.order_by = 'usr_last_login';
        params.params.order = 'desc';
      }
      else if($scope.source == 'nearest') {
        params.params.source = 'nearest';
        params.params.order_by = 'usr_approved_date';
        params.params.order = 'desc';
      }
      else if($scope.source == 'facebook') {
        params.params.order_by = 'usr_approved_date';
        params.params.order = 'desc';
        params.params.source = 'facebook';
      }
      else {
        params.params.order_by = 'usr_approved_date';
        params.params.order = 'desc';
      }
      searchUsers(params);

    };

    $scope.openSavedSearch = function(ev) {
      $mdDialog.show({
        controller: SearchController,
        templateUrl: 'app/search/saved-searches.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        locals: {
          filter: $scope.filter,
          gender: $scope.filter.gender,
          user: $scope.user,
          source: $scope.source,
          ageRangeSlider: $scope.ageRangeSlider,
          distanceRangeSlider: $scope.distanceRangeSlider
        },
        clickOutsideToClose:true,
        fullscreen: true // Only for -xs, -sm breakpoints.
      })
      .then(function(answer) {
        $log.debug(answer);
      }, function() {
        $scope.status = 'You cancelled the dialog.';
      });
    };

    function SearchController($scope, filter, FeedService, user, source, ageRangeSlider, distanceRangeSlider, gender) {
      $scope.filter = filter;
      $scope.user = user;
      $scope.source = source;
      $scope.filter.gender = gender;
      $scope.ageRangeSlider = ageRangeSlider;
      $scope.distanceRangeSlider = distanceRangeSlider;
      FeedService.getSavedSearches().then(function(res){
        if(res.success){
          $scope.searches = res.data[0];
          $scope.loading = false;
        } 
        else {
          $scope.loading = false;
          $scope.message = 'No saved searches.';
        }
      });
        $scope.loadSearch = function(id) {
        $scope.loading = true;
        $scope.message = '';
        $scope.users = [];
        $scope.cancel();
        var params = {
          link: URL.API + 'searches/search?is_infinite=true&page=1&page_size=20',
          params: {
            sch_id: id,
            lat: $scope.user.ulc_lat,
            long: $scope.user.ulc_lng,
            distance_from: $scope.filter.distance_from,
            distance_to: $scope.filter.distance_to,
            source: $scope.source
          }
        };
        FeedService.search(params).then(function(res){
          if(res.success){
            $scope.loading = false;
            $scope.users = res.data[0];
            $rootScope.$broadcast('search', $scope.users);
            if(res.pagination) {
              $scope.pagination = res.pagination;
              if(res.pagination.num_rows > $scope.users.length) {
                $scope.next = res.pagination.links.next
              }
              else {
                $scope.next = false;
              }
            }
          } 
          else {
            $scope.loading = false;
            $scope.message = 'Nothing to display.';
            $scope.users =[];
            $scope.next = false;
            $rootScope.$broadcast('search', $scope.users)
          }
        });
      };

      $scope.$on('search', function(events, args){
        $scope.users = args; //now we've registered!
      });

      $scope.deleteSearch = function(id) {
        var params = {
          sch_id: id
        }
        FeedService.deleteSearch(params).then(function(res){
          $scope.cancel();
          if(res.success) {
            toastr.success('Deleted');
          }
        });
      };
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
    }

    $scope.cancel = function() {
      $mdDialog.cancel();
    };

    $scope.saveSearch = function(filter, ageRangeSlider) {
      if(angular.equals($scope.filterMaster, filter)) {
        toastr.warning('No changes made.');
      }
      else {
        var filters = {
          inf_spending_habits: filter.inf_spending_habits,
          inf_net_worth: filter.inf_net_worth,
          inf_yearly_income: filter.inf_yearly_income,
          usr_gender: _.where(filter.gender, {checked: true}).map(function(item) { return item.value; }),
          usr_interested_age_from: ageRangeSlider.minValue,
          usr_interested_age_to: ageRangeSlider.maxValue,
          inf_ethnicity: _.where(filter.ethnicity, {checked: true}).map(function(item) { return item.value; }),
          inf_body_type: _.where(filter.bodyType, {checked: true}).map(function(item) { return item.value; }),
          inf_height_from: filter.height_from,
          inf_height_to: filter.height_to,
          inf_relationship: _.where(filter.relationship, {checked: true}).map(function(item) { return item.value; }),
          inf_children: _.where(filter.children, {checked: true}).map(function(item) { return item.value; }),
          inf_language: _.where(filter.language, {checked: true}).map(function(item) { return item.value; }),
          inf_education: _.where(filter.education, {checked: true}).map(function(item) { return item.value; }),
          inf_smoke: _.where(filter.smoke, {checked: true}).map(function(item) { return item.value; }),
          inf_drink: _.where(filter.drink, {checked: true}).map(function(item) { return item.value; }),
          lat: $scope.user.ulc_lat,
          long: $scope.user.ulc_lng,
          distance_from: $scope.filter.distance_from,
          distance_to: $scope.filter.distance_to,

        };
        var params = {
          sch_content: filters,
          distance_to: $scope.filter.distance_to
        }

        FeedService.saveSearch(params).then(function(res){
          toastr.success('Saved');
        });
      }

    };

    $scope.reset = function(ev) {
      var confirm = $mdDialog.confirm()
        .title('Reset Filters')
        .textContent('Are you sure you want to reset your filters?')
        .ariaLabel('Confirm Reset')
        .targetEvent(ev)
        .ok('OK')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function() {
        $scope.filter = $scope.filterMaster;
      }, function() {
      });
    };

    $scope.searchByName = function(name) {
      $scope.loading = true;
      $scope.message = '';
      $scope.usr_screen_name = '';
      $scope.users = [];
      var param = {
        usr_screen_name: name,
        inf_spending_habits: $scope.filter.inf_spending_habits,
        inf_net_worth: $scope.filter.inf_net_worth,
        inf_yearly_income: $scope.filter.inf_yearly_income,
        usr_gender: _.where($scope.filter.gender, {checked: true}).map(function(item) { return item.value; }),
        usr_interested_age_from: $scope.ageRangeSlider.minValue,
        usr_interested_age_to: $scope.ageRangeSlider.maxValue,
        usr_interested_country: $scope.filter.usr_interested_country,
        inf_ethnicity: _.where($scope.filter.ethnicity, {checked: true}).map(function(item) { return item.value; }),
        inf_body_type: _.where($scope.filter.bodyType, {checked: true}).map(function(item) { return item.value; }),
        inf_height_from: $scope.filter.height_from,
        inf_height_to: $scope.filter.height_to,
        inf_relationship: _.where($scope.filter.relationship, {checked: true}).map(function(item) { return item.value; }),
        inf_children: _.where($scope.filter.children, {checked: true}).map(function(item) { return item.value; }),
        inf_language: _.where($scope.filter.language, {checked: true}).map(function(item) { return item.value; }),
        inf_education: _.where($scope.filter.education, {checked: true}).map(function(item) { return item.value; }),
        inf_smoke: _.where($scope.filter.smoke, {checked: true}).map(function(item) { return item.value; }),
        inf_drink: _.where($scope.filter.drink, {checked: true}).map(function(item) { return item.value; })
      };
      var params = {
        link: URL.API + 'searches/search?is_infinite=true&page=1&page_size=20',
        params: {
          sch_content: param,
          lat: $scope.user.ulc_lat,
          long: $scope.user.ulc_lng,
          distance_from: $scope.filter.distance_from,
          distance_to: $scope.filter.distance_to
        }
      };
      if($scope.source == 'recent') {
        params.params.order_by = 'usr_last_login';
        params.params.order = 'desc';
      }
      else if($scope.source == 'nearest') {
        params.params.source = 'nearest';
      }
      else if($scope.source == 'facebook') {
        params.params.order_by = 'usr_approved_date';
        params.params.order = 'desc';
        params.params.source = 'facebook';
      }
      else {
        params.params.order_by = 'usr_approved_date';
        params.params.order = 'desc';
      }
      FeedService.search(params).then(function(res){
        if(res.success){
          $scope.users = res.data[0];
          $scope.loading = false;
          if(res.pagination) {
            $scope.pagination = res.pagination;
            if(res.pagination.num_rows > $scope.users.length) {
              $scope.next = res.pagination.links.next
              $scope.pagination = res.pagination;
            }
            else {
              $scope.next = false;
            }
          }
        } 
        else {
          $scope.loading = false;
          $scope.message = 'Nothing to display.';
          $scope.next = false;
          $scope.users = [];
          $scope.pagination.num_rows = 0;
        }
      });
    };

    $scope.searchOnEnter = function(name, evt) {
      evt.stopPropagation();
      if(evt.keyCode == 13) {
        $scope.searchByName(name);
      }
    };

    $scope.search = function(filter, ageRangeSlider) {
      var filters = {
        inf_spending_habits: filter.inf_spending_habits,
        inf_net_worth: filter.inf_net_worth,
        inf_yearly_income: filter.inf_yearly_income,
        usr_gender: _.where(filter.gender, {checked: true}).map(function(item) { return item.value; }),
        usr_interested_age_from: ageRangeSlider.minValue,
        usr_interested_age_to: ageRangeSlider.maxValue,
        usr_interested_country: filter.usr_interested_country,
        inf_ethnicity: _.where(filter.ethnicity, {checked: true}).map(function(item) { return item.value; }),
        inf_body_type: _.where(filter.bodyType, {checked: true}).map(function(item) { return item.value; }),
        inf_height_from: filter.height_from,
        inf_height_to: filter.height_to,
        inf_relationship: _.where(filter.relationship, {checked: true}).map(function(item) { return item.value; }),
        inf_children: _.where(filter.children, {checked: true}).map(function(item) { return item.value; }),
        inf_language: _.where(filter.language, {checked: true}).map(function(item) { return item.value; }),
        inf_education: _.where(filter.education, {checked: true}).map(function(item) { return item.value; }),
        inf_smoke: _.where(filter.smoke, {checked: true}).map(function(item) { return item.value; }),
        inf_drink: _.where(filter.drink, {checked: true}).map(function(item) { return item.value; })
      };

      var params = {
        link: URL.API + 'searches/search?is_infinite=true&page=1&page_size=20',
        params: {
          sch_content: filters,
          lat: $scope.user.ulc_lat,
          long: $scope.user.ulc_lng,
          distance_from: $scope.filter.distance_from,
          distance_to: $scope.filter.distance_to
        }
      };
      if($scope.source == 'recent') {
        params.params.order_by = 'usr_last_login';
        params.params.order = 'desc';
      }
      else if($scope.source == 'nearest') {
        params.params.source = 'nearest';
      }
      else if($scope.source == 'facebook') {
        params.params.order_by = 'usr_approved_date';
        params.params.order = 'desc';
        params.params.source = 'facebook';
      }
      else {
        params.params.order_by = 'usr_approved_date';
        params.params.order = 'desc';
      }
      FeedService.search(params).then(function(res){
        if(res.success){
          $scope.users = res.data[0];
          $scope.loading = false;
          if(res.pagination) {
            $scope.pagination = res.pagination;
            if(res.pagination.num_rows > $scope.users.length) {
              $scope.next = res.pagination.links.next
            }
            else {
              $scope.next = false;
            }
          }
        } 
        else {
          $scope.loading = false;
          $scope.next = false;
          $scope.message = 'Nothing to display.';
          $scope.pagination.num_rows = 0;
          $scope.users = [];
        }
      });
    };

    var callSearch = function(filter, ageRangeSlider) {
      $scope.loading = true;
      var filters = {
        inf_spending_habits: filter.inf_spending_habits,
        inf_net_worth: filter.inf_net_worth,
        inf_yearly_income: filter.inf_yearly_income,
        usr_gender: _.where(filter.gender, {checked: true}).map(function(item) { return item.value; }),
        usr_interested_age_from: ageRangeSlider.minValue,
        usr_interested_age_to: ageRangeSlider.maxValue,
        usr_interested_country: filter.usr_interested_country,
        inf_ethnicity: _.where(filter.ethnicity, {checked: true}).map(function(item) { return item.value; }),
        inf_body_type: _.where(filter.bodyType, {checked: true}).map(function(item) { return item.value; }),
        inf_height_from: filter.height_from,
        inf_height_to: filter.height_to,
        inf_relationship: _.where(filter.relationship, {checked: true}).map(function(item) { return item.value; }),
        inf_children: _.where(filter.children, {checked: true}).map(function(item) { return item.value; }),
        inf_language: _.where(filter.language, {checked: true}).map(function(item) { return item.value; }),
        inf_education: _.where(filter.education, {checked: true}).map(function(item) { return item.value; }),
        inf_smoke: _.where(filter.smoke, {checked: true}).map(function(item) { return item.value; }),
        inf_drink: _.where(filter.drink, {checked: true}).map(function(item) { return item.value; })
      };

      var params = {
        link: URL.API + 'searches/search?is_infinite=true&page=1&page_size=20',
        params: {
          sch_content: filters,
          lat: $scope.user.ulc_lat,
          long: $scope.user.ulc_lng,
          distance_from: $scope.filter.distance_from,
          distance_to: $scope.filter.distance_to
        }
      };
      if($scope.source == 'recent') {
        params.params.order_by = 'usr_last_login';
        params.params.order = 'desc';
      }
      else if($scope.source == 'nearest') {
        params.params.source = 'nearest';
      }
      else if($scope.source == 'facebook') {
        params.params.order_by = 'usr_approved_date';
        params.params.order = 'desc';
        params.params.source = 'facebook';
      }
      else {
        params.params.order_by = 'usr_approved_date';
        params.params.order = 'desc';
      }
      FeedService.search(params).then(function(res){
        if(res.success){
          $scope.users = res.data[0];
          $scope.loading = false;
          if(res.pagination) {
            $scope.pagination = res.pagination;
            if(res.pagination.num_rows > $scope.users.length) {
              $scope.next = res.pagination.links.next
            }
            else {
              $scope.next = false;
            }
          }
        } 
        else {
          $scope.users = [];
          $scope.next = false;
          $scope.pagination = false;
          $scope.loading = false;
          $scope.message = 'Nothing to display.';
          $scope.pagination.num_rows = 0;
        }
      });
    }

    $rootScope.$on('ageDistanceSlider', function() {
      var age = angular.element(document.querySelector('.age-slider'));
      var dist = angular.element(document.querySelector('.distance-slider'));
      age.click();
      dist.click();
    });

    $rootScope.$on('heightSlider', function() {
      var height = angular.element(document.querySelector('.height-slider'));
      height.click();
    });
    $scope.check = function(state, filter, dropdown, ageRangeSlider) {
      if(dropdown == 'general') {
        $timeout(function () {
            $rootScope.$broadcast('ageDistanceSlider');
        }, 200);
      }
      if(dropdown == 'looks') {
        $timeout(function () {
            $rootScope.$broadcast('heightSlider');
        }, 200);
      }
      if(!state) {
        callSearch(filter, ageRangeSlider);
      }
    };
    
    $scope.loadSearch = function(id) {
      $scope.loading = true;
      $scope.message = '';
      $scope.users = [];
      $scope.cancel();
      var params = {
        link: URL.API + 'searches/search?is_infinite=true&page=1&page_size=20',
        params: {
          sch_id: id,
          lat: $scope.user.ulc_lat,
          long: $scope.user.ulc_lng,
          distance_from: $scope.filter.distance_from,
          distance_to: $scope.filter.distance_to
        }
      };
      FeedService.search(params).then(function(res){
        if(res.success){
          $scope.loading = false;
          $scope.users = res.data[0];
          $rootScope.$broadcast('search', $scope.users);
          if(res.pagination) {
            if(res.pagination.num_rows > $scope.users.length) {
              $scope.next = res.pagination.links.next
              $scope.pagination = res.pagination;
            }
            else {
              $scope.next = false;
            }
          }
        } 
        else {
          $scope.loading = false;
          $scope.message = 'Nothing to display.';
          $scope.users =[];
          $scope.next = false;
          $rootScope.$broadcast('search', $scope.users)
        }
      });
    };

    $scope.$on('search', function(events, args){
      $scope.users = args; //now we've registered!
    });

    $scope.deleteSearch = function(id) {
      var params = {
        sch_id: id
      }
      FeedService.deleteSearch(params).then(function(res){
        $scope.cancel();
        if(res.success) {
          toastr.success('Deleted');
        }
      });
    };

  }
})();
