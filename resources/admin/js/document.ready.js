$(document).ready(function() {
	// Enable Bootstrap JS
	$("#page-notification").delay(3000).slideUp();

	$(".alert").alert();
	$('.dropdown-toggle').dropdown();
	$(".collapse").collapse();

	// Select all checkbox for a set of checkboxes
	$(".select-all").click(function() {
		var clicks = $(this).data('clicks');
		selectCheckboxes(!clicks, 'name="' + $(this).data('fields') + '[]"');
		$(this).data("clicks", !clicks)
	});

	$('#confirm-modal').on('hide', function () {
		$('select.select-submit').val('');
	});

	$('button[name="form_mode"]').on('click', function(e) {
		e.preventDefault();
		var btn = $(this);
		if(btn.val() !== '')
		{
			$('#confirm-modal').modal();
			var button = $('#confirm-modal .btn.btn-primary');
			button.unbind('click');
			button.click(function() {
				btn.unbind('click');
				btn.click();
			});
		}
	});

	$('a.indv_delete').on('click', function(e) {
		e.preventDefault();
		var btn = $(this);
		var href = btn.attr("href");
		var error = btn.attr('data-error');

		console.log(error);
		
		$('#confirm-modal').modal();
		var button = $('#confirm-modal .btn.btn-primary');
		button.unbind('click');
		button.click(function() {
			btn.unbind('click');
			btn.click();
			
			$.ajax({
				type: 'post',
				url: href,
				dataType: 'json',
				success: function(response) {
					console.log(response);
					window.location = response.url;
				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {
					console.log(XMLHttpRequest);
					$('#confirm-modal').modal('hide');
					if(error !== 'undefined'){
						window.location.href = error;
					}else{
						show_notification('Something went wrong.');
					}
				}
			});
		});
	});

	$('select.select-submit').change(function() {
		var dropdown = $(this);
		if(dropdown.val() !== '')
		{
			$('#confirm-modal').modal();
			var button = $('#confirm-modal .btn.btn-primary');
			button.unbind('click');
			button.click(function() {
				dropdown.closest("form").submit();
			});
		}
	});

	//wrapping image elements to ahref tagfor lightboxx
	$(".lightbox").each(function(e) {
	    $(this).removeClass('lightbox');
			var img = $(this);
	    var imghref = img.attr('src');
			var imgLrg  = img.data('image');
			if (imgLrg) {
				imghref = imgLrg;
			}
	    $(this).wrap("<a href='"+ imghref +"' data-toggle='lightbox'> </a>");
			$(this).parent().fancybox();
	});


	// tablesorter
	$("table.table-list").each(function() {
		var skip_json = '';
		var i = 0;
		$(this).find('th').each(function() {
			if($(this).hasClass('skip-sort'))
			{
				skip_json += ', "' + i + '": { "sorter": false }';
			}
			i++;
		});
		if(skip_json.length >= 2)
		{
			skip_json = skip_json.substring(2);
		}

		skip_json = '{ "headers": { ' + skip_json + ' } }';
		var params = jQuery.parseJSON(skip_json);

		params.textExtraction = function(node) {
			var sortText = '';
			$(node).find('.sort-data').each(function() {
				sortText = $(this).html();
			});

			if(sortText == '')
			{
				sortText = $(node).html();
			}
			return sortText;
		};

		$(this).tablesorter(params);
	});

	var sidebar_nav = $('.sidebar-nav');
	sidebar_nav.affix();
	sidebar_nav.css('width', sidebar_nav.width());


	// For Redactor
	$('.redactor, .tinymce').redactor({
		minHeight: 200, //pixels
		imageUpload: SITE_URL + 'admin/redactor/do_upload',
		imageUploadCallback: function(image, json) {
			console.log(json);
		}
	});

	$('.btn-disabled').on('click', function(e) {
		e.preventDefault();
		var btn = $(this);
		if( btn.hasClass('btn-table-action') ) {
			btn.next().click();
		}
		return false;
	});

	$('#pag_status').change(function() {
		if($(this).val() == 'published')
		{
			$('#date_published_row').show();
		}
		else
		{
			$('#date_published_row').hide();
		}
	});
	
	$(".sumodate").each(function(e) {
		$(this).sumodate({
            monthFormat: 'mmm', // m – One-digit month,mm – Two-digit month,mmm – Three-letter abbreviation for month, mmmm – Month spelled out in full, e.g. April
            dayFormat: 'dd', //d – One-digit day for days below 10, dd – Two-digit day
           // yearFormat: 'yyyy', //yy – Two-digit year,yyyy – Four-digit year
            maxYear: 'current',//exact year or current
            minYear:'1940' //exact year or current
	    });
    });

	//Only for tables muna
	$('tbody.sortable .order-up, tbody.sortable .order-down').click(function(){
		//Get parent tr
		$tbody = $(this).parents("tbody");
		$tr = $($(this).parents("tr")[0]);

		targetUrl = $tbody.data('target');

		var data = {
			primKey : $tr.data('primkey'),
			otherKey : null
		};

		if($(this).hasClass('order-down'))
		{
			if($tr.next())
			{
				$tr2 = $($tr.next()[0]);
				data.otherKey = $tr2.data('primkey');
				$tr2.after($tr);
			}
			data.order = 'down';
		}
		else
		{
			if($tr.prev())
			{
				$tr2 = $($tr.prev()[0]);
				data.otherKey = $tr2.data('primkey');
				$tr2.before($tr);
			}
			data.order = 'up';
		}

		//AJAX call for element to be ordered before its previous row
		$.ajax({
			type : 'POST',
			url: targetUrl,
			data:data,
			success : function(data){
				console.log(data);
				//alert("Order Saved");
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
			}
		});
	});
});

function submitForm(form)
{
	var $box = $(form).find('.box').first();
	console.log($box);

	var html = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';

	$box.append(html);
	form.submit();
}



// Add / Update a key-value pair in the URL query parameters
function updateUrlParameter(uri, key, value) {
    // remove the hash part before operating on the uri
    var i = uri.indexOf('#');
    var hash = i === -1 ? ''  : uri.substr(i);
         uri = i === -1 ? uri : uri.substr(0, i);
 
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        uri = uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        uri = uri + separator + key + "=" + value;
    }
    return uri + hash;  // finally append the hash as well
}

function show_notification(message){
$('#page-notification').html('<div class="alert alert-danger fade in clearfix" role="alert"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4 class="alert-message"><i class="icon fa fa-ban"></i>Danger</h4>' + message + '</div>');
$('#page-notification').css('display', 'block');
	setTimeout(function(){
		$('#page-notification').fadeOut(3000);
	},7000);
}