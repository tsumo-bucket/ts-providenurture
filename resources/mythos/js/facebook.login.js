$(function(){
	$.ajaxSetup({ cache: true });
	$.getScript('//connect.facebook.net/en_US/all.js', function(){
		FB.init({
		  appId: APP_ID,
		});

		if(!LOGGED_IN){
			isAuth(connected, connect);
		}     
	});
});


/**
 * [isAuth Use this function if you need to check facebook authentication on page load]
 * @param  {callback function}  connected [handles auth response when user account is already linked to fb - defined below]
 * @param  {callback function}  connect   [handles auth respnse when user is not linked to fb - defined below]
 * @return {Boolean}           [description]
 */
function isAuth(connected, connect){
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			// the user is logged in and has authenticated your
			// app, and response.authResponse supplies
			// the user's ID, a valid access token, a signed
			// request, and the time the access token 
			// and signed request each expire
			console.log('connected');

			if(connected && (typeof connected == 'function')) connected(response);

		} else if (response.status === 'not_authorized') {
		// the user is logged in to Facebook, 
		// but has not authenticated your app
			console.log('connect mo muna');

			if(connect && (typeof connect == 'function')) connect();

		} else {
		// the user isn't logged in to Facebook.
			if(connect && (typeof connect == 'function')) connect();

		}
	});
}

// Use this function as click handler for connect to facebook buttons 
/**
 * [fbLogin connects user account to facebook]
 * @param  {function} connected [callback function for handling fb auth results]
 */
function fbLogin(connected){
	FB.login(function(response) {
		if (response.authResponse) {
			console.log('Welcome!  Fetching your information.... ');

			var fbid = response.authResponse.userID;

			if(connected && (typeof connected == 'function')) connected(response);
			
		} else {
		 console.log('User cancelled login or did not fully authorize.');
		}
	}, { scope: 'email, user_website, user_work_history, user_birthday, user_location, publish_actions' });
}

function connect(){
	// You can show a popup/prompt with facebook connect button
}

function connected(initResponse){
	var data = {
		"ajax_request" : 1,
		"uai_api_id" : initResponse.authResponse.userID,
		"uai_api_token" : initResponse.authResponse.accessToken,
	}

		FB.api('/me', function(response) {
		console.log(response);
		data.usr_email = response.email;
		data.usr_fname = response.first_name;
		data.usr_lname = response.last_name;
		data.usr_username = response.username;
		data.usr_birthdate = response.birthday;
		data.usr_image = 'https://graph.facebook.com/' + data.uai_api_id +'/picture?type=large';
		data.usr_thumb = 'https://graph.facebook.com/' + data.uai_api_id +'/picture?type=normal';
		// data.acc_current_location = response.location.name;

		$.ajax({
			// url to controller that handles db related operations
			url: SITE_URL + 'fb_registration/login',
			type: "post",
			dataType: 'json',
			data: data,
			success: function(data){
				console.log('saved');
				if(data.logged_in != false){
					/**
					 * Define where page redirects on fb login success
					 * 
					 */
					window.location = SITE_URL;
				} else {
					alert('Error encountered. Please try again.');
				}
			}
		});
	});
}