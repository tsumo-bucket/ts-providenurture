var overlay = (function () {

   return {
	  
      init: function (path) {
		$('body').append(
			'<div id="page-template-overlay"><img src="'+path+'" /></div>'+
			'<div id="page-template-overlay-control"><select id="page-template-opacity" onchange="overlay.set_opacity(this.value)"></select><div id="page-template-overlay-close" onclick="overlay.toggle()">x</div></div>'
		);
		
		for(var x = 0; x <= 100; x = x + 10){
			$('#page-template-opacity').append("<option value='"+(x/100)+"'>"+x+"</option>");
		}
      },
	  
	  set_opacity: function(level) {
		if(level == 0){
			$('#page-template-overlay').hide();
		} else {
			$('#page-template-opacity').val(level);
			$('#page-template-overlay').css('opacity', level);
			$('#page-template-overlay').show();
		}
	  },
	  
	  toggle: function() {
		$('#page-template-overlay').toggle();
	  },
	  
      close: function (key) {
         $('#page-template-overlay').hide();
      },
	  
       show: function (key) {
         $('#page-template-overlay').show();
      }
   };
})();