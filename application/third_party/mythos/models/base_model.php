<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Base_model extends CI_Model 
{
	protected $table;
	protected $fields;
	protected $searchable_fields;
	protected $order_on;
	private $title_singular;
	private $title_plural;
	
	public function __construct($table = '', $fields = array(), $searchable_fields = array(), $order_on = false) 
	{
		$this->table = $table;
		$this->fields = $fields;
		$this->searchable_fields = $searchable_fields;
		$this->order_on = $order_on;
		
		parent::__construct();
		
		$this->load->library('MYTHOS_Pagination', '', 'pagination');
		$this->load->library('MYTHOS_Form_validation', '', 'validation');
		$this->load->helper('inflector');
		//added karlob for base messages
		$this->title_singular = ucwords(str_replace('_', ' ', $this->table));
		$this->title_plural = plural($this->title_singular);
	}
	
	// Returns the name of the table associated to the Base_model.
	public function get_table()
	{
		return $this->table;
	}
	
	// Returns an array of the fields of the Base_model's table.
	public function get_fields()
	{
		return $this->fields;
	}
	
	protected function filter_data($data, $field_list)
	{
		foreach($data as $key => $value)
		{
			if(!in_array($key, $field_list))
			{
				unset($data[$key]);
			}
		}
		
		return $data;
	}
	
	// Inserts a row in the Base_model's table
	public function create($data, $field_list = array())
	{

		//run validators
		$error = $this->run_validators();
		if (!is_bool($error)) {
			return $this->compose_result(FALSE, $error);
		}

		if(!is_array($data))
		{
			$data = get_object_vars($data);
		}
		
		if(count($field_list) > 0)
		{
			$data = $this->filter_data($data, $field_list);
		}
		
		$valid_data = array();
		$i = 0;
		foreach($this->fields as $field)
		{
			if($i > 0) // Skip primary key from the list of fields
			{
				if(isset($data[$field]))
				{
					$valid_data[$field] = preg_replace('~>\s+<~m', '><', $data[$field]);
				}
			}
			$i++;
		}
		$this->db->insert($this->table, $valid_data);
		$return = array("insert_id"=>$this->db->insert_id());
		return $this->compose_result(TRUE, "New $this->title_singular created.", $return);
	}
	
	// Updates a row in the Base_model's table using the primary key as filter.
	public function update($data, $field_list = array())
	{
		//run validators
		$error = $this->run_validators();
		if (!is_bool($error)) {
			return $this->compose_result(FALSE, $error);
		}

		if(!is_array($data))
		{
			$data = get_object_vars($data);
		}
		
		if(count($field_list) > 0)
		{
			$id = $data[$this->fields[0]];
			$data = $this->filter_data($data, $field_list);
			$data[$this->fields[0]] = $id;
		}
		
		$valid_data = array();
		$i = 0;
		foreach($this->fields as $field)
		{
			if($i > 0)
			{
				if(isset($data[$field]))
				{
					$valid_data[$field] = preg_replace('~>\s+<~m', '><', $data[$field]);
				}
			}
			else
			{
				$this->db->where($field, $data[$field]);
			}
			$i++;
		}
		$this->db->update($this->table, $valid_data);

		$return = array("affected_rows"=>$this->db->affected_rows());
		return $this->compose_result(TRUE, "$this->title_singular updated.", $return);
	}
	
	// Deletes a row in the Base_model's table using the primary key as filter.
	public function delete($id)
	{
		$this->db->where($this->fields[0], $id);
		$this->db->delete($this->table); 
		return $this->db->affected_rows();
	}
	
	// Retrieves a row from the Base_model's table using the primary key as filter.
	public function get_one($id)
	{
		$this->db->where($this->fields[0], $id);
		$query = $this->db->get($this->table); 
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}
	
	/* 
	Returns a result set from the Base_model's table.
	Optional parameter is an associative array to filter the result set.
		Example:
			$params = array(
				'page_category_id' => 1,
				'status' => 'published'
			);
			
			Returns all rows with page_category_id "1" AND status "published"
	*/
	public function get_all($params = array(), $order_by = array())
	{
		if(is_array($params))
		{
			foreach($params as $key=> $value)
			{
				$this->db->where($key, $value);
			}
		}

		//added parameter to order results properly
		if(is_array($order_by) && count($order_by) > 0)
		{
			
			foreach($order_by as $key=> $value)
			{
				//manual hack to avoid ambiguous fields
				$key = str_replace('---', '.', $key);
				$this->db->order_by($key, $value);
			}
		}
		else 
		{
				$this->db->order_by($this->table.".".$this->fields[0], 'desc');
		}

		return $this->db->get($this->table);
	}

	public function search($keyword = "", $params = array(), $order_by = array())
	{
		if(is_array($this->searchable_fields) && trim($keyword) !== "")
		{			

			if(count($this->searchable_fields) > 0)
			{
				$search_string = "(";
				foreach($this->searchable_fields as $value)
				{
					$search_string .= "$value LIKE '%$keyword%' OR ";
				}

				$search_string = rtrim($search_string, ' OR ');
				$search_string .= ")";

				$this->db->where($search_string);
			}
		}

		return $this->get_all($params, $order_by);
	}

	public function reorder($id, $id2 = false, $order)
	{
		$ord = $this->order_on;

		if($ord)
		{
			$row = $this->get_one($id);

			//Get the other element
			if($id2)
			{
				$row2 = $this->get_one($id2);
			}
			else
			{
				if($order == 'up' || $order == 'before')
				{
					$this->db->order_by($this->order_on, 'desc');
					$this->db->where($this->order_on.' <', $row->$ord);
				}
				else
				{
					$this->db->order_by($this->order_on, 'asc');
					$this->db->where($this->order_on.' >', $row->$ord);
				}

				$row2 = $this->get_all()->row();
			}

			if($row2)
			{
				//Reassign the order
				if(!$id2)
				{
					//Swap lang
					$temp = $row2->$ord;
					$row2->$ord = $row->$ord;
					$row->$ord = $temp;

					$this->update($row);
					$this->update($row2);

					return true;
				}

				//Will use this when sorting with drag and drop
				if($order == 'up' || $order == 'before')
				{
					//Add +1 or position of elements after row
					//Shift elements

					$this->db->where($ord.' >=', $row2->$ord);
					$this->db->where($ord.' <', $row->$ord);
					$this->db->set($ord, $ord.' + 1', false);
					$this->db->update($this->table);

					//Get position of replaced
					$row->$ord = $row2->$ord;
					$this->update($row);
				}
				else
				{
					//Add -1 or position of elements after row
					//Shift Elements

					$this->db->where($ord.' >', $row->$ord);
					$this->db->where($ord.' <=', $row2->$ord);
					$this->db->set($ord, $ord.' - 1', false);
					$this->db->update($this->table);

					//Get position of replaced
					$row->$ord = $row2->$ord;
					$this->update($row);
				}

				return true;
			}
		}		

		return false;
	}
	
	public function pagination()
	{
		$parameters = func_get_args();
		$page_uri = array_shift($parameters);
		$function = array_shift($parameters);
		
		$this->pagination->set_function($this->table . '_model', $function, $parameters);
		return $this->pagination->run_query($page_uri);
	}
	
	public function pagination_links()
	{
		return $this->pagination->query_links();
	}
	
	public function join($join_table, $join_type)
	{
		$this->db->join($join_table, $join_table . '.' . $this->fields[0] . ' = ' . $this->table . '.' . $this->fields[0], $join_type);
		return $this->db->get($this->table);
	}

	//added karlob for config-based validators
	public function set_validation_params($function_name){
		$this->config->load("validator/{$this->table}", TRUE);
		$rules = $this->config->item($function_name, 'validator/'.$this->table);
		foreach ($rules as $field_name=>$rule){
			$split = explode('|', $rule,2);
			// echo $split[0]."|".$split[1];
			$this->form_validation->set_rules($field_name, $split[0], $split[1]);
		}
	}

	private function run_validators(){
		// echo 
		if($this->form_validation->run() === FALSE){
			return validation_errors();
		} else {
			return true;
		}
	}

	private function compose_result($success = "", $message = "", $result = "")
	{
		return array('success'=>$success, 'message'=>$message, 'result'=>$result);
	}


}
