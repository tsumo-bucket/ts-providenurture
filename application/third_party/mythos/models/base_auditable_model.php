<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Base_auditable_model extends Base_model 
{
	public function create($data, $fieldlist, $log = '')
	{	
		if (array_key_exists('form_submit', $data)) {
			unset($data['form_submit']);
		}
		if (array_key_exists('submit', $data)) {
			unset($data['submit']);
		}
		if (array_key_exists('check', $data)) {
			unset($data['check']);
		}

		$create = parent::create($data);
		
		if($log == '') {
			$author = $this->session->userdata('acc_username');

			if(!$author) {
				$message['before'] = '';
				$message['after'] = json_encode($data);

				$log['log_date_added'] = format_mysql_datetime();
				$log['log_added_by'] = $this->session->userdata('acc_id');
				$log['log_table'] = parent::get_table();
				$log['log_message'] = $author .' added a new '.parent::get_table().'.';
				$log['log_details'] = serialize($message);

				$this->db->insert('log', $log);
			}
		}
		return $create;
	}
	
	public function update($data, $fieldlist, $log = '')
	{
		if (!is_array($data)) {
	    $data = get_object_vars($data);
	  }
	    
		$fields = parent::get_fields();
		if (array_key_exists('form_submit', $data)) {
			unset($data['form_submit']);
		}

		$before = parent::get_one($data[$fields[0]]);
		$update = parent::update($data);
		$after = parent::get_one($data[$fields[0]]);

		if($log == '') {
			$author = $this->session->userdata('acc_username');
			
			if(!$author) {
				$message['before'] = json_encode($before);
				$message['after'] = json_encode($after);

				$log['log_date_added'] = format_mysql_datetime();
				$log['log_added_by'] = $this->session->userdata('acc_id');
				$log['log_table'] = parent::get_table();
				$log['log_message'] = $author .' modified a '.parent::get_table().'.';
				$log['log_details'] = serialize($message);

				$this->db->insert('log', $log);
			}
		}

		return $update;
	}
	
	public function delete($id)
	{
		$before = parent::get_one($id);
		$delete = parent::delete($id);

		if($delete){
			$author = $this->session->userdata('acc_username');

			if(!$author){
				$table = parent::get_table();
				
				$message['before'] = json_encode($before);
				$message['after'] = '';

				$log['log_date_added'] = format_mysql_datetime();
				$log['log_added_by'] = $this->session->userdata('acc_id');
				$log['log_table'] = parent::get_table();
				$log['log_message'] = $author .' deleted a '.parent::get_table().'.';
				$log['log_details'] = serialize($message);

				$this->db->insert('log', $log);
			}
		}

		return $delete;
	}

}