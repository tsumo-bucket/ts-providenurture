<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Think Sumo Mythos API Model
 *
 * @package        	CodeIgniter
 * @subpackage 		Mythos
 * @category    	Moels
 * @link
 * @author        	Jonathan Conanan. Design collaboration with Karlo Banzuela. Based on Hubert Ursua's Mythos Base_model.
 * @version 		1.0.0
 */

class Api_model extends CI_Model
{
	protected $table;
	protected $fields;
	protected $message;
	protected $error;
	protected $data;
	protected $page;
	protected $page_size;
	protected $is_infinite;
	protected $num_rows;
	private $title_singular;
	private $title_plural;
	private $query_config;
	private $CI;

	public function __construct($table = '', $fields = array())
	{
		$this->table = $table;
		$this->fields = $fields;

		parent::__construct();
		$this->CI =& get_instance();

		$error = false;
		$this->load->library('MYTHOS_Form_validation', '', 'validation');
		$this->load->helper('inflector');
		$this->title_singular = ucwords(str_replace('_', ' ', $this->table));
		$this->title_plural = plural($this->title_singular);

		$this->query_config = $this->CI->config->item('pagination', 'mythos');
	}

	// Returns the name of the table associated to the Base_model.
	public function get_table()
	{
		return $this->table;
	}

	// Returns an array of the fields of the Base_model's table.
	public function get_fields()
	{
		return $this->fields;
	}

	protected function filter_data($data, $field_list)
	{
		foreach($data as $key => $value)
		{
			if(!in_array($key, $field_list))
			{
				unset($data[$key]);
			}
		}

		return $data;
	}

	// Inserts a row in the Base_model's table
	public function create($data, $field_list = array())
	{
		//run validators
		$error = $this->run_validators();

		if (!is_bool($error)) {
			$this->set_error($error, false);
			return $this->compose_result();
		}

		if(!is_array($data))
		{
			$data = get_object_vars($data);
		}

		if(count($field_list) > 0)
		{
			$data = $this->filter_data($data, $field_list);
		}

		$valid_data = array();

		$i = 0;
		foreach($this->fields as $field)
		{
			if($i > 0) // Skip primary key from the list of fields
			{
				if(isset($data[$field]))
				{
					$valid_data[$field] = preg_replace('~>\s+<~m', '><', $data[$field]);
				}
			}
			$i++;
		}

		$table_prefix = substr(($this->get_fields()[0]),0,3);

		//append the created datetime to the insert
		$valid_data[$table_prefix.'_date_created'] = format_mysql_datetime();
		$valid_data[$table_prefix.'_date_modified'] = format_mysql_datetime();

		$this->db->insert($this->table, $valid_data);
		$return = array("insert_id"=>$this->db->insert_id());
		$this->set_data($return, true);
		return $this->compose_result();
	}

	// Updates a row in the Base_model's table using the primary key as filter.
	public function update($data, $field_list = array())
	{
		//run validators
		$error = $this->run_validators();
		if (!is_bool($error)) {
			$this->set_error($error, false);
			return $this->compose_result();
		}

		if(!is_array($data))
		{
			$data = get_object_vars($data);
		}

		if(count($field_list) > 0)
		{
			$id = $data[$this->fields[0]];
			$data = $this->filter_data($data, $field_list);
			$data[$this->fields[0]] = $id;
		}

		$valid_data = array();
		$i = 0;
		foreach($this->fields as $field)
		{
			if($i > 0)
			{
				if(isset($data[$field]))
				{
					$valid_data[$field] = preg_replace('~>\s+<~m', '><', $data[$field]);
				}
			}
			else
			{
				$this->db->where($field, $data[$field]);
			}
			$i++;
		}

		$table_prefix = substr(($this->get_fields()[0]),0,3);

		//append the created datetime to the insert
		$valid_data[$table_prefix.'_date_modified'] = format_mysql_datetime();

		$this->db->update($this->table, $valid_data);

		$return = array("affected_rows"=>$this->db->affected_rows());
		$this->set_data($return, true);
		return $this->compose_result();
	}

	// Deletes a row in the Base_model's table using the primary key as filter.
	public function delete($id)
	{
		$this->db->where($this->fields[0], $id);
		$this->db->delete($this->table);

		$return = array("affected_rows"=>$this->db->affected_rows());
		$this->set_data($return, true);
		return $this->compose_result();
	}

	// Retrieves a row from the Base_model's table using the primary key as filter.
	public function get_one($id)
	{
		$this->db->where($this->table.'.'.$this->fields[0], $id);
		$query = $this->db->get($this->table);
		if($query->num_rows() > 0)
		{
			$this->set_data($query->row(),true);
			return $this->compose_result();
		}
		else
		{
			$this->set_error(array('message'=>'Row Not Found'));
			return $this->compose_result();
		}
	}

	/*
	Returns a result set from the Base_model's table.
	Optional parameter is an associative array to filter the result set.
		Example:
			$params = array(
				'page_category_id' => 1,
				'status' => 'published'
			);

			Returns all rows with page_category_id "1" AND status "published"
	*/
	public function get_all($params = array(), $order_by = array())
	{
		if(is_array($params))
		{
			foreach($params as $key=> $value)
			{
				if($key==='page')
				{
					$this->page = $value;
					if($this->page<0)
					{
						$this->page = 0;
					}
				}
				elseif($key==='page_size')
				{
					$this->page_size = $value;
				}
				elseif($key==='is_infinite')
				{
					if($value==='true')
					{
						$this->is_infinite = true;
					}
					else
					{
						$this->is_infinite = false;
					}
				}
				else
				{
					$key = str_replace('---', '.', $key);
					$this->db->where($key, $value);
				}

			}
		}

		//added parameter to order results properly
		if(is_array($order_by) && count($order_by) > 0)
		{
			foreach($order_by as $key=> $value)
			{
				//manual hack to avoid ambiguous fields
				$key = str_replace('---', '.', $key);
				$this->db->order_by($key, $value);
			}
		}
		else
		{
			$this->db->order_by($this->table.".".$this->fields[0], 'desc');
		}

		if($this->page_size!=null&&$this->page!=null)
		{
			$this->db->limit($this->page_size, ($this->page-1)*$this->page_size);
		}
		$return = $this->db->get($this->table);

		if($this->page_size!=null&&$this->page!=null)
		{
			//parse the last query db and remove the limit
			$query_string = $this->db->last_query();
			$query_array = explode("\n", $query_string);
			unset($query_array[count($query_array) - 1]);
			$query_string = implode($query_array); //this is the query string without the limit
			$this->num_rows = $this->db->query($query_string)->num_rows;
		}

		if($return->num_rows()==0)
		{
			$this->set_error(array('message'=>'No records found.'));
		}

		$this->db->flush_cache();

		$this->set_data($return->result(),true);
		$this->pagination();
		return $this->compose_result();
	}

	public function join($join_table, $join_type)
	{
		$this->db->join($join_table, $join_table . '.' . $this->fields[0] . ' = ' . $this->table . '.' . $this->fields[0], $join_type);
		$return = $this->db->get($this->table);
		$this->set_data($return->result(),true);
		return $return;
	}

	protected function run_validators()
	{
		if($this->form_validation->run() === FALSE){
			if(validation_errors()==="")
			{
				return true;
			}
			else
			{
				return validation_errors();
			}
		}
		else
		{
			return true;
		}
	}

	protected function compose_result()
	{
		if($this->message()===null)
		{
			$this->set_message('No message specified');
			$this->set_error(array('message'=>'No message specified'));
		}
		if ($this->error===null)
		{
			if($this->pagination()!=false)
			{
				return array('success'=>true, 'message'=>$this->message(), 'data'=>$this->data(), 'pagination' => $this->pagination());
			}
			else
			{
				return array('success'=>true, 'message'=>$this->message(), 'data'=>$this->data());
			}
		}
		else
		{
			return array('success'=>false, 'message'=>$this->message(), 'error'=>$this->error());
		}
	}

	public function set_message($message = null)
	{
		// accepts any data type (object, array, string, number). the last message is the one that is followed
		$this->message = $message;
	}

	public function message()
	{
		return $this->message;
	}

	public function set_error($error = false, $custom = true)
	{
		if($custom===false)
		{
			$validation_errors = $this->form_validation->get_error_array();
			foreach ($validation_errors as $key => $value)
			{
				$this->error[$key] = $value;
			}
		}
		else
		{
			if(is_array($error))
			{
				foreach ($error as $key => $value)
				{
					$this->error[$key] = $value;
				}
			}
		}
	}

	public function infinite_pagination()
	{
		$query_string = $_SERVER['QUERY_STRING'];
		$query_string = explode('&',$query_string);
		$next_page = $_GET['page'] + 1;
		$pointer = 0;
		foreach($query_string as $segment)
		{
			$key_value = explode('=',$segment);
			if($key_value[0]=='page')
			{
				break;
			}
			$pointer = $pointer +1;
		}
		unset($query_string[$pointer]);

		$next_url = current_url().'?'.implode('&',$query_string)."&page=".$next_page;

		$links = array(
			'next' => $next_url
			);
		return $links;
	}

	public function default_pagination()
	{
		$html_link = $this->query_config['full_tag_open'];
		$links_count = 3;
		$query_string = $_SERVER['QUERY_STRING'];
		$query_string = explode('&',$query_string);
		$pointer = 0;
		foreach($query_string as $segment)
		{
			$key_value = explode('=',$segment);
			if($key_value[0]=='page')
			{
				break;
			}
			$pointer = $pointer +1;
		}
		unset($query_string[$pointer]);

		$links = array();

		$before_link_count = $this->page - $this->get_before();
		if($this->page>1)
		{
			$links['first'] = current_url().'?'.implode('&',$query_string)."&page=1";
			$html_link .= $this->query_config['first_tag_open'] . '<a href="' . $links['first'] . '">&larr; First</a>' . $this->query_config['first_tag_close'];
			$links['back'] = current_url().'?'.implode('&',$query_string)."&page=".($this->page-1);

			$html_link .= $this->query_config['prev_tag_open'] . '<a href="' . $links['back'] . '">&larr; Prev</a>' . $this->query_config['prev_tag_close'];
		}

		if($this->num_rows > $this->page_size)
		{
			$tempcount = 0;
			for ($x=$before_link_count-1; $x<$this->page; $x++)
			{
				if(($this->page - $x) < $links_count+1)
				{
					$page = $x;
					$links["page-".$page] = current_url().'?'.implode('&',$query_string)."&page=".$page;

					$html_link .= $this->query_config['num_tag_open'] . '<a href="' . $links["page-".$page] . '">' . $x . '</a>' . $this->query_config['num_tag_close'];
				}
				else
				{
					continue;
				}
			}
		}

		$active_page = $this->page;

		$links['page-'.$active_page.'-active'] = current_url().'?'.implode('&',$query_string)."&page=".$_GET['page'];

		$html_link .= $this->query_config['cur_tag_open'] . $active_page . $this->query_config['cur_tag_close'];

		$count = $this->page;
		$tempcount = 0;
		for ($x=$count; $x<ceil($this->num_rows / $this->page_size); $x++)
		{
			$page = $x + 1;
			$links["page-".$page] = current_url().'?'.implode('&',$query_string)."&page=".$page;

			$html_link .= $this->query_config['num_tag_open'] . '<a href="' . $links["page-".$page] . '">' . $x . '</a>' . $this->query_config['num_tag_close'];

			$tempcount = $tempcount + 1;
			if($tempcount == $links_count) { break; }
		}
		$last_page = $this->get_last_page();
		if($active_page!=$last_page&&$this->num_rows>$this->page_size)
		{
			$links['next'] = current_url().'?'.implode('&',$query_string)."&page=".($this->page+1);
			$html_link .= $this->query_config['next_tag_open'] . '<a href="' . $links['next'] . '">&rarr; Next</a>' . $this->query_config['next_tag_close'];

			$links['last'] = current_url().'?'.implode('&',$query_string)."&page=".$last_page;

			$html_link .= $this->query_config['last_tag_open'] . '<a href="' . $links['last'] . '">&rarr; Last</a>' . $this->query_config['last_tag_close'];
		}
		$html_link  .= $this->query_config['full_tag_close'];
		$links['html_links'] = $html_link;
		return $links;
	}

	public function get_before()
	{

		//returns the number of results that are before the current query set
		$this->page;
		$this->page_size;
		$this->num_rows;
		$belong_set = ($this->page * $this->page_size) / $this->page_size;

		if($this->page_size > $this->num_rows)
		{
			return 0;
		}
		else
		{
			return $belong_set - 2;
		}
	}

	public function get_after()
	{
		//returns the number of results that are before the current query set
		$this->page;
		$this->page_size;
		$this->num_rows;
		$num_sets = ceil($this->num_rows / $this->page_size) - 1;
		$belong_set = ($this->page * $this->page_size) / $this->page_size;

		if($this->page_size > $this->num_rows)
		{
			return 0;
		}
		else
		{
			if($num_sets-$this->page >= 0)
			{
				return $num_sets-$this->page;
			}
			else
			{
				return 0;
			}
		}
	}

	private function get_last_page()
	{
		if($this->num_rows>$this->page_size)
		{
			return ceil($this->num_rows / $this->page_size);
		}
		else
		{
			return 0;
		}
	}

	public function pagination()
	{
		if(isset($this->page)&&isset($this->page_size))
		{
			$pagination = array(
					'num_rows' => $this->num_rows,
					'page' => $this->page,
					'page_size' => $this->page_size,
					'is_infinite' => $this->is_infinite
				);
			if($this->is_infinite==true)
			{
				$pagination['links'] = $this->infinite_pagination();
			}
			else
			{
				$pagination['links'] = $this->default_pagination();
			}
			return $pagination;
		}
		else
		{
			return false;
		}
	}

	public function pagination_api()
	{
		$parameters = func_get_args();
		$page_uri = array_shift($parameters);
		$function = array_shift($parameters);
		$this->pagination->set_function($this->table . '_model', $function, $parameters);

		return $this->pagination->run_api_query($page_uri);
	}

	public function error()
	{
		return $this->error;
	}

	public function set_data($data = null, $append = true)
	{
		if($append===false)
		{
			$this->data = $data;
		}
		else
		{
			$this->data[] = $data;
		}
	}

	public function data()
	{
		return $this->data;
	}
}