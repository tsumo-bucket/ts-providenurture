<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require_once(SYSDIR . '/libraries/Upload.php');

/*****

This library handles basic file uploads

*****/
class MYTHOS_Upload extends CI_Upload
{
	private $CI;

	private $website;
	private $production;
	private $path;
	
	public function __construct() 
	{
		$this->CI =& get_instance();
		$this->production = true;
		if($this->production) {
			$this->path = 'pn-live/';
		}
		else {
			$this->path = 'pn-beta/';
		}

	}
	
	/**
	* params
	* field - input type file name attribute
	* path - path where the file will be uploaded. Default is uploads folder in root
	* extensions - allowed extensions. Default is *
	* 
	*
	* returns the upload data
	*
	*/

	
	
	public function file($field = false, $path = false, $extensions = '*')
	{
		//Check if field is set
		if(!$field) {
			$error = array('error' => 'Specify File field');
			return $error;
		}
		
		if(!file_exists($_FILES[$field]['tmp_name']) || !is_uploaded_file($_FILES[$field]['tmp_name'])) {
			$error = array('error' => 'No file attached');
			return $error;
		}
		
		$upload_config = array();
		
		if(!$path)
		{
			$upload_config['upload_path'] = FCPATH . 'uploads/';
		}
		else
		{
			$upload_config['upload_path'] = rtrim($path, '/\\') . '/';
		}
		
		$upload_config['allowed_types'] = $extensions;
		$upload_config['max_size']	= '0';
		$upload_config['encrypt_name'] = true;
		$upload_config['overwrite'] = false;
		$this->CI->upload->initialize($upload_config);

		if(!$this->CI->upload->do_upload($field))
		{
			$error = array('error' => $this->CI->upload->display_errors());
			$d = $this->CI->upload->data();
			return $error;
		}
		else
		{
			$data = $this->CI->upload->data();
			// var_dump($data);
			// die();

			if(strpos(base_url(), $this->website) !== false){
				$this->CI->load->library('S3_upload');

				// $tmp_path  = $data['full_path'];
				$file_name = $data['file_name'];
				$file_type = $data['file_type'];
				$s3_path  = ltrim(rtrim($path, '/\\'), '.') . '/' . $file_name;
				$s3_path  = ltrim($s3_path, '/\\');
				$tmp_path  = $s3_path;

				echo $tmp_path;

				// Sanitize path
				$s3_path = (substr($s3_path, 0, 3) === '../') ? substr($s3_path, 3) : $s3_path;
				$s3_path = (substr($s3_path, 0, 2) === './')  ? substr($s3_path, 2) : $s3_path;
				$s3_path = $this->path . $s3_path;
				$tmp_path = $s3_path;

				$result = $this->CI->s3_upload->upload($tmp_path, $s3_path, $file_type);

				unlink($s3_path);
				// die();
			}

			return $data;
		}
	}

	public function _image($filename, $path, $image, $decoded = false)
	{
		if(!$decoded){
			file_put_contents($path.$filename.'.png', base64_decode($image));
		}else{
			file_put_contents($path.$filename.'.png', $image);
		}

		if(strpos(base_url(), $this->website) !== false){
			$this->CI->load->library('S3_upload');

			$file_name = $filename.'.png';
			$file_type = 'image/png';
			$s3_path   = $this->path . $file_name;
			$tmp_path  = $s3_path;

			// Sanitize path
			$s3_path = (substr($s3_path, 0, 3) === '../') ? substr($s3_path, 3) : $s3_path;
			$s3_path = (substr($s3_path, 0, 2) === './')  ? substr($s3_path, 2) : $s3_path;

			$result = $this->CI->s3_upload->upload($tmp_path, $s3_path, $file_type);

			unlink($s3_path);
			
			return $result;
		}
	}

	public function _imageJPG($filename, $path, $image, $decoded = false)
	{
			
		if(!$decoded){
			file_put_contents($path.$filename, base64_decode($image));
		}else{
			file_put_contents($path.$filename, $image);
		}

		if(strpos(base_url(), $this->website) !== false){
			$this->CI->load->library('S3_upload');

			$file_name = $filename;
			$file_type = 'image/jpeg';
			$s3_path   = $this->path . $file_name;
			$tmp_path  = $s3_path;

			// Sanitize path
			$s3_path = (substr($s3_path, 0, 3) === '../') ? substr($s3_path, 3) : $s3_path;
			$s3_path = (substr($s3_path, 0, 2) === './')  ? substr($s3_path, 2) : $s3_path;

			//beta
			// $s3_path = 'sf-beta/' . $s3_path;
			//live
			$s3_path = 'sf-live/' . $s3_path;
			$result = $this->CI->s3_upload->upload($tmp_path, $s3_path, $file_type);
			// var_dump($result);
			// die();

			unlink($path.$filename);
			
			return $result;
		}
	}

	public function _imagePNG($filename, $path, $image, $decoded = false)
	{
		if(!$decoded){
			file_put_contents($path.$filename.'.png', base64_decode($image));
		}else{
			file_put_contents($path.$filename.'.png', $image);
		}

		if(strpos(base_url(), $this->website) !== false){
			$this->CI->load->library('S3_upload');

			$file_name = $filename.'.png';
			$file_type = 'image/png';
			$s3_path   = $this->path . $file_name;
			$tmp_path  = $s3_path;

			// Sanitize path
			$s3_path = (substr($s3_path, 0, 3) === '../') ? substr($s3_path, 3) : $s3_path;
			$s3_path = (substr($s3_path, 0, 2) === './')  ? substr($s3_path, 2) : $s3_path;

			$result = $this->CI->s3_upload->upload($tmp_path, $s3_path, $file_type);

			// unlink($s3_path);
			
			return $result;
		}
	}

	public function _thumb($filename, $path, $image)
	{
		if(strpos(base_url(), $this->website) !== false){
			$this->CI->load->library('S3_upload');

			$file_name = $filename.'0.png';
			$file_type = 'image/png';
			$s3_path   = $this->path . $file_name;
			$tmp_path  = $s3_path;

			// Sanitize path
			$s3_path = (substr($s3_path, 0, 3) === '../') ? substr($s3_path, 3) : $s3_path;
			$s3_path = (substr($s3_path, 0, 2) === './')  ? substr($s3_path, 2) : $s3_path;

			$result = $this->CI->s3_upload->upload($tmp_path, $s3_path, $file_type);
			
			unlink($s3_path);
			
			return $result;
		}
	}
}