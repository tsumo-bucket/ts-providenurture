<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'third_party/google-api-php-client/src/Google_Client.php';
require_once 'third_party/google-api-php-client/src/contrib/Google_YouTubeService.php';


class MYTHOS_Youtube
{
    private $CI;
    private $youtube;
    private $client;

    public function __construct() 
    {
        $this->CI =& get_instance();
        $params = $this->CI->config->item('youtube', 'mythos');
        $this->client = new Google_Client();
        $this->client->setClientId($params['google_client_id']);
        $this->client->setClientSecret($params['google_client_secret']);
        $this->client->setRedirectUri(current_url());

        $this->youtube = new Google_YoutubeService($this->client);
    }

    public function initialize(){
        if ($this->CI->session->userdata('yt_access_token')){
            $this->client->setAccessToken($this->CI->session->userdata('yt_access_token'));
            return;
        }

        if (isset($_GET['code'])) {
          $this->client->authenticate();
          $_SESSION['token'] = $this->client->getAccessToken();
        }
        if (isset($_SESSION['token'])) {
          $this->client->setAccessToken($_SESSION['token']);
          //SAVE THIS ACCESS TOKEN in CI session
          $this->CI->session->set_userdata('yt_access_token', $this->client->getAccessToken());
        }

        if (!$this->client->getAccessToken()) {
          redirect($this->client->createAuthUrl());
        }
    }

    //$video_path = video-url path
    //$params['title'] = Video Title
    //$params['description'] = Video Description
    //$params['tags'] = array("tag1", "tag2")
    //$params['category_id'] = Numeric video cat ID: https://developers.google.com/youtube/v3/docs/videoCategories/list 
    //$params['privacy'] = "public", "private" and "unlisted"
    //return youtube video ID
    public function upload($video_path, $params = array()){
        // Check if access token successfully acquired
        $vid_id = "";
        if ($this->client->getAccessToken()) {
          try{
            // REPLACE with the path to your file that you want to upload
            $videoPath = $video_path;

            // Create a snipet with title, description, tags and category id
            $snippet = new Google_VideoSnippet();
            $snippet->setTitle($params['title']);
            $snippet->setDescription($params['description']);
            $snippet->setTags($params['tags']);

            // Numeric video category. See
            // https://developers.google.com/youtube/v3/docs/videoCategories/list 
            $snippet->setCategoryId($params['category_id']);

            // Create a video status with privacy status. Options are "public", "private" and "unlisted".
            $status = new Google_VideoStatus();
            $status->privacyStatus = $params['privacy'];

            // Create a YouTube video with snippet and status
            $video = new Google_Video();
            $video->setSnippet($snippet);
            $video->setStatus($status);

            // Size of each chunk of data in bytes. Setting it higher leads faster upload (less chunks,
            // for reliable connections). Setting it lower leads better recovery (fine-grained chunks)
            $chunkSizeBytes = 1 * 1024 * 1024;

            // Create a MediaFileUpload with resumable uploads
            $media = new Google_MediaFileUpload('video/*', null, true, $chunkSizeBytes);
            $media->setFileSize(filesize($videoPath));

            // Create a video insert request
            $insertResponse = $this->youtube->videos->insert("status,snippet", $video,
                array('mediaUpload' => $media));

            $uploadStatus = false;

            // Read file and upload chunk by chunk
            $handle = fopen($videoPath, "rb");
            while (!$uploadStatus && !feof($handle)) {
              $chunk = fread($handle, $chunkSizeBytes);
              $uploadStatus = $media->nextChunk($insertResponse, $chunk);
            }

            fclose($handle);

            $vid_id =  $uploadStatus['id'];

          } catch (Google_ServiceException $e) {
            echo $e; die();
          } catch (Google_Exception $e) {
            echo $e; die();
          }
          $_SESSION['token'] = $this->client->getAccessToken();
        } else {
            echo "You must be authenticated with youtube first"; die();
        }

        $this->CI->session->unset_userdata('yt_access_token');
        return $vid_id;
    }

}
