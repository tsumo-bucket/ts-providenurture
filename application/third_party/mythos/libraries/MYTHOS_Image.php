<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require_once(SYSDIR . '/libraries/Upload.php');
require_once(SYSDIR . '/libraries/Image_lib.php');
require_once('third_party/image_moo/image_moo.php');

class MYTHOS_Image extends CI_Upload
{
	private $CI;
	private $image_moo;
	private $bgcolor = "";
	
	public function __construct() 
	{
		$this->CI =& get_instance();
		$this->image_moo = new Image_moo();
		$this->CI->mythos->library('upload');
	}
	
	/**
	*
	* Sets bg color of thumbnail. Set to empty string to disable
	*
	* params
	* hex - hex color value
	*
	*
	*/
	public function set_bgcolor($hex = false){
		if($hex){
			$this->bgcolor = $hex;
		}
		else
		{
			$this->bgcolor = "";
		}
	}
	
	/**
	*
	* params
	* field - input type file name attribute
	* path - path where the file will be uploaded. Default is uploads folder in root
	* extensions - allowed extensions. Default are gif, png, jpg, jpeg
	*
	*
	*/
	public function upload($field = false, $path = false, $extensions = '*')
	{
		return $this->CI->upload->file($field, $path, $extensions);
	}


	public function _upload($filename, $path, $image, $decoded = false)
	{
		return $this->CI->upload->_image($filename, $path, $image, $decoded);
	}

	public function _uploadJPG($filename, $path, $image, $decoded = false)
	{
		return $this->CI->upload->_imageJPG($filename, $path, $image, $decoded);
	}

	public function _uploadPNG($filename, $path, $image, $decoded = false)
	{
		return $this->CI->upload->_imagePNG($filename, $path, $image, $decoded);
	}

	public function _direct($filename, $path, $image)
	{
		return $this->CI->upload->_thumb($filename, $path, $image);
	}
	
	/**
	*
	* params
	* image - fullpath or relative path
	* thumbs - array of image specs thumb = array( 'width'=>width, 'height'=>height, 'filename'=>filename )
	*
	*
	*/
	public function create_thumbs( $image = false, $thumbs = array(), $spill = true )
	{
		if($image){
			if(file_exists ( $image )) {
				$paths = array();
				foreach($thumbs as $key => $thumb){
					$ext = pathinfo($image, PATHINFO_EXTENSION);
					$directory = dirname( $image ).'/';
					$filename = basename( $image, '.'.$ext );
					
					//No Filename
					if( !isset($thumb['filename']) ){
						$path = $directory. $filename . $key. "." . $ext;
					}
					else {
						//Filename parameter has extension
						if( strpos($thumb['filename'], '.') ){
							$path = $directory.$thumb['filename'];
						}
						else
						{
							$path = $directory.$thumb['filename']. "." .$ext;
						}
					}
					
					$paths[$key] = $path;

					if(!$spill)
					{
						//Resize the image
						$this->image_moo
						->load( $image )
						->resize_crop($thumb['width'],$thumb['height'])
						->save( $path, TRUE )->clear();

					}
					else
					{
						if($this->bgcolor && $this->bgcolor != "")
						{
							//Resize the image
							$this->image_moo
							->load( $image )
							->set_background_colour($this->bgcolor)
							->resize($thumb['width'],$thumb['height'], ($this->bgcolor != ""))
							->save( $path, TRUE )->clear();

						}
						else
						{
							//Resize the image
							$this->image_moo
							->load( $image )
							->resize($thumb['width'],$thumb['height'], TRUE, TRUE)
							->save( $path, TRUE )->clear();

						}

					}
				}

				return $paths;
			}
			else {
				$error = array('error' => 'File does not exist');
				$d = $this->CI->upload->data();
				return $error;
			}
		}
		else {
			$error = array('error' => 'No file specified');
			$d = $this->CI->upload->data();
			return $error;
		}
	}
	
	/**
	*
	* params
	* coords - array( 'x1'=> x1, 'y1'=> y1, 'x2'=> x2, 'y2'=> y2 )
	*
	*
	*/
	public function crop( $image, $coords = false )
	{		
		if(!$coords){
			$error = array('error' => 'Specify crop coordinates');
			$d = $this->CI->upload->data();
			return $error;
		}
		
		if($image){
			if(file_exists ( $image )) {
				
				$this->image_moo
				->load($image)
				->set_jpeg_quality(100)
				->crop($coords['x1'], $coords['y1'], $coords['x2'], $coords['y2'])
				->save($image, TRUE);
				
				return true;
			}
			else {
				$error = array('error' => 'File does not exist');
				$d = $this->CI->upload->data();
				return $error;
			}
		}
		else {
			$error = array('error' => 'No file specified');
			$d = $this->CI->upload->data();
			return $error;
		}
	}
}