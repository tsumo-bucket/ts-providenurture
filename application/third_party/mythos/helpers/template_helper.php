<?php
/*
Echoes the content of the template parameter based on the specified position.
Prints blank ("") if template parameter has no content.
*/
function template($position, $type = false)
{
	$CI =& get_instance();
	if ($type) {
		$position = $position . "_" . $type;
	}
	return $CI->template->get($position);
}

function template_header($template_folder)
{
	$CI =& get_instance();
	return $CI->template->show($template_folder, 'template-header');
}

function template_footer($template_folder)
{
	$CI =& get_instance();
	return $CI->template->show($template_folder, 'template-footer');
}

/*
Shorthand function for the base URL of resources folder.
*/
function res_url($path = '')
{
	$CI =& get_instance();
	$CI->load->helper('url');

	if(!empty($path))
	{
		return base_url('resources/' . ltrim($path, '/'));
	}
	else
	{
		return base_url('resources') . '/';
	}
}

/*
Shorthand function for the base URL of resources folder.
*/
function admin_url($path = '')
{
	$CI =& get_instance();
	$CI->load->helper('url');

	if(!empty($path))
	{
		return site_url('admin/' . ltrim($path, '/'));
	}
	else
	{
		return site_url('admin') . '/';
	}
}

/*
Shorthand function for the base URL of resources folder.
*/
function api_url($path = '')
{
	$CI =& get_instance();
	$CI->load->helper('url');

	if(!empty($path))
	{
		return site_url('api/' . ltrim($path, '/'));
	}
	else
	{
		return site_url('api') . '/';
	}
}


/*
DEPRECATED: Old res_url() function
*/
function resources_url($path = '')
{
	return res_url($path);
}

function uri_css_class()
{
	$CI =& get_instance();
	$template_folder = get_module();
	$s2 = $CI->uri->segment(2);
	$s3 = $CI->uri->segment(3, 'index');

	if($s2 == '' && $CI->uri->segment(1) != '')
	{
		if($CI->uri->segment(1) != $template_folder){
			$s2 = $CI->uri->segment(1);
			$s3 = 'index';
		}
		else {
			$s2 = 'index';
			$s3 = 'index';
		}
	}
	elseif($CI->uri->segment(1) == '')
	{
		$s2 = 'index';
		$s3 = 'index';
	}
	elseif($s2 != '' && $CI->uri->segment(1) != '' && $s3 == "")
	{
		$s3 = $s2;
		$s2 = $CI->uri->segment(1);

	}

	return 'page-' . $s2 . ' page-' . $s2 . '-' . $s3;
}

function get_module()
{
	$RTR =& load_class('Router', 'core');
	return trim($RTR->fetch_directory(), '/');
}


function client_url($path = ''){
	$CI =& get_instance();
	$CI->load->helper('url');
	$client_url = $CI->config->item('client_url');

	if(!empty($path))
	{
		return $client_url . ltrim($path, '/');
	}
	else
	{
		return $client_url;
	}
}