<?php
/*
Converts any string to a MySQL date format.
If no string is passed, it will return the current date.
*/
function format_mysql_date($date = null, $timezone = 'UTC')
{
	$date = strtotime($date);
	if($date != null)
	{
		$timestamp = $date;
	}
	else
	{
		$timestamp = mktime();
	}
	$current_tz = date_default_timezone_get();
	date_default_timezone_set($timezone);
	$output = date('Y-m-d', $timestamp);
	date_default_timezone_set($current_tz);
	return $output;
}

/*
Converts any string to a MySQL time format.
If no string is passed, it will return the current time.
*/
function format_mysql_time($time = null, $timezone = 'UTC')
{
	$time = strtotime($time);
	if($time != null)
	{
		$timestamp = $time;
	}
	else
	{
		$timestamp = mktime();
	}
	$current_tz = date_default_timezone_get();
	date_default_timezone_set($timezone);
	$output = date('H:i:s', $timestamp);
	date_default_timezone_set($current_tz);
	return $output;
}

/*
Converts any string to a MySQL datetime format.
If no string is passed, it will return the current date.
*/
function format_mysql_datetime($datetime = null, $timezone = 'UTC')
{
	$datetime =strtotime($datetime);
	if($datetime != null )
	{
		$timestamp = $datetime;
	}
	else
	{
		$timestamp = mktime();
	}
	$current_tz = date_default_timezone_get();
	date_default_timezone_set($timezone);
	$output = date('Y-m-d H:i:s', $timestamp);
	date_default_timezone_set($current_tz);
	return $output;
}

/*
Converts any string to a custom date format, often used for front-end display.
*/
function format_date($date = 'now', $format = '', $timezone = '')
{
	$date = strtotime($date);
	if($date != "" && $date >= 0) {
		if($format == '')
		{
			$CI =& get_instance();
			$config = $CI->config->item('format_helper', 'mythos');
			$format = $config['date'];
		}
		
		$current_tz = date_default_timezone_get();
		if($timezone == '')
		{
			$timezone = $current_tz;
		}
		date_default_timezone_set($timezone);
		$output = date($format, $date);
		date_default_timezone_set($current_tz);
		return $output;
	}
	else
	{
		return "n/a";
	}
}

/*
Converts any string to a custom time format, often used for front-end display.
*/
function format_time($time = 'now', $format = '', $timezone = '')
{
	$time = strtotime($time);
	if ($time != ""  && $time >= 0) {
		if($format == '')
		{
			$CI =& get_instance();
			$config = $CI->config->item('format_helper', 'mythos');
			$format = $config['time'];
		}
		
		$current_tz = date_default_timezone_get();
		if($timezone == '')
		{
			$timezone = $current_tz;
		}
		date_default_timezone_set($timezone);
		$output = date($format, $time);
		date_default_timezone_set($current_tz);
		return $output;
	}
	else 
	{
		return "n/a";
	}
}

/*
Converts any string to a custom datetime format, often used for front-end display.
*/
function format_datetime($datetime = 'now', $format = '', $timezone = '')
{
	$datetime = strtotime($datetime);
	if ($datetime != "" && $datetime >= 0) {
		if($format == '')
		{
			$CI =& get_instance();
			$config = $CI->config->item('format_helper', 'mythos');
			$format = $config['datetime'];
		}
		
		$current_tz = date_default_timezone_get();
		if($timezone == '')
		{
			$timezone = $current_tz;
		}
		date_default_timezone_set($timezone);
		$output = date($format, $datetime);
		date_default_timezone_set($current_tz);
		return $output;
	}
	else 
	{
		return "n/a";
	}
}

//gets facebook-like readible time string
function get_time_ago($time_stamp)
{
    $time_difference = strtotime('now') - $time_stamp;

    if ($time_difference >= 60 * 60 * 24 * 365.242199)
    {
        /*
         * 60 seconds/minute * 60 minutes/hour * 24 hours/day * 365.242199 days/year
         * This means that the time difference is 1 year or more
         */
        return get_time_ago_string($time_stamp, 60 * 60 * 24 * 365.242199, 'year');
    }
    elseif ($time_difference >= 60 * 60 * 24 * 30.4368499)
    {
        /*
         * 60 seconds/minute * 60 minutes/hour * 24 hours/day * 30.4368499 days/month
         * This means that the time difference is 1 month or more
         */
        return get_time_ago_string($time_stamp, 60 * 60 * 24 * 30.4368499, 'month');
    }
    elseif ($time_difference >= 60 * 60 * 24 * 7)
    {
        /*
         * 60 seconds/minute * 60 minutes/hour * 24 hours/day * 7 days/week
         * This means that the time difference is 1 week or more
         */
        return get_time_ago_string($time_stamp, 60 * 60 * 24 * 7, 'week');
    }
    elseif ($time_difference >= 60 * 60 * 24)
    {
        /*
         * 60 seconds/minute * 60 minutes/hour * 24 hours/day
         * This means that the time difference is 1 day or more
         */
        return get_time_ago_string($time_stamp, 60 * 60 * 24, 'day');
    }
    elseif ($time_difference >= 60 * 60)
    {
        /*
         * 60 seconds/minute * 60 minutes/hour
         * This means that the time difference is 1 hour or more
         */
        return get_time_ago_string($time_stamp, 60 * 60, 'hour');
    }
    else
    {
        /*
         * 60 seconds/minute
         * This means that the time difference is a matter of minutes
         */
        return get_time_ago_string($time_stamp, 60, 'minute');
    }
}

function get_time_ago_string($time_stamp, $divisor, $time_unit)
{
    $time_difference = strtotime("now") - $time_stamp;
    $time_units      = floor($time_difference / $divisor);

    settype($time_units, 'string');

    if ($time_units === '0')
    {
        return 'less than 1 ' . $time_unit . ' ago';
    }
    elseif ($time_units === '1')
    {
        return '1 ' . $time_unit . ' ago';
    }
    else
    {
        /*
         * More than "1" $time_unit. This is the "plural" message.
         */
        // TODO: This pluralizes the time unit, which is done by adding "s" at the end; this will not work for i18n!
        return $time_units . ' ' . $time_unit . 's ago';
    }
}