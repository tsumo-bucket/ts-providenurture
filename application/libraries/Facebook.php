<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  
load_composer();

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;

class Facebook
{
	public function __construct() 
	{

	}

	public function test()
	{
		FacebookSession::setDefaultApplication('260141941079253','9a66c661a4a47e4eafffd77214dc5e6f');

		// Use one of the helper classes to get a FacebookSession object.
		//   FacebookRedirectLoginHelper
		//   FacebookCanvasLoginHelper
		//   FacebookJavaScriptLoginHelper
		// or create a FacebookSession with a valid access token:
		$session = new FacebookSession('access-token-here');

		// Get the GraphUser object for the current user:

		try {
		  $me = (new FacebookRequest(
		    $session, 'GET', '/me'
		  ))->execute()->getGraphObject(GraphUser::className());
		  echo $me->getName();
		} catch (FacebookRequestException $e) {
		  // The Graph API returned an error
		} catch (\Exception $e) {
		  // Some other error occurred
		}
	}
}
