<?php
require_once dirname(__FILE__) . '/../third_party/aws/aws-autoloader.php';

use Aws\Common\Aws;
use Aws\S3\S3Client;

// Create the AWS service builder, providing the path to the config file

class S3_upload
{
    private $aws;
    private $CI;
    private $s3;

    public function __construct() {
        $this->CI =& get_instance();
        $this->CI->config->load('aws', TRUE);

        $this->s3 = S3Client::factory(array(
            'key'    => $this->CI->config->item('access_key', 'aws'),
            'secret' => $this->CI->config->item('access_secret', 'aws')
        ));
    }

    public function upload($filepath, $s3path, $file_type = null) {
        $bucket = $this->CI->config->item('s3_bucket', 'aws');

        $keyname = $s3path;

        $config = array(
            'Bucket'       => $bucket,
            'Key'          => $keyname,
            'SourceFile'   => $filepath,
            'ACL'          => 'public-read',
            'Metadata'     => array('Cache-Control' => 'no-cache'),
            'Cache-Control' => 'no-cache',
            'Response-Cache-Control' => 'no-cache'
        );

        if ($file_type) {
            if ($file_type === 'application/octet-stream') {
                try {
                    $ext = strtolower(array_pop(explode('.', $filepath)));

                    if ($ext === 'mp4') {
                        $config['Content-Type'] = 'video/mp4';
                    } elseif ($ext === 'jpg' || $ext === 'jpeg') {
                        $config['Content-Type'] = 'image/jpeg';
                    } elseif ($ext === 'png') {
                        $config['Content-Type'] = 'image/png';
                    } elseif ($ext === 'pdf') {
                        $config['Content-Type'] = 'application/pdf';
                    }
                } catch (Exception $e) {

                }
            } else {
                $config['Content-Type'] = $file_type;
            }
        }

        // Upload a file.
        $result = $this->s3->putObject($config);

        return $result;
    }

    public function delete($image) 
    {
        $bucket = $this->CI->config->item('s3_bucket', 'aws');
        $params = array('Bucket' => $bucket,'Objects' => array(array('Key' => $image)));
        $result = $this->s3->deleteObjects($params);
        return $result;
    }

}
