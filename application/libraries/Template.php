<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template extends MYTHOS_Template
{
	private $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		parent::__construct();
	}

	// Displays the page using the parameters preset using the set methods of this class
	public function show($template_folder = null, $template_view = 'template', $return_string = false)
	{
		$validator = $this->CI->form_validation->jquery_validator();
		if($validator !== false)
		{
			$this->set('head', $validator);
		}
		$this->CI->load->model('site_options_model');
		$site_options = $this->CI->site_options_model->get_all();

		foreach($site_options->result() as $site_options)
		{
			$this->set($site_options->opt_slug,$site_options->opt_value);
		}
		$this->set('facebook_meta_tags', $this->facebook_meta_tags());
		$this->set('notification', $this->render_notification());
		$this->set('fb_login', $this->fb_login());
		$this->set('mythos', $this->mythos());
		$this->set('mythos_utils', $this->mythos_utils());
		$this->set('font_awesome', $this->font_awesome());

		$this->set('bootstrap', $this->bootstrap());
		$this->set('bootstrap_js', $this->bootstrap('js'));

		$this->set('jqueryui', $this->jqueryui());
		$this->set('jqueryui_js', $this->jqueryui('js'));

		$this->set('redactor', $this->redactor());
		$this->set('redactor_js', $this->redactor('js'));

		$this->set('admin_lte', $this->admin_lte());
		$this->set('admin_lte_js', $this->admin_lte('js'));

		$this->set('slick_carousel', $this->slick_carousel());
		$this->set('slick_carousel_js', $this->slick_carousel('js'));

		$this->set('datepicker', $this->datepicker());
		$this->set('datepicker_js', $this->datepicker('js'));

		$this->set('sharebutton', $this->sharebutton());

		return parent::show($template_folder, $template_view, $return_string);
	}

	/*
	Notification types: warning (yellow), error (red), success (green), and info (blue)
	*/
	public function notification($message, $type = 'info')
	{
		$this->CI->session->set_userdata('notification', $message);
		$this->CI->session->set_userdata('notification_type', $type);
	}

	/*
	Process object return from base model
	*/
	public function process_notification($reply){
		if ($reply['success']){
			$this->notification($reply['message'], "success");
		} else {
			$this->notification($reply['message'], "error");
		}
	}

	private function render_notification()
	{
		$message = $this->CI->session->userdata('notification');
		$type = $this->CI->session->userdata('notification_type');

		$this->CI->session->unset_userdata('notification');
		$this->CI->session->unset_userdata('notification_type');

		$icons = array(
			'danger'  => '<i class="icon fa fa-ban"></i>',
			'info'    => '<i class="icon fa fa-info"></i>',
			'warning' => '<i class="icon fa fa-warning"></i>',
			'success' => '<i class="icon fa fa-check"></i>',
			'congratulations' => '<i class="icon fa fa-check"></i>'
		);

		if($message !== false)
		{
			$icon = $icons[$type];
			$title = ucfirst($type);
			return <<<EOT
			<div class="alert alert-{$type} fade in clearfix" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4 class="alert-message">
					{$icon}{$title}
				</h4>
				{$message}
			</div>
EOT;
		}
		else
		{
			return '';
		}
	}

	private function mythos()
	{
		$base_url = base_url();
		$site_url = site_url();
		$resources_url = res_url();
		$access = $this->CI->access_control->check_logged_in();
		$mythosHead = <<<EOT
	<script type="text/javascript"> var BASE_URL = "{$base_url}"; var SITE_URL = "{$site_url}"; var RESOURCES_URL = "{$resources_url}";</script>
		<script type="text/javascript"> var LOGGED_IN = "{$access}"; </script>

EOT;

		$this->CI->load->library('config');
		$csrfJS = '';
		if($this->CI->config->config['csrf_protection'])
		{
			$csrf_token_name = $this->CI->security->get_csrf_token_name();
			$csrf_hash = $this->CI->security->get_csrf_hash();
			$mythosHead .= "<script type=\"text/javascript\">eval(function(p,a,c,k,e,r){e=function(c){return c.toString(a)};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\b'+e(c)+'\\\b','g'),k[c]);return p}('\$(1(){\$(\'9\').b(1(i){6 a=\$(c);7(a.8(\'3[4=\"2\"]\').5()==0){a.d(1(){a.e(\'<3 f=\"g\" 4=\"2\" h=\"j\" />\')})}})});',20,20,'|function|{$csrf_token_name}|input|name|size|var|if|find|form||each|this|submit|append|type|hidden|value||{$csrf_hash}'.split('|'),0,{}))</script>";

			/* SOURCE CODE
			1. Compress code using http://javascriptcompressor.com/ (Base 62, shrink variables)
			2. Replace all " with \"
			3. Replace all $ with \$
			4. Replace all \\ with \\\

			$(function() {
				$('form').each(function(i) {
					var form_elem =  $(this);
					if(form_elem.find('input[name="{$csrf_token_name}"]').size() == 0) {
						form_elem.submit(function() {
							form_elem.append('<input type="hidden" name="{$csrf_token_name}" value="{$csrf_hash}" />');
						});
					}
				});
			});
			*/
		}

		return $mythosHead;
	}

	private function mythos_utils()
	{
		$resources_url = res_url();
		$mythosHead = <<<EOT
		<script type="text/javascript" src="{$resources_url}bower/jquery/dist/jquery.min.js"></script>
		<script type="text/javascript" src="{$resources_url}bower/jquery-validation/dist/jquery.validate.min.js"></script>
		<script type="text/javascript" src="{$resources_url}bower/floodling/jquery.floodling.min.js"></script>
		<script type="text/javascript" src="{$resources_url}mythos/js/utils.js"></script>

EOT;

		return $mythosHead;
	}

	private function bootstrap($type = 'css')
	{
		$resources_url = res_url();
		if ($type == 'css') {
			$bootstrap = <<<EOT
	<link rel="stylesheet" type="text/css" href="{$resources_url}bower/bootstrap/dist/css/bootstrap.min.css" />

EOT;
		} else {
			$bootstrap = <<<EOT
	<script type="text/javascript" src="{$resources_url}bower/bootstrap/dist/js/bootstrap.min.js"></script>
EOT;
		}
		return $bootstrap;
	}

	private function jqueryui($type = 'css')
	{
		$resources_url = res_url();
		if ($type == 'css') {
			$jqueryui = <<<EOT
	<link rel="stylesheet" type="text/css" href="{$resources_url}bower/jquery-ui/themes/base/jquery-ui.min.css" />

EOT;
		} else {
			$jqueryui = <<<EOT
	<script type="text/javascript" src="{$resources_url}bower/jquery-ui/jquery-ui.min.js"></script>
EOT;
		}
		return $jqueryui;
	}


	private function redactor($type = 'css')
	{
		$resources_url = res_url();
		if ($type == 'css') {
			$redactor = <<<EOT
	<link rel="stylesheet" type="text/css" href="{$resources_url}mythos/components/redactor/redactor.css" />

EOT;
		} else {
			$redactor = <<<EOT
	<script type="text/javascript" src="{$resources_url}mythos/components/redactor/redactor.min.js"></script>
EOT;
		}
		return $redactor;
	}

	private function fb_login()
	{
		$resources_url = res_url();
		$this->CI->config->load('mythos', TRUE);
		$fb_config = $this->CI->config->item('facebook','mythos');
		return <<<EOT
		<div id="fb-root"></div>
		<script type="text/javascript"> var APP_ID = "{$fb_config['app_id']}"; </script>
		<script type="text/javascript" src="{$resources_url}mythos/js/facebook.login.js"></script>
EOT;
	}

	private function facebook_meta_tags()
	{
		$this->CI->load->model('site_options_model');
		$facebook_title = $this->CI->site_options_model->get_by_slug('facebook-title');
		$site_name = $this->CI->site_options_model->get_by_slug('site-name');
		$facebook_description = $this->CI->site_options_model->get_by_slug('facebook-description');
		$facebook_image = $this->CI->site_options_model->get_by_slug('facebook-image');
		$image_url = base_url($facebook_image->opt_value);
		$url = current_url();

		return <<<EOT
	<meta property="og:title" content="{$facebook_title->opt_value}" />
	<meta property="og:site_name" content="{$site_name->opt_value}" />
	<meta property="og:url" content="{$url}" />
	<meta property="og:description" content="{$facebook_description->opt_value}" />
	<meta property="og:image" content="{$image_url}" />
EOT;
	}

	private function font_awesome()
	{
		$resources_url = res_url();
		$font_awesome = <<<EOT
	<link rel="stylesheet" type="text/css" href="{$resources_url}bower/components-font-awesome/css/font-awesome.min.css" />

EOT;
		return $font_awesome;
	}

	private function admin_lte($type = 'css')
	{
		$resources_url = res_url();
		if ($type == 'css') {
			$admin_lte = <<<EOT
	<link rel="stylesheet" type="text/css" href="{$resources_url}bower/admin-lte/dist/css/AdminLTE.min.css" />
	<link rel="stylesheet" type="text/css" href="{$resources_url}bower/admin-lte/dist/css/skins/_all-skins.min.css" />
	<link rel="stylesheet" type="text/css" href="{$resources_url}bower/admin-lte/bootstrap/css/bootstrap.min.css" />

EOT;
		} else {
			$admin_lte = <<<EOT
<script type="text/javascript" src="{$resources_url}bower/admin-lte/dist/js/app.min.js"></script>
<script type="text/javascript" src="{$resources_url}bower/admin-lte/bootstrap/js/bootstrap.min.js"></script>
EOT;
		}
		return $admin_lte;
	}

	private function slick_carousel($type = 'css')
	{
		$resources_url = res_url();
		if ($type == 'css') {
			$admin_lte = <<<EOT
			<link rel="stylesheet" type="text/css" href="{$resources_url}site/components/slick/slick.css" />
EOT;
		} else {
			$admin_lte = <<<EOT
			<script type="text/javascript" src="{$resources_url}site/components/slick/slick.min.js"></script>
EOT;
		}
		return $admin_lte;
	}

	private function datepicker($type = 'css')
	{
		$resources_url = res_url();
		if ($type == 'css') {
			$datepicker = <<<EOT
	<link rel="stylesheet" type="text/css" href="{$resources_url}bower/sumo-datepicker/css/jquery.sumo-datepicker.min.css">

EOT;
		} else {
			$datepicker = <<<EOT
	<script type="text/javascript" src="{$resources_url}bower/sumo-datepicker/js/jquery.sumo-datepicker.min.js"></script>

EOT;
		}
		return $datepicker;
	}

	private function sharebutton()
	{
		$resources_url = res_url();
		$sharebutton = <<<EOT
	<script type="text/javascript" src="{$resources_url}bower/share-button/build/share.min.js"></script>

EOT;

		return $sharebutton;
	}

	public function autofill($data = array(), $form_selector = 'form')
	{
		if(count($data) > 0)
		{
			$jsFill = '';
			foreach($data as $key => $value)
			{
				$value = json_encode(addslashes($value));
				$jsFill .= "$('{$form_selector}').floodling('{$key}', {$value});";
			}
			$this->set('autofill', "<script type=\"text/javascript\">$(function() { {$jsFill} });</script>");
		}
	}
}
