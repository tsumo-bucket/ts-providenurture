
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Socket extends MYTHOS_Access_control
{
	private $CI;
	
	public function __construct() 
	{
		$this->CI =& get_instance();
		parent::__construct();

		$this->CI->load->config('socket');
		$this->socket_url = $this->CI->config->item('socket_url');
	}

	public function member_message($msg)
	{

		$curl = curl_init();

		curl_setopt_array($curl, array(
		    CURLOPT_RETURNTRANSFER => 1,
		    CURLOPT_URL => $this->socket_url,
		    CURLOPT_USERAGENT => 'SugarFlame-Socket',
		    CURLOPT_POST => 1,
		    CURLOPT_POSTFIELDS => array(
		        'msg_content' => $msg['msg_content'],
		        'usr_id' => $msg['usr_id'],
		        'msg_user' => $msg['msg_user'],
		        'msg_date_created' => $msg['msg_date_created'],
		        'msg_received' => $msg['msg_received']
		    )
		));

		$resp = curl_exec($curl);
		
		curl_close($curl);
	}
}
