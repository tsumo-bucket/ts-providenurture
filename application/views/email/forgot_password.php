<div>
	<p>	
		Hello <?php echo ($user->usr_screen_name); ?>,
	</p>
	<p>
		We received a request to reset the password asscociated with this email address. If you made this request, please follow the instructions below.
	</p>
	<p>
		If you did not request to have your password reset, you can safely ignore this email. We assure you that your customer account is safe.
	</p>
	<p>
		<b>Click the link below to reset your password: </b>
	</p>
	<p>
		<a href="<?php echo client_url('reset/'.$user->usr_password_reset); ?>"><?php echo client_url('reset/'.$user->usr_password_reset); ?></a>
	</p>
	<p>
		If clicking the link doesn't work, you can copy and paste the link into your browser's address window, or retype it there. Once you have returned to ProvideNurture, we will give you instructions for resetting your password.
	</p>
	<p>
		This link will expire in 24 hours from the date you've received this email.
	</p>
	<p>
		Cheers,
		<br>
		The ProvideNurture Team
	</p>
</div>