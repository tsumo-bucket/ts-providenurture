Thank you for submitting your profile to ProvideNurture.<br /><br />
Unfortunately, we were unable to approve it.<br /><br />
Some of your profile pictures did not follow the guidelines and/or were inappropriate. Kindly ensure that all the photos you upload are legitimate pictures of yourself. Please also note that we don't accept upside down pictures. These six photo tips may also help you present yourself better to the community.<br /><br />
You will find instructions on how to edit or delete photos here.<br /><br />
Please revise and update your profile, and resubmit for approval. To do so, simply login with your ProvideNurture credentials.<br /><br />
We look forward to receiving your revised profile.<br /><br />
Sincerely,<br />
The ProvideNurture team

