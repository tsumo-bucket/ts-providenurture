Thank you for submitting your profile to ProvideNurture.<br /><br />
Unfortunately, we were unable to approve a violation of our terms of use which may be one of a combination of the following:<br />
<ul>
	<li>Posting profile or personal pictures that expose private parts (breast, nipples, vagina and penis pictures)</li>
	<li>Posting any incomplete, false, misleading, or inaccurate Content about yourself and/or your profile;</li>
	<li>Using meta tags or code or other devices to direct any person to any other website for any purpose;</li>
	<li>Posting or sending material that exploits people under the age of 18 in a sexual or violent manner, or solicits personal information from anyone under 18;</li>
	<li>Using the Website as an escort, or using the Service to solicit clients for an escort service including but not limited to prostitution;</li>
	<li>Posting advertisements or solicitations of employment, business, or pyramid schemes;</li>
	<li>Using the Website or Service to promote, solicit, or engage in prostitution;</li>
</ul>
<br />
Please revise and update your profile, and resubmit for approval. To do so, simply login with your ProvideNurture credentials.<br /><br />
We look forward to receiving your revised profile.<br /><br />
Sincerely,<br />
The ProvideNurture team

