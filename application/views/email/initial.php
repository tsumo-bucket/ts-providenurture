<?php
	$hash = md5($user->usr_id);
?>

<!-- Dear <?php echo $user->usr_screen_name; ?>,<br /><br /> -->
Thanks for signing up on ProvideNurture!<br /><br />
Please verify your email address to create your account: <br />
<a href="<?php echo client_url('verify/'.$hash); ?>"><?php echo client_url('verify/'.$hash); ?></a><br /><br />
Cheers, <br />
The ProvideNurture Team