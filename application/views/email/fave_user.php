<?php 
	if($sender->usr_gender == 'female'){
		$gender = 'Her';
	}else{
		$gender = 'His';
	}
?>

<div class="content-holder">
	<div class="content">
		<table style="margin: 0 auto !important; text-align: center !important;">
			<tr>
				<td>
					<div class="image-holder" style="background: url('<?php echo base_url($sender->img_image); ?>') center center no-repeat; width: 50px !important; height: 50px !important; border: 1px solid #aaa !important; background-size: cover !important; display: inline-block !important;"></div>
				</td>
				<td>
					<b><?php echo $sender->usr_screen_name; ?> faved you</b>
				</td>
			</tr>
		</table>
	</div>
	<a href="<?php echo client_url('user/'.$sender->usr_token_id); ?>" target="_blank" style="text-decoration: none !important;">
		<div class="button-holder" style="margin: 20px auto !important; width: 200px !important; height: 48px !important; line-height: 48px !important; background: #cc4885 !important; border: 1px solid #cc4885 !important; font-size: 14px !important; text-transform: uppercase !important; text-align: center !important; color: #fff !important;">
			<b style="color: #fff !important;">View <?php echo $gender; ?> Profile</b>
		</div>
	</a>
</div>
<div class="privacy-content">
	<i style="font-size: 11px !important; margin-bottom: 20px;">
		<a href="<?php echo client_url(); ?>" target="_blank" style="text-decoration: none !important;">ProvideNurture.com</a> protects the privacy and security of your profile information. All your data including credit card information, address, phone number, etc. is not shared with anyone. To ensure your privacy, do not share your account email or password with anyone under any circumstances.
	</i>
</div>
<div class="links-holder" style="margin: 0 auto !important; text-align: center !important;">
	<br>
	ProvideNurture | <a href="#" target="_blank" style="text-decoration: none !important;">Unsubscribe</a>
</div>