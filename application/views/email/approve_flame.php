Congratulations!<br /><br />
Your profile has been approved to join the ProvideNurture community!<br /><br />
You can now access ProvideNurture and meet your ProvideNurture mate. Get started now and don’t hesitate to visit our Help Center to acquaint yourself with the website. Do keep in mind that while having a profile and using the basic features are free, premium membership has its benefits: <br /><br />
Provider MAN/WOMAN Premium Membership Benefits<br />
<ul>
	<li>Increase your chances! Your profile is included in an exclusive priority section. You also standout in all searches.</li>
	<li>View special preferences of Nurturer Men/Women (Privacy Preferences, Relationship Expectations, etc.</li>
	<li>Have control over your activity in ProvideNurture (Recent Login, Join Date, etc.</li>
	<li>Have control over notifications sent to Nurturer Men/Women (Hide when you Like, View and Fave Someone.)</li>
</ul>
<br /><br />
As one of our early members, you automatically get a FREE 3 months unlimited use.<br /><br />
We look forward to seeing you on ProvideNurture.com.<br /><br />
Sincerely, <br />
The ProvideNurture team"
