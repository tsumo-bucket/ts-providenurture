Thank you for submitting your profile to ProvideNurture.<br /><br />
Unfortunately, we were unable to approve it.<br /><br />
<?php echo $message; ?> <br /><br />
Please revise and update your profile, and resubmit for approval. To do so, simply login with your ProvideNurture credentials.<br /><br />
We look forward to receiving your revised profile.<br /><br />
Sincerely,<br />
The ProvideNurture team

