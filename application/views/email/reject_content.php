Thank you for submitting your profile to ProvideNurture.<br /><br />
Unfortunately, we were unable to approve it.<br /><br />
Part of your profile text may have been unintelligible. Please also keep in mind that all written portions of your profile must be in English. In addition, the answers to the Questions must be at most 1000 characters (without repeating yourself or adding extra spaces or unnecessary characters).
We look forward to receiving your revised profile.<br /><br />
Sincerely,<br />
The ProvideNurture team

