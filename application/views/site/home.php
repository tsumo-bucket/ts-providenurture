<br><br>
<div class="text-center">
	<span>
		<a href="https://itunes.apple.com/us/app/orange-world/id1133096172?ls=1&mt=8" target="_blank">
			<img src="<?php echo res_url('site/images/apple_store.png'); ?>" style="max-width: 100%; margin-top: 20px;">
		</a>
	</span>
	<span>
		<a href="https://play.google.com/store/apps/details?id=ph.com.orangeworld" target="_blank">
			<img src="<?php echo res_url('site/images/play_store.png'); ?>" style="max-width: 100%; margin-top: 20px;">
		</a>
	</span>
</div>