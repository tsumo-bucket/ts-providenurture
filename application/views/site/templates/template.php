﻿<!DOCTYPE html>
<html lang="en">
<head>
	<title> <?php echo template('title'); ?> | <?php echo template('site-name') ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="fb:app_id" content="615588798580008">
	<meta property="og:type" content="article">
	<meta property="og:title" content="<?php echo @$article->art_title . @$reward->rew_name; ?>">
	<meta property="og:description" content="<?php echo strip_tags(@$article->art_short. @$reward->rew_short); ?>">
	<meta property="og:image" content="<?php echo base_url(@$article->art_thumb. @$reward->rew_banner); ?>">
	<meta property="og:url" content="<?php if(@$article->art_slug){ echo base_url('/article/index/'. @$article->art_slug); } else { echo base_url('/reward/index/'. @$reward->rew_id); } ?>">
	<?php //echo template('facebook_meta_tags'); ?>

	<?php echo template('bootstrap'); ?>
	<link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('bower/components-font-awesome/css/font-awesome.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('site/css/fonts.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('site/css/styles.css'); ?>" />
	<!-- 
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('bower/slick-carousel/slick/slick.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('site/css/progress-button.css'); ?>" />
	 -->
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('bower/animate.css/animate.min.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('bower/Ionicons/css/ionicons.css'); ?>" />
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('favicon.ico') ?>">

	<script type="text/javascript" src="<?php echo res_url('bower/jquery/dist/jquery.min.js'); ?>"></script>

	<?php echo template('mythos'); ?>
	<?php echo template('head'); ?>
	<!-- <script type="text/javascript" src="<?php echo res_url('site/js/modernizr.custom.js'); ?>"></script> -->
</head>
<body class="<?php echo uri_css_class(); ?>">
	<input type="text" id="title" class="hidden" value="<?php echo @$article->art_title . @$reward->rew_name; ?>"/>
	<input type="text" id="description" class="hidden" value="<?php echo strip_tags(@$article->art_short. @$reward->rew_short); ?>"/>
	<input type="text" id="image" class="hidden" value="<?php echo base_url(@$article->art_thumb. @$reward->rew_banner); ?>"/>
	<input type="text" id="url" class="hidden" value="<?php if(@$article->art_slug){ echo base_url('/article/index/'. @$article->art_slug); } else { echo base_url('/reward/index/'. @$reward->rew_id); } ?>"/>
	<div class="container" id>
		<nav class="text-center">
			<a href="<?php echo base_url(); ?>">
				<img src="<?php echo res_Url('site/images/logo.png'); ?>">
			</a>
		</nav>
		<div>
			<?php echo template('content'); ?>
			<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
				<?php echo template('notification'); ?>
			</div>
		</div>
	</div>
	<?php echo template('mythos', 'utils'); ?>
	<?php //echo template('fb_login'); ?>
	<?php echo template('bootstrap', 'js'); ?>
	<?php echo template('sharebutton'); ?>
	<?php echo template('autofill'); ?>
	<!-- 
	<script type="text/javascript" src="<?php echo res_url('bower/fullpage.js/vendors/jquery.slimscroll.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo res_url('bower/fullpage.js/dist/jquery.fullPage.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo res_url('bower/slick-carousel/slick/slick.min.js'); ?>"></script>
	 -->
	<script type="text/javascript" src="<?php echo res_url('site/js/document.ready.js'); ?>"></script>
</body>
</html>
