<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="usr_id" class="control-label">User Inactive</label>
						<select name="usr_id" class="form-control">
					<?php foreach($usr_ids->result() as $usr_id): ?>
						<option value="<?php echo $usr_id->usr_id ?>"><?php echo $usr_id->usi_id ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="ulc_latitude" class="control-label">Latitude</label>
						<input type="text" name="ulc_latitude" class="form-control" />
					</div>
					<div class="form-group">
						<label for="ulc_longitude" class="control-label">Longitude</label>
						<input type="text" name="ulc_longitude" class="form-control" />
					</div>
					<div class="form-group">
						<label for="ulc_date_added" class="control-label">Date Added</label>
						<input type="text" name="ulc_date_added" class="form-control datetime" />
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$('form').floodling('usr_id', '<?php echo addslashes($user_location->usr_id); ?>');
	$('form').floodling('ulc_latitude', '<?php echo addslashes($user_location->ulc_latitude); ?>');
	$('form').floodling('ulc_longitude', '<?php echo addslashes($user_location->ulc_longitude); ?>');
	$('form').floodling('ulc_date_added', '<?php echo addslashes($user_location->ulc_date_added); ?>');
});
</script>
