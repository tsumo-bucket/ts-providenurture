<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('user_locations/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				</h3>
				<div class="box-tools pull-right">
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Latitude</h3>
                	<div><?php echo $user_location->ulc_latitude ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Longitude</h3>
                	<div><?php echo $user_location->ulc_longitude ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Added</h3>
                	<div><?php echo $user_location->ulc_date_added ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('user locations/edit/'.$user_location->ulc_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>