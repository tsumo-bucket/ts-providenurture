<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="usr_id" class="control-label">User</label>
						<select name="usr_id" class="form-control">
					<?php foreach($usr_ids->result() as $usr_id): ?>
						<option value="<?php echo $usr_id->usr_id ?>"><?php echo $usr_id->usr_type ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="img_image" class="control-label">Image</label>
						<textarea name="img_image" style="width:100%" class="form-control redactor"></textarea>
					</div>
					<div class="form-group">
						<label for="img_thumb" class="control-label">Thumb</label>
						<textarea name="img_thumb" style="width:100%" class="form-control redactor"></textarea>
					</div>
					<div class="form-group">
						<label for="img_icon" class="control-label">Icon</label>
						<textarea name="img_icon" style="width:100%" class="form-control redactor"></textarea>
					</div>
					<div class="form-group">
						<label for="img_offset_x" class="control-label">Offset X</label>
						<input type="text" name="img_offset_x" class="form-control" />
					</div>
					<div class="form-group">
						<label for="img_offset_y" class="control-label">Offset Y</label>
						<input type="text" name="img_offset_y" class="form-control" />
					</div>
					<div class="form-group">
						<label for="img_order" class="control-label">Order</label>
						<input type="text" name="img_order" class="form-control" />
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>