<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('images/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				</h3>
				<div class="box-tools pull-right">
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Image</h3>
                	<div><?php echo $image->img_image ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Thumb</h3>
                	<div><?php echo $image->img_thumb ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Icon</h3>
                	<div><?php echo $image->img_icon ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Offset X</h3>
                	<div><?php echo $image->img_offset_x ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Offset Y</h3>
                	<div><?php echo $image->img_offset_y ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Order</h3>
                	<div><?php echo $image->img_order ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Created</h3>
                	<div><?php echo $image->img_date_created ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Modified</h3>
                	<div><?php echo $image->img_date_modified ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('images/edit/'.$image->img_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>