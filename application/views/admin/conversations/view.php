<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('conversations/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				</h3>
				<div class="box-tools pull-right">
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Slug</h3>
                	<div><?php echo $conversation->cnv_slug ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>User 1</h3>
                	<div><?php echo $conversation->cnv_user_1 ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>User 2</h3>
                	<div><?php echo $conversation->cnv_user_2 ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>User 1 Deleted</h3>
                	<div><?php echo $conversation->cnv_user_1_deleted ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>User 1 Deleted Date</h3>
                	<div><?php echo $conversation->cnv_user_1_deleted_date ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>User 2 Deleted</h3>
                	<div><?php echo $conversation->cnv_user_2_deleted ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>User 2 Deleted Date</h3>
                	<div><?php echo $conversation->cnv_user_2_deleted_date ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Created</h3>
                	<div><?php echo $conversation->cnv_date_created ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Modified</h3>
                	<div><?php echo $conversation->cnv_date_modified ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('conversations/edit/'.$conversation->cnv_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>