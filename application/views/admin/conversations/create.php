<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="cnv_slug" class="control-label">Slug</label>
						<input type="text" name="cnv_slug" class="form-control" />
					</div>
					<div class="form-group">
						<label for="cnv_user_1" class="control-label">User 1</label>
						<input type="text" name="cnv_user_1" class="form-control" />
					</div>
					<div class="form-group">
						<label for="cnv_user_2" class="control-label">User 2</label>
						<input type="text" name="cnv_user_2" class="form-control" />
					</div>
					<div class="form-group">
						<label for="cnv_user_1_deleted" class="control-label">User 1 Deleted</label>
						<select name="cnv_user_1_deleted" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="cnv_user_1_deleted_date" class="control-label">User 1 Deleted Date</label>
						<input type="text" name="cnv_user_1_deleted_date" class="form-control datetime" />
					</div>
					<div class="form-group">
						<label for="cnv_user_2_deleted" class="control-label">User 2 Deleted</label>
						<select name="cnv_user_2_deleted" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="cnv_user_2_deleted_date" class="control-label">User 2 Deleted Date</label>
						<input type="text" name="cnv_user_2_deleted_date" class="form-control datetime" />
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>