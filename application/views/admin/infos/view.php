<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('infos/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				</h3>
				<div class="box-tools pull-right">
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Height</h3>
                	<div><?php echo $info->inf_height ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Body Type</h3>
                	<div><?php echo $info->inf_body_type ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Ethnicity</h3>
                	<div><?php echo $info->inf_ethnicity ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Smoke</h3>
                	<div><?php echo $info->inf_smoke ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Drink</h3>
                	<div><?php echo $info->inf_drink ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Relationship</h3>
                	<div><?php echo $info->inf_relationship ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Children</h3>
                	<div><?php echo $info->inf_children ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Language</h3>
                	<div><?php echo $info->inf_language ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Education</h3>
                	<div><?php echo $info->inf_education ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Occupation</h3>
                	<div><?php echo $info->inf_occupation ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Spending Habits</h3>
                	<div><?php echo $info->inf_spending_habits ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Quality Time</h3>
                	<div><?php echo $info->inf_quality_time ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Gifts</h3>
                	<div><?php echo $info->inf_gifts ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Travel</h3>
                	<div><?php echo $info->inf_travel ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Staycations</h3>
                	<div><?php echo $info->inf_staycations ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>High Life</h3>
                	<div><?php echo $info->inf_high_life ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Simple</h3>
                	<div><?php echo $info->inf_simple ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Net Worth</h3>
                	<div><?php echo $info->inf_net_worth ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Yearly Income</h3>
                	<div><?php echo $info->inf_yearly_income ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Preferred Range From</h3>
                	<div><?php echo $info->inf_preferred_range_from ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Preferred Range To</h3>
                	<div><?php echo $info->inf_preferred_range_to ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Relationship Length</h3>
                	<div><?php echo $info->inf_relationship_length ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Relationship Loyalty</h3>
                	<div><?php echo $info->inf_relationship_loyalty ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Relationship Privacy</h3>
                	<div><?php echo $info->inf_relationship_privacy ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Relationship Preference</h3>
                	<div><?php echo $info->inf_relationship_preference ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Sexual Limit</h3>
                	<div><?php echo $info->inf_sexual_limit ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Privacy Expectations</h3>
                	<div><?php echo $info->inf_privacy_expectations ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Created</h3>
                	<div><?php echo $info->inf_date_created ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Modified</h3>
                	<div><?php echo $info->inf_date_modified ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('infos/edit/'.$info->inf_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>