<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="usr_id" class="control-label">User</label>
						<select name="usr_id" class="form-control">
					<?php foreach($usr_ids->result() as $usr_id): ?>
						<option value="<?php echo $usr_id->usr_id ?>"><?php echo $usr_id->usr_type ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_height" class="control-label">Height</label>
						<input type="text" name="inf_height" class="form-control" />
					</div>
					<div class="form-group">
						<label for="inf_body_type" class="control-label">Body Type</label>
						<select name="inf_body_type" class="form-control">
							<option value="slim">Slim</option>
							<option value="athletic">Athletic</option>
							<option value="average">Average</option>
							<option value="curvy">Curvy</option>
							<option value="a few extra pounds">A few extra pounds</option>
							<option value="overweight">Overweight</option>
							<option value="other">Other</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_ethnicity" class="control-label">Ethnicity</label>
						<select name="inf_ethnicity" class="form-control">
							<option value="asian">Asian</option>
							<option value="black / african descent">Black / african descent</option>
							<option value="latin / hispanic">Latin / hispanic</option>
							<option value="east indian">East indian</option>
							<option value="middle eastern">Middle eastern</option>
							<option value="mixed">Mixed</option>
							<option value="native american">Native american</option>
							<option value="pacific islander">Pacific islander</option>
							<option value="white / caucasian">White / caucasian</option>
							<option value="other">Other</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_smoke" class="control-label">Smoke</label>
						<select name="inf_smoke" class="form-control">
							<option value="non smoker">Non smoker</option>
							<option value="light smoker">Light smoker</option>
							<option value="heavy smoker">Heavy smoker</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_drink" class="control-label">Drink</label>
						<select name="inf_drink" class="form-control">
							<option value="non drinker">Non drinker</option>
							<option value="social drinker">Social drinker</option>
							<option value="heavy drinker">Heavy drinker</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_relationship" class="control-label">Relationship</label>
						<select name="inf_relationship" class="form-control">
							<option value="single">Single</option>
							<option value="divorced">Divorced</option>
							<option value="seperated">Seperated</option>
							<option value="married but looking">Married but looking</option>
							<option value="open relationship">Open relationship</option>
							<option value="widowed">Widowed</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_children" class="control-label">Children</label>
						<select name="inf_children" class="form-control">
							<option value="prefer not to say">Prefer not to say</option>
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_language" class="control-label">Language</label>
						<select name="inf_language" class="form-control">
							<option value="english">English</option>
							<option value="espanol">Espanol</option>
							<option value="francias">Francias</option>
							<option value="deutch">Deutch</option>
							<option value="chinese">Chinese</option>
							<option value="italiano">Italiano</option>
							<option value="nederlandese">Nederlandese</option>
							<option value="portuges">Portuges</option>
							<option value="russian">Russian</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_education" class="control-label">Education</label>
						<select name="inf_education" class="form-control">
							<option value="high school">High school</option>
							<option value="some college">Some college</option>
							<option value="associates degree">Associates degree</option>
							<option value="bachelors degree">Bachelors degree</option>
							<option value="graduate degree">Graduate degree</option>
							<option value="post doctorate">Post doctorate</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_occupation" class="control-label">Occupation</label>
						<input type="text" name="inf_occupation" class="form-control" />
					</div>
					<div class="form-group">
						<label for="inf_spending_habits" class="control-label">Spending Habits</label>
						<input type="text" name="inf_spending_habits" class="form-control" />
					</div>
					<div class="form-group">
						<label for="inf_quality_time" class="control-label">Quality Time</label>
						<select name="inf_quality_time" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_gifts" class="control-label">Gifts</label>
						<select name="inf_gifts" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_travel" class="control-label">Travel</label>
						<select name="inf_travel" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_staycations" class="control-label">Staycations</label>
						<select name="inf_staycations" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_high_life" class="control-label">High Life</label>
						<select name="inf_high_life" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_simple" class="control-label">Simple</label>
						<select name="inf_simple" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_net_worth" class="control-label">Net Worth</label>
						<input type="text" name="inf_net_worth" class="form-control" />
					</div>
					<div class="form-group">
						<label for="inf_yearly_income" class="control-label">Yearly Income</label>
						<input type="text" name="inf_yearly_income" class="form-control" />
					</div>
					<div class="form-group">
						<label for="inf_preferred_range_from" class="control-label">Preferred Range From</label>
						<input type="text" name="inf_preferred_range_from" class="form-control" />
					</div>
					<div class="form-group">
						<label for="inf_preferred_range_to" class="control-label">Preferred Range To</label>
						<input type="text" name="inf_preferred_range_to" class="form-control" />
					</div>
					<div class="form-group">
						<label for="inf_relationship_length" class="control-label">Relationship Length</label>
						<select name="inf_relationship_length" class="form-control">
							<option value="short term">Short term</option>
							<option value="long term">Long term</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_relationship_loyalty" class="control-label">Relationship Loyalty</label>
						<select name="inf_relationship_loyalty" class="form-control">
							<option value="no string attached">No string attached</option>
							<option value="exclusive">Exclusive</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_relationship_privacy" class="control-label">Relationship Privacy</label>
						<select name="inf_relationship_privacy" class="form-control">
							<option value="discreet">Discreet</option>
							<option value="public">Public</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_relationship_preference" class="control-label">Relationship Preference</label>
						<select name="inf_relationship_preference" class="form-control">
							<option value="rather not say">Rather not say</option>
							<option value="open to discussion">Open to discussion</option>
							<option value="strictly sexual">Strictly sexual</option>
							<option value="serious relationship">Serious relationship</option>
							<option value="marriage">Marriage</option>
							<option value="just friends">Just friends</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_sexual_limit" class="control-label">Sexual Limit</label>
						<select name="inf_sexual_limit" class="form-control">
							<option value="thinking about it">Thinking about it</option>
							<option value="open to new things">Open to new things</option>
							<option value="anything goes">Anything goes</option>
							<option value="rather not say">Rather not say</option>
							<option value="lets keep that to imagination">Lets keep that to imagination</option>
							<option value="no experimentation">No experimentation</option>
						</select>
					</div>
					<div class="form-group">
						<label for="inf_privacy_expectations" class="control-label">Privacy Expectations</label>
						<select name="inf_privacy_expectations" class="form-control">
							<option value="open to any arrangement">Open to any arrangement</option>
							<option value="very discreet">Very discreet</option>
							<option value="ok being in public">Ok being in public</option>
							<option value="depends on attraction level">Depends on attraction level</option>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$('form').floodling('usr_id', '<?php echo addslashes($info->usr_id); ?>');
	$('form').floodling('inf_height', '<?php echo addslashes($info->inf_height); ?>');
	$('form').floodling('inf_body_type', '<?php echo addslashes($info->inf_body_type); ?>');
	$('form').floodling('inf_ethnicity', '<?php echo addslashes($info->inf_ethnicity); ?>');
	$('form').floodling('inf_smoke', '<?php echo addslashes($info->inf_smoke); ?>');
	$('form').floodling('inf_drink', '<?php echo addslashes($info->inf_drink); ?>');
	$('form').floodling('inf_relationship', '<?php echo addslashes($info->inf_relationship); ?>');
	$('form').floodling('inf_children', '<?php echo addslashes($info->inf_children); ?>');
	$('form').floodling('inf_language', '<?php echo addslashes($info->inf_language); ?>');
	$('form').floodling('inf_education', '<?php echo addslashes($info->inf_education); ?>');
	$('form').floodling('inf_occupation', '<?php echo addslashes($info->inf_occupation); ?>');
	$('form').floodling('inf_spending_habits', '<?php echo addslashes($info->inf_spending_habits); ?>');
	$('form').floodling('inf_quality_time', '<?php echo addslashes($info->inf_quality_time); ?>');
	$('form').floodling('inf_gifts', '<?php echo addslashes($info->inf_gifts); ?>');
	$('form').floodling('inf_travel', '<?php echo addslashes($info->inf_travel); ?>');
	$('form').floodling('inf_staycations', '<?php echo addslashes($info->inf_staycations); ?>');
	$('form').floodling('inf_high_life', '<?php echo addslashes($info->inf_high_life); ?>');
	$('form').floodling('inf_simple', '<?php echo addslashes($info->inf_simple); ?>');
	$('form').floodling('inf_net_worth', '<?php echo addslashes($info->inf_net_worth); ?>');
	$('form').floodling('inf_yearly_income', '<?php echo addslashes($info->inf_yearly_income); ?>');
	$('form').floodling('inf_preferred_range_from', '<?php echo addslashes($info->inf_preferred_range_from); ?>');
	$('form').floodling('inf_preferred_range_to', '<?php echo addslashes($info->inf_preferred_range_to); ?>');
	$('form').floodling('inf_relationship_length', '<?php echo addslashes($info->inf_relationship_length); ?>');
	$('form').floodling('inf_relationship_loyalty', '<?php echo addslashes($info->inf_relationship_loyalty); ?>');
	$('form').floodling('inf_relationship_privacy', '<?php echo addslashes($info->inf_relationship_privacy); ?>');
	$('form').floodling('inf_relationship_preference', '<?php echo addslashes($info->inf_relationship_preference); ?>');
	$('form').floodling('inf_sexual_limit', '<?php echo addslashes($info->inf_sexual_limit); ?>');
	$('form').floodling('inf_privacy_expectations', '<?php echo addslashes($info->inf_privacy_expectations); ?>');
});
</script>
