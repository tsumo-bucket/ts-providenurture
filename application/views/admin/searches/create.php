<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="usr_id" class="control-label">User</label>
						<select name="usr_id" class="form-control">
					<?php foreach($usr_ids->result() as $usr_id): ?>
						<option value="<?php echo $usr_id->usr_id ?>"><?php echo $usr_id->usr_type ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="sch_name" class="control-label">Name</label>
						<input type="text" name="sch_name" class="form-control" />
					</div>
					<div class="form-group">
						<label for="sch_content" class="control-label">Content</label>
						<textarea name="sch_content" style="width:100%" class="form-control redactor"></textarea>
					</div>
					<div class="form-group">
						<label for="sch_status" class="control-label">Status</label>
						<select name="sch_status" class="form-control">
							<option value="active">Active</option>
							<option value="archived">Archived</option>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>