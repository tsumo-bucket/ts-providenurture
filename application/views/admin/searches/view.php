<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('searches/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				</h3>
				<div class="box-tools pull-right">
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Name</h3>
                	<div><?php echo $search->sch_name ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Content</h3>
                	<div><?php echo $search->sch_content ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Status</h3>
                	<div><?php echo $search->sch_status ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Created</h3>
                	<div><?php echo $search->sch_date_created ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('searches/edit/'.$search->sch_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>