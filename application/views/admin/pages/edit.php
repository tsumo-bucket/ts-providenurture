<div class="row">
	<div class="col-md-12">
		<form method="post" enctype="multipart/form-data" data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="pag_title" class="control-label">Page Title</label>
						<input type="text" name="pag_title" class="form-control" maxlength="140" />
					</div>
					<div class="form-group">
						<label for="pct_id" class="control-label">Category</label>
						<select class="form-control" name="pct_id" id="pct_id">
							<?php
							if($this->access_control->check_account_type('developer'))
							{
							?>
							<option value="0"><?php echo Page_category_model::UNCATEGORIZED; ?></option>
							<?php
							}
							else
							{
								?>
								<option value="">choose...</option>
								<?php
							}

							foreach($page_categories->result() as $page_category)
							{
							?>
								<option value="<?php echo $page_category->pct_id; ?>"><?php echo $page_category->pct_name; ?></option>
							<?php
							}
							?>
						</select>
					</div>
					<?php if($this->access_control->check_account_type('developer')): ?>
					<div class="form-group">
						<label for="pag_type" class="control-label"> Type</label>
						<select name="pag_type" id="pag_type" class="form-control">
							<option value="editable">Editable</option>
							<option value="static">Static</option>
						</select>
					</div>
					<?php endif; ?>
					<div class="form-group">
						<label for="pag_status" class="control-label">Status</label>
						<select name="pag_status" id="pag_status" class="form-control">
							<option value="published">Published</option>
							<option value="draft">Draft</option>
						</select>
					</div>
					<div class="form-group" id="date_published_row">
						<label for="pag_date_published" class="control-label">Date Published</label>
						<div class="row">
							<div class="col-md-6">
								<input type="text" value="<?php echo $page->pag_date_published != NULL ? $page->pag_date_published : format_mysql_date(); ?>" name="pag_date_published" class="sumodate" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="pag_content" class="control-label">Content</label>
						<input type="hidden" class="data-controller" name="data-controller" value="pages">
						<textarea name="pag_content" class="redactor" style="width: 100%; height: 400px;"><?php echo $page->pag_content ?></textarea>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>"><i class="fa fa-times"></i> Cancel</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {


	$('form').floodling('pag_title', "<?php echo addslashes($page->pag_title); ?>");
	$('form').floodling('pag_status', "<?php echo addslashes($page->pag_status); ?>");
	$('#pct_id').val('<?php echo $page->pct_id; ?>');
	<?php
	if($this->access_control->check_account_type('developer'))
	{
	?>
	$('#pag_type').val('<?php echo $page->pag_type; ?>');
	<?php
	}
	?>
	$('#pag_status').val('<?php echo $page->pag_status; ?>');
});
</script>
