<div class="row">
	<div class="col-md-3">
		
		<a href="<?php echo admin_url('pages/create') ?>" class="btn btn-primary btn-block">
			<i class="fa fa-plus"></i> Create
		</a>
		
		<br>
		<div class="box box-default">
			<div class="box-header with-border">
				<h3 class="box-title">Categories</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('pages') ?>"><i class="fa fa-circle-o"></i> All</a>
						<a href="<?php echo admin_url('pages/index/0') ?>"><i class="fa fa-circle-o"></i> <?php echo Page_category_model::UNCATEGORIZED ?></a>
						<?php foreach($page_categories->result() as $category): ?>
						<a href="<?php echo admin_url('pages/index/'.$category->pct_id) ?>"><i class="fa fa-circle-o"></i> <?php echo $category->pct_name ?></a>
						<?php endforeach; ?>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage pages</h3>
			</div>
			<?php if ($pages->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">
					
						<div class="btn btn-default btn-sm checkbox-toggle select-all" data-fields="pag_ids"><i class="fa fa-square-o"></i></div>
						<div class="btn-group">
							<button class="btn btn-default btn-sm action-delete" name="form_mode" value="delete"> <i class="fa fa-trash-o"></i> Delete</button>
						</div>
						<div class="pull-right">
							<?php echo $pages_pagination; ?>
						</div>
					</div>
					<table class="table table-striped table-hover">
						<tbody>
						<?php
						foreach($pages->result() as $page)
						{

							if( $page->pct_id == null || $page->pct_id == 0 ) { $page->pct_name = Page_category_model::UNCATEGORIZED; }
						?>
							<tr>
								<td style="width:20px" class="center">
									<input type="checkbox" name="pag_ids[]" value="<?php echo $page->pag_id; ?>" />
								</td>
								<td>
									<a href="<?php echo admin_url('pages/edit/'.$page->pag_id) ?>"><?php echo $page->pag_title; ?></a>
								</td>
								<td class='text-right' style="width:300px">
									<span class="label label-default"><?php echo $page->pag_status; ?></span> 
								</td>
							</tr>
						<?php
						}
						?>
						</tbody>
					</table>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No pages found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>