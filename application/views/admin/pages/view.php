<div class="col-sm-3 col-sm-push-9 block">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Menu</h3>
		</div>
		<div class="box-body">
			<div class="btn-group">
				<?php if($this->access_control->check_account_type('developer')): ?>
				<a href="<?php echo site_url('admin/pages/edit/' . $page->pag_id); ?>" class="btn btn-danger btn-flat btn-sm">
					<i class="fa fa-edit"></i> Edit
				</a>
				<?php endif; ?>
				<?php echo create_admin_back_btn($this->router->fetch_class()); ?>
			</div>
		</div>
	</div>
</div>
<div class="col-sm-9 col-sm-pull-3">
	<div class="box">
		<div class="box-body no-padding table-responsive">
			<table class="table table-bordered table-hover table-view">
				<tr>
					<th>Title</th>
					<td><?php echo $page->pag_title; ?></td>
				</tr>
				<tr>
					<th>Category</th>
					<td>
						<?php 
							if($page->pct_id == 0){
								echo 'Uncategorized';
							}else{
								$this->load->model('page_category_model');
								$category = $this->page_category_model->get_one($page->pct_id);
								if($category){
									echo $category->pct_name;
								}else{
									echo 'Uncategorized';
								}
							}
						?>
					</td>
				</tr>
				<tr>
					<th>Content</th>
					<td><?php echo $page->pag_content; ?></td>
				</tr>
				<tr>
					<th>Type</th>
					<td><?php echo $page->pag_type; ?></td>
				</tr>
				<tr>
					<th>Status</th>
					<td><?php echo $page->pag_status; ?></td>
				</tr>
				<tr>
					<th>Date Published</th>
					<td><?php echo format_datetime($page->pag_date_published, '', 'Asia/Manila'); ?></td>
				</tr>
				<tr>
					<th>Date Created</th>
					<td><?php echo format_datetime($page->pag_date_created, '', 'Asia/Manila'); ?></td>
				</tr>

			</table>
		</div>
	</div>
</div>