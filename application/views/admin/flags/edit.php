<style type="text/css">
	div.post-holder{
	  padding: 15px 20px;
	}

	.post-image-holder,
	.post-content-holder{
		display: inline-block;
	}

	.post-content-holder{
		vertical-align: top;
		padding: 10px 15px;
	}

	.post-date{
		padding-top: 5px;
	}

	.post{
		padding-top: 10px;
	}

	span.bold{
		font-weight: 700;
	}
</style>

<?php 
	$this->db->where('pos_id', $flag->pos_id);
	$posts = $this->db->get('post');
	$post = $posts->row();

	$this->db->Where('usr_id', $post->usr_id);
	$users = $this->db->get('user');
	$user = $users->row();


	$from = new DateTime($user->usr_birthyear.'-'.$user->usr_birthmonth.'-'.$user->usr_birthdate);
	$to = new DateTime('today');

	$age = $from->diff($to)->y;

	$cty_name = '';
	$cnt_name = '';

	$this->db->where('cty_id', $user->cty_id);
	$this->db->join("region", "region.reg_id = city.reg_id", "left outer");
	$this->db->join("country", "country.cnt_id = region.cnt_id", "left outer");
	$cities = $this->db->get('city');
	if($cities->num_rows() > 0){
		$city = $cities->row();
		$cty_name = $city->cty_name;
		$cnt_name = $city->cnt_short_name;
	}

?>


<div class="row">
	<div class="col-md-6">
		<form method="post"  data-submit="submitForm">
			<div class="box" id="flag-view">
				<div class="box-body">
					<div class="post-holder">
						<div class="post-image-holder">
							<?php
								if($flag->img_id == 0){
									$url = res_url('admin/images/usr_icon.png');
								}else{
									$this->db->where('img_id', $user->img_id);
									$images = $this->db->get('image');

									if($images->num_rows() > 0){
										$url = base_url($images->row()->img_image);
									}else{
										$url = res_url('admin/images/usr_icon.png');
									}
								}
							?>
							<div class="usr_photo" style="background: url(<?php echo $url; ?>);"></div>
						</div>
						<div class="post-content-holder">
							<div class="usr_details">
								<span class="bold"><?php echo $user->usr_screen_name; ?></span>
								<span><?php echo ' '.$age; ?></span>
								<span><?php echo ' &#183; '.ucfirst($user->usr_gender); ?></span>
								<?php if($cnt_name != '' && $cty_name != ''): ?>
								<span><?php echo ' &#183; '.$cty_name.', '.$cnt_name; ?></span>	
								<?php endif; ?>
							</div>
							<div class="post-date"><?php echo format_datetime($flag->pos_date_created, 'Y-m-d h:i:s A', 'Asia/Manila'); ?></div>
							<div class="post">
								<?php echo $flag->pos_content; ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="flg_status" class="control-label">Flagged By</label>
						<div>
							<a href="<?php echo admin_url('users/view/'.$flag->usr_id); ?>">
								<?php echo $flag->usr_email; ?>
							</a>
						</div>
						<div>
							<?php echo format_datetime($flag->flg_date_created, 'Y-m-d h:i:s A', 'Asia/Manila'); ?>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-trash"></i> Delete Post
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	// $('form').floodling('usr_id', '<?php echo addslashes($flag->usr_id); ?>');
	// $('form').floodling('pos_id', '<?php echo addslashes($flag->pos_id); ?>');
	// $('form').floodling('flg_status', '<?php echo addslashes($flag->flg_status); ?>');
});
</script>
