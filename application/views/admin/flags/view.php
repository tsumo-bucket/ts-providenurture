<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('flags/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Status</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('flags') ?>"><i class="fa fa-circle-o"></i>All</a>
					</li>
					<li>
						<a href="<?php echo admin_url('flags?flg_status=active') ?>"><i class="fa fa-circle-o"></i>Active</a>
					</li>
					<li>
						<a href="<?php echo admin_url('flags?flg_status=inactive') ?>"><i class="fa fa-circle-o"></i>Inactive</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
					Id: <?php echo $flag->usr_id; ?> |
					Id: <?php echo $flag->pos_id; ?> |
				</h3>
				<div class="box-tools pull-right">
					<span class="label label-default">Status: <?php echo $flag->flg_status ?></span>
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Status</h3>
                	<div><?php echo $flag->flg_status ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Created</h3>
                	<div><?php echo $flag->flg_date_created ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Modified</h3>
                	<div><?php echo $flag->flg_date_modified ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('flags/edit/'.$flag->flg_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>