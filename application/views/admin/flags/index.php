<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage Flags</h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<form role="form" method="get">
							<input type="text" class="form-control input-sm" name="search" placeholder="Search" value="<?php echo $this->input->get('search') ?>" />
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</form>
					</div>
				</div><!-- /.box-tools -->
			</div>
			<?php if($flags->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">	
						<div class="pull-right">
							<?php echo $flags_pagination; ?>
						</div>
					</div>
					<table class="table table-striped table-hover">
						<tbody>
						<?php foreach($flags->result() as $flag): ?>
							<tr data-primkey="<?php echo $flag->flg_id; ?>">
								<td style="width:50px" class="center">
									<!-- <input type="checkbox" name="flg_ids[]" value="<?php echo $flag->flg_id; ?>" /> -->
								</td>
								
								<td>
									<a href="<?php echo admin_url('flags/edit/'.$flag->flg_id); ?>"><?php echo format_datetime($flag->flg_date_created, 'Y-m-d h:i:s A', 'Asia/Manila'); ?></a>
								</td>
								
								<td>
									<a href="<?php echo admin_url('users/view/'.$flag->usr_id); ?>">
										<?php echo $flag->usr_email; ?>
									</a>
								</td>
								
								<td class="text-right">
			
									<!-- <span class="label label-default"><?php echo $flag->flg_status  ?></span> -->
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No flags found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>