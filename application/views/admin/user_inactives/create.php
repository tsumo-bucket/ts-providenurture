<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="usr_id" class="control-label">User</label>
						<select name="usr_id" class="form-control">
					<?php foreach($usr_ids->result() as $usr_id): ?>
						<option value="<?php echo $usr_id->usr_id ?>"><?php echo $usr_id->usr_type ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="usi_reason" class="control-label">Reason</label>
						<select name="usi_reason" class="form-control">
							<option value="found the ideal relationship">Found the ideal relationship</option>
							<option value="no longer looking">No longer looking</option>
							<option value="taking a break">Taking a break</option>
							<option value="didn''t find what I wanted">Didn''t find what I wanted</option>
							<option value="didn''t feel safe">Didn''t feel safe</option>
							<option value="rather not say">Rather not say</option>
							<option value="too busy">Too busy</option>
							<option value="not using this service">Not using this service</option>
							<option value="other">Other</option>
						</select>
					</div>
					<div class="form-group">
						<label for="usi_comment" class="control-label">Comment</label>
						<textarea name="usi_comment" style="width:100%" class="form-control redactor"></textarea>
					</div>
					<div class="form-group">
						<label for="usi_type" class="control-label">Type</label>
						<select name="usi_type" class="form-control">
							<option value="deactivate">Deactivate</option>
							<option value="delete">Delete</option>
						</select>
					</div>
					<div class="form-group">
						<label for="usi_status" class="control-label">Status</label>
						<select name="usi_status" class="form-control">
							<option value="active">Active</option>
							<option value="archived">Archived</option>
						</select>
					</div>
					<div class="form-group">
						<label for="usi_created_date" class="control-label">Created Date</label>
						<input type="text" name="usi_created_date" class="form-control datetime" />
					</div>
					<div class="form-group">
						<label for="usi_modified_date" class="control-label">Modified Date</label>
						<input type="text" name="usi_modified_date" class="form-control datetime" />
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>