<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('user_inactives/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				</h3>
				<div class="box-tools pull-right">
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Reason</h3>
                	<div><?php echo $user_inactive->usi_reason ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Comment</h3>
                	<div><?php echo $user_inactive->usi_comment ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Type</h3>
                	<div><?php echo $user_inactive->usi_type ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Status</h3>
                	<div><?php echo $user_inactive->usi_status ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Created Date</h3>
                	<div><?php echo $user_inactive->usi_created_date ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Modified Date</h3>
                	<div><?php echo $user_inactive->usi_modified_date ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('user inactives/edit/'.$user_inactive->usi_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>