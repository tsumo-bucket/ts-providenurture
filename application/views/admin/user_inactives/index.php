<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('user_inactives/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>

	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage User Inactives</h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<form role="form" method="get">
							<input type="text" class="form-control input-sm" name="search" placeholder="Search" value="<?php echo $this->input->get('search') ?>" />
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</form>
					</div>
				</div><!-- /.box-tools -->
			</div>
			<?php if($user_inactives->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">
					<!-- Check all button -->
						<div class="btn btn-default btn-sm checkbox-toggle select-all" data-fields="usi_ids">
							<i class="fa fa-square-o"></i>
						</div>
						<div class="btn-group">
							<button class="btn btn-default btn-sm action-delete" name="form_mode" value="delete"> 
								<i class="fa fa-trash-o"></i> Delete All
							</button>
						</div><!-- /.btn-group --> 
						<div class="pull-right">
							<?php echo $user_inactives_pagination; ?>
						</div><!-- /.pull-right -->
					</div>
					<table class="table table-striped table-hover">
						<tbody>
						<?php foreach($user_inactives->result() as $user_inactive): ?>
							<tr data-primkey="<?php echo $user_inactive->usi_id; ?>">
								<td style="width:50px" class="center">
									<input type="checkbox" name="usi_ids[]" value="<?php echo $user_inactive->usi_id; ?>" />
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No user inactives found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>