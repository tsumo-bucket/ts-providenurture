<h1>
	User Inactives	
	<?php if($this->router->fetch_method() != 'index'): ?>
	<small><?php echo $this->router->fetch_method(); ?></small>
	<?php endif; ?>
</h1>
<ol class="breadcrumb">
	<li>
		<a href="<?php echo admin_url('user_inactives') ?>"><i class="fa fa-dashboard"></i> Home</a>
	</li>
	<?php if($this->router->fetch_method() != 'index'): ?>
	<li class="active"><?php echo $this->router->fetch_method(); ?></li>
	<?php endif; ?>
</ol>