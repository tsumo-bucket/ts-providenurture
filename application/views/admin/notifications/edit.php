<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="usr_id" class="control-label">User</label>
						<select name="usr_id" class="form-control">
					<?php foreach($usr_ids->result() as $usr_id): ?>
						<option value="<?php echo $usr_id->usr_id ?>"><?php echo $usr_id->usr_type ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="not_message" class="control-label">Message</label>
						<select name="not_message" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="not_like" class="control-label">Like</label>
						<select name="not_like" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="not_profile" class="control-label">Profile</label>
						<select name="not_profile" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="not_content" class="control-label">Content</label>
						<select name="not_content" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="not_email" class="control-label">Email</label>
						<select name="not_email" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$('form').floodling('usr_id', '<?php echo addslashes($notification->usr_id); ?>');
	$('form').floodling('not_message', '<?php echo addslashes($notification->not_message); ?>');
	$('form').floodling('not_like', '<?php echo addslashes($notification->not_like); ?>');
	$('form').floodling('not_profile', '<?php echo addslashes($notification->not_profile); ?>');
	$('form').floodling('not_content', '<?php echo addslashes($notification->not_content); ?>');
	$('form').floodling('not_email', '<?php echo addslashes($notification->not_email); ?>');
});
</script>
