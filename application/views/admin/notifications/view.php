<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('notifications/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				</h3>
				<div class="box-tools pull-right">
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Message</h3>
                	<div><?php echo $notification->not_message ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Like</h3>
                	<div><?php echo $notification->not_like ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Profile</h3>
                	<div><?php echo $notification->not_profile ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Content</h3>
                	<div><?php echo $notification->not_content ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Email</h3>
                	<div><?php echo $notification->not_email ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Created</h3>
                	<div><?php echo $notification->not_date_created ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Modified</h3>
                	<div><?php echo $notification->not_date_modified ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('notifications/edit/'.$notification->not_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>