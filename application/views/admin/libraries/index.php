<?php 
// Normal form here. Form validation are taken care of by the controller.
// Make sure to name your form elements properly and uniquely.
?>

<!-- QR Code Library -->
<h3> QR Code </h3>
<form method="post" action="<?php echo site_url('/admin/libraries/qr_generate'); ?>">
	<table class="table-form table-bordered">
		<tr>
			<th>QR Code</th>
			<td><input type="text" name="qr_text"/></td>
		</tr>
		<tr>
			<th></th>
			<td>
				<?php
				// A custom Javascript method redirect is just a shorthand for window.location.
				?>
				<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
			</td>
		</tr>
	</table>
</form>

<h3> Facebook Login </h3>
<form method="post" action="<?php echo site_url('/admin/libraries/qr_generate'); ?>">
	<table class="table-form table-bordered">
		<tr>
			<th>Login With Facebook</th>
			<td><input type="text" name="qr_text" style="width: 100%;" /></td>
		</tr>
	</table>
</form>
