<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('cities/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
		<br>
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Countries</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('cities') ?>"><i class="fa fa-circle-o"></i>All</a>
					</li>
				<?php foreach($cnt_ids->result() as $cnt_id): ?>
					<li>
						<a href="<?php echo admin_url('cities?cnt_name='.$cnt_id->cnt_name) ?>"><i class="fa fa-circle-o"></i><?php echo ucfirst($cnt_id->cnt_name) ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>

	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage Cities</h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<form role="form" method="get">
							<input type="text" class="form-control input-sm" name="search" placeholder="Search" value="<?php echo $this->input->get('search') ?>" />
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</form>
					</div>
				</div>
			</div>
			<?php if($cities->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">
						<div class="btn btn-default btn-sm checkbox-toggle select-all" data-fields="cty_ids">
							<i class="fa fa-square-o"></i>
						</div>
						<div class="btn-group">
							<button class="btn btn-default btn-sm action-delete" name="form_mode" value="delete"> 
								<i class="fa fa-trash-o"></i> Delete
							</button>
						</div>
						<div class="pull-right">
							<?php echo $cities_pagination; ?>
						</div>
					</div>
					<table class="table table-striped table-hover">
						<tbody>
						<?php foreach($cities->result() as $city): ?>
							<tr data-primkey="<?php echo $city->cty_id; ?>">
								<td style="width:50px" class="center">
									<input type="checkbox" name="cty_ids[]" value="<?php echo $city->cty_id; ?>" />
								</td>
								<td>
									<a href="<?php echo admin_url('cities/edit/'.$city->cty_id); ?>"><?php echo $city->cty_name?></a>
								</td>
								<td class="text-right">
									<span class="label label-default"><?php echo $city->cnt_name  ?></span>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No cities found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>