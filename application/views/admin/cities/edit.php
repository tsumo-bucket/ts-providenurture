<div class="row">
	<div class="col-md-6">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="reg_id" class="control-label">Region</label>
						<select name="reg_id" class="form-control">
					<?php foreach($reg_ids->result() as $reg_id): ?>
						<option value="<?php echo $reg_id->reg_id ?>"><?php echo $reg_id->reg_name ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="cty_name" class="control-label">Name</label>
						<input type="text" name="cty_name" class="form-control" />
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$('form').floodling('reg_id', '<?php echo addslashes($city->reg_id); ?>');
	$('form').floodling('cty_name', '<?php echo addslashes($city->cty_name); ?>');
});
</script>
