<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('cities/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Region</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('cities') ?>"><i class="fa fa-circle-o"></i>All</a>
					</li>
				<?php foreach($reg_ids->result() as $reg_id): ?>
					<li>
						<a href="<?php echo admin_url('cities?reg_id='.$reg_id->reg_id) ?>"><i class="fa fa-circle-o"></i><?php echo ucfirst($reg_id->cnt_id) ?></a>
					</li>
				<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
					Name: <?php echo $city->cty_name; ?> |
				</h3>
				<div class="box-tools pull-right">
					<span class="label label-default">Id: <?php echo $city->reg_id ?></span>
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Name</h3>
                	<div><?php echo $city->cty_name ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('cities/edit/'.$city->cty_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>