<div class="row">
	<div class="col-md-12 col-lg-6">
		<form method="post" enctype="multipart/form-data" data-submit="submitForm">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">
						<i class="glyphicon glyphicon-user"></i>
						User Profile
					</h3>
					<div class="box-tools pull-right">
	                    <div class="label label-default"><?php echo $account->acc_type; ?></div> 
	                </div>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="acc_first_name" class="control-label">Fist Name</label>
						<input type="text" name="acc_first_name" class="form-control" maxlength="100" value="<?php echo $account->acc_first_name ?>" />
					</div>
					<div class="form-group">
						<label for="acc_last_name" class="control-label">Fist Name</label>
						<input type="text" name="acc_last_name" class="form-control" maxlength="100" value="<?php echo $account->acc_last_name ?>" />
					</div>
					<div class="form-group">
						<div class="btn btn-default btn-file">
	                    	<i class="fa fa-file-image-o"></i> Image
	                    	<input type="file" name="acc_image">
	                    </div>

						<br>
						<br>
						<div>
	                    	<?php if($account->acc_image!=""){?>
							<div id="thumb">
								<img src="<?php echo base_url($account->acc_image); ?>" />
							</div>
							<br />

							<a class="btn-btn-link" href="<?php echo base_url($account->acc_image); ?>" target="_blank">Download Image</a>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<a class="btn btn-warning" href="<?php echo site_url('admin/profile/change_password'); ?>">
						<i class="fa fa-lock"></i>
						Change Password
					</a>
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>