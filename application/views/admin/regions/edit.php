<div class="row">
	<div class="col-md-6">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="cnt_id" class="control-label">Country</label>
						<select name="cnt_id" class="form-control">
					<?php foreach($cnt_ids->result() as $cnt_id): ?>
						<option value="<?php echo $cnt_id->cnt_id ?>"><?php echo $cnt_id->cnt_name ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="reg_name" class="control-label">Name</label>
						<input type="text" name="reg_name" class="form-control" />
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$('form').floodling('cnt_id', '<?php echo addslashes($region->cnt_id); ?>');
	$('form').floodling('reg_name', '<?php echo addslashes($region->reg_name); ?>');
});
</script>
