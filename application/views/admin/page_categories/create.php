<div class="row">
	<div class="col-md-12">
		<form method="post" enctype="multipart/form-data" data-submit="submitForm">
			<div class="box">
				<div class="box-body"><form method="post">
					<div class="form-group">
						<label for="pct_name" class="control-label">Category Name</label>
						<input type="text" name="pct_name" class="form-control" maxlength="140" />
					</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-danger btn-flat"><i class="fa fa-save"></i> Save</button>
					</div>
					<a class="btn btn-default btn-flat" href="<?php echo back_href(); ?>"><i class="fa fa-times"></i> Cancel</a>
				</div>
			</div>
		</form>
	</div>
</div>