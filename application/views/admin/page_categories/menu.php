<h1>
	Page Categories
	<?php if($this->router->fetch_method() != 'index'): ?>
	<small><?php echo $this->router->fetch_method(); ?></small>
	<?php endif; ?>
</h1>
<ol class="breadcrumb">
	<li>
		<a href="<?php echo admin_url('page_categories') ?>" class="btn-flat"><i class="fa fa-book"></i> Page categories</a>
	</li>
	<?php if($this->router->fetch_method() != 'index'): ?>
	<li class="active"><?php echo $this->router->fetch_method(); ?></li>
	<?php endif; ?>
</ol>