<div class="row">
	<div class="col-md-3">
		<!-- <a href="<?php echo admin_url('page_categories/create') ?>" class="btn btn-danger btn-block margin-bottom btn-flat">
			<i class="fa fa-plus"></i> Create
		</a> -->
	</div>

	<div class="col-md-12">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage page categories</h3>
			</div>
			<?php if ($page_categories->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">
					<!-- Check all button -->
						<!-- div class="btn btn-default btn-sm btn-flat checkbox-toggle select-all" data-fields="pct_ids"><i class="fa fa-square-o"></i></div>
						<div class="btn-group">
							<button class="btn btn-default btn-sm btn-flat action-delete" name="form_mode" value="delete"> <i class="fa fa-trash-o"></i> Delete</button>
						</div> --><!-- /.btn-group --> 
						<div class="pull-right">
							<?php echo $page_categories_pagination; ?>
						</div><!-- /.pull-right -->
					</div>
					<table class="table table-striped table-hover">
						<tbody>
						<?php
						foreach($page_categories->result() as $page)
						{

							
						?>
							<tr>
								<td style="width:30px" class="center">
									<!-- <input type="checkbox" name="pct_ids[]" value="<?php echo $page->pct_id; ?>" /> -->
								</td>
								<td>
									<a href="<?php echo admin_url('page_categories/view/'.$page->pct_id) ?>"><?php echo $page->pct_name; ?></a>
								</td>
							</tr>
						<?php
						}
						?>
						</tbody>
					</table>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No page categories found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>