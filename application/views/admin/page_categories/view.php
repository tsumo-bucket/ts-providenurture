<div class="col-sm-3 col-sm-push-9 block">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Menu</h3>
		</div>
		<div class="box-body">
			<div class="btn-group">
				<?php if($this->access_control->check_account_type('developer')): ?>
				<a href="<?php echo site_url('admin/page_categories/edit/' . $page_category->pct_id); ?>" class="btn btn-danger btn-flat btn-sm">
					<i class="fa fa-edit"></i> Edit
				</a>
				<?php endif; ?>
				<?php echo create_admin_back_btn($this->router->fetch_class()); ?>
			</div>
		</div>
	</div>
</div>
<div class="col-sm-9 col-sm-pull-3">
	<div class="box">
		<div class="box-body no-padding table-responsive">
			<table class="table table-bordered table-hover table-view">
				<tr>
					<th>Name</th>
					<td><?php echo $page_category->pct_name; ?></td>
				</tr>
			</table>
		</div>
	</div>
</div>