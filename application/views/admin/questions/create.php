<div class="row">
	<div class="col-md-6">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="que_question" class="control-label">Question</label>
						<input type="text" name="que_question" class="form-control" />
					</div>
					<div class="form-group">
						<label for="que_category" class="control-label">Category</label>
						<select name="que_category" class="form-control">
							<option value="baby">Baby</option>
							<option value="sponsor">Sponsor</option>
							<option value="both">Both</option>
						</select>
					</div>
					<div class="form-group">
						<label for="que_status" class="control-label">Status</label>
						<select name="que_status" class="form-control">
							<option value="published">Published</option>
							<option value="draft">Draft</option>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>