<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('questions/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
		<br>
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Category</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('questions') ?>"><i class="fa fa-circle-o"></i>All</a>
					</li>
					<li>
						<a href="<?php echo admin_url('questions?que_category=baby') ?>"><i class="fa fa-circle-o"></i>Baby</a>
					</li>
					<li>
						<a href="<?php echo admin_url('questions?que_category=sponsor') ?>"><i class="fa fa-circle-o"></i>Sponsor</a>
					</li>
					<li>
						<a href="<?php echo admin_url('questions?que_category=both') ?>"><i class="fa fa-circle-o"></i>Both</a>
					</li>
				</ul>
			</div>
		</div>
	
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Status</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('questions') ?>"><i class="fa fa-circle-o"></i>All</a>
					</li>
					<li>
						<a href="<?php echo admin_url('questions?que_status=published') ?>"><i class="fa fa-circle-o"></i>Published</a>
					</li>
					<li>
						<a href="<?php echo admin_url('questions?que_status=draft') ?>"><i class="fa fa-circle-o"></i>Draft</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage Questions</h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<form role="form" method="get">
							<input type="text" class="form-control input-sm" name="search" placeholder="Search" value="<?php echo $this->input->get('search') ?>" />
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</form>
					</div>
				</div>
			</div>
			<?php if($questions->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">
						<div class="btn btn-default btn-sm checkbox-toggle select-all" data-fields="que_ids">
							<i class="fa fa-square-o"></i>
						</div>
						<div class="btn-group">
							<button class="btn btn-default btn-sm action-delete" name="form_mode" value="delete"> 
								<i class="fa fa-trash-o"></i> Delete
							</button>
						</div>
						<div class="pull-right">
							<?php echo $questions_pagination; ?>
						</div>
					</div>
					<table class="table table-striped table-hover">
						<tbody>
						<?php foreach($questions->result() as $question): ?>
							<tr data-primkey="<?php echo $question->que_id; ?>">
								<td style="width:50px" class="center">
									<input type="checkbox" name="que_ids[]" value="<?php echo $question->que_id; ?>" />
								</td>
								
								<td>
									<a href="<?php echo admin_url('questions/edit/'.$question->que_id); ?>"><?php echo $question->que_question?></a>
								</td>
								
								<td>
									<?php echo $question->que_characters?>
								</td>
								
								
								<td class="text-right">
	
									<span class="label label-default"><?php echo $question->que_category  ?></span>
	
									<span class="label label-default"><?php echo $question->que_status  ?></span>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No questions found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>