<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('questions/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Category</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('questions') ?>"><i class="fa fa-circle-o"></i>All</a>
					</li>
					<li>
						<a href="<?php echo admin_url('questions?que_category=baby') ?>"><i class="fa fa-circle-o"></i>Baby</a>
					</li>
					<li>
						<a href="<?php echo admin_url('questions?que_category=sponsor') ?>"><i class="fa fa-circle-o"></i>Sponsor</a>
					</li>
				</ul>
			</div>
		</div>
	
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Status</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('questions') ?>"><i class="fa fa-circle-o"></i>All</a>
					</li>
					<li>
						<a href="<?php echo admin_url('questions?que_status=published') ?>"><i class="fa fa-circle-o"></i>Published</a>
					</li>
					<li>
						<a href="<?php echo admin_url('questions?que_status=draft') ?>"><i class="fa fa-circle-o"></i>Draft</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
					Question: <?php echo $question->que_question; ?> |
					Characters: <?php echo $question->que_characters; ?> |
				</h3>
				<div class="box-tools pull-right">
					<span class="label label-default">Category: <?php echo $question->que_category ?></span>
					<span class="label label-default">Status: <?php echo $question->que_status ?></span>
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Question</h3>
                	<div><?php echo $question->que_question ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Slug</h3>
                	<div><?php echo $question->que_slug ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Characters</h3>
                	<div><?php echo $question->que_characters ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Type</h3>
                	<div><?php echo $question->que_type ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Category</h3>
                	<div><?php echo $question->que_category ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Status</h3>
                	<div><?php echo $question->que_status ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Created</h3>
                	<div><?php echo $question->que_date_created ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Modified</h3>
                	<div><?php echo $question->que_date_modified ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Created By</h3>
                	<div><?php echo $question->que_created_by ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Updated By</h3>
                	<div><?php echo $question->que_updated_by ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('questions/edit/'.$question->que_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>