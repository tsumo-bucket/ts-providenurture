<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="usr_id" class="control-label">User</label>
						<select name="usr_id" class="form-control">
					<?php foreach($usr_ids->result() as $usr_id): ?>
						<option value="<?php echo $usr_id->usr_id ?>"><?php echo $usr_id->usr_type ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="que_id" class="control-label">Question</label>
						<select name="que_id" class="form-control">
					<?php foreach($que_ids->result() as $que_id): ?>
						<option value="<?php echo $que_id->que_id ?>"><?php echo $que_id->que_question ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="prf_content" class="control-label">Content</label>
						<textarea name="prf_content" style="width:100%" class="form-control redactor"><?php echo $preference->prf_content; ?></textarea>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$('form').floodling('usr_id', '<?php echo addslashes($preference->usr_id); ?>');
	$('form').floodling('que_id', '<?php echo addslashes($preference->que_id); ?>');
});
</script>
