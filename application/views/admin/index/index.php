<div class="container">
	<div class="row">
		<figure class="text-center login-logo">
			<img src="<?php echo res_url('admin/images/pn_logo_long.png'); ?>">
		</figure>
		<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-4 col-lg-offset-4">
			<div class="login-form-container">
				<h3 class="text-center login-form-title"><?php echo template('title'); ?></h3>
				<form class="login-form" method="post" action="<?php echo site_url('admin/index'); ?>">
					<div class="form-group has-feedback">
						<input class="form-control" type="text" name="acc_username" placeholder="Email">
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input class="form-control" type="password" name="acc_password" placeholder="Password">
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<?php if( $show_captcha == true ): ?>
					<div class="form-group">
						<div class="well">
							<h6>Captcha</h6>
							<img id="captcha" src="<?php echo res_url('mythos/components/securimage/securimage_show.php'); ?>" alt="CAPTCHA Image" />
							<p class="captcha-instruction">Enter the word in the image</p>
              <input type="text" name="captcha_code" class="form-control" /><br />
              <a href="#" onclick="document.getElementById('captcha').src = '<?php echo res_url('mythos/securimage/securimage_show.php'); ?>?' + Math.random(); return false">Try a different image</a>
						</div>
					</div>
					<?php endif; ?>
					<div class="form-group">
						<input type="hidden" value="<?php echo $this->session->flashdata('current_url'); ?>" name="current_url" id="current_url"/>
          	<input type="submit" name="submit" value="Sign In" class="btn btn-solid btn-large btn-block" />
					</div>
				</form>
			</div>
		</div>
	</div>
</div>