<div id="sumo-bubble">
    <div id="sumo-bubble-med"></div>
    <div id="sumo-bubble-small"></div>
    <div class="sumo-bubble-content">
        <img class="sumo-logo" src="<?php echo res_url('admin/images/logo.png') ?>" alt="ThinkSumo Creative Inc." />
        <!-- <h1 class="center"><?php echo template('title'); ?></h1> -->
        <form class="form-horizontal" method="post" action="<?php echo site_url('admin/index'); ?>">
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    <input type="text" placeholder="Email" name="acc_username" class="form-control" maxlength="100" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    <input type="password" placeholder="Password" name="acc_password" class="form-control" maxlength="100" />
                </div>
            </div>
            <?php if($show_captcha == true): ?>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    Type the word below
                </div>
                <div class="col-sm-6 col-sm-offset-3">
                    <img id="captcha" src="<?php echo res_url('mythos/components/securimage/securimage_show.php'); ?>" alt="CAPTCHA Image" /><br />
                    <input type="text" name="captcha_code" class="form-control" /><br />
                    <a href="#" onclick="document.getElementById('captcha').src = '<?php echo res_url('mythos/securimage/securimage_show.php'); ?>?' + Math.random(); return false">Try a different image</a>
                </div>
            </div>
            <?php endif; ?>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    <input type="hidden" value="<?php echo $this->session->flashdata('current_url'); ?>" name="current_url" id="current_url"/>
                    <input type="submit" name="submit" value="Login" class="btn btn-primary btn-large btn-block" />
            </div>
                </div>
        </form>
    </div>
</div>
