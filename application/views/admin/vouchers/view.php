<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('vouchers/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Status</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('vouchers') ?>"><i class="fa fa-circle-o"></i>All</a>
					</li>
					<li>
						<a href="<?php echo admin_url('vouchers?vch_status=active') ?>"><i class="fa fa-circle-o"></i>Active</a>
					</li>
					<li>
						<a href="<?php echo admin_url('vouchers?vch_status=archived') ?>"><i class="fa fa-circle-o"></i>Archived</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
					Code: <?php echo $voucher->vch_code; ?> |
					Value: <?php echo $voucher->vch_value; ?> |
					Valid Until: <?php echo $voucher->vch_valid_until; ?> |
				</h3>
				<div class="box-tools pull-right">
					<span class="label label-default">Status: <?php echo $voucher->vch_status ?></span>
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Code</h3>
                	<div><?php echo $voucher->vch_code ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Status</h3>
                	<div><?php echo $voucher->vch_status ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Value</h3>
                	<div><?php echo $voucher->vch_value ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Valid Until</h3>
                	<div><?php echo $voucher->vch_valid_until ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Created</h3>
                	<div><?php echo $voucher->vch_date_created ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Modified</h3>
                	<div><?php echo $voucher->vch_date_modified ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Created By</h3>
                	<div><?php echo $voucher->vch_created_by ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Modified By</h3>
                	<div><?php echo $voucher->vch_modified_by ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('vouchers/edit/'.$voucher->vch_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>