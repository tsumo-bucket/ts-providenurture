<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="vch_code" class="control-label">Code</label>
						<input type="text" name="vch_code" class="form-control" />
					</div>
					<div class="form-group">
						<label for="vch_status" class="control-label">Status</label>
						<select name="vch_status" class="form-control">
							<option value="active">Active</option>
							<option value="archived">Archived</option>
						</select>
					</div>
					<div class="form-group">
						<label for="vch_value" class="control-label">Value</label>
						<input type="text" name="vch_value" class="form-control" />
					</div>
					<div class="form-group">
						<label for="vch_valid_until" class="control-label">Valid Until</label>
						<input type="text" name="vch_valid_until" class="form-control datetime" />
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$('form').floodling('vch_code', '<?php echo addslashes($voucher->vch_code); ?>');
	$('form').floodling('vch_status', '<?php echo addslashes($voucher->vch_status); ?>');
	$('form').floodling('vch_value', '<?php echo addslashes($voucher->vch_value); ?>');
	$('form').floodling('vch_valid_until', '<?php echo addslashes($voucher->vch_valid_until); ?>');
});
</script>
