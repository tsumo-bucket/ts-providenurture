<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('vouchers/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Status</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('vouchers') ?>"><i class="fa fa-circle-o"></i>All</a>
					</li>
					<li>
						<a href="<?php echo admin_url('vouchers?vch_status=active') ?>"><i class="fa fa-circle-o"></i>Active</a>
					</li>
					<li>
						<a href="<?php echo admin_url('vouchers?vch_status=archived') ?>"><i class="fa fa-circle-o"></i>Archived</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage Vouchers</h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<form role="form" method="get">
							<input type="text" class="form-control input-sm" name="search" placeholder="Search" value="<?php echo $this->input->get('search') ?>" />
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</form>
					</div>
				</div><!-- /.box-tools -->
			</div>
			<?php if($vouchers->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">
					<!-- Check all button -->
						<div class="btn btn-default btn-sm checkbox-toggle select-all" data-fields="vch_ids">
							<i class="fa fa-square-o"></i>
						</div>
						<div class="btn-group">
							<button class="btn btn-default btn-sm action-delete" name="form_mode" value="delete"> 
								<i class="fa fa-trash-o"></i> Delete All
							</button>
						</div><!-- /.btn-group --> 
						<div class="pull-right">
							<?php echo $vouchers_pagination; ?>
						</div><!-- /.pull-right -->
					</div>
					<table class="table table-striped table-hover">
						<tbody>
						<?php foreach($vouchers->result() as $voucher): ?>
							<tr data-primkey="<?php echo $voucher->vch_id; ?>">
								<td style="width:50px" class="center">
									<input type="checkbox" name="vch_ids[]" value="<?php echo $voucher->vch_id; ?>" />
								</td>
								
								<td>
									<a href="<?php echo admin_url('vouchers/edit/'.$voucher->vch_id); ?>"><?php echo $voucher->vch_code?></a>
								</td>
								
								
								<td>
									<?php echo $voucher->vch_value?>
								</td>
								
								<td>
									<?php echo $voucher->vch_valid_until?>
								</td>
								<td class="text-right">
	
									<span class="label label-default"><?php echo $voucher->vch_status  ?></span>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No vouchers found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>