<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('user_accounts/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
					Type: <?php echo ($user_accounts->usa_type == 'sugar' ? 'provider' : 'nurturer'); ?> |
					Email: <?php echo $user_accounts->usa_email; ?> |
				</h3>
				<div class="box-tools pull-right">
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Type</h3>
                	<div><?php echo $user_accounts->usa_type ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Email</h3>
                	<div><?php echo $user_accounts->usa_email ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Access Id</h3>
                	<div><?php echo $user_accounts->usa_access_id ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Access Token</h3>
                	<div><?php echo $user_accounts->usa_access_token ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Created</h3>
                	<div><?php echo $user_accounts->usa_date_created ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Modified</h3>
                	<div><?php echo $user_accounts->usa_date_modified ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('user accounts/edit/'.$user_accounts->usa_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>