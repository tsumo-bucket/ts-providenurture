<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('user_accounts/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>

	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage User Accounts</h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<form role="form" method="get">
							<input type="text" class="form-control input-sm" name="search" placeholder="Search" value="<?php echo $this->input->get('search') ?>" />
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</form>
					</div>
				</div><!-- /.box-tools -->
			</div>
			<?php if($user_accounts->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">
					<!-- Check all button -->
						<div class="btn btn-default btn-sm checkbox-toggle select-all" data-fields="usa_ids">
							<i class="fa fa-square-o"></i>
						</div>
						<div class="btn-group">
							<button class="btn btn-default btn-sm action-delete" name="form_mode" value="delete"> 
								<i class="fa fa-trash-o"></i> Delete All
							</button>
						</div><!-- /.btn-group --> 
						<div class="pull-right">
							<?php echo $user_accounts_pagination; ?>
						</div><!-- /.pull-right -->
					</div>
					<table class="table table-striped table-hover">
						<tbody>
						<?php foreach($user_accounts->result() as $user_accounts): ?>
							<tr data-primkey="<?php echo $user_accounts->usa_id; ?>">
								<td style="width:50px" class="center">
									<input type="checkbox" name="usa_ids[]" value="<?php echo $user_accounts->usa_id; ?>" />
								</td>
								
								<td>
									<a href="<?php echo admin_url('user_accounts/edit/'.$user_accounts->usa_id); ?>"><?php echo $user_accounts->usa_type?></a>
								</td>
								
								<td>
									<?php echo $user_accounts->usa_email?>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No user accounts found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>