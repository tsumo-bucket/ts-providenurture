<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="src_name" class="control-label">Name</label>
						<input type="text" name="src_name" class="form-control" />
					</div>
					<div class="form-group">
						<label for="src_status" class="control-label">Status</label>
						<select name="src_status" class="form-control">
							<option value="active">Active</option>
							<option value="archived">Archived</option>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$('form').floodling('src_name', '<?php echo addslashes($source->src_name); ?>');
	$('form').floodling('src_status', '<?php echo addslashes($source->src_status); ?>');
});
</script>
