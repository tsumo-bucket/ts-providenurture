<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('sources/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Status</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('sources') ?>"><i class="fa fa-circle-o"></i>All</a>
					</li>
					<li>
						<a href="<?php echo admin_url('sources?src_status=active') ?>"><i class="fa fa-circle-o"></i>Active</a>
					</li>
					<li>
						<a href="<?php echo admin_url('sources?src_status=archived') ?>"><i class="fa fa-circle-o"></i>Archived</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
					Name: <?php echo $source->src_name; ?> |
				</h3>
				<div class="box-tools pull-right">
					<span class="label label-default">Status: <?php echo $source->src_status ?></span>
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Name</h3>
                	<div><?php echo $source->src_name ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Status</h3>
                	<div><?php echo $source->src_status ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('sources/edit/'.$source->src_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>