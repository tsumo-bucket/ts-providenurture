<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo template('title'); ?> | Administration Panel</title>
	<meta charset="utf-8">

	<!-- CSS -->
	<?php echo template('font_awesome'); ?>
	<?php echo template('jqueryui'); ?>
	<?php echo template('redactor'); ?>
	<?php echo template('admin_lte'); ?>
	<?php echo template('datepicker'); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('admin/css/styles.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('admin/js/featherlight/src/featherlight.css'); ?>" />
		
	<link href="<?php echo res_url('admin/chosen/chosen.css'); ?>" rel="stylesheet">

	<!-- Javascripts (Jquery) -->
	<?php echo template('mythos'); ?>
	<?php echo template('mythos_utils'); ?>
	<?php echo template('head'); ?>
</head>
<body class="<?php echo uri_css_class(); ?> skin-blue">
	<script type="text/javascript">
		function toggleMenuCookie()
		{
			if(sessionStorage.sidebarCollapse == "true")
			{
				sessionStorage.sidebarCollapse = 'false';
			}
			else
			{
				sessionStorage.sidebarCollapse = 'true';
			}
		}

		if(sessionStorage.sidebarCollapse == "true")
		{
			document.body.className = document.body.className + " sidebar-collapse";
		}
	</script>
	<div class="wrapper">
		<section id="page-notification">
			<?php echo template('notification'); ?>
		</section>
		<header class="header main-header">
			<a href="<?php echo site_url('admin'); ?>" class="logo">
				<img src="<?php echo res_url('admin/images/pn_logo.png'); ?>" style="max-height: 40px; ">
			</a>
			<nav class="navbar navbar-static-top" role="navigation">
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" onclick="toggleMenuCookie()">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<?php if ($this->access_control->check_logged_in()): ?>
						<li class="dropdown user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="glyphicon glyphicon-user"></i>
								<span><?php echo $this->session->userdata('acc_name'); ?><i class="caret"></i></span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-header bg-sf-purple">
									<?php
										$email = $this->session->userdata('acc_username');
										if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
											$email = "info@sumofy.me";
									  	}
										$size = 90;
										$grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=&s=" . $size;
									?>
									<?php if($this->session->userdata('acc_image')): ?>
									<img src="<?php echo base_url($this->session->userdata('acc_image')); ?>" class="img-circle" alt="User Image">
									<?php else: ?>
									<img src="<?php echo res_url('admin/images/default_avatar.jpg'); ?>" class="img-circle" alt="User Image">
									<?php endif; ?>
									<p>
										<small><?php echo $this->session->userdata('acc_name'); ?></small>
									</p>
								</li>
								<li class="user-footer">
									<div class="pull-left">
										<a href="<?php echo admin_url('profile'); ?>" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="<?php echo admin_url('index/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
						<?php endif; ?>
					</ul>
				</div>
			</nav>
		</header>
		<aside class="main-sidebar">
			<section class="sidebar">
				<div class="user-panel">
					<div class="pull-left image">
						<?php if($this->session->userdata('acc_image')): ?>
						<img src="<?php echo base_url($this->session->userdata('acc_image')); ?>" class="img-circle" alt="User Image">
						<?php else: ?>
						<img src="<?php echo res_url('admin/images/default_avatar.jpg'); ?>" class="img-circle" alt="User Image">
						<?php endif; ?>
					</div>
					<div class="pull-left info" style="width:160px;">
						<p><?php echo $this->session->userdata('acc_name'); ?></p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
			  	<ul class="sidebar-menu">
			  		<li class="header">SITE MODULES</li>
					<?php if ($this->access_control->check_account_type('admin', 'developer', 'content creator')): ?>
					<li class="<?php if($this->uri->segment(2) ==  'pages'){ echo 'active'; } ?>">
						<a href="<?php echo admin_url('pages'); ?>">
							<i class="fa fa-bookmark"></i>
							<span>Pages</span>
						</a>
					</li>
					<li class="treeview">
            <a href="#">
              <i class="fa fa-location-arrow"></i>
              <span>Location</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li class="smaller <?php if($this->uri->segment(2) ==  'cities'){ echo 'active'; } ?>"><a href="<?php echo admin_url('cities'); ?>">Cities</a></li>
              <li class="smaller <?php if($this->uri->segment(2) ==  'regions'){ echo 'active'; } ?>"><a href="<?php echo admin_url('regions'); ?>">Regions</a></li>
              <li class="smaller <?php if($this->uri->segment(2) ==  'countries'){ echo 'active'; } ?>"><a href="<?php echo admin_url('countries'); ?>">Countries</a></li>
            </ul>
          </li>
         <!--  <li class="treeview <?php if($this->uri->segment(2) ==  'users') { echo 'active'; } ?>">
            <a href="#">
              <i class="fa fa-user-plus"></i>
              <span>Users</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li class="smaller">
              	<a href="<?php echo admin_url('users?usr_status=approved'); ?>">
              		Approved <span class="badge pull-right"><?php echo user_count('approved'); ?></span>
              	</a>
              </li>
              <li class="smaller">
              	<a href="<?php echo admin_url('users?usr_status=pending'); ?>">
              		Pending <span class="badge pull-right"><?php echo user_count('pending'); ?></span>
              	</a>
              </li>
              <li class="smaller">
              	<a href="<?php echo admin_url('users?usr_status='); ?>">
              		Unverified <span class="badge pull-right"><?php echo user_count('unverified'); ?></span>
              	</a>
              </li>
              <li class="smaller">
              	<a href="<?php echo admin_url('users'); ?>">
              		Uncompleted <span class="badge pull-right"><?php echo user_count('uncompleted'); ?></span>
              	</a>
              </li>
              <li class="smaller">
              	<a href="<?php echo admin_url('users'); ?>">
              		Rejected <span class="badge pull-right"><?php echo user_count('rejected'); ?></span>
              	</a>
              </li>

            </ul>
          </li> -->
          <li class="<?php if($this->uri->segment(2) ==  'users'){ echo 'active'; } ?>">
						<a href="<?php echo admin_url('users'); ?>">
							<i class="fa fa-user-plus"></i>
							<span>Users</span>
						</a>
					</li>
					<!-- <li class="<?php if($this->uri->segment(2) ==  'user_updates'){ echo 'active'; } ?>">
						<a href="<?php echo admin_url('user_updates'); ?>">
							<i class="fa fa-chevron-circle-down"></i>
							<span>User Updates</span>
						</a>
					</li> -->
					<li class="<?php if($this->uri->segment(2) ==  'questions'){ echo 'active'; } ?>">
						<a href="<?php echo admin_url('questions'); ?>">
							<i class="fa fa-question-circle"></i>
							<span>Questions</span>
						</a>
					</li>
					<li class="<?php if($this->uri->segment(2) ==  'user_reports'){ echo 'active'; } ?>">
						<a href="<?php echo admin_url('user_reports'); ?>">
							<i class="fa fa-warning"></i>
							<span>Reported Users </span>
						</a>
					</li>
					<li class="<?php if($this->uri->segment(2) ==  'flags'){ echo 'active'; } ?>">
						<a href="<?php echo admin_url('flags'); ?>">
							<i class="fa fa-flag"></i>
							<span>Flags</span>
						</a>
					</li>
					<li class="<?php if($this->uri->segment(2) ==  'accounts'){ echo 'active'; } ?>">
						<a href="<?php echo admin_url('accounts'); ?>">
							<i class="fa fa-user-secret"></i><span>Accounts</span>
						</a>
					</li>
					<li class="<?php if($this->uri->segment(2) ==  'site_options'){ echo 'active'; } ?>">
						<a href="<?php echo admin_url('site_options'); ?>">
							<i class="fa fa-gear"></i><span>Site Options</span>
						</a>
					</li>
					<?php endif; ?>
					<?php if ($this->access_control->check_account_type('developer')): ?>
					<li class="header">DEVELOPER TOOLS</li>	
					<li class="<?php if($this->uri->segment(2) ==  'emails'){ echo 'active'; } ?>">
						<a href="<?php echo admin_url('emails'); ?>"><i class="fa fa-angle-double-right"></i> Emails</a>
					</li>
					<li class="<?php if($this->uri->segment(2) ==  'settings'){ echo 'active'; } ?>">
						<a href="<?php echo admin_url('settings'); ?>"><i class="fa fa-angle-double-right"></i> Site Settings</a>
					</li>
					<?php endif; ?>
				</ul>
			</section>
		</aside>
		<div class="content-wrapper">
			<section class="content-header clearfix">
		   		<?php echo template('page-nav'); ?>
			</section>
			<section class="content">
				<?php echo template('content'); ?>
			</section>
		</div>
	</div>
	<div class="modal fade" id="confirm-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body center">
					<a class="close" data-dismiss="modal">&times;</a>
					<h3>Confirm Action</h3>
					<p>Are you sure you want to continue?</p>
				</div>
				<div class="modal-footer center">
					<a href="#" class="btn btn-primary btn-large">Yes</a>
					<a href="#" class="btn btn-large" data-dismiss="modal">No</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade bs-example-modal-lg" id="user-image-modal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-body center">
					<a class="close" data-dismiss="modal">&times;</a>
					<div style="margin-top: 20px;">
						<br>
						<img class="img-responsive" style="margin: 0 auto !important; ">
					</div>
				</div>
				<div class="modal-footer center">
					<a href="#" class="btn btn-default btn-large" data-dismiss="modal">Cancel</a>
					<a href="#" class="btn btn-primary btn-large" id="user-image-delete-btn">Delete</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="rejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content"></div>
		</div>
	</div>
	<?php echo template('redactor', 'js'); ?>
	<?php echo template('admin_lte', 'js'); ?>
	<?php echo template('fancybox', 'js'); ?>
	<?php echo template('datepicker', 'js'); ?>
	<?php echo template('jqueryui', 'js'); ?>
	<?php echo template('autofill'); ?>
	<script type="text/javascript" src="<?php echo res_url('admin/js/featherlight/src/featherlight.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo res_url('admin/js/jquery.sortable.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo res_url('admin/js/document.ready.js'); ?>"></script>

	<script type="text/javascript" src="<?php echo res_url('admin/chosen/chosen.jquery.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo res_url('admin/chosen/docsupport/prism.js'); ?>"></script>
</body>
</html>
