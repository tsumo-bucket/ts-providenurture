<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php echo template('title'); ?> | Administration Panel</title>
	<meta charset="utf-8">
	<?php echo template('mythos'); ?>
	<?php echo template('bootstrap'); ?>
	<?php echo template('head'); ?>
	<?php echo template('font_awesome'); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('admin/css/styles.css'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo res_url('admin/css/font.css'); ?>" />
	<!-- include if using sumo bubble look -->
	<!-- <link rel="stylesheet" type="text/css" href="<?php //echo res_url('admin/css/sumo_bubble.css'); ?>" /> -->

	<?php echo template('mythos_utils'); ?>

</head>
<body class="<?php echo uri_css_class(); ?>">
<div id="page-notification">
	<?php echo template('notification'); ?>
</div>
<div class="clearfix" style="height: 50px"></div>
<div class="container">
	<div class="row">
		<?php echo template('content'); ?>
	</div>
</div>
	<?php echo template('bootstrap', 'js'); ?>
	<?php echo template('redactor', 'js'); ?>
	<?php echo template('autofill'); ?>
	<script type="text/javascript" src="<?php echo res_url('admin/js/document.ready.js'); ?>"></script>

	<!-- Include if using sumo bubble login -->
	<!--<script type="text/javascript" src="<?php //echo res_url('admin/js/sumob_bubble.js'); ?>"></script>-->
</body>
</html>
