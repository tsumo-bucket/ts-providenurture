<div class="row">
	<div class="col-md-6">
		<form method="post" enctype="multipart/form-data" data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="acc_username" class="control-label">Email</label>
						<input type="text" name="acc_username" class="form-control" maxlength="150" />
					</div>
					<div class="form-group">
						<label for="acc_first_name" class="control-label">First Name</label>
						<input type="text" name="acc_first_name" class="form-control" maxlength="150" />
					</div>
					<div class="form-group">
						<label for="acc_last_name" class="control-label">Last Name</label>
						<input type="text" name="acc_last_name" class="form-control" maxlength="150" />
					</div>
					<div class="form-group">
						<label for="acc_image" class="control-label">Image</label>
						<div>
							<?php if($account->acc_image): ?>
							<img src="<?php echo base_url($account->acc_image); ?>" class="img-circle" alt="User Image">
							<?php else: ?>
							<!-- <img src="<?php echo $grav_url; ?>" class="img-circle" alt="User Image"> -->
							<img src="<?php echo res_url('admin/images/default_avatar.jpg'); ?>" class="img-circle" alt="User Image">
							<?php endif; ?>
							<br>
							<br>
						</div>
						<div class="btn btn-default btn-file">
            	<i class="fa fa-file-image-o"></i> Image
            	<input type="file" name="acc_image">
            </div>
					</div>
					<div class="form-group">
						<label for="acc_type" class="control-label">Account Type</label>
						<?php
							$options =array();
							if($this->session->userdata('acc_type') == 'developer'){
								$options['developer'] = "Super Admin";
							}
							$options['admin'] = "Admin";
							$options['content creator'] = "Content Creator";
							echo form_dropdown('acc_type',$options, '', 'class="valid form-control"');
						?>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>"><i class="fa fa-times"></i> Cancel</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$('form').floodling('acc_username', '<?php echo addslashes($account->acc_username); ?>');
	$('form').floodling('acc_first_name', '<?php echo addslashes($account->acc_first_name); ?>');
	$('form').floodling('acc_last_name', '<?php echo addslashes($account->acc_last_name); ?>');
	$('form').floodling('acc_type', '<?php echo addslashes($account->acc_type); ?>');

});
</script>
