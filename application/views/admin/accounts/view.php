<div class="row">
	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">
					<i class="glyphicon glyphicon-user"></i>
					User Profile
				</h3>
				<div class="box-tools pull-right">
						<div class="label label-default"><?php if($account->acc_type == 'developer'){ echo 'super admin';  }else{ echo $account->acc_type; } ?></div> 
					</div>
			</div>
			<div class="box-body no-padding">
				<table class="table table-hover table-view">
					<tr>
						<td colspan="2" class="text-center">
							<?php if($account->acc_image): ?>
							<img src="<?php echo base_url($account->acc_image); ?>" class="img-circle" alt="User Image">
							<?php else: ?>
							<!-- <img src="<?php echo $grav_url; ?>" class="img-circle" alt="User Image"> -->
							<img src="<?php echo res_url('admin/images/default_avatar.jpg'); ?>" class="img-circle" alt="User Image">
							<?php endif; ?>
						</td>
					</tr>
					<tr>
						<th>Email</th>
						<td><?php echo $account->acc_username; ?></td>
					</tr>
					<tr>
						<th>Name</th>
						<td><?php echo $account->acc_first_name . ' ' . $account->acc_last_name; ?></td>
					</tr>
					<tr>
						<th>Account Type</th>
						<td><?php if($account->acc_type == 'developer'){ echo 'super admin';  }else{ echo $account->acc_type; } ?></td>
					</tr>
				</table>
			</div>
			<div class="box-footer">
					<div class="pull-right">	
						<a href="<?php echo site_url('admin/accounts/reset_password/' . $account->acc_id); ?>" class="btn btn-default"> <i class="fa fa-key"></i> Reset Password</a>
						<?php if($this->session->userdata('acc_type') == 'developer'): ?>
						<a href="<?php echo site_url('admin/accounts/edit/' . $account->acc_id); ?>" class="btn btn-default"> <i class="fa fa-edit"></i> Edit</a>
						<?php endif; ?>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>"><i class="fa fa-times"></i> Cancel</a>
			</div>
		</div>
	</div>
</div>
