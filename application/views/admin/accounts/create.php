<div class="row">
	<div class="col-md-6">
		<form method="post" enctype="multipart/form-data" data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="acc_username" class="control-label">Email</label>
						<input type="text" name="acc_username" class="form-control" maxlength="150" />
					</div>
					<div class="form-group">
						<label for="acc_password" class="control-label">Password</label>
						<input type="password" name="acc_password" class="form-control" maxlength="150" />
					</div>
					<div class="form-group">
						<label for="acc_password2" class="control-label">Retype Password</label>
						<input type="password" name="acc_password2" class="form-control" maxlength="150" />
					</div>
					<div class="form-group">
						<label for="acc_first_name" class="control-label">First Name</label>
						<input type="text" name="acc_first_name" class="form-control" maxlength="150" />
					</div>
					<div class="form-group">
						<label for="acc_last_name" class="control-label">Last Name</label>
						<input type="text" name="acc_last_name" class="form-control" maxlength="150" />
					</div>
					<div class="form-group">
						<div class="btn btn-default btn-file">
	                    	<i class="fa fa-file-image-o"></i> Image
	                    	<input type="file" name="acc_image">
	                    </div>
					</div>
					<div class="form-group">
						<label for="acc_last_name" class="control-label">Account Type</label>
						<?php
							$options =array();
							if($this->session->userdata('acc_type') == 'developer'){
								$options['developer'] = "Super Admin";
							}
							$options['admin'] = "Admin";
							$options['content creator'] = "Content Creator";
							echo form_dropdown('acc_type',$options, '', 'class="valid form-control"');
						?>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>"><i class="fa fa-times"></i> Cancel</a>
				</div>
			</div>
		</form>
	</div>
</div>