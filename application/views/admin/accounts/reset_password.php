<div class="row">
	<div class="col-md-12 col-lg-6">
		<form method="post" data-submit="submitForm">
			<div class="box box-primary">
				<div class="box-body">				
					<div class="form-group">
						<label for="acc_username" class="control-label">Account</label>
						<?php echo $account->acc_first_name . ' ' . $account->acc_last_name; ?> (<?php echo $account->acc_username ?>)
						<input type="hidden" class="form-control" name="acc_username" />
					</div>
					<div class="form-group">
						<label for="acc_username" class="control-label">New Password</label>
						<?php echo $acc_password; ?>
						<input type="hidden" name="acc_password" value="<?php echo $acc_password; ?>" />
					</div>
					<div class="form-group">
						<span class="label label-warning">Please copy the password above.</span>
					</div>
					<div class="form-group">
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>"><i class="fa fa-times"></i> Cancel</a>
				</div>
			</div>
		</form>
	</div>
</div>
