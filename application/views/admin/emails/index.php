<form class="form-inline" role="form" method="post">
	<div class="box">
		<div class="box-header">
			<div class="box-tools clearfix">
				<div class="btn-group">
					<button class="btn btn-flat btn-default btn-sm" name="form_mode" value="delete">
						<i class="fa fa-trash-o"></i>
					</button>
				</div>
				<!-- <div class="pull-right">
					<div class="form-group">
						<label for="field" class="sr-only">Fields</label>
						<select name="field" id="" class="form-control input-sm">
							<option value="part_name"></option>
							<option value="part_name"></option>
						</select>
					</div>
					<div class="form-group input-group">
						<label for="keywords" class="sr-only">Keywords</label>
						<input type="text" class="form-control input-sm" name="keywords" placeholder="Filter" value="">
						<div class="input-group-btn">
							<button class="btn btn-flat btn-primary btn-sm" name="search" type="submit" value="search">
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</div> -->
			</div>
		</div>
		<?php if( $emails->num_rows() ): ?>
			<div class="box-body no-padding table-responsive">
				<table class="table table-list table-hover">
					<thead>
						<tr>
							<th class="center skip-sort" style="width: 30px;">
								<input type="checkbox" class="select-all" value="eml_ids" />
							</th>
							<th>Mail To</th>
							<th>Date Sent</th>
							<th>Subject</th>
							<th>Status</th>
							<th style="width: 120px">Action</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach($emails->result() as $email)
					{
						?>
						<tr>
							<td class="center"><input type="checkbox" name="eml_ids[]" value="<?php echo $email->eml_id; ?>" /></td>
							<td><?php echo $email->eml_mail_to; ?></td>
							<td><?php echo format_datetime($email->eml_date_sent); ?></td>
							<td><?php echo character_limiter($email->eml_subject, 100); ?></td>
							<td><?php echo $email->eml_status; ?></td>
							<td>
								<div class="btn-group">
									<button class="btn btn-primary btn-flat btn-disabled btn-table-action">Action</button>
									<button class="btn btn-primary btn-flat dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu table-action" role="menu">
										<li>
											<a href="<?php echo admin_url('emails/view/' . $email->eml_id); ?>"><i class="fa fa-eye"></i> View</a>
										</li>
									</ul>
								</div>
							</td>
						</tr>
						<?php
					}
					?>
					</tbody>
				</table>
			</div>
			<div class="box-footer clearfix">
				<?php echo $emails_pagination; ?>
			</div>
		<?php else: ?>
			<div class="box-body no-padding">
				<p class="text-center" style="padding: 10px;">
					No emails found.
				</p>
			</div>
		<?php endif; ?>
	</div>	
</form>
