<form method="post">
	<div class="col-sm-3 col-sm-push-9 block">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title">Menu</h3>
			</div>
			<div class="box-body">
				<?php echo create_admin_back_btn($this->router->fetch_class(), 'Cancel'); ?>
			</div>
		</div>
	</div>
	<div class="col-sm-9 col-sm-pull-3">
		<div class="box">
			<div class="box-body">
				<div class="form-group">
					<label for="eml_mail_to" class="control-label">Mail To</label>
					<input class="form-control" type="text" name="eml_mail_to" size="80" maxlength="150" value="" />
				</div>
				<div class="form-group">
					<label for="eml_cc" class="control-label">Cc</label>
					<input class="form-control" type="text" name="eml_cc" size="80" maxlength="150" value="" />
				</div>
				<div class="form-group">
					<label for="eml_bcc" class="control-label">Bcc</label>
					<input class="form-control" type="text" name="eml_bcc" size="80" maxlength="150" value="" />
				</div>
				<div class="form-group">
					<label for="eml_subject" class="control-label">Subject</label>
					<input class="form-control" type="text" name="eml_subject" size="80" maxlength="150" value="" />
				</div>
				<div class="form-group">
					<label for="eml_message" class="control-label">Message</label>
					<textarea class="form-control redactor" name="eml_message" rows="5" cols="80"></textarea>
				</div>
				<div class="form-group">
					<label for="eml_date_sent" class="control-label">Date Sent</label>
					<input class="form-control" type="text" name="eml_date_sent" size="" maxlength="" value="" />
				</div>
				<div class="form-group">
					<input type="submit" name="submit" value="Submit" class="btn btn-primary btn-flat btn-small" />
				</div>
			</div>
		</div>
	</div>
</form>
