<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('countries/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>
	<br>
	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage Countries</h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<form role="form" method="get">
							<input type="text" class="form-control input-sm" name="search" placeholder="Search" value="<?php echo $this->input->get('search') ?>" />
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</form>
					</div>
				</div>
			</div>
			<?php if($countries->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">
						<div class="btn btn-default btn-sm checkbox-toggle select-all" data-fields="cnt_ids">
							<i class="fa fa-square-o"></i>
						</div>
						<div class="btn-group">
							<button class="btn btn-default btn-sm action-delete" name="form_mode" value="delete"> 
								<i class="fa fa-trash-o"></i> Delete
							</button>
						</div>
						<div class="pull-right">
							<?php echo $countries_pagination; ?>
						</div>
					</div>
					<table class="table table-striped table-hover">
						<tbody>
						<?php foreach($countries->result() as $country): ?>
							<tr data-primkey="<?php echo $country->cnt_id; ?>">
								<td style="width:50px" class="center">
									<input type="checkbox" name="cnt_ids[]" value="<?php echo $country->cnt_id; ?>" />
								</td>
								
								<td>
									<a href="<?php echo admin_url('countries/edit/'.$country->cnt_id); ?>"><?php echo $country->cnt_name?></a>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No countries found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>