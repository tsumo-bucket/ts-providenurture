<div class="row">
	<div class="col-md-6">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="cnt_name" class="control-label">Name</label>
						<input type="text" name="cnt_name" class="form-control" />
					</div>
					<div class="form-group">
						<label for="cnt_short_name" class="control-label">Short Name</label>
						<input type="text" name="cnt_short_name" class="form-control" />
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>