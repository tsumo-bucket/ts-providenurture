<form class="form-horizontal" method="post" enctype="multipart/form-data">
	<div class="col-sm-12">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title"><?php echo template('title'); ?></h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-2 control-label">Option</label>
					<div class="col-sm-5">
						<h6><?php echo $site_options->opt_name; ?></h6>
					</div>
				</div>
				<div class="form-group">
					<label for="opt_value" class="col-sm-2 control-label">Value</label>
					<div class="col-sm-5">
						<?php if($site_options->opt_type == "image" ): ?>
						<input class="form-control" type="file" name="upload_pic"/>
						<img src="<?php echo base_url($site_options->opt_value); ?>" alt="" class="lightbox"/>
						<?php else: ?>
						<input class="form-control" type="text" name="opt_value" value="<?php echo $site_options->opt_value; ?>" style="width:90%" />
						<?php endif; ?>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-5 col-sm-offset-2">
						<input type="hidden" name="opt_slug" value="<?php echo $site_options->opt_slug ?>" />
						<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
						<input type="button" value="Cancel" onclick="redirect('<?php echo site_url('admin/site_options/'); ?>');" class="btn btn-default" />
					</div>
				</div>
			</div>
		</div>
	</div>
</form>