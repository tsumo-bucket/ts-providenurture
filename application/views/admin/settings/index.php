<div class="col-sm-3 col-sm-push-9 block">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Menu</h3>
		</div>
		<div class="box-body">
			<ul class="nav nav-pills nav-stacked">
				<li class="active"><a href="<?php echo site_url('admin/settings'); ?>">System Settings</a></li>
				<li><a href="<?php echo site_url('admin/settings/phpinfo'); ?>">PHP System Information</a></li>
			</ul>
		</div>
	</div>
</div>
<div class="col-sm-9 col-sm-pull-3">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title"><?php echo template('title'); ?></h3>
		</div>
		<div class="box-body no-padding">
			<table class="table table-hover">
				<tr>
					<th scope="row">Mythos CMS</th>
					<td><?php echo MYTHOS_CMS_VERSION; ?></td>
				</tr>
				<tr>
					<th scope="row">Mythos</th>
					<td><?php echo MYTHOS_VERSION; ?> (<?php echo MYTHOS_VERSION_NAME; ?>)</td>
				</tr>
				<tr>
					<th scope="row">PHP</th>
					<td><?php echo phpversion(); ?></td>
				</tr>
				<tr>
					<th scope="row">Base Path</th>
					<td><?php echo FCPATH; ?></td>
				</tr>
				<tr>
					<th scope="row">Models</th>
					<td>
						<?php
						if(count($models) > 0)
						{
						?>
						<ul>
							<?php
							foreach ($models as $model)
							{
							?>
							<li><?php echo ucfirst($model); ?></li>
							<?php
							}
							?>
						</ul>
						<?php
						}
						?>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
