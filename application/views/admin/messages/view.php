<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('messages/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				</h3>
				<div class="box-tools pull-right">
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Content</h3>
                	<div><?php echo $message->msg_content ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>User</h3>
                	<div><?php echo $message->msg_user ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Status</h3>
                	<div><?php echo $message->msg_status ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Created</h3>
                	<div><?php echo $message->msg_date_created ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('messages/edit/'.$message->msg_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>