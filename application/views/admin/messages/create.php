<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="usr_id" class="control-label">User</label>
						<select name="usr_id" class="form-control">
					<?php foreach($usr_ids->result() as $usr_id): ?>
						<option value="<?php echo $usr_id->usr_id ?>"><?php echo $usr_id->usr_type ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="msg_content" class="control-label">Content</label>
						<textarea name="msg_content" style="width:100%" class="form-control redactor"></textarea>
					</div>
					<div class="form-group">
						<label for="msg_user" class="control-label">User</label>
						<input type="text" name="msg_user" class="form-control" />
					</div>
					<div class="form-group">
						<label for="msg_status" class="control-label">Status</label>
						<select name="msg_status" class="form-control">
							<option value="unread">Unread</option>
							<option value="read">Read</option>
						</select>
					</div>
					<div class="form-group">
						<label for="cnv_id" class="control-label">Conversation</label>
						<select name="cnv_id" class="form-control">
					<?php foreach($cnv_ids->result() as $cnv_id): ?>
						<option value="<?php echo $cnv_id->cnv_id ?>"><?php echo $cnv_id->cnv_slug ?></option>
					<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>