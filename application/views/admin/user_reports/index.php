<div class="row">
	<div class="col-md-3">
		<!-- <a href="<?php echo admin_url('user_reports/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a> -->
		<br>
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Sort By</h3>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="<?php echo admin_url('user_reports?order_by=rep_user') ?>"><i class="fa fa-circle-o"></i>Users</a>
					</li>
					<li>
						<a href="<?php echo admin_url('user_reports') ?>"><i class="fa fa-circle-o"></i>Date Reported</a>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage User Reports</h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<form role="form" method="get">
							<input type="text" class="form-control input-sm" name="search" placeholder="Search" value="<?php echo $this->input->get('search') ?>" />
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</form>
					</div>
				</div><!-- /.box-tools -->
			</div>
			<?php if($user_reports->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">
					<!-- Check all button -->
						<div class="btn btn-default btn-sm checkbox-toggle select-all" data-fields="rep_ids">
							<i class="fa fa-square-o"></i>
						</div>
						<div class="btn-group">
							<button class="btn btn-default btn-sm action-delete" name="form_mode" value="delete"> 
								<i class="fa fa-trash-o"></i> Delete All
							</button>
						</div><!-- /.btn-group --> 
						<div class="pull-right">
							<?php echo $user_reports_pagination; ?>
						</div><!-- /.pull-right -->
					</div>
					<table class="table table-striped table-hover">
						<thead>
							<th style="width:50px" class="center"></th>
							<th>User</th>
							<th>Reported By</th>
							<th>Reason</th>
							<th>Other Reasons</th>
							<th>Evidence</th>
							<th>Date Reported</th>
						</thead>
						<tbody>
						<?php foreach($user_reports->result() as $user_report): ?>
							<?php $user_report->reported = $this->user_model->get_one($user_report->rep_user);  ?>
							<tr data-primkey="<?php echo $user_report->rep_id; ?>">
								<td style="width:50px" class="center">
									<input type="checkbox" name="rep_ids[]" value="<?php echo $user_report->rep_id; ?>" />
								</td>
								
								<td>
									<a href="<?php echo admin_url('user_reports/view/'.$user_report->rep_id); ?>">
										<?php echo $user_report->reported->usr_screen_name; ?>
									</a>
								</td>
								
								<td>
									<?php echo $user_report->usr_screen_name; ?>
								</td>
								<td>
									<?php echo $user_report->rep_reason; ?>
								</td>
								<td>
									<?php echo word_limiter($user_report->rep_other_reason, 10); ?>
								</td>
								<td>
									<?php if($user_report->rep_evidence): ?>
										<img style="max-width: 100px;" src="<?php echo base_url($user_report->rep_evidence);  ?>" alt="">
									<?php else: ?>
										No evidence uploaded
					       			<?php endif; ?>
								</td>
								<td>
									<?php echo format_datetime($user_report->rep_date_created, 'M d, Y h:i:s A', 'Asia/Manila'); ?>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No user reports found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>