<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('user_reports/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
					User Reported: <?php echo $user_report->reported->usr_screen_name; ?> 
				</h3>
				<div class="box-tools pull-right">
                </div>
			</div>
			<div class="box-body">
				<table class="table table-striped table-hover">
					<tbody>
						<tr>
							<th>Reported By</th>
							<td><a href="<?php echo admin_url('users/view/'.$user_report->usr_id); ?>"><?php echo $user_report->usr_screen_name; ?></a></td>						
						</tr>
						<tr>
							<th>Reason</th>
							<td><?php echo $user_report->rep_reason; ?></td>
						</tr>
						<tr>
							<th>Other Reason</th>
							<td><?php echo $user_report->rep_other_reason; ?></td>
						</tr>
						<tr>
							<th>Evidence</th>
							<?php if($user_report->rep_evidence): ?>
								<td>
									<img style="max-width: 100%;" src="<?php echo base_url($user_report->rep_evidence);  ?>" alt="">
								</td>
							<?php else: ?>
								<td>No evidence uploaded</td>
					        <?php endif; ?>
						</tr>
						<tr>
							<th>Date Reported</th>
							<td><?php echo format_datetime($user_report->rep_date_created, 'M d, Y h:i:s A', 'Asia/Manila'); ?></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
			<!-- 	<a class="btn btn-default" href="<?php echo admin_url('user reports/edit/'.$user_report->rep_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a> -->
			</div>
		</div>
	</div>
</div>