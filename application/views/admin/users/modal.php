<style type="text/css">
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
  margin: 0 auto;
}
</style>
<div class="modal-dialog modal-md">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title" id="myModalLabel">Choose Reason</h4>
		</div>
		<div class="modal-body">
			<?php 
				$attributes = array('class' => 'form-horizontal');
				echo form_open(site_url('admin/users/edit/' . $id . '/rejected/'), $attributes); 
			?>
				<div class="form-group">
					<label for="eml_name" class="col-sm-2 control-label">Reason</label>
					<div class="col-sm-10">
						<?php 
						$options = array();
						$options['custom'] = 'Custom Message';
						$options['lacking-and-repetitious-texts'] = 'Lacking and Repetitious Texts';
						$options['inappropriate-photos'] = 'Inappropriate Photos';

						echo form_dropdown('eml_slug', $options, '', 'class="form-control"');
						?>
					</div>
				</div>
				<div class="form-group customMessage">
					<label for="eml_message" class="col-sm-2 control-label">Message</label>
					<div class="col-sm-10"><textarea class="form-control" name="eml_message" placeholder="Type the message here" rows="5" cols="80"></textarea></div>
				</div>
				<div class="form-group">
					<div class="text-right col-sm-12">
						<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
					</div>
				</div>		
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#rejectModal select').on('change', function() {
		var select = $(this);
		var value = select.find(':selected').val();

		if (value == 'custom') {
			$('#rejectModal .customMessage').slideDown();
		} else {
			$('#rejectModal .customMessage').slideUp();
		}
	});
</script>