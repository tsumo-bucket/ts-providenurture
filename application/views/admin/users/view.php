<style type="text/css">
	.image-holder {
    height: 150px;
    width: 159px;
    overflow: hidden;
    display: inline-block;
	}
</style>
<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-header">
				<h3 class="box-title"></h3>
				<div class="box-tools pull-right">
					<span class="label label-default">Type: <?php echo ($user->usr_type == 'sugar' ? 'provider' : 'nurturer'); ?></span>
					<span class="label label-default">Status: <?php echo $user->usr_status ?></span>
					<span class="label label-default">Verified: <?php echo $user->usr_verified ?></span>
				</div>
			</div>
			<div class="box-body"> 
				<div class="no-margin-10">
					<table class="table table-striped table-hover">
						<tbody>
							<tr>
								<th>Name</th>
								<td><?php echo $user->usr_first_name.' '.$user->usr_last_name  ?></td>
							</tr>
							<tr>
								<th>Screen Name</th>
								<td><?php echo $user->usr_screen_name ?></td>
							</tr>
							<tr>
								<th>Email</th>
								<td><?php echo $user->usr_email ?></td>
							</tr>
							<tr>
								<th>Email Verified</th>
								<td><?php echo $user->usr_verified ?></td>
							</tr>
							<tr>
								<th>Socially Verified</th>
								<td><?php echo $user->usr_social_verified ?></td>
							</tr>
							<tr>
								<th>Birthdate</th>
								<?php
									$from = new DateTime($user->usr_birthyear.'-'.$user->usr_birthmonth.'-'.$user->usr_birthdate);
        					$to = new DateTime('today');

        					$age = $from->diff($to)->y;
        				?>
								<td><?php echo $user->usr_birthday.' ('.$age.')';  ?></td>
							</tr>
							<tr>
								<th>Gender</th>
								<td><?php echo ucfirst($user->usr_gender); ?></td>
							</tr>
							<tr>
								<th>Location</th>
								<td><?php echo $user->usr_location; ?></td>
							</tr>						
							<tr>
								<th>Interested</th>
								<?php 
									if($user->usr_interested == 'both'){ $user->usr_interested = 'both male and female'; } 
									$this->db->where('cnt_id', $user->usr_interested_country);
									$countries = $this->db->get('country');

									$country_text = '';

									if($countries->num_rows() > 0){
										$country = $countries->row();
										$country_text = ' in '.$country->cnt_name; 
									}
								?>
								<td><?php echo ucfirst($user->usr_interested).' between ages '.$user->usr_interested_age_from.' to '.$user->usr_interested_age_to.$country_text; ?></td>
							</tr>
							<?php if($user->usr_signup_step != 'complete'): ?>
							<tr>
								<th>Signup Step</th>
								<td><?php echo $user->usr_signup_step; ?></td>
							</tr>
							<?php endif; ?>
							<tr>
								<th>Signup Date</th>
								<td><?php echo format_datetime($user->usr_created_date, 'Y-m-d h:i:s A', 'Asia/Manila'); ?></td>
							</tr>
							<?php if($user->usr_status == 'approved'): ?>
							<tr>
								<th>Approved Date</th>
								<td><?php echo format_datetime($user->usr_approved_date, 'Y-m-d h:i:s A', 'Asia/Manila'); ?></td>
							</tr>
							<?php endif; ?>
							<tr>
								<th>Modified Date</th>
								<td><?php echo format_datetime($user->usr_modified_date, 'Y-m-d h:i:s A', 'Asia/Manila'); ?></td>
							</tr>
							<tr>
								<th>Last Login</th>
								<td><?php echo format_datetime($user->usr_last_login, 'Y-m-d h:i:s A', 'Asia/Manila'); ?></td>
							</tr>
							<?php $this->db->where('src_id', $user->src_id); $sources = $this->db->get('source'); ?>
							<?php if($sources->num_rows() > 0): ?>
							<tr>
								<th>Source</th>
								<td><?php echo $sources->row()->src_name; ?></td>
							</tr>
							<?php endif; ?>
						</tbody>
					</table>
					<div class="images-holder col-md-12">
					<?php if($images->num_rows() > 0): ?>
						<?php foreach($images->result() as $image): ?>
							<a href="#" data-image="<?php echo base_url($image->img_image); ?>" data-url="<?php echo admin_url('images/delete/'.$image->img_id); ?>" class="user-images">
							<div class="image-holder" style="background: url('<?php echo base_url($image->img_image); ?>') center center !important; background-size: cover !important"></div>
							</a>
						<?php endforeach; ?>
					<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="pull-right">
					<?php if($user->usr_verified == 'no'): ?>
						<a href="<?php echo admin_url('users/resend?usr_id='.$user->usr_id.'&usr_email='.$user->usr_email); ?>" class="btn btn-primary">
							Resend Verification
						</a>
					<?php endif; ?>
					<?php if($user->usr_signup_step != 'done' && $user->usr_status == 'pending'): ?>
						<a href="<?php echo admin_url('users/edit/'.$user->usr_id.'/force_approved'); ?>" class="btn btn-primary">
							Force Approve
						</a>
					<?php endif; ?>
					<?php if($user->usr_signup_step == 'done' && $user->usr_status == 'pending'): ?>
					<!-- <a href="<?php echo site_url('admin/users/reject_modal/'.$user->usr_id); ?>" class="btn btn-default" data-toggle="modal" data-target="#rejectModal">
						Reject
					</a> -->
					<a href="<?php echo admin_url('users/edit/'.$user->usr_id.'/rejected'); ?>" class="btn btn-default">
						Reject
					</a>
					<a href="<?php echo admin_url('users/edit/'.$user->usr_id.'/approved'); ?>" class="btn btn-primary">
						Approve
					</a>
					<?php endif; ?>
					<?php if($user->usr_status == 'approved'): ?>
					<a href="<?php echo admin_url('users/edit/'.$user->usr_id.'/suspended'); ?>" class="btn btn-default">
						Freeze
					</a>
					<?php endif; ?>
					<?php if($user->usr_status == 'suspended'): ?>
					<a href="<?php echo admin_url('users/edit/'.$user->usr_id.'/unfreeze'); ?>" class="btn btn-default">
						Unfreeze
					</a>
					<?php endif; ?>
				</div>
				<a class="btn btn-default" href="<?php echo back_href(); ?>">
					Cancel
				</a>
			</div>
		</div>
		<?php if($reports->result()): ?>
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Reports</h3>
			</div>
			<div class="box-body">
				<table class="table table-striped table-hover">
					<tbody>
						<?php if($reports->num_rows() > 0): ?>
						<tr>
							<th>Screen Name</th>
							<th>Date Reported</th>
						</tr>
							<?php foreach($reports->result() as $report): ?>
						<tr>
							<td>
								<a href="<?php echo admin_url('user_reports/view/'.$report->rep_id); ?>"><?php echo $report->usr_screen_name; ?></a>
								</td>
							<td><?php echo format_datetime($report->rep_date_created, 'M d, Y h:i:s A', 'Asia/Manila'); ?></td>
						</tr>
							<?php endforeach; ?>
						<?php endif; ?>
					</tbody>
				</table>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<div class="col-md-6">
		<?php if($info): ?>
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Info</h3>
			</div>
			<div class="box-body no-padding">
				<table class="table table-striped table-hover">
					<tbody>
						<tr>
							<th>Height</th>
							<td class="<?php echo $info->inf_height == "" ? 'error' : ''; ?>">
								<?php echo $info->inf_height;
									if($info->inf_height):
										echo ' '.$info->inf_height_unit;
								?> 
							<?php endif; ?>
							</td>
						</tr>
						<tr>
							<th>Body Type</th>
							<td class="<?php echo $info->inf_body_type == "" ? 'error' : ''; ?>"><?php echo $info->inf_body_type; ?></td>
						</tr>
						<tr>
							<th>Ethnicity</th>
							<td class="<?php echo $info->inf_ethnicity == "" ? 'error' : ''; ?>"><?php echo $info->inf_ethnicity; ?></td>
						</tr>
						<tr>
							<th>Relationship</th>
							<td class="<?php echo $info->inf_relationship == "" ? 'error' : ''; ?>"><?php echo $info->inf_relationship; ?></td>
						</tr>
						<tr>
							<th>Children</th>
							<td class="<?php echo $info->inf_children == "" ? 'error' : ''; ?>"><?php echo $info->inf_children; ?></td>
						</tr>
						<tr>
							<th>Smoke</th>
							<td class="<?php echo $info->inf_smoke == "" ? 'error' : ''; ?>"><?php echo $info->inf_smoke; ?></td>
						</tr>
						<tr>
							<th>Drink</th>
							<td class="<?php echo $info->inf_drink == "" ? 'error' : ''; ?>"><?php echo $info->inf_drink; ?></td>
						</tr>
						<tr>
							<th>Language</th>
							<td class="<?php echo $info->inf_language == "" ? 'error' : ''; ?>"><?php echo $info->inf_language; ?></td>
						</tr>
						<tr>
							<th>Education</th>
							<td class="<?php echo $info->inf_education == "" ? 'error' : ''; ?>"><?php echo $info->inf_education; ?></td>
						</tr>
						<tr>
							<th>Occupation</th>
							<td class="<?php echo $info->inf_occupation == "" ? 'error' : ''; ?>"><?php echo $info->inf_occupation; ?></td>
						</tr>
						<tr>
							<th>Spending Habit</th>
							<?php 
								$inf_spending_habits = 'NA';

								switch ($info->inf_spending_habits) {
									case '0':
										if($user->usr_type == 'sugar'){
											$inf_spending_habits = "I'm open to discussion";
										}else{
											$inf_spending_habits = "Negotiable: No Budget Specified";
										}
										break;

									case '1':
										if($user->usr_type == 'sugar'){
											$inf_spending_habits = "Up to $250 per month";
										}else{
											$inf_spending_habits = "Minimal: Up to $250 per month";
										}
										break;

									case '2':
										if($user->usr_type == 'sugar'){
											$inf_spending_habits = "Up to $500 per month";
										}else{
											$inf_spending_habits = "Base Support: Up to $500 per month";
										}
										break;

									case '3':
										if($user->usr_type == 'sugar'){
											$inf_spending_habits = "Up to $1,000 per month";
										}else{
											$inf_spending_habits = "Moderate Support: Up to $1,000 per month";
										}
										break;

									case '4':
										if($user->usr_type == 'sugar'){
											$inf_spending_habits = "Up to $2,000 per month";
										}else{
											$inf_spending_habits = "Mid Support: Up to $2,000 per month";
										}
										break;

									case '5':
										if($user->usr_type == 'sugar'){
											$inf_spending_habits = "Up to $4,000 per month";
										}else{
											$inf_spending_habits = "Easy Living: Up to $4,000 per month";
										}
										break;

									case '6':
										if($user->usr_type == 'sugar'){
											$inf_spending_habits = "Up to $8,000 per month";
										}else{
											$inf_spending_habits = "Good Living: Up to $8,000 per month";
										}
										break;

									case '7':
										if($user->usr_type == 'sugar'){
											$inf_spending_habits = "Above $8,000 per month";
										}else{
											$inf_spending_habits = "High Roller: Above $8,000 per month";
										}
										break;
									
									default:
										break;
								}
							?>
							<td class="<?php echo $inf_spending_habits == "" ? 'error' : ''; echo $inf_spending_habits == "NA" ? 'error' : ''; ?>"><?php echo $inf_spending_habits; ?></td>
						</tr>
						<?php if($user->usr_type == 'sugar'): ?>
						<tr>
							<th>Net Worth</th>
							<?php 
								$inf_net_worth = 'NA';

								switch ($info->inf_net_worth) {
									case '0':
										$inf_net_worth = 'Enough to Make my SugarBaby Feel Secure';
										break;

									case '1':
										$inf_net_worth = '$100,000';
										break;

									case '2':
										$inf_net_worth = '$200,000';
										break;

									case '3':
										$inf_net_worth = '$500,000';
										break;

									case '4':
										$inf_net_worth = '$750,000';
										break;

									case '5':
										$inf_net_worth = '$1 million';
										break;

									case '6':
										$inf_net_worth = '$2 million';
										break;

									case '7':
										$inf_net_worth = '$5 million';
										break;

									case '8':
										$inf_net_worth = '$10 million';
										break;

									case '9':
										$inf_net_worth = '$50 million';
										break;

									case '10':
										$inf_net_worth = '$100 million';
										break;

									case '11':
										$inf_net_worth = 'Above $100 million';
										break;
									
									default:
										break;
								}
							?>
							<td class="<?php echo $inf_net_worth == "" ? 'error' : ''; echo $inf_net_worth == "NA" ? 'error' : ''; ?>"><?php echo $inf_net_worth; ?></td>
						</tr>
						<tr>
							<th>Yearly Income</th>
								<?php 
								$inf_yearly_income = 'NA';

								switch ($info->inf_yearly_income) {
									case '0':
										$inf_yearly_income = 'Enough to Support my SugarBaby';
										break;

									case '1':
										$inf_yearly_income = '$50,000';
										break;

									case '2':
										$inf_yearly_income = '$75,000';
										break;

									case '3':
										$inf_yearly_income = '$100,000';
										break;

									case '4':
										$inf_yearly_income = '$150,000';
										break;

									case '5':
										$inf_yearly_income = '$200,000';
										break;

									case '6':
										$inf_yearly_income = '$250,000';
										break;

									case '7':
										$inf_yearly_income = '$300,000';
										break;

									case '8':
										$inf_yearly_income = '$400,000';
										break;

									case '9':
										$inf_yearly_income = '$500,000';
										break;

									case '10':
										$inf_yearly_income = '$1 million';
										break;

									case '11':
										$inf_yearly_income = 'Above $1 million';
										break;
									
									default:
										break;
								}
							?>
							<td class="<?php echo $inf_yearly_income == "" ? 'error' : ''; echo $inf_yearly_income == "NA" ? 'error' : '';?>"><?php echo $inf_yearly_income; ?></td>
						</tr>
						<?php endif; ?>
						<tr>
							<th>Prefered Range of Support</th>
							<td class="<?php echo $info->inf_preferred_range_from == "" ? 'error' : ''; echo $info->inf_preferred_range_to == "" ? 'error' : ''; ?>"><?php echo $info->inf_preferred_range_from.'-'.$info->inf_preferred_range_to; ?></td>
						</tr>
						<tr>
							<th>Quality Time</th>
							<td class="<?php echo $info->inf_quality_time == "" ? 'error' : ''; ?>"><?php echo $info->inf_quality_time; ?></td>
						</tr>
						<tr>
							<th>Gifts</th>
							<td class="<?php echo $info->inf_gifts == "" ? 'error' : ''; ?>"><?php echo $info->inf_gifts; ?></td>
						</tr>
						<?php if($info->inf_other != ''): ?>
						<tr>
							<th>Other</th>
							<td class="<?php echo $info->inf_other == "" ? 'error' : ''; ?>"><?php echo $info->inf_other; ?></td>
						</tr>
						<?php endif; ?>
						<tr>
							<th>Staycation</th>
							<td class="<?php echo $info->inf_staycations == "" ? 'error' : ''; ?>"><?php echo $info->inf_staycations; ?></td>
						</tr>
						<tr>
							<th>The High Life</th>
							<td class="<?php echo $info->inf_high_life == "" ? 'error' : ''; ?>"><?php echo $info->inf_high_life; ?></td>
						</tr>
						<tr>
							<th>I am a simple baby</th>
							<td class="<?php echo $info->inf_simple == "" ? 'error' : ''; ?>"><?php echo $info->inf_simple; ?></td>
						</tr>
						<tr>
							<th>Short/Long Term</th>
							<td class="<?php echo $info->inf_relationship_length == "" ? 'error' : ''; ?>"><?php echo $info->inf_relationship_length; ?></td>
						</tr>
						<tr>
							<th>NSA/Exclusive</th>
							<td class="<?php echo $info->inf_relationship_loyalty == "" ? 'error' : ''; ?>"><?php echo $info->inf_relationship_loyalty; ?></td>
						</tr>
						<tr>
							<th>Privacy Expectation</th>
							<td class="<?php echo $info->inf_privacy_expectations == "" ? 'error' : ''; ?>"><?php echo $info->inf_privacy_expectations; ?></td>
						</tr>
						<tr>
							<th>Relationship Preference</th>
							<td class="<?php echo $info->inf_relationship_preference == "" ? 'error' : ''; ?>"><?php echo $info->inf_relationship_preference; ?></td>
						</tr>
						<tr>
							<th>Sexual Limits</th>
							<td class="<?php echo $info->inf_sexual_limit == "" ? 'error' : ''; ?>"><?php echo $info->inf_sexual_limit; ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<?php endif; ?>
		<?php if($preferences): ?>
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Questions</h3>
			</div>
			<div class="box-body no-padding">
				<table class="table table-striped table-hover">
					<tbody>
						<tr>
							<td class="<?php echo $user->usr_description == "" ? 'error' : ''; ?>">
								<h5>Description</h5>
								<div>
									<?php echo $user->usr_description ?>
								</div>
							</td>
						</tr>
						<?php foreach($preferences as $preference): ?>
						<tr>
							<td class="<?php echo $preference->prf_content == "" ? 'error' : ''; ?>">
								<h5><?php echo $preference->que_question; ?></h5>
								<div>
									<?php echo $preference->prf_content; ?>
								</div>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<?php endif; ?>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('a.user-images').on('click', function(event){
			event.preventDefault();
			$('#user-image-modal img').attr("src", $(this).data('image'));
			$('#user-image-modal #user-image-delete-btn').attr("href", $(this).data('url'));
			$('#user-image-modal').modal('show');
		});

		$('a.user-status-update').on('click', function(event){
			event.preventDefault();
		});
	});
</script>