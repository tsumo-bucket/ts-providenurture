<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="usr_type" class="control-label">Type</label>
						<select name="usr_type" class="form-control">
							<option value="baby">Baby</option>
							<option value="sponsor">Sponsor</option>
						</select>
					</div>
					<div class="form-group">
						<label for="usr_first_name" class="control-label">First Name</label>
						<input type="text" name="usr_first_name" class="form-control" />
					</div>
					<div class="form-group">
						<label for="usr_last_name" class="control-label">Last Name</label>
						<input type="text" name="usr_last_name" class="form-control" />
					</div>
					<div class="form-group">
						<label for="usr_screen_name" class="control-label">Screen Name</label>
						<input type="text" name="usr_screen_name" class="form-control" />
					</div>
					<div class="form-group">
						<label for="usr_email" class="control-label">Email</label>
						<input type="text" name="usr_email" class="form-control" />
					</div>
					<div class="form-group">
						<label for="usr_password" class="control-label">Password</label>
						<input type="text" name="usr_password" class="form-control" />
					</div>
					<div class="form-group">
						<label for="usr_birthdate" class="control-label">Birthdate</label>
						<div class="row">
							<div class='col-md-6 col-xs-8'>
								<input type="text" name="usr_birthdate" class="form-control sumodate" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="cty_id" class="control-label">City</label>
						<select name="cty_id" class="form-control">
					<?php foreach($cty_ids->result() as $cty_id): ?>
						<option value="<?php echo $cty_id->cty_id ?>"><?php echo $cty_id->reg_id ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="usr_status" class="control-label">Status</label>
						<select name="usr_status" class="form-control">
							<option value="pending">Pending</option>
							<option value="approved">Approved</option>
							<option value="rejected">Rejected</option>
							<option value="deactivated">Deactivated</option>
						</select>
					</div>
					<div class="form-group">
						<label for="usr_gender" class="control-label">Gender</label>
						<select name="usr_gender" class="form-control">
							<option value="male">Male</option>
							<option value="female">Female</option>
							<option value="trans male">Trans male</option>
							<option value="trans female">Trans female</option>
						</select>
					</div>
					<div class="form-group">
						<label for="usr_interested" class="control-label">Interested</label>
						<select name="usr_interested" class="form-control">
							<option value="male">Male</option>
							<option value="female">Female</option>
							<option value="both">Both</option>
						</select>
					</div>
					<div class="form-group">
						<label for="usr_interested_age_from" class="control-label">Interested Age From</label>
						<input type="text" name="usr_interested_age_from" class="form-control" />
					</div>
					<div class="form-group">
						<label for="usr_interested_age_to" class="control-label">Interested Age To</label>
						<input type="text" name="usr_interested_age_to" class="form-control" />
					</div>
					<div class="form-group">
						<label for="usr_description" class="control-label">Description</label>
						<textarea name="usr_description" style="width:100%" class="form-control redactor"></textarea>
					</div>
					<div class="form-group">
						<label for="usr_signup_step" class="control-label">Signup Step</label>
						<select name="usr_signup_step" class="form-control">
							<option value="initial">Initial</option>
							<option value="photo">Photo</option>
							<option value="info">Info</option>
							<option value="preference">Preference</option>
							<option value="done">Done</option>
						</select>
					</div>
					<div class="form-group">
						<label for="usr_verified" class="control-label">Verified</label>
						<select name="usr_verified" class="form-control">
							<option value="yes">Yes</option>
							<option value="no">No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="usr_password_reset" class="control-label">Password Reset</label>
						<input type="text" name="usr_password_reset" class="form-control" />
					</div>
					<div class="form-group">
						<label for="usr_created_date" class="control-label">Created Date</label>
						<div class="row">
							<div class='col-md-6 col-xs-8'>
								<input type="text" name="usr_created_date" class="form-control sumodate" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="usr_modified_date" class="control-label">Modified Date</label>
						<div class="row">
							<div class='col-md-6 col-xs-8'>
								<input type="text" name="usr_modified_date" class="form-control sumodate" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="usr_last_login" class="control-label">Last Login</label>
						<input type="text" name="usr_last_login" class="form-control datetime" />
					</div>
					<div class="form-group">
						<label for="usr_token" class="control-label">Token</label>
						<input type="text" name="usr_token" class="form-control" />
					</div>
					<div class="form-group">
						<label for="usr_token_id" class="control-label">Token Id</label>
						<input type="text" name="usr_token_id" class="form-control" />
					</div>
					<div class="form-group">
						<label for="img_id" class="control-label">Image</label>
						<select name="img_id" class="form-control">
					<?php foreach($img_ids->result() as $img_id): ?>
						<option value="<?php echo $img_id->img_id ?>"><?php echo $img_id->usr_id ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="src_id" class="control-label">Source</label>
						<select name="src_id" class="form-control">
					<?php foreach($src_ids->result() as $src_id): ?>
						<option value="<?php echo $src_id->src_id ?>"><?php echo $src_id->src_name ?></option>
					<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>