<div class="row">
	<div class="col-md-3">
		<form role="form" method="get">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Type</h3>
				</div>
				<div class="box-body no-padding">
					<ul class="nav nav-pills nav-stacked">
						<!-- <li>
							<label><input type="checkbox" name="usr_type" value="" />All</label>
						</li> -->
						<li>
							<label><input type="checkbox" name="usr_types[]" value="sugar" /></i>Provider</label>
						</li>
						<li>
							<label><input type="checkbox" name="usr_types[]" value="flame" /></i>Nurturer</label>
						</li>
					</ul>
				</div>
			</div>

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Status</h3>
				</div>
				<div class="box-body no-padding">
					<ul class="nav nav-pills nav-stacked">
						<!-- <li>
							<label><input type="checkbox" name="usr_status" value="" />All</label>
						</li> -->
						<li>
							<label><input type="checkbox" name="usr_statuses[]" value="pending" />Pending</label>
						</li>
						<li>
							<label><input type="checkbox" name="usr_statuses[]" value="approved" />Approved</label>
						</li>
						<li>
							<label><input type="checkbox" name="usr_statuses[]" value="rejected" />Rejected</label>
						</li>
						<li>
							<label><input type="checkbox" name="usr_statuses[]" value="deactivated" />Deactivated</label>
						</li>
						<li>
							<label><input type="checkbox" name="usr_statuses[]" value="suspended" />Suspended</label>
						</li>
					</ul>
				</div>
			</div>

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Verified</h3>
				</div>
				<div class="box-body no-padding">
					<ul class="nav nav-pills nav-stacked">
						<li>
							<label><input type="checkbox" name="usr_verifieds[]" value="yes" />Yes</label>
						</li>
						<li>
							<label><input type="checkbox" name="usr_verifieds[]" value="no" />No</label>
						</li>
						<li>
							<label><input type="checkbox" name="usr_social_verified" value="yes" />Socially Verified</label>
						</li>
					</ul>
				</div>
			</div>

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Gender</h3>
				</div>
				<div class="box-body no-padding">
					<ul class="nav nav-pills nav-stacked">
						<!-- <li>
							<label><input type="checkbox" name="usr_gender" value="" />All</label>
						</li> -->
						<li>
							<label><input type="checkbox" name="usr_genders[]" value="male" />Male</label>
						</li>
						<li>
							<label><input type="checkbox" name="usr_genders[]" value="female" />Female</label>
						</li>
					</ul>
				</div>
			</div>

			<input name="filter" class="btn btn-primary btn-block margin-bottom" type="submit" value="Filter" />
			<input type="submit" class="btn btn-primary btn-block margin-bottom" formaction="<?php echo admin_url('users/export');?>" value="Export">
		</form>

	</div>

	<div class="col-md-9">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Manage Users <small>(<?php echo $users_count; ?> users)</small></h3>
				<div class="box-tools pull-right">
					<div class="has-feedback">
						<form role="form" method="get">
							<input type="text" class="form-control input-sm" name="search" placeholder="Search" value="<?php echo $this->input->get('search') ?>" />
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</form>
					</div>
				</div>
			</div>
			<?php if($users->num_rows()): ?>
			<div class="box-body table-responsive no-padding">
				<form role="form" method="post">
					<div class="mailbox-controls">
					<!-- Check all button -->
						<div class="btn btn-default btn-sm checkbox-toggle select-all" data-fields="usr_ids">
							<i class="fa fa-square-o"></i>
						</div>
						<div class="btn-group">
							<?php $request = $this->input->get();?>
							<?php if(isset($request['usr_verified'])): ?>
								<?php if($request['usr_verified'] == 'no'): ?>
								<button class="btn btn-primary" name="form_mode" value="resend">
									Resend Verification
								</button>
								<?php endif; ?>
							<?php endif; ?>
						</div><!-- /.btn-group -->
					</div>
					<table class="table table-striped table-hover">
						<tbody>
						<?php foreach($users->result() as $user): ?>
							<tr data-primkey="<?php echo $user->usr_id; ?>">
								<td style="width:50px" class="center">
									<input type="checkbox" name="usr_ids[]" value="<?php echo $user->usr_id; ?>" />
								</td>
								<td width="200px">
									<div class="usr_photo-holder text-center">
										<a href="<?php echo admin_url('users/view/'.$user->usr_id); ?>">
											<?php
												if($user->img_id == 0){
													$url = res_url('admin/images/usr_icon.png');
												}else{
													$this->db->where('img_id', $user->img_id);
													$images = $this->db->get('image');

													if($images->num_rows() > 0){
														$url = base_url($images->row()->img_image);
													}else{
														$url = res_url('admin/images/usr_icon.png');
													}
												}
											?>
											<div class="usr_photo" style="background: url(<?php echo $url; ?>);"></div>
											<br>
											<?php echo $user->usr_first_name.' '.$user->usr_last_name; ?>
											<span class="label label-default label-absolute">
												<?php
													$pn_usr_type = ($user->usr_type == 'sugar') ? 'provider' : 'nurturer';
													echo $pn_usr_type;
												?>
											</span>
										</a>
									</div>
								</td>
								<td>
									<?php echo print_agn($user->usr_id);  ?>
									<br>
								</td>
								<td>
									<?php echo format_date($user->usr_created_date, 'M d, Y'); ?>
								</td>
								<td class="text-center">
									<?php echo $user->usr_status; ?>
								</td>
								<td class="text-center">
									<?php if($user->usr_verified == 'yes'):  ?>
										verified
									<?php else:  ?>
										not verified
									<?php endif;  ?>

								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
					<div class="mailbox-controls">
						<div class="pull-right">
							<?php echo $users_pagination; ?>
						</div>
					</div>
				</form>
			</div>
			<?php else: ?>
			<div class="box-body">
				<p class="text-center" style="padding: 10px;">
					No users found.
				</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>