<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="usr_id" class="control-label">User</label>
						<select name="usr_id" class="form-control">
					<?php foreach($usr_ids->result() as $usr_id): ?>
						<option value="<?php echo $usr_id->usr_id ?>"><?php echo $usr_id->usr_type ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="sub_status" class="control-label">Status</label>
						<select name="sub_status" class="form-control">
							<option value="pending">Pending</option>
							<option value="paid">Paid</option>
						</select>
					</div>
					<div class="form-group">
						<label for="sub_start_date" class="control-label">Start Date</label>
						<input type="text" name="sub_start_date" class="form-control datetime" />
					</div>
					<div class="form-group">
						<label for="sub_end_date" class="control-label">End Date</label>
						<input type="text" name="sub_end_date" class="form-control datetime" />
					</div>
					<div class="form-group">
						<label for="sub_amount" class="control-label">Amount</label>
						<input type="text" name="sub_amount" class="form-control" />
					</div>
					<div class="form-group">
						<label for="sub_type" class="control-label">Type</label>
						<select name="sub_type" class="form-control">
							<option value="monthly">Monthly</option>
							<option value="yearly">Yearly</option>
						</select>
					</div>
					<div class="form-group">
						<label for="sub_transaction_id" class="control-label">Transaction Id</label>
						<input type="text" name="sub_transaction_id" class="form-control" />
					</div>
					<div class="form-group">
						<label for="vch_id" class="control-label">Voucher</label>
						<select name="vch_id" class="form-control">
					<?php foreach($vch_ids->result() as $vch_id): ?>
						<option value="<?php echo $vch_id->vch_id ?>"><?php echo $vch_id->vch_code ?></option>
					<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>