<div class="row">
	<div class="col-md-3">
		<a href="<?php echo admin_url('subscriptions/create') ?>" class="btn btn-primary btn-block margin-bottom">
			<i class="fa fa-plus-square"></i> Create
		</a>
	</div>

	<div class="col-md-9">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">
				</h3>
				<div class="box-tools pull-right">
                </div>
			</div>
			<div class="box-body">
		        <div class="mailbox-read-message">
                	<h3>Status</h3>
                	<div><?php echo $subscription->sub_status ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Start Date</h3>
                	<div><?php echo $subscription->sub_start_date ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>End Date</h3>
                	<div><?php echo $subscription->sub_end_date ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Created</h3>
                	<div><?php echo $subscription->sub_date_created ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Date Modified</h3>
                	<div><?php echo $subscription->sub_date_modified ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Amount</h3>
                	<div><?php echo $subscription->sub_amount ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Type</h3>
                	<div><?php echo $subscription->sub_type ?></div>
		        </div>
		        <div class="mailbox-read-message">
                	<h3>Transaction Id</h3>
                	<div><?php echo $subscription->sub_transaction_id ?></div>
		        </div>
			</div>
			<div class="box-footer">
				<a class="btn btn-default" href="<?php echo admin_url('subscriptions/edit/'.$subscription->sub_id); ?>">
					<i class="fa fa-edit"></i> Edit
				</a>
			</div>
		</div>
	</div>
</div>