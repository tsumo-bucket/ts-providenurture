<div class="row">
	<div class="col-md-9">
		<form method="post"  data-submit="submitForm">
			<div class="box">
				<div class="box-body">
					<div class="form-group">
						<label for="usr_id" class="control-label">User</label>
						<select name="usr_id" class="form-control">
					<?php foreach($usr_ids->result() as $usr_id): ?>
						<option value="<?php echo $usr_id->usr_id ?>"><?php echo $usr_id->usr_username ?></option>
					<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="uai_type" class="control-label">Type</label>
						<select name="uai_type" class="form-control">
							<option value="facebook">Facebook</option>
							<option value="twitter">Twitter</option>
							<option value="instagram">Instagram</option>
						</select>
					</div>
					<div class="form-group">
						<label for="uai_api_id" class="control-label">Api Id</label>
						<textarea name="uai_api_id" style="width:100%" class="form-control redactor"><?php echo $user_api_ids->uai_api_id; ?></textarea>
					</div>
					<div class="form-group">
						<label for="uai_api_token" class="control-label">Api Token</label>
						<textarea name="uai_api_token" style="width:100%" class="form-control redactor"><?php echo $user_api_ids->uai_api_token; ?></textarea>
					</div>
					<div class="form-group">
						<label for="uai_api_secret" class="control-label">Api Secret</label>
						<textarea name="uai_api_secret" style="width:100%" class="form-control redactor"><?php echo $user_api_ids->uai_api_secret; ?></textarea>
					</div>
				</div>
				<div class="box-footer">
					<div class="pull-right">
						<button type="submit" name="form_submit" value='submit' class="btn btn-primary">
							<i class="fa fa-save"></i> Save
						</button>
					</div>
					<a class="btn btn-default" href="<?php echo back_href(); ?>">
						<i class="fa fa-times"></i> Cancel
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$('form').floodling('usr_id', '<?php echo addslashes($user_api_ids->usr_id); ?>');
	$('form').floodling('uai_type', '<?php echo addslashes($user_api_ids->uai_type); ?>');
});
</script>
