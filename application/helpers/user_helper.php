<?php

function print_agn($usr_id){
	$CI = get_instance();

	$CI->load->model('user_model');

	$user = $CI->user_model->get_one($usr_id);

	if($user){
		$date = new DateTime($user->usr_birthyear.'-'.$user->usr_birthmonth.'-'.$user->usr_birthdate);
 		$now = new DateTime();
 		$interval = $now->diff($date);

 		$CI->db->where('info.usr_id', $user->usr_id);
 		$infs = $CI->db->get('info');

 		if($infs->num_rows() > 0){
 			$info = $infs->row();
 			$ethnicity = $info->inf_ethnicity; 
 		}else{
 			$ethnicity = '';
 		}

		return $interval->y.' &#183; '.ucwords($user->usr_gender).' &#183; '.ucwords($ethnicity);
	}else{
		return false;
	}
}

function user_count($type){
	$CI = get_instance();
	
	$CI->load->model('user_model');

	$return = '';

	switch ($type) {
		case 'approved':
			$CI->db->where('usr_status', 'approved');
			$users = $CI->db->get('user');

			$return = $users->num_rows();
			break;

		case 'pending':
			$CI->db->where('usr_status', 'pending');
			$CI->db->where('usr_signup_step', 'done');
			$users = $CI->db->get('user');

			$return = $users->num_rows();
			break;

		case 'unverified':
			$CI->db->where('usr_verified', 'no');
			$users = $CI->db->get('user');

			$return = $users->num_rows();
			break;

		case 'uncompleted':
			$CI->db->where('usr_signup_step !=', 'done');
			$users = $CI->db->get('user');

			$return = $users->num_rows();
			break;

		case 'rejected':
			$CI->db->where('usr_status', 'rejected');
			$users = $CI->db->get('user');

			$return = $users->num_rows();
			break;
		
		default:
			$return = 0;
			break;
	}

	if($return == 0){
		return false;
	}else{
		return $return;
	}
}

function blocked_ids($usr_id){
	$CI = get_instance();

	$usr_ids = array();

	$CI->db->where('block.usr_id', $usr_id);
	$CI->db->where('blo_status', 'active');
	$blocks = $CI->db->get('block');

	if($blocks->num_rows() > 0){
		foreach($blocks->result() as $block){
			$CI->db->where('usr_id', $block->blo_user);
			$users = $CI->db->get('user');

			if($users->num_rows() > 0){
				$user = $users->row();

				if($user && $user->usr_status == 'approved'){
					if(!in_array($block->blo_user, $usr_ids)){
						array_push($usr_ids, $block->blo_user);
					}
				}
			}			
		}
	}

	$CI->db->where('blo_user', $usr_id);
	$CI->db->where('blo_status', 'active');
	$blocks = $CI->db->get('block');

	if($blocks->num_rows() > 0){
		foreach($blocks->result() as $block){
			$user = $CI->user_model->get_one($block->usr_id);

			if($user && $user->usr_status == 'approved'){
				if(!in_array($block->usr_id, $usr_ids)){
					array_push($usr_ids, $block->usr_id);
				}
			}
		}
	}

	return $usr_ids;
}

function update_signup_status($usr_id)
{
	$CI = get_instance();
	$CI->mythos->library('email');

	$return = false;

	$CI->db->where('usr_id', $usr_id);
	$users = $CI->db->get('user');

	if($users->num_rows() > 0){
		$user = $users->row();

		if($user->usr_signup_step != 'done' && $user->usr_status == 'pending'){
			switch ($user->usr_signup_step) {
				case 'photo':
					$CI->db->where('image.usr_id', $usr_id);
					$images = $CI->db->get('image');

					if($images->num_rows() >= 1){
						$data = array();
						$data['usr_signup_step'] = 'done';
						$data['usr_modified_date'] = format_mysql_datetime();

						$CI->db->where('usr_id', $user->usr_id);
						$CI->db->update('user', $data);

						$return = true;
					}
					break;

				case 'info':
					$count = 0;
					$total = 0;

					$CI->db->where('info.usr_id', $usr_id);
					$infos = $CI->db->get('info');

					if($infos->num_rows() > 0){
						$info = $infos->row();

						if($info->inf_height != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}

						if($info->inf_height_unit != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}

						if($info->inf_ethnicity != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}
						
						if($info->inf_body_type != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}

						if($user->usr_description != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}	

						if($user->usr_type == 'flame'){
							if($info->inf_relationship != ''){
								$count = $count + 1;
								$total = $total + 1;
							}else{
								$total = $total + 1;
							}

							if($info->inf_drink != ''){
								$count = $count + 1;
								$total = $total + 1;
							}else{
								$total = $total + 1;
							}

							if($info->inf_children != ''){
								$count = $count + 1;
								$total = $total + 1;
							}else{
								$total = $total + 1;
							}

							if($info->inf_language != ''){
								$count = $count + 1;
								$total = $total + 1;
							}else{
								$total = $total + 1;
							}

							if($info->inf_smoke != ''){
								$count = $count + 1;
								$total = $total + 1;
							}else{
								$total = $total + 1;
							}

							if($info->inf_education != ''){
								$count = $count + 1;
								$total = $total + 1;
							}else{
								$total = $total + 1;
							}

							if($info->inf_occupation != ''){
								$count = $count + 1;
								$total = $total + 1;
							}else{
								$total = $total + 1;
								$count = $count + 1;
							}
						}

						$CI->db->where('preference.usr_id', $usr_id);
						$CI->db->where('prf_content !=', '');
						$preferences = $CI->db->get('preference');

						if($preferences->num_rows() >= 2){
							$count = $count + 2;
							$total = $total + 2;
						}else{
							$total = $total + 2;
						}

						if($count == $total){
							$data = array();
							$data['usr_signup_step'] = 'preference';
							$data['usr_modified_date'] = format_mysql_datetime();

							$CI->db->where('usr_id', $user->usr_id);
							$CI->db->update('user', $data);

							$return = true;
						}
					}
					break;

				case 'preference':
					$count = 0;
					$total = 0;

					$CI->db->where('info.usr_id', $usr_id);
					$infos = $CI->db->get('info');

					if($infos->num_rows() > 0){
						$info = $infos->row();

						if($info->inf_relationship_length != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}

						if($info->inf_relationship_loyalty != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}

						if($info->inf_privacy_expectations != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}

						if($info->inf_relationship_preference != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}

						if($info->inf_sexual_limit != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}

						if($info->inf_spending_habits != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}

						if($info->inf_preferred_range_from != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}

						if($info->inf_preferred_range_from != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}

						if($info->inf_quality_time != '' || $info->inf_gifts != '' || $info->inf_staycations != '' || $info->inf_high_life != '' || $info->inf_simple != '' || $info->inf_other != ''){
							$count = $count + 1;
							$total = $total + 1;
						}else{
							$total = $total + 1;
						}

						if($user->usr_type == 'sugar'){
							if($info->inf_net_worth != ''){
								$count = $count + 1;
								$total = $total + 1;
							}else{
								$total = $total + 1;
							}

							if($info->inf_yearly_income != ''){
								$count = $count + 1;
								$total = $total + 1;
							}else{
								$total = $total + 1;
							}
						}

						if($count == $total){
							$data = array();
							$data['usr_signup_step'] = 'done';
							$data['usr_status'] = 'pending';
							$data['usr_modified_date'] = format_mysql_datetime();

							$CI->db->where('usr_id', $user->usr_id);
							$CI->db->update('user', $data);

							$params = array();
				      $params['mail_to'] = $user->usr_email;
				      $params['subject'] = 'ProvideNurture Registration';
				      $params['cc'] = '';
				      $params['bcc'] = '';

				      $content = array();
				      $content['user'] = $CI->user_model->get_by_email($user->usr_email);

				      $params['content'] = $CI->load->view('email/complete_registration', $content, true);
				      $success = $CI->email->send_mail($params,'email', 'template');

							$return = true;
						}
					}
					break;
				
				default:
					break;
			}
		}
	}

	return $return;
}

function cnv_check($cnv_ids = array(), $usr_id)
{
	$CI = get_instance();

	$return = array();

	foreach($cnv_ids as $cnv){
		$CI->db->where('conversation.cnv_id', $cnv);
		$conversations = $CI->db->get('conversation');

		$conversation = $conversations->row();

		if($conversation->cnv_user_1 == $usr_id){
			$CI->db->where('user.usr_id', $conversation->cnv_user_2);
		}else{
			$CI->db->where('user.usr_id', $conversation->cnv_user_1);
		}

		$CI->db->where('user.usr_status', 'approved');
		$users = $CI->db->get('user');

		if($users->num_rows() > 0){
			array_push($return, $conversation->cnv_id);
		}
	}

	return $return;
}

function user_complete($usr_id)
{
	$CI = get_instance();

	$CI->load->model('user_model');

	$user = $CI->user_model->get_one($usr_id);

	$total = 0;
	$count = 0;


	$CI->db->where('image.usr_id', $usr_id);
	$images = $CI->db->get('image');

	if($images->num_rows() >= 1){
		$count = $count + 1;
		$total = $total + 1;
	}else{
		$total = $total + 1;
	}


	if($user->usr_type == 'sugar'){

	}else{

	}

	//CHECK USER TYPE

	//CHECK INFO ENTRY


	//CHECK QUESTION ENTRIES


	//TAG USER AS SEARCHABLE


	//UNTAG USER AS SEARCHABLE






}