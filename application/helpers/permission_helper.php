<?php

function check_permission($ftr_id, $acc_type){
	$CI = get_instance();
	$CI->load->model('feature_model');

	$feature = $CI->feature_model->get_one($ftr_id);

	$return = false;
	
	if($feature){
		if(strpos($feature->ftr_type, $acc_type) !== false){
			$return = true;
		}
	}

	return $return;
}