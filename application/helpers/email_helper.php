<?php

function emailIntercept($usr_email){
	$CI = get_instance();
	$return = false;

	$CI->load->model('user_model');

	$user = $CI->user_model->get_by_email($usr_email);

	if($user){

		if($user->usr_verified == 'yes'){
			$return = true;
		}
		
		$domains = array('sumofy.me', 'sugarflame.com');
		$email = explode('@', $usr_email);
		
		if(in_array($email[1], $domains)){
			$return = false;
		}
	}else{
		$return = false;
	}

	return $return;
}