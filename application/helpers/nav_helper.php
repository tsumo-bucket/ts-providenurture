<?php

function back_href()
{
    $CI =& get_instance();

    if( $CI->agent->is_referral() ) {
        $url = $CI->agent->referrer();

        if($url == current_url())
        {
        	 $url = admin_url($CI->router->fetch_class());
        }
    } else {
        $url = admin_url($CI->router->fetch_class());
    }

    return $url;
}
