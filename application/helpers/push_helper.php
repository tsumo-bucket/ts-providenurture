<?php

function send_push($usr_id, $ann_id){
	$CI = get_instance();
​
	$CI->load->model('user_device_model');
	$CI->load->model('user_model');
	$CI->load->model('announcement_model');

	$users = $CI->user_model->get_all();
	$announcement = $CI->announcement_model->get_one($ann_id);

	if($users->num_rows() > 0){
		foreach($users->result() as $user){
			if(($announcement->ann_type != 'in app' && $announcement->ann_published == 'published') && $user->usr_push_notif == 'yes'){
				$devices = $CI->user_device_model->get_all( array('usr_id'=>$user->usr_id) );
				if($devices->num_rows() > 0){
					foreach($devices->result() as $device){
						if($device->udv_platform == 'android'){
							$apiKey = "AIzaSyA1T4RkK1DPw5kjCrEnq1B6_w5Xw4qS0LI";
							$url = 'https://android.googleapis.com/gcm/send';

							$fields = array(
								'registration_ids'  => array($device->udv_token),
								'data'              => array( "message" => $announcement->ann_content, "title" => 'Orange World' ),
							);

							$headers = array( 
								'Authorization: key=' . $apiKey,
								'Content-Type: application/json'
							);

							$ch = curl_init();
					 
					 		curl_setopt( $ch, CURLOPT_URL, $url );
			 
							curl_setopt( $ch, CURLOPT_POST, true );
							curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
							curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
							 
							curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
							 
							$result = curl_exec($ch);

							curl_close($ch);
						}else{
							$ctx = stream_context_create();
							stream_context_set_option($ctx, 'ssl', 'local_cert', FCPATH . "certs/hnnPush.pem");
							stream_context_set_option($ctx, 'ssl', 'passphrase', 'EgborKosak2013');

							$fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
							
							if(!$fp){
								exit("Failed to connect: $err $errstr" . PHP_EOL);
							}

							$body['aps'] = array(
								'alert' => $announcement->ann_content,
								'sound' => 'default'
							);

							$payload = json_encode($body);

							$msg = chr(0) . pack('n', 32) . pack('H*', $device->udv_token) . pack('n', strlen($payload)) . $payload;

							try
							{
								$result = fwrite($fp, $msg, strlen($msg));
							}
							catch(Exception $e)
							{
								
							}

							if (!$result) {
							} else {
							}
							
							fclose($fp);
						}
					}
				}
			}
		}
	}
}