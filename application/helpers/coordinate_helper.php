<?php

function check_coordinate($location)
{
  $coordinates = explode(",", $location);

  $longitude = str_replace(' ', '', $coordinates[0]);
  $latitude = str_replace(' ', '', $coordinates[1]);

  $longitudeCheck = false;
  $latitudeCheck = false;

  if(preg_match("/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/", $latitude)){
    $latitudeCheck = true;
  }

  if(preg_match("/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/", $longitude)){
  	$longitudeCheck = true;
  }

  if($longitudeCheck && $latitudeCheck){
  	return true;
  }else{
  	return false;
  }
}
