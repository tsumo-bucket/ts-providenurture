<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('format');

		$this->mythos->library('email');
	}

	public function index()
	{
		$content = [];
		
		$this->template->title('Home');
		$this->template->content('home', $content);
		$this->template->show('site');

		// $request['usr_email'] = 'aquino.dudan@gmail.com';

		// $params = array();
  //   $params['mail_to'] = $request['usr_email'];
  //   $params['subject'] = 'Orange World Registration Confirmation';
  //   $params['cc'] = '';
  //   $params['bcc'] = '';

  //   $this->load->model('email_support_model');

  //   $content = array();
  //   $content['user'] = $this->user_model->get_by_email($request['usr_email']);
  //   $content['application'] = $this->email_support_model->get_one(4);
      
  //   $params['content'] = $this->load->view('email/auto-reply', $content, true);
  //   $success = $this->email->send_mail($params,'email', 'template');
	}
}
