<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if($this->access_control->check_logged_in())
		{
			redirect('admin/dashboard');
		}

		$this->template->title('Login');

		$page = array();
		$page['acc_username'] = '';

		$show_captcha = false;
		$this->load->model('account_model');

		if($this->input->post('submit'))
		{
			$post = $this->extract->post(TRUE);
			$username = $post['acc_username'];
			$password = $post['acc_password'];

			$account_captcha = $this->account_model->get_by_username($username);

			if($account_captcha !== false && $account_captcha->acc_failed_login >= 5)
			{
				$check_captcha = true;
			}
			else
			{
				$check_captcha = false;
			}

			$this->mythos->library('captcha');


			if ($check_captcha === false || (isset($post['captcha_code']) && $this->captcha->check($post['captcha_code']) !== false))
			{
				$account = $this->account_model->authenticate($username, $password);
				if($account !== false)
				{
					$this->account_model->failed_login_reset($username);

					$this->access_control->login_session($account, array('acc_id', 'acc_image'), array('acc_name' => $account->acc_first_name . ' ' . $account->acc_last_name));

					if(isset($post['current_url']) && $post['current_url'] != '')
					{
						redirect($post['current_url'], 'refresh');
					}
					else
					{
						$this->template->notification('Welcome ' . $this->session->userdata('acc_name'), 'success');
						redirect('admin/dashboard');
					}
				}
				else
				{
					$this->account_model->failed_login($username);

					if($account_captcha !== false && $account_captcha->acc_failed_login + 1 >= 5)
					{
						$show_captcha = true;
					}
					else
					{
						$this->template->notification('Invalid username or password.', 'warning');
						$page['acc_username'] = $username;

					}
				}
			}
			else
			{
				$this->account_model->failed_login($username);
				$show_captcha = true;

				$this->template->notification('Incorrect CAPTCHA code.', 'danger');
				$page['acc_username'] = $username;
			}

			unset($post['acc_password']);
			unset($post['captcha_code']);
			unset($post['current_url']);

			$this->template->autofill($post);
		}

		$page['show_captcha'] = $show_captcha;

		$this->template->content('index-index', $page); // Use index-sumo_bubble if using think sumo logo look
		$this->template->show('admin/templates/index');
	}

	public function forbidden()
	{
		$this->template->title('Forbidden');

		$this->template->content('index-forbidden');

		$this->template->show();
	}

	public function logout()
	{
		$this->access_control->logout_session();
		$this->template->notification('You are now logged out.', 'success');

		redirect('admin/index');
	}

}
