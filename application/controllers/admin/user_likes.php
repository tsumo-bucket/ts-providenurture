<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_likes extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('user_like_model');
		$this->load->model("user_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('User Likes');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$usl_ids = $this->input->post('usl_ids');
				if($usl_ids !== false)
				{
					foreach($usl_ids as $usl_id)
					{
						$user_like = $this->user_like_model->get_one($usl_id);
						if($user_like !== false)
						{
							$this->user_like_model->delete($usl_id);
						}
					}
					$this->template->notification('Selected user likes were deleted.', 'success');
					redirect('admin/user_likes');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();

		$page['user_likes'] = $this->user_like_model->pagination("admin/user_likes/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['user_likes_count'] = $this->user_like_model->pagination->total_rows();
		$page['user_likes_pagination'] = $this->user_like_model->pagination_links();
		$this->template->content('user_likes-index', $page);
		$this->template->content('user_likes-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create User Like');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usl_user", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usl_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$user_like = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->user_like_model->create($user_like, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New user like created.', 'success');
				redirect("admin/user_likes/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($user_like);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('user_likes-menu', null, null, 'page-nav');
		$this->template->content('user_likes-create', $page);
		$this->template->show();
	}

	public function edit($usl_id)
	{
		$this->template->title('Edit User Like');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usl_user", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usl_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$user_like = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$user_like['usl_id'] = $usl_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->user_like_model->update($user_like, $fields);

				$this->template->notification('User like updated.', 'success');
				redirect("admin/user_likes/edit/$usl_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($user_like);
		}

		$page = array();
		$page['user_like'] = $this->user_like_model->get_one($usl_id);

		if($page['user_like'] === false)
		{
			$this->template->notification('User like was not found.', 'danger');
			redirect('admin/user_likes');
		}

		$page["usr_ids"] = $this->user_model->get_all();
		$this->template->content('user_likes-menu', null, null, 'page-nav');
		$this->template->content('user_likes-edit', $page);
		$this->template->show();
	}

	public function view($usl_id)
	{
		$this->template->title('View User Like');

		$page = array();
		$page['user_like'] = $this->user_like_model->get_one($usl_id);

		if($page['user_like'] === false)
		{
			$this->template->notification('User like was not found.', 'danger');
			redirect('admin/user_likes');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('user_likes-menu', null, null, 'page-nav');
		$this->template->content('user_likes-view', $page);
		$this->template->show();
	}


}
