<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Messages extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('message_model');
		$this->load->model("user_model");		$this->load->model("conversation_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Messages');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$msg_ids = $this->input->post('msg_ids');
				if($msg_ids !== false)
				{
					foreach($msg_ids as $msg_id)
					{
						$message = $this->message_model->get_one($msg_id);
						if($message !== false)
						{
							$this->message_model->delete($msg_id);
						}
					}
					$this->template->notification('Selected messages were deleted.', 'success');
					redirect('admin/messages');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();		$page["cnv_ids"] = $this->conversation_model->get_all();

		$page['messages'] = $this->message_model->pagination("admin/messages/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['messages_count'] = $this->message_model->pagination->total_rows();
		$page['messages_pagination'] = $this->message_model->pagination_links();
		$this->template->content('messages-index', $page);
		$this->template->content('messages-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Message');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("msg_content", "Content", "trim|required");
		$this->form_validation->set_rules("msg_user", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("msg_status", "Status", "trim|required");
		$this->form_validation->set_rules("cnv_id", "Conversation", "trim|required|integer|max_length[11]");

		if($this->input->post('form_submit'))
		{
			$message = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->message_model->create($message, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New message created.', 'success');
				redirect("admin/messages/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($message);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();		$page["cnv_ids"] = $this->conversation_model->get_all();

		$this->template->content('messages-menu', null, null, 'page-nav');
		$this->template->content('messages-create', $page);
		$this->template->show();
	}

	public function edit($msg_id)
	{
		$this->template->title('Edit Message');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("msg_content", "Content", "trim|required");
		$this->form_validation->set_rules("msg_user", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("msg_status", "Status", "trim|required");
		$this->form_validation->set_rules("cnv_id", "Conversation", "trim|required|integer|max_length[11]");

		if($this->input->post('form_submit'))
		{
			$message = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$message['msg_id'] = $msg_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->message_model->update($message, $fields);

				$this->template->notification('Message updated.', 'success');
				redirect("admin/messages/edit/$msg_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($message);
		}

		$page = array();
		$page['message'] = $this->message_model->get_one($msg_id);

		if($page['message'] === false)
		{
			$this->template->notification('Message was not found.', 'danger');
			redirect('admin/messages');
		}

		$page["usr_ids"] = $this->user_model->get_all();		$page["cnv_ids"] = $this->conversation_model->get_all();
		$this->template->content('messages-menu', null, null, 'page-nav');
		$this->template->content('messages-edit', $page);
		$this->template->show();
	}

	public function view($msg_id)
	{
		$this->template->title('View Message');

		$page = array();
		$page['message'] = $this->message_model->get_one($msg_id);

		if($page['message'] === false)
		{
			$this->template->notification('Message was not found.', 'danger');
			redirect('admin/messages');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();		$page["cnv_ids"] = $this->conversation_model->get_all();

		$this->template->content('messages-menu', null, null, 'page-nav');
		$this->template->content('messages-view', $page);
		$this->template->show();
	}


}
