<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model("user_model");
		$this->load->model("city_model");		
		$this->load->model("image_model");		
		$this->load->model("source_model");

		$this->load->model("info_model");
		$this->load->model("preference_model");
		$this->load->model("user_report_model");

		$this->load->helper('nav');

		$this->mythos->library('email');
	}

	public function index()
	{
		$this->user_model->additional(10);


		$this->template->title('Users');


		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'resend')
			{
				$usr_ids = $this->input->post('usr_ids');
				if($usr_ids !== false)
				{
					foreach($usr_ids as $usr_id)
					{
						$user = $this->user_model->get_one($usr_id);
						if($user !== false)
						{
							$this->user_model->resend_verification($user->usr_email);
						}
					}
					$this->template->notification('Sent email verification reminder to selected users.', 'success');
					redirect('admin/users');
				}
			}
		}


		$page = array();
		$params = $this->input->get();
		$get = "";
		$query = "";

		// if($this->input->get('search'))
		// {
		// 	$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		// }

		unset($params['search']);

		$page["cty_ids"] = $this->city_model->get_all();		
		$page["img_ids"] = $this->image_model->get_all();		
		$page["src_ids"] = $this->source_model->get_all();
		if($this->input->get('filter'))
		{
			$filter = $this->input->get();
			
			unset($params['usr_types']);
			unset($params['usr_statuses']);
			unset($params['usr_verifieds']);
			unset($params['usr_social_verified']);
			unset($params['usr_genders']);
			unset($params['filter']);
			$query = '?' . $_SERVER['QUERY_STRING'];
			$page['users'] = $this->user_model->pagination("admin/users/index/__PAGE__/$query", 'filter', $this->input->get('filter'), $filter);
			$page['users_count'] = $this->user_model->pagination->total_rows();
			$page['users_pagination'] = $this->user_model->pagination_links();
		}
		else {
			$query = '?' . $_SERVER['QUERY_STRING'];
			$page['users'] = $this->user_model->pagination("admin/users/index/__PAGE__/$query", 'search', $this->input->get('search'), $params);
			$page['users_count'] = $this->user_model->pagination->total_rows();
			$page['users_pagination'] = $this->user_model->pagination_links();
		}

		$this->template->content('users-index', $page);
		$this->template->content('users-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function reject_modal($user_id)
	{
		$content = array();
		$content['id'] = $user_id;

		$this->load->view('admin/users/modal', $content);
	}


	public function edit($usr_id, $status = '')
	{
		$this->db->where('usr_id', $usr_id);
		$users = $this->db->get('user');

		if($users->num_rows() > 0){
			$user = $users->row();

			$user_update = array();
			
			if($status == 'approved' || $status == 'force_approved' || $status == 'suspended' || $status == 'rejected' || $status == 'unfreeze'){
				if($status == 'approved' && $user->usr_status == 'pending' && $user->usr_signup_step == 'done'){
					$user_update['usr_token'] = md5($user->usr_token);
					$user_update['usr_status'] = 'approved';
					$user_update['usr_modified_date'] = format_mysql_datetime();
					$user_update['usr_approved_date'] = format_mysql_datetime();

					$this->db->where('usr_id', $usr_id);
					$this->db->update('user', $user_update);

					$params['mail_to'] = $user->usr_email;
					$params['subject'] = "Welcome to ProvideNurture!";

					$content = array();

					if($user->usr_type == 'sugar'){
						$params['content'] = $this->load->view('email/approve_sugar', $content, true);
					}else{
						$params['content'] = $this->load->view('email/approve_flame', $content, true);
					}
							
					$email = $this->email->send_mail($params,'email', 'template');
				}

				if($status == 'force_approved' && $user->usr_status == 'pending'){
					$user_update['usr_token'] = md5($user->usr_token);
					$user_update['usr_status'] = 'approved';
					$user_update['usr_modified_date'] = format_mysql_datetime();
					$user_update['usr_approved_date'] = format_mysql_datetime();

					$this->db->where('usr_id', $usr_id);
					$this->db->update('user', $user_update);

					$params['mail_to'] = $user->usr_email;
					$params['subject'] = "Welcome to SugarFlame!";

					$content = array();

					if($user->usr_type == 'sugar'){
						$params['content'] = $this->load->view('email/approve_sugar', $content, true);
					}else{
						$params['content'] = $this->load->view('email/approve_flame', $content, true);
					}
							
					$email = $this->email->send_mail($params,'email', 'template');
				}

				if($status == 'rejected' && $user->usr_status == 'pending' && $user->usr_signup_step == 'done'){
					$user_update['usr_token'] = md5($user->usr_token);
					$user_update['usr_status'] = 'rejected';
					$user_update['usr_modified_date'] = format_mysql_datetime();

					$this->db->where('usr_id', $usr_id);
					$this->db->update('user', $user_update);

					$params['mail_to'] = $user->usr_email;
					$params['subject'] = "Please resubmit your profile on ProvideNurture";

					$content = array();

					$post = $this->extract->post();

					if(isset($post['eml_slug'])){
						if($post['eml_slug'] == 'inappropriate-photos'){
							$params['content'] = $this->load->view('email/reject_photos', $content, true);
						}elseif($post['eml_slug'] == 'lacking-and-repetitious-texts'){
							$params['content'] = $this->load->view('email/reject_content', $content, true);
						}elseif($post['eml_slug'] == 'custom'){
							$content['message'] = nl2br($post['eml_message']);	
							$params['content'] = $this->load->view('email/reject_custom', $content, true);
						}
					}else{
						$params['content'] = $this->load->view('email/reject', $content, true);
					}
	
					$email = $this->email->send_mail($params,'email', 'template');
				}

				if($status == 'suspended' && $user->usr_status == 'approved'){
					$user_update['usr_token'] = md5($user->usr_token);
					$user_update['usr_status'] = 'suspended';
					$user_update['usr_modified_date'] = format_mysql_datetime();

					$this->db->where('usr_id', $usr_id);
					$this->db->update('user', $user_update);
				}

				if($status == 'unfreeze' && $user->usr_status == 'suspended'){
					$user_update['usr_token'] = md5($user->usr_token);
					$user_update['usr_status'] = 'approved';
					$user_update['usr_modified_date'] = format_mysql_datetime();

					$this->db->where('usr_id', $usr_id);
					$this->db->update('user', $user_update);
				}
			}
			$this->template->notification('User updated.', 'success');
			redirect('admin/users/view/'.$usr_id);
		}else{
			$this->template->notification('User was not found.', 'danger');
		}

		// $this->template->title('Edit User');

		// $this->form_validation->set_rules("usr_status", "Status", "trim|required");
		// $this->form_validation->set_rules("usr_modified_date", "Modified Date", "trim|required|date");

		// if($this->input->post('form_submit'))
		// {
		// 	$user = $this->extract->post();
		// 	if($this->form_validation->run() !== false)
		// 	{
		// 		$user['usr_id'] = $usr_id;
		// 		$fields = $this->form_validation->get_fields();

		// 		$rows_affected = $this->user_model->update($user, $fields);

		// 		$this->template->notification('User updated.', 'success');
		// 		redirect("admin/users/edit/$usr_id");
		// 	}
		// 	else
		// 	{
		// 		$this->template->notification(validation_errors());
		// 	}
		// 	$this->template->autofill($user);
		// }

		// $page = array();
		// $page['user'] = $this->user_model->get_one($usr_id);

		// if($page['user'] === false)
		// {
		// 	$this->template->notification('User was not found.', 'danger');
		// 	redirect('admin/users');
		// }

		// $page["cty_ids"] = $this->city_model->get_all();		$page["img_ids"] = $this->image_model->get_all();		$page["src_ids"] = $this->source_model->get_all();
		// $this->template->content('users-menu', null, null, 'page-nav');
		// $this->template->content('users-edit', $page);
		// $this->template->show();
	}

	public function view($usr_id)
	{
		$this->template->title('View User');

		$page = array();
		$page['user'] = $this->user_model->get_one($usr_id);

		if($page['user'] === false)
		{
			$this->template->notification('User was not found.', 'danger');
			redirect('admin/users');
		}
			
		$page["img_ids"] = $this->image_model->get_all(array('img_status'=>'active'));
		$page["images"] = $this->image_model->get_all(array('image.usr_id'=>$usr_id, 'img_status'=>'active'), array('img_order'=>'asc'));		
		$page["info"] = $this->info_model->get_all_by_user($usr_id);	
		$page["preferences"] = $this->preference_model->get_all_by_user($usr_id);	
		$params['rep_user'] = $usr_id;	
		$page["reports"] = $this->user_report_model->get_all($params);		
		
		$this->template->content('users-menu', null, null, 'page-nav');
		$this->template->content('users-view', $page);
		$this->template->show();
	}

	public function resend()
	{
		$request = $this->input->get();
		$params = array();
      	$params['mail_to'] = $request['usr_email'];
 		$params['subject'] = 'ProvideNurture Registration';
      	$params['cc'] = '';
      	$params['bcc'] = '';

      	$content = array();
     	$content['user'] = $this->user_model->get_by_email($request['usr_email']);

      	$params['content'] = $this->load->view('email/initial', $content, true);
      	
		if($this->email->send_mail($params,'email', 'template')) {
			$this->template->notification('Email Reminder Sent', 'success');
			redirect('admin/users/view/'.$request['usr_id']);
		}
		else {
			redirect('admin/users/view/'.$request['usr_id']);
		}
		
	}

	public function export($params = array())
    {
    	$filename = "All";
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('Users');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Type');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Screen Name');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'First Name');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Last Name');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Email Adress');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Status');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Verified');
        $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Socially Verified');
        $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Gender');
        $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Birthday');
        $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Location');
        $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Interest');
        $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Age from');
        $objPHPExcel->getActiveSheet()->setCellValue('N1', 'Age to');
        $objPHPExcel->getActiveSheet()->setCellValue('O1', 'Description');
        $objPHPExcel->getActiveSheet()->setCellValue('P1', 'Signup Step');
        $objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Signup Source');
        $objPHPExcel->getActiveSheet()->setCellValue('R1', 'Signup Date');
        $objPHPExcel->getActiveSheet()->setCellValue('S1', 'Modified Date');
        $objPHPExcel->getActiveSheet()->setCellValue('T1', 'Approved Date');
        $objPHPExcel->getActiveSheet()->setCellValue('U1', 'Heard From');
        $objPHPExcel->getActiveSheet()->setCellValue('V1', 'Last Login');

        $objPHPExcel->getActiveSheet()->getStyle('A1:V1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle("A1:V1")->getFont()->setBold(true);
        
        if($this->input->get()) {
        	$filter = $this->input->get();
			if(isset($filter['usr_types'])) {
				$this->db->where_in('usr_type', $filter['usr_types']);
			}
			if(isset($filter['usr_statuses'])){
				$this->db->where_in('usr_status', $filter['usr_statuses']);
			}
			if(isset($filter['usr_verifieds'])) {
				$this->db->where_in('usr_verified', $filter['usr_verifieds']);
			}
			if(isset($filter['usr_social_verified'])) {
				$this->db->where('usr_social_verified', $filter['usr_social_verified']);
			}
			if(isset($filter['usr_genders'])) {
				$this->db->where_in('usr_gender', $filter['usr_genders']);
			}
        }
   		$entries = $this->user_model->get_all();


   		$filename = format_mysql_date();

        $row = 2;
        foreach ($entries->result() as $entry) {
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $entry->usr_type);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $entry->usr_screen_name);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $entry->usr_first_name);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $entry->usr_last_name);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $entry->usr_email);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $entry->usr_status);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $entry->usr_verified);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $entry->usr_social_verified);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $entry->usr_gender);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $entry->usr_birthday);
            $objPHPExcel->getActiveSheet()->setCellValue('K'.$row, $entry->usr_location);
            $objPHPExcel->getActiveSheet()->setCellValue('L'.$row, $entry->usr_interested);
            $objPHPExcel->getActiveSheet()->setCellValue('M'.$row, $entry->usr_interested_age_from);
            $objPHPExcel->getActiveSheet()->setCellValue('N'.$row, $entry->usr_interested_age_to);
            $objPHPExcel->getActiveSheet()->setCellValue('O'.$row, $entry->usr_description);
            $objPHPExcel->getActiveSheet()->setCellValue('P'.$row, $entry->usr_signup_step);
            $objPHPExcel->getActiveSheet()->setCellValue('Q'.$row, $entry->usr_signup_source);
            $objPHPExcel->getActiveSheet()->setCellValue('R'.$row, format_datetime($entry->usr_created_date, "M d, Y", "Asia/Manila"));
            $objPHPExcel->getActiveSheet()->setCellValue('S'.$row, format_datetime($entry->usr_modified_date, "M d, Y", "Asia/Manila"));
            $objPHPExcel->getActiveSheet()->setCellValue('T'.$row, format_datetime($entry->usr_approved_date, "M d, Y", "Asia/Manila"));
            $objPHPExcel->getActiveSheet()->setCellValue('U'.$row, $entry->src_name);
            $objPHPExcel->getActiveSheet()->setCellValue('V'.$row, format_datetime($entry->usr_last_login, "M d, Y", "Asia/Manila"));

            $link_style_array = [
                'font'  => [
                    'color' => ['rgb' => '0000FF'],
                    'underline' => 'single'
                ]
            ];
            
            $row++;
        }

        foreach(range('A','V') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        $filename .= '_Users_List.csv';
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }
}
