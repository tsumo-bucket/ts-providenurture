<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Photo_galleries extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->access_control->logged_in();
		$this->access_control->account_type('dev', 'admin');
		$this->access_control->validate();

		$this->load->model('photo_gallery_model');
		$this->load->model('photo_model');
	}

	public function index()
	{
		$this->template->title('Photo Galleries');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$phg_ids = $this->input->post('phg_ids');
				if($phg_ids !== false)
				{
					foreach($phg_ids as $phg_id)
					{
						$photo_gallery = $this->photo_gallery_model->get_one($phg_id);
						if($photo_gallery !== false)
						{
							$this->photo_gallery_model->delete($phg_id);
						}
					}
					$this->template->notification('Selected photo galleries were deleted.', 'success');
				}
			}
		} elseif ($this->input->post('search')) {
			$search = $this->extract->post('search');
			$search_params['field'] = $search['field'];
			$search_params['keywords'] = $search['keywords'];
		}
		
		if($this->input->post('images'))
		{
			$images = $this->input->post('images');

			if($images == 'delete')
			{
				$pho_ids = $this->input->post('pho_ids');
				if($pho_ids !== false)
				{
					foreach($pho_ids as $pho_id)
					{
						$photo = $this->photo_model->get_one($pho_id);
						if($photo !== false)
						{
							$this->photo_model->delete($pho_id);
						}
					}
					$this->template->notification('Selected photos were deleted.', 'success');
				}
			}
		}
		
		$this->form_validation->set_rules('phg_id', 'Name', 'trim|integer|max_length[11]');
		$this->form_validation->set_rules('pho_src', 'Src', 'trim|max_length[100]');
		$this->form_validation->set_rules('pho_caption', 'Caption', 'trim|max_length[100]');
		
		if($this->input->post('submit'))
		{
			$photo = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				if ($_FILES['pho_src']['tmp_name']) {
					$this->mythos->library('image');
					$dir = 'uploads/photos/';
					$data = array();
					$image= $this->image->upload('pho_src', './'.$dir,'jpg|png' );
					$data['filename'] = $image["file_name"];
 					$photo['pho_src'] = serialize($data);
					$thumbs = array(
						array('width'=>262, 'height'=>262)
					);
					$n = $this->image->create_thumbs($image['file_name'], $thumbs);
					//$coupon['cpn_thumb'] = $n[0];
				}
				
				$fields = $this->form_validation->get_fields();
				$rows_affected = $this->photo_model->update($photo, $fields);

				$this->template->notification('Photo updated.', 'success');
				redirect('admin/photo_galleries');
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($photo);
		}
		
		
		
		$page = array();
		$page['keywords'] = @$search['keywords'];
		$this->db->order_by("pho_order", "asc"); 
		$page['photos'] = $this->photo_model->get_all();
		$page['photo_galleries'] = $this->photo_gallery_model->pagination("admin/photo_galleries/index/__PAGE__", 'search_keyword_admin', @$search_params);
		$page['photo_galleries_pagination'] = $this->photo_gallery_model->pagination_links();
		$page['phg_ids'] = $this->photo_gallery_model->get_all();
		$this->template->content('photo_galleries-index', $page);
		$this->template->content('menu-photo_galleries', null, 'admin', 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Photo Gallery');


		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules('phg_name', 'Name', 'trim|max_length[100]');
		$this->form_validation->set_rules('phg_description', 'Description', 'trim');
		$this->form_validation->set_rules('phg_status', 'Status', 'trim');

		if($this->input->post('submit'))
		{
			$photo_gallery = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();

				$photo_gallery['phg_date_created'] = format_mysql_datetime();
				$photo_gallery['phg_created_by'] = $this->session->userdata('acc_name');

				$fields[] = 'phg_date_created';
				$fields[] = 'phg_created_by';

				$this->photo_gallery_model->create($photo_gallery, $fields);
				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New photo gallery created.', 'success');
				redirect('admin/photo_galleries/');
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'error');
			}

			$this->template->autofill($photo_gallery);
		}

		$page = array();
		
		$this->template->content('photo_galleries-create', $page);
		$this->template->show();
	}

	public function edit($phg_id)
	{
		$this->template->title('Edit Photo Gallery');


		$this->form_validation->set_rules('phg_name', 'Name', 'trim|max_length[100]');
		$this->form_validation->set_rules('phg_description', 'Description', 'trim');
		$this->form_validation->set_rules('phg_status', 'Status', 'trim');

		if($this->input->post('submit'))
		{
			$photo_gallery = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$photo_gallery['phg_id'] = $phg_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->photo_gallery_model->update($photo_gallery, $fields);

				$this->template->notification('Photo gallery updated.', 'success');
				redirect('admin/photo_galleries');
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($photo_gallery);
		}

		$page = array();
		$page['photo_gallery'] = $this->photo_gallery_model->get_one($phg_id);

		if($page['photo_gallery'] === false)
		{
			$this->template->notification('Photo gallery was not found.', 'error');
			redirect('admin/photo_galleries');
		}

		$this->template->content('photo_galleries-edit', $page);
		$this->template->show();
	}

	public function view($photo_gallery_id)
	{
		$this->template->title('View Photo Gallery');
		
		$page = array();
		$page['photo_gallery'] = $this->photo_gallery_model->get_one($photo_gallery_id);

		if($page['photo_gallery'] === false)
		{
			$this->template->notification('Photo gallery was not found.', 'error');
			redirect('admin/photo_galleries');
		}
		
		$this->template->content('photo_galleries-view', $page);
		$this->template->show();
	}
}



