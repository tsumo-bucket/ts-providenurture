<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Voucher_xrefs extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('voucher_xref_model');
		$this->load->model("user_model");		$this->load->model("voucher_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Voucher Xrefs');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$vcx_ids = $this->input->post('vcx_ids');
				if($vcx_ids !== false)
				{
					foreach($vcx_ids as $vcx_id)
					{
						$voucher_xref = $this->voucher_xref_model->get_one($vcx_id);
						if($voucher_xref !== false)
						{
							$this->voucher_xref_model->delete($vcx_id);
						}
					}
					$this->template->notification('Selected voucher xrefs were deleted.', 'success');
					redirect('admin/voucher_xrefs');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();		$page["vch_ids"] = $this->voucher_model->get_all();

		$page['voucher_xrefs'] = $this->voucher_xref_model->pagination("admin/voucher_xrefs/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['voucher_xrefs_count'] = $this->voucher_xref_model->pagination->total_rows();
		$page['voucher_xrefs_pagination'] = $this->voucher_xref_model->pagination_links();
		$this->template->content('voucher_xrefs-index', $page);
		$this->template->content('voucher_xrefs-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Voucher Xref');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("vch_id", "Voucher", "trim|required|integer|max_length[11]");

		if($this->input->post('form_submit'))
		{
			$voucher_xref = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->voucher_xref_model->create($voucher_xref, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New voucher xref created.', 'success');
				redirect("admin/voucher_xrefs/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($voucher_xref);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();		$page["vch_ids"] = $this->voucher_model->get_all();

		$this->template->content('voucher_xrefs-menu', null, null, 'page-nav');
		$this->template->content('voucher_xrefs-create', $page);
		$this->template->show();
	}

	public function edit($vcx_id)
	{
		$this->template->title('Edit Voucher Xref');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("vch_id", "Voucher", "trim|required|integer|max_length[11]");

		if($this->input->post('form_submit'))
		{
			$voucher_xref = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$voucher_xref['vcx_id'] = $vcx_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->voucher_xref_model->update($voucher_xref, $fields);

				$this->template->notification('Voucher xref updated.', 'success');
				redirect("admin/voucher_xrefs/edit/$vcx_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($voucher_xref);
		}

		$page = array();
		$page['voucher_xref'] = $this->voucher_xref_model->get_one($vcx_id);

		if($page['voucher_xref'] === false)
		{
			$this->template->notification('Voucher xref was not found.', 'danger');
			redirect('admin/voucher_xrefs');
		}

		$page["usr_ids"] = $this->user_model->get_all();		$page["vch_ids"] = $this->voucher_model->get_all();
		$this->template->content('voucher_xrefs-menu', null, null, 'page-nav');
		$this->template->content('voucher_xrefs-edit', $page);
		$this->template->show();
	}

	public function view($vcx_id)
	{
		$this->template->title('View Voucher Xref');

		$page = array();
		$page['voucher_xref'] = $this->voucher_xref_model->get_one($vcx_id);

		if($page['voucher_xref'] === false)
		{
			$this->template->notification('Voucher xref was not found.', 'danger');
			redirect('admin/voucher_xrefs');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();		$page["vch_ids"] = $this->voucher_model->get_all();

		$this->template->content('voucher_xrefs-menu', null, null, 'page-nav');
		$this->template->content('voucher_xrefs-view', $page);
		$this->template->show();
	}


}
