<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Images extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('image_model');
		$this->load->model("user_model");

		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Images');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$img_ids = $this->input->post('img_ids');
				if($img_ids !== false)
				{
					foreach($img_ids as $img_id)
					{
						$image = $this->image_model->get_one($img_id);
						if($image !== false)
						{
							$this->image_model->delete($img_id);
						}
					}
					$this->template->notification('Selected images were deleted.', 'success');
					redirect('admin/images');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();

		$page['images'] = $this->image_model->pagination("admin/images/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['images_count'] = $this->image_model->pagination->total_rows();
		$page['images_pagination'] = $this->image_model->pagination_links();
		$this->template->content('images-index', $page);
		$this->template->content('images-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function delete($img_id)
	{
		$this->session->set_userdata('refered_from', $_SERVER['HTTP_REFERER']);

		$image = $this->image_model->get_one($img_id);
		if($image !== false){
			$this->db->where('usr_id', $image->usr_id);
			$users = $this->db->get('user');

			if($users->num_rows() > 0){
				$user = $users->row();

				if($user->img_id == $img_id){
					$usr_update = array();
					$usr_update['img_id'] = 0;
					$usr_update['usr_modified_date'] = format_mysql_datetime();

					$this->db->where('usr_id', $user->usr_id);
					$this->db->update('user', $usr_update);
				}
				$this->image_model->delete($img_id);	

				$this->template->notification('Selected user image deleted.', 'success');
			}else{
				$this->template->notification('User not found.', 'danger');
			}
		}else{
			$this->template->notification('User image not found.', 'danger');
		}
		redirect($this->session->userdata('refered_from'));
	}
}
