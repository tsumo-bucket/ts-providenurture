<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Views extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('view_model');
		$this->load->model("user_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Views');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$vie_ids = $this->input->post('vie_ids');
				if($vie_ids !== false)
				{
					foreach($vie_ids as $vie_id)
					{
						$view = $this->view_model->get_one($vie_id);
						if($view !== false)
						{
							$this->view_model->delete($vie_id);
						}
					}
					$this->template->notification('Selected views were deleted.', 'success');
					redirect('admin/views');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();

		$page['views'] = $this->view_model->pagination("admin/views/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['views_count'] = $this->view_model->pagination->total_rows();
		$page['views_pagination'] = $this->view_model->pagination_links();
		$this->template->content('views-index', $page);
		$this->template->content('views-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create View');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("vie_user", "User", "trim|required|integer|max_length[11]");

		if($this->input->post('form_submit'))
		{
			$view = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->view_model->create($view, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New view created.', 'success');
				redirect("admin/views/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($view);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('views-menu', null, null, 'page-nav');
		$this->template->content('views-create', $page);
		$this->template->show();
	}

	public function edit($vie_id)
	{
		$this->template->title('Edit View');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("vie_user", "User", "trim|required|integer|max_length[11]");

		if($this->input->post('form_submit'))
		{
			$view = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$view['vie_id'] = $vie_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->view_model->update($view, $fields);

				$this->template->notification('View updated.', 'success');
				redirect("admin/views/edit/$vie_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($view);
		}

		$page = array();
		$page['view'] = $this->view_model->get_one($vie_id);

		if($page['view'] === false)
		{
			$this->template->notification('View was not found.', 'danger');
			redirect('admin/views');
		}

		$page["usr_ids"] = $this->user_model->get_all();
		$this->template->content('views-menu', null, null, 'page-nav');
		$this->template->content('views-edit', $page);
		$this->template->show();
	}

	public function view($vie_id)
	{
		$this->template->title('View View');

		$page = array();
		$page['view'] = $this->view_model->get_one($vie_id);

		if($page['view'] === false)
		{
			$this->template->notification('View was not found.', 'danger');
			redirect('admin/views');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('views-menu', null, null, 'page-nav');
		$this->template->content('views-view', $page);
		$this->template->show();
	}


}
