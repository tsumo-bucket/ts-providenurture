<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sources extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('source_model');



		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Sources');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$src_ids = $this->input->post('src_ids');
				if($src_ids !== false)
				{
					foreach($src_ids as $src_id)
					{
						$source = $this->source_model->get_one($src_id);
						if($source !== false)
						{
							$this->source_model->delete($src_id);
						}
					}
					$this->template->notification('Selected sources were deleted.', 'success');
					redirect('admin/sources');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}



		$page['sources'] = $this->source_model->pagination("admin/sources/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['sources_count'] = $this->source_model->pagination->total_rows();
		$page['sources_pagination'] = $this->source_model->pagination_links();
		$this->template->content('sources-index', $page);
		$this->template->content('sources-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Source');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("src_name", "Name", "trim|required|max_length[100]");
		$this->form_validation->set_rules("src_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$source = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->source_model->create($source, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New source created.', 'success');
				redirect("admin/sources/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($source);
		}

		$page = array();



		$this->template->content('sources-menu', null, null, 'page-nav');
		$this->template->content('sources-create', $page);
		$this->template->show();
	}

	public function edit($src_id)
	{
		$this->template->title('Edit Source');

		$this->form_validation->set_rules("src_name", "Name", "trim|required|max_length[100]");
		$this->form_validation->set_rules("src_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$source = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$source['src_id'] = $src_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->source_model->update($source, $fields);

				$this->template->notification('Source updated.', 'success');
				redirect("admin/sources/edit/$src_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($source);
		}

		$page = array();
		$page['source'] = $this->source_model->get_one($src_id);

		if($page['source'] === false)
		{
			$this->template->notification('Source was not found.', 'danger');
			redirect('admin/sources');
		}


		$this->template->content('sources-menu', null, null, 'page-nav');
		$this->template->content('sources-edit', $page);
		$this->template->show();
	}

	public function view($src_id)
	{
		$this->template->title('View Source');

		$page = array();
		$page['source'] = $this->source_model->get_one($src_id);

		if($page['source'] === false)
		{
			$this->template->notification('Source was not found.', 'danger');
			redirect('admin/sources');
		}
		


		$this->template->content('sources-menu', null, null, 'page-nav');
		$this->template->content('sources-view', $page);
		$this->template->show();
	}


}
