<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_reports extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('user_report_model');


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('User Reports');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$rep_ids = $this->input->post('rep_ids');
				if($rep_ids !== false)
				{
					foreach($rep_ids as $rep_id)
					{
						$user_report = $this->user_report_model->get_one($rep_id);
						if($user_report !== false)
						{
							$this->user_report_model->delete($rep_id);
						}
					}
					$this->template->notification('Selected user reports were deleted.', 'success');
					redirect('admin/user_reports');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}


		$page["rep_ids"] = $this->user_report_model->get_all();
		if(isset($params['order_by'])) {
			$order_by = 'rep_user';
			$order = 'asc';
			unset($params);
			$params = array();
		}
		else {
			$order_by = 'rep_date_created';
			$order = 'desc';
		}
		$page['user_reports'] = $this->user_report_model->pagination("admin/user_reports/index/__PAGE__/$get", 'search', $this->input->get('search'), $params, array($order_by=>$order));
		$page['user_reports_count'] = $this->user_report_model->pagination->total_rows();
		$page['user_reports_pagination'] = $this->user_report_model->pagination_links();
		$this->template->content('user_reports-index', $page);
		$this->template->content('user_reports-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create User Report');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User Reporting", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("rep_user", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("rep_reason", "Reason", "trim|required");
		$this->form_validation->set_rules("rep_other_reason", "Other Reason", "trim|required");

		if($this->input->post('form_submit'))
		{
			$user_report = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->user_report_model->create($user_report, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New user report created.', 'success');
				redirect("admin/user_reports/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($user_report);
		}

		$page = array();

		$page["rep_ids"] = $this->user_report_model->get_all();

		$this->template->content('user_reports-menu', null, null, 'page-nav');
		$this->template->content('user_reports-create', $page);
		$this->template->show();
	}

	public function edit($rep_id)
	{
		$this->template->title('Edit User Report');

		$this->form_validation->set_rules("usr_id", "User Reporting", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("rep_user", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("rep_reason", "Reason", "trim|required");
		$this->form_validation->set_rules("rep_other_reason", "Other Reason", "trim|required");

		if($this->input->post('form_submit'))
		{
			$user_report = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$user_report['rep_id'] = $rep_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->user_report_model->update($user_report, $fields);

				$this->template->notification('User report updated.', 'success');
				redirect("admin/user_reports/edit/$rep_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($user_report);
		}

		$page = array();
		$page['user_report'] = $this->user_report_model->get_one($rep_id);

		if($page['user_report'] === false)
		{
			$this->template->notification('User report was not found.', 'danger');
			redirect('admin/user_reports');
		}

		$page["rep_ids"] = $this->user_report_model->get_all();
		$this->template->content('user_reports-menu', null, null, 'page-nav');
		$this->template->content('user_reports-edit', $page);
		$this->template->show();
	}

	public function view($rep_id)
	{
		$this->template->title('View User Report');

		$page = array();

		$page['user_report'] = $this->user_report_model->get_one($rep_id);
		$page['user_report']->reported = $this->user_model->get_one($page['user_report']->rep_user);

		if($page['user_report'] === false)
		{
			$this->template->notification('User report was not found.', 'danger');
			redirect('admin/user_reports');
		}
		
		$page["rep_ids"] = $this->user_report_model->get_all();

		$this->template->content('user_reports-menu', null, null, 'page-nav');
		$this->template->content('user_reports-view', $page);
		$this->template->show();
	}


}
