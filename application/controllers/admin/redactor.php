<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Redactor extends CI_Controller
{
	private $upload_path = './uploads/';
	private $CI;
	public function __construct()
	{
		parent::__construct();
		$this->access_control->logged_in();
		$this->access_control->account_type('developer', 'admin');
		$this->access_control->validate();
		$this->mythos->library('image');
		$this->load->library('user_agent');
		$this->CI =& get_instance();
	}

	public function index()
	{
		redirect('/admin');
	}

	public function do_upload()
	{
		$referrer = $this->agent->referrer();
		$segments = explode('/', $referrer);
		$admin    = array_search('admin', $segments);

		if(!empty($_FILES['file']['name']))
    	{
			$path = $this->upload_path . 'pages/';

			if( !empty($segments) ):
				$path = $this->upload_path . $segments[$admin + 1] . '/';
			endif;

			if ( !is_dir($path) ) {
				mkdir($path, 0777, true);
			}

			$config['upload_path'] = $path;
			$config['allowed_types'] = 'gif|jpg|png';

			$this->load->library('upload', $config);

			// Attempt upload
			if($this->upload->do_upload('file'))
			{
			  $image_data = $this->upload->data();

			  $json = array(
			  	'filelink' => base_url($path . $image_data['file_name']),
			  	'path' => $path
			  );

			  echo stripslashes(json_encode($json));
			}
		}
	}
}
