<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Infos extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('info_model');
		$this->load->model("user_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Infos');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$inf_ids = $this->input->post('inf_ids');
				if($inf_ids !== false)
				{
					foreach($inf_ids as $inf_id)
					{
						$info = $this->info_model->get_one($inf_id);
						if($info !== false)
						{
							$this->info_model->delete($inf_id);
						}
					}
					$this->template->notification('Selected infos were deleted.', 'success');
					redirect('admin/infos');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();

		$page['infos'] = $this->info_model->pagination("admin/infos/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['infos_count'] = $this->info_model->pagination->total_rows();
		$page['infos_pagination'] = $this->info_model->pagination_links();
		$this->template->content('infos-index', $page);
		$this->template->content('infos-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Info');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_height", "Height", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_body_type", "Body Type", "trim|required");
		$this->form_validation->set_rules("inf_ethnicity", "Ethnicity", "trim|required");
		$this->form_validation->set_rules("inf_smoke", "Smoke", "trim|required");
		$this->form_validation->set_rules("inf_drink", "Drink", "trim|required");
		$this->form_validation->set_rules("inf_relationship", "Relationship", "trim|required");
		$this->form_validation->set_rules("inf_children", "Children", "trim|required");
		$this->form_validation->set_rules("inf_language", "Language", "trim|required");
		$this->form_validation->set_rules("inf_education", "Education", "trim|required");
		$this->form_validation->set_rules("inf_occupation", "Occupation", "trim|required|max_length[100]");
		$this->form_validation->set_rules("inf_spending_habits", "Spending Habits", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_quality_time", "Quality Time", "trim|required");
		$this->form_validation->set_rules("inf_gifts", "Gifts", "trim|required");
		$this->form_validation->set_rules("inf_travel", "Travel", "trim|required");
		$this->form_validation->set_rules("inf_staycations", "Staycations", "trim|required");
		$this->form_validation->set_rules("inf_high_life", "High Life", "trim|required");
		$this->form_validation->set_rules("inf_simple", "Simple", "trim|required");
		$this->form_validation->set_rules("inf_net_worth", "Net Worth", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_yearly_income", "Yearly Income", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_preferred_range_from", "Preferred Range From", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_preferred_range_to", "Preferred Range To", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_relationship_length", "Relationship Length", "trim|required");
		$this->form_validation->set_rules("inf_relationship_loyalty", "Relationship Loyalty", "trim|required");
		$this->form_validation->set_rules("inf_relationship_privacy", "Relationship Privacy", "trim|required");
		$this->form_validation->set_rules("inf_relationship_preference", "Relationship Preference", "trim|required");
		$this->form_validation->set_rules("inf_sexual_limit", "Sexual Limit", "trim|required");
		$this->form_validation->set_rules("inf_privacy_expectations", "Privacy Expectations", "trim|required");

		if($this->input->post('form_submit'))
		{
			$info = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->info_model->create($info, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New info created.', 'success');
				redirect("admin/infos/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($info);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('infos-menu', null, null, 'page-nav');
		$this->template->content('infos-create', $page);
		$this->template->show();
	}

	public function edit($inf_id)
	{
		$this->template->title('Edit Info');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_height", "Height", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_body_type", "Body Type", "trim|required");
		$this->form_validation->set_rules("inf_ethnicity", "Ethnicity", "trim|required");
		$this->form_validation->set_rules("inf_smoke", "Smoke", "trim|required");
		$this->form_validation->set_rules("inf_drink", "Drink", "trim|required");
		$this->form_validation->set_rules("inf_relationship", "Relationship", "trim|required");
		$this->form_validation->set_rules("inf_children", "Children", "trim|required");
		$this->form_validation->set_rules("inf_language", "Language", "trim|required");
		$this->form_validation->set_rules("inf_education", "Education", "trim|required");
		$this->form_validation->set_rules("inf_occupation", "Occupation", "trim|required|max_length[100]");
		$this->form_validation->set_rules("inf_spending_habits", "Spending Habits", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_quality_time", "Quality Time", "trim|required");
		$this->form_validation->set_rules("inf_gifts", "Gifts", "trim|required");
		$this->form_validation->set_rules("inf_travel", "Travel", "trim|required");
		$this->form_validation->set_rules("inf_staycations", "Staycations", "trim|required");
		$this->form_validation->set_rules("inf_high_life", "High Life", "trim|required");
		$this->form_validation->set_rules("inf_simple", "Simple", "trim|required");
		$this->form_validation->set_rules("inf_net_worth", "Net Worth", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_yearly_income", "Yearly Income", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_preferred_range_from", "Preferred Range From", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_preferred_range_to", "Preferred Range To", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("inf_relationship_length", "Relationship Length", "trim|required");
		$this->form_validation->set_rules("inf_relationship_loyalty", "Relationship Loyalty", "trim|required");
		$this->form_validation->set_rules("inf_relationship_privacy", "Relationship Privacy", "trim|required");
		$this->form_validation->set_rules("inf_relationship_preference", "Relationship Preference", "trim|required");
		$this->form_validation->set_rules("inf_sexual_limit", "Sexual Limit", "trim|required");
		$this->form_validation->set_rules("inf_privacy_expectations", "Privacy Expectations", "trim|required");

		if($this->input->post('form_submit'))
		{
			$info = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$info['inf_id'] = $inf_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->info_model->update($info, $fields);

				$this->template->notification('Info updated.', 'success');
				redirect("admin/infos/edit/$inf_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($info);
		}

		$page = array();
		$page['info'] = $this->info_model->get_one($inf_id);

		if($page['info'] === false)
		{
			$this->template->notification('Info was not found.', 'danger');
			redirect('admin/infos');
		}

		$page["usr_ids"] = $this->user_model->get_all();
		$this->template->content('infos-menu', null, null, 'page-nav');
		$this->template->content('infos-edit', $page);
		$this->template->show();
	}

	public function view($inf_id)
	{
		$this->template->title('View Info');

		$page = array();
		$page['info'] = $this->info_model->get_one($inf_id);

		if($page['info'] === false)
		{
			$this->template->notification('Info was not found.', 'danger');
			redirect('admin/infos');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('infos-menu', null, null, 'page-nav');
		$this->template->content('infos-view', $page);
		$this->template->show();
	}


}
