<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Photos extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->access_control->logged_in();
		$this->access_control->account_type('dev', 'admin');
		$this->access_control->validate();

		$this->load->model('photo_model');
		$this->load->model('photo_gallery_model');
		$this->load->helper(array('form', 'url'));
		$this->load->helper('file');
		$this->mythos->library('image');
	}

	public function index()
	{
		$this->template->title('Photos');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$pho_ids = $this->input->post('pho_ids');
				if($pho_ids !== false)
				{
					foreach($pho_ids as $pho_id)
					{
						$photo = $this->photo_model->get_one($pho_id);
						if($photo !== false)
						{
							$this->photo_model->delete($pho_id);
						}
					}
					$this->template->notification('Selected photos were deleted.', 'success');
				}
			}
		} elseif ($this->input->post('search')) {
			$search = $this->extract->post('search');
			$search_params['field'] = $search['field'];
			$search_params['keywords'] = $search['keywords'];
		}

		$page = array();
		$page['keywords'] = @$search['keywords'];
		$page['photos'] = $this->photo_model->pagination("admin/photos/index/__PAGE__", 'search_keyword_admin', @$search_params);
		$page['photos_pagination'] = $this->photo_model->pagination_links();
		$this->template->content('photos-index', $page);
		$this->template->content('menu-photos', null, 'admin', 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Photo');
				
		$this->load->model('photo_gallery_model');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules('phg_id', 'Name', 'trim|integer|max_length[11]');
		$this->form_validation->set_rules('pho_src', 'Src', 'trim|max_length[100]');
		$this->form_validation->set_rules('pho_caption', 'Caption', 'trim|max_length[100]');
		$this->form_validation->set_rules('pho_date_created', 'Date Created', 'trim|datetime');
		$this->form_validation->set_rules('pho_created_by', 'Created By', 'trim|max_length[50]');

		if($this->input->post('submit'))
		{
			$photo = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$this->photo_model->create($photo, $fields);
				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New photo created.', 'success');
				redirect('admin/photos');
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'error');
			}

			$this->template->autofill($photo);
		}

		$page = array();
		$page['phg_ids'] = $this->photo_gallery_model->get_all();
		
		$this->template->content('photos-create', $page);
		$this->template->show();
	}

	public function edit($pho_id)
	{
		$this->template->title('Edit Photo');
				
		$this->load->model('photo_gallery_model');

		$this->form_validation->set_rules('phg_id', 'Name', 'trim|integer|max_length[11]');
		$this->form_validation->set_rules('pho_src', 'Src', 'trim|max_length[100]');
		$this->form_validation->set_rules('pho_caption', 'Caption', 'trim|max_length[100]');
		$this->form_validation->set_rules('pho_date_created', 'Date Created', 'trim|datetime');
		$this->form_validation->set_rules('pho_created_by', 'Created By', 'trim|max_length[50]');

		if($this->input->post('submit'))
		{
			$photo = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$photo['pho_id'] = $pho_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->photo_model->update($photo, $fields);

				$this->template->notification('Photo updated.', 'success');
				redirect('admin/photos');
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($photo);
		}

		$page = array();
		$page['photo'] = $this->photo_model->get_one($pho_id);

		if($page['photo'] === false)
		{
			$this->template->notification('Photo was not found.', 'error');
			redirect('admin/photos');
		}
		$page['phg_ids'] = $this->photo_gallery_model->get_all();

		$this->template->content('photos-edit', $page);
		$this->template->show();
	}

	public function view($photo_id)
	{
		$this->template->title('View Photo');
		
		$page = array();
		$page['photo'] = $this->photo_model->get_one($photo_id);

		if($page['photo'] === false)
		{
			$this->template->notification('Photo was not found.', 'error');
			redirect('admin/photos');
		}
		
		$this->template->content('photos-view', $page);
		$this->template->show();
	}

	public function album($phg_id){
		$album = $this->photo_gallery_model->get_one($phg_id);

		if($album == false) {
			$this->template->notification('Album does not exist.', 'danger');
			redirect('admin/photo_galleries/');
		}

		$this->template->title('View - ' . $album->phg_name);

		$content = array();
		$content['error'] = '';
		$content['phg_id'] = $phg_id;
		
		$this->template->content('photos-album', $content, 'admin');
		$this->template->show('admin');
	}
	
	public function do_upload($phg_id) {
    $upload_path_url = base_url('uploads/photos');
    $config['upload_path'] = FCPATH . 'uploads/photos';
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
    $config['max_size'] = '30000';

    $this->load->library('upload', $config);
		$this->mythos->library('image');
		
      if (!$this->upload->do_upload()) {
	      //$error = array('error' => $this->upload->display_errors());
	      //$this->load->view('upload', $error);

	      //Load the list of existing files in the upload directory
	      $existingFiles = get_dir_file_info($config['upload_path']);
	      $foundFiles = array();
	      $f=0;
	      foreach ($existingFiles as $fileName => $info) {
	        if($fileName!='thumbnail'){//Skip over thumbs directory
	          //set the data for the json array   
	          $foundFiles[$f]['name'] = $fileName;
	          $foundFiles[$f]['size'] = $info['size'];
	          $foundFiles[$f]['url'] = $upload_path_url . $fileName;
	          $foundFiles[$f]['thumbnailUrl'] = $upload_path_url . 'thumbnail/' . $fileName;
	          $foundFiles[$f]['deleteUrl'] = base_url() . 'upload/deleteImage/' . $fileName;
	          $foundFiles[$f]['deleteType'] = 'DELETE';
	          $foundFiles[$f]['error'] = null;

	          $f++;
	        }
	      }
	      $this->output
	      ->set_content_type('application/json')
	      ->set_output(json_encode(array('files' => $foundFiles)));
      } 
      else {
            $data = $this->upload->data();
            /*
             * Array
              (
              [file_name] => png1.jpg
              [file_type] => image/jpeg
              [file_path] => /home/ipresupu/public_html/uploads/
              [full_path] => /home/ipresupu/public_html/uploads/png1.jpg
              [raw_name] => png1
              [orig_name] => png.jpg
              [client_name] => png.jpg
              [file_ext] => .jpg
              [file_size] => 456.93
              [is_image] => 1
              [image_width] => 1198
              [image_height] => 1166
              [image_type] => jpeg
              [image_size_str] => width="1198" height="1166"
              )
             */
            // to re-size for thumbnail images un-comment and set path here and in json array
            $config = array();
            $config['image_library'] = 'gd2';
            $config['source_image'] = $data['full_path'];
            $config['create_thumb'] = TRUE;
            $config['new_image'] = $data['file_path'] . 'thumbnail/';
            $config['maintain_ratio'] = TRUE;
            $config['thumb_marker'] = '';
            $config['width'] = 75;
            $config['height'] = 50;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

			$info = array();
            //set the data for the json array   
            $info['name'] = $data['file_name'];
            $info['size'] = $data['file_size'];
            $info['type'] = $data['file_type'];
            $info['url'] = $upload_path_url . $data['file_name'];
            // I set this to original file since I did not create thumbs.  change to thumbnail directory if you do = $upload_path_url .'/thumbs' .$data['file_name']
            $info['thumbnailUrl'] = $upload_path_url . '/thumbnail/' . $data['file_name'];
            $info['deleteUrl'] = base_url() . 'upload/deleteImage/' . $data['file_name'];
            $info['deleteType'] = 'DELETE';
            $info['error'] = null;
			$info['caption'] = $_REQUEST['caption'];
			
			$photos = array();
			$photo_upload = array();
			$thumb_sml = 'thumb_sml_' . $data['raw_name'] . $data['file_ext'];
			$thumb_med = 'thumb_med_' . $data['raw_name'] . $data['file_ext'];
			$thumb_lrg = 'thumb_lrg_' . $data['raw_name'] . $data['file_ext'];
			$thumbs = array(
				array('width'=>100, 'height'=>100, 'filename' => $thumb_sml),
				array('width'=>200, 'height'=>100, 'filename' => $thumb_med),
				array('width'=>100, 'height'=>200, 'filename' => $thumb_lrg)				
			);
			
				$filename = 'uploads/photos/'.$data['file_name'];
				$photo_upload['phg_id'] = $phg_id; 
				$photo_upload['pho_caption'] = ''; 
				$photo_upload['pho_date_created'] = format_mysql_datetime(); 
				$photo_upload['pho_created_by'] = $this->session->userdata('acc_username');
				$photo_upload['pho_caption'] = $_REQUEST['caption'];
				
				$photo_filter['photo.phg_id'] = $phg_id;
				$query = $this->photo_model->get_order($photo_filter);
				$result = $query->row_array(1);
				//$lastorder = $result->pho_order + 1;
		
				//$photo_upload['pho_order'] = $lastorder;
				
				$this->image->set_bgcolor("#ffffff");
				$photos['filename'] = $data['file_name'];
				
				// $photos['thumb_sml'] = $thumb_sml;
				// $photos['thumb_med'] = $thumb_med;
				// $photos['thumb_lrg'] = $thumb_lrg;
			
				// $this->image->create_thumbs($data['full_path'], $thumbs);
				
				$photo_upload['pho_src'] = serialize($photos);

				$this->photo_model->create($photo_upload);
				
				$query_2 = $this->photo_model->get_latest($photo_filter);
				$result_2 = $query_2->row_array(1);
				$lastid = $result_2['pho_id'];
				//$photo_update['pho_order'] = $lastorder;
				$photo_update['pho_order'] = $lastid;
				$this->db->where('pho_id', $lastid);
				$this->db->update('photo', $photo_update); 
			
        $files[] = $info;
        //this is why we put this in the constants to pass only json data
				if (IS_AJAX) {
	      	echo json_encode(array("files" => $files, "data" => $data));
	              //this has to be the only data returned or you will get an error.
	              //if you don't give this a json array it will give you a Empty file upload result error
	              //it you set this without the if(IS_AJAX)...else... you get ERROR:TRUE (my experience anyway)
	              // so that this will still work if javascript is not enabled
	      } 
	      else {
	              $file_data['upload_data'] = $this->upload->data();
	              $this->load->view('upload/upload_success', $file_data);
	      }
      }
    }

    public function deleteImage($file) {//gets the job done but you might want to add error checking and security
     
		$success = unlink(FCPATH . 'uploads/photos' . $file);
        $success = unlink(FCPATH . 'uploads/photos/thumbnail/' . $file);
        //info to see if it is doing what it is supposed to 
        $info->sucess = $success;
        $info->path = base_url() . 'uploads/photos/' . $file;
        $info->file = is_file(FCPATH . 'uploads/photos/' . $file);

        if (IS_AJAX) {
            //I don't think it matters if this is set but good for error checking in the console/firebug
            echo json_encode(array($info));
        } else {
            //here you will need to decide what you want to show for a successful delete        
            $file_data['delete_data'] = $file;
            //$this->load->view('admin/delete_success', $file_data);
        }
    }
	
	function reorder(){
		$ids = $this->input->post();  
		foreach($ids['item'] as $order => $id){
			$this->db
			->where('pho_id', $id)
			->update('photo', array('pho_order' => $order + 1));
		}

		print_r(json_encode($ids));
	}

	public function ajaxEdit($pho_id)
	{	
		$return = array();
		$photo = $this->photo_model->get_one($pho_id);
		if( $photo ) {
			$photo->pho_src = unserialize($photo->pho_src);
			$return['photo'] = $photo;
		}
		else {
			$return['photo'] = false;
		}
		print_r(json_encode($return));
	}
	
}