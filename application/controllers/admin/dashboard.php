<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		
		$this->access_control->account_type('developer', 'admin');
		$this->access_control->validate();
	}
	
	public function index() 
	{
		$this->template->title('Admin Dashboard');

		// $this->template->content('dashboard-header', null, 'admin', 'page-nav');	

		$this->_dashboard1();

		$this->template->show();
	}

	private function _dashboard1()
	{	
		$page = array();
		$this->template->content('dashboard-index1', $page);
	}

	private function _dashboard2()
	{
		$page = array();
		$page['tables'] = array(
			'Banners'=>'banner', 
			'Articles'=>'article', 
			'Users'=>'user', 
			'Pages'=>'page'
		);

		foreach($page['tables'] as $key=>$value)
		{
			$page[$value.'_count'] = $this->db->query("select count(*) as count from $value")->row()->count;
		}

		$this->template->content('dashboard-index2', $page);	
	}

	public function test()
	{
		$this->db->distinct();
		$this->db->where('uev_category', 'read');
		$query = $this->db->get('user_event');
		foreach($query->result() as $uev){
			$this->db->where('uev_category', 'read');
			$this->db->where('art_id', $uev->art_id);
			$query = $this->db->get('user_event');

			echo $uev->art_id.': '.$query->num_rows();
			echo "<br>";
		}
	}
}