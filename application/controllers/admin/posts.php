<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Posts extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('post_model');
		$this->load->model("user_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Posts');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$pos_ids = $this->input->post('pos_ids');
				if($pos_ids !== false)
				{
					foreach($pos_ids as $pos_id)
					{
						$post = $this->post_model->get_one($pos_id);
						if($post !== false)
						{
							$this->post_model->delete($pos_id);
						}
					}
					$this->template->notification('Selected posts were deleted.', 'success');
					redirect('admin/posts');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();

		$page['posts'] = $this->post_model->pagination("admin/posts/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['posts_count'] = $this->post_model->pagination->total_rows();
		$page['posts_pagination'] = $this->post_model->pagination_links();
		$this->template->content('posts-index', $page);
		$this->template->content('posts-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Post');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("pos_content", "Content", "trim|required");
		$this->form_validation->set_rules("pos_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$post = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->post_model->create($post, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New post created.', 'success');
				redirect("admin/posts/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($post);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('posts-menu', null, null, 'page-nav');
		$this->template->content('posts-create', $page);
		$this->template->show();
	}

	public function edit($pos_id)
	{
		$this->template->title('Edit Post');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("pos_content", "Content", "trim|required");
		$this->form_validation->set_rules("pos_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$post = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$post['pos_id'] = $pos_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->post_model->update($post, $fields);

				$this->template->notification('Post updated.', 'success');
				redirect("admin/posts/edit/$pos_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($post);
		}

		$page = array();
		$page['post'] = $this->post_model->get_one($pos_id);

		if($page['post'] === false)
		{
			$this->template->notification('Post was not found.', 'danger');
			redirect('admin/posts');
		}

		$page["usr_ids"] = $this->user_model->get_all();
		$this->template->content('posts-menu', null, null, 'page-nav');
		$this->template->content('posts-edit', $page);
		$this->template->show();
	}

	public function view($pos_id)
	{
		$this->template->title('View Post');

		$page = array();
		$page['post'] = $this->post_model->get_one($pos_id);

		if($page['post'] === false)
		{
			$this->template->notification('Post was not found.', 'danger');
			redirect('admin/posts');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('posts-menu', null, null, 'page-nav');
		$this->template->content('posts-view', $page);
		$this->template->show();
	}


}
