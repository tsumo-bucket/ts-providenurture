<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_options extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->access_control->logged_in();
		$this->access_control->account_type('developer', 'admin');
		$this->access_control->validate();

		$this->load->model('site_options_model');
		$this->load->model('account_model');
		$this->load->helper('button');
	}

	public function index()
	{
		$this->template->title('Site Options');

		$page = array();
		$page['site_options'] = $this->site_options_model->pagination("admin/site_options/index/__PAGE__", 'get_all');
		$page['site_options_pagination'] = $this->site_options_model->pagination_links();
		$this->template->content('site_options-index', $page);
		$this->template->show();
	}


	public function edit($opt_id)
	{
		$this->template->title('Edit Site Options');


		$this->form_validation->set_rules('opt_name', 'Name', 'trim|max_length[256]');
		$this->form_validation->set_rules('opt_value', 'Value', 'trim');

		if($this->input->post('submit'))
		{
			$site_options = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$site_options['opt_id'] = $opt_id;
				
				if (@$_FILES['upload_pic']['tmp_name']) {
					$data = $this->_do_upload("upload_pic",$site_options['opt_slug'] , './uploads/site_options/');
				
					if(isset($data['error']))
					{
						$this->template->notification($data['error'], 'danger');
					}
					else
					{
						$upload_data = $data['upload_data'];
						$site_options['opt_value'] = './uploads/site_options/'.$upload_data['file_name'];	
					}
				} 
				
				$rows_affected = $this->site_options_model->update($site_options, $this->form_validation->get_fields());
				$opt = $this->site_options_model->get_one($opt_id);
				if($opt->opt_slug == 'token'){
					$account = $this->account_model->get_by_username('developer');
					$account->acc_token = $site_options['opt_value'];

					$this->account_model->update($account);
				}

				$this->template->notification('Site options updated.', 'success');
				redirect('admin/site_options');
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($site_options);
		}

		$page = array();
		$page['site_options'] = $this->site_options_model->get_one($opt_id);

		if($page['site_options'] === false)
		{
			$this->template->notification('Site options was not found.', 'danger');
			redirect('admin/site_options');
		}

		$this->template->content('site_options-edit', $page);
		$this->template->show();
	}
	
	private function _do_upload($field, $file_name = "", $path = "")
	{
		$this->load->library('upload');
		$upload_config = array();
		
		if($path == "")
		{
			$upload_config['upload_path'] = FCPATH . 'uploads/';
		}
		else
		{
			$upload_config['upload_path'] = rtrim($path, '/\\') . '/';
		}
		
		if( $file_name == "" ){
			$upload_config['encrypt_name'] = true;
		} else {
			//$upload_config['encrypt_name'] = true;
			$upload_config['encrypt_name'] = false;
			$upload_config['file_name'] = $file_name;
		}
		
		$upload_config['allowed_types'] = 'gif|jpg|png|jpeg';
		$upload_config['max_size']	= '0';
		$upload_config['max_width']  = '0';
		$upload_config['max_height']  = '0';
		$upload_config['overwrite'] = true;
		$this->upload->initialize($upload_config);
		
		if(!$this->upload->do_upload($field))
		{
			$error = array('error' => $this->upload->display_errors());
			$d = $this->upload->data();
			return $error;
		}
		else
		{
			$upload_data = $this->upload->data('full_path');
			
			$data = array();
			$data['upload_data'] = $this->upload->data();
			return $data;
		}
	}

	public function view($site_options_id)
	{
		$this->template->title('View Site Options');
		
		$page = array();
		$page['site_options'] = $this->site_options_model->get_one($site_options_id);

		if($page['site_options'] === false)
		{
			$this->template->notification('Site options was not found.', 'error');
			redirect('admin/site_options');
		}
		
		$this->template->content('site_options-view', $page);
		$this->template->show();
	}
}