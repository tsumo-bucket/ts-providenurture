<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emails extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->access_control->logged_in();
		$this->access_control->account_type('developer', 'admin');
		$this->access_control->validate();

		$this->load->model('email_model');
		$this->load->helper('button');
	}

	public function index()
	{
		$this->template->title('Emails');
		$get_search = $this->extract->get();
		$get_string = "";

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$eml_ids = $this->input->post('eml_ids');
				if($eml_ids !== false)
				{
					foreach($eml_ids as $eml_id)
					{
						$email = $this->email_model->get_one($eml_id);
						if($email !== false)
						{
							$this->email_model->delete($eml_id);
						}
					}
					$this->template->notification('Selected emails were deleted.', 'success');
				}
			}
		} elseif ($this->input->post('search')) {
			$search = $this->extract->post('search');
			$search_params['keywords'] = $search['keywords'];
			$search_params['field'] = $search['field'];
			$get_string = http_build_query($search_params);
			redirect("admin/emails/index/0/?$get_string");
		} elseif ($get_search) {
			$search = $get_search;
			$search_params['field'] = $get_search['field'];
			$search_params['keywords'] = $get_search['keywords'];
			$get_string = http_build_query($search_params);
		}

		$page = array();
		$page['keywords'] = @$search['keywords'];
		$page['field'] = @$search['field'];
		$page['emails'] = $this->email_model->pagination("admin/emails/index/__PAGE__/?$get_string", 'search_keyword_admin', @$search_params);
		$page['emails_count'] = $this->email_model->pagination->total_rows();
		$page['emails_pagination'] = $this->email_model->pagination_links();
		$this->template->content('emails-index', $page);
		$this->template->content('menu-emails', null, 'admin', 'page-nav');
		$this->template->show();
	}
/*
	public function create()
	{
		$this->template->title('Create Email');


		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules('eml_mail_to', 'Mail To', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('eml_cc', 'Cc', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('eml_bcc', 'Bcc', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('eml_subject', 'Subject', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('eml_message', 'Message', 'trim|required');
		$this->form_validation->set_rules('eml_date_sent', 'Date Sent', 'trim|required|is_natural');

		if($this->input->post('submit'))
		{
			$email = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$this->email_model->create($email, $fields);
				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New email created.', 'success');
				redirect('admin/emails');
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($email);
		}

		$page = array();

		$this->template->content('emails-create', $page);
		$this->template->show();
	}

	public function edit($eml_id)
	{
		$this->template->title('Edit Email');


		$this->form_validation->set_rules('eml_mail_to', 'Mail To', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('eml_cc', 'Cc', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('eml_bcc', 'Bcc', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('eml_subject', 'Subject', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('eml_message', 'Message', 'trim|required');
		$this->form_validation->set_rules('eml_date_sent', 'Date Sent', 'trim|required|is_natural');

		if($this->input->post('submit'))
		{
			$email = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$email['eml_id'] = $eml_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->email_model->update($email, $fields);

				$this->template->notification('Email updated.', 'success');
				redirect('admin/emails');
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($email);
		}

		$page = array();
		$page['email'] = $this->email_model->get_one($eml_id);

		if($page['email'] === false)
		{
			$this->template->notification('Email was not found.', 'danger');
			redirect('admin/emails');
		}

		$this->template->content('emails-edit', $page);
		$this->template->show();
	}
*/
	public function view($email_id)
	{
		$this->template->title('View Email');

		$page = array();
		$page['email'] = $this->email_model->get_one($email_id);

		if($page['email'] === false)
		{
			$this->template->notification('Email was not found.', 'danger');
			redirect('admin/emails');
		}

		$this->template->content('emails-view', $page);
		$this->template->show();
	}

	public function resend($email_id)
	{
		// Get email
		$email = $this->email_model->get_one( $email_id );

		$params = array();
		$params['mail_to'] = $email->eml_mail_to;
		$params['cc']      = $email->eml_cc;
		$params['bcc']     = $email->eml_bcc;
		$params['subject'] = $email->eml_subject;
		$params['content'] = $email->eml_message; // change this
		$return = $this->email->send_mail($params,'email', 'template');


		if( $return['success'] ) {
			$this->template->notification('Email was sent successfully', 'success');
		} else {
			$this->template->notification('Email was sent successfully', 'success');
		}

		if( $this->agent->is_referral() ) {
			$url = $this->agent->referrer();
		} else {
			$url = admin_url('emails');
		}

		redirect( $url );
	}
}
