<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Follows extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('follow_model');
		$this->load->model("user_inactive_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Follows');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$fol_ids = $this->input->post('fol_ids');
				if($fol_ids !== false)
				{
					foreach($fol_ids as $fol_id)
					{
						$follow = $this->follow_model->get_one($fol_id);
						if($follow !== false)
						{
							$this->follow_model->delete($fol_id);
						}
					}
					$this->template->notification('Selected follows were deleted.', 'success');
					redirect('admin/follows');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_inactive_model->get_all();

		$page['follows'] = $this->follow_model->pagination("admin/follows/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['follows_count'] = $this->follow_model->pagination->total_rows();
		$page['follows_pagination'] = $this->follow_model->pagination_links();
		$this->template->content('follows-index', $page);
		$this->template->content('follows-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Follow');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User Inactive", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("fol_user", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("fol_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$follow = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->follow_model->create($follow, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New follow created.', 'success');
				redirect("admin/follows/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($follow);
		}

		$page = array();

		$page["usr_ids"] = $this->user_inactive_model->get_all();

		$this->template->content('follows-menu', null, null, 'page-nav');
		$this->template->content('follows-create', $page);
		$this->template->show();
	}

	public function edit($fol_id)
	{
		$this->template->title('Edit Follow');

		$this->form_validation->set_rules("usr_id", "User Inactive", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("fol_user", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("fol_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$follow = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$follow['fol_id'] = $fol_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->follow_model->update($follow, $fields);

				$this->template->notification('Follow updated.', 'success');
				redirect("admin/follows/edit/$fol_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($follow);
		}

		$page = array();
		$page['follow'] = $this->follow_model->get_one($fol_id);

		if($page['follow'] === false)
		{
			$this->template->notification('Follow was not found.', 'danger');
			redirect('admin/follows');
		}

		$page["usr_ids"] = $this->user_inactive_model->get_all();
		$this->template->content('follows-menu', null, null, 'page-nav');
		$this->template->content('follows-edit', $page);
		$this->template->show();
	}

	public function view($fol_id)
	{
		$this->template->title('View Follow');

		$page = array();
		$page['follow'] = $this->follow_model->get_one($fol_id);

		if($page['follow'] === false)
		{
			$this->template->notification('Follow was not found.', 'danger');
			redirect('admin/follows');
		}
		
		$page["usr_ids"] = $this->user_inactive_model->get_all();

		$this->template->content('follows-menu', null, null, 'page-nav');
		$this->template->content('follows-view', $page);
		$this->template->show();
	}


}
