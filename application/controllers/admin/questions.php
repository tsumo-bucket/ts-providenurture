<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questions extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('question_model');

		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Questions');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$que_ids = $this->input->post('que_ids');
				if($que_ids !== false)
				{
					foreach($que_ids as $que_id)
					{
						$question = $this->question_model->get_one($que_id);
						if($question !== false)
						{
							$question->que_status = 'draft';

							$this->question_model->update($question);
						}
					}
					$this->template->notification('Selected questions were deleted.', 'success');
					redirect('admin/questions');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";
		$query = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		if($this->input->get())
		{
			$query = "?".$_SERVER['QUERY_STRING'];
		}

		unset($params['search']);

		$page['questions'] = $this->question_model->pagination("admin/questions/index/__PAGE__/$query", 'search', $this->input->get('search'), $params);
		$page['questions_count'] = $this->question_model->pagination->total_rows();
		$page['questions_pagination'] = $this->question_model->pagination_links();
		$this->template->content('questions-index', $page);
		$this->template->content('questions-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Question');

		$this->form_validation->set_rules("que_question", "Question", "trim");
		$this->form_validation->set_rules("que_slug", "Slug", "trim");
		$this->form_validation->set_rules("que_category", "Category", "trim|required");
		$this->form_validation->set_rules("que_status", "Status", "trim|required");

		$this->form_validation->set_rules("que_created_by", "que_created_by", "trim");
		$this->form_validation->set_rules("que_date_created", "que_date_created", "trim");
		if($this->input->post('form_submit'))
		{
			$question = $this->extract->post();

			if($this->form_validation->run() !== false)
			{
				$question['que_date_created'] = format_mysql_datetime();
				$question["que_created_by"] = $this->session->userdata('acc_id');

				$question['que_date_modified'] = format_mysql_datetime();
				$question["que_modified_by"] = $this->session->userdata('acc_id');

				$fields = $this->form_validation->get_fields();
				$result = $this->question_model->create($question, $fields);
				$id = $result['result']['insert_id'];

				$slug = format_html_slug($question['que_question']);

				if($this->question_model->get_by_slug($slug) !== false){
					$slug .= '-' . $id;
				}

				$this->question_model->update_slug($id, $slug);

				$this->template->notification('New question created.', 'success');
				redirect("admin/questions/edit/$id");
			}
			else
			{
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($question);
		}

		$page = array();

		$this->template->content('questions-menu', null, null, 'page-nav');
		$this->template->content('questions-create', $page);
		$this->template->show();
	}

	public function edit($que_id)
	{
		$this->template->title('Edit Question');

		$this->form_validation->set_rules("que_question", "Question", "trim");
		$this->form_validation->set_rules("que_category", "Category", "trim|required");
		$this->form_validation->set_rules("que_status", "Status", "trim|required");

		$this->form_validation->set_rules("que_modified_by", "que_modified_by", "trim");
		$this->form_validation->set_rules("que_date_modified", "que_date_modified", "trim");

		if($this->input->post('form_submit'))
		{
			$question = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$question['que_date_modified'] = format_mysql_datetime();
				$question["que_modified_by"] = $this->session->userdata('acc_id');

				$question['que_id'] = $que_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->question_model->update($question, $fields);

				$this->template->notification('Question updated.', 'success');
				redirect("admin/questions/edit/$que_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($question);
		}

		$page = array();
		$page['question'] = $this->question_model->get_one($que_id);

		if($page['question'] === false)
		{
			$this->template->notification('Question was not found.', 'danger');
			redirect('admin/questions');
		}


		$this->template->content('questions-menu', null, null, 'page-nav');
		$this->template->content('questions-edit', $page);
		$this->template->show();
	}
}
