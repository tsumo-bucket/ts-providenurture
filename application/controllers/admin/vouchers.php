<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vouchers extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('voucher_model');



		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Vouchers');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$vch_ids = $this->input->post('vch_ids');
				if($vch_ids !== false)
				{
					foreach($vch_ids as $vch_id)
					{
						$voucher = $this->voucher_model->get_one($vch_id);
						if($voucher !== false)
						{
							$this->voucher_model->delete($vch_id);
						}
					}
					$this->template->notification('Selected vouchers were deleted.', 'success');
					redirect('admin/vouchers');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}



		$page['vouchers'] = $this->voucher_model->pagination("admin/vouchers/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['vouchers_count'] = $this->voucher_model->pagination->total_rows();
		$page['vouchers_pagination'] = $this->voucher_model->pagination_links();
		$this->template->content('vouchers-index', $page);
		$this->template->content('vouchers-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Voucher');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("vch_code", "Code", "trim|required|max_length[100]");
		$this->form_validation->set_rules("vch_status", "Status", "trim|required");
		$this->form_validation->set_rules("vch_value", "Value", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("vch_valid_until", "Valid Until", "trim|required|datetime");

		if($this->input->post('form_submit'))
		{
			$voucher = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->voucher_model->create($voucher, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New voucher created.', 'success');
				redirect("admin/vouchers/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($voucher);
		}

		$page = array();



		$this->template->content('vouchers-menu', null, null, 'page-nav');
		$this->template->content('vouchers-create', $page);
		$this->template->show();
	}

	public function edit($vch_id)
	{
		$this->template->title('Edit Voucher');

		$this->form_validation->set_rules("vch_code", "Code", "trim|required|max_length[100]");
		$this->form_validation->set_rules("vch_status", "Status", "trim|required");
		$this->form_validation->set_rules("vch_value", "Value", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("vch_valid_until", "Valid Until", "trim|required|datetime");

		if($this->input->post('form_submit'))
		{
			$voucher = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$voucher['vch_id'] = $vch_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->voucher_model->update($voucher, $fields);

				$this->template->notification('Voucher updated.', 'success');
				redirect("admin/vouchers/edit/$vch_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($voucher);
		}

		$page = array();
		$page['voucher'] = $this->voucher_model->get_one($vch_id);

		if($page['voucher'] === false)
		{
			$this->template->notification('Voucher was not found.', 'danger');
			redirect('admin/vouchers');
		}


		$this->template->content('vouchers-menu', null, null, 'page-nav');
		$this->template->content('vouchers-edit', $page);
		$this->template->show();
	}

	public function view($vch_id)
	{
		$this->template->title('View Voucher');

		$page = array();
		$page['voucher'] = $this->voucher_model->get_one($vch_id);

		if($page['voucher'] === false)
		{
			$this->template->notification('Voucher was not found.', 'danger');
			redirect('admin/vouchers');
		}
		


		$this->template->content('vouchers-menu', null, null, 'page-nav');
		$this->template->content('vouchers-view', $page);
		$this->template->show();
	}


}
