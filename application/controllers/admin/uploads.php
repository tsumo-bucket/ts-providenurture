<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Uploads extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
	}
	
	public function index() 
	{
		//Normal File Upload
		$this->mythos->library('upload');
		
		//Image Management
		$this->mythos->library('image');
		
		if($this->input->post('submit')){
		
			//Upload file, 3rd parameter is the accepted extension types separated by |
			$file = ($this->upload->file('file', './uploads/test/' ) );
		
		
			//Image upload, 3rd parameter is the accepted extension types separated by |
			$image = ($this->image->upload('image', './uploads/test/' ) );

			/**************
			$image will return an array with the following properties
			[file_name] 
			[file_type] 
			[file_path] 
			[full_path] 
			[raw_name] 
			[orig_name] 
			[client_name] 
			[file_ext] 
			[file_size] 
			[is_image] 
			[image_width] 
			[image_height] 
			[image_type] 
			[image_size_str] 
			**************/


			
			// Old Crop
			// $this->image_moo
			// ->load( $banner['source_file'] )
			// ->crop($banner["thumb_x"],$banner["thumb_y"],$banner["thumb_x2"], $banner["thumb_y2"])
			// ->save( $banner['target_file'], TRUE );
			
			// $this->image_moo
			// ->load( $thumb )
			// ->resize_crop($this->img_width, $this->img_height)
			// ->save( $thumb, TRUE );
			
			
			//Thumbnail array, if no filename, the code will append to the current filename
			$thumbs = array(
				array('width'=>100, 'height'=>100, 'filename'=>'test1'),
				array('width'=>200, 'height'=>100),
				array('width'=>100, 'height'=>200, 'filename'=>'test3.jpg')				
			);
			
			//Set bg color
			$this->image->set_bgcolor("#ff0000");
			
			//Create thumbs
			$this->image->create_thumbs($image['full_path'], $thumbs);
			
			//Crop
			$this->image->crop($image['full_path'], array('x1'=>10,'y1'=>10,'x2'=>20,'y2'=>20));
			
		}
		$this->template->show('site');
	}
}
