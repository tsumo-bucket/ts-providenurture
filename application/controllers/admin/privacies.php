<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Privacies extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('privacy_model');
		$this->load->model("user_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Privacies');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$pri_ids = $this->input->post('pri_ids');
				if($pri_ids !== false)
				{
					foreach($pri_ids as $pri_id)
					{
						$privacy = $this->privacy_model->get_one($pri_id);
						if($privacy !== false)
						{
							$this->privacy_model->delete($pri_id);
						}
					}
					$this->template->notification('Selected privacies were deleted.', 'success');
					redirect('admin/privacies');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();

		$page['privacies'] = $this->privacy_model->pagination("admin/privacies/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['privacies_count'] = $this->privacy_model->pagination->total_rows();
		$page['privacies_pagination'] = $this->privacy_model->pagination_links();
		$this->template->content('privacies-index', $page);
		$this->template->content('privacies-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Privacy');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("pri_comment", "Comment", "trim|required");
		$this->form_validation->set_rules("pri_profile", "Profile", "trim|required");

		if($this->input->post('form_submit'))
		{
			$privacy = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->privacy_model->create($privacy, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New privacy created.', 'success');
				redirect("admin/privacies/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($privacy);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('privacies-menu', null, null, 'page-nav');
		$this->template->content('privacies-create', $page);
		$this->template->show();
	}

	public function edit($pri_id)
	{
		$this->template->title('Edit Privacy');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("pri_comment", "Comment", "trim|required");
		$this->form_validation->set_rules("pri_profile", "Profile", "trim|required");

		if($this->input->post('form_submit'))
		{
			$privacy = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$privacy['pri_id'] = $pri_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->privacy_model->update($privacy, $fields);

				$this->template->notification('Privacy updated.', 'success');
				redirect("admin/privacies/edit/$pri_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($privacy);
		}

		$page = array();
		$page['privacy'] = $this->privacy_model->get_one($pri_id);

		if($page['privacy'] === false)
		{
			$this->template->notification('Privacy was not found.', 'danger');
			redirect('admin/privacies');
		}

		$page["usr_ids"] = $this->user_model->get_all();
		$this->template->content('privacies-menu', null, null, 'page-nav');
		$this->template->content('privacies-edit', $page);
		$this->template->show();
	}

	public function view($pri_id)
	{
		$this->template->title('View Privacy');

		$page = array();
		$page['privacy'] = $this->privacy_model->get_one($pri_id);

		if($page['privacy'] === false)
		{
			$this->template->notification('Privacy was not found.', 'danger');
			redirect('admin/privacies');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('privacies-menu', null, null, 'page-nav');
		$this->template->content('privacies-view', $page);
		$this->template->show();
	}


}
