<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subscriptions extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('subscription_model');
		$this->load->model("user_model");		$this->load->model("voucher_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Subscriptions');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$sub_ids = $this->input->post('sub_ids');
				if($sub_ids !== false)
				{
					foreach($sub_ids as $sub_id)
					{
						$subscription = $this->subscription_model->get_one($sub_id);
						if($subscription !== false)
						{
							$this->subscription_model->delete($sub_id);
						}
					}
					$this->template->notification('Selected subscriptions were deleted.', 'success');
					redirect('admin/subscriptions');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();		$page["vch_ids"] = $this->voucher_model->get_all();

		$page['subscriptions'] = $this->subscription_model->pagination("admin/subscriptions/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['subscriptions_count'] = $this->subscription_model->pagination->total_rows();
		$page['subscriptions_pagination'] = $this->subscription_model->pagination_links();
		$this->template->content('subscriptions-index', $page);
		$this->template->content('subscriptions-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Subscription');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("sub_status", "Status", "trim|required");
		$this->form_validation->set_rules("sub_start_date", "Start Date", "trim|required|datetime");
		$this->form_validation->set_rules("sub_end_date", "End Date", "trim|required|datetime");
		$this->form_validation->set_rules("sub_amount", "Amount", "trim|required|decimal");
		$this->form_validation->set_rules("sub_type", "Type", "trim|required");
		$this->form_validation->set_rules("sub_transaction_id", "Transaction Id", "trim|required|max_length[100]");
		$this->form_validation->set_rules("vch_id", "Voucher", "trim|required|integer|max_length[11]");

		if($this->input->post('form_submit'))
		{
			$subscription = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->subscription_model->create($subscription, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New subscription created.', 'success');
				redirect("admin/subscriptions/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($subscription);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();		$page["vch_ids"] = $this->voucher_model->get_all();

		$this->template->content('subscriptions-menu', null, null, 'page-nav');
		$this->template->content('subscriptions-create', $page);
		$this->template->show();
	}

	public function edit($sub_id)
	{
		$this->template->title('Edit Subscription');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("sub_status", "Status", "trim|required");
		$this->form_validation->set_rules("sub_start_date", "Start Date", "trim|required|datetime");
		$this->form_validation->set_rules("sub_end_date", "End Date", "trim|required|datetime");
		$this->form_validation->set_rules("sub_amount", "Amount", "trim|required|decimal");
		$this->form_validation->set_rules("sub_type", "Type", "trim|required");
		$this->form_validation->set_rules("sub_transaction_id", "Transaction Id", "trim|required|max_length[100]");
		$this->form_validation->set_rules("vch_id", "Voucher", "trim|required|integer|max_length[11]");

		if($this->input->post('form_submit'))
		{
			$subscription = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$subscription['sub_id'] = $sub_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->subscription_model->update($subscription, $fields);

				$this->template->notification('Subscription updated.', 'success');
				redirect("admin/subscriptions/edit/$sub_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($subscription);
		}

		$page = array();
		$page['subscription'] = $this->subscription_model->get_one($sub_id);

		if($page['subscription'] === false)
		{
			$this->template->notification('Subscription was not found.', 'danger');
			redirect('admin/subscriptions');
		}

		$page["usr_ids"] = $this->user_model->get_all();		$page["vch_ids"] = $this->voucher_model->get_all();
		$this->template->content('subscriptions-menu', null, null, 'page-nav');
		$this->template->content('subscriptions-edit', $page);
		$this->template->show();
	}

	public function view($sub_id)
	{
		$this->template->title('View Subscription');

		$page = array();
		$page['subscription'] = $this->subscription_model->get_one($sub_id);

		if($page['subscription'] === false)
		{
			$this->template->notification('Subscription was not found.', 'danger');
			redirect('admin/subscriptions');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();		$page["vch_ids"] = $this->voucher_model->get_all();

		$this->template->content('subscriptions-menu', null, null, 'page-nav');
		$this->template->content('subscriptions-view', $page);
		$this->template->show();
	}


}
