<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Preferences extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('preference_model');
		$this->load->model("user_model");		$this->load->model("question_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Preferences');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$prf_ids = $this->input->post('prf_ids');
				if($prf_ids !== false)
				{
					foreach($prf_ids as $prf_id)
					{
						$preference = $this->preference_model->get_one($prf_id);
						if($preference !== false)
						{
							$this->preference_model->delete($prf_id);
						}
					}
					$this->template->notification('Selected preferences were deleted.', 'success');
					redirect('admin/preferences');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();		$page["que_ids"] = $this->question_model->get_all();

		$page['preferences'] = $this->preference_model->pagination("admin/preferences/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['preferences_count'] = $this->preference_model->pagination->total_rows();
		$page['preferences_pagination'] = $this->preference_model->pagination_links();
		$this->template->content('preferences-index', $page);
		$this->template->content('preferences-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Preference');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("que_id", "Question", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("prf_content", "Content", "trim|required");

		if($this->input->post('form_submit'))
		{
			$preference = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->preference_model->create($preference, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New preference created.', 'success');
				redirect("admin/preferences/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($preference);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();		$page["que_ids"] = $this->question_model->get_all();

		$this->template->content('preferences-menu', null, null, 'page-nav');
		$this->template->content('preferences-create', $page);
		$this->template->show();
	}

	public function edit($prf_id)
	{
		$this->template->title('Edit Preference');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("que_id", "Question", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("prf_content", "Content", "trim|required");

		if($this->input->post('form_submit'))
		{
			$preference = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$preference['prf_id'] = $prf_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->preference_model->update($preference, $fields);

				$this->template->notification('Preference updated.', 'success');
				redirect("admin/preferences/edit/$prf_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($preference);
		}

		$page = array();
		$page['preference'] = $this->preference_model->get_one($prf_id);

		if($page['preference'] === false)
		{
			$this->template->notification('Preference was not found.', 'danger');
			redirect('admin/preferences');
		}

		$page["usr_ids"] = $this->user_model->get_all();		$page["que_ids"] = $this->question_model->get_all();
		$this->template->content('preferences-menu', null, null, 'page-nav');
		$this->template->content('preferences-edit', $page);
		$this->template->show();
	}

	public function view($prf_id)
	{
		$this->template->title('View Preference');

		$page = array();
		$page['preference'] = $this->preference_model->get_one($prf_id);

		if($page['preference'] === false)
		{
			$this->template->notification('Preference was not found.', 'danger');
			redirect('admin/preferences');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();		$page["que_ids"] = $this->question_model->get_all();

		$this->template->content('preferences-menu', null, null, 'page-nav');
		$this->template->content('preferences-view', $page);
		$this->template->show();
	}


}
