<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Regions extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model("city_model");
		$this->load->model("region_model");
		$this->load->model("country_model");

		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Regions');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$reg_ids = $this->input->post('reg_ids');
				if($reg_ids !== false)
				{
					foreach($reg_ids as $reg_id)
					{
						$region = $this->region_model->get_one($reg_id);
						if($region !== false)
						{
							$region->reg_status = 'archived';

							$this->region_model->update($region);

							$cities = $this->city_model->get_all(array('city.reg_id'=>$reg_id));

							if($cities->num_rows() > 0){
								foreach($cities->result() as $city){
									$city->cty_status = 'archived';

									$this->city_model->update($city);
								}
							}
						}
					}
					$this->template->notification('Selected regions were deleted.', 'success');
					redirect('admin/regions');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";
		$query = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		if($this->input->get())
		{
			$query = "?".$_SERVER['QUERY_STRING'];
		}

		unset($params['search']);

		$page["cnt_ids"] = $this->country_model->get_all(array('cnt_status'=>'active'));

		$params['reg_status'] = 'active';

		$page['regions'] = $this->region_model->pagination("admin/regions/index/__PAGE__/$query", 'search', $this->input->get('search'), $params);
		$page['regions_count'] = $this->region_model->pagination->total_rows();
		$page['regions_pagination'] = $this->region_model->pagination_links();
		$this->template->content('regions-index', $page);
		$this->template->content('regions-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Region');

		$this->form_validation->set_rules("cnt_id", "Country", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("reg_name", "Name", "trim|required|max_length[100]");

		if($this->input->post('form_submit'))
		{
			$region = $this->extract->post();

			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->region_model->create($region, $fields);
				$id = $result['result']['insert_id'];

				$this->template->notification('New region created.', 'success');
				redirect("admin/regions/edit/$id");
			}
			else
			{
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($region);
		}

		$page = array();

		$page["cnt_ids"] = $this->country_model->get_all();

		$this->template->content('regions-menu', null, null, 'page-nav');
		$this->template->content('regions-create', $page);
		$this->template->show();
	}

	public function edit($reg_id)
	{
		$this->template->title('Edit Region');

		$this->form_validation->set_rules("cnt_id", "Country", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("reg_name", "Name", "trim|required|max_length[100]");

		if($this->input->post('form_submit'))
		{
			$region = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$region['reg_id'] = $reg_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->region_model->update($region, $fields);

				$this->template->notification('Region updated.', 'success');
				redirect("admin/regions/edit/$reg_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($region);
		}

		$page = array();
		$page['region'] = $this->region_model->get_one($reg_id);

		if($page['region'] === false)
		{
			$this->template->notification('Region was not found.', 'danger');
			redirect('admin/regions');
		}

		$page["cnt_ids"] = $this->country_model->get_all();
		$this->template->content('regions-menu', null, null, 'page-nav');
		$this->template->content('regions-edit', $page);
		$this->template->show();
	}
}
