<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_accounts extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->account_type('dev', 'admin');
		$this->access_control->validate();

		$this->load->model('user_accounts_model');
		$this->load->model("_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('User Accounts');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$usa_ids = $this->input->post('usa_ids');
				if($usa_ids !== false)
				{
					foreach($usa_ids as $usa_id)
					{
						$user_accounts = $this->user_accounts_model->get_one($usa_id);
						if($user_accounts !== false)
						{
							$this->user_accounts_model->delete($usa_id);
						}
					}
					$this->template->notification('Selected user accounts were deleted.', 'success');
					redirect('admin/user_accounts');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["s"] = $this->_model->get_all();

		$page['user_accounts'] = $this->user_accounts_model->pagination("admin/user_accounts/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['user_accounts_count'] = $this->user_accounts_model->pagination->total_rows();
		$page['user_accounts_pagination'] = $this->user_accounts_model->pagination_links();
		$this->template->content('user_accounts-index', $page);
		$this->template->content('user_accounts-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create User Accounts');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usa_type", "Type", "trim|required|max_length[50]");
		$this->form_validation->set_rules("usa_email", "Email", "trim|required|max_length[200]");
		$this->form_validation->set_rules("usa_access_id", "Access Id", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usa_access_token", "Access Token", "trim|required|integer|max_length[11]");

		if($this->input->post('form_submit'))
		{
			$user_accounts = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->user_accounts_model->create($user_accounts, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New user accounts created.', 'success');
				redirect("admin/user_accounts/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($user_accounts);
		}

		$page = array();

		$page["s"] = $this->_model->get_all();

		$this->template->content('user_accounts-menu', null, null, 'page-nav');
		$this->template->content('user_accounts-create', $page);
		$this->template->show();
	}

	public function edit($usa_id)
	{
		$this->template->title('Edit User Accounts');

		$this->form_validation->set_rules("usr_id", "", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usa_type", "Type", "trim|required|max_length[50]");
		$this->form_validation->set_rules("usa_email", "Email", "trim|required|max_length[200]");
		$this->form_validation->set_rules("usa_access_id", "Access Id", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usa_access_token", "Access Token", "trim|required|integer|max_length[11]");

		if($this->input->post('form_submit'))
		{
			$user_accounts = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$user_accounts['usa_id'] = $usa_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->user_accounts_model->update($user_accounts, $fields);

				$this->template->notification('User accounts updated.', 'success');
				redirect("admin/user_accounts/edit/$usa_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($user_accounts);
		}

		$page = array();
		$page['user_accounts'] = $this->user_accounts_model->get_one($usa_id);

		if($page['user_accounts'] === false)
		{
			$this->template->notification('User accounts was not found.', 'danger');
			redirect('admin/user_accounts');
		}

		$page["s"] = $this->_model->get_all();
		$this->template->content('user_accounts-menu', null, null, 'page-nav');
		$this->template->content('user_accounts-edit', $page);
		$this->template->show();
	}

	public function view($usa_id)
	{
		$this->template->title('View User Accounts');

		$page = array();
		$page['user_accounts'] = $this->user_accounts_model->get_one($usa_id);

		if($page['user_accounts'] === false)
		{
			$this->template->notification('User accounts was not found.', 'danger');
			redirect('admin/user_accounts');
		}
		
		$page["s"] = $this->_model->get_all();

		$this->template->content('user_accounts-menu', null, null, 'page-nav');
		$this->template->content('user_accounts-view', $page);
		$this->template->show();
	}


}
