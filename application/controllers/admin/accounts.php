<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
	
		$this->access_control->account_type('developer', 'admin');
		$this->access_control->validate();
		
		$this->load->model('account_model');
		$this->load->helper('nav');
	}
	
	public function index($acc_status = false, $acc_id = false) 
	{
		$this->template->title('Accounts');
		
		if($this->input->post('form_mode'))
		{

			$form_mode = $this->input->post('form_mode');

			if($form_mode === 'delete')
			{
				$account_ids = $this->input->post('acc_ids');
				if($account_ids !== false)
				{
					foreach($account_ids as $account_id)
					{
						$account = $this->account_model->get_one($account_id);
						if($account !== false)
						{
							// Prevent admin user from being deleted
							if($account->acc_type == 'content creator')
							{
								$this->account_model->delete($account_id);
								$this->template->notification('Selected accounts were deleted.', 'success');
							}else{
								$this->template->notification("Can't delete super admin and admin accounts.", 'danger');
							}
						}
					}
				}
			}
		}
		
		$page = array();
		$page['accounts'] = $this->account_model->pagination('admin/accounts/index/__PAGE__', 'get_all', array('acc_id !=' => $this->session->userdata('acc_id')));
		$page['accounts_pagination'] = $this->account_model->pagination_links();
		
		$this->template->content('accounts-index', $page);
		$this->template->content('accounts-menu', null, 'admin', 'page-nav');
		$this->template->show();
	}
	
	public function create() 
	{
		$this->template->title('Create Account');
	
		$this->form_validation->set_rules('acc_username', 'Email', 'trim|required|valid_email|max_length[150]');
		$this->form_validation->set_rules('acc_password', 'Password', 'required|min_length[6]');
		$this->form_validation->set_rules('acc_password2', 'Retype Password', 'required|matches[acc_password]');
		$this->form_validation->set_rules('acc_first_name', 'First Name', 'trim|required|max_length[60]');
		$this->form_validation->set_rules('acc_last_name', 'Last Name', 'trim|required|max_length[30]');
		$this->form_validation->set_rules('acc_type', 'Account Type', 'trim|required');
		
		if($this->input->post('form_submit'))
		{
			$account = $this->extract->post();

			if($this->form_validation->run() !== false)
			{
				$result = $this->account_model->create($account);

				if($result)
				{
					$this->template->notification('New account created.', 'success');
					redirect('admin/accounts');
				}
			}
			else
			{
				$this->template->notification(validation_errors(), 'danger');
			}

			unset($account['acc_password']);
			unset($account['acc_password2']);
			$this->template->autofill($account);
		}
		
		$this->template->content('accounts-create');
		$this->template->content('accounts-menu', null, 'admin', 'page-nav');
		$this->template->show();
	}
	
	public function view($id = 0)
	{
		$this->template->title('Accounts');

		$page = array();
		$page['accounts'] = $this->account_model->pagination("admin/accounts/view/$id/__PAGE__", 'get_all', array('acc_type !=' => 'developer'));
		$page['accounts_pagination'] = $this->account_model->pagination_links();
		
		$account = $this->account_model->get_one($id);
		if($account !== false)
		{
			// Prevent viewing 'developer' accounts if user is not 'developer'
			if($account->acc_type == 'developer' && !$this->access_control->check_account_type('developer'))
			{
				redirect('admin/accounts');
			}
			
			$page['account'] = $account;
			$this->template->content('accounts-view', $page);
			$this->template->content('accounts-menu', null, 'admin', 'page-nav');
			$this->template->show();
		}
		else
		{
			redirect('admin/accounts');
		}
	}

	public function edit($id = 0)
	{
		if($this->session->userdata('acc_type') == 'developer'){
			$this->template->title('Edit Account');
		
			$this->form_validation->set_rules('acc_username', 'Email', 'trim|required|valid_email|max_length[150]');
			$this->form_validation->set_rules('acc_first_name', 'First Name', 'trim|required|max_length[60]');
			$this->form_validation->set_rules('acc_last_name', 'Last Name', 'trim');
			$this->form_validation->set_rules('acc_type', 'Account Type', 'trim|required');
			
			if($this->input->post('form_submit'))
			{
				$account = $this->extract->post();

				if($this->form_validation->run() !== false)
				{
					$account['acc_id'] = $id;

					$fields = $this->form_validation->get_fields();

					$rows_affected = $this->account_model->update($account, $fields);

					$this->template->notification('Account updated.', 'success');
					redirect("admin/accounts/edit/$id");
				}
				else
				{
					$this->template->notification(validation_errors(), 'danger');
				}

				$this->template->autofill($account);
			}

			$page = array();
			$page['account'] = $this->account_model->get_one($id);

			if($page['account'] === false)
			{
				$this->template->notification('Account was not found.', 'danger');
				redirect('admin/accounts');
			}

			$this->template->content('accounts-menu', null, null, 'page-nav');
			$this->template->content('accounts-edit', $page);
			$this->template->show();
		}else{
			$this->template->notification('Permission denied.', 'danger');
			redirect("admin/accounts/");
		}
	}
	
	public function reset_password($id = 0)
	{
		$this->template->title('Reset Password');
		
		$page = array();
		$page['accounts'] = $this->account_model->pagination("admin/accounts/reset_password/$id/__PAGE__", 'get_all', array('acc_type !=' => 'developer'));
		$page['accounts_pagination'] = $this->account_model->pagination_links();
		
		$account = $this->account_model->get_one($id);
		if($account === false)
		{
			redirect('admin/accounts');
		}
		else
		{
			// Prevent viewing 'developer' accounts if user is not 'developer' 
			if($account->acc_type == 'developer' && !$this->access_control->check_account_type('developer'))
			{
				redirect('admin/accounts');
			}
		
			if($this->input->post('form_submit') !== false)
			{
				
				$password = $this->input->post('acc_password');
				$this->account_model->change_password($account->acc_username, $password);
				
				$this->template->notification('Password for ' . $account->acc_username . ' was changed.', 'success');
				
				redirect('admin/accounts');
			}
			else
			{
				$page['account'] = $account;
				$page['acc_password'] = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 12)),0, 12);
				$this->template->content('accounts-reset_password', $page);
				$this->template->show();
			}
		}
	}
	
	public function clear()
	{
		$user_api_ids = $this->db->get('user_api_ids');
		if($user_api_ids->num_rows() > 0){
			foreach($user_api_ids->result() as $user_api_id){
				$user = $this->user_model->get_one($user_api_id->usr_id);

				if(!$user){
					echo $user_api_id->uai_id.' ';
					$this->user_api_ids_model->delete($user_api_id->uai_id);
				}
			}
		}

		echo 'user_api_ids ';

		$user_rewards = $this->db->get('user_reward');
		if($user_rewards->num_rows() > 0){
			foreach($user_rewards->result() as $user_reward){
				$user = $this->user_model->get_one($user_reward->usr_id);

				if(!$user){
					echo $user_reward->ure_id.' ';
					$this->user_reward_model->delete($user_reward->ure_id);
				}
			}
		}

		echo 'user_rewards ';

		$user_reward_reviews = $this->db->get('user_reward_review');
		if($user_reward_reviews->num_rows() > 0){
			foreach($user_reward_reviews->result() as $user_reward_review){
				$user = $this->user_model->get_one($user_reward_review->usr_id);
				
				if(!$user){
					echo $user_reward_review->urr_id.' ';
					$this->user_reward_review_model->delete($user_reward_review->urr_id);
				}	
			}
		}

		echo 'user_reward_reviews ';

		$user_passionpoints = $this->db->get('user_passionpoint');
		if($user_passionpoints->num_rows() > 0){
			foreach($user_passionpoints->result() as $user_passionpoint){
				$user = $this->user_model->get_one($user_passionpoint->usr_id);

				if(!$user){
					echo $user_passionpoint->upa_id.' ';
					$this->user_passionpoint_model->delete($user_passionpoint->upa_id);
				}
			}
		}

		echo 'user_passionpoints ';

		$user_events = $this->db->get('user_event');
		if($user_events->num_rows() > 0){
			foreach($user_events->result() as $user_event){
				$user = $this->user_model->get_one($user_event->usr_id);

				if(!$user){
					echo $user_event->uev_id.' ';
					$this->user_event_model->delete($user_event->uev_id);
				}
			}
		}

		echo 'user_events ';

		$app_activities = $this->db->get('app_activity');
		if($app_activities->num_rows() > 0){
			foreach($app_activities->result() as $app_activity){
				$user = $this->user_model->get_one($app_activity->usr_id);

				if(!$user){
					echo $app_activity->apa_id.' ';
					$this->app_activity_model->delete($app_activity->apa_id);
				}
			}
		}

		echo 'app_activities ';
	}
}
