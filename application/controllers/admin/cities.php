<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cities extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model("city_model");
		$this->load->model("region_model");
		$this->load->model("country_model");

		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Cities');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$cty_ids = $this->input->post('cty_ids');
				if($cty_ids !== false)
				{
					foreach($cty_ids as $cty_id)
					{
						$city = $this->city_model->get_one($cty_id);
						if($city !== false)
						{
							$city->cty_status = 'archived';

							$this->city_model->update($city);
						}
					}
					$this->template->notification('Selected cities were deleted.', 'success');
					redirect('admin/cities');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";
		$query = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		if($this->input->get())
		{
			$query = "?".$_SERVER['QUERY_STRING'];
		}

		unset($params['search']);


		$page["cnt_ids"] = $this->country_model->get_all(array('cnt_status'=>'active'));

		$params['cty_status'] = 'active';

		$page['cities'] = $this->city_model->pagination("admin/cities/index/__PAGE__/$query", 'search', $this->input->get('search'), $params);
		$page['cities_count'] = $this->city_model->pagination->total_rows();
		$page['cities_pagination'] = $this->city_model->pagination_links();
		$this->template->content('cities-index', $page);
		$this->template->content('cities-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create City');

		$this->form_validation->set_rules("reg_id", "Region", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("cty_name", "Name", "trim|required|max_length[100]");

		if($this->input->post('form_submit'))
		{
			$city = $this->extract->post();

			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->city_model->create($city, $fields);
				$id = $result['result']['insert_id'];

				$this->template->notification('New city created.', 'success');
				redirect("admin/cities/edit/$id");
			}
			else
			{
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($city);
		}

		$page = array();

		$page["reg_ids"] = $this->region_model->get_all(array('reg_status'=>'active'));

		$this->template->content('cities-menu', null, null, 'page-nav');
		$this->template->content('cities-create', $page);
		$this->template->show();
	}

	public function edit($cty_id)
	{
		$this->template->title('Edit City');

		$this->form_validation->set_rules("reg_id", "Region", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("cty_name", "Name", "trim|required|max_length[100]");

		if($this->input->post('form_submit'))
		{
			$city = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$city['cty_id'] = $cty_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->city_model->update($city, $fields);

				$this->template->notification('City updated.', 'success');
				redirect("admin/cities/edit/$cty_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($city);
		}

		$page = array();
		$page['city'] = $this->city_model->get_one($cty_id);

		if($page['city'] === false)
		{
			$this->template->notification('City was not found.', 'danger');
			redirect('admin/cities');
		}

		$page["reg_ids"] = $this->region_model->get_all(array('reg_status'=>'active'));

		$this->template->content('cities-menu', null, null, 'page-nav');
		$this->template->content('cities-edit', $page);
		$this->template->show();
	}
}
