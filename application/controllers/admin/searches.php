<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Searches extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('search_model');
		$this->load->model("user_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Searches');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$sch_ids = $this->input->post('sch_ids');
				if($sch_ids !== false)
				{
					foreach($sch_ids as $sch_id)
					{
						$search = $this->search_model->get_one($sch_id);
						if($search !== false)
						{
							$this->search_model->delete($sch_id);
						}
					}
					$this->template->notification('Selected searches were deleted.', 'success');
					redirect('admin/searches');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();

		$page['searches'] = $this->search_model->pagination("admin/searches/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['searches_count'] = $this->search_model->pagination->total_rows();
		$page['searches_pagination'] = $this->search_model->pagination_links();
		$this->template->content('searches-index', $page);
		$this->template->content('searches-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Search');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("sch_name", "Name", "trim|required|max_length[100]");
		$this->form_validation->set_rules("sch_content", "Content", "trim|required");
		$this->form_validation->set_rules("sch_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$search = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->search_model->create($search, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New search created.', 'success');
				redirect("admin/searches/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($search);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('searches-menu', null, null, 'page-nav');
		$this->template->content('searches-create', $page);
		$this->template->show();
	}

	public function edit($sch_id)
	{
		$this->template->title('Edit Search');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("sch_name", "Name", "trim|required|max_length[100]");
		$this->form_validation->set_rules("sch_content", "Content", "trim|required");
		$this->form_validation->set_rules("sch_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$search = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$search['sch_id'] = $sch_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->search_model->update($search, $fields);

				$this->template->notification('Search updated.', 'success');
				redirect("admin/searches/edit/$sch_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($search);
		}

		$page = array();
		$page['search'] = $this->search_model->get_one($sch_id);

		if($page['search'] === false)
		{
			$this->template->notification('Search was not found.', 'danger');
			redirect('admin/searches');
		}

		$page["usr_ids"] = $this->user_model->get_all();
		$this->template->content('searches-menu', null, null, 'page-nav');
		$this->template->content('searches-edit', $page);
		$this->template->show();
	}

	public function view($sch_id)
	{
		$this->template->title('View Search');

		$page = array();
		$page['search'] = $this->search_model->get_one($sch_id);

		if($page['search'] === false)
		{
			$this->template->notification('Search was not found.', 'danger');
			redirect('admin/searches');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('searches-menu', null, null, 'page-nav');
		$this->template->content('searches-view', $page);
		$this->template->show();
	}


}
