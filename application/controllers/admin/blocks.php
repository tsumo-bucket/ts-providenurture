<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blocks extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('block_model');
		$this->load->model("user_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Blocks');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$blo_ids = $this->input->post('blo_ids');
				if($blo_ids !== false)
				{
					foreach($blo_ids as $blo_id)
					{
						$block = $this->block_model->get_one($blo_id);
						if($block !== false)
						{
							$this->block_model->delete($blo_id);
						}
					}
					$this->template->notification('Selected blocks were deleted.', 'success');
					redirect('admin/blocks');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();

		$page['blocks'] = $this->block_model->pagination("admin/blocks/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['blocks_count'] = $this->block_model->pagination->total_rows();
		$page['blocks_pagination'] = $this->block_model->pagination_links();
		$this->template->content('blocks-index', $page);
		$this->template->content('blocks-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Block');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("blo_user", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("blo_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$block = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->block_model->create($block, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New block created.', 'success');
				redirect("admin/blocks/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($block);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('blocks-menu', null, null, 'page-nav');
		$this->template->content('blocks-create', $page);
		$this->template->show();
	}

	public function edit($blo_id)
	{
		$this->template->title('Edit Block');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("blo_user", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("blo_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$block = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$block['blo_id'] = $blo_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->block_model->update($block, $fields);

				$this->template->notification('Block updated.', 'success');
				redirect("admin/blocks/edit/$blo_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($block);
		}

		$page = array();
		$page['block'] = $this->block_model->get_one($blo_id);

		if($page['block'] === false)
		{
			$this->template->notification('Block was not found.', 'danger');
			redirect('admin/blocks');
		}

		$page["usr_ids"] = $this->user_model->get_all();
		$this->template->content('blocks-menu', null, null, 'page-nav');
		$this->template->content('blocks-edit', $page);
		$this->template->show();
	}

	public function view($blo_id)
	{
		$this->template->title('View Block');

		$page = array();
		$page['block'] = $this->block_model->get_one($blo_id);

		if($page['block'] === false)
		{
			$this->template->notification('Block was not found.', 'danger');
			redirect('admin/blocks');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('blocks-menu', null, null, 'page-nav');
		$this->template->content('blocks-view', $page);
		$this->template->show();
	}


}
