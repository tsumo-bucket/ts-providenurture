<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->access_control->logged_in();
		$this->access_control->account_type('developer', 'admin');
		$this->access_control->validate();

		$this->load->model('page_model');
		$this->load->model('page_category_model');
		$this->load->helper('nav');

	}

	public function index($pct_id = -1)
	{
		$this->template->title('Pages');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$page_ids = $this->input->post('pag_ids');
				if($page_ids !== false)
				{
					foreach($page_ids as $page_id)
					{
						$page = $this->page_model->get_one($page_id);
						if($page !== false)
						{
							$this->page_model->delete($page_id);
						}
					}
					$this->template->notification('Selected pages were deleted.', 'success');
				}
			}
		}

		$page = array();
		$filter = array();

		$page['page_categories'] = $this->page_category_model->get_all();

		if(!$this->access_control->check_account_type('developer'))
		{
			$filter['pag_type'] = 'editable';
		}
		else
		{
			$this->template->content('menu-pages', null, 'admin', 'page-nav');
		}

		if($pct_id != -1)
		{
			$filter['page.pct_id'] = $pct_id;
		}

		$page['pages'] = $this->page_model->pagination("admin/pages/index/$pct_id/__PAGE__", 'get_all_with_categories', $filter);
		$page['pages_pagination'] = $this->page_model->pagination_links();
		$this->template->content('pages-index', $page);
		$this->template->content('pages-menu', null, 'admin', 'page-nav');

		$this->template->show();
	}

	public function create($mce_fix = null)
	{
		$this->template->title('Create Page');

		$this->form_validation->set_rules('pag_title', 'Page Title', 'trim|required|max_length[140]');
		$this->form_validation->set_rules('pct_id', 'Category', 'integer|required');
		$this->form_validation->set_rules('pag_content', 'Content', '');
		$this->form_validation->set_rules('pag_date_published', 'Date Published', 'trim');
		$this->form_validation->set_rules('pag_status', 'Status', 'trim|required');
		if($this->access_control->check_account_type('developer'))
		{
			$this->form_validation->set_rules('pag_type', 'Type', 'trim|required');
		}

		if($this->input->post('form_submit'))
		{
			$page = $this->extract->post();

			if($this->form_validation->run() !== false)
			{
				$pct = $this->page_category_model->get_one($page['pct_id']);
				$pages = $this->page_model->get_all_from_category_id($page['pct_id']);
				if($pages && ($pct->pct_name == 'Terms & Conditions' || $pct->pct_name == 'Privacy Policy')){
					$this->template->notification('Page under selected category already exists.', 'warning');
				}else{
					$page = $this->page_model->create($page);

					$this->template->notification('New page created.', 'success');
					redirect('admin/pages/edit/'.$page['result']['insert_id']);
				}
			}
			else
			{
				$this->template->notification(validation_errors(), 'warning');
			}
			$this->template->autofill($page);
		}

		$page_params = array();

		$page_params['page_categories'] = $this->page_category_model->get_all();


		$this->template->content('pages-create', $page_params);
		$this->template->content('pages-menu', null, 'admin', 'page-nav');

		$this->template->show();
	}

	public function edit($page_id)
	{
		$this->template->title('Edit Page');

		$this->form_validation->set_rules('pag_title', 'Page Title', 'trim|required|max_length[140]');
		$this->form_validation->set_rules('pct_id', 'Category', 'integer');
		$this->form_validation->set_rules('pag_content', 'Content', '');
		$this->form_validation->set_rules('pag_date_published', 'Date Published', 'trim');
		$this->form_validation->set_rules('pag_status', 'Status', 'trim|required');

		if($this->access_control->check_account_type('developer'))
		{
			$this->form_validation->set_rules('pag_type', 'Type', 'trim|required');
		}

		if($this->input->post('form_submit'))
		{
			$page = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$page['pag_id'] = $page_id;

				$pct = $this->page_category_model->get_one($page['pct_id']);
				$pages = $this->page_model->get_all_from_category_id($page['pct_id']);
				if(($pages && $pages->pag_id != $page['pag_id']) && ($pct->pct_name == 'Terms & Conditions' || $pct->pct_name == 'Privacy Policy')){
					$this->template->notification('Page under selected category already exists.', 'warning');
				}else{
					$rows_affected = $this->page_model->update($page);

					$this->template->notification('Page updated.', 'success');
					redirect('admin/pages/edit/'.$page_id);
				}
			}
			else
			{
				$this->template->notification(validation_errors(), 'warning');
			}
			$this->template->autofill($page);
		}

		$this->load->model('page_category_model');
		$page_params = array();
		$page_params['page_categories'] = $this->page_category_model->get_all();
		$page_params['page'] = $this->page_model->get_one($page_id);
		

		if($page_params['page'] === false)
		{
			$this->template->notification('Page was not found.', 'danger');
			redirect('admin/pages');
		}

		$this->template->content('pages-edit', $page_params);
		$this->template->content('pages-menu', null, 'admin', 'page-nav');

		$this->template->show();
	}

}
