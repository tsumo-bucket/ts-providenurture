<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('notification_model');
		$this->load->model("user_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Notifications');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$not_ids = $this->input->post('not_ids');
				if($not_ids !== false)
				{
					foreach($not_ids as $not_id)
					{
						$notification = $this->notification_model->get_one($not_id);
						if($notification !== false)
						{
							$this->notification_model->delete($not_id);
						}
					}
					$this->template->notification('Selected notifications were deleted.', 'success');
					redirect('admin/notifications');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();

		$page['notifications'] = $this->notification_model->pagination("admin/notifications/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['notifications_count'] = $this->notification_model->pagination->total_rows();
		$page['notifications_pagination'] = $this->notification_model->pagination_links();
		$this->template->content('notifications-index', $page);
		$this->template->content('notifications-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Notification');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("not_message", "Message", "trim|required");
		$this->form_validation->set_rules("not_like", "Like", "trim|required");
		$this->form_validation->set_rules("not_profile", "Profile", "trim|required");
		$this->form_validation->set_rules("not_content", "Content", "trim|required");
		$this->form_validation->set_rules("not_email", "Email", "trim|required");

		if($this->input->post('form_submit'))
		{
			$notification = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->notification_model->create($notification, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New notification created.', 'success');
				redirect("admin/notifications/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($notification);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('notifications-menu', null, null, 'page-nav');
		$this->template->content('notifications-create', $page);
		$this->template->show();
	}

	public function edit($not_id)
	{
		$this->template->title('Edit Notification');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("not_message", "Message", "trim|required");
		$this->form_validation->set_rules("not_like", "Like", "trim|required");
		$this->form_validation->set_rules("not_profile", "Profile", "trim|required");
		$this->form_validation->set_rules("not_content", "Content", "trim|required");
		$this->form_validation->set_rules("not_email", "Email", "trim|required");

		if($this->input->post('form_submit'))
		{
			$notification = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$notification['not_id'] = $not_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->notification_model->update($notification, $fields);

				$this->template->notification('Notification updated.', 'success');
				redirect("admin/notifications/edit/$not_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($notification);
		}

		$page = array();
		$page['notification'] = $this->notification_model->get_one($not_id);

		if($page['notification'] === false)
		{
			$this->template->notification('Notification was not found.', 'danger');
			redirect('admin/notifications');
		}

		$page["usr_ids"] = $this->user_model->get_all();
		$this->template->content('notifications-menu', null, null, 'page-nav');
		$this->template->content('notifications-edit', $page);
		$this->template->show();
	}

	public function view($not_id)
	{
		$this->template->title('View Notification');

		$page = array();
		$page['notification'] = $this->notification_model->get_one($not_id);

		if($page['notification'] === false)
		{
			$this->template->notification('Notification was not found.', 'danger');
			redirect('admin/notifications');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('notifications-menu', null, null, 'page-nav');
		$this->template->content('notifications-view', $page);
		$this->template->show();
	}


}
