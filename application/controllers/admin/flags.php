<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Flags extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('flag_model');
		$this->load->model("user_model");		
		$this->load->model("user_api_model");		
		$this->load->model("post_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Flags');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$flg_ids = $this->input->post('flg_ids');
				if($flg_ids !== false)
				{
					foreach($flg_ids as $flg_id)
					{
						$flag = $this->flag_model->get_one($flg_id);
						if($flag !== false)
						{
							$this->flag_model->delete($flg_id);
						}
					}
					$this->template->notification('Selected flags were deleted.', 'success');
					redirect('admin/flags');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		unset($params['search']);

		$page["usr_ids"] = $this->user_model->get_all();		
		$page["pos_ids"] = $this->post_model->get_all();

		$params['flg_status'] = 'active';
		$params['post.pos_status'] = 'active';

		$page['flags'] = $this->flag_model->pagination("admin/flags/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['flags_count'] = $this->flag_model->pagination->total_rows();
		$page['flags_pagination'] = $this->flag_model->pagination_links();
		$this->template->content('flags-index', $page);
		$this->template->content('flags-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function edit($flg_id)
	{
		$this->template->title('Edit Flag');

		$page = array();
		$page['flag'] = $this->flag_model->get_one($flg_id);

		if($this->input->post('form_submit'))
		{
			$flag = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$post = array();
				$post['pos_status'] = 'archived';
				$post['pos_date_modified'] = format_mysql_datetime();

				$this->db->where('pos_id', $page['flag']->pos_id);
				$this->db->update('post', $post);

				$this->template->notification('Post deleted.', 'success');
				redirect("admin/flags/");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($flag);
		}


		if($page['flag'] === false)
		{
			$this->template->notification('Flag was not found.', 'danger');
			redirect('admin/flags');
		}

		$this->template->content('flags-menu', null, null, 'page-nav');
		$this->template->content('flags-edit', $page);
		$this->template->show();
	}
}
