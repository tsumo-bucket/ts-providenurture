<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_locations extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('user_location_model');
		$this->load->model("user_inactive_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('User Locations');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$ulc_ids = $this->input->post('ulc_ids');
				if($ulc_ids !== false)
				{
					foreach($ulc_ids as $ulc_id)
					{
						$user_location = $this->user_location_model->get_one($ulc_id);
						if($user_location !== false)
						{
							$this->user_location_model->delete($ulc_id);
						}
					}
					$this->template->notification('Selected user locations were deleted.', 'success');
					redirect('admin/user_locations');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_inactive_model->get_all();

		$page['user_locations'] = $this->user_location_model->pagination("admin/user_locations/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['user_locations_count'] = $this->user_location_model->pagination->total_rows();
		$page['user_locations_pagination'] = $this->user_location_model->pagination_links();
		$this->template->content('user_locations-index', $page);
		$this->template->content('user_locations-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create User Location');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User Inactive", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("ulc_latitude", "Latitude", "trim|required|max_length[100]");
		$this->form_validation->set_rules("ulc_longitude", "Longitude", "trim|required|max_length[100]");
		$this->form_validation->set_rules("ulc_date_added", "Date Added", "trim|required|datetime");

		if($this->input->post('form_submit'))
		{
			$user_location = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->user_location_model->create($user_location, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New user location created.', 'success');
				redirect("admin/user_locations/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($user_location);
		}

		$page = array();

		$page["usr_ids"] = $this->user_inactive_model->get_all();

		$this->template->content('user_locations-menu', null, null, 'page-nav');
		$this->template->content('user_locations-create', $page);
		$this->template->show();
	}

	public function edit($ulc_id)
	{
		$this->template->title('Edit User Location');

		$this->form_validation->set_rules("usr_id", "User Inactive", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("ulc_latitude", "Latitude", "trim|required|max_length[100]");
		$this->form_validation->set_rules("ulc_longitude", "Longitude", "trim|required|max_length[100]");
		$this->form_validation->set_rules("ulc_date_added", "Date Added", "trim|required|datetime");

		if($this->input->post('form_submit'))
		{
			$user_location = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$user_location['ulc_id'] = $ulc_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->user_location_model->update($user_location, $fields);

				$this->template->notification('User location updated.', 'success');
				redirect("admin/user_locations/edit/$ulc_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($user_location);
		}

		$page = array();
		$page['user_location'] = $this->user_location_model->get_one($ulc_id);

		if($page['user_location'] === false)
		{
			$this->template->notification('User location was not found.', 'danger');
			redirect('admin/user_locations');
		}

		$page["usr_ids"] = $this->user_inactive_model->get_all();
		$this->template->content('user_locations-menu', null, null, 'page-nav');
		$this->template->content('user_locations-edit', $page);
		$this->template->show();
	}

	public function view($ulc_id)
	{
		$this->template->title('View User Location');

		$page = array();
		$page['user_location'] = $this->user_location_model->get_one($ulc_id);

		if($page['user_location'] === false)
		{
			$this->template->notification('User location was not found.', 'danger');
			redirect('admin/user_locations');
		}
		
		$page["usr_ids"] = $this->user_inactive_model->get_all();

		$this->template->content('user_locations-menu', null, null, 'page-nav');
		$this->template->content('user_locations-view', $page);
		$this->template->show();
	}


}
