<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_inactives extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('user_inactive_model');
		$this->load->model("user_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('User Inactives');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$usi_ids = $this->input->post('usi_ids');
				if($usi_ids !== false)
				{
					foreach($usi_ids as $usi_id)
					{
						$user_inactive = $this->user_inactive_model->get_one($usi_id);
						if($user_inactive !== false)
						{
							$this->user_inactive_model->delete($usi_id);
						}
					}
					$this->template->notification('Selected user inactives were deleted.', 'success');
					redirect('admin/user_inactives');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();

		$page['user_inactives'] = $this->user_inactive_model->pagination("admin/user_inactives/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['user_inactives_count'] = $this->user_inactive_model->pagination->total_rows();
		$page['user_inactives_pagination'] = $this->user_inactive_model->pagination_links();
		$this->template->content('user_inactives-index', $page);
		$this->template->content('user_inactives-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create User Inactive');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usi_reason", "Reason", "trim|required");
		$this->form_validation->set_rules("usi_comment", "Comment", "trim|required");
		$this->form_validation->set_rules("usi_type", "Type", "trim|required");
		$this->form_validation->set_rules("usi_status", "Status", "trim|required");
		$this->form_validation->set_rules("usi_created_date", "Created Date", "trim|required|datetime");
		$this->form_validation->set_rules("usi_modified_date", "Modified Date", "trim|required|datetime");

		if($this->input->post('form_submit'))
		{
			$user_inactive = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->user_inactive_model->create($user_inactive, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New user inactive created.', 'success');
				redirect("admin/user_inactives/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($user_inactive);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('user_inactives-menu', null, null, 'page-nav');
		$this->template->content('user_inactives-create', $page);
		$this->template->show();
	}

	public function edit($usi_id)
	{
		$this->template->title('Edit User Inactive');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usi_reason", "Reason", "trim|required");
		$this->form_validation->set_rules("usi_comment", "Comment", "trim|required");
		$this->form_validation->set_rules("usi_type", "Type", "trim|required");
		$this->form_validation->set_rules("usi_status", "Status", "trim|required");
		$this->form_validation->set_rules("usi_created_date", "Created Date", "trim|required|datetime");
		$this->form_validation->set_rules("usi_modified_date", "Modified Date", "trim|required|datetime");

		if($this->input->post('form_submit'))
		{
			$user_inactive = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$user_inactive['usi_id'] = $usi_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->user_inactive_model->update($user_inactive, $fields);

				$this->template->notification('User inactive updated.', 'success');
				redirect("admin/user_inactives/edit/$usi_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($user_inactive);
		}

		$page = array();
		$page['user_inactive'] = $this->user_inactive_model->get_one($usi_id);

		if($page['user_inactive'] === false)
		{
			$this->template->notification('User inactive was not found.', 'danger');
			redirect('admin/user_inactives');
		}

		$page["usr_ids"] = $this->user_model->get_all();
		$this->template->content('user_inactives-menu', null, null, 'page-nav');
		$this->template->content('user_inactives-edit', $page);
		$this->template->show();
	}

	public function view($usi_id)
	{
		$this->template->title('View User Inactive');

		$page = array();
		$page['user_inactive'] = $this->user_inactive_model->get_one($usi_id);

		if($page['user_inactive'] === false)
		{
			$this->template->notification('User inactive was not found.', 'danger');
			redirect('admin/user_inactives');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('user_inactives-menu', null, null, 'page-nav');
		$this->template->content('user_inactives-view', $page);
		$this->template->show();
	}


}
