<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_api_ids extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('user_api_ids_model');
		$this->load->model("user_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('User Api Ids');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$uai_ids = $this->input->post('uai_ids');
				if($uai_ids !== false)
				{
					foreach($uai_ids as $uai_id)
					{
						$user_api_ids = $this->user_api_ids_model->get_one($uai_id);
						if($user_api_ids !== false)
						{
							$this->user_api_ids_model->delete($uai_id);
						}
					}
					$this->template->notification('Selected user api ids were deleted.', 'success');
					redirect('admin/user_api_ids');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();

		$page['user_api_ids'] = $this->user_api_ids_model->pagination("admin/user_api_ids/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['user_api_ids_count'] = $this->user_api_ids_model->pagination->total_rows();
		$page['user_api_ids_pagination'] = $this->user_api_ids_model->pagination_links();
		$this->template->content('user_api_ids-index', $page);
		$this->template->content('user_api_ids-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create User Api Ids');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("uai_type", "Type", "trim|required");
		$this->form_validation->set_rules("uai_api_id", "Api Id", "trim|required");
		$this->form_validation->set_rules("uai_api_token", "Api Token", "trim|required");
		$this->form_validation->set_rules("uai_api_secret", "Api Secret", "trim|required");

		if($this->input->post('form_submit'))
		{
			$user_api_ids = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->user_api_ids_model->create($user_api_ids, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New user api ids created.', 'success');
				redirect("admin/user_api_ids/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($user_api_ids);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('user_api_ids-menu', null, null, 'page-nav');
		$this->template->content('user_api_ids-create', $page);
		$this->template->show();
	}

	public function edit($uai_id)
	{
		$this->template->title('Edit User Api Ids');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("uai_type", "Type", "trim|required");
		$this->form_validation->set_rules("uai_api_id", "Api Id", "trim|required");
		$this->form_validation->set_rules("uai_api_token", "Api Token", "trim|required");
		$this->form_validation->set_rules("uai_api_secret", "Api Secret", "trim|required");

		if($this->input->post('form_submit'))
		{
			$user_api_ids = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$user_api_ids['uai_id'] = $uai_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->user_api_ids_model->update($user_api_ids, $fields);

				$this->template->notification('User api ids updated.', 'success');
				redirect("admin/user_api_ids/edit/$uai_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($user_api_ids);
		}

		$page = array();
		$page['user_api_ids'] = $this->user_api_ids_model->get_one($uai_id);

		if($page['user_api_ids'] === false)
		{
			$this->template->notification('User api ids was not found.', 'danger');
			redirect('admin/user_api_ids');
		}

		$page["usr_ids"] = $this->user_model->get_all();
		$this->template->content('user_api_ids-menu', null, null, 'page-nav');
		$this->template->content('user_api_ids-edit', $page);
		$this->template->show();
	}

	public function view($uai_id)
	{
		$this->template->title('View User Api Ids');

		$page = array();
		$page['user_api_ids'] = $this->user_api_ids_model->get_one($uai_id);

		if($page['user_api_ids'] === false)
		{
			$this->template->notification('User api ids was not found.', 'danger');
			redirect('admin/user_api_ids');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();

		$this->template->content('user_api_ids-menu', null, null, 'page-nav');
		$this->template->content('user_api_ids-view', $page);
		$this->template->show();
	}


}
