<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Libraries extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->access_control->logged_in();
		$this->access_control->validate();
		$this->mythos->library('ciqrcode');
		$this->mythos->library('facebook');
		$this->mythos->library('youtube');
	}

	public function index() 
	{
		$this->template->title('Library samples');
		$page = array();
		$this->template->content('libraries-index', $page);
		$this->template->show();
	}


	//Generate a QR Code
	public function qr_generate(){
		$params = array();
		$params['data'] = $this->input->post('qr_text');
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH."/uploads/images/qr.png";
		$this->ciqrcode->generate($params);
		$url = site_url('/uploads/images/qr.png');
		echo "<img src='$url'/>";
	}


	public function fb_login(){
		
	}

	
}