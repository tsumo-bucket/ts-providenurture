<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conversations extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('conversation_model');



		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Conversations');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$cnv_ids = $this->input->post('cnv_ids');
				if($cnv_ids !== false)
				{
					foreach($cnv_ids as $cnv_id)
					{
						$conversation = $this->conversation_model->get_one($cnv_id);
						if($conversation !== false)
						{
							$this->conversation_model->delete($cnv_id);
						}
					}
					$this->template->notification('Selected conversations were deleted.', 'success');
					redirect('admin/conversations');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}



		$page['conversations'] = $this->conversation_model->pagination("admin/conversations/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['conversations_count'] = $this->conversation_model->pagination->total_rows();
		$page['conversations_pagination'] = $this->conversation_model->pagination_links();
		$this->template->content('conversations-index', $page);
		$this->template->content('conversations-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Conversation');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("cnv_slug", "Slug", "trim|required|max_length[100]");
		$this->form_validation->set_rules("cnv_user_1", "User 1", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("cnv_user_2", "User 2", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("cnv_user_1_deleted", "User 1 Deleted", "trim|required");
		$this->form_validation->set_rules("cnv_user_1_deleted_date", "User 1 Deleted Date", "trim|required|datetime");
		$this->form_validation->set_rules("cnv_user_2_deleted", "User 2 Deleted", "trim|required");
		$this->form_validation->set_rules("cnv_user_2_deleted_date", "User 2 Deleted Date", "trim|required|datetime");

		if($this->input->post('form_submit'))
		{
			$conversation = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->conversation_model->create($conversation, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New conversation created.', 'success');
				redirect("admin/conversations/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($conversation);
		}

		$page = array();



		$this->template->content('conversations-menu', null, null, 'page-nav');
		$this->template->content('conversations-create', $page);
		$this->template->show();
	}

	public function edit($cnv_id)
	{
		$this->template->title('Edit Conversation');

		$this->form_validation->set_rules("cnv_slug", "Slug", "trim|required|max_length[100]");
		$this->form_validation->set_rules("cnv_user_1", "User 1", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("cnv_user_2", "User 2", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("cnv_user_1_deleted", "User 1 Deleted", "trim|required");
		$this->form_validation->set_rules("cnv_user_1_deleted_date", "User 1 Deleted Date", "trim|required|datetime");
		$this->form_validation->set_rules("cnv_user_2_deleted", "User 2 Deleted", "trim|required");
		$this->form_validation->set_rules("cnv_user_2_deleted_date", "User 2 Deleted Date", "trim|required|datetime");

		if($this->input->post('form_submit'))
		{
			$conversation = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$conversation['cnv_id'] = $cnv_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->conversation_model->update($conversation, $fields);

				$this->template->notification('Conversation updated.', 'success');
				redirect("admin/conversations/edit/$cnv_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($conversation);
		}

		$page = array();
		$page['conversation'] = $this->conversation_model->get_one($cnv_id);

		if($page['conversation'] === false)
		{
			$this->template->notification('Conversation was not found.', 'danger');
			redirect('admin/conversations');
		}


		$this->template->content('conversations-menu', null, null, 'page-nav');
		$this->template->content('conversations-edit', $page);
		$this->template->show();
	}

	public function view($cnv_id)
	{
		$this->template->title('View Conversation');

		$page = array();
		$page['conversation'] = $this->conversation_model->get_one($cnv_id);

		if($page['conversation'] === false)
		{
			$this->template->notification('Conversation was not found.', 'danger');
			redirect('admin/conversations');
		}
		


		$this->template->content('conversations-menu', null, null, 'page-nav');
		$this->template->content('conversations-view', $page);
		$this->template->show();
	}


}
