<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Countries extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('country_model');

		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Countries');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$cnt_ids = $this->input->post('cnt_ids');
				if($cnt_ids !== false)
				{
					foreach($cnt_ids as $cnt_id)
					{
						$country = $this->country_model->get_one($cnt_id);
						if($country !== false)
						{
							$country->cnt_status = 'archived';

							$this->country_model->update($country);

							$regions = $this->region_model->get_all(array('region.cnt_id'=>$cnt_id));

							if($regions->num_rows() > 0){
								foreach($regions->result() as $region){
									$region->reg_status = 'archived';

									$this->region_model->update($region);

									$cities = $this->city_model->get_all(array('city.reg_id'=>$region->reg_id));

									if($cities->num_rows() > 0){
										foreach($cities->result() as $city){
											$city->cty_status = 'archived';

											$this->city_model->update($city);
										}
									}
								}
							}
						}
					}
					$this->template->notification('Selected countries were deleted.', 'success');
					redirect('admin/countries');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";
		$query = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		if($this->input->get())
		{
			$query = "?".$_SERVER['QUERY_STRING'];
		}

		unset($params['search']);

		$params['cnt_status'] = 'active';

		$page['countries'] = $this->country_model->pagination("admin/countries/index/__PAGE__/$query", 'search', $this->input->get('search'), $params);
		$page['countries_count'] = $this->country_model->pagination->total_rows();
		$page['countries_pagination'] = $this->country_model->pagination_links();
		$this->template->content('countries-index', $page);
		$this->template->content('countries-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Country');

		$this->form_validation->set_rules("cnt_name", "Name", "trim|required|max_length[100]");
		$this->form_validation->set_rules("cnt_short_name", "Name", "trim|required|max_length[100]");

		if($this->input->post('form_submit'))
		{
			$country = $this->extract->post();

			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->country_model->create($country, $fields);
				$id = $result['result']['insert_id'];

				$this->template->notification('New country created.', 'success');
				redirect("admin/countries/edit/$id");
			}
			else
			{
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($country);
		}

		$page = array();



		$this->template->content('countries-menu', null, null, 'page-nav');
		$this->template->content('countries-create', $page);
		$this->template->show();
	}

	public function edit($cnt_id)
	{
		$this->template->title('Edit Country');

		$this->form_validation->set_rules("cnt_name", "Name", "trim|required|max_length[100]");
		$this->form_validation->set_rules("cnt_short_name", "Name", "trim|required|max_length[100]");

		if($this->input->post('form_submit'))
		{
			$country = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$country['cnt_id'] = $cnt_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->country_model->update($country, $fields);

				$this->template->notification('Country updated.', 'success');
				redirect("admin/countries/edit/$cnt_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($country);
		}

		$page = array();
		$page['country'] = $this->country_model->get_one($cnt_id);

		if($page['country'] === false)
		{
			$this->template->notification('Country was not found.', 'danger');
			redirect('admin/countries');
		}


		$this->template->content('countries-menu', null, null, 'page-nav');
		$this->template->content('countries-edit', $page);
		$this->template->show();
	}
}
