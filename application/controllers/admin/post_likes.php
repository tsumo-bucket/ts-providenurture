<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post_likes extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->access_control->logged_in();
		$this->access_control->validate();

		$this->load->model('post_like_model');
		$this->load->model("user_model");		$this->load->model("post_model");


		$this->load->helper('nav');
	}

	public function index()
	{
		$this->template->title('Post Likes');

		if($this->input->post('form_mode'))
		{
			$form_mode = $this->input->post('form_mode');

			if($form_mode == 'delete')
			{
				$pol_ids = $this->input->post('pol_ids');
				if($pol_ids !== false)
				{
					foreach($pol_ids as $pol_id)
					{
						$post_like = $this->post_like_model->get_one($pol_id);
						if($post_like !== false)
						{
							$this->post_like_model->delete($pol_id);
						}
					}
					$this->template->notification('Selected post likes were deleted.', 'success');
					redirect('admin/post_likes');
				}
			}
		}

		$page = array();
		$params = $this->input->get();
		$get = "";

		if($this->input->get('search'))
		{
			$get = parse_str($_SERVER['QUERY_STRING'], $_GET);
		}

		$page["usr_ids"] = $this->user_model->get_all();		$page["pos_ids"] = $this->post_model->get_all();

		$page['post_likes'] = $this->post_like_model->pagination("admin/post_likes/index/__PAGE__/$get", 'search', $this->input->get('search'), $params);
		$page['post_likes_count'] = $this->post_like_model->pagination->total_rows();
		$page['post_likes_pagination'] = $this->post_like_model->pagination_links();
		$this->template->content('post_likes-index', $page);
		$this->template->content('post_likes-menu', null, null, 'page-nav');
		$this->template->show();
	}

	public function create()
	{
		$this->template->title('Create Post Like');

		// Use the set_rules from the Form_validation class for form validation.
		// Already combined with jQuery. No extra coding required for JS validation.
		// We get both JS and PHP validation which makes it both secure and user friendly.
		// NOTE: Set the rules before you check if $_POST is set so that the jQuery validation will work.
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("pos_id", "Post", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("pol_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$post_like = $this->extract->post();

			// Call run method from Form_validation to check
			if($this->form_validation->run() !== false)
			{
				$fields = $this->form_validation->get_fields();
				$result = $this->post_like_model->create($post_like, $fields);
				$id = $result['result']['insert_id'];

				// Set a notification using notification method from Template.
				// It is okay to redirect after and the notification will be displayed on the redirect page.
				$this->template->notification('New post like created.', 'success');
				redirect("admin/post_likes/edit/$id");
			}
			else
			{
				// To display validation errors caught by the Form_validation, you should have the code below.
				$this->template->notification(validation_errors(), 'danger');
			}

			$this->template->autofill($post_like);
		}

		$page = array();

		$page["usr_ids"] = $this->user_model->get_all();		$page["pos_ids"] = $this->post_model->get_all();

		$this->template->content('post_likes-menu', null, null, 'page-nav');
		$this->template->content('post_likes-create', $page);
		$this->template->show();
	}

	public function edit($pol_id)
	{
		$this->template->title('Edit Post Like');

		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("pos_id", "Post", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("pol_status", "Status", "trim|required");

		if($this->input->post('form_submit'))
		{
			$post_like = $this->extract->post();
			if($this->form_validation->run() !== false)
			{
				$post_like['pol_id'] = $pol_id;
				$fields = $this->form_validation->get_fields();

				$rows_affected = $this->post_like_model->update($post_like, $fields);

				$this->template->notification('Post like updated.', 'success');
				redirect("admin/post_likes/edit/$pol_id");
			}
			else
			{
				$this->template->notification(validation_errors());
			}
			$this->template->autofill($post_like);
		}

		$page = array();
		$page['post_like'] = $this->post_like_model->get_one($pol_id);

		if($page['post_like'] === false)
		{
			$this->template->notification('Post like was not found.', 'danger');
			redirect('admin/post_likes');
		}

		$page["usr_ids"] = $this->user_model->get_all();		$page["pos_ids"] = $this->post_model->get_all();
		$this->template->content('post_likes-menu', null, null, 'page-nav');
		$this->template->content('post_likes-edit', $page);
		$this->template->show();
	}

	public function view($pol_id)
	{
		$this->template->title('View Post Like');

		$page = array();
		$page['post_like'] = $this->post_like_model->get_one($pol_id);

		if($page['post_like'] === false)
		{
			$this->template->notification('Post like was not found.', 'danger');
			redirect('admin/post_likes');
		}
		
		$page["usr_ids"] = $this->user_model->get_all();		$page["pos_ids"] = $this->post_model->get_all();

		$this->template->content('post_likes-menu', null, null, 'page-nav');
		$this->template->content('post_likes-view', $page);
		$this->template->show();
	}


}
