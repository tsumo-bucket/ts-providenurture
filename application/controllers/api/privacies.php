<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class Privacies extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('privacy_api_model');
	}

	public function view_get()
	{
		$request = $this->extract->get();

		$data = $this->api_access_control->validate();

		$response = $this->privacy_api_model->get_one($request['pri_id']);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function list_get()
	{
		$request = $this->get();
  	$request['privacy.usr_id'] = $this->api_access_control->validate();
		$response = $this->privacy_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function update_post()
	{
		$request = $this->post();

		$request['usr_id'] = $this->api_access_control->validate();
		
		$response = $this->privacy_api_model->update($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}
}
