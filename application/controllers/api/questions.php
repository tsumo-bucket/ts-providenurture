<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Questions extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('question_api_model');

	}
}
