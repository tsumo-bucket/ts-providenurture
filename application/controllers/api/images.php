<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class Images extends Mythos_REST_Controller
{

	public $production;
	public $path;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('image_api_model');
		$this->load->model('image_model');
		$this->load->model('user_api_model');
		$this->mythos->library('upload');
    	$this->mythos->library('image');
		$this->load->library('S3_upload');
		
		$this->production = true;
		if($this->production) {
			$this->path = 'pn-live/';
		}
		else {
			$this->path = 'pn-beta/';
		}
	}

	public function view_get()
	{
		$request = $this->get();
		$response = $this->image_api_model->get_one($request['img_id']);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function list_get()
	{
		$request = $this->get();
		$request['usr_id'] = $this->api_access_control->validate();
		$response = $this->image_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function upload_post()
	{
		$request = $this->post();
		$params = array();
		$params['img_source'] = $request['img_source'];
		$params['img_image'] = $request['img_image'];
		$params['img_status'] = $request['img_status'];
		$params['usr_id'] = $this->api_access_control->validate();
        $image_name = '';
        $rand = '';
        $rand = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 24)),0, 24);
		if($request['img_source'] == 'facebook') {
	        $image_name = $rand . $request['index'] . '.jpg';
			$jpeg_data = file_get_contents($params['img_image']);
			// file_put_contents('uploads/images/' . $image_name, $jpeg_data);
	        $image_thumb_name = $rand .'_thumb' .'.jpg';
	        $thumb_data = file_get_contents($request['img_thumb']);
			// file_put_contents('uploads/images/' . $image_thumb_name, $thumb_data);
	        $params['img_image'] = 'uploads/images/' . $image_name;
	        $params['img_thumb'] = 'uploads/images/' .$image_thumb_name;
	        $this->image->_uploadJPG($image_name, 'uploads/images/', $jpeg_data, true);
	        $this->image->_uploadJPG($image_thumb_name, 'uploads/images/', $thumb_data, true);
		   
		}
		else {
			if(isset($request['img_image']) && $request['img_image'] != ''){
	      		$explode = explode('data:image/', $request['img_image']);
		      	if(isset($explode['0']) && $explode['0'] == ''){
			        $comma = strpos($request['img_image'], ',');
			        $decoded = base64_decode(substr($params['img_image'], $comma+1));
			        $image_name = $rand . '.jpg';
			        $image_thumb_name = $rand .'_thumb' .'.jpg';
			        // file_put_contents('uploads/images/' . $image_name, $decoded);
				   	$thumb_data = file_get_contents($request['img_thumb']);
					// file_put_contents('uploads/images/' . $image_thumb_name, $thumb_data);
			        $params['img_image'] = 'uploads/images/' . $image_name;
			        $params['img_thumb'] = 'uploads/images/' .$image_thumb_name;
			        $this->image->_uploadJPG($image_name, 'uploads/images/', $decoded, true);
			        $this->image->_uploadJPG($image_thumb_name, 'uploads/images/', $thumb_data, true);
		      	}
	   		}
		}
		$response = $this->image_api_model->upload($params);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function delete_post()
	{
		$request = $this->post();

		$request['usr_id'] = $this->api_access_control->validate();

		$response = $this->image_api_model->upload($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
	        $image = $this->image_model->get_one($request['img_id']);
	       
	        if($image->img_image) {
		        $img = $this->path . $image->img_image;
		        $this->s3_upload->delete($img);
	        }
	        if($image->img_thumb) {
		        $thumb = $this->path . $image->img_thumb;
		        $this->s3_upload->delete($thumb);
	        }
	        
			$params['usr_id'] = $response['data'][0]->usr_id;
			$params['img_status'] = 'active';
			$response = $this->image_api_model->get_all($params);
			$this->response($response, 200);
		}
	}

	public function privacy_post()
	{
		$request = $this->post();

		$request['usr_id'] = $this->api_access_control->validate();

		$response = $this->image_api_model->upload($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function crop_post()
	{
		$request = $this->post();

		$request['usr_id'] = $this->api_access_control->validate();
		if(isset($request['img_image']) && $request['img_image'] != ''){
      		$explode = explode('data:image/', $request['img_image']);
	      	if(isset($explode['0']) && $explode['0'] == ''){
        		$rand = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 24)),0, 24);
		        $comma = strpos($request['img_image'], ',');
		        $decoded = base64_decode(substr($request['img_image'], $comma+1));
		        $image = $this->image_model->get_one($request['img_id']);
		        if($image->img_image) {
			        $img = $this->path . $image->img_image;
			        $this->s3_upload->delete($img);
		        }
		        if($image->img_thumb) {
			        $thumb = $this->path . $image->img_thumb;
			        $this->s3_upload->delete($thumb);
		        }
		        
		        $image_name = $rand . '.jpg';
		        $image_thumb_name = $rand . '_thumb.jpg';
		        // file_put_contents('uploads/images/' . $image_name, $decoded);
		        $thumb_data = file_get_contents($request['img_thumb']);
				// file_put_contents('uploads/images/' . $image_thumb_name, $thumb_data);
		        $request['img_image'] = 'uploads/images/' . $image_name;
		        $request['img_thumb'] = 'uploads/images/' .$image_thumb_name;
		        $this->image->_uploadJPG($image_name, 'uploads/images/', $decoded, true);
		        $this->image->_uploadJPG($image_thumb_name, 'uploads/images/', $thumb_data, true);

	      	}
   		}
		$response = $this->image_api_model->upload($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$params['usr_id'] = $request['usr_id'];
			$params['img_status'] = 'active';
			$response = $this->image_api_model->get_all($params);
			$this->response($response, 200);
		}
	}
	public function blur_post()
	{
		$request = $this->post();

		$request['usr_id'] = $this->api_access_control->validate();
		if(isset($request['img_image']) && $request['img_image'] != ''){
      		$explode = explode('data:image/', $request['img_image']);
	      	if(isset($explode['0']) && $explode['0'] == ''){
        		$rand = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 24)),0, 24);
		        $comma = strpos($request['img_image'], ',');
		        $decoded = base64_decode(substr($request['img_image'], $comma+1));
		        $image = $this->image_model->get_one($request['img_id']);
		        if($image->img_image) {
			        $img = $this->path . $image->img_image;
			        $this->s3_upload->delete($img);
		        }
		        if($image->img_thumb) {
			        $thumb = $this->path . $image->img_thumb;
			        $this->s3_upload->delete($thumb);
		        }
		        
		        
		        $image_name = $rand . '.jpg';
		        $image_thumb_name = $rand . '_thumb.jpg';
		        // file_put_contents('uploads/images/' . $image_name, $decoded);
		        $thumb_data = file_get_contents($request['img_thumb']);
				// file_put_contents('uploads/images/' . $image_thumb_name, $thumb_data);
		        $request['img_image'] = 'uploads/images/' . $image_name;
		        $request['img_thumb'] = 'uploads/images/' .$image_thumb_name;
		 		$this->image->_uploadJPG($image_name, 'uploads/images/', $decoded, true);
		        $this->image->_uploadJPG($image_thumb_name, 'uploads/images/', $thumb_data, true);
			 
	      	}
   		}
		$response = $this->image_api_model->upload($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$params['usr_id'] = $request['usr_id'];
			$params['img_status'] = 'active';
			$response = $this->image_api_model->get_all($params);
			$this->response($response, 200);
		}
	}

	public function makeProfile_post() {
		$request = $this->post();

		$request['usr_id'] = $this->api_access_control->validate();
		$this->user_api_model->make_profile($request);
		$response = $this->image_api_model->get_one($request['img_id']);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}
}
