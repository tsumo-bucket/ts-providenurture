<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class Blocks extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('block_api_model');

	}

	public function list_get()
	{
		$request = $this->extract->get();

		$data = $this->api_access_control->validate();

		$response = $this->block_api_model->get_all($data);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			foreach($response['data']['0'] as $block){
        $block->additional_data = $this->user_model->additional_block($block->blo_user); 
      }

			$this->response($response, 200);
		}
	}
}
