<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class Searches extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('search_api_model');
		$this->load->model('user_api_model');
		$this->load->model('user_like_model');
	}

	public function save_post()
	{
		$request = $this->post();
		$request['usr_id'] = $this->api_access_control->validate();
		$response = $this->search_api_model->save($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function update_post()
	{
		$request = $this->post();
		$request['usr_id'] = $this->api_access_control->validate();
		$response = $this->search_api_model->update($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function list_get()
	{
		$request = $this->get();
		$request['usr_id'] = $this->api_access_control->validate();
		$response = $this->search_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}
	
	public function search_get()
	{
		$request = $this->get();
		// $lat = deg2rad($request['lat']);
		// $long = deg2rad($request['long']);
		// unset($request['lat']);
		// unset($request['long']);
		$request['usr_id'] = $this->api_access_control->validate();

		$params = array();
		if (isset($request['order_by']) && isset($request['order'])){
      $order_by = array($request['order_by']=>$request['order']);
      unset($request['order']);
      unset($request['order_by']);
    }
    else {
    	$order_by = array();
    }
		if(isset($request['is_infinite']) && isset($request['page']) && isset($request['page_size'])){
			$params['is_infinite'] = $request['is_infinite'];
			$params['page'] = $request['page'];
			$params['page_size'] = $request['page_size'];

			unset($request['is_inifinite']);
			unset($request['page']);
			unset($request['page_size']);
		}

		$response = $this->user_api_model->search($request, $params, $order_by);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			foreach($response['data']['0'] as $user){
        $from = new DateTime($user->usr_birthyear.'-'.$user->usr_birthmonth.'-'.$user->usr_birthdate);
        $to = new DateTime('today');

        unset($user->usr_birthyear);
        unset($user->usr_birthmonth);
        unset($user->usr_birthdate);

        $user->usr_age = $from->diff($to)->y;
        $user->additional_data = $this->user_model->additional($user->usr_id, false); 
        $user->faved = $this->user_model->faved($user->usr_id, $request['usr_id']);
        $user->matched = $this->user_like_model->is_matched($user->usr_id, $request['usr_id']);
      //   foreach($result['data']['0'] as $user){
	  			// if($user->ulc_lat && $user->ulc_lng) {
	     //    	$lat2 = deg2rad($user->ulc_lat);
	     //    	$long2 = deg2rad($user->ulc_lng);
	     //    	$eq1 = acos(sin($lat) * sin($lat2));
	     //    	$eq2 = cos($lat);
	     //    	$eq3 = cos($lat2);
	     //    	$eq4 = cos($long -$long2);
	     //    	$user->distance = acos(sin($lat) * sin($lat2) + cos($lat) * cos($lat2) * cos($long - $long2)) * 6371;
	     //    }
	     //    else {
	     //    	$user->distance = null;
	     //    }
        
      }

			$this->response($response, 200);
		}
	}
}
