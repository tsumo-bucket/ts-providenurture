<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class Messages extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('message_api_model');
		$this->load->model('conversation_api_model');
    $this->load->model('user_api_model');
		$this->load->model('user_model');
		$this->load->model('post_model');
		$this->load->library('socket');
	}

	public function send_post()
	{
		$request = $this->post();

		$request['usr_id'] = $this->api_access_control->validate();

		$response = $this->message_api_model->send($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function view_get()
	{
		$request = $this->input->get();

		$request['usr_id'] = $this->api_access_control->validate();

		$response = $this->conversation_api_model->view($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			foreach($response['data']['0'] as $message){
				if(isset($message->pos_id)){
					$message->post = $this->post_model->message_post($message->pos_id);
				}
				unset($message->pos_id);
			}

			$this->response($response, 200);
		}
	}

	public function read_post()
	{
		$request = $this->post();

		$request['usr_id'] = $this->api_access_control->validate();

		$response = $this->message_api_model->read($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function update_post()
	{
		$request = $this->post();

		$request['usr_id'] = $this->api_access_control->validate();

		$response = $this->conversation_api_model->update($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function list_get()
	{
		$request = $this->extract->get();

		$request['usr_id'] = $this->api_access_control->validate();
		
		$response = $this->conversation_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			foreach($response['data']['0'] as $message){
        $message->additional_data = $this->user_model->additional_fave($message->usr_id); 
      }

			$this->response($response, 200);
		}
	}

	public function unread_get()
	{
		$request = $this->extract->get();

		$data = $this->api_access_control->validate();
		
		$response = $this->conversation_api_model->unread($data);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}
}
