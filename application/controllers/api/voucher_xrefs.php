<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Voucher_xrefs extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('voucher_xref_api_model');

	}
}
