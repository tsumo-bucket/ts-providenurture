<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Subscriptions extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('subscription_api_model');

	}

	public function view_get()
	{
		$request = $this->input->get();
		$response = $this->subscription_api_model->get_one($request['sub_id']);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function list_get()
	{
		$request = $this->extract->get();
		$response = $this->subscription_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function create_post()
	{
		$request = $this->extract->post();
		$response = $this->subscription_api_model->create($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function update_post()
	{
		$request = $this->extract->post();
		$response = $this->subscription_api_model->update($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function delete_post()
	{
		$request = $this->extract->post();
		$response = $this->subscription_api_model->delete($request['sub_id']);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}
}
