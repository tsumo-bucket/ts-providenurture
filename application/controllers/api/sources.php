<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Sources extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('source_api_model');

	}

	public function view_get()
	{
		$request = $this->input->get();
		$response = $this->source_api_model->get_one($request['src_id']);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function list_get()
	{
		$request = $this->extract->get();
		$response = $this->source_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}
}
