<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class Views extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('view_api_model');
		$this->load->model('user_like_model');
	}

	public function list_get()
	{
		$request = $this->get();

		$data = $this->api_access_control->validate();
		$request['usr_id'] = $data;
		
		$response = $this->view_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			foreach($response['data']['0'] as $view){
				$view->additional_data = $this->user_model->additional_fave($view->usr_id); 
      			$view->faved = $this->user_model->faved($view->usr_id, $data);
        		$view->matched = $this->user_like_model->is_matched($view->usr_id, $request['usr_id']);
      		}		

			$this->response($response, 200);
		}
	}
}
