<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class Flags extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('flag_api_model');
	}

	public function report_post()
	{
		$request = $this->post();

		$request['usr_id'] = $this->api_access_control->validate();
		
		$response = $this->flag_api_model->report($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}
}
