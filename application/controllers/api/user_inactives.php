<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class User_inactives extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_inactive_api_model');

	}
}
