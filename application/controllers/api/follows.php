<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class Follows extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('follow_api_model');
		$this->load->model('user_like_model');

	}

	public function list_get()
	{
		$request = $this->extract->get();

		$request['usr_id'] = $this->api_access_control->validate();

		$response = $this->follow_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			foreach($response['data']['0'] as $follow){
		        $follow->additional_data = $this->user_model->additional_fave($follow->fol_user); 
		        $follow->usr_id = $follow->fol_user;
		        $follow->faved = $this->user_model->faved($follow->fol_user, $request['usr_id']);
        		$follow->matched = $this->user_like_model->is_matched($follow->usr_id, $request['usr_id']);
	      	}
			$this->response($response, 200);
		}
	}
}
