<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class User_reports extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_report_api_model');

	}

	public function view_get()
	{
		$request = $this->input->get();
		$response = $this->user_report_api_model->get_one($request['rep_id']);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function list_get()
	{
		$request = $this->extract->get();
		$response = $this->user_report_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function create_post()
	{
		$request = $this->extract->post();
		$response = $this->user_report_api_model->create($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function update_post()
	{
		$request = $this->extract->post();
		$response = $this->user_report_api_model->update($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function delete_post()
	{
		$request = $this->extract->post();
		$response = $this->user_report_api_model->delete($request['rep_id']);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}
}
