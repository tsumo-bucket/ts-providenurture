<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class Posts extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('post_api_model');
		$this->load->model('post_like_api_model');
		$this->load->model('user_api_model');
	}

	public function list_get()
	{
		$request = $this->get();
		$request['usr_id'] = $this->api_access_control->validate();
		$response = $this->post_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			foreach($response['data']['0'] as $post){
				$post->user = $this->user_api_model->get_one_simplified($post->usr_id, $request['usr_id']);
				$post->flag = $this->post_api_model->flagged($post->pos_id, $request['usr_id']);
				$post->like = $this->post_api_model->liked($post->pos_id, $request['usr_id']);

				unset($post->usr_id);
			}

			$this->response($response, 200);
		}
	}

	public function post_post()
	{
		$request = $this->post();

		$request['usr_id'] = $this->api_access_control->validate();

		$response = $this->post_api_model->post($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$response['data']['0']['user'] = $this->user_api_model->get_one_simplified($response['data']['0']['usr_id'], $response['data']['0']['usr_id']);
			$this->response($response, 200);
		}
	}

	public function like_post()
	{
		$request = $this->post();

		$request['usr_id'] = $this->api_access_control->validate();

		$response = $this->post_like_api_model->update($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}
}
