<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class Preferences extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('preference_api_model');
		$this->load->model('user_api_model');
	}

	public function view_get()
	{
		$request = $this->input->get();
		$response = $this->preference_api_model->get_one($request['prf_id']);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function list_get()
	{
		$request = $this->get();
		$request['preference.usr_id'] = $this->api_access_control->validate();
		$response = $this->preference_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	function edit_post()
	{
		$request = $this->post();

	    $request['usr_id'] = $this->api_access_control->validate();

	    $response = $this->preference_api_model->edit($request);
	    if(isset($response['error']))
	    {
	      $this->response($response, 200);
	    }
	    else
	    {
	      $this->response($response, 200);
	    }
	}

	function create_post()
	{
		$request = $this->post();

	    $request['usr_id'] = $this->api_access_control->validate();

	    $response = $this->preference_api_model->create($request);
	    if(isset($response['error']))
	    {
	      $this->response($response, 200);
	    }
	    else
	    {
	      $this->response($response, 200);
	    }
	}
}
