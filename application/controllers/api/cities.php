<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class Cities extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('city_api_model');

	}

	public function view_get()
	{
		$request = $this->input->get();
		$response = $this->city_api_model->get_one($request['cty_id']);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}

	public function list_get()
	{
		$request = $this->get();
		$params['region.reg_id'] = $request['reg_id'];
		$response = $this->city_api_model->get_all($params);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}
}
