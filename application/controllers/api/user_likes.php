<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class User_likes extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_like_api_model');
		$this->load->model('user_like_model');
	}

	public function list_get()
	{
		$request = $this->get();

		$request['usr_id'] = $this->api_access_control->validate();

		$response = $this->user_like_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			foreach($response['data']['0'] as $usl){
				if(isset($request['source']) && $request['source'] == 'i-faved'){
		        	$usl->additional_data = $this->user_model->additional_fave($usl->usl_user); 
		        	$usl->faved = $this->user_model->faved($usl->usl_user, $request['usr_id']);
		        	$usl->matched = $this->user_like_model->is_matched($usl->usr_id, $request['usr_id']);
						}elseif(isset($request['source']) && $request['source'] == 'faved-me'){
		        	$usl->additional_data = $this->user_model->additional_fave($usl->usr_id); 
		        	$usl->faved = $this->user_model->faved($usl->usr_id, $request['usr_id']);
		        	$usl->matched = $this->user_like_model->is_matched($usl->usr_id, $request['usr_id']);
				}
      }

			$this->response($response, 200);
		}
	}
}
