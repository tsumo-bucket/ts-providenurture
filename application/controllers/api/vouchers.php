<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Vouchers extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('voucher_api_model');

	}
}
