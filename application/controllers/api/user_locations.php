<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class User_locations extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('user_location_api_model');

	}

	public function update_post()
	{
		$request = $this->post();
		$request['usr_id'] = $this->api_access_control->validate();
		$response = $this->user_location_api_model->update($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}
}
