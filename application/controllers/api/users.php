<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class Users extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
    $this->load->model('user_model');
    $this->load->model('user_api_model');
    $this->load->model('user_like_api_model');
    $this->load->model('user_like_model');
    $this->load->model('user_report_api_model');
    $this->load->model('user_accounts_api_model');
    $this->load->model('block_api_model');
		$this->load->model('follow_api_model');

    $this->mythos->library('email');
	}

	function initial_post()
  {
    $request = $this->post();
    $source = $request['usr_signup_source'];
    $response = $this->user_api_model->initial($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      if($source != 'facebook' || $source != 'google') {
        $params = array();
        $params['mail_to'] = $request['usr_email'];
        $params['subject'] = 'ProvideNurture Registration';
        $params['cc'] = '';
        $params['bcc'] = '';

        $content = array();
        $content['user'] = $this->user_model->get_by_email($request['usr_email']);

        $params['content'] = $this->load->view('email/initial', $content, true);
        $success = $this->email->send_mail($params,'email', 'template');
      }
      
      $this->response($response, 200);
    }
  }

  function email_post() {
    $request = $this->post();

    $response = $this->user_api_model->email($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }


  function verify_post()
  {
  	$request = $this->post();

  	$response = $this->user_api_model->verify($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function resend_post()
  {
  	$request = $this->post();
  	$response = $this->user_api_model->resend($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      if(isset($response['data']['0']['usr_email'])){
      	$params = array();
	      $params['mail_to'] = $request['usr_email'];
	      $params['subject'] = 'ProvideNurture Registration';
	      $params['cc'] = '';
	      $params['bcc'] = '';

	      $content = array();
	      $content['user'] = $this->user_model->get_by_email($request['usr_email']);

	      $params['content'] = $this->load->view('email/initial', $content, true);
	      $success = $this->email->send_mail($params,'email', 'template');
      }

      $this->response($response, 200);
    }
  }

  function login_post()
  {
  	$email = $this->post('usr_email');
    $password = $this->post('usr_password');

    if(!$email || !$password)
    {
      $this->response(array('error' => 'Email and password are required'), 200);
    }

    $user = $this->user_model->authenticate($email, $password);

    if($user){
      $message = $user->_message;
      $redirect = $user->_redirect;
      $continue = $user->_continue;

      //FOR CHAT
      if(isset($user->usr_password)) {
        $socket_key = $user->usr_password;
        $user->socket_key = random_string('alnum',1).strrev($socket_key).random_string('alnum',1);
        unset($user->_message);
        unset($user->_redirect);
        unset($user->_continue);
      }

      if($continue){
       $this->response(array('success'=> true, 'data' => $user, 'message' => $message, 'redirect' => $redirect), 200); 
      }else{
    	 $this->response(array('error' => $message), 200); 
      }
    }else{
    	$this->response(array('error' => 'Invalid user credentials.'), 200);
    }
  }

  function get_token_get() {
    $request = $this->get();
    $params = $request['usr_email'];
    $response = $this->user_api_model->get_token($params);
    $this->response($response, 200);
  }

  function forgot_post()
  {
  	$request = $this->post();
    
  	$response = $this->user_api_model->forgot($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      if(isset($response['data']['0']['usr_email'])){
      	$params = array();
	      $params['mail_to'] = $request['usr_email'];
	      $params['subject'] = 'Forgot Password';
	      $params['cc'] = '';
	      $params['bcc'] = '';

	      $content = array();
	      $content['user'] = $this->user_model->get_by_email($request['usr_email']);

	      $params['content'] = $this->load->view('email/forgot_password', $content, true);
	      $success = $this->email->send_mail($params,'email', 'template');
      }

      $this->response($response, 200);
    }
  }

  function reset_post()
  {
  	$request = $this->post();

  	$response = $this->user_api_model->reset($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function logout_post()
  {
  	$data = $this->api_access_control->validate();
  
    $response = $this->user_api_model->generate_token($data);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function deactivate_post()
  {
 		$request = $this->post();
  	
  	$request['usr_id'] = $this->api_access_control->validate();
  
    $response = $this->user_api_model->deactivate($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function delete_post()
  {
  	$request = $this->post();
  	
  	$request['usr_id'] = $this->api_access_control->validate();
  
    $response = $this->user_api_model->delete($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function profile_get()
  {
  	$request = $this->get();
    $viewer = $this->api_access_control->validate();
    $request['usr_id'] = $viewer;
    $response = $this->user_api_model->profile($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $usr_id = $response['data']['0']->usr_id;
      $from = new DateTime($response['data']['0']->usr_birthyear.'-'.$response['data']['0']->usr_birthmonth.'-'.$response['data']['0']->usr_birthdate);
      $to = new DateTime('today');

      $response['data']['0']->usr_age = $from->diff($to)->y;

      if(isset($request['usr_token_id'])){
        $response = $this->user_api_model->additional($response, $usr_id, $viewer);
      }else{
        $response = $this->user_api_model->additional($response, $usr_id);
      }

      $this->response($response, 200);
    }
  }

  function simple_profile_get()
  {
    $request = $this->get();
    $viewer = $this->api_access_control->validate();
    $response = $this->user_api_model->get_one($viewer);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function edit_post()
  {
    $request = $this->post();

    $request['usr_id'] = $this->api_access_control->validate();
    $response = $this->user_api_model->edit($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $response = $this->user_api_model->additional($response, $request['usr_id']);

      $this->response($response, 200);
    }
  }

  function edit_description_post()
  {
    $request = $this->post();

    $request['usr_id'] = $this->api_access_control->validate();

    $response = $this->user_api_model->edit_description($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $response = $this->user_api_model->additional($response, $request['usr_id']);

      $this->response($response, 200);
    }
  }

  function preference_get()
  {
    $request = $this->post();
    $data = $this->api_access_control->validate();
    $response = $this->user_api_model->preference($data);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function update_preference_post()
  {
    $request = $this->post();
    $request['usr_id'] = $this->api_access_control->validate();
    $response = $this->user_api_model->update_preference($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function account_post()
  {
  	$request = $this->post();

  	$request['usr_id'] = $this->api_access_control->validate();

  	$response = $this->user_api_model->account($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function password_post()
  {
  	$request = $this->post();

  	$request['usr_id'] = $this->api_access_control->validate();

  	$response = $this->user_api_model->password($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function facebook_post()
  {
  	$request = $this->post();

  	$request['usr_id'] = $this->api_access_control->validate();

  	$response = $this->user_api_model->facebook($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

	function list_get()
	{
		$request = $this->get();
    $data = $this->api_access_control->validate();
    $request['usr_status'] = 'approved';
    $request['usr_id'] = $data;

    $order_by = array();

    if (isset($request['order_by']) && isset($request['order'])){
      $order_by = array($request['order_by']=>$request['order']);
      unset($request['order']);
      unset($request['order_by']);
    }

		$response = $this->user_api_model->get_all($request, $order_by);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
      foreach($response['data']['0'] as $user){
        $from = new DateTime($user->usr_birthyear.'-'.$user->usr_birthmonth.'-'.$user->usr_birthdate);
        $to = new DateTime('today');

        unset($user->usr_birthyear);
        unset($user->usr_birthmonth);
        unset($user->usr_birthdate);

        $user->usr_age = $from->diff($to)->y;

        $user->additional_data = $this->user_model->additional($user->usr_id); 
        $user->faved = $this->user_model->faved($user->usr_id, $data);
        $user->followed = $this->user_model->followed($user->usr_id, $data);
        $user->matched = $this->user_like_model->is_matched($user->usr_id, $data);
      }

      $this->response($response, 200);
		}
	}

  function sugar_get()
  {
    $request = $this->get();

    $request['usr_status'] = 'approved';
    $request['usr_type'] = 'sugar';

    $order_by = array();

    $this->db->limit(10);
    $response = $this->user_api_model->normal_get_all($request, $order_by);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function sugar_man_get()
  {
    $request = $this->get();

    $request['usr_status'] = 'approved';
    $request['usr_type'] = 'sugar';
    $request['usr_gender'] = 'male';

    $order_by = array();

    $this->db->limit(10);
    $response = $this->user_api_model->normal_get_all($request, array('user.usr_id' => 'asc'));
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function sugar_woman_get()
  {
    $request = $this->get();

    $request['usr_status'] = 'approved';
    $request['usr_type'] = 'sugar';
    $request['usr_gender'] = 'female';

    $order_by = array();

    $this->db->limit(10);
    $response = $this->user_api_model->normal_get_all($request,  array('user.usr_id' => 'asc'));
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function flame_man_get()
  {
    $request = $this->get();

    $request['usr_status'] = 'approved';
    $request['usr_type'] = 'flame';
    $request['usr_gender'] = 'male';

    $order_by = array();

    $this->db->limit(12);
    $response = $this->user_api_model->normal_get_all($request, array('user.usr_id' => 'asc'));
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function flame_woman_get()
  {
    $request = $this->get();

    $request['usr_status'] = 'approved';
    $request['usr_type'] = 'flame';
    $request['usr_gender'] = 'female';

    $order_by = array();

    $this->db->limit(12);
    $response = $this->user_api_model->normal_get_all($request, array('user.usr_id' => 'asc'));
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

  function signup_step_get()
  {
    $request = $this->get();
    $usr_id = $this->api_access_control->validate();
    update_signup_status($usr_id);
    $response = $this->user_api_model->get_one($usr_id);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
   
  }

	function favorite_post()
	{
		$request = $this->post();

  	$request['usr_id'] = $this->api_access_control->validate();

  	$response = $this->user_like_api_model->favorite($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
	}

  function report_post()
  {
    $request = $this->post();
    $request['usr_id'] = $this->api_access_control->validate();
    
    $response = $this->user_report_api_model->create($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
  }

	function block_post()
	{
    $request = $this->post();

    $request['usr_id'] = $this->api_access_control->validate();

    $response = $this->block_api_model->block($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
	}

	function follow_post()
	{
    $request = $this->post();

    $request['usr_id'] = $this->api_access_control->validate();

    $response = $this->follow_api_model->follow($request);
    if(isset($response['error']))
    {
      $this->response($response, 200);
    }
    else
    {
      $this->response($response, 200);
    }
	}
}
