<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Conversations extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('conversation_api_model');

	}
}
