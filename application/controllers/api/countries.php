<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/Mythos_REST_Controller.php';

class Countries extends Mythos_REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('country_api_model');

	}

	public function list_get()
	{
		$request = $this->extract->get();
		$response = $this->country_api_model->get_all($request);
		if(isset($response['error']))
		{
			$this->response($response, 200);
		}
		else
		{
			$this->response($response, 200);
		}
	}
}
