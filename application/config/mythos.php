<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*** Base Configurations ***/

// Default timezone. If set to NULL, Mythos will not set a timezone.
$config['timezone'] = 'UTC';



/*** Autoload Mythos Files ***/

$config['autoload']['libraries'] = array('template', 'access_control', 'form_validation', 'extract', 'pagination');
$config['autoload']['helper'] = array('format', 'datetime', 'template');
$config['autoload']['config'] = array();
$config['autoload']['language'] = array();
$config['autoload']['model'] = array('base_model', 'api_model', 'base_auditable_model');



/*** More helper ***/

$config['more_helper']['max_char_count'] = 100;
$config['more_helper']['max_word_count'] = 100;
$config['more_helper']['ellipsis'] = '...';



/*** Format helper ***/

$config['format_helper']['date'] = 'D, j M Y';
$config['format_helper']['time'] = 'h:i A';
$config['format_helper']['datetime'] = 'D, j M Y h:i A';
$config['format_helper']['html_slug_length'] = 70;



/*** Access_control library ***/
// Para sa admin side
// Index of the session that Mythos will check to determine if the user is logged in.
$config['access_control']['logged_in_index'] = 'acc_username';
// Index of the session that Mythos will check to get the user's account type.
$config['access_control']['account_type_index'] = 'acc_type';

// Para sa front end -> same as above kung account table gamit mo for login, change if user table etc.
// Index of the session that Mythos will check to determine if the user is logged in.
$config['access_control']['fe_logged_in_index'] = 'usr_username';
// Index of the session that Mythos will check to get the user's account type/status depende sa use mo.
$config['access_control']['fe_account_type_index'] = 'usr_status';



/*** Pagination library ***/

// Refer to the Pagination library's documentation (http://codeigniter.com/user_guide/libraries/pagination.html)
$config['pagination']['per_page'] = 20;
$config['pagination']['num_links'] = 4;
$config['pagination']['first_link'] = '&larr; First';
$config['pagination']['prev_link'] = '&larr; Back';
$config['pagination']['next_link'] = '&rarr; Next';
$config['pagination']['last_link'] = '&rarr; Last';
$config['pagination']['full_tag_open'] = '<div class="text-center"><ul class="pagination">';
$config['pagination']['full_tag_close'] = '</ul></div>';
$config['pagination']['first_tag_open'] = '<li class="prev">';
$config['pagination']['first_tag_close'] = '</li>';
$config['pagination']['prev_tag_open'] = '<li class="prev">';
$config['pagination']['prev_tag_close'] = '</li>';
$config['pagination']['next_tag_open'] = '<li class="next">';
$config['pagination']['next_tag_close'] = '</li>';
$config['pagination']['last_tag_open'] = '<li class="next">';
$config['pagination']['last_tag_close'] = '</li>';
$config['pagination']['cur_tag_open'] = '<li class="active"><a href="#">';
$config['pagination']['cur_tag_close'] = '</a></li>';
$config['pagination']['num_tag_open'] = '<li>';
$config['pagination']['num_tag_close'] = '</li>';


/*** Facebook library ***/

$config['facebook']['app_id'] = '577570172325320';
$config['facebook']['app_secret'] = 'bf9187a7bef1811c5d8003c2e4207eb7';
$config['facebook']['file_upload'] = false;
$config['facebook']['verify_peer_certificate'] = true;


/*** Youtube Library ***/
$config['youtube']['google_client_id'] = '985709565679.apps.googleusercontent.com';
$config['youtube']['google_client_secret'] = 'icYRpeyvFsd7pL2DsWZIW8NW';
$config['youtube']['youtube_api_key'] = 'AI39si5-MP14zzTSPSHKqzuqV68b_YWRj1oi9B24jDDD1qX_oIHPpmFS9gSfwTPUAqXNCg6K1KgFhY8k1tNUvjV5vYbJC8j2tw';

/*** Email Library ***/
$config['email']['useragent'] = 'CodeIgniter';
$config['email']['protocol'] = 'smtp';
$config['email']['mailpath'] = '/usr/sbin/sendmail';


$config['email']['smtp_host'] = 'mail.thinksumocreative.com';
$config['email']['smtp_user'] = 'tester@thinksumocreative.com';
$config['email']['smtp_pass'] = 'dVz]PG+3?5Ki';
$config['email']['smtp_port'] = 26;

$config['email']['smtp_timeout'] = 5;
// $config['email']['smtp_crypto'] = 'tls';

$config['email']['wordwrap'] = false;
$config['email']['wrapchars'] = 76;
$config['email']['mailtype'] = 'html';
$config['email']['charset'] = 'utf-8';

$config['email']['validate'] = false;
$config['email']['priority'] = 3;

$config['email']['crlf'] = "\r\n";
$config['email']['newline'] = "\r\n";

$config['email']['bcc_batch_mode'] = false;
$config['email']['bcc_batch_size'] = 200;

$config['email']['from_email'] = "no-reply@providenurture.com";
$config['email']['from_email_name'] = "ProvideNurture";

// Set to preview to return email output
// Set to simulation to simulate sending without actually sending the email
$config['email']['debug'] = false;

//saves the email on the database
$config['email']['store_email']= true;
