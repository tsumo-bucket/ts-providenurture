<?php
class Notification_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'not_id', 
			'usr_id', 
			'not_message', 
			'not_like', 
			'not_profile', 
			'not_content', 
			'not_email', 
			'not_date_created', 
			'not_date_modified'
		);

		parent::__construct('notification', $fields);
	}

	public function update($data)
	{
		$this->set_message("Update Notification");

		$this->form_validation->set_rules('not_id', 'Notification Id', 'integer|required');
		$this->form_validation->set_rules("usr_id", "User", "trim");
		$this->form_validation->set_rules("not_message", "Message", "trim|required");
		$this->form_validation->set_rules("not_like", "Like", "trim|required");
		$this->form_validation->set_rules("not_profile", "Profile", "trim|required");
		$this->form_validation->set_rules("not_content", "Content", "trim|required");
		$this->form_validation->set_rules("not_email", "Email", "trim|required");

		$error = $this->run_validators();

		if(!is_bool($error)){
      $this->set_error($error, false);
    }else{
    	$this->db->where('notification.not_id', $data['not_id']);
    	$this->db->where('notification.usr_id', $data['usr_id']);
    	$notifications = $this->db->get('notification');

    	if($notifications->num_rows() > 0){
    		$notification = $notifications->row();

    		$notification->not_message = $data['not_message'];
    		$notification->not_like = $data['not_like'];
    		$notification->not_profile = $data['not_profile'];
    		$notification->not_content = $data['not_content'];
    		$notification->not_email = $data['not_email'];
    		$notification->not_date_modified = format_mysql_datetime();
    		
    		$this->db->where('not_id', $notification->not_id);
    		$this->db->update('notification', $notification);

				$this->set_data($notification, true);
			}else{
				$this->set_error(array('message'=>'Notification not found.'));
			}
    }
    return $this->compose_result();
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Notification");
		$this->db->join("user", "user.usr_id = notification.usr_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Notifications");
		$this->db->join("user", "user.usr_id = notification.usr_id");

    return parent::get_all($params, $order_by);
	}
}