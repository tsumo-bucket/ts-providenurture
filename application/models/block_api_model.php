<?php
class Block_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'blo_id', 
			'usr_id', 
			'blo_user', 
			'blo_status', 
			'blo_date_created', 
			'blo_date_modified'
		);

		parent::__construct('block', $fields);
	}

	public function block($data)
	{
		$this->set_message("Block User");
		
		$this->form_validation->set_rules("usr_id", "User", "trim");
		$this->form_validation->set_rules("blo_id", "Block", "trim");
		$this->form_validation->set_rules("blo_user", "User", "trim|required|integer|max_length[11]");
			
    $error = $this->run_validators();
		if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
    	$this->db->where('usr_id', $data['blo_user']);
    	$this->db->where('usr_status', 'approved');
    	$users = $this->db->get('user');

    	if($users->num_rows() > 0){    		
    		if(isset($data['blo_id'])){
    			$this->db->where('blo_id', $data['blo_id']);
    			$this->db->where('block.usr_id', $data['usr_id']);
    			$this->db->where('blo_user', $data['blo_user']);
    			$blocks = $this->db->get('block');

    			if($blocks->num_rows() > 0){
    				$return = array();

    				$block = $blocks->row();

    				if($block->blo_status == 'inactive'){
    					$return['blo_status'] = 'active';
    					$this->db->where('user_like.usr_id', $data['usr_id']);
    					$this->db->where('usl_user', $data['blo_user']);
    					$user_likes = $this->db->get('user_like');

    					if($user_likes->num_rows() > 0){
    						$user_like = $user_likes->row();

    						$usl = array();
    						$usl['usl_status'] = 'inactive';
    						$usl['usl_date_modified'] = format_mysql_datetime();

    						$this->db->where('usl_id', $user_like->usl_id);
    						$this->db->update('user_like', $usl);
    					}

    					$this->db->where('follow.usr_id', $data['usr_id']);
    					$this->db->where('fol_user', $data['fol_user']);
    					$this->db->where('fol_status', 'active');
    					$follows = $this->db->get('follow');

    					if($follows->num_rows() > 0){
    						$follow = $follows->row();

    						$fol = array();
    						$fol['fol_status'] = 'inactive';
    						$fol['fol_date_modified'] = format_mysql_datetime();

    						$this->db->where('fol_id', $follow->fol_id);
    						$this->db->update('follow', $fol);
    					}

    					$this->db->where("(`cnv_user_1` = ".$data['usr_id']." AND `cnv_user_2` = ".$data['blo_user'].") OR (`cnv_user_1` = ".$data['blo_user']." AND `cnv_user_2` = ".$data['usr_id'].")");
		    			$this->db->where('cnv_status', 'active');
		    			$conversations = $this->db->get('conversation');
		    			if($conversations->num_rows() > 0){
		    				$conversation = $conversations->row();

		    				$cnv = array();
		    				$cnv['cnv_status'] = 'archived';

		    				$this->db->where('cnv_id', $conversation->cnv_id);
		    				$this->db->update('conversation', $cnv);
		    			}
    				}else{
    					$return['blo_status'] = 'inactive';
    				}

    				$blo = array();
    				$blo['blo_status'] = $return['blo_status'];
    				$blo['blo_date_modified'] = format_mysql_datetime();

    				$this->db->where('blo_id', $block->blo_id);
    				$this->db->update('block', $blo);

    				$return['blo_id'] = $block->blo_id;

		        $this->set_data($return, true);
    			}else{
    				$this->set_error(array('message'=>'Block entry not found.'));
    			}
    		}else{
	    		$this->db->where('block.usr_id', $data['usr_id']);
	    		$this->db->where('blo_user', $data['blo_user']);
	    		$this->db->where('blo_status', 'active');
	    		$blocks = $this->db->get('block');

	    		if($blocks->num_rows == 0){
		    		$block = array();

		    		$block['usr_id'] = $data['usr_id'];
		    		$block['blo_user'] = $data['blo_user'];
		    		$block['blo_status'] = 'active';
		    		$block['blo_date_created'] = format_mysql_datetime();
		    		$block['blo_date_modified'] = format_mysql_datetime();

		    		$this->db->insert('block', $block);
		        $blo_id = $this->db->insert_id();

		        $return = array();

		        $return['blo_id'] = $blo_id;
		        $return['blo_status'] = 'active';

		        $this->db->where("(`cnv_user_1` = ".$data['usr_id']." AND `cnv_user_2` = ".$data['blo_user'].") OR (`cnv_user_1` = ".$data['blo_user']." AND `cnv_user_2` = ".$data['usr_id'].")");
	    			$this->db->where('cnv_status', 'active');
	    			$conversations = $this->db->get('conversation');
	    			if($conversations->num_rows() > 0){
	    				$conversation = $conversations->row();

	    				$cnv = array();
	    				$cnv['cnv_status'] = 'archived';

	    				$this->db->where('cnv_id', $conversation->cnv_id);
	    				$this->db->update('conversation', $cnv);
	    			}

		        $this->set_data($return, true);
	    		}else{
	    			$this->set_error(array('message'=>'User already blocked.'));
	    		}
    		}
    	}else{
    		$this->set_error(array('message'=>'User not found.'));
    	}

    }
    return $this->compose_result();
	}

	public function get_all($usr_id)
	{
		$this->set_message("Users I've Blocked");

		$usr_ids = blocked_ids($usr_id);

		$this->db->select('blo_id, blo_user, blo_status, blo_date_modified');
		
		$this->db->where('block.usr_id', $usr_id);

		if(!empty($usr_ids)){
			$this->db->where_not_in('block.usr_id', $usr_ids);
		}

		$this->db->where('blo_status', 'active');
		$this->db->where('user.usr_status', 'approved');
		$this->db->join("user", "user.usr_id = block.blo_user");

		$this->db->order_by('blo_date_modified', 'desc');
		$blocks = $this->db->get('block');

		if($blocks->num_rows() > 0){
			$this->set_data($blocks->result(), true);
		}else{
			$this->set_error(array('message'=>"No users found."));
		}

    return $this->compose_result();
	}
}