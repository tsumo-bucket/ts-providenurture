<?php
// Extend Base_model instead of CI_model
class Voucher_xref_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'vcx_id', 
			'usr_id', 
			'vch_id', 
			'vcx_date_created'
		);

		$searchable_fields = array('');

		// Call the parent constructor with the table name and fields as parameters.


		parent::__construct('voucher_xref', $fields, $searchable_fields, null);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		$this->db->join("user", "user.usr_id = voucher_xref.usr_id");
		$this->db->join("voucher", "voucher.vch_id = voucher_xref.vch_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("user", "user.usr_id = voucher_xref.usr_id");
		$this->db->join("voucher", "voucher.vch_id = voucher_xref.vch_id");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}