<?php
class Info_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'inf_id', 
			'usr_id', 
			'inf_height', 
			'inf_height_unit', 
			'inf_body_type', 
			'inf_ethnicity', 
			'inf_smoke', 
			'inf_drink', 
			'inf_relationship', 
			'inf_children', 
			'inf_language', 
			'inf_education', 
			'inf_occupation', 
			'inf_spending_habits', 
			'inf_quality_time', 
			'inf_gifts', 
			'inf_travel', 
			'inf_staycations', 
			'inf_high_life', 
			'inf_simple', 
			'inf_other',
			'inf_net_worth', 
			'inf_yearly_income', 
			'inf_preferred_range_from', 
			'inf_preferred_range_to', 
			'inf_relationship_length', 
			'inf_relationship_loyalty',  
			'inf_relationship_preference', 
			'inf_sexual_limit', 
			'inf_privacy_expectations', 
			'inf_date_created', 
			'inf_date_modified'
		);

		parent::__construct('info', $fields);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Info");
		$this->db->where('info.usr_id', $id);
		$info = $this->db->get('info');
    	if($info->num_rows() > 0){
	    	$this->set_data($info->row(), true);
    	}
    	else {
    		$this->set_error(array('message'=>'Info not found.'));
    	}
    	return $this->compose_result();
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Infos");
		$this->db->join("user", "user.usr_id = info.usr_id");

   		return parent::get_all($params, $order_by);
	}

	public function edit($data)
	{
		$this->set_message("Update User");
		$this->form_validation->set_rules('usr_id', 'User', 'trim');

		$usr_id = $data['usr_id'];

		unset($data['usr_id']);

		$this->db->where('usr_id', $usr_id);
		$users = $this->db->get('user');

		$user = $users->row();

		$this->form_validation->set_rules("inf_id", "Info ID", "trim");
		$this->form_validation->set_rules("inf_height", "Height", "trim|integer|max_length[11]");
		$this->form_validation->set_rules("inf_body_type", "Body Type", "trim");
		$this->form_validation->set_rules("inf_ethnicity", "Ethnicity", "trim");
		
		$this->form_validation->set_rules("inf_spending_habits", "Spending Habits", "trim|integer|max_length[11]");
		$this->form_validation->set_rules("inf_preferred_range_from", "Preferred Range From", "trim|integer|max_length[11]");
		$this->form_validation->set_rules("inf_preferred_range_to", "Preferred Range To", "trim|integer|max_length[11]");
		$this->form_validation->set_rules("inf_quality_time", "Quality Time", "trim");
		$this->form_validation->set_rules("inf_gifts", "Gifts", "trim");
		$this->form_validation->set_rules("inf_travel", "Travel", "trim");
		$this->form_validation->set_rules("inf_staycations", "Staycations", "trim");
		$this->form_validation->set_rules("inf_high_life", "High Life", "trim");
		$this->form_validation->set_rules("inf_simple", "Simple", "trim");
		$this->form_validation->set_rules("inf_net_worth", "Net Worth", "trim|integer|max_length[11]");
		$this->form_validation->set_rules("inf_yearly_income", "Yearly Income", "trim|integer|max_length[11]");
		$this->form_validation->set_rules("inf_relationship_preference", "Relationship Preference", "trim");
		$this->form_validation->set_rules("inf_sexual_limit", "Sexual Limit", "trim");
		
		if($user->usr_type == 'sugar'){
			$this->form_validation->set_rules("inf_relationship", "Relationship", "trim");
			$this->form_validation->set_rules("inf_children", "Children", "trim");
			$this->form_validation->set_rules("inf_smoke", "Smoke", "trim");
			$this->form_validation->set_rules("inf_drink", "Drink", "trim");
			$this->form_validation->set_rules("inf_language", "Language", "trim");
			$this->form_validation->set_rules("inf_education", "Education", "trim");
			$this->form_validation->set_rules("inf_occupation", "Occupation", "trim|max_length[100]");
			
			$this->form_validation->set_rules("inf_relationship_length", "Relationship Length", "trim");
			$this->form_validation->set_rules("inf_relationship_loyalty", "Relationship Loyalty", "trim");
			$this->form_validation->set_rules("inf_privacy_expectations", "Privacy Expectations", "trim");
		}else{
			$this->form_validation->set_rules("inf_relationship", "Relationship", "trim");
			$this->form_validation->set_rules("inf_children", "Children", "trim");
			$this->form_validation->set_rules("inf_smoke", "Smoke", "trim");
			$this->form_validation->set_rules("inf_drink", "Drink", "trim");
			$this->form_validation->set_rules("inf_language", "Language", "trim");
			$this->form_validation->set_rules("inf_education", "Education", "trim");
			$this->form_validation->set_rules("inf_occupation", "Occupation", "trim|max_length[100]");
		}

		$error = $this->run_validators();
    
    if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
    	$this->db->where('info.usr_id', $usr_id);
    	$infos = $this->db->get('info');

    	if($infos->num_rows() > 0){
    		$info = $infos->row();

	    	$data['inf_date_modified'] = format_mysql_datetime();

	    	$this->db->where('inf_id', $info->inf_id);
	    	$this->db->update('info', $data);

	    	$this->db->join("city", "city.cty_id = user.cty_id", "left outer");
				$this->db->join("region", "region.reg_id = city.reg_id", "left outer");
				$this->db->join("country", "country.cnt_id = region.cnt_id", "left outer");
				$this->db->join("source", "source.src_id = user.src_id", "left outer");
				$this->db->where('user.usr_id', $usr_id);
				$users = $this->db->get('user');

				update_signup_status($usr_id);

	    	$this->set_data($users->row(), true);
    	}else{
    		$this->set_error(array('message'=>'Info not found.'));
    	}
    }
    return $this->compose_result();
	}
}