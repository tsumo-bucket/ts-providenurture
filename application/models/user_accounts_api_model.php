<?php
// Extend Base_model instead of CI_model
class User_accounts_api_model extends Api_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'usa_id', 
			'usr_id', 
			'usa_type', 
			'usa_email', 
			'usa_first_name', 
			'usa_last_name', 
			'usa_image', 
			'usa_friend_count', 
			'usa_photo_count', 
			'usa_birthday', 
			'usa_access_id', 
			'usa_access_token', 
			'usa_date_created', 
			'usa_date_modified'
		);

		parent::__construct('user_accounts', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of api_model.
	public function create($data, $field_list = array())
	{
		$this->set_message("Create User Accounts");
		$this->form_validation->set_rules("usr_id", "", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usa_type", "Type", "trim|required|max_length[50]");
		$this->form_validation->set_rules("usa_email", "Email", "trim|required|max_length[200]");
		$this->form_validation->set_rules("usa_first_name", "First Name", "trim|required|max_length[200]");
		$this->form_validation->set_rules("usa_last_name", "Last Name", "trim|required|max_length[200]");
		$this->form_validation->set_rules("usa_image", "Display Picture", "trim|required|max_length[200]");
		$this->form_validation->set_rules("usa_friend_count", "Friend Count", "trim|max_length[200]");
		$this->form_validation->set_rules("usa_photo_count", "Photo Count", "trim|max_length[200]");
		$this->form_validation->set_rules("usa_birthday", "Birthday", "trim|max_length[200]");
		$this->form_validation->set_rules("usa_access_id", "Access Id", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usa_access_token", "Access Token", "trim|required|integer|max_length[11]");
		return parent::create($data, $field_list = array());
	}

	public function update($data, $field_list = array())
	{
		$this->set_message("Update User Accounts");
		$this->form_validation->set_rules('usa_id', 'User Accounts Id', 'integer|required');
		$this->form_validation->set_rules("usr_id", "", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usa_type", "Type", "trim|required|max_length[50]");
		$this->form_validation->set_rules("usa_email", "Email", "trim|required|max_length[200]");
		$this->form_validation->set_rules("usa_first_name", "First Name", "trim|required|max_length[200]");
		$this->form_validation->set_rules("usa_last_name", "Last Name", "trim|required|max_length[200]");
		$this->form_validation->set_rules("usa_image", "Display Picture", "trim|required|max_length[200]");
		$this->form_validation->set_rules("usa_friend_count", "Friend Count", "trim|max_length[200]");
		$this->form_validation->set_rules("usa_photo_count", "Photo Count", "trim|max_length[200]");
		$this->form_validation->set_rules("usa_birthday", "Birthday", "trim|max_length[200]");
		$this->form_validation->set_rules("usa_access_id", "Access Id", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usa_access_token", "Access Token", "trim|required|integer|max_length[11]");
		return parent::update($data, $field_list = array());
	}

	public function delete($id)
	{
		$this->set_message("Delete User Accounts");
		return parent::delete($id);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single User Accounts");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List User Accounts");

        return parent::get_all($params, $order_by);
	}
}