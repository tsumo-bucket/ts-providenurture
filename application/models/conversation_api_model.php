<?php
class Conversation_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'cnv_id', 
			'cnv_slug', 
			'cnv_user_1', 
			'cnv_user_2', 
			'cnv_user_1_deleted', 
			'cnv_user_1_deleted_date', 
			'cnv_user_2_deleted', 
			'cnv_user_2_deleted_date', 
			'cnv_status',
			'cnv_date_created', 
			'cnv_date_modified'
		);

		parent::__construct('conversation', $fields);

		$this->load->model('message_model');
	}

	public function view($data)
	{
		$this->set_message("View Conversation");
		$this->form_validation->set_rules("usr_token_id", "User Token ID", "trim|required|max_length[100]");
		
		$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
    } else {
    	$this->db->where('usr_token_id', $data['usr_token_id']);
    	$this->db->where('usr_status', 'approved');
    	$users = $this->db->get('user');

    	if($users->num_rows() > 0){
    		$user = $users->row();


    		$this->db->where("((`usr_id` = ".$data['usr_id']." AND `blo_user` = ".$user->usr_id.") OR (`usr_id` = ".$user->usr_id." AND `blo_user` = ".$data['usr_id'].")) AND `blo_status` = 'active'");
    		$blocks = $this->db->get('block');

    		if($blocks->num_rows() > 0){
    			$this->set_error(array('message'=>'User not found.'));
    		}else{
		    	$this->db->where("(`cnv_status` = 'active') AND ((`cnv_user_1` = ".$user->usr_id." AND `cnv_user_2` = ".$data['usr_id'].") OR (`cnv_user_2` = ".$user->usr_id." AND `cnv_user_1` = ".$data['usr_id']."))");
		    	$conversations = $this->db->get('conversation');

		    	if($conversations->num_rows() > 0){
		    		$conversation = $conversations->row();


		    		if($conversation->cnv_user_1 == $data['usr_id']){
		    			if($conversation->cnv_user_1_deleted == 'yes'){
		    				$this->db->where('msg_date_created >=', $conversation->cnv_user_1_deleted_date);	
		    			}
		    		}else{
		    			if($conversation->cnv_user_2_deleted == 'yes'){
		    				$this->db->where('msg_date_created >=', $conversation->cnv_user_2_deleted_date);	
		    			}
		    		}

		    		$this->db->where('message.cnv_id', $conversation->cnv_id);
		    		$messages = $this->db->get('message');

		    		if($messages->num_rows() > 0){
		    			$this->set_data($messages->result(), true);
		    		}else{
		    			$this->set_error(array('message'=>'No messages found.'));
		    		}
		    	}else{
		    		$this->set_error(array('message'=>'No messages found.'));
		    	}
    		}
    	}else{
    		$this->set_error(array('message'=>'User not found.'));
    	}
    }
		return $this->compose_result();
	}

	public function update($data)
	{
		$this->set_message("Update Conversation");
		$this->form_validation->set_rules("usr_token_id", "User Token ID", "trim|required|max_length[100]");

		$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
    } else {
    	$this->db->where('usr_token_id', $data['usr_token_id']);
    	$this->db->where('usr_status', 'approved');
    	$users = $this->db->get('user');

    	if($users->num_rows() > 0){
    		$user = $users->row();

    		$this->db->where("((`usr_id` = ".$data['usr_id']." AND `blo_user` = ".$user->usr_id.") OR (`usr_id` = ".$user->usr_id." AND `blo_user` = ".$data['usr_id'].")) AND `blo_status` = 'active'");
    		$blocks = $this->db->get('block');

    		if($blocks->num_rows() > 0){
    			$this->set_error(array('message'=>'User not found.'));
    		}else{
    			$this->db->where("(`cnv_status` = 'active') AND ((`cnv_user_1` = ".$user->usr_id." AND `cnv_user_2` = ".$data['usr_id'].") OR (`cnv_user_2` = ".$user->usr_id." AND `cnv_user_1` = ".$data['usr_id']."))");
		    	$conversations = $this->db->get('conversation');

		    	if($conversations->num_rows() > 0){
		    		$cnv = array();
		    		$conversation = $conversations->row();

	    			$this->db->where('cnv_id', $conversation->cnv_id);
	    			$this->db->where('usr_id', $user->usr_id);
	    			$this->db->where('msg_user', $data['usr_id']);
	    			$this->db->where('msg_status', 'unread');
	    			$messages = $this->db->get('message');

	    			if($messages->num_rows() > 0){
	    				foreach($messages->result() as $message){
	    					$msg = array();
	    					$msg['msg_status'] = 'read';

	    					$this->db->where('msg_id', $message->msg_id);
	    					$this->db->update('message', $msg);
	    				}
	    			}
		    		
		    		if($conversation->cnv_user_1 == $data['usr_id']){
		    			$cnv['cnv_user_1_deleted'] = 'yes';
		    			$cnv['cnv_user_1_deleted_date'] = format_mysql_datetime();
		    		}else{
		    			$cnv['cnv_user_2_deleted'] = 'yes';
		    			$cnv['cnv_user_2_deleted_date'] = format_mysql_datetime();
		    		}

		    		$cnv['cnv_date_modified'] = format_mysql_datetime();
		    		$cnv['cnv_status'] = 'archived';

		    		$this->db->where('cnv_id', $conversation->cnv_id);
		    		$this->db->update('conversation', $cnv);

		    		$return = array();
	    			$return['message'] = 'Conversation deleted.';
	    			
	    			$this->set_data($return, true);
		    	}else{
		    		$this->set_error(array('message'=>'Conversation not found.'));
		    	}
    		}
    	}else{
    		$this->set_error(array('message'=>'User not found.'));
    	}
    }
    return $this->compose_result();
	}

	public function get_all($data, $order_by = array())
	{
		$this->set_message("List Conversations");

		$usr_id = $data['usr_id'];
		$source = $data['source'];
		unset($data['usr_id']);
    if(isset($data['source']) && ($data['source'] == 'inbox' || $data['source'] == 'sent' || $data['source'] == 'unread')){
   		switch ($data['source']) {
   			case 'inbox':
					unset($data['source']);
   				$this->db->where("(`cnv_user_1` = ".$usr_id." OR `cnv_user_2` = ".$usr_id.") AND (`cnv_status` = 'active')");
   				$this->db->where('cnv_status', 'active');
   				$conversations = parent::get_all($data, $order_by);
   				if($conversations['success']) {

	   				if(count($conversations['data'][0]) > 0){
	   					$cnv_ids = array();
	   					foreach($conversations['data'][0] as $conversation){
	   						if($conversation->cnv_user_1 == $usr_id){
				    			if($conversation->cnv_user_1_deleted == 'yes'){
				    				$this->db->where('msg_date_created >=', $conversation->cnv_user_1_deleted_date);	
				    			}
				    		}else{
				    			if($conversation->cnv_user_2_deleted == 'yes'){
				    				$this->db->where('msg_date_created >=', $conversation->cnv_user_2_deleted_date);	
				    			}
				    		}

				    		// $this->db->where('message.usr_id', $usr_id);
				    		// $this->db->or_where('msg_user', $usr_id);
				    		$this->db->where('message.cnv_id', $conversation->cnv_id);
				    		$messages = $this->db->get('message');
				    		
				    		if(count($messages->result()) > 0){
				    			array_push($cnv_ids, $conversation->cnv_id);
				    		}
	   					}

	   					if(!empty($cnv_ids)){
	   						$cnvs = cnv_check($cnv_ids, $usr_id);
	   						if(!empty($cnvs)){
			   					$this->db->where_in('cnv_id', $cnvs);
			   					$list = $this->db->get('conversation');


			   					foreach($list->result() as $conversation){
			   						
			   						$message = $this->message_model->latest_message($conversation->cnv_id, $usr_id, 'inbox');
			   						if(!empty($message)) {
		   								$conversation->usr_id = $message['usr_id'];
				   						$conversation->message = $message['message'];
				   						$conversation->date = $message['date'];
				   						$conversation->unread = $message['unread'];
			   						}

			   					}
			   					// print_r($list->result());die();
			   					$conversations['data'][0] = $list->result();
			   					return $conversations;

	   						}else{
	   							$this->set_error(array('message'=>"Nothing to display."));
	    						return $this->compose_result();
	   						}
		   				}else{
		   					$this->set_error(array('message'=>"Nothing to display."));
	  						return $this->compose_result();
		   				}
	   				}else{
	   					$this->set_error(array('message'=>"Nothing to display."));
							return $this->compose_result();
	   				}
   				}
   				else {
   					return $conversations;
   				}
   				break;

   			case 'sent':
   				unset($data['source']);
   				$this->db->where("(`cnv_user_1` = ".$usr_id." OR `cnv_user_2` = ".$usr_id.") AND (`cnv_status` = 'active')");
   				$this->db->where('cnv_status', 'active');
   				// $conversations = $this->db->get('conversation');
   				$conversations = parent::get_all($data, $order_by);

   				if(count($conversations['data'][0]) > 0){
   					$cnv_ids = array();
   					foreach($conversations['data'][0] as $conversation){
   						if($conversation->cnv_user_1 == $usr_id){
			    			if($conversation->cnv_user_1_deleted == 'yes'){
			    				$this->db->where('msg_date_created >=', $conversation->cnv_user_1_deleted_date);	
			    			}
			    		}else{
			    			if($conversation->cnv_user_2_deleted == 'yes'){
			    				$this->db->where('msg_date_created >=', $conversation->cnv_user_2_deleted_date);	
			    			}
			    		}

			    		$this->db->where('message.usr_id', $usr_id);
			    		$this->db->where('message.cnv_id', $conversation->cnv_id);
			    		$messages = $this->db->get('message');

			    		if($messages->num_rows() > 0){
			    			array_push($cnv_ids, $conversation->cnv_id);
			    		}
   					}

   					if(!empty($cnv_ids)){
   						$cnvs = cnv_check($cnv_ids, $usr_id);

   						if(!empty($cnvs)){
		   					$this->db->where_in('cnv_id', $cnvs);
		   					$sent = $this->db->get('conversation');


		   					foreach($sent->result() as $conversation){
		   						
		   						$message = $this->message_model->latest_message($conversation->cnv_id, $usr_id, 'sent');
		   						if(!empty($message)) {
			   						$conversation->usr_id = $message['usr_id'];
			   						$conversation->message = $message['message'];
			   						$conversation->date = $message['date'];
			   						$conversation->unread = $message['unread'];
		   						}
		   						else {
		   							continue;
		   						}

		   					}
		   					$conversations['data'][0] = $sent->result();
		   					return $conversations;
   						}else{
   							$this->set_error(array('message'=>"Nothing to display."));
    						return $this->compose_result();
   						}


	   				}else{
	   					$this->set_error(array('message'=>"Nothing to display."));
  						return $this->compose_result();
	   				}
   				}else{
   					$this->set_error(array('message'=>"Nothing to display."));
						return $this->compose_result();
   				}
   				break;

				case 'unread':
					unset($data['source']);
					$this->db->where("(`cnv_user_1` = ".$usr_id." OR `cnv_user_2` = ".$usr_id.") AND (`cnv_status` = 'active')");
   				$this->db->where('cnv_status', 'active');
   				$conversations = parent::get_all($data, $order_by);

   				if(count($conversations['data'][0]) > 0){
   					$cnv_ids = array();
   					foreach($conversations['data'][0] as $conversation){
   						if($conversation->cnv_user_1 == $usr_id){
			    			if($conversation->cnv_user_1_deleted == 'yes'){
			    				$this->db->where('msg_date_created >=', $conversation->cnv_user_1_deleted_date);	
			    			}
			    		}else{
			    			if($conversation->cnv_user_2_deleted == 'yes'){
			    				$this->db->where('msg_date_created >=', $conversation->cnv_user_2_deleted_date);	
			    			}
			    		}

			    		$this->db->where('msg_user', $usr_id);
			    		$this->db->where('msg_status', 'unread');
			    		$this->db->where('message.cnv_id', $conversation->cnv_id);
			    		$messages = $this->db->get('message');

			    		if($messages->num_rows() > 0){
			    			array_push($cnv_ids, $conversation->cnv_id);
			    		}
	   				}

	   				if(!empty($cnv_ids)){
	   					$cnvs = cnv_check($cnv_ids, $usr_id);

   						if(!empty($cnvs)){
		   					$this->db->where_in('cnv_id', $cnvs);
		   					$unread = $this->db->get('conversation');


		   					foreach($unread->result() as $conversation){
		   						
		   						$message = $this->message_model->latest_message($conversation->cnv_id, $usr_id, 'unread');
		   						if(!empty($message)) {
			   						$conversation->usr_id = $message['usr_id'];
			   						$conversation->message = $message['message'];
			   						$conversation->date = $message['date'];
			   						$conversation->unread = $message['unread'];
		   						}
		   						else {
		   							continue;
		   						}

		   					}

		   					$conversations['data'][0] = $unread->result();
		   					return $conversations;
		   					
   						}else{
   							$this->set_error(array('message'=>"Nothing to display."));
								return $this->compose_result();
   						}
	   				}else{
	   					$this->set_error(array('message'=>"Nothing to display."));
							return $this->compose_result();
	   				}
   				}else{
   					$this->set_error(array('message'=>"Nothing to display."));
						return $this->compose_result();
   				}
   				break;   			
   			
   			default:
   				break;
   		}
    }else{
    	$this->set_error(array('message'=>"Nothing to display."));
			return $this->compose_result();
    }
	}

	public function unread($data)
	{
		$this->set_message("Unread");

		$return = array();

		$this->db->where("(`cnv_user_1` = ".$data." OR `cnv_user_2` = ".$data.") AND (`cnv_status` = 'active')");
		$this->db->where('cnv_status', 'active');
		$conversations = $this->db->get('conversation');

		if($conversations->num_rows() > 0){
			$cnv_ids = array();
			foreach($conversations->result() as $conversation){
				if($conversation->cnv_user_1 == $data){
	  			if($conversation->cnv_user_1_deleted == 'yes'){
	  				$this->db->where('msg_date_created >=', $conversation->cnv_user_1_deleted_date);	
	  			}
	  		}else{
	  			if($conversation->cnv_user_2_deleted == 'yes'){
	  				$this->db->where('msg_date_created >=', $conversation->cnv_user_2_deleted_date);	
	  			}
	  		}

	  		$this->db->where('msg_user', $data);
	  		$this->db->where('msg_status', 'unread');
	  		$this->db->where('message.cnv_id', $conversation->cnv_id);
	  		$messages = $this->db->get('message');

	  		if($messages->num_rows() > 0){
	  			array_push($cnv_ids, $conversation->cnv_id);
	  		}
	  	}
			
			if(!empty($cnv_ids)){
				$this->db->where_in('cnv_id', $cnv_ids);
				$conversations = $this->db->get('conversation');

				$return['unread_count'] = $conversations->num_rows();
			}else{
				$return['unread_count'] = 0;
			}
		}else{
			$return['unread_count'] = 0;
		}

		$this->set_data($return, true);	
    
    return $this->compose_result();
	}
}