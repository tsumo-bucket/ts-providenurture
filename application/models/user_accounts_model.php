<?php
// Extend Base_model instead of CI_model
class User_accounts_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'usa_id', 
			'usr_id', 
			'usa_type', 
			'usa_email', 
			'usa_first_name', 
			'usa_last_name', 
			'usa_image', 
			'usa_friend_count', 
			'usa_photo_count', 
			'usa_birthday', 
			'usa_access_id', 
			'usa_access_token', 
			'usa_date_created', 
			'usa_date_modified'
		);

		$searchable_fields = array('usa_type', 'usa_email');

		// Call the parent constructor with the table name and fields as parameters.


		parent::__construct('user_accounts', $fields, $searchable_fields, null);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("", ". = user_accounts.");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}