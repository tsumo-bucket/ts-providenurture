<?php
class User_location_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'ulc_id', 
			'usr_id', 
			'ulc_street_number',
			'ulc_street',
			'ulc_city',
			'ulc_state',
			'ulc_country',
			'ulc_lat', 
			'ulc_lng', 
			'ulc_date_added'
		);

		parent::__construct('user_location', $fields);
	}

	public function update($data)
	{
		$this->set_message("Update User Location");

		$this->form_validation->set_rules("usr_id", "usr_id", "trim|required|max_length[255]");

		$usr_id = $data['usr_id'];
		unset($data['usr_id']);
		
		$this->db->where('cnt_name', $data['country_long_name']);
		$countries = $this->db->get('country');

		if($countries->num_rows() > 0){
			$data['cnt_id'] = $countries->row()->cnt_id;
		}else{
			$country = array();
			$country['cnt_name'] = $data['country_long_name'];
			$country['cnt_short_name'] = $data['country_short_name'];

			$this->db->insert('country', $country);
			$data['cnt_id'] = $this->db->insert_id();
		}

		$location = array();
		$location['usr_id'] = $usr_id;
		$location['ulc_lat'] = $data['lat'];
		$location['ulc_lng'] = $data['lng'];
		if(isset($data['city'])){
			$location['ulc_city'] = $data['city'];
			
			$data['usr_location'] = $data['city'].', '.$data['country_short_name'];
		}else{
			$data['usr_location'] = $data['state'].', '.$data['country_short_name'];
		}
		$location['ulc_state'] = $data['state'];
		$location['ulc_country'] = $data['country_long_name'];
		$location['ulc_date_added'] = format_mysql_datetime();

		$this->db->insert('user_location', $location);
		$data['ulc_id'] = $this->db->insert_id();

		unset($data['country_long_name']);
  	unset($data['country_short_name']);
  	unset($data['state']);
  	unset($data['city']);
  	unset($data['lat']);
  	unset($data['lng']);

  	$data['usr_modified_date'] = format_mysql_datetime();

		$this->db->where('usr_id', $usr_id);		
		$this->db->update('user', $data);
        
		$message = array();
		$message['message'] = 'Location updated.';

    $this->set_data($message,true);

		return $this->compose_result();
	}
}