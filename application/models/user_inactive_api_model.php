<?php
// Extend Base_model instead of CI_model
class User_inactive_api_model extends Api_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'usi_id', 
			'usr_id', 
			'usi_reason', 
			'usi_comment', 
			'usi_type', 
			'usi_status', 
			'usi_created_date', 
			'usi_modified_date'
		);

		parent::__construct('user_inactive', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of api_model.
	public function create($data, $field_list = array())
	{
		$this->set_message("Create User Inactive");
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usi_reason", "Reason", "trim|required");
		$this->form_validation->set_rules("usi_comment", "Comment", "trim|required");
		$this->form_validation->set_rules("usi_type", "Type", "trim|required");
		$this->form_validation->set_rules("usi_status", "Status", "trim|required");
		$this->form_validation->set_rules("usi_created_date", "Created Date", "trim|required|datetime");
		$this->form_validation->set_rules("usi_modified_date", "Modified Date", "trim|required|datetime");
		return parent::create($data, $field_list = array());
	}

	public function update($data, $field_list = array())
	{
		$this->set_message("Update User Inactive");
		$this->form_validation->set_rules('usi_id', 'User Inactive Id', 'integer|required');
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("usi_reason", "Reason", "trim|required");
		$this->form_validation->set_rules("usi_comment", "Comment", "trim|required");
		$this->form_validation->set_rules("usi_type", "Type", "trim|required");
		$this->form_validation->set_rules("usi_status", "Status", "trim|required");
		$this->form_validation->set_rules("usi_created_date", "Created Date", "trim|required|datetime");
		$this->form_validation->set_rules("usi_modified_date", "Modified Date", "trim|required|datetime");
		return parent::update($data, $field_list = array());
	}

	public function delete($id)
	{
		$this->set_message("Delete User Inactive");
		return parent::delete($id);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single User Inactive");
		$this->db->join("user", "user.usr_id = user_inactive.usr_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List User Inactives");
		$this->db->join("user", "user.usr_id = user_inactive.usr_id");

        return parent::get_all($params, $order_by);
	}
}