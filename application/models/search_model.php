<?php
// Extend Base_model instead of CI_model
class Search_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'sch_id', 
			'usr_id', 
			'sch_name', 
			'sch_content', 
			'sch_status', 
			'sch_date_created'
		);

		$searchable_fields = array('');

		// Call the parent constructor with the table name and fields as parameters.


		parent::__construct('search', $fields, $searchable_fields, null);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		$this->db->join("user", "user.usr_id = search.usr_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("user", "user.usr_id = search.usr_id");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}