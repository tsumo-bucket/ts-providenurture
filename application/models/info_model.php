<?php
class Info_model extends Base_model
{
	public function __construct()
	{
		$fields = array(
			'inf_id', 
			'usr_id', 
			'inf_height', 
			'inf_height_unit', 
			'inf_body_type', 
			'inf_ethnicity', 
			'inf_smoke', 
			'inf_drink', 
			'inf_relationship', 
			'inf_children', 
			'inf_language', 
			'inf_education', 
			'inf_occupation', 
			'inf_spending_habits', 
			'inf_quality_time', 
			'inf_gifts', 
			'inf_travel', 
			'inf_staycations', 
			'inf_high_life', 
			'inf_simple', 
			'inf_other',
			'inf_net_worth', 
			'inf_yearly_income', 
			'inf_preferred_range_from', 
			'inf_preferred_range_to', 
			'inf_relationship_length', 
			'inf_relationship_loyalty', 
			'inf_relationship_preference', 
			'inf_sexual_limit', 
			'inf_privacy_expectations', 
			'inf_date_created', 
			'inf_date_modified'
		);

		$searchable_fields = array('');

		parent::__construct('info', $fields, $searchable_fields, null);
	}
	
	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		$this->db->join("user", "user.usr_id = info.usr_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("user", "user.usr_id = info.usr_id");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}

	public function get_all_by_user($usr_id)
	{
		$this->db->where('info.usr_id', $usr_id);
		$infos = $this->db->get('info');

		if($infos->num_rows() > 0){
			return $infos->row();
		}else{
			return false;
		}	
	}
}