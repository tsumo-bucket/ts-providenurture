<?php
// Extend Base_model instead of CI_model
class Flag_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'flg_id', 
			'usr_id', 
			'pos_id', 
			'flg_status', 
			'flg_date_created', 
			'flg_date_modified'
		);

		$searchable_fields = array('user.usr_email', 'post.pos_content', 'flg_status');

		// Call the parent constructor with the table name and fields as parameters.


		parent::__construct('flag', $fields, $searchable_fields, null);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		$this->db->join("user", "user.usr_id = flag.usr_id");
		$this->db->join("post", "post.pos_id = flag.pos_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("user", "user.usr_id = flag.usr_id");
		$this->db->join("post", "post.pos_id = flag.pos_id");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}