<?php
// Extend Base_model instead of CI_model
class Voucher_xref_api_model extends Api_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'vcx_id', 
			'usr_id', 
			'vch_id', 
			'vcx_date_created'
		);

		parent::__construct('voucher_xref', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of api_model.
	public function create($data, $field_list = array())
	{
		$this->set_message("Create Voucher Xref");
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("vch_id", "Voucher", "trim|required|integer|max_length[11]");
		return parent::create($data, $field_list = array());
	}

	public function update($data, $field_list = array())
	{
		$this->set_message("Update Voucher Xref");
		$this->form_validation->set_rules('vcx_id', 'Voucher Xref Id', 'integer|required');
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("vch_id", "Voucher", "trim|required|integer|max_length[11]");
		return parent::update($data, $field_list = array());
	}

	public function delete($id)
	{
		$this->set_message("Delete Voucher Xref");
		return parent::delete($id);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Voucher Xref");
		$this->db->join("user", "user.usr_id = voucher_xref.usr_id");
		$this->db->join("voucher", "voucher.vch_id = voucher_xref.vch_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Voucher Xrefs");
		$this->db->join("user", "user.usr_id = voucher_xref.usr_id");
		$this->db->join("voucher", "voucher.vch_id = voucher_xref.vch_id");

        return parent::get_all($params, $order_by);
	}
}