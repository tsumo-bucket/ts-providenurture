<?php
class Country_model extends Base_model
{
	public function __construct()
	{
		$fields = array(
			'cnt_id', 
			'cnt_name',
			'cnt_short_name',
			'cnt_status'
		);

		$searchable_fields = array('cnt_name', 'cnt_short_name');

		parent::__construct('country', $fields, $searchable_fields, null);
	}

	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}