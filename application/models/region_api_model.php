<?php
class Region_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'reg_id', 
			'cnt_id', 
			'reg_name',
			'reg_status'
		);

		parent::__construct('region', $fields);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Region");
		$this->db->join("country", "country.cnt_id = region.cnt_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Regions");
		$this->db->join("country", "country.cnt_id = region.cnt_id");

    return parent::get_all($params, $order_by);
	}
}