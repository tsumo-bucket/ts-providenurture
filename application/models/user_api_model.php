<?php
class User_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'usr_id', 
			'usr_type', 
			'usr_first_name', 
			'usr_last_name', 
			'usr_screen_name', 
			'usr_email', 
			'usr_password', 
			'usr_birthyear', 
			'usr_birthmonth', 
			'usr_birthdate',
			'usr_birthday', 
			'cty_id', 
			'cnt_id', 
			'ulc_id', 
			'usr_location', 
			'usr_status', 
			'usr_gender', 
			'usr_interested', 
			'usr_interested_age_from', 
			'usr_interested_age_to',
			'usr_interested_country', 
			'usr_description', 
			'usr_signup_step', 
			'usr_signup_source', 
			'usr_verified', 
			'usr_social_verified', 
			'usr_password_reset', 
			'usr_created_date', 
			'usr_modified_date', 
			'usr_approved_date',
			'usr_last_login', 
			'usr_password_reset_date',
			'usr_token', 
			'usr_token_id', 
			'img_id', 
			'src_id'
		);

		parent::__construct('user', $fields);
		$this->load->model('user_model');
		$this->load->model('user_accounts_api_model');
		$this->load->model('user_accounts_model');

		$this->mythos->library('email');
	}
	public function initial($data)
	{
		$this->set_message("Register User");

    $this->form_validation->set_rules("usr_gender", "Gender", "trim|required|max_length[255]");
    $this->form_validation->set_rules("usr_type", "Type", "trim|required|max_length[255]");
    $this->form_validation->set_rules("usr_interested", "Interested", "trim|required|max_length[255]");
    $this->form_validation->set_rules("usr_interested_age_from", "Interested Age From", "trim|required|integer");
    $this->form_validation->set_rules("usr_interested_age_to", "Interested Age To", "trim|required|integer");
    $this->form_validation->set_rules("cty_id", "City", "trim");
    $this->form_validation->set_rules("usr_birthyear", "Birth Year", "trim|required");
    $this->form_validation->set_rules("usr_birthmonth", "Birth Month", "trim|required");
    $this->form_validation->set_rules("usr_birthdate", "Birth Date", "trim|required");
    $this->form_validation->set_rules("usr_first_name", "First Name", "trim|max_length[100]");
		$this->form_validation->set_rules("usr_last_name", "Last Name", "trim|max_length[100]");
		$this->form_validation->set_rules("usr_screen_name", "Screen Name", "trim|max_length[100]");
  	if($data['usr_signup_source'] != "facebook" || $data['usr_signup_source'] != "google") {
			$this->form_validation->set_rules("usr_email", "Email", "trim|required|max_length[100]|valid_email|is_unique[user.usr_email]");
  	}
		$this->form_validation->set_rules("usr_password", "Password", "trim|required");
		$this->form_validation->set_rules("usr_signup_source", "Signup Source", "trim|required");
		$this->form_validation->set_rules("src_id", "Source", "trim");

  	$this->form_validation->set_message('is_unique', 'The %s is already taken.');
  	$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
    } else {
    	$this->db->where('usr_email', $data['usr_email']);
    	$query = $this->db->get('user');
    	if($query->num_rows() > 0){
    		$this->set_error(array('message'=>'Email address is not valid or unavailable.'));
    	}else{
    		//SET USER_LOCATION HERE
    		
				$this->db->where('cnt_short_name', $data['country_short_name']);
				$countries = $this->db->get('country');

				if($countries->num_rows() > 0){
					$data['cnt_id'] = $countries->row()->cnt_id;
				}else{
					if($data['country_short_name'] == 'GB')
					{
						$this->db->where('cnt_short_name', 'UK');
						$countries = $this->db->get('country');
						$data['cnt_id'] = $countries->row()->cnt_id;
					}
					else {
						$country = array();
						$country['cnt_name'] = $data['country_long_name'];
						$country['cnt_short_name'] = $data['country_short_name'];

						$this->db->insert('country', $country);
						$data['cnt_id'] = $this->db->insert_id();
					}
				}

				$location = array();
				$location['ulc_lat'] = $data['lat'];
				$location['ulc_lng'] = $data['lng'];
				if(isset($data['city'])){
					$location['ulc_city'] = $data['city'];
					
					$data['usr_location'] = $data['city'].', '.$data['country_short_name'];
				}else{
					$data['usr_location'] = $data['state'].', '.$data['country_short_name'];
				}
				if(isset($data['street_number'])) {
					$location['ulc_street_number'] = $data['street_number'];
				}
				if(isset($data['street'])) {
					$location['ulc_street'] = $data['street'];
				}
				$location['ulc_state'] = $data['state'];
				$location['ulc_country'] = $data['country_long_name'];
				$location['ulc_date_added'] = format_mysql_datetime();

				$this->db->insert('user_location', $location);
				$data['ulc_id'] = $this->db->insert_id();
    		
				//END SET USER_LOCATION

    		// if(isset($data['cty_id']) && $data['cty_id'] != ''){
	    	// 	$this->db->join("region", "region.reg_id = city.reg_id", "left outer");
	    	// 	$this->db->where('cty_id', $data['cty_id']);
	    	// 	$cities = $this->db->get('city');

	    	// 	if($cities->num_rows() > 0){
	    	// 		$city = $cities->row();
	    	// 		$data['usr_interested_country'] = $city->cnt_id;
	    	// 	}
    		// }

    		$data['usr_interested_country'] = $data['cnt_id'];
    		
    		$data['usr_birthday'] = $data['usr_birthyear'].'-'.str_pad($data['usr_birthmonth'], 2, '0', STR_PAD_LEFT).'-'.str_pad($data['usr_birthdate'], 2, '0', STR_PAD_LEFT);

      	if($data['usr_signup_source'] == "web") {
      		$data['usr_password'] = md5($data['usr_password']);
      	}
      	$data['usr_status'] = 'pending';
 				$data['usr_signup_step'] = 'info';
        $data['usr_created_date'] = format_mysql_datetime();
        $data['usr_modified_date'] = format_mysql_datetime();
        $data['usr_signup_source'] = $data['usr_signup_source'];
      	$data['usr_token'] =  md5(substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 32)),0, 32));

      	unset($data['country_long_name']);
      	unset($data['country_short_name']);
      	unset($data['state']);
      	unset($data['city']);
      	unset($data['lat']);
      	unset($data['lng']);

      	if(isset($data['fb_id']) || isset($data['access_token'])) {
	      	$access_token = $data['access_token'];
	      	$fb_id = $data['fb_id'];
	      	$fb_fname = $data['first_name'];
	      	$fb_lname = $data['last_name'];
	      	$fb_birthday = $data['fb_birthday'];
	      	$fb_friend = $data['friend_count'];
	      	$fb_image = $data['fb_image'];
	      	$fb_photo = $data['photo_count'];
	      	unset($data['access_token']);
	      	unset($data['fb_id']);
	      	unset($data['first_name']);
	      	unset($data['last_name']);
	      	unset($data['fb_birthday']);
	      	unset($data['friend_count']);
	      	unset($data['fb_image']);
	      	unset($data['photo_count']);
	      	if($fb_friend >= 100 && $fb_photo >= 20) {
	      		$data['usr_social_verified'] = 'yes';
	      	}
      	}


				$this->db->insert('user', $data);
        $usr_id = $this->db->insert_id();

      	if($data['usr_signup_source'] != "web") {
		    	$this->db->where('usa_email', $data['usr_email']);
		    	$query = $this->db->get('user_accounts');
		    	if($query->num_rows() > 0){
		    		$this->set_error(array('message'=>'Email address is not valid or unavailable.'));
		    	}else{
	      		$usa = array();
	      		$usa['usa_type'] = $data['usr_signup_source'];
	      		$usa['usr_id'] = $usr_id;
	      		$usa['usa_email'] = $data['usr_email'];
	      		$usa['usa_first_name'] = $fb_fname;
	      		$usa['usa_last_name'] = $fb_lname;
	      		$usa['usa_birthday'] = $fb_birthday;
	      		$usa['usa_friend_count'] = $fb_friend;
	      		$usa['usa_photo_count'] = $fb_photo;
	      		$usa['usa_image'] = $fb_image;
	      		$usa['usa_access_token'] = $access_token;
	  				if($data['usr_signup_source'] != "facebook" || $data['usr_signup_source'] != "google") {
	      			$usa['usa_access_id'] = $fb_id;
	  				}
	  				else if($data['usr_signup_source'] != "google") {
	  					//change to google params
	      			$usa['usa_access_id'] = $fb_id;
	  				}
	      		$usa['usa_date_created'] = format_mysql_datetime();
	      		$this->user_accounts_api_model->create($usa);
		    	}
      	}
      	
        $user = array();
        $user['usr_token_id'] = format_datetime(format_mysql_datetime(), 'U').$usr_id;

        $this->db->where('usr_id', $usr_id);
        $this->db->update('user', $user);


        //UPDATE USER_LOCATION
        $location = array();
        $location['usr_id'] = $usr_id;

        $this->db->where('ulc_id', $data['ulc_id']);
        $this->db->update('user_location', $location);

        //CREATE INFO
        $info = array();
        $info['usr_id'] = $usr_id;
        $info['inf_date_created'] = format_mysql_datetime();
        $info['inf_date_modified'] = format_mysql_datetime();

        $this->db->insert('info', $info);
				$info_id = $this->db->insert_id();

        //CREATE PREFERENCE
        $que_ids = array();
        $prf_ids = array();

        $this->db->where('que_type', 'about me');
        $this->db->where('que_category', $data['usr_type']);
        $this->db->where('que_status', 'published');
        $questions_a = $this->db->get('question');

        if($questions_a->num_rows() > 0){
        	$question = $questions_a->row();
        	array_push($que_ids, $question->que_id);
        }

        $this->db->where('que_type', 'looking for');
        $this->db->where('que_status', 'published');
        $questions_lf = $this->db->get('question');

        if($questions_lf->num_rows() > 0){
        	$question = $questions_lf->row();
        	array_push($que_ids, $question->que_id);
        }

        for($i = 0; $i < sizeof($que_ids); $i++){
	        $preference = array();
	        $preference['usr_id'] = $usr_id;
	        $preference['que_id'] = $que_ids[$i];
	        $preference['prf_date_created'] = format_mysql_datetime();
	        $preference['prf_date_modified'] = format_mysql_datetime();
        	$this->db->insert('preference', $preference);        	
      		array_push($prf_ids, $this->db->insert_id());
        }

        //CREATE NOTIFICATION
        $notification = array();
        $notification['usr_id'] = $usr_id;
        $notification['not_date_created'] = format_mysql_datetime();
        $notification['not_date_modified'] = format_mysql_datetime();

        $this->db->insert('notification', $notification);
				$not_id = $this->db->insert_id();

        //CREATE PRIVACY
        $privacy = array();
        $privacy['usr_id'] = $usr_id;
        $privacy['usr_id'] = $usr_id;
        $privacy['pri_date_created'] = format_mysql_datetime();
        $privacy['pri_date_modified'] = format_mysql_datetime();

        $this->db->insert('privacy', $privacy);
				$pri_id = $this->db->insert_id();

        $this->db->where('usr_id', $usr_id);
        $user = $this->db->get('user');
        $user = $user->row();
        $user->inf_id = $info_id;
        $user->prf_ids = $prf_ids;
        $user->not_id = $not_id;
        $user->pri_id = $pri_id;

				$return = $user;

        $this->set_data($return,true);
    	}
    }
		return $this->compose_result();
	}

	public function verify($data)
	{
		$this->set_message("Verify Email");

    $this->form_validation->set_rules("usr_id", "usr_id", "trim|required|max_length[255]");

    $error = $this->run_validators();
		if(!is_bool($error)){
    	$this->set_error(array('message'=>'Invalid link.'));
    }else{
    	$hit = false;

    	$users = $this->db->get('user');
			foreach($users->result() as $usr){
				if(md5($usr->usr_id) == $data['usr_id']){
					$hit = true;
					$user = $usr;
				}
			}

			if($hit){
				if($user && $user->usr_verified == 'no'){
					$update = array();
					$update['usr_verified'] = 'yes';
					$update['usr_modified_date'] = format_mysql_datetime();

					$this->db->where('usr_id', $user->usr_id);
					$this->db->update('user', $update); 	
				}

				$message = array();
				$message['message'] = 'Account verified.';

				$this->set_data($message, true);
			}else{
				$this->set_error(array('message'=>'Invalid link.'));
			}
    }
    return $this->compose_result();
	}

	public function resend($data)
	{
		$this->set_message("Verify Email");

  	$this->form_validation->set_rules("usr_email", "usr_email", "trim|required|max_length[255]");

  	$error = $this->run_validators();
		if(!is_bool($error)){
    	$this->set_error(array('message'=>'Invalid link.'));
    }else{
    	$this->db->where('usr_email', $data['usr_email']);
    	$users = $this->db->get('user');

    	$return = array();
    	
    	if($users->num_rows() > 0){
    		$user = $users->row();
    		
    		$return['usr_email'] = $user->usr_email;
    	}

			$this->set_data($return, true);
    }
    return $this->compose_result();
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single User");
		// $this->db->join("city", "city.cty_id = user.cty_id", "left outer");
		$this->db->join("image", "image.img_id = user.img_id", "left outer");
		$this->db->join("source", "source.src_id = user.src_id", "left outer");
		$this->db->join("user_accounts", "user_accounts.usr_id = user.usr_id", "left outer");
		$this->db->join("user_location", "user_location.ulc_id = user.ulc_id", "left outer");

		return parent::get_one($id);
	}

	public function get_one_simplified($usr_id, $viewer)
	{
		$this->db->select('user.usr_id, usr_type, usr_screen_name, usr_gender, usr_birthmonth, usr_birthdate, usr_birthyear, usr_description, user.img_id, usr_token_id, usr_approved_date, usr_last_login, usr_location, cnt_id');

		$this->db->where('user.usr_id', $usr_id);

		// $this->db->join("city", "city.cty_id = user.cty_id", "left outer");
		// $this->db->join("region", "region.reg_id = city.reg_id", "left outer");
		// $this->db->join("country", "country.cnt_id = region.cnt_id", "left outer");
		$this->db->join("image", "image.img_id = user.img_id", "left outer");
		$this->db->join("source", "source.src_id = user.src_id", "left outer");

		$users = $this->db->get('user');
		if($users->num_rows() > 0){
			$user = $users->row();

			$from = new DateTime($user->usr_birthyear.'-'.$user->usr_birthmonth.'-'.$user->usr_birthdate);
      $to = new DateTime('today');

      unset($user->usr_birthyear);
      unset($user->usr_birthmonth);
      unset($user->usr_birthdate);

      $user->usr_age = $from->diff($to)->y;

      $additional_data = $this->user_model->additional($user->usr_id); 

      $user = (object) array_merge((array) $user, (array) $additional_data);

      $user->faved = $this->user_model->faved($user->usr_id, $viewer);
      $user->followed = $this->user_model->followed($user->usr_id, $viewer);

      return $user;
		}else{
			return false;
		}
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Users");

		$usr_ids = array();
		$usr_ids[] = $params['usr_id'];

		$usr_id = $params['usr_id'];
		$source = $params['source'];
		unset($params['usr_id']);
		unset($params['source']);

		// $this->db->where('block.usr_id', $usr_id);
		// $this->db->where('blo_status', 'active');
		// $blocks = $this->db->get('block');

		// if($blocks->num_rows() > 0){
		// 	foreach($blocks->result() as $block){
		// 		$user = $this->user_model->get_one($block->blo_user);

		// 		if($user && $user->usr_status == 'approved'){
		// 			if(!in_array($block->blo_user, $usr_ids)){
		// 				array_push($usr_ids, $block->blo_user);
		// 			}
		// 		}
		// 	}
		// }

		// $this->db->where('blo_user', $usr_id);
		// $this->db->where('blo_status', 'active');
		// $blocks = $this->db->get('block');

		// if($blocks->num_rows() > 0){
		// 	foreach($blocks->result() as $block){
		// 		$user = $this->user_model->get_one($block->usr_id);

		// 		if($user && $user->usr_status == 'approved'){
		// 			if(!in_array($block->usr_id, $usr_ids)){
		// 				array_push($usr_ids, $block->blo_user);
		// 			}
		// 		},
		// 	}
		// }

		$user = $this->user_model->get_one_simple($usr_id);
		// search by distance
		if(isset($params["lat"]) || isset($params["long"])) {
  		$this->db->select(", (acos(sin(" . deg2rad($params["lat"]) . ") * sin(radians(`user_location`.`ulc_lat`)) + cos(" . deg2rad($params["lat"]) . ") * cos(radians(`user_location`.`ulc_lat`)) * cos(" . deg2rad($params["long"]) . " - radians(`user_location`.`ulc_lng`))) * 6371) as distance  ");
		}
  	$this->db->select("user.img_id, IF(`user`.`img_id` IS NULL, 0, 1) as img_id_sort ", false);
		$this->db->select("user.usr_id, usr_type, usr_screen_name, usr_gender, usr_birthmonth, usr_birthdate, usr_birthyear, usr_description, usr_approved_date, usr_last_login, usr_social_verified, usr_token_id, usr_location, cnt_id ");

		if($user->usr_type == 'sugar'){
  		$type = 'flame';
  	}else{
  		$type = 'sugar';
  	}

		if(isset($params["lat"]) || isset($params["long"])) {
    	if($params["distance_to"] >= 10001) {
    		$this->db->having("distance >= " . $params["distance_from"] . " ");
    	}
    	else {
    		$this->db->having("distance >= " . $params["distance_from"] . " ");
    		$this->db->having("distance <= " . $params["distance_to"] . " ");
    	}
		}
  	if($source == 'nearest') {
  		$this->db->order_by("img_id_sort", "desc");   
  		$this->db->order_by("distance");   
  	}

  	if($source == 'recent') {
  		$this->db->order_by("img_id_sort", "desc");   
  		$this->db->order_by("usr_last_login", "desc");   
  	}

  	if($source == 'facebook' || $source == '') {
  		$this->db->order_by("img_id_sort", "desc");   
  	}
  	unset($params["distance_from"]);
  	unset($params["distance_to"]);
  	unset($params["long"]);
  	unset($params["lat"]);
  	unset($params["source"]);


  	if($source == 'facebook') {
  		$this->db->where('usr_social_verified', 'yes');
  	}

  	$this->db->where('user.usr_type', $type);
  	$this->db->where('user.usr_status', 'approved');

  	if($user->usr_interested != 'both'){
			$this->db->where('user.usr_gender', $user->usr_interested);
		}

		$age_from = $user->usr_interested_age_from;
		$age_to = $user->usr_interested_age_to + 1;

    $this->db->where('user.usr_birthday <=', date("Y-m-d", strtotime("-$age_from year", time())));
    $this->db->where('user.usr_birthday >=', date("Y-m-d", strtotime("-$age_to year", time())));

    // if($user->usr_interested_country != 0){
    // 	$this->db->where('user.cnt_id', $user->usr_interested_country);
    // }

    $this->db->where('user.usr_id !=', $user->usr_id);
   	$this->db->where("(user.usr_id not in (select (CASE usr_id when ".$user->usr_id." then blo_user else usr_id end) as usr_id from block where (blo_user = ".$user->usr_id." AND blo_status = 'active') or (usr_id = ".$user->usr_id." AND blo_status = 'active')))");

    $this->db->where("((`privacy`.`pri_profile` = 'no') OR (`privacy`.`pri_profile` = 'yes' AND ((`user`.`usr_interested` = 'both' OR `user`.`usr_interested` = '".$user->usr_gender."') AND (`user`.`usr_interested_age_from` <= DATEDIFF(CURRENT_DATE, STR_TO_DATE('".$user->usr_birthday."', '%Y-%m-%d'))/365) AND (`user`.`usr_interested_age_to` >= DATEDIFF(CURRENT_DATE, STR_TO_DATE('".$user->usr_birthday."', '%Y-%m-%d'))/365))))");

    $this->db->join("privacy", "privacy.usr_id = user.usr_id", "left outer");
		// $this->db->join("city", "city.cty_id = user.cty_id", "left outer");
		// $this->db->join("region", "region.reg_id = city.reg_id", "left outer");
		// $this->db->join("country", "country.cnt_id = region.cnt_id", "left outer");
		$this->db->join("image", "image.img_id = user.img_id", "left outer");
		$this->db->join("source", "source.src_id = user.src_id", "left outer");
		$this->db->join("user_accounts", "user_accounts.usr_id = user.usr_id", "left outer");
		$this->db->join("user_location", "user_location.ulc_id = user.ulc_id", "left outer");
  	return parent::get_all($params, $order_by);
	}

	public function generate_token($usr_id)
	{
		$this->set_message("Verify Email");

		$this->db->where('usr_id', $usr_id);
		$users = $this->db->get('user');

		if($users->num_rows() > 0){
			$user = $users->row();

			$user_update = array();
			$user_update['usr_token'] = md5($user->usr_token);
			$user_update['usr_modified_date'] = format_mysql_datetime();

			$this->db->where('usr_id', $usr_id);
			$this->db->update('user', $user_update);
		}

		$this->set_data(array('message'=>'logout'), true);
		return $this->compose_result();
	}

	public function get_token($email)
	{
		$this->set_message("Get User Token");

		$this->db->select('usr_token');
		$this->db->where('usr_email', $email);
		$user = $this->db->get('user');
		$result = $user->row();
		$this->set_data($result, true);
		return $this->compose_result();
	}

	public function forgot($data)
	{
		$this->set_message("Forgot Password");

    $this->form_validation->set_rules('usr_email', 'Email Address', 'trim|required');

    $error = $this->run_validators();

    if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
      $return = array();

      $this->db->where('user.usr_email', $data['usr_email']);
      $query = $this->db->get('user');
      if($query->num_rows() > 0)
      {
        $user = $query->row();

        $user->usr_password_reset = $this->user_model->generate_password_hash();
        $user->usr_password_reset_date = format_mysql_datetime();

        $this->db->where('usr_id', $user->usr_id);
        $this->db->update('user', $user);
         
        $return['usr_email'] = $data['usr_email'];
      }

      $this->set_data($return, true);
     
    }
    return $this->compose_result();
	}

	public function reset($data)
	{
		$this->set_message("Reset Password");

    $this->form_validation->set_rules('usr_password_reset', 'Password Hash', 'trim|required');
    $this->form_validation->set_rules("usr_password", "Password", "trim|required");

    $error = $this->run_validators();

    if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
    	$this->db->where('usr_password_reset', $data['usr_password_reset']);
    	$users = $this->db->get('user');

    	if($users->num_rows() > 0){
    		$user = $users->row();

        $end_time = strtotime("+1 days", strtotime($user->usr_password_reset_date));
        $current = strtotime(format_mysql_datetime());

        if($end_time >= $current){
	    		$user->usr_password = md5($data['usr_password']);
	    		$user->usr_password_reset = NULL;
	    		$user->usr_password_reset_date = NULL;
	    		$user->usr_modified_date = format_mysql_datetime();
	    		$user->usr_token = md5($user->usr_token);

	    		$this->db->where('usr_id', $user->usr_id);
	    		$this->db->update('user', $user);
    			
    			$this->set_data(array('message'=>'Password updated.'), true);
        }else{
          $this->set_error(array('message'=>"Link has expired"));
        }
    	} else {
    		$this->set_error(array('message'=>'Invalid link.'));
    	}
    }
    return $this->compose_result();
	}

	public function deactivate($data)
	{
		$this->set_message("Deactivate Account");

    $this->form_validation->set_rules('usr_id', 'User', 'trim');
    $this->form_validation->set_rules('usi_reason', 'Reason', 'trim|required');
    $this->form_validation->set_rules('usi_comment', 'Suggestion', 'trim');
    $this->form_validation->set_rules('usr_password', "Password", "trim|required");

    $error = $this->run_validators();

    if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
    	$this->db->where('usr_id', $data['usr_id']);
    	$users = $this->db->get('user');

    	if($users->num_rows() > 0){
    		$user = $users->row();

    		if(md5($data['usr_password']) == $user->usr_password){
	    		if($user->usr_status == 'approved'){
		    		$user->usr_status = 'deactivated';
		    		$user->usr_modified_date = format_mysql_datetime();

		    		$this->db->where('usr_id', $user->usr_id);
		    		$this->db->update('user', $user);
		    		
			    	$user_inactive = array();

			    	$user_inactive['usr_id'] = $user->usr_id;
			    	$user_inactive['usi_reason'] = $data['usi_reason'];
			    	$user_inactive['usi_comment'] = $data['usi_comment'];
			    	$user_inactive['usi_type'] = 'deactivate';
			    	$user_inactive['usi_status'] = 'active';
			    	$user_inactive['usi_created_date'] = format_mysql_datetime();

			    	$this->db->insert('user_inactive', $user_inactive);
	    		}
    			$this->set_data(array('message'=>'Accound deactivated.'), true);
    		}else{
					$this->set_error(array('message'=>'Incorrect password.'));	
    		}
    	}else{
				$this->set_error(array('message'=>'Account not found.'));
    	}
    }
    return $this->compose_result();
	}

	public function delete($data)
	{
		$this->set_message("Delete Account");

  	$this->form_validation->set_rules('usr_id', 'User', 'trim');
    $this->form_validation->set_rules('usr_email', 'Email', 'trim|required|valid_email');
    $this->form_validation->set_rules('usr_password', 'Password', 'trim|required');
    $this->form_validation->set_rules('usi_reason', 'Reason', 'trim|required');
    $this->form_validation->set_rules('usi_comment', 'Suggestion', 'trim');
    $this->form_validation->set_rules('usr_password', "Password", "trim|required");

		$error = $this->run_validators();

    if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
    	$this->db->where('usr_id', $data['usr_id']);
    	$users = $this->db->get('user');

    	if($users->num_rows() > 0){
    		$user = $users->row();

    		if(($user->usr_email == $data['usr_email']) && ($user->usr_password == md5($data['usr_password']))){
    			$usr_id = $user->usr_id; 

    			$user_inactive['usr_id'] = $usr_id;
		    	$user_inactive['usi_reason'] = $data['usi_reason'];
		    	$user_inactive['usi_comment'] = $data['usi_comment'];
		    	$user_inactive['usi_type'] = 'delete';
		    	$user_inactive['usi_status'] = 'active';
		    	$user_inactive['usi_created_date'] = format_mysql_datetime();

		    	$this->db->insert('user_inactive', $user_inactive);

    			$this->db->where('usr_id', $usr_id);
					$this->db->delete('user');

					//DELETE IMAGES
					$this->db->where('image.usr_id', $usr_id);
					$images = $this->db->get('image');

					if($images->num_rows() > 0){
						foreach($images->result() as $image){
							$this->db->where('img_id', $image->img_id);
							$this->db->delete('image');
						}
					}

					//DELETE INFO
					$this->db->where('info.usr_id', $usr_id);
					$infos = $this->db->get('info');

					if($infos->num_rows() > 0){
						foreach($infos->result() as $info){
							$this->db->where('inf_id', $info->inf_id);
							$this->db->delete('info');
						}
					}

					//DELETE PREFERENCE
					$this->db->where('preference.usr_id', $usr_id);
					$preferences = $this->db->get('preference');

					if($preferences->num_rows() > 0){
						foreach($preferences->result() as $preference){
							$this->db->where('prf_id', $preference->prf_id);
							$this->db->delete('preference');
						}
					}

					//DELETE CONVERSATION
					$this->db->where('conversation.cnv_user_1', $usr_id);
					$this->db->or_where('conversation.cnv_user_2', $usr_id);
					$conversations = $this->db->get('conversation');

					if($conversations->num_rows() > 0){
						foreach($conversations->result() as $conversation){
							$this->db->where('cnv_id', $conversation->cnv_id);
							$this->db->delete('conversation');
						}
					}

					//DELETE MESSAGE
					$this->db->where('message.usr_id', $usr_id);
					$this->db->or_where('message.msg_user', $usr_id);
					$messages = $this->db->get('message');

					if($messages->num_rows() > 0){
						foreach($messages->result() as $message){
							$this->db->where('msg_id', $message->msg_id);
							$this->db->delete('message');
						}
					}

					//DELETE USER_LIKE
					$this->db->where('user_like.usr_id', $usr_id);
					$this->db->or_where('user_like.usl_user', $usr_id);
					$user_likes = $this->db->get('user_like');

					if($user_likes->num_rows() > 0){
						foreach($user_likes->result() as $user_like){
							$this->db->where('usl_id', $user_like->usl_id);
							$this->db->delete('user_like');
						}
					}

					//DELETE BLOCK
					$this->db->where('block.usr_id', $usr_id);
					$this->db->or_where('block.blo_user', $usr_id);
					$blocks = $this->db->get('block');

					if($blocks->num_rows() > 0){
						foreach($blocks->result() as $block){
							$this->db->where('blo_id', $block->blo_id);
							$this->db->delete('block');
						}
					}

					//DELETE NOTIFICATION
					$this->db->where('notification.usr_id', $usr_id);
					$notifications = $this->db->get('notification');

					if($notifications->num_rows() > 0){
						foreach($notifications->result() as $notification){
							$this->db->where('not_id', $notification->not_id);
							$this->db->delete('notification');
						}
					}

					//DELETE PRIVACY
					$this->db->where('privacy.usr_id', $usr_id);
					$privacies = $this->db->get('privacy');

					if($privacies->num_rows() > 0){
						foreach($privacies->result() as $privacy){
							$this->db->where('pri_id', $privacy->pri_id);
							$this->db->delete('privacy');
						}
					}

					//DELETE POST
					$this->db->where('post.usr_id', $usr_id);
					$posts = $this->db->get('post');

					if($posts->num_rows() > 0){
						foreach($posts->result() as $post){

							$this->db->where('post_like.pos_id', $post->pos_id);
							$post_likes = $this->db->get('post_like');

							if($post_likes->num_rows() > 0){
								foreach($post_likes->result() as $post_like){
									$this->db->where('pol_id', $post_like->pol_id);
									$this->db->delete('post_like');
								}
							}

							$this->db->where('flag.pos_id', $post->pos_id);
							$flags = $this->db->get('flag');

							if($flags->num_rows() > 0){
								foreach($flags->result() as $flag){
									$this->db->where('flg_id', $flag->flg_id);
									$this->db->delete('flag');
								}
							}

							$this->db->where('pos_id', $post->pos_id);
							$this->db->delete('post');
						}
					}

					//DELETE POST_LIKE
					$this->db->where('post_like.usr_id', $usr_id);
					$post_likes = $this->db->get('post_like');

					if($post_likes->num_rows() > 0){
						foreach($post_likes->result() as $post_like){
							$this->db->where('pol_id', $post_like->pol_id);
							$this->db->delete('post_like');
						}
					}

					//DELETE FLAG
					$this->db->where('flag.usr_id', $usr_id);
					$flags = $this->db->get('flag');

					if($flags->num_rows() > 0){
						foreach($flags->result() as $flag){
							$this->db->where('flg_id', $flag->flg_id);
							$this->db->delete('flag');
						}
					}

					//DELETE SEARCH
					$this->db->where('search.usr_id', $usr_id);
					$searches = $this->db->get('search');

					if($searches->num_rows() > 0){
						foreach($searches->result() as $search){
							$this->db->where('sch_id', $search->sch_id);
							$this->db->delete('search');
						}
					}

					//DELETE VIEW
					$this->db->where('view.usr_id', $usr_id);
					$this->db->or_where('view.vie_user', $usr_id);
					$views = $this->db->get('view');

					if($views->num_rows() > 0){
						foreach($views->result() as $view){
							$this->db->where('vie_id', $view->vie_id);
							$this->db->delete('view');
						}
					}

					//DELETE SUBSCRIPTION
					$this->db->where('subscription.usr_id', $usr_id);
					$subscriptions = $this->db->get('subscription');

					if($subscriptions->num_rows() > 0){
						foreach($subscriptions->result() as $subscription){
							$this->db->where('sub_id', $subscription->sub_id);
							$this->db->delete('subscription');
						}
					}

					//DELETE VOUCHER_XREF
					$this->db->where('voucher_xref.usr_id', $usr_id);
					$voucher_xrefs = $this->db->get('voucher_xref');

					if($voucher_xrefs->num_rows() > 0){
						foreach($voucher_xrefs->result() as $voucher_xref){
							$this->db->where('vcx_id', $voucher_xref->vcx_id);
							$this->db->delete('voucher_xref');
						}
					}
					$this->set_data(array('message'=>'Accound deleted.'), true);
    		}else{
    			$this->set_data(array('error' => 'Invalid user credentials.'), 200);
    		}
    	}else{
    		$this->set_error(array('message'=>'Account not found.'));
    	}
    }
    return $this->compose_result();
	}

	public function preference($data)
	{
		$this->set_message("Get Preference");

    $error = $this->run_validators();

    if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
    	$this->db->select('usr_interested, usr_interested_age_from, usr_interested_age_to, usr_interested_country');
    	$this->db->where('usr_id', $data);
    	$user = $this->db->get('user');

    	if($user->num_rows() > 0){
    		$user = $user->row();

  			$this->set_data($user, true);
    	}else{
    		$this->set_error(array('message'=>'Account not found.'));
    	}
    }
    return $this->compose_result();
	}

	public function update_preference($data)
	{
		$this->set_message("Update Preference");

    $this->form_validation->set_rules("usr_interested", "Interested in", "trim|required");
    $this->form_validation->set_rules("usr_interested_age_from", "Age from", "trim|required");
    $this->form_validation->set_rules("usr_interested_age_to", "Age to", "trim|required");
    $this->form_validation->set_rules("usr_interested_country", "Country", "trim|required");

    $error = $this->run_validators();

    if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
    	$this->db->where('usr_id', $data['usr_id']);
    	$user = $this->db->get('user');

    	if($user->num_rows() > 0){
    		$user = $user->row();

  			$user->usr_interested = $data['usr_interested'];
  			$user->usr_interested_age_from = $data['usr_interested_age_from'];
  			$user->usr_interested_age_to = $data['usr_interested_age_to'];

  			$this->db->where('cnt_id', $data['usr_interested_country']);
  			$countries = $this->db->get('country');

  			if($countries->num_rows() > 0){
  				$user->usr_interested_country = $data['usr_interested_country'];
  			}

  			$user->usr_modified_date = format_mysql_datetime();

  			$this->db->where('usr_id', $user->usr_id);
  			$this->db->update('user', $user);

  			$this->set_data($user, true);
    	}else{
    		$this->set_error(array('message'=>'Account not found.'));
    	}
    }
    return $this->compose_result();
	}

	public function account($data)
	{
		$this->set_message("Update Account");

    $this->form_validation->set_rules("usr_birthdate", "Birth Date", "trim|required");
    $this->form_validation->set_rules("usr_birthmonth", "Birth Month", "trim|required");
    $this->form_validation->set_rules("usr_birthyear", "Birth Year", "trim|required");
    $this->form_validation->set_rules("usr_first_name", "First Name", "trim|required|max_length[100]");
    $this->form_validation->set_rules("usr_last_name", "Last Name", "trim|required|max_length[100]");
    $this->form_validation->set_rules("src_id", "Heard From", "trim");

    $error = $this->run_validators();

    if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
    	$this->db->where('usr_id', $data['usr_id']);
    	$user = $this->db->get('user');

    	if($user->num_rows() > 0){
    		$user = $user->row();

  			$user->usr_birthdate = $data['usr_birthdate'];
  			$user->usr_birthmonth = $data['usr_birthmonth'];
  			$user->usr_birthyear = $data['usr_birthyear'];
  			$user->usr_first_name = $data['usr_first_name'];
  			$user->usr_last_name = $data['usr_last_name'];

  			$user->usr_birthday = $data['usr_birthyear'].'-'.str_pad($data['usr_birthmonth'], 2, '0', STR_PAD_LEFT).'-'.str_pad($data['usr_birthdate'], 2, '0', STR_PAD_LEFT);
  			if(isset($data['src_id'])){
  				$user->src_id = $data['src_id'];
  			}
  			$user->usr_modified_date = format_mysql_datetime();

  			$this->db->where('usr_id', $user->usr_id);
  			$this->db->update('user', $user);

  			$this->set_data($user, true);
    	}else{
    		$this->set_error(array('message'=>'Account not found.'));
    	}
    }
    return $this->compose_result();
	}	

	public function password($data)
	{
		$this->set_message("Update Password");

    $this->form_validation->set_rules('usr_password', 'Current Password', 'trim|required');
    $this->form_validation->set_rules("usr_passwordn", "New Password", "trim|required");

    $error = $this->run_validators();

    if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
    	$this->db->where('usr_id', $data['usr_id']);
    	$users = $this->db->get('user');

    	if($users->num_rows() > 0){
    		$user = $users->row();

    		if($user->usr_password == md5($data['usr_password'])){
    			$user->usr_password = md5($data['usr_passwordn']);
    			$user->usr_modified_date = format_mysql_datetime();

    			$this->db->where('usr_id', $user->usr_id);
    			$this->db->update('user', $user);

    			$this->set_data($user, true);
    		}else{
    			$this->set_error(array('message'=>'Incorrect password.'));
    		}
    	}else{
    		$this->set_error(array('message'=>'Account not found.'));
    	}
    }
    return $this->compose_result();
	}

	public function facebook($data)
	{

		$this->set_message("Connect Facebook Account");
		$this->db->where('usa_email', $data['fb_email']);
  	$query = $this->db->get('user_accounts');
  	if($query->num_rows() > 0){
  		$this->set_error(array('message'=>'This facebook account is already connected with another user.'));
  	}else{
  		$usa = array();
  		$usa['usa_type'] = 'facebook';
  		$usa['usr_id'] = $data['usr_id'];
  		$usa['usa_email'] = $data['fb_email'];
  		$usa['usa_first_name'] = $data['first_name'];
  		$usa['usa_last_name'] = $data['last_name'];
  		$usa['usa_birthday'] = $data['fb_birthday'];
  		if(isset($data['friend_count']) && $data['friend_count'] != 0) {
  			$usa['usa_friend_count'] = $data['friend_count'];
  		}
  		else {
  			$usa['usa_friend_count'] = 0;
  		}
  		if(isset($data['photo_count']) && $data['photo_count'] != 0) {
  			$usa['usa_photo_count'] = $data['photo_count'];
  		}
  		else {
  			$usa['usa_photo_count'] = 0;
  		}
  		$usa['usa_image'] = $data['fb_image'];
  		$usa['usa_access_token'] = $data['access_token'];
			$usa['usa_access_id'] = $data['fb_id'];
  		$usa['usa_date_created'] = format_mysql_datetime();
  		$this->db->insert('user_accounts', $usa);
  		$id = $this->db->insert_id();
  		$this->db->where('usa_id', $id);
    	$result = $this->db->get('user_accounts');
  		if($data['friend_count'] >= 100 && $data['photo_count'] >= 20) {
  			$user = array();
    		$user['usr_social_verified'] = 'yes';
	    	$this->db->where('usr_id', $data['usr_id']);
	    	$this->db->update('user', $user);
    	}
			$this->set_data($result->row(), true);
  	}
    return $this->compose_result();
	}

	public function profile($data)
	{
		$this->set_message("View Profile");

		if(isset($data['usr_token_id'])){
			$this->db->where('usr_token_id', $data['usr_token_id']);
			$this->db->where('usr_status', 'approved');
			$users = $this->db->get('user');

			if($users->num_rows() > 0){
				$user = $users->row();
				$this->db->where("((`block`.`usr_id` = ".$data['usr_id']." AND `blo_user` = ".$user->usr_id.") OR (`block`.`usr_id` = ".$user->usr_id." AND `blo_user` = ".$data['usr_id']."))AND `blo_status` = 'active'");
				$blocks = $this->db->get('block');

				if($blocks->num_rows() > 0){
					$this->set_error(array('message'=>'Account not found.'));
				}else{
					// $this->db->join("city", "city.cty_id = user.cty_id", "left outer");
					// $this->db->join("region", "region.reg_id = city.reg_id", "left outer");
					// $this->db->join("country", "country.cnt_id = region.cnt_id", "left outer");
					$this->db->join("source", "source.src_id = user.src_id", "left outer");
					$this->db->where('user.usr_id', $user->usr_id);
					$users = $this->db->get('user');

					$user = $users->row();

					if($data['usr_id'] != $user->usr_id){
						$view = array();
						$view['usr_id'] = $data['usr_id'];
						$view['vie_user'] = $user->usr_id;
						$view['vie_date_created'] = format_mysql_datetime();

						$this->db->insert('view', $view);
					}
					
					$this->db->where('view.usr_id', $data['usr_id']);
					$this->db->where('vie_user', $user->usr_id);
					$this->db->like('vie_date_created', format_mysql_date());
					$views = $this->db->get('view');

					if($views->num_rows() == 1){

						$this->db->where('usr_id', $user->usr_id);
		        $notifications = $this->db->get('notification');
		        $notification = $notifications->row();

		        if($notification->not_profile == 'yes'){
		        	$this->db->select('usr_id, usr_email');
		        	$this->db->where('usr_id', $user->usr_id);
		        	$users = $this->db->get('user');
		        	$receiver = $users->row();

		        	if(emailIntercept($receiver->usr_email)){
			        	$this->db->select('user.usr_id, usr_screen_name, usr_token_id, user.img_id, img_image');
			        	$this->db->join("image", "image.img_id = user.img_id", "left outer");
			        	$this->db->where('user.usr_id', $data['usr_id']);
			        	$users = $this->db->get('user');
			        	$sender = $users->row();

			        	$params['mail_to'] = $receiver->usr_email;
			        	$params['cc'] = '';
			        	$params['bcc'] = '';
								$params['subject'] = "Your Profile has been viewed";

								$content = array();
								$content['receiver'] = $receiver;
								$content['sender'] = $sender;

								$params['content'] = $this->load->view('email/view_user', $content, true);
										
								$email = $this->email->send_mail($params,'email', 'template');
		        	}
		        }
					}

					$this->set_data($user, true);
				}
			}else{
				$this->set_error(array('message'=>'Account not found.'));
			}
		}else{
			// $this->db->join("city", "city.cty_id = user.cty_id", "left outer");
			// $this->db->join("region", "region.reg_id = city.reg_id", "left outer");
			// $this->db->join("country", "country.cnt_id = region.cnt_id", "left outer");
			$this->db->join("source", "source.src_id = user.src_id", "left outer");
			$this->db->where('usr_status', 'approved');
			$this->db->where('user.usr_id', $data['usr_id']);
			
			$users = $this->db->get('user');

			if($users->num_rows() > 0){
				$user = $users->row();

				$this->set_data($user, true);
			}else{
				$this->set_error(array('message'=>'Account not found.'));
			}
		}
		return $this->compose_result();
	}

	public function additional($response, $usr_id, $viewer=0)
	{
		$this->db->where('info.usr_id', $usr_id);
  	$info = $this->db->get('info');

  	$this->set_data($info->result(), true);

  	$this->db->select('prf_id, preference.que_id, prf_content, que_question, que_characters, que_type, que_category');
  	$this->db->join("question", "question.que_id = preference.que_id");
  	$this->db->where('preference.usr_id', $usr_id);
  	$preference = $this->db->get('preference');

  	$this->set_data($preference->result(), true);

  	$this->db->select('img_id, img_image, img_order, img_source');
  	$this->db->where('image.usr_id', $usr_id);
  	$this->db->where('img_status', 'active');
  	$images = $this->db->get('image');

  	if($images->num_rows() > 0){
  		$image = $images->result();
  	} else{
  		$image = array();
  		$image['message'] = 'No images found.';
  	}

  	$this->set_data($image, true);

  	$this->db->where('post.usr_id', $usr_id);
  	$this->db->where('pos_status', 'active');
  	$posts = $this->db->get('post');

  	if($posts->num_rows() > 0){
  		if($viewer != 0){
		  	foreach($posts->result() as $post){
		  		$flg = array();
 					$flg['flg_id'] = false;

		  		$this->db->where('flag.pos_id', $post->pos_id);
		  		$this->db->where('flag.usr_id', $viewer);
		  		$flags = $this->db->get('flag');

		  		if($flags->num_rows() > 0){
		  			$flag = $flags->row();

		  			$flg['flg_id'] = $flag->flg_id;
		  			$flg['flg_status'] = $flag->flg_status;
		  		}

		  		$post->flag = $flg;

		  		$pol = array();
		  		$pol['pol_id'] = false;

		  		$this->db->where('post_like.pos_id', $post->pos_id);
		  		$this->db->where('post_like.usr_id', $viewer);
		  		$post_likes = $this->db->get('post_like');

		  		if($post_likes->num_rows() > 0){
		  			$post_like = $post_likes->row();

		  			$pol['pol_id'] = $post_like->pol_id;
		  			$pol['pol_status'] = $post_like->pol_status;
		  		}

		  		$post->like = $pol;
		  	}
		  }


	  	$post = $posts->result();
	  	//ADD POST_LIKE INFO
  	}else{
	  	$post = array();
	  	$post['message'] = 'No posts found.';
 		}

  	$this->set_data($post, true);
 		
 		$usl = array();
 		$usl['usl_id'] = false;

 		if($viewer != 0){
			$this->db->where('user_like.usr_id', $viewer);
			$this->db->where('usl_user', $usr_id);
			$user_likes = $this->db->get('user_like');

			if($user_likes->num_rows() > 0){
				$user_like = $user_likes->row();
				
				$usl['usl_id'] = $user_like->usl_id;
				$usl['usl_status'] = $user_like->usl_status;
			}
		}

		$this->set_data($usl, true);

		$fol = array();
 		$fol['fol_id'] = false;

 		if($viewer != 0){
			$this->db->where('follow.usr_id', $viewer);
			$this->db->where('fol_user', $usr_id);
			$follows = $this->db->get('follow');

			if($follows->num_rows() > 0){
				$follow = $follows->row();
				
				$fol['fol_id'] = $follow->fol_id;
				$fol['fol_status'] = $follow->fol_status;
			}
		}

		$this->set_data($fol, true);
  
  	return $this->compose_result();
	}

	public function edit($data)
	{
		$this->set_message("Update User");
		$this->form_validation->set_rules('usr_id', 'User', 'trim');
		$this->form_validation->set_rules("usr_screen_name", "Screen Name", "trim|max_length[100]");
		$this->form_validation->set_rules("usr_birthdate", "Birth date", "trim");
		$this->form_validation->set_rules("usr_birthmonth", "Birth month", "trim");
		$this->form_validation->set_rules("usr_birthyear", "Birth year", "trim");
		$this->form_validation->set_rules("cnt_id", "Country", "trim|integer|max_length[11]");
		$this->form_validation->set_rules("usr_gender", "Gender", "trim");
		$this->form_validation->set_rules("usr_location", "Location", "trim");
		$this->form_validation->set_rules("usr_description", "Description", "trim");
		$error = $this->run_validators();

	    if (!is_bool($error)) {
	      	$this->set_error($error, false);
	    } else {
	    	$user = array();
	    	$user['usr_screen_name'] = $data['usr_screen_name'];

	    	$user['usr_birthday'] = $data['usr_birthyear'].'-'.str_pad($data['usr_birthmonth'], 2, '0', STR_PAD_LEFT).'-'.str_pad($data['usr_birthdate'], 2, '0', STR_PAD_LEFT);

	    	$user['usr_birthdate'] = $data['usr_birthdate'];
	    	$user['usr_birthmonth'] = $data['usr_birthmonth'];
	    	$user['usr_birthyear'] = $data['usr_birthyear'];
	    	// $user['cty_id'] = $data['cty_id'];
	    	$user['usr_gender'] = $data['usr_gender'];
	    	$user['usr_description'] = $data['usr_description'];
	    	$user['usr_modified_date'] = format_mysql_datetime();
	    	
	    	//set location
	    	if(isset($data['country_short_name'])) {
		    	$this->db->where('cnt_short_name', $data['country_short_name']);
					$countries = $this->db->get('country');
					if($countries->num_rows() > 0){
						$data['cnt_id'] = $countries->row()->cnt_id;
					}else{
						$country = array();
						$country['cnt_name'] = $data['country_long_name'];
						$country['cnt_short_name'] = $data['country_short_name'];

						$this->db->insert('country', $country);
						$data['cnt_id'] = $this->db->insert_id();
					}
		    	$location = array();
					$location['usr_id'] = $data['usr_id'];
					$location['ulc_lat'] = $data['lat'];
					$location['ulc_lng'] = $data['lng'];
					if(isset($data['city'])){
						$location['ulc_city'] = $data['city'];
						$user['usr_location'] = $data['city'].', '.$data['country_short_name'];
					}else{
						$user['usr_location'] = $data['state'].', '.$data['country_short_name'];
						$location['ulc_state'] = $data['state'];
					}
					if(isset($data['street_number'])) {
						$location['ulc_street_number'] = $data['street_number'];
					}
					if(isset($data['street'])) {
						$location['ulc_street'] = $data['street'];
					}
					$location['ulc_country'] = $data['country_long_name'];
					$location['ulc_date_added'] = format_mysql_datetime();
					if(isset($data['ulc_id'])) {
						$this->db->where('ulc_id', $data['ulc_id']);
	    			$this->db->update('user_location', $location);
					}
					else {
						$this->db->insert('user_location', $location);
						$user['ulc_id'] = $this->db->insert_id();
					}
	    		
	    		$user['cnt_id'] = $data['cnt_id'];
					//END SET USER_LOCATION
	    	}



	    	$this->db->where('usr_id', $data['usr_id']);
	    	$this->db->update('user', $user);

	   //  	$this->db->join("city", "city.cty_id = user.cty_id", "left outer");
				// $this->db->join("region", "region.reg_id = city.reg_id", "left outer");
				// $this->db->join("country", "country.cnt_id = region.cnt_id", "left outer");
				$this->db->join("source", "source.src_id = user.src_id", "left outer");
				$this->db->where('user.usr_id', $data['usr_id']);
				$users = $this->db->get('user');

	    	$this->set_data($users->row(), true);
	    }
	    return $this->compose_result();
	}

	public function make_profile($data)
	{
		$this->set_message("Update User");
		$this->form_validation->set_rules('usr_id', 'User', 'trim');
		$this->form_validation->set_rules("img_id", "Profile Picture", "trim|required");
		$error = $this->run_validators();

	    if (!is_bool($error)) {
	      	$this->set_error($error, false);
	    } else {
	    	$user = array();
	    	$user['img_id'] = $data['img_id'];
    		$user['usr_modified_date'] = format_mysql_datetime();

	    	$this->db->where('usr_id', $data['usr_id']);
	    	$this->db->update('user', $user);

	    	$this->db->where('usr_id', $data['usr_id']);
				$users = $this->db->get('user');

	    	$this->set_data($users->row(), true);
	    }
	    return $this->compose_result();
	}

	public function edit_description($data)
	{
		$this->set_message("Update User");
		$this->form_validation->set_rules('usr_id', 'User', 'trim');
		$this->form_validation->set_rules("usr_description", "Description", "trim|required");
		$error = $this->run_validators();

    if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
    	$user = array();
    	$user['usr_description'] = $data['usr_description'];
    	$user['usr_modified_date'] = format_mysql_datetime();

    	$this->db->where('usr_id', $data['usr_id']);
    	$this->db->update('user', $user);

   //  	$this->db->join("city", "city.cty_id = user.cty_id", "left outer");
			// $this->db->join("region", "region.reg_id = city.reg_id", "left outer");
			// $this->db->join("country", "country.cnt_id = region.cnt_id", "left outer");
			$this->db->join("source", "source.src_id = user.src_id", "left outer");
			$this->db->where('user.usr_id', $data['usr_id']);
			$users = $this->db->get('user');

    	$this->set_data($users->row(), true);
    }
    return $this->compose_result();
	}

	public function normal_get_all($params, $order_by) {
		$this->set_message("Users");
		$this->db->join("image", "image.img_id = user.img_id", "left outer");

		return parent::get_all($params, $order_by);
	}

	public function search($data, $params = array(), $order_by = array())
	{
		$this->set_message("Searches");
		$this->form_validation->set_rules("usr_id", "User", "trim");
		$this->form_validation->set_rules("sch_id", "Search ID", "trim");

		$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
  		return $this->compose_result();
    } else {
    	$user = $this->user_model->get_one_simple($data['usr_id']);

    	if(isset($data['sch_id'])){
    		$this->db->where('search.usr_id', $data['usr_id']);
    		$this->db->where('sch_id', $data['sch_id']);
    		$this->db->where('sch_status', 'active');
    		$searches = $this->db->get('search');

    		if($searches->num_rows() > 0){
    			$search = $searches->row();
    			$temp = json_decode($search->sch_content, true);
    		}
    	}else{
    		if(isset($data['sch_content'])){
    			$temp = json_decode($data['sch_content']);
    		}
    	}

    	// $blocked_ids = blocked_ids($data['usr_id']);
    	// array_push($blocked_ids, $data['usr_id']);
    	// search by distance
			if(isset($data["lat"]) || isset($data["long"])) {
    		$this->db->select(", (acos(sin(" . deg2rad($data["lat"]) . ") * sin(radians(`user_location`.`ulc_lat`)) + cos(" . deg2rad($data["lat"]) . ") * cos(radians(`user_location`.`ulc_lat`)) * cos(" . deg2rad($data["long"]) . " - radians(`user_location`.`ulc_lng`))) * 6371) as distance  ");
    	}
  		$this->db->select("user.img_id, IF(`user`.`img_id` IS NULL, 0, 1) as img_id_sort ", false);
    	$this->db->select('user.usr_id, usr_type, usr_screen_name, usr_gender, usr_birthyear, usr_birthmonth, usr_birthdate, usr_birthday, user.cnt_id, usr_description, usr_approved_date, usr_social_verified, usr_last_login, usr_location, usr_token_id, inf_height, inf_height_unit, inf_ethnicity, inf_body_type, inf_relationship, inf_children, inf_language, inf_education, inf_smoke, inf_drink, inf_spending_habits, user.ulc_id, ulc_lat, ulc_lng');

    	if($user->usr_type == 'sugar'){
    		$type = 'flame';
    	}else{
    		$type = 'sugar';
    	}

			if(isset($data["lat"]) || isset($data["long"])) {
	    	if($data["distance_to"] >= 10001) {
	    		$this->db->having("distance >= " . $data["distance_from"] . " ");
	    	}
	    	else {
	    		$this->db->having("distance >= " . $data["distance_from"] . " ");
	    		$this->db->having("distance <= " . $data["distance_to"] . " ");
	    	}
			}
    	if(isset($data['source'])) {
    		$source = $data['source'];
    		unset($data['source']);
	    	if($source == 'facebook') {
	    		$this->db->where('usr_social_verified', 'yes');
	    	}

	    	if($source == 'nearest') {
		  		$this->db->order_by("img_id_sort", "desc");   
		  		$this->db->order_by("distance");   
		  	}

		  	if($source == 'recent') {
		  		$this->db->order_by("img_id_sort", "desc");   
		  		$this->db->order_by("usr_last_login", "desc");   
		  	}

		  	if($source == 'facebook' || $source == '') {
		  		$this->db->order_by("img_id_sort", "desc");   
		  	}
    	}


    	$this->db->where('user.usr_type', $type);
    	$this->db->where('user.usr_status', 'approved');


    	if(isset($temp) && !empty($temp)){
    		if(isset($temp->usr_screen_name) && $temp->usr_screen_name != ''){
    			$this->db->like('user.usr_screen_name', $temp->usr_screen_name);
    		}

    		if(isset($temp->usr_gender) && count($temp->usr_gender) > 0){
    			$this->db->where_in('user.usr_gender', $temp->usr_gender);
    		}else{
    			$this->db->where('user.usr_gender', $temp['usr_gender'][0]);
    		}

    		if((isset($temp->usr_interested_age_from) && $temp->usr_interested_age_from != '') && (isset($temp->usr_interested_age_to) && $temp->usr_interested_age_to != '')){
    			$age_from = $temp->usr_interested_age_from;
					$age_to = $temp->usr_interested_age_to + 1;
			
			    $this->db->where('user.usr_birthday <=', date("Y-m-d", strtotime("-$age_from year", time())));
			    $this->db->where('user.usr_birthday >=', date("Y-m-d", strtotime("-$age_to year", time())));
    		}

    		// if(isset($temp->usr_interested_country) && $temp->usr_interested_country != 0){
    		// 	$this->db->where('user.cnt_id', $temp->usr_interested_country);
    		// }

    		if((isset($temp->inf_height_from) && $temp->inf_height_from != '') && (isset($temp->inf_height_to) && $temp->inf_height_to != '')){
    			$this->db->where('info.inf_height >=', $temp->inf_height_from);
    			$this->db->where('info.inf_height <=', $temp->inf_height_to);
    			$this->db->where('info.inf_height_unit', 'cm');
    		}

    		if(isset($temp->inf_ethnicity) && count($temp->inf_ethnicity) > 0){
    			$this->db->where_in('info.inf_ethnicity', $temp->inf_ethnicity . ' ');
    		}

    		if(isset($temp->inf_relationship) && count($temp->inf_relationship) > 0){
    			$this->db->where_in('info.inf_relationship', $temp->inf_relationship . ' ');
    		}

    		if(isset($temp->inf_body_type) && count($temp->inf_body_type) > 0){
    			$this->db->where_in('info.inf_body_type', $temp->inf_body_type . ' ');
    		}

    		if(isset($temp->inf_children) && count($temp->inf_children) > 0){
    			$this->db->where_in('info.inf_children', $temp->inf_children . ' ');
    		}

    		if(isset($temp->inf_language) && count($temp->inf_language) > 0){
    			$this->db->where_in('info.inf_language', $temp->inf_language . ' ');
    		}

    		if(isset($temp->inf_education) && count($temp->inf_education) > 0){
    			$this->db->where_in('info.inf_education', $temp->inf_education . ' ');
    		}

    		if(isset($temp->inf_smoke) && count($temp->inf_smoke) > 0){
    			$this->db->where_in('info.inf_smoke', $temp->inf_smoke . ' ');
    		}

    		if(isset($temp->inf_drink) && count($temp->inf_drink) > 0){
    			$this->db->where_in('info.inf_drink', $temp->inf_drink . ' ');
    		}

    		if(isset($temp->inf_spending_habits) && $temp->inf_spending_habits != ''){
    			$this->db->where('info.inf_spending_habits <=', $temp->inf_spending_habits . ' ');
    		}

    		if(isset($temp->inf_net_worth) && $temp->inf_net_worth != ''){
    			$this->db->where('info.inf_net_worth <=', $temp->inf_net_worth . ' ');
    		}

    		if(isset($temp->inf_yearly_income) && $temp->inf_yearly_income != ''){
    			$this->db->where('info.inf_yearly_income <=', $temp->inf_yearly_income . ' ');
    		}
    	}else{
    		if($user->usr_interested != 'both'){
    			$this->db->where('user.usr_gender', $user->usr_interested . ' ');
    		}

  			$age_from = $user->usr_interested_age_from;
				$age_to = $user->usr_interested_age_to + 1;
		
		    $this->db->where('user.usr_birthday <=', date("Y-m-d", strtotime("-$age_from year", time())));
		    $this->db->where('user.usr_birthday >=', date("Y-m-d", strtotime("-$age_to year", time())));

		    // if($user->usr_interested_country != 0){
		    // 	$this->db->where('user.cnt_id', $user->usr_interested_country);
		    // }
    	}
    	// $this->db->where_not_in('user.usr_id', $blocked_ids);

      $this->db->where("`user`.`usr_id` !=", $user->usr_id);

      $this->db->where("(user.usr_id not in (select (CASE usr_id when ".$data['usr_id']." then blo_user else usr_id end) as usr_id from block where (blo_user = ".$data['usr_id']." AND blo_status = 'active') or (usr_id = ".$data['usr_id']." AND blo_status = 'active')))");

      $this->db->where("((`privacy`.`pri_profile` = 'no') OR (`privacy`.`pri_profile` = 'yes' AND ((`user`.`usr_interested` = 'both' OR `user`.`usr_interested` = '".$user->usr_gender."') AND (`user`.`usr_interested_age_from` <= DATEDIFF(CURRENT_DATE, STR_TO_DATE('".$user->usr_birthday."', '%Y-%m-%d'))/365) AND (`user`.`usr_interested_age_to` >= DATEDIFF(CURRENT_DATE, STR_TO_DATE('".$user->usr_birthday."', '%Y-%m-%d'))/365))))");

      $this->db->join("privacy", "privacy.usr_id = user.usr_id", "left outer");
      $this->db->join("info", "info.usr_id = user.usr_id", "left outer");
      $this->db->join("user_location", "user_location.ulc_id = user.ulc_id", "left outer");
      // $this->db->join("city", "city.cty_id = user.cty_id", "left outer");
      // $this->db->join("region", "region.reg_id = city.reg_id", "left outer");
      // $this->db->join("country", "country.cnt_id = region.cnt_id", "left outer");

      $this->db->group_by('user.usr_id');
  		// $users = $this->db->get('user');
  		return parent::get_all($params, $order_by);

  		// if($users->num_rows() > 0){
  		// 	$this->set_data($users->result(), true);
  		// }else{
  		// 	$this->set_error(array('message'=>'No users found.'));
  		// }
    }
    // return $this->compose_result();
	}
	
	public function email($data)
  {
    $this->set_message("User");

    $this->form_validation->set_rules('email', 'User Email', 'trim');

    $error = $this->run_validators();

    if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
      $this->db->where('user.usr_email', $data['email']);
			$this->db->join("image", "image.img_id = user.img_id", "left outer");
      $query = $this->db->get('user');
      if($query->num_rows() > 0)
      {
        $user = $query->row();

				//GET INFO_ID
				$this->db->where('info.usr_id', $user->usr_id);
				$infos = $this->db->get('info');

				if($infos->num_rows() > 0){
					$info = $infos->row();

					$user->inf_id = $info->inf_id;
				}

				//GET PRF_IDs
				$this->db->where('preference.usr_id', $user->usr_id);
				$preferences = $this->db->get('preference');

				if($preferences->num_rows() > 0){
					$prf_ids = array();

					foreach($preferences->result() as $preference){
						array_push($prf_ids, $preference->prf_id);
					}

					$user->prf_ids = $prf_ids;
				}

				$user->_message = '';
				$user->_redirect = '';
				$user->_continue = true;
				$data = array();

				if($user->usr_signup_step != 'done'){
					$data['usr_last_login'] = format_mysql_datetime();

					$this->db->where('usr_id', $user->usr_id);
					$this->db->update('user', $data); 

					$user->_redirect = $user->usr_signup_step;

					// return $user;
				} else{
					switch ($user->usr_status) {
						case 'pending':
							// $user = new stdClass();
							// $user->_message = 'Waiting for account approval.';
							$user->_message = '';
							$user->_redirect = 'search';
							$user->_continue = true;
							
							// return $user;
							break;

						case 'rejected':
							$data['usr_last_login'] = format_mysql_datetime();
							$data['usr_status'] = 'pending';
							$data['usr_modified_date'] = format_mysql_datetime();
							
							$this->db->where('usr_id', $user->usr_id);
							$this->db->update('user', $data);

							$user->_message = 'Account submission was rejected, please resubmit your profile.';
							$user->_redirect = 'photo';

							// return $user;
							break;

						case 'deactivated':
							$data['usr_last_login'] = format_mysql_datetime();
							$data['usr_status'] = 'approved';
							$data['usr_modified_date'] = format_mysql_datetime();

							$this->db->where('user_inactive.usr_id', $user->usr_id);
							$this->db->where('usi_type', 'deactivate');
							$this->db->where('usi_status', 'active');
							$user_inactives = $this->db->get('user_inactive');

							if($user_inactives->num_rows() > 0){
								foreach($user_inactives->result() as $user_inactive){
									$user_inactive->usi_status = 'archived';
									$user_inactive->usi_modified_date = format_mysql_datetime();

									$this->db->where('usi_id', $user_inactive->usi_id);
									$this->db->update('user_inactive', $user_inactive);
								}
							}
							
							$this->db->where('usr_id', $user->usr_id);
							$this->db->update('user', $data); 

							$user->_message = 'Welcome back!';
							
							// return $user;
							break;

						case 'suspended':
							$user = new stdClass();
							$user->_message = 'Account is suspended.';
							$user->_redirect = '';
							$user->_continue = false;

							// return $user;
							break;
						
						default:
							$data['usr_last_login'] = format_mysql_datetime();

							$this->db->where('usr_id', $user->usr_id);
							$this->db->update('user', $data); 

							// return $user;
							break;
					}
				}
				$this->set_data($user, true);
      }
      else
      {
        $this->set_error(array('message'=>'User ID invalid.'));
      }
    }
    return $this->compose_result();
  }
}