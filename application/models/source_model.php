<?php
// Extend Base_model instead of CI_model
class Source_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'src_id', 
			'src_name', 
			'src_status'
		);

		$searchable_fields = array('src_name', 'src_status');

		// Call the parent constructor with the table name and fields as parameters.


		parent::__construct('source', $fields, $searchable_fields, null);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}