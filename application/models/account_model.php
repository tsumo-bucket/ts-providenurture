<?php
// Extend Base_model instead of CI_model
class Account_model extends Base_model 
{	
	private $DEFAULT_PASS = 'developer1qasw23ed';
	private $img_width = 100;
	private $img_height = 100;
	private $upload_path = 'uploads/';


	public function __construct() 
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'acc_id',
			'acc_username',
			'acc_password',
			'acc_last_name',
			'acc_first_name',
			'acc_type',
			'acc_image',
			'acc_failed_login',
			'acc_status',
			'acc_token'
		);
		// Call the parent constructor with the table name and fields as parameters.
		$this->mythos->library('upload');
		$this->mythos->library('image');
		
		parent::__construct('account', $fields);
	}
	
	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function create($account)
	{
		if(!empty($_FILES['acc_image']["tmp_name"]))
		{

			$upload_data = $this->image->upload("acc_image", $this->upload_path);

			if(!isset($upload_data['error']))
			{
				$account['acc_image'] = $this->upload_path.$upload_data['file_name'];

				if($this->img_height && $this->img_width)
				{

					$thumbs = array(
						array('width'=>$this->img_width, 'height'=>$this->img_height, 'filename'=>$upload_data['raw_name'])	
					);

					$this->image->create_thumbs($account['acc_image'], $thumbs, false);
				}	
			}
			else
			{
				return array('error'=>$upload_data['error']);
			}
		} 

		$duplicateAccount = $this->get_by_username($account['acc_username']);
		if($duplicateAccount === false)
		{
			// Encrypt password
			$account['acc_password'] = md5($account['acc_password']);
			return parent::create($account);
			
		}
		else
		{
			$this->template->notification('Username already exists.', 'warning');
			return false;
		}
	}

	public function update($account)
	{
		if(!empty($_FILES['acc_image']["tmp_name"]))
		{

			$upload_data = $this->image->upload("acc_image", $this->upload_path);

			if(!isset($upload_data['error']))
			{
				$account['acc_image'] = $this->upload_path.$upload_data['file_name'];

				if($this->img_height && $this->img_width)
				{

					$thumbs = array(
						array('width'=>$this->img_width, 'height'=>$this->img_height, 'filename'=>$upload_data['raw_name'])	
					);

					$this->image->create_thumbs($account['acc_image'], $thumbs, false);
				}	
			}
			else
			{
				return array('error'=>$upload_data['error']);
			}
		} 

		return parent::update($account);
	}

	public function get_by_username($acc_username)
	{
		$this->db->where('acc_username', $acc_username);
		$query = $this->db->get($this->table); // Use $this->table to get the table name
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}
	
	public function authenticate($acc_username, $acc_password)
	{
		$this->db->where('acc_username', $acc_username);
		$this->db->where('acc_password', md5($acc_password));
		$query = $this->db->get($this->table); 
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}
	
	public function using_default_pass($acc_username)
	{
		$result = $this->account_model->authenticate($acc_username, $this->DEFAULT_PASS);
		return ($result !== false);
	}
	
	public function change_password($acc_username, $acc_password)
	{
		$account = $this->get_by_username($acc_username);
		if($account !== false)
		{
			$data = array();
			$data['acc_id'] = $account->acc_id;
			$data['acc_password'] = md5($acc_password);
			
			$result = $this->update($data);
			return ($result == 0);
		}
		else
		{
			return false;
		}
	}
	
	public function failed_login($acc_username)
	{
		$account = $this->get_by_username($acc_username);
		if($account !== false)
		{
			$data = array();
			$data['acc_id'] = $account->acc_id;
			$data['acc_failed_login'] = $account->acc_failed_login + 1;
			
			$result = $this->update($data);
			return ($result == 0);
		}
		else
		{
			return false;
		}
	}
	
	public function failed_login_reset($acc_username)
	{
		$account = $this->get_by_username($acc_username);
		if($account !== false)
		{
			$data = array();
			$data['acc_id'] = $account->acc_id;
			$data['acc_failed_login'] = 0;
			
			$result = $this->update($data);
			return ($result == 0);
		}
		else
		{
			return false;
		}
	}

	public function api_authenticate($acc_token)
	{
		$this->db->where('acc_token', $acc_token);
		$query = $this->db->get($this->table); 
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}
}