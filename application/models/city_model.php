<?php
class City_model extends Base_model
{
	public function __construct()
	{
		$fields = array(
			'cty_id', 
			'reg_id', 
			'cty_name'
		);

		$searchable_fields = array('cnt_name', 'reg_name', 'cty_name');

		parent::__construct('city', $fields, $searchable_fields, null);
	}

	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		$this->db->join("region", "region.reg_id = city.reg_id");
		$this->db->join("country", "country.cnt_id = region.cnt_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("region", "region.reg_id = city.reg_id");
		$this->db->join("country", "country.cnt_id = region.cnt_id");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}