<?php
// Extend Base_model instead of CI_model
class Voucher_api_model extends Api_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'vch_id', 
			'vch_code', 
			'vch_status', 
			'vch_value', 
			'vch_valid_until', 
			'vch_date_created', 
			'vch_date_modified', 
			'vch_created_by', 
			'vch_modified_by'
		);

		parent::__construct('voucher', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of api_model.
	public function create($data, $field_list = array())
	{
		$this->set_message("Create Voucher");
		$this->form_validation->set_rules("vch_code", "Code", "trim|required|max_length[100]");
		$this->form_validation->set_rules("vch_status", "Status", "trim|required");
		$this->form_validation->set_rules("vch_value", "Value", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("vch_valid_until", "Valid Until", "trim|required|datetime");
		return parent::create($data, $field_list = array());
	}

	public function update($data, $field_list = array())
	{
		$this->set_message("Update Voucher");
		$this->form_validation->set_rules('vch_id', 'Voucher Id', 'integer|required');
		$this->form_validation->set_rules("vch_code", "Code", "trim|required|max_length[100]");
		$this->form_validation->set_rules("vch_status", "Status", "trim|required");
		$this->form_validation->set_rules("vch_value", "Value", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("vch_valid_until", "Valid Until", "trim|required|datetime");
		return parent::update($data, $field_list = array());
	}

	public function delete($id)
	{
		$this->set_message("Delete Voucher");
		return parent::delete($id);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Voucher");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Vouchers");

        return parent::get_all($params, $order_by);
	}
}