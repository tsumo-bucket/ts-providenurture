<?php
// Extend Base_model instead of CI_model
class Preference_api_model extends Api_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'prf_id', 
			'usr_id', 
			'que_id', 
			'prf_content', 
			'prf_date_created', 
			'prf_date_modified'
		);

		parent::__construct('preference', $fields);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Preference");

		return parent::get_one($id);
	}
	public function edit($data)
	{
		$this->set_message("Update Preference");
		
		$this->form_validation->set_rules('prf_id', 'Preference ID', 'trim');
		$this->form_validation->set_rules("usr_id", "User", "trim");
		$this->form_validation->set_rules("que_id", "Question", "trim|integer|max_length[11]");
		$this->form_validation->set_rules("prf_content", "Content", "trim|required");

		$error = $this->run_validators();
    
	    if (!is_bool($error)) {
	      $this->set_error($error, false);
	    } else {
	    	if(isset($data['prf_id'])){
	    		$this->db->where('prf_id', $data['prf_id']);
	    		$this->db->where('preference.usr_id', $data['usr_id']);
	    		$preferences = $this->db->get('preference');

	    		if($preferences->num_rows() > 0){
	  				$preference = array();

	  				$preference['usr_id'] = $data['usr_id'];
	  				$preference['prf_content'] = $data['prf_content'];
	  				$preference['prf_date_modified'] = format_mysql_datetime();

	  				$this->db->where('prf_id', $data['prf_id']);
	  				$this->db->update('preference', $preference);
	  				$pref = $this->db->insert_id();

	  				update_signup_status($data['usr_id']);

	  				$this->set_data($pref, true);
	    		}else{
	    			$this->set_error(array('message'=>'Preference not found.'));
	    		}
	    	}else{
    			$preference = array();

					$preference['usr_id'] = $data['usr_id'];
					$preference['que_id'] = $data['que_id'];
					$preference['prf_content'] = $data['prf_content'];
					$preference['prf_date_modified'] = format_mysql_datetime();

					$this->db->insert('preference', $preference);
					$pref = $this->db->insert_id();

					update_signup_status($data['usr_id']);

  				$this->set_data($pref, true);
    		}
   		}
    	return $this->compose_result();
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Preferences");
		$this->db->order_by("prf_id", "asc"); 

   		return parent::get_all($params, $order_by);
	}
}