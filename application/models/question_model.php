<?php
class Question_model extends Base_model
{
	public function __construct()
	{
		$fields = array(
			'que_id', 
			'que_question', 
			'que_slug', 
			'que_characters', 
			'que_type', 
			'que_category', 
			'que_status', 
			'que_date_created', 
			'que_date_modified', 
			'que_created_by', 
			'que_modified_by'
		);

		$searchable_fields = array('que_question', 'que_category', 'que_status');

		parent::__construct('question', $fields, $searchable_fields, null);
	}

	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}

	public function get_by_slug($que_slug)
	{
		$this->db->where('que_slug', $que_slug);
		$query = $this->db->get($this->table);
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function update_slug($que_id, $que_slug)
	{
		$page = $this->get_one($que_id);
		if($page !== false)
		{
			$data = array();
			$data['que_id'] = $que_id;
			$data['que_slug'] = $que_slug;
			
			return $this->update($data);
		}
		else
		{
			return 0;
		}
	}
}