<?php
// Extend Base_model instead of CI_model
class Subscription_api_model extends Api_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'sub_id', 
			'usr_id', 
			'sub_status', 
			'sub_start_date', 
			'sub_end_date', 
			'sub_date_created', 
			'sub_date_modified', 
			'sub_amount', 
			'sub_type', 
			'sub_transaction_id', 
			'vch_id'
		);

		parent::__construct('subscription', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of api_model.
	public function create($data, $field_list = array())
	{
		$this->set_message("Create Subscription");
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("sub_status", "Status", "trim|required");
		$this->form_validation->set_rules("sub_start_date", "Start Date", "trim|required|datetime");
		$this->form_validation->set_rules("sub_end_date", "End Date", "trim|required|datetime");
		$this->form_validation->set_rules("sub_amount", "Amount", "trim|required|decimal");
		$this->form_validation->set_rules("sub_type", "Type", "trim|required");
		$this->form_validation->set_rules("sub_transaction_id", "Transaction Id", "trim|required|max_length[100]");
		$this->form_validation->set_rules("vch_id", "Voucher", "trim|required|integer|max_length[11]");
		return parent::create($data, $field_list = array());
	}

	public function update($data, $field_list = array())
	{
		$this->set_message("Update Subscription");
		$this->form_validation->set_rules('sub_id', 'Subscription Id', 'integer|required');
		$this->form_validation->set_rules("usr_id", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("sub_status", "Status", "trim|required");
		$this->form_validation->set_rules("sub_start_date", "Start Date", "trim|required|datetime");
		$this->form_validation->set_rules("sub_end_date", "End Date", "trim|required|datetime");
		$this->form_validation->set_rules("sub_amount", "Amount", "trim|required|decimal");
		$this->form_validation->set_rules("sub_type", "Type", "trim|required");
		$this->form_validation->set_rules("sub_transaction_id", "Transaction Id", "trim|required|max_length[100]");
		$this->form_validation->set_rules("vch_id", "Voucher", "trim|required|integer|max_length[11]");
		return parent::update($data, $field_list = array());
	}

	public function delete($id)
	{
		$this->set_message("Delete Subscription");
		return parent::delete($id);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Subscription");
		$this->db->join("user", "user.usr_id = subscription.usr_id");
		$this->db->join("voucher", "voucher.vch_id = subscription.vch_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Subscriptions");
		$this->db->join("user", "user.usr_id = subscription.usr_id");
		$this->db->join("voucher", "voucher.vch_id = subscription.vch_id");

        return parent::get_all($params, $order_by);
	}
}