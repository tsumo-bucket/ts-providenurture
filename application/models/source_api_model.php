<?php
// Extend Base_model instead of CI_model
class Source_api_model extends Api_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'src_id', 
			'src_name', 
			'src_status'
		);

		parent::__construct('source', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of api_model.
	public function create($data, $field_list = array())
	{
		$this->set_message("Create Source");
		$this->form_validation->set_rules("src_name", "Name", "trim|required|max_length[100]");
		$this->form_validation->set_rules("src_status", "Status", "trim|required");
		return parent::create($data, $field_list = array());
	}

	public function update($data, $field_list = array())
	{
		$this->set_message("Update Source");
		$this->form_validation->set_rules('src_id', 'Source Id', 'integer|required');
		$this->form_validation->set_rules("src_name", "Name", "trim|required|max_length[100]");
		$this->form_validation->set_rules("src_status", "Status", "trim|required");
		return parent::update($data, $field_list = array());
	}

	public function delete($id)
	{
		$this->set_message("Delete Source");
		return parent::delete($id);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Source");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Sources");

        return parent::get_all($params, $order_by);
	}
}