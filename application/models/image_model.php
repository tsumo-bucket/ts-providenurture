<?php
// Extend Base_model instead of CI_model
class Image_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'img_id', 
			'usr_id', 
			'img_image', 
			'img_thumb', 
			'img_icon', 
			'img_offset_x', 
			'img_offset_y', 
			'img_order', 
			'img_status',
			'img_date_created', 
			'img_date_modified'
		);

		$searchable_fields = array('');

		// Call the parent constructor with the table name and fields as parameters.


		parent::__construct('image', $fields, $searchable_fields, null);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		// $this->db->join("user", "user.usr_id = image.usr_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		// $this->db->join("user", "user.usr_id = image.usr_id");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}