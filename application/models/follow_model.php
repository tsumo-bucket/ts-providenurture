<?php
class Follow_model extends Base_model
{
	public function __construct()
	{
		$fields = array(
			'fol_id', 
			'usr_id', 
			'fol_user', 
			'fol_status', 
			'fol_date_created', 
			'fol_date_modified'
		);

		$searchable_fields = array('');

		parent::__construct('follow', $fields, $searchable_fields, null);
	}

	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		$this->db->join("user_inactive", "user_inactive.usr_id = follow.usr_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("user_inactive", "user_inactive.usr_id = follow.usr_id");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}