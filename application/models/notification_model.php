<?php
class Notification_model extends Base_model
{
	public function __construct()
	{
		$fields = array(
			'not_id', 
			'usr_id', 
			'not_message', 
			'not_like', 
			'not_profile', 
			'not_content', 
			'not_email', 
			'not_date_created', 
			'not_date_modified'
		);

		$searchable_fields = array('');

		parent::__construct('notification', $fields, $searchable_fields, null);
	}

	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		$this->db->join("user", "user.usr_id = notification.usr_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("user", "user.usr_id = notification.usr_id");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}