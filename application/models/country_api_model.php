<?php
class Country_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'cnt_id', 
			'cnt_name',
			'cnt_short_name',
			'cnt_status'
		);

		parent::__construct('country', $fields);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Country");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Countries");

		$params['cnt_status'] = 'active';

    return parent::get_all($params, $order_by);
	}
}