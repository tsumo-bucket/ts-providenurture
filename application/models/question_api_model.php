<?php
class Question_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'que_id', 
			'que_question', 
			'que_slug', 
			'que_characters', 
			'que_type', 
			'que_category', 
			'que_status', 
			'que_date_created', 
			'que_date_modified', 
			'que_created_by', 
			'que_modified_by'
		);

		parent::__construct('question', $fields);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Question");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Questions");

    return parent::get_all($params, $order_by);
	}
}