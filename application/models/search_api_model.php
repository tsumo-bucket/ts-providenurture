<?php
class Search_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'sch_id', 
			'usr_id', 
			'sch_name', 
			'sch_content', 
			'sch_status', 
			'sch_date_created'
		);

		parent::__construct('search', $fields);
		$this->load->model('user_model');
	}

	public function save($data)
	{
		$this->set_message("Save Search");

		$this->form_validation->set_rules("usr_id", "User", "trim|integer|max_length[11]");

		$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
    } else {
    	$return = array();
    	if(isset($data['sch_content']) && !empty($data['sch_content'])){
    		$user = $this->user_model->get_one_simple($data['usr_id']);

    		if(isset($data['sch_content']['usr_gender']) && (count($data['sch_content']['usr_gender']) != 0)){
    			$__gender = $data['sch_content']['usr_gender'];
          if(in_array('male', $__gender) && in_array('female', $__gender)){
            $_gender = 'both';
          }else{
            if(in_array('male', $__gender)){
              $_gender = 'male';
            }

            if(in_array('female', $__gender)){
              $_gender = 'female';
            }
          }
    		}else{
    			$_gender = $user->usr_interested;
    		}

    		switch ($_gender) {
  				case 'both':
  					$gender = 'Both Men and Women, ';
  					break;

  				case 'male':
  					$gender = 'Men, ';
  					break;

  				case 'female':
  					$gender = 'Women, ';
  					break;
  				
  				default:
  					break;
  			}

    		if(isset($data['sch_content']['usr_interested_age_from']) && $data['sch_content']['usr_interested_age_from'] != ''){
    			$age_from = $data['sch_content']['usr_interested_age_from'];
    		}else{
    			$age_from = $user->usr_interested_age_from;
    		}

    		if(isset($data['sch_content']['usr_interested_age_to']) && $data['sch_content']['usr_interested_age_to'] != ''){
    			$age_to = $data['sch_content']['usr_interested_age_to'];
    		}else{
    			$age_to = $user->usr_interested_age_to;
    		}

    		$search = array();
    		$search['usr_id'] = $data['usr_id'];
    		$search['sch_name'] = $gender.$age_from.'-'.$age_to;
    		$search['sch_content'] = json_encode($data['sch_content']);
    		$search['sch_status'] = 'active';
    		$search['sch_date_created'] = format_mysql_datetime();

    		$this->db->insert('search', $search);

    		$return['search'] = $search;
    	}else{
    		$return['message'] = 'Nothing to save';
    	}
    	$this->set_data($return, true);
    }
   	return $this->compose_result();
	}

	public function update($data)
	{
		$this->set_message("Delete Search");
		$this->form_validation->set_rules("usr_id", "User", "trim");
		$this->form_validation->set_rules('sch_id', 'Search Id', 'integer|required');

		$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
    } else {
    	$this->db->where('search.usr_id', $data['usr_id']);
    	$this->db->where('sch_id', $data['sch_id']);
    	$this->db->where('sch_status', 'active');
    	$searches = $this->db->get('search');

    	if($searches->num_rows() > 0){
    		$sch = array();
    		$sch['sch_status'] = 'archived';

    		$this->db->where('sch_id', $data['sch_id']);
    		$this->db->update('search', $sch);

    		$return = array();
    		$return['message'] = 'Search deleted.';

    		$this->set_data($return, true);
    	}else{
    		$this->set_error(array('message'=>'Search entry not found.'));
    	}
    }
		return $this->compose_result();
	}

	public function get_all($data)
	{
		$this->set_message("List Searches");
		$this->form_validation->set_rules("usr_id", "User", "trim");

		$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
    } else {
    	$this->db->select('sch_id, sch_name, sch_date_created');
    	$this->db->where('search.usr_id', $data['usr_id']);
    	$this->db->where('sch_status', 'active');
    	$this->db->order_by('sch_date_created', 'desc');
    	$searches = $this->db->get('search');

    	if($searches->num_rows() > 0){
    		$this->set_data($searches->result(), true);
    	}else{
    		$this->set_error(array('message'=>'No saved searches found.'));
    	}
    }
		return $this->compose_result();
	}

	public function search($data)
	{
		$this->set_message("Searches");
		$this->form_validation->set_rules("usr_id", "User", "trim");
		$this->form_validation->set_rules("sch_id", "Search ID", "trim");

		$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
    } else {
    	$user = $this->user_model->get_one_simple($data['usr_id']);

    	if(isset($data['sch_id'])){
    		$this->db->where('search.usr_id', $data['usr_id']);
    		$this->db->where('sch_id', $data['sch_id']);
    		$this->db->where('sch_status', 'active');
    		$searches = $this->db->get('search');

    		if($searches->num_rows() > 0){
    			$search = $searches->row();
    			$data['sch_content'] = json_decode($search->sch_content, true);
    		}
    	}

    	// $blocked_ids = blocked_ids($data['usr_id']);
    	// array_push($blocked_ids, $data['usr_id']);

    	$this->db->select('user.usr_id, usr_type, usr_screen_name, usr_gender, usr_birthyear, usr_birthmonth, usr_birthdate, usr_birthday, user.cnt_id, usr_description, usr_approved_date, usr_last_login, user.img_id, usr_location, usr_token_id, inf_height, inf_height_unit, inf_ethnicity, inf_body_type, inf_relationship, inf_children, inf_language, inf_education, inf_smoke, inf_drink, inf_spending_habits');

    	if($user->usr_type == 'sugar'){
    		$type = 'flame';
    	}else{
    		$type = 'sugar';
    	}

    	$this->db->where('user.usr_type', $type);
    	$this->db->where('user.usr_status', 'approved');

    	if(isset($data['sch_content']) && !empty($data['sch_content'])){
    		if(isset($data['sch_content']['usr_screen_name']) && $data['sch_content']['usr_screen_name'] != ''){
    			$this->db->like('user.usr_screen_name', $data['sch_content']['usr_screen_name']);
    		}

    		if(isset($data['sch_content']['usr_gender']) && count($data['sch_content']['usr_gender']) > 0){
    			$this->db->where_in('user.usr_gender', $data['sch_content']['usr_gender']);
    		}

    		if((isset($data['sch_content']['usr_interested_age_from']) && $data['sch_content']['usr_interested_age_from'] != '') && (isset($data['sch_content']['usr_interested_age_to']) && $data['sch_content']['usr_interested_age_to'] != '')){
    			$age_from = $data['sch_content']['usr_interested_age_from'];
					$age_to = $data['sch_content']['usr_interested_age_to'] + 1;
			
			    $this->db->where('user.usr_birthday <=', date("Y-m-d", strtotime("-$age_from year", time())));
			    $this->db->where('user.usr_birthday >=', date("Y-m-d", strtotime("-$age_to year", time())));
    		}

    		if(isset($data['sch_content']['usr_interested_country']) && $data['sch_content']['usr_interested_country'] != 0){
    			$this->db->where('user.cnt_id', $data['sch_content']['usr_interested_country']);
    		}

    		if((isset($data['sch_content']['inf_height_from']) && $data['sch_content']['inf_height_from'] != '') && (isset($data['sch_content']['inf_height_to']) && $data['sch_content']['inf_height_to'] != '')){
    			$this->db->where('info.inf_height >=', $data['sch_content']['inf_height_from']);
    			$this->db->where('info.inf_height <=', $data['sch_content']['inf_height_to']);
    			$this->db->where('info.inf_height_unit', 'cm');
    		}

    		if(isset($data['sch_content']['inf_ethnicity']) && count($data['sch_content']['inf_ethnicity']) > 0){
    			$this->db->where_in('info.inf_ethnicity', $data['sch_content']['inf_ethnicity']);
    		}

    		if(isset($data['sch_content']['inf_relationship']) && count($data['sch_content']['inf_relationship']) > 0){
    			$this->db->where_in('info.inf_relationship', $data['sch_content']['inf_relationship']);
    		}

    		if(isset($data['sch_content']['inf_body_type']) && count($data['sch_content']['inf_body_type']) > 0){
    			$this->db->where_in('info.inf_body_type', $data['sch_content']['inf_body_type']);
    		}

    		if(isset($data['sch_content']['inf_children']) && count($data['sch_content']['inf_children']) > 0){
    			$this->db->where_in('info.inf_children', $data['sch_content']['inf_children']);
    		}

    		if(isset($data['sch_content']['inf_language']) && count($data['sch_content']['inf_language']) > 0){
    			$this->db->where_in('info.inf_language', $data['sch_content']['inf_language']);
    		}

    		if(isset($data['sch_content']['inf_education']) && count($data['sch_content']['inf_education']) > 0){
    			$this->db->where_in('info.inf_education', $data['sch_content']['inf_education']);
    		}

    		if(isset($data['sch_content']['inf_smoke']) && count($data['sch_content']['inf_smoke']) > 0){
    			$this->db->where_in('info.inf_smoke', $data['sch_content']['inf_smoke']);
    		}

    		if(isset($data['sch_content']['inf_drink']) && count($data['sch_content']['inf_drink']) > 0){
    			$this->db->where_in('info.inf_drink', $data['sch_content']['inf_drink']);
    		}

    		if(isset($data['sch_content']['inf_spending_habits']) && $data['sch_content']['inf_spending_habits'] != ''){
    			$this->db->where('info.inf_spending_habits <=', $data['sch_content']['inf_spending_habits']);
    		}

    		if(isset($data['sch_content']['inf_net_worth']) && $data['sch_content']['inf_net_worth'] != ''){
    			$this->db->where('info.inf_net_worth <=', $data['sch_content']['inf_net_worth']);
    		}

    		if(isset($data['sch_content']['inf_yearly_income']) && $data['sch_content']['inf_yearly_income'] != ''){
    			$this->db->where('info.inf_yearly_income <=', $data['sch_content']['inf_yearly_income']);
    		}
    	}else{
    		if($user->usr_interested != 'both'){
    			$this->db->where('user.usr_gender', $user->usr_interested);
    		}

  			$age_from = $user->usr_interested_age_from;
				$age_to = $user->usr_interested_age_to + 1;
		
		    $this->db->where('user.usr_birthday <=', date("Y-m-d", strtotime("-$age_from year", time())));
		    $this->db->where('user.usr_birthday >=', date("Y-m-d", strtotime("-$age_to year", time())));

		    if($user->usr_interested_country != 0){
		    	$this->db->where('user.cnt_id', $user->usr_interested_country);
		    }
    	}
    	// $this->db->where_not_in('user.usr_id', $blocked_ids);

      $this->db->where('user.usr_id !=', $user->usr_id);

      $this->db->where("(user.usr_id not in (select (CASE usr_id when ".$data['usr_id']." then blo_user else usr_id end) as usr_id from block where (blo_user = ".$data['usr_id']." AND blo_status = 'active') or (usr_id = ".$data['usr_id']." AND blo_status = 'active')))");

      $this->db->where("((`privacy`.`pri_profile` = 'no') OR (`privacy`.`pri_profile` = 'yes' AND ((`user`.`usr_interested` = 'both' OR `user`.`usr_interested` = '".$user->usr_gender."') AND (`user`.`usr_interested_age_from` <= DATEDIFF(CURRENT_DATE, STR_TO_DATE('".$user->usr_birthday."', '%Y-%m-%d'))/365) AND (`user`.`usr_interested_age_to` >= DATEDIFF(CURRENT_DATE, STR_TO_DATE('".$user->usr_birthday."', '%Y-%m-%d'))/365))))");

      $this->db->join("privacy", "privacy.usr_id = user.usr_id", "left outer");
      $this->db->join("info", "info.usr_id = user.usr_id", "left outer");
      // $this->db->join("city", "city.cty_id = user.cty_id", "left outer");
      // $this->db->join("region", "region.reg_id = city.reg_id", "left outer");
      // $this->db->join("country", "country.cnt_id = region.cnt_id", "left outer");

      $this->db->group_by('user.usr_id');
  		$users = $this->db->get('user');

  		if($users->num_rows() > 0){
  			$this->set_data($users->result(), true);
  		}else{
  			$this->set_error(array('message'=>'No users found.'));
  		}
    }
    return $this->compose_result();
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Search");
		$this->db->join("user", "user.usr_id = search.usr_id");

		return parent::get_one($id);
	}

	
}