<?php
// Extend Base_model instead of CI_model
class Image_api_model extends Api_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'img_id', 
			'usr_id', 
			'img_original', 
			'img_image', 
			'img_thumb', 
			'img_icon', 
			'img_offset_x', 
			'img_offset_y', 
			'img_order', 
			'img_status',
			'img_source',
			'img_date_created', 
			'img_date_modified'
		);

		parent::__construct('image', $fields);
	}


	public function upload($data)
	{
		$this->set_message("Upload Image");
		
		$this->form_validation->set_rules('img_id', 'Image ID', 'trim');
		$this->form_validation->set_rules("usr_id", "User", "trim");
		$this->form_validation->set_rules("img_original", "Image", "trim");
		$this->form_validation->set_rules("img_image", "Image", "trim");
		$this->form_validation->set_rules("img_status", "Status", "trim|required");
		$this->form_validation->set_rules("img_source", "Source", "trim|required");
		$this->form_validation->set_rules("img_order", "Order", "trim|integer|max_length[11]");

		$error = $this->run_validators();
		if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
    	if(isset($data['img_id'])){
    		$this->db->where('img_id', $data['img_id']);
    		$this->db->where('image.usr_id', $data['usr_id']);
    		$images = $this->db->get('image');
 
    		if($images->num_rows() > 0){
    			$data['img_date_modified'] = format_mysql_datetime();

    			$this->db->where('img_id', $data['img_id']);
    			$this->db->update('image', $data);

    			if(isset($data['img_order'])) {
	    			if($data['img_order'] == 1){
	    				$user = array();
	    				$user['img_id'] = $data['img_id'];

	    				$this->db->where('usr_id', $data['usr_id']);
	    				$this->db->update('user', $user);
	    			}
    			}

    			$this->db->where('img_id', $data['img_id']);
    			$img = $this->db->get('image');

    			update_signup_status($data['usr_id']);

    			$this->set_data($img->row(), true);
    		}else{
    			$this->set_error(array('message'=>'Image not found.'));
    		}
    	}else{
    		if(isset($data['img_image'])){
    			$moderated = false;

    			$this->db->where('usr_id', $data['usr_id']);
    			$users = $this->db->get('user');
    			$user = $users->row();

	    		$this->db->where('image.usr_id', $data['usr_id']);
	    		$this->db->where('img_status', 'active');
	    		$this->db->order_by('img_order', 'desc');
	    		$images = $this->db->get('image');

	    		if($images->num_rows() < 20){
	    			if($images->num_rows() > 0){
	    				$image = $images->row();
	    				$order = $image->img_order + 1;
	    			}else{
	    				$order = 1;
	    			}

	    			$img = array();
	    			$img['usr_id'] = $data['usr_id'];
	    			// $img['img_original'] = $data['img_original'];
	    			$img['img_image'] = $data['img_image'];
	    			$img['img_thumb'] = $data['img_thumb'];
	    			$img['img_order'] = $order;
	    			$img['img_source'] = $data['img_source'];
	    			$img['img_date_created'] = format_mysql_datetime();
	    			$img['img_date_modified'] = format_mysql_datetime();
	    			
						if($user->usr_status == 'approved'){
							// $img['img_status'] = 'pending';
						}
	    			

	    			$this->db->insert('image', $img);
	    			$img_id = $this->db->insert_id();

	    			if($order == 1){
	    				$usr_img = array();
	    				$usr_img['img_id'] = $this->db->insert_id();

	    				$this->db->where('usr_id', $data['usr_id']);
	    				$this->db->update('user', $usr_img);
	    			}
	    			
	    			if($user->usr_status == 'pending' && $user->usr_signup_step == 'initial'){
	    				$usr = array();
	    				$usr['usr_signup_step'] = 'photo';
	    				$usr['usr_modified_date'] = format_mysql_datetime();

	    				$this->db->where('usr_id', $user->usr_id);
	    				$this->db->update('user', $usr);
	    			}

	    			$this->db->where('img_id', $img_id);
	    			$image = $this->db->get('image');

	    			update_signup_status($data['usr_id']);

	    			if($user->usr_status == 'approved'){
	    		// 		$moderated = false;

	    		// 		$img['img_id'] = $img_id;

	    		// 		$update = array();
							// $update['usr_id'] = $data['usr_id'];
							// $update['upd_type'] = 'photo';
							// $update['upd_status'] = 'pending';
							// $update['upd_content'] = json_encode($img);
							// $update['upd_date_created'] = format_mysql_datetime();
							// $update['upd_date_modified'] = format_mysql_datetime();

							// $this->db->insert('user_update', $update);
	    			}

	    			if($moderated){
	    				$this->set_error(array('message'=>'Image subject for admin validation.'));
	    			}else{
	    				$this->set_data($image->row(), true);
	    			}
	    		}else{
	    			$this->set_error(array('message'=>'Maximum of 20 images.'));
	    		}
    		}else{
	    		$this->set_error(array('message'=>'Image is blank.'));
    		}
    	}
    }
		return $this->compose_result();
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Image");
		$this->db->join("user", "user.usr_id = image.usr_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		if(isset($params['usr_id'])){
			$usr_id = $params['usr_id'];
			// unset($params['usr_id']);
		}
		
		$this->set_message("List Images");

		// $this->db->join("user", "user.usr_id = image.usr_id");
		$this->db->where('usr_id', $usr_id);
		$this->db->where('img_status', 'active');
		$this->db->or_where('img_status', 'private');

    return parent::get_all($params, $order_by);
	}
}