<?php
class Post_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'pos_id', 
			'usr_id', 
			'pos_content', 
			'pos_status', 
			'pos_date_created', 
			'pos_date_modified'
		);

		parent::__construct('post', $fields);
    $this->load->model('user_model');
	}

	public function post($data)
	{
		$this->set_message("Post");
		$this->form_validation->set_rules('pos_id', 'Post Id', 'integer|trim');
		$this->form_validation->set_rules("usr_id", "User", "trim|integer|max_length[11]");
		$this->form_validation->set_rules("pos_content", "Content", "trim");
		$this->form_validation->set_rules("pos_status", "Status", "trim");
		$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
    } else {
    	if(isset($data['pos_id'])){
    		$this->db->where('pos_id', $data['pos_id']);
    		$this->db->where('post.usr_id', $data['usr_id']);
    		$posts = $this->db->get('post');

    		if($posts->num_rows() > 0){
    			$post = $posts->row();

    			$_post = array();
    			$_post['pos_status'] = $data['pos_status'];
    			$_post['pos_date_modified'] = format_mysql_datetime();

    			$this->db->where('pos_id', $post->pos_id);
    			$this->db->update('post', $_post);

    			$this->db->select('pos_id, usr_id, pos_content, pos_status');
    			$this->db->where('pos_id', $post->pos_id);
    			$posts = $this->db->get('post');

    			$this->set_data($posts->row(), true);
    		}else{
    			$this->set_error(array('message'=>'Post not found.'));
    		}
    	}else{
    		$post = array();
    		$post['usr_id'] = $data['usr_id'];
        
        $regex = '/<[^>]*>[^<]*<[^>]*>/';
        
    		$post['pos_content'] = preg_replace($regex, '', $data['pos_content']);
    		$post['pos_status'] = 'active';
    		$post['pos_date_created'] = format_mysql_datetime();
    		$post['pos_date_modified'] = format_mysql_datetime();

    		$this->db->insert('post', $post);

    		$post['pos_id'] = $this->db->insert_id();

    		unset($post['pos_date_created']);

    		$this->set_data($post, true);
    	}
    }
		return $this->compose_result();
	}


	public function get_all($params = array(), $order_by = array())
	{
    $this->set_message("List Posts");

    $usr_id = $params['usr_id'];
    unset($params['usr_id']);

    $user = $this->user_model->get_one_simple($usr_id);
   
    $continue = true;

    if(isset($params['source'])){
      switch ($params['source']) {
        case 'i-faved':
          unset($params['source']);

          $this->db->where("user.usr_id in (select (CASE usr_id when ".$user->usr_id." then usl_user else usr_id end) as usr_id from user_like where (usr_id = ".$user->usr_id." AND usl_status = 'active'))");

          $this->db->where("user.usr_id !=", $user->usr_id);

          break;

        case 'i-messaged':
          unset($params['source']);

          $this->db->where("user.usr_id in (select (CASE cnv_user_2 when ".$user->usr_id." then cnv_user_1 else cnv_user_2  end) as usr_id from conversation where (cnv_user_1 = ".$user->usr_id." AND cnv_status = 'active'))");

          $this->db->where("user.usr_id !=", $user->usr_id);
          break;

        case 'i-followed':
          unset($params['source']);

          $this->db->where("user.usr_id in (select (CASE usr_id when ".$user->usr_id." then fol_user else usr_id end) as usr_id from follow where (usr_id = ".$user->usr_id." AND fol_status = 'active'))");

          $this->db->where("user.usr_id !=", $user->usr_id);
          break;

        case 'consolidated':
          unset($params['source']);

          $this->db->where("((user.usr_id in (select (CASE usr_id when ".$user->usr_id." then usl_user else usr_id end) as usr_id from user_like where (usr_id = ".$user->usr_id." AND usl_status = 'active'))) OR (user.usr_id in (select (CASE cnv_user_2 when ".$user->usr_id." then cnv_user_1 else cnv_user_2  end) as usr_id from conversation where (cnv_user_1 = ".$user->usr_id." AND cnv_status = 'active'))) OR (user.usr_id in (select (CASE usr_id when ".$user->usr_id." then fol_user else usr_id end) as usr_id from follow where (usr_id = ".$user->usr_id." AND fol_status = 'active'))) OR (user.usr_id = ".$user->usr_id."))");

          // $this->db->where("user.usr_id !=", $user->usr_id);
          break;
        
        default:
          // $continue = false;
          break;
      }
    }else{
      $this->db->where("((user.usr_id in (select (CASE usr_id when ".$user->usr_id." then usl_user else usr_id end) as usr_id from user_like where (usr_id = ".$user->usr_id." AND usl_status = 'active'))) OR (user.usr_id in (select (CASE cnv_user_2 when ".$user->usr_id." then cnv_user_1 else cnv_user_2  end) as usr_id from conversation where (cnv_user_1 = ".$user->usr_id." AND cnv_status = 'active'))) OR (user.usr_id in (select (CASE usr_id when ".$user->usr_id." then fol_user else usr_id end) as usr_id from follow where (usr_id = ".$user->usr_id." AND fol_status = 'active'))) OR (user.usr_id = ".$user->usr_id."))");
    }
  
    if($continue){
      $this->db->where("user.usr_id not in (select (CASE usr_id when ".$user->usr_id." then blo_user else usr_id end) as usr_id from block where (blo_user = ".$user->usr_id." AND blo_status = 'active') or (usr_id = ".$user->usr_id." AND blo_status = 'active'))");

      $this->db->where("post.pos_status", 'active');

      $this->db->where("((`privacy`.`pri_profile` = 'no') OR (`privacy`.`pri_profile` = 'yes' AND ((`user`.`usr_interested` = 'both' OR `user`.`usr_interested` = '".$user->usr_gender."') AND (`user`.`usr_interested_age_from` <= DATEDIFF(CURRENT_DATE, STR_TO_DATE('".$user->usr_birthday."', '%Y-%m-%d'))/365) AND (`user`.`usr_interested_age_to` >= DATEDIFF(CURRENT_DATE, STR_TO_DATE('".$user->usr_birthday."', '%Y-%m-%d'))/365))))");

      $this->db->join("user", "user.usr_id = post.usr_id");
      $this->db->join("privacy", "privacy.usr_id = user.usr_id", "left outer");
      // $this->db->join("city", "city.cty_id = user.cty_id", "left outer");
      // $this->db->join("region", "region.reg_id = city.reg_id", "left outer");
      // $this->db->join("country", "country.cnt_id = region.cnt_id", "left outer");

      $this->db->group_by('post.pos_id');
      return parent::get_all($params, $order_by);

      // print_r($this->db->last_query());
      // die();
    }else{
      $this->set_error(array('message'=>"Nothing to display."));
      return $this->compose_result();
    }
	}

  public function liked($pos_id, $viewer)
  {
    $pol = array();
    $pol['pol_id'] = false;

    $this->db->where('post_like.pos_id', $pos_id);
    $this->db->where('post_like.usr_id', $viewer);
    $post_likes = $this->db->get('post_like');

    if($post_likes->num_rows() > 0){
      $post_like = $post_likes->row();

      $pol['pol_id'] = $post_like->pol_id;
      $pol['pol_status'] = $post_like->pol_status;
    }

    return $pol;
  }

  public function flagged($pos_id, $viewer)
  {
    $flg = array();
    $flg['flg_id'] = false;

    $this->db->where('flag.pos_id', $pos_id);
    $this->db->where('flag.usr_id', $viewer);
    $flags = $this->db->get('flag');

    if($flags->num_rows() > 0){
      $flag = $flags->row();

      $flg['flg_id'] = $flag->flg_id;
      $flg['flg_status'] = $flag->flg_status;
    }

    return $flg;
  }
}