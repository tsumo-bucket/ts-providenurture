<?php
class User_location_model extends Base_model
{
	public function __construct()
	{
		$fields = array(
			'ulc_id', 
			'usr_id', 
			'ulc_street',
			'ulc_street_number',
			'ulc_city',
			'ulc_state',
			'ulc_country',
			'ulc_lat', 
			'ulc_lng', 
			'ulc_date_added'
		);

		$searchable_fields = array('');

		parent::__construct('user_location', $fields, $searchable_fields, null);
	}

	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{
		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}