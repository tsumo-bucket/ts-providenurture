<?php
class Region_model extends Base_model
{
	public function __construct()
	{
		$fields = array(
			'reg_id', 
			'cnt_id', 
			'reg_name',
			'reg_status'
		);

		$searchable_fields = array('cnt_name', 'reg_name');

		parent::__construct('region', $fields, $searchable_fields, null);
	}

	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		$this->db->join("country", "country.cnt_id = region.cnt_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("country", "country.cnt_id = region.cnt_id");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}