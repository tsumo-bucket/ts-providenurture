<?php
// Extend Base_model instead of CI_model
class User_report_api_model extends Api_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'rep_id', 
			'usr_id', 
			'rep_user', 
			'rep_reason', 
			'rep_other_reason', 
			'rep_evidence', 
			'rep_date_created',
			'rep_date_modified',
		);

		parent::__construct('user_report', $fields);

		$this->load->model('user_report_model');

		$this->mythos->library('upload');
    	$this->mythos->library('image');
	}

	// Inherits the create, update, delete, get_one, and get_all methods of api_model.
	public function create($data, $field_list = array())
	{
		$this->set_message("Create User Report");
		$this->form_validation->set_rules("usr_id", "Reporting User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("rep_user", "Reported User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("rep_reason", "Reason for Reporting", "trim|required");
		$this->form_validation->set_rules("rep_other_reason", "More Information", "trim");
		$this->form_validation->set_rules("rep_evidence", "Evidence", "trim");
		$this->form_validation->set_rules("rep_other_reason", "Other Reason", "trim");
		$error = $this->run_validators();
		if (!is_bool($error)) {
      		$this->set_error($error, false);
		}
		else {

			$this->db->where('usr_id', $data['usr_id']);
			$this->db->where('rep_user', $data['rep_user']);
    		$query = $this->db->get('user_report');
			if($query->num_rows() == 0) {
				if($data['rep_evidence']) {
			      	$rand = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 24)),0, 24);
			      	$comma = strpos($data['rep_evidence'], ',');
			      	$decoded = base64_decode(substr($data['rep_evidence'], $comma+1));
			      	$image_name = $rand . '.jpg';
			      	// file_put_contents('uploads/evidences/' . $image_name, $decoded);
			      	$data['rep_evidence'] = 'uploads/evidences/' . $image_name;
		 			$this->image->_uploadJPG($image_name, 'uploads/evidences/', $decoded, true);
			    }
	      		$data['rep_date_created'] = format_mysql_datetime();
		    	// $this->db->insert('user_report', $data);
		    	// $id = $this->db->insert_id();
				return parent::create($data, $field_list = array());

		    	$return = array();
		    	$return['rep_id'] = $id;
		    	$return['rep_user'] = $data['rep_user'];
		    	$return['usr_id'] = $data['usr_id'];

	  			$this->set_data($return, true);
			}
			else {
	    		$this->set_error(array('message'=>'You already reported this user.'));
				return $this->compose_result();	
			}
		}
	}

	public function update($data, $field_list = array())
	{
		$this->set_message("Update User Report");
		$this->form_validation->set_rules('rep_id', 'User Report Id', 'integer|required');
		$this->form_validation->set_rules("usr_id", "", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("rep_user", "User", "trim|required|integer|max_length[11]");
		$this->form_validation->set_rules("rep_reason", "Reason", "trim|required");
		$this->form_validation->set_rules("rep_other_reason", "Other Reason", "trim|required");
		return parent::update($data, $field_list = array());
	}

	public function delete($id)
	{
		$this->set_message("Delete User Report");
		return parent::delete($id);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single User Report");

		$this->db->join("user", "user.usr_id = user.usr_id", "left outer");
		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List User Reports");

        return parent::get_all($params, $order_by);
	}
}