<?php
class User_model extends Base_model
{
	public function __construct()
	{
		$fields = array(
			'usr_id', 
			'usr_type', 
			'usr_first_name', 
			'usr_last_name', 
			'usr_screen_name', 
			'usr_email', 
			'usr_password', 
			'usr_birthyear', 
			'usr_birthmonth', 
			'usr_birthdate',
			'usr_birthday', 
			'cty_id', 
			'cnt_id', 
			'ulc_id', 
			'usr_location', 
			'usr_status', 
			'usr_gender', 
			'usr_interested', 
			'usr_interested_age_from', 
			'usr_interested_age_to',
			'usr_interested_country', 
			'usr_description', 
			'usr_signup_step', 
			'usr_signup_source', 
			'usr_verified', 
			'usr_social_verified', 
			'usr_password_reset', 
			'usr_created_date', 
			'usr_modified_date', 
			'usr_approved_date',
			'usr_last_login', 
			'usr_password_reset_date',
			'usr_token', 
			'usr_token_id', 
			'img_id', 
			'src_id'
		);

		$searchable_fields = array('usr_type', 'usr_first_name', 'usr_last_name', 'usr_screen_name', 'usr_email', 'usr_status', 'usr_gender');

		$this->mythos->library('email');
		
		parent::__construct('user', $fields, $searchable_fields, null);
	}

	public function create($data, $field_list = array())
	{
		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one_simple($id)
	{
		return parent::get_one($id);
	}

	public function get_one($id)
	{
		// $this->db->join("city", "city.cty_id = user.cty_id", "left outer");
		// $this->db->join("source", "source.src_id = user.src_id", "left outer");
		// $this->db->join("image", "image.img_id = user.img_id", "left outer");
		// $this->db->join("user_report", "rep.rep_user = user.usr_id", "left outer");
		
		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		// $this->db->join("city", "city.cty_id = user.cty_id", "left outer");
		$this->db->join("source", "source.src_id = user.src_id", "left outer");
		// $this->db->join("image", "image.img_id = user.img_id", "left outer");
		$this->db->order_by("usr_modified_date", "desc");
		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}

	public function get_by_email($email)
	{
		$this->db->where('usr_email', $email);
		$query = $this->db->get('user');

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	public function resend_verification($email)
	{
		$request = $this->input->get();
		$params = array();
      	$params['mail_to'] = $email;
 		$params['subject'] = 'ProvideNurture Registration';
      	$params['cc'] = '';
      	$params['bcc'] = '';

      	$content = array();
     	$content['user'] = $this->user_model->get_by_email($email);

      	$params['content'] = $this->load->view('email/initial', $content, true);
      	
		if($this->email->send_mail($params,'email', 'template')) {
			return true;
		}
		else {
			return false;
		}

	}

	public function authenticate($usr_email, $usr_password)
	{
		$this->db->join("image", "image.img_id = user.img_id", "left outer");
		$this->db->join("user_location", "user_location.ulc_id = user.ulc_id", "left outer");
		$this->db->join("user_accounts", "user_accounts.usr_id = user.usr_id", "left outer");
		
		$this->db->select("*, `user`.`usr_id` AS usr_id");
		$this->db->where("user.usr_email = '".$usr_email."' AND user.usr_password ='".md5($usr_password)."'", "", FALSE);
		$query = $this->db->get($this->table);
		
		if($query->num_rows() > 0){
			$user = $query->row();

			//GET INFO_ID
			$this->db->where('info.usr_id', $user->usr_id);
			$infos = $this->db->get('info');

			if($infos->num_rows() > 0){
				$info = $infos->row();

				$user->inf_id = $info->inf_id;
			}

			//GET PRF_IDs
			$this->db->where('preference.usr_id', $user->usr_id);
			$preferences = $this->db->get('preference');

			if($preferences->num_rows() > 0){
				$prf_ids = array();

				foreach($preferences->result() as $preference){
					array_push($prf_ids, $preference->prf_id);
				}

				$user->prf_ids = $prf_ids;
			}

			$user->_message = '';
			$user->_redirect = '';
			$user->_continue = true;

			$data = array();

			if($user->usr_signup_step != 'done'){
				$data['usr_last_login'] = format_mysql_datetime();

				$this->db->where('usr_id', $user->usr_id);
				$this->db->update('user', $data); 

				$user->_redirect = $user->usr_signup_step;

				return $user;
			}else{
				switch ($user->usr_status) {
					case 'pending':
						// $user = new stdClass();
						// $user->_message = 'Waiting for account approval.';
						$user->_message = '';
						$user->_redirect = 'search';
						$user->_continue = true;
						
						return $user;
						break;

					case 'rejected':
						$data['usr_last_login'] = format_mysql_datetime();
						$data['usr_status'] = 'pending';
						$data['usr_modified_date'] = format_mysql_datetime();
						
						$this->db->where('usr_id', $user->usr_id);
						$this->db->update('user', $data);

						$user->_message = 'Account submission was rejected, please resubmit your profile.';
						$user->_redirect = 'photo';

						return $user;
						break;

					case 'deactivated':
						$data['usr_last_login'] = format_mysql_datetime();
						$data['usr_status'] = 'approved';
						$data['usr_modified_date'] = format_mysql_datetime();

						$this->db->where('user_inactive.usr_id', $user->usr_id);
						$this->db->where('usi_type', 'deactivate');
						$this->db->where('usi_status', 'active');
						$user_inactives = $this->db->get('user_inactive');

						if($user_inactives->num_rows() > 0){
							foreach($user_inactives->result() as $user_inactive){
								$user_inactive->usi_status = 'archived';
								$user_inactive->usi_modified_date = format_mysql_datetime();

								$this->db->where('usi_id', $user_inactive->usi_id);
								$this->db->update('user_inactive', $user_inactive);
							}
						}
						
						$this->db->where('usr_id', $user->usr_id);
						$this->db->update('user', $data); 

						$user->_message = 'Welcome back!';
						
						return $user;
						break;

					case 'suspended':
						$user = new stdClass();
						$user->_message = 'Account is suspended.';
						$user->_redirect = '';
						$user->_continue = false;

						return $user;
						break;
					
					default:
						$data['usr_last_login'] = format_mysql_datetime();

						$this->db->where('usr_id', $user->usr_id);
						$this->db->update('user', $data); 

						return $user;
						break;
				}
			}
		}else{
			return false;
		}
	}

	public function api_authenticate($usr_token)
	{
		$this->db->where('usr_token', $usr_token);
		$query = $this->db->get($this->table); 
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function generate_password_hash()
	{
		$hash = $this->generateRandomString(16);
		if($this->get_by_password_hash($hash)){
			$hash = $this->generateRandomString(14, false);
		}

		return $hash;
	}

	public function get_by_password_hash($usr_password_reset)
	{
		$this->db->or_where('usr_password_reset', $usr_password_reset);
		$query = $this->db->get($this->table); 
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function generateRandomString($length = 10, $vowels = true) {
		if($vowels){
    	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		}else{
    	$characters = '0123456789bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ';
		}
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
	}
	
	public function additional($usr_id, $_info = true)
	{
		$this->db->where('usr_id', $usr_id);
		$this->db->where('usr_status', 'approved');
		$users = $this->db->get('user');

		if($users->num_rows() > 0){
			$user = $users->row();

			$data = array();
			$data['usr_token_id'] = $user->usr_token_id;

			if($_info){ 
				$this->db->select('inf_height, inf_height_unit, inf_body_type, inf_ethnicity, inf_spending_habits');
				$this->db->where('info.usr_id', $usr_id);
		  	$info = $this->db->get('info');

				$data = (object) array_merge((array) $data, (array) $info->row()); 	
			}

	  	$this->db->select('img_image, img_thumb');
	  	$this->db->where('img_id', $user->img_id);
	  	$images = $this->db->get('image');

			$data = (object) array_merge((array) $data, (array) $images->row());

			return $data;
		}else{
			return false;
		}
	}

	public function additional_block($usr_id)
	{
		$this->db->select('usr_type, usr_screen_name, usr_gender, usr_birthmonth, usr_birthdate, usr_birthyear, usr_token_id, img_id, usr_approved_date, usr_last_login, usr_location, cnt_id');
		$this->db->where('usr_id', $usr_id);
		$this->db->where('usr_status', 'approved');
		$users = $this->db->get('user');

		if($users->num_rows() > 0){
			$user = $users->row();

			$from = new DateTime($user->usr_birthyear.'-'.$user->usr_birthmonth.'-'.$user->usr_birthdate);
			$to = new DateTime('today');

			unset($user->usr_birthyear);
			unset($user->usr_birthmonth);
			unset($user->usr_birthdate);

			$user->usr_age = $from->diff($to)->y;

			$this->db->where('info.usr_id', $usr_id);
	  	$info = $this->db->get('info');

			$data = (object) array_merge((array) $user, (array) $info->row()); 	

			$this->db->select('img_image');
	  	$this->db->where('img_id', $user->img_id);
	  	$images = $this->db->get('image');

			$data = (object) array_merge((array) $data, (array) $images->row());

			return $data;
		}else{
			return false;
		}
	}

	public function additional_fave($usr_id)
	{
		$this->db->select('usr_type, usr_screen_name, usr_gender, usr_description, usr_birthmonth, usr_birthdate, usr_birthyear, usr_token_id, img_id, cty_id, usr_approved_date, usr_last_login, usr_location, cnt_id');
		$this->db->where('usr_id', $usr_id);
		$this->db->where('usr_status', 'approved');
		$users = $this->db->get('user');

		if($users->num_rows() > 0){
			$user = $users->row();

			$from = new DateTime($user->usr_birthyear.'-'.$user->usr_birthmonth.'-'.$user->usr_birthdate);
			$to = new DateTime('today');

			unset($user->usr_birthyear);
			unset($user->usr_birthmonth);
			unset($user->usr_birthdate);

			$user->usr_age = $from->diff($to)->y;

			$this->db->select('inf_height, inf_height_unit, inf_body_type, inf_ethnicity, inf_spending_habits');
			$this->db->where('info.usr_id', $usr_id);
	  	$info = $this->db->get('info');

			$data = (object) array_merge((array) $user, (array) $info->row()); 	

			$this->db->select('img_image');
	  	$this->db->where('img_id', $user->img_id);
	  	$images = $this->db->get('image');
			$data = (object) array_merge((array) $data, (array) $images->row());

			// $this->db->select('cty_name, cnt_name, cnt_short_name');
			// $this->db->join("region", "region.reg_id = city.reg_id", "left outer");
			// $this->db->join("country", "country.cnt_id = region.cnt_id", "left outer");
			// $this->db->where('city.cty_id', $user->cty_id);
			// $cities = $this->db->get('city');

			// $data = (object) array_merge((array) $data, (array) $cities->row());

			unset($data->img_id);
			// unset($data->cty_id);
		
			return $data;
		}else{
			return false;
		}
	}

	public function faved($usr_id, $data)
	{
		$this->db->where('usr_id', $usr_id);
		$this->db->where('usr_status', 'approved');
		$users = $this->db->get('user');

		if($users->num_rows() > 0){
			$this->db->where('user_like.usr_id', $data);
			$this->db->where('usl_user', $usr_id);
			$this->db->where('usl_status', 'active');
			$user_likes = $this->db->get('user_like');

			if($user_likes->num_rows() > 0){
				$user_like = $user_likes->row();
				
				return $user_like->usl_id;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function followed($usr_id, $data)
	{
		$this->db->where('usr_id', $usr_id);
		$this->db->where('usr_status', 'approved');
		$users = $this->db->get('user');

		if($users->num_rows() > 0){
			$this->db->where('follow.usr_id', $data);
			$this->db->where('fol_user', $usr_id);
			$this->db->where('fol_status', 'active');
			$follows = $this->db->get('follow');

			if($follows->num_rows() > 0){
				$follow = $follows->row();
				
				return $follow->fol_id;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function filter($keyword = "", $params = array(), $order_by = array())
	{
		
		if(isset($params['usr_types'])) {
			$this->db->where_in('usr_type', $params['usr_types']);
		}
		if(isset($params['usr_statuses'])){
			$this->db->where_in('usr_status', $params['usr_statuses']);
		}
		if(isset($params['usr_verifieds'])) {
			$this->db->where_in('usr_verified', $params['usr_verifieds']);
		}
		if(isset($params['usr_social_verified'])) {
			$this->db->where('usr_social_verified', $params['usr_social_verified']);
		}
		if(isset($params['usr_genders'])) {
			$this->db->where_in('usr_gender', $params['usr_genders']);
		}		
		unset($params['usr_types']);
		unset($params['usr_statuses']);
		unset($params['usr_verifieds']);
		unset($params['usr_social_verified']);
		unset($params['usr_genders']);
		unset($params['filter']);
		return $this->get_all($params, $order_by);
	}
}