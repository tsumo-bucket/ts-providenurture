<?php
// Extend Base_model instead of CI_model
class Privacy_api_model extends Api_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'pri_id', 
			'usr_id', 
			'pri_comment', 
			'pri_profile', 
			'pri_date_created', 
			'pri_date_modified'
		);

		parent::__construct('privacy', $fields);
	}

	public function update($data)
	{
		$this->set_message("Update Privacy");

		$this->form_validation->set_rules('pri_id', 'Privacy Id', 'integer|required');
		$this->form_validation->set_rules("usr_id", "User", "trim");
		$this->form_validation->set_rules("pri_comment", "Comment", "trim|required");
		$this->form_validation->set_rules("pri_profile", "Profile", "trim|required");

		$error = $this->run_validators();

		if(!is_bool($error)){
			$this->set_error($error, false);
    }else{
    	$this->db->where('privacy.pri_id', $data['pri_id']);
    	$this->db->where('privacy.usr_id', $data['usr_id']);
    	$privacies = $this->db->get('privacy');

    	if($privacies->num_rows() > 0){
    		$privacy = $privacies->row();

    		$privacy->pri_comment = $data['pri_comment'];
    		$privacy->pri_profile = $data['pri_profile'];
    		$privacy->pri_date_modified = format_mysql_datetime();
    		
    		$this->db->where('pri_id', $privacy->pri_id);
    		$this->db->update('privacy', $privacy);

				$this->set_data($privacy, true);
			}else{
				$this->set_error(array('message'=>'Privacy not found.'));
			}
    }
    return $this->compose_result();
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single Privacy");
		$this->db->join("user", "user.usr_id = privacy.usr_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Privacies");
		$this->db->join("user", "user.usr_id = privacy.usr_id");

    return parent::get_all($params, $order_by);
	}
}