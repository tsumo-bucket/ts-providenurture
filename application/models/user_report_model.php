<?php
// Extend Base_model instead of CI_model
class User_report_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'rep_id', 
			'usr_id', 
			'rep_user', 
			'rep_reason', 
			'rep_other_reason', 
			'rep_evidence', 
			'rep_date_created'
		);

		$searchable_fields = array('');

		// Call the parent constructor with the table name and fields as parameters.
		$this->mythos->library("upload");
		$this->mythos->library("image");

		parent::__construct('user_report', $fields, $searchable_fields, null);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function create($data, $field_list = array())
	{
		$data["rep_evidence"] = "uploads/evidences/".$this->image->upload("rep_evidence", "uploads/evidences/")["file_name"];
		$field_list[] = "rep_evidence";
		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{
		$data["rep_evidence"] = "uploads/evidences/".$this->image->upload("rep_evidence", "uploads/evidences/")["file_name"];
		$field_list[] = "rep_evidence";
		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		$this->db->join("user", "user.usr_id = user_report.usr_id", "left outer");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("user", "user.usr_id = user_report.usr_id", "left outer");
		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}