<?php
class View_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'vie_id', 
			'usr_id', 
			'vie_user', 
			'vie_date_created'
		);

		parent::__construct('view', $fields);
	}

	public function get_all($data)
	{
		$this->set_message("Viewed Me");

		$this->db->where('usr_id', $data['usr_id']);
		$users = $this->db->get('user');
		$user = $users->row();
		
		$this->db->select('view.usr_id, vie_date_created');
		$this->db->where('vie_user', $data['usr_id']);	

		if($user->usr_interested != 'both'){
			$this->db->where('usr_gender', $user->usr_interested);
		}
		
		$this->db->where('user.usr_status', 'approved');
		$this->db->where("(user.usr_id not in (select (CASE usr_id when ".$data['usr_id']." then blo_user else usr_id end) as usr_id from block where (blo_user = ".$data['usr_id']." AND blo_status = 'active') or (usr_id = ".$data['usr_id']." AND blo_status = 'active')))");

		$this->db->join("user", "user.usr_id = view.usr_id");
		$this->db->order_by('vie_date_created', 'desc');
		$this->db->group_by("view.usr_id"); 
		$params = array();
		$params['is_infinite'] = $data['is_infinite'];
		$params['page'] = $data['page'];
		$params['page_size'] = $data['page_size'];
		return parent::get_all($params);
		// $views = $this->db->get('view');

		// if($views->num_rows() > 0){
		// 	$this->set_data($views->result(), true);
		// }else{
		// 	$this->set_error(array('message'=>"No users found."));
		// }

  //   return $this->compose_result();
	}
}