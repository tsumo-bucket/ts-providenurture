<?php
class Message_model extends Base_model
{
	public function __construct()
	{
		$fields = array(
			'msg_id', 
			'usr_id', 
			'msg_content', 
			'msg_user', 
			'msg_status', 
			'msg_date_created', 
			'cnv_id'
		);

		$searchable_fields = array('');

		parent::__construct('message', $fields, $searchable_fields, null);
	}

	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		$this->db->join("user", "user.usr_id = message.usr_id");
		$this->db->join("conversation", "conversation.cnv_id = message.cnv_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("user", "user.usr_id = message.usr_id");
		$this->db->join("conversation", "conversation.cnv_id = message.cnv_id");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}

	public function latest_message($cnv_id, $usr_id, $source)
	{
		$return = array();

		switch ($source) {
			case 'inbox':
				$this->db->where('cnv_id', $cnv_id);
				$conversations = $this->db->get('conversation');
				$conversation = $conversations->row();

				$user = 0;

				if($conversation->cnv_user_1 == $usr_id){
    			if($conversation->cnv_user_1_deleted == 'yes'){
    				$user = 1;
    				$this->db->where('msg_date_created >=', $conversation->cnv_user_1_deleted_date);	
    			}
    		}else{
    			if($conversation->cnv_user_2_deleted == 'yes'){
    				$user = 2;
    				$this->db->where('msg_date_created >=', $conversation->cnv_user_2_deleted_date);	
    			}
    		}

    		$this->db->where('cnv_id', $cnv_id);
				$this->db->order_by('msg_date_created', 'desc');
				$messages = $this->db->get('message');

				$message = $messages->row();
				if($message) {

					$return['message'] = $message->msg_content;
					$return['date'] = $message->msg_date_created;
					
					if($message->usr_id == $usr_id){
						$return['usr_id'] = $message->msg_user;
						$return['unread'] = 0;
					}else{
						$this->db->where('cnv_id', $cnv_id);
						$this->db->where('msg_user', $usr_id);
						$this->db->where('msg_status', 'unread');
					
						if($user == 1){
							$this->db->where('msg_date_created >=', $conversation->cnv_user_1_deleted_date);
						}
						if($user == 2){
							$this->db->where('msg_date_created >=', $conversation->cnv_user_2_deleted_date);
						}

						$messages_ = $this->db->get('message');

						$return['usr_id'] = $message->usr_id;
						$return['unread'] = $messages_->num_rows();					
					}
					
				}

				break;

			case 'sent':
				$this->db->where('cnv_id', $cnv_id);
				$conversations = $this->db->get('conversation');
				$conversation = $conversations->row();

				if($conversation->cnv_user_1 == $usr_id){
    			if($conversation->cnv_user_1_deleted == 'yes'){
    				$this->db->where('msg_date_created >=', $conversation->cnv_user_1_deleted_date);	
    			}
    		}else{
    			if($conversation->cnv_user_2_deleted == 'yes'){
    				$this->db->where('msg_date_created >=', $conversation->cnv_user_2_deleted_date);	
    			}
    		}

    		$this->db->where('cnv_id', $cnv_id);
				$this->db->where('message.usr_id', $usr_id);
				$this->db->order_by('msg_date_created', 'desc');
				$messages = $this->db->get('message');

				$message = $messages->row();
				if($message) {
					$return['usr_id'] = $message->msg_user;
					$return['message'] = $message->msg_content;
					$return['date'] = $message->msg_date_created;
					$return['unread'] = 0;
				}
				break;

			case 'unread':
				$this->db->where('cnv_id', $cnv_id);
				$this->db->where('msg_user', $usr_id);
				$this->db->where('msg_status', 'unread');
				$this->db->order_by('msg_date_created', 'desc');
				$messages = $this->db->get('message');

				$count = $messages->num_rows();
				$message = $messages->row();

				$return['usr_id'] = $message->usr_id;
				$return['message'] = $message->msg_content;
				$return['date'] = $message->msg_date_created;
				$return['unread'] = $count;
				break;
			
			default:
				break;
		}
		return $return;
	}
}