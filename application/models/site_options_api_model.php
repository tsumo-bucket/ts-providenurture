<?php
// Extend Base_model instead of CI_model
class Site_options_api_model extends Api_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('opt_id', 'opt_name', 'opt_slug', 'opt_value','opt_type');

		parent::__construct('site_options', $fields);
	}

  public function check_version($data)
  {
    $this->set_message("Check Version");
    $this->load->model('site_options_model');

    if(array_key_exists('os', $data)){
      if($data['os'] == 'android' || $data['os'] == 'ios'){
        $version = $this->site_options_model->get_by_slug($data['os'].'_version');
        $text = $this->site_options_model->get_by_slug($data['os'].'_version_message');

        if($version && $text){
          $data = array();
          $data['version_number'] = $version->opt_value;
          $data['version_message'] = $text->opt_value;

          $this->set_data($data,true);
        }else{
          $this->set_error(array('message'=>"Site_options not set."));
        }
      }else{
        $this->set_error(array('message'=>"Invalid OS."));
      }
    }else{
      $this->set_error(array('message'=>"'OS' parameter is required."));
    }
    return $this->compose_result();
  }

  public function system_time()
  {
    $this->set_message("Server Time");

    date_default_timezone_set('UTC');

    $data = array();
    $data['datetime'] = date('U');
    $this->set_data($data,true);

    return $this->compose_result();
  }
}