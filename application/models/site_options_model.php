<?php
// Extend Base_model instead of CI_model
class Site_options_model extends Base_auditable_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array('opt_id', 'opt_name', 'opt_slug', 'opt_value','opt_type');
		// Call the parent constructor with the table name and fields as parameters.
		parent::__construct('site_options', $fields);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.

	public function get_by_slug($opt_slug)
	{
		$this->db->where('opt_slug', $opt_slug);
		$query = $this->db->get($this->table); // Use $this->table to get the table name
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

}