<?php
// Extend Base_model instead of CI_model
class Conversation_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'cnv_id', 
			'cnv_slug', 
			'cnv_user_1', 
			'cnv_user_2', 
			'cnv_user_1_deleted', 
			'cnv_user_1_deleted_date', 
			'cnv_user_2_deleted', 
			'cnv_user_2_deleted_date', 
			'cnv_status',
			'cnv_date_created', 
			'cnv_date_modified'
		);

		$searchable_fields = array('');

		// Call the parent constructor with the table name and fields as parameters.


		parent::__construct('conversation', $fields, $searchable_fields, null);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}