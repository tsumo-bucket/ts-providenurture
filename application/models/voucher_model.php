<?php
// Extend Base_model instead of CI_model
class Voucher_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'vch_id', 
			'vch_code', 
			'vch_status', 
			'vch_value', 
			'vch_valid_until', 
			'vch_date_created', 
			'vch_date_modified', 
			'vch_created_by', 
			'vch_modified_by'
		);

		$searchable_fields = array('vch_code', 'vch_status', 'vch_value');

		// Call the parent constructor with the table name and fields as parameters.


		parent::__construct('voucher', $fields, $searchable_fields, null);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}
}