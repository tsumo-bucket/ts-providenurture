<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_api_model extends Api_model
{

	public function __construct()
	{
		$fields = array('pag_id', 'pag_title', 'pct_id', 'pag_slug', 'pag_content', 'pag_date_created', 'pag_date_modified', 'pag_date_published', 'pag_type', 'pag_status');
		parent::__construct('page', $fields);
	}

	public function get_one($id)
	{
		$this->set_message("View Page");
				
		$this->db->join('page_category', "page_category.pct_id = page.pct_id", "left outer");
		return parent::get_one($id);
	}

	public function get_by_slug($pag_slug)
	{
		$this->set_message("View Page");
				
		$this->db->join('page_category', "page_category.pct_id = page.pct_id", "left outer");
		$this->db->where('pag_slug', $pag_slug);
		$query = $this->db->get('page');
		if($query->num_rows() > 0)
		{
			return $query->row();
			// return parent::get_one($page->pag_id);
		}
		else
		{
			$this->set_error(array('message'=>'Page not found.'));
		}

	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Pages");
		
		$this->db->join('page_category', "page_category.pct_id = page.pct_id", "left outer");
		
   	return parent::get_all($params, $order_by);
	}
}