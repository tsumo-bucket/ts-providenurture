<?php
class City_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'cty_id', 
			'reg_id', 
			'cty_name'
		);

		parent::__construct('city', $fields);
	}

	public function get_one($id)
	{
		$this->set_message("Retrieve Single City");
		$this->db->join("region", "region.reg_id = city.reg_id");
		$this->db->join("country", "country.cnt_id = region.cnt_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->set_message("List Cities");
		$this->db->join("region", "region.reg_id = city.reg_id");
		$this->db->join("country", "country.cnt_id = region.cnt_id");

    return parent::get_all($params, $order_by);
	}
}