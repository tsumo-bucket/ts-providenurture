<?php
class Message_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'msg_id', 
			'usr_id', 
			'msg_content', 
			'msg_user', 
			'msg_status', 
			'msg_date_created', 
			'cnv_id'
		);

		parent::__construct('message', $fields);

		$this->mythos->library('email');
	}

	public function send($data)
	{
		$this->set_message("Send Message");
		$this->form_validation->set_rules("usr_id", "User", "trim");
		$this->form_validation->set_rules("msg_content", "Content", "trim|required");
		$this->form_validation->set_rules("msg_user", "User", "trim|required|integer|max_length[11]");

		$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
    } else {
    	$this->db->where('usr_id', $data['msg_user']);
    	$this->db->where('usr_status', 'approved');
    	$users = $this->db->get('user');

    	if($users->num_rows() > 0){
    		$this->db->where("((`usr_id` = ".$data['usr_id']." AND `blo_user` = ".$data['msg_user'].") OR (`usr_id` = ".$data['msg_user']." AND `blo_user` = ".$data['usr_id'].")) AND `blo_status` = 'active'");
    		$blocks = $this->db->get('block');

    		if($blocks->num_rows() > 0){
    			$this->set_error(array('message'=>'User not found.'));
    		}else{
	    		$this->db->where("((`cnv_user_1` = ".$data['usr_id']." AND `cnv_user_2` = ".$data['msg_user'].") OR (`cnv_user_1` = ".$data['msg_user']." AND `cnv_user_2` = ".$data['usr_id'].")) AND `cnv_status` = 'active'");
	    		$conversations = $this->db->get('conversation');

	    		if($conversations->num_rows() > 0){
	    			$conversation = $conversations->row();

	    			$cnv_id = $conversation->cnv_id;
	    		}else{
	    			$conversation = array();
	    			$conversation['cnv_date_created'] = format_mysql_datetime();
	    			$conversation['cnv_date_modified'] = format_mysql_datetime();
	    			$conversation['cnv_user_1'] = $data['usr_id'];
	    			$conversation['cnv_user_2'] = $data['msg_user'];
	    			$conversation['cnv_user_1_deleted'] = 'no';
	    			$conversation['cnv_user_2_deleted'] = 'no';

	    			$this->db->insert('conversation', $conversation);
	       		$cnv_id = $this->db->insert_id();

	       		$cnv = array();
		        $cnv['cnv_slug'] = format_datetime(format_mysql_datetime(), 'U').$cnv_id;

		        $this->db->where('cnv_id', $cnv_id);
		        $this->db->update('conversation', $cnv);
		      }

	        if(isset($data['pos_id'])){
	        	$this->db->where('pos_id', $data['pos_id']);
	        	$this->db->where('pos_status', 'active');
	        	$posts = $this->db->get('post');

	        	if($posts->num_rows() == 0){
	        		unset($data['pos_id']);
	        	}
	        }

	        $data['msg_content'] = strip_tags($data['msg_content']);
	        $data['cnv_id'] = $cnv_id;
	        $data['msg_status'] = 'unread';
	        $data['msg_date_created'] = format_mysql_datetime();

	        $this->db->insert('message', $data);

	        $this->db->where('usr_id', $data['msg_user']);
	        $notifications = $this->db->get('notification');

	        $notification = $notifications->row();

	        if($notification->not_message == 'yes'){
	        	$this->db->select('usr_id, usr_email');
	        	$this->db->where('usr_id', $data['msg_user']);
	        	$users = $this->db->get('user');
	        	$receiver = $users->row();

	        	if(emailIntercept($receiver->usr_email)){
		        	$this->db->select('user.usr_id, usr_screen_name, usr_token_id, user.img_id, img_image');
		        	$this->db->join("image", "image.img_id = user.img_id", "left outer");
		        	$this->db->where('user.usr_id', $data['usr_id']);
		        	$users = $this->db->get('user');
		        	$sender = $users->row();

		        	$params['mail_to'] = $receiver->usr_email;
							$params['subject'] = "Someone sent you a message";
							$params['cc'] = '';
		        	$params['bcc'] = '';
							$content = array();
							$content['receiver'] = $receiver;
							$content['sender'] = $sender;

							$params['content'] = $this->load->view('email/message', $content, true);
									
							$email = $this->email->send_mail($params,'email', 'template');
	        	}
	        }

	        $this->set_data($data, true);
    		}
    	}else{
    		$this->set_error(array('message'=>'User not found.'));
    	}
    }
    return $this->compose_result();
	}

	public function read($data)
	{
		$this->set_message("Read Messages");
		$this->form_validation->set_rules("usr_token_id", "User Token ID", "trim|required|max_length[100]");

		$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
    } else {
    	$this->db->where('usr_token_id', $data['usr_token_id']);
    	$this->db->where('usr_status', 'approved');
    	$users = $this->db->get('user');

    	if($users->num_rows() > 0){
    		$user = $users->row();

    		$this->db->where("((`usr_id` = ".$data['usr_id']." AND `blo_user` = ".$user->usr_id.") OR (`usr_id` = ".$user->usr_id." AND `blo_user` = ".$data['usr_id'].")) AND `blo_status` = 'active'");
    		$blocks = $this->db->get('block');

    		if($blocks->num_rows() > 0){
    			$this->set_error(array('message'=>'User not found.'));
    		}else{
    			$this->db->where("(`cnv_status` = 'active') AND ((`cnv_user_1` = ".$user->usr_id." AND `cnv_user_2` = ".$data['usr_id'].") OR (`cnv_user_2` = ".$user->usr_id." AND `cnv_user_1` = ".$data['usr_id']."))");
		    	$conversations = $this->db->get('conversation');

		    	if($conversations->num_rows() > 0){
		    		$conversation = $conversations->row();

		    		$this->db->where('cnv_id', $conversation->cnv_id);
		    		$this->db->where('msg_user', $data['usr_id']);
		    		$this->db->where('msg_status', 'unread');
		    		$this->db->where('msg_date_created <=', format_mysql_datetime());
						$this->db->order_by('msg_date_created', 'asc');
		    		$messages = $this->db->get('message');

		    		if($messages->num_rows() > 0){
		    			foreach($messages->result() as $message){
		    				$msg = array();
		    				$msg['msg_status'] = 'read';

		    				$this->db->where('msg_id', $message->msg_id);
		    				$this->db->update('message', $msg);
		    			}

		    			$return = array();
		    			$return['message'] = 'Messages updated';
		    			
		    			$this->set_data($return, true);
		    		}else{
		    			$this->set_error(array('message'=>'Nothing to update.'));
		    		}
		    	}else{
		    		$this->set_error(array('message'=>'Conversation not found.'));
		    	}
    		}
    	}else{
    		$this->set_error(array('message'=>'User not found.'));
    	}
    }
    return $this->compose_result();
	}

	public function view($data)
	{
		$this->set_message("View Conversation");
		$this->form_validation->set_rules("usr_token_id", "User Token ID", "trim|required|max_length[100]");
		
		$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
    } else {
    	$this->db->where('usr_token_id', $data['usr_token_id']);
    	$this->db->where('usr_status', 'approved');
    	$users = $this->db->get('user');

    	if($users->num_rows() > 0){
    		$user = $users->row();


    		$this->db->where("((`usr_id` = ".$data['usr_id']." AND `blo_user` = ".$user->usr_id.") OR (`usr_id` = ".$user->usr_id." AND `blo_user` = ".$data['usr_id'].")) AND `blo_status` = 'active'");
    		$blocks = $this->db->get('block');

    		if($blocks->num_rows() > 0){
    			$this->set_error(array('message'=>'User not found.'));
    		}else{
		    	$this->db->where("(`cnv_status` = 'active') AND ((`cnv_user_1` = ".$user->usr_id." AND `cnv_user_2` = ".$data['usr_id'].") OR (`cnv_user_2` = ".$user->usr_id." AND `cnv_user_1` = ".$data['usr_id']."))");
		    	$conversations = $this->db->get('conversation');

		    	if($conversations->num_rows() > 0){
		    		$conversation = $conversations->row();


		    		if($conversation->cnv_user_1 == $data['usr_id']){
		    			if($conversation->cnv_user_1_deleted == 'yes'){
		    				$this->db->where('msg_date_created >=', $conversation->cnv_user_1_deleted_date);	
		    			}
		    		}else{
		    			if($conversation->cnv_user_2_deleted == 'yes'){
		    				$this->db->where('msg_date_created >=', $conversation->cnv_user_2_deleted_date);	
		    			}
		    		}
		    		$this->db->order_by('msg_id', 'desc');
		    		$this->db->where('message.cnv_id', $conversation->cnv_id);
		    		// $messages = $this->db->get('message');
		    		$params = array();
		    		$params['is_infinite'] = $data['is_infinite'];
		    		$params['page'] = $data['page'];
		    		$params['page_size'] = $data['page_size'];
		    		return parent::get_all($params);

		    	// 	if($messages->num_rows() > 0){
		    	// 		$this->set_data($messages->result(), true);
		    	// 	}else{
		    	// 		$this->set_error(array('message'=>'No messages found.'));
							// return $this->compose_result();
		    	// 	}
		    	}else{
		    		$this->set_error(array('message'=>'No messages found.'));
						return $this->compose_result();
		    	}
    		}
    	}else{
    		$this->set_error(array('message'=>'User not found.'));
				return $this->compose_result();
    	}
    }
	}
}