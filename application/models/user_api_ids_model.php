<?php
// Extend Base_model instead of CI_model
class User_api_ids_model extends Base_model
{
	public function __construct()
	{
		// List all fields of the table.
		// Primary key must be auto-increment and must be listed here first.
		$fields = array(
			'uai_id', 
			'usr_id', 
			'uai_type', 
			'uai_api_id', 
			'uai_api_token', 
			'uai_api_secret'
		);

		$searchable_fields = array('');

		// Call the parent constructor with the table name and fields as parameters.


		parent::__construct('user_api_ids', $fields, $searchable_fields, null);
	}

	// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function create($data, $field_list = array())
	{

		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{

		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		$this->db->join("user", "user.usr_id = user_api_ids.usr_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("user", "user.usr_id = user_api_ids.usr_id");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}

	public function get_one_based_on_id_type($uai_type, $id)
	{				
		$this->db->where('uai_type', $uai_type);
		$this->db->where('uai_api_id', $id);
		$this->db->join('user', "user.usr_id = {$this->table}.usr_id");
		$query = $this->db->get($this->table); 
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

		// Inherits the create, update, delete, get_one, and get_all methods of base_model.
	public function get_one_based_on_secret($uai_type, $secret)
	{				
		$this->db->where('uai_type', $uai_type);
		$this->db->where('uai_api_secret', $secret);
		$this->db->join('user', "user.usr_id = {$this->table}.usr_id");
		$query = $this->db->get($this->table); 
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function get_one_based_on_usr_id_secret($usr_id, $secret)
	{				
		$this->db->where('usr_id', $usr_id);
		$this->db->where('uai_api_secret', $secret);
		$query = $this->db->get($this->table); 
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function get_one_by_user($usr_id)
	{
		$this->db->where('usr_id', $usr_id);
		$query = $this->db->get($this->table); 
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}

	public function get_one_by_user_and_type($usr_id, $uai_type)
	{
		$this->db->where('usr_id', $usr_id);
		$this->db->where('uai_type', $uai_type);
		$query = $this->db->get($this->table); 
		
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}
}