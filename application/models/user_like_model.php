<?php
class User_like_model extends Base_model
{
	public function __construct()
	{
		$fields = array(
			'usl_id', 
			'usr_id', 
			'usl_user', 
			'usl_status', 
			'usl_date_created', 
			'usl_date_modified'
		);

		$searchable_fields = array('');

		parent::__construct('user_like', $fields, $searchable_fields, null);
	}

	public function create($data, $field_list = array())
	{
		return parent::create($data, $field_list);
	}

	public function update($data, $field_list = array())
	{
		return parent::update($data, $field_list);
	}

	public function get_one($id)
	{
		$this->db->join("user", "user.usr_id = user_like.usr_id");

		return parent::get_one($id);
	}

	public function get_all($params = array(), $order_by = array())
	{
		$this->db->join("user", "user.usr_id = user_like.usr_id");

		return parent::get_all($params, $order_by);
	}

	public function delete($id)
	{
		return parent::delete($id);
	}

	public function is_matched($user1, $user2)
	{
		// $this->db->join("user", "user.usr_id = user_like.usr_id");
		$this->db->where("usr_id", $user1);
		$this->db->where("usl_user", $user2);
		$query1 = $this->db->get($this->table);

		$this->db->where("usr_id", $user2);
		$this->db->where("usl_user", $user1);
		$query2 = $this->db->get($this->table);

		if($query1->num_rows > 0 && $query2->num_rows() > 0) {
			return true;
		}
		else {
			return false;
		}

	}
}