<?php
class Follow_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'fol_id', 
			'usr_id', 
			'fol_user', 
			'fol_status', 
			'fol_date_created', 
			'fol_date_modified'
		);

		parent::__construct('follow', $fields);
	}

	public function follow($data)
	{
		$this->set_message("Update Follow");
		
		$this->form_validation->set_rules('fol_id', 'Follow Id', 'integer');
		$this->form_validation->set_rules("usr_id", "User Inactive", "trim|integer|max_length[11]");
		$this->form_validation->set_rules("fol_user", "User", "trim|integer|max_length[11]");
		
		$error = $this->run_validators();
    if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
    	$this->db->where('usr_id', $data['fol_user']);
    	$this->db->where('usr_status', 'approved');
    	$users = $this->db->get('user');

    	if($users->num_rows() > 0){
    		if(isset($data['fol_id'])){
    			$this->db->where('fol_id', $data['fol_id']);
    			$this->db->where('follow.usr_id', $data['usr_id']);
	    		$this->db->where('fol_user', $data['fol_user']);
	    		$follows = $this->db->get('follow');

	    		if($follows->num_rows() > 0){
	    			$follow = $follows->row();

	    			if($follow->fol_status == 'inactive'){
	    				$data['fol_status'] = 'active';
	    			}else{
	    				$data['fol_status'] = 'inactive';
	    			}

	    			unset($data['fol_id']);
	    			$data['fol_date_modified'] = format_mysql_datetime();

	    			$this->db->where('fol_id', $follow->fol_id);
	  				$this->db->update('follow', $data);

	  				$return = array();
	  				$return['fol_id'] = $follow->fol_id;
	  				$return['fol_status'] = $data['fol_status'];

		        $this->set_data($return, true);
	    		}else{
	    			$this->set_error(array('message'=>'Follow not found.'));
	    		}
    		}else{
    			$this->db->where('follow.usr_id', $data['usr_id']);
	    		$this->db->where('fol_user', $data['fol_user']);
	    		$this->db->where('fol_status', 'active');
	    		$follows = $this->db->get('follow');

	    		if($follows->num_rows() == 0){
	    			$follow = array();

		    		$follow['usr_id'] = $data['usr_id'];
		    		$follow['fol_user'] = $data['fol_user'];
		    		$follow['fol_status'] = 'active';
		    		$follow['fol_date_created'] = format_mysql_datetime();
		    		$follow['fol_date_modified'] = format_mysql_datetime();

		    		$this->db->insert('follow', $follow);
		        $fol_id = $this->db->insert_id();

		        $return['fol_id'] = $fol_id;
		        $return['fol_status'] = 'active';
		       
	    			$this->set_data($return, true);
	    		}else{
    				$this->set_error(array('message'=>'User already faved.'));
	    		}
    		}
    	}else{
    		$this->set_error(array('message'=>'User not found.'));
    	}
    }
		return $this->compose_result();
	}


	public function get_all($data)
	{
		$this->set_message("List Following");

		$usr_ids = blocked_ids($data['usr_id']);

		$this->db->select('fol_id, fol_user, fol_status, fol_date_modified');
		$this->db->where('follow.usr_id', $data['usr_id']);

		if(!empty($usr_ids)){
			$this->db->where_not_in('fol_user', $usr_ids);
		}

		$this->db->where('fol_status', 'active');

		$this->db->order_by('fol_date_modified', 'desc');
		$params = array();
		$params['is_infinite'] = $data['is_infinite'];
		$params['page'] = $data['page'];
		$params['page_size'] = $data['page_size'];
		return parent::get_all($params);
		// $follows = $this->db->get('follow');

		// if($follows->num_rows() > 0){
		// 	$this->set_data($follows->result(), true);
		// }else{
		// 	$this->set_error(array('message'=>"No users found."));
		// }
		// return $this->compose_result();
	}
}