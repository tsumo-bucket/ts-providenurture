<?php
class Post_like_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'pol_id', 
			'usr_id', 
			'pos_id', 
			'pol_status', 
			'pol_date_created', 
			'pol_date_modified'
		);

		parent::__construct('post_like', $fields);

		$this->mythos->library('email');
	}

	public function update($data)
	{
		$this->set_message("Update Post Like");
		$this->form_validation->set_rules('pol_id', 'Post Like Id', 'integer');
		$this->form_validation->set_rules("usr_id", "User", "trim|integer|max_length[11]");
		$this->form_validation->set_rules("pos_id", "Post", "trim|integer|max_length[11]");

		$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
    } else {
    	$this->db->where('pos_id', $data['pos_id']);
    	$this->db->where('pos_status', 'active');
    	$posts = $this->db->get('post');

    	if($posts->num_rows() > 0){
    		if(isset($data['pol_id'])){
	    		$this->db->where('pol_id', $data['pol_id']);
	    		$this->db->where('post_like.usr_id', $data['usr_id']);
	    		$this->db->where('post_like.pos_id', $data['pos_id']);
	    		$post_likes = $this->db->get('post_like');

	    		if($post_likes->num_rows() > 0){
	    			$post_like = $post_likes->row();

	    			if($post_like->pol_status == 'active'){
	    				$data['pol_status'] = 'inactive';
	    			}else{
	    				$data['pol_status'] = 'active';
	    			}

	    			unset($data['pol_id']);

	    			$data['pol_date_modified'] = format_mysql_datetime();

	    			$this->db->where('pol_id', $post_like->pol_id);
	    			$this->db->update('post_like', $data);

	    			$return = array();
	    			$return['pol_id'] = $post_like->pol_id;
	    			$return['pos_id'] = $data['pos_id'];
	    			$return['pol_status'] = $data['pol_status'];

	    			if($data['pol_status'] == 'active'){
	    				$post = $posts->row();

	    				$this->db->where('usr_id', $post->usr_id);
			        $notifications = $this->db->get('notification');
			        $notification = $notifications->row();

			        if($notification->not_like == 'yes'){
			        	$this->db->select('usr_id, usr_email');
			        	$this->db->where('usr_id', $post->usr_id);
			        	$users = $this->db->get('user');
			        	$receiver = $users->row();

			        	if(emailIntercept($receiver->usr_email)){
				        	$this->db->select('user.usr_id, usr_gender, usr_screen_name, usr_token_id, user.img_id, img_image');
				        	$this->db->join("image", "image.img_id = user.img_id", "left outer");
				        	$this->db->where('user.usr_id', $data['usr_id']);
				        	$users = $this->db->get('user');
				        	$sender = $users->row();

				        	$params['mail_to'] = $receiver->usr_email;
									$params['subject'] = "Your Post has been Faved";
									$params['cc'] = "";
									$params['bcc'] = "";

									$content = array();
									$content['receiver'] = $receiver;
									$content['sender'] = $sender;

									$params['content'] = $this->load->view('email/fave_post', $content, true);
											
									$email = $this->email->send_mail($params,'email', 'template');
			        	}
			       	}	    				
	    			}

	    			$this->set_data($return, true);
	    		}else{
	    			$this->set_error(array('message'=>'Post like entry not found.'));
	    		}
	    	}else{
	    		$this->db->where('post_like.usr_id', $data['usr_id']);
	    		$this->db->where('post_like.pos_id', $data['pos_id']);
	    		$this->db->where('pol_status', 'active');
	    		$post_likes = $this->db->get('post_like');

	    		if($post_likes->num_rows() == 0){
	    			$post_like = array();

	    			$post_like['usr_id'] = $data['usr_id'];
	    			$post_like['pos_id'] = $data['pos_id'];
	    			$post_like['pol_status'] = 'active';
	    			$post_like['pol_date_created'] = format_mysql_datetime();
	    			$post_like['pol_date_modified'] = format_mysql_datetime();

	    			$this->db->insert('post_like', $post_like);
		        $pol_id = $this->db->insert_id();

		        $return['pol_id'] = $pol_id;
		        $return['pos_id'] = $data['pos_id'];
		        $return['flg_status'] = 'active';

		        $post = $posts->row();

    				$this->db->where('usr_id', $post->usr_id);
		        $notifications = $this->db->get('notification');
		        $notification = $notifications->row();

		        $this->db->where('usr_id', $post->usr_id);
		        $notifications = $this->db->get('notification');
		        $notification = $notifications->row();

		        if($notification->not_like == 'yes'){
		        	$this->db->select('usr_id, usr_email');
		        	$this->db->where('usr_id', $post->usr_id);
		        	$users = $this->db->get('user');
		        	$receiver = $users->row();

		        	$this->db->select('user.usr_id, usr_gender, usr_screen_name, usr_token_id, user.img_id, img_image');
		        	$this->db->join("image", "image.img_id = user.img_id", "left outer");
		        	$this->db->where('user.usr_id', $data['usr_id']);
		        	$users = $this->db->get('user');
		        	$sender = $users->row();

		        	$params['mail_to'] = $receiver->usr_email;
							$params['subject'] = "Your Post has been Faved";

							$content = array();
							$content['receiver'] = $receiver;
							$content['sender'] = $sender;

							$params['content'] = $this->load->view('email/fave_post', $content, true);
									
							$email = $this->email->send_mail($params,'email', 'template');
		       	}
		       
	    			$this->set_data($return, true);
	    		}else{
	    			$this->set_error(array('message'=>'Post already liked.'));
	    		}
	    	}
    	}else{
  			$this->set_error(array('message'=>'Post not found.'));
    	}
    }
		return $this->compose_result();
	}
}