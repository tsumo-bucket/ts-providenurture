<?php
class User_like_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'usl_id', 
			'usr_id', 
			'usl_user', 
			'usl_status', 
			'usl_date_created', 
			'usl_date_modified'
		);

		parent::__construct('user_like', $fields);

		$this->mythos->library('email');
	}

	public function favorite($data)
	{
		$this->set_message("Favorite User");
		
		$this->form_validation->set_rules("usr_id", "User", "trim");
		$this->form_validation->set_rules("usl_id", "User Like", "trim");
		$this->form_validation->set_rules("usl_user", "User", "trim|required|integer|max_length[11]");
			
    $error = $this->run_validators();
		if (!is_bool($error)) {
      $this->set_error($error, false);
    } else {
    	$this->db->where('usr_id', $data['usl_user']);
    	$this->db->where('usr_status', 'approved');
    	$users = $this->db->get('user');

    	if($users->num_rows() > 0){
    		if(isset($data['usl_id'])){
    			$this->db->where('usl_id', $data['usl_id']);
    			$this->db->where('user_like.usr_id', $data['usr_id']);
	    		$this->db->where('usl_user', $data['usl_user']);
	    		$user_likes = $this->db->get('user_like');

	    		if($user_likes->num_rows() > 0){
	    			$usl = $user_likes->row();

	    			if($usl->usl_status == 'inactive'){
	    				$data['usl_status'] = 'active';
	    			}else{
	    				$data['usl_status'] = 'inactive';
	    			}

	    			unset($data['usl_id']);
	    			$data['usl_date_modified'] = format_mysql_datetime();

	    			$this->db->where('usl_id', $usl->usl_id);
	  				$this->db->update('user_like', $data);

	  				$return = array();
	  				$return['usl_id'] = $usl->usl_id;
	  				$return['usl_status'] = $data['usl_status'];

	  				if($data['usl_status'] == 'active'){
			        $this->db->where('usr_id', $data['usl_user']);
			        $notifications = $this->db->get('notification');
			        $notification = $notifications->row();

			        if($notification->not_like == 'yes'){
			        	$this->db->select('usr_id, usr_email');
			        	$this->db->where('usr_id', $data['usl_user']);
			        	$users = $this->db->get('user');
			        	$receiver = $users->row();

			        	if(emailIntercept($receiver->usr_email)){
				        	$this->db->select('user.usr_id, usr_gender, usr_screen_name, usr_token_id, user.img_id, img_image');
				        	$this->db->join("image", "image.img_id = user.img_id", "left outer");
				        	$this->db->where('user.usr_id', $data['usr_id']);
				        	$users = $this->db->get('user');
				        	$sender = $users->row();

				        	$params['mail_to'] = $receiver->usr_email;
									$params['subject'] = "Your Profile has been Faved";
									$params['cc'] = "";
									$params['bcc'] = "";

									$content = array();
									$content['receiver'] = $receiver;
									$content['sender'] = $sender;

									$params['content'] = $this->load->view('email/fave_user', $content, true);
											
									$email = $this->email->send_mail($params,'email', 'template');
			        	}
			       	}
		        }

		        $this->set_data($return, true);
	    		}else{
	    			$this->set_error(array('message'=>'User like not found.'));
	    		}
    		}else{
    			$this->db->where('user_like.usr_id', $data['usr_id']);
	    		$this->db->where('usl_user', $data['usl_user']);
	    		$this->db->where('usl_status', 'active');
	    		$user_likes = $this->db->get('user_like');

	    		if($user_likes->num_rows() == 0){
	    			$user_like = array();

		    		$user_like['usr_id'] = $data['usr_id'];
		    		$user_like['usl_user'] = $data['usl_user'];
		    		$user_like['usl_status'] = 'active';
		    		$user_like['usl_date_created'] = format_mysql_datetime();
		    		$user_like['usl_date_modified'] = format_mysql_datetime();

		    		$this->db->insert('user_like', $user_like);
		        $usl_id = $this->db->insert_id();

		        $return['usl_id'] = $usl_id;
		        $return['usl_status'] = 'active';
		       
	        	$this->db->where('usr_id', $data['usl_user']);
		        $notifications = $this->db->get('notification');
		        $notification = $notifications->row();

		        if($notification->not_like == 'yes'){
		        	$this->db->select('usr_id, usr_email');
		        	$this->db->where('usr_id', $data['usl_user']);
		        	$users = $this->db->get('user');
		        	$receiver = $users->row();

		        	if(emailIntercept($receiver->usr_email)){
			        	$this->db->select('user.usr_id, usr_gender, usr_screen_name, usr_token_id, user.img_id, img_image');
			        	$this->db->join("image", "image.img_id = user.img_id", "left outer");
			        	$this->db->where('user.usr_id', $data['usr_id']);
			        	$users = $this->db->get('user');
			        	$sender = $users->row();

			        	$params['mail_to'] = $receiver->usr_email;
								$params['subject'] = "Your Profile has been Faved";
								$params['cc'] = "";
								$params['bcc'] = "";

								$content = array();
								$content['receiver'] = $receiver;
								$content['sender'] = $sender;

								$params['content'] = $this->load->view('email/fave_user', $content, true);
										
								$email = $this->email->send_mail($params,'email', 'template');
		        	}
		       	}
		       
	    			$this->set_data($return, true);
	    		}else{
    				$this->set_error(array('message'=>'User already faved.'));
	    		}
    		}
    	}else{
    		$this->set_error(array('message'=>'User not found.'));
    	}

    }
    return $this->compose_result();
	}

	public function get_all($data)
	{
		$this->set_message("List Faves");

		if(isset($data['source']) && ($data['source'] == 'i-faved' || $data['source'] == 'faved-me')){
			$this->db->where('usr_id', $data['usr_id']);
			$users = $this->db->get('user');
			$user = $users->row();

			if($data['source'] == 'i-faved'){
				$this->db->select('usl_id, usl_user, user.usr_id, usl_status, usl_date_modified');
				$this->db->where('user_like.usr_id', $data['usr_id']);
			}else{
				$this->db->select('usl_id, user.usr_id, usl_user, usl_status, usl_date_modified');
				$this->db->where('usl_user', $data['usr_id']);

				if($user->usr_interested != 'both'){
					$this->db->where('usr_gender', $user->usr_interested);
				}
			}
			
			$this->db->where('user.usr_status', 'approved');
			$this->db->where('usl_status', 'active');
			$this->db->where("(user.usr_id not in (select (CASE usr_id when ".$data['usr_id']." then blo_user else usr_id end) as usr_id from block where (blo_user = ".$data['usr_id']." AND blo_status = 'active') or (usr_id = ".$data['usr_id']." AND blo_status = 'active')))");
			
			if($data['source'] == 'i-faved'){
				$this->db->join("user", "user.usr_id = user_like.usl_user");
			}else{
				$this->db->join("user", "user.usr_id = user_like.usr_id");
			}

			$this->db->order_by('usl_date_modified', 'desc');
			// $user_likes = $this->db->get('user_like');
			$params = array();
			$params['is_infinite'] = $data['is_infinite'];
			$params['page'] = $data['page'];
			$params['page_size'] = $data['page_size'];
  		return parent::get_all($params);

			// if($user_likes->num_rows() > 0){
			// 	$this->set_data($user_likes->result(), true);
			// }else{
			// 	$this->set_error(array('message'=>"No users found."));
			// }
			
		}else{
			$this->set_error(array('message'=>"Nothing to display."));
    	return $this->compose_result();
		}
	}
}