<?php
class Flag_api_model extends Api_model
{
	public function __construct()
	{
		$fields = array(
			'flg_id', 
			'usr_id', 
			'pos_id', 
			'flg_status', 
			'flg_date_created', 
			'flg_date_modified'
		);

		parent::__construct('flag', $fields);
	}

	public function report($data)
	{
		$this->set_message("Update Flag");
		$this->form_validation->set_rules('flg_id', 'Flag Id', 'integer');
		$this->form_validation->set_rules("usr_id", "User", "trim|integer|max_length[11]");
		$this->form_validation->set_rules("pos_id", "Post", "trim|integer|max_length[11]");
		
		$error = $this->run_validators();
		if (!is_bool($error)) {
  		$this->set_error($error, false);
    } else {
    	$this->db->where('pos_id', $data['pos_id']);
    	$this->db->where('pos_status', 'active');
    	$posts = $this->db->get('post');

    	if($posts->num_rows() > 0){
	    	if(isset($data['flg_id'])){
	    		$this->db->where('flg_id', $data['flg_id']);
	    		$this->db->where('flag.usr_id', $data['usr_id']);
	    		$this->db->where('flag.pos_id', $data['pos_id']);
	    		$flags = $this->db->get('flag');

	    		if($flags->num_rows() > 0){
	    			$flag = $flags->row();

	    			if($flag->flg_status == 'active'){
	    				$data['flg_status'] = 'inactive';
	    			}else{
	    				$data['flg_status'] = 'active';
	    			}

	    			unset($data['flg_id']);

	    			$data['flg_date_modified'] = format_mysql_datetime();

	    			$this->db->where('flg_id', $flag->flg_id);
	    			$this->db->update('flag', $data);

	    			$return = array();
	    			$return['flg_id'] = $flag->flg_id;
	    			$return['pos_id'] = $data['pos_id'];
	    			$return['flg_status'] = $data['flg_status'];

	    			$this->set_data($return, true);
	    		}else{
	    			$this->set_error(array('message'=>'Flag entry not found.'));
	    		}
	    	}else{
	    		$this->db->where('flag.usr_id', $data['usr_id']);
	    		$this->db->where('flag.pos_id', $data['pos_id']);
	    		$this->db->where('flg_status', 'active');
	    		$flags = $this->db->get('flag');

	    		if($flags->num_rows() == 0){
	    			$flag = array();

	    			$flag['usr_id'] = $data['usr_id'];
	    			$flag['pos_id'] = $data['pos_id'];
	    			$flag['flg_status'] = 'active';
	    			$flag['flg_date_created'] = format_mysql_datetime();
	    			$flag['flg_date_modified'] = format_mysql_datetime();

	    			$this->db->insert('flag', $flag);
		        $flg_id = $this->db->insert_id();

		        $return['flg_id'] = $flg_id;
		        $return['pos_id'] = $data['pos_id'];
		        $return['flg_status'] = 'active';
		       
	    			$this->set_data($return, true);
	    		}else{
	    			$this->set_error(array('message'=>'Post already flaged.'));
	    		}
	    	}
    	}else{
  			$this->set_error(array('message'=>'Post not found.'));
    	}
    }
    return $this->compose_result();
	}
}