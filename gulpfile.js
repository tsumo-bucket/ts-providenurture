'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var notify = require("gulp-notify");

gulp.task('sass', function () {
	gulp.src('./resources/site/sass/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('./resources/site/css'))
		.pipe(notify("SASS compiled"));
});

gulp.task('watch', function () {
	gulp.watch('./resources/site/sass/**/*.scss', ['sass']);
});